exports.GetTimeFromTimeServer = function(successCallback,errorCallback){
	var url = "http://json-time.appspot.com/time.json";
	 var client = Ti.Network.createHTTPClient({
	     onload : function(e) {
	     	var timeObject = JSON.parse(this.responseText);
	       	Ti.API.info("Current Time from TimeServer:");
	       	Ti.API.info(new Date(timeObject.datetime));
	       	successCallback();
	     },
	     onerror : function(e) {
	         Ti.API.debug("Time Server: Error connecting:"+e.error);
	         if (errorCallback)errorCallback();
	     },
	     timeout : 5000
	 });
	 client.open("GET", url);
	 client.send();
};
