//Wrapper for Real Time Storage
var applicationKey ="MiNrWy";
var privateKey = "Find it Online";//;
var authenticationToken = "normalUser";

function MakeCall (method, object, callback){
	var url = 'https://storage-backend-prd-useast1.realtime.co/';
	//var url = "https://storage-backend-prd-useast1-s0001.realtime.co/";
	var client = Ti.Network.createHTTPClient({
	     onload : function(e) {
	         callback({text:this.responseText, status: this.statusText});
	         Ti.API.info(this.responseText);
	     	 Ti.API.info(this.statusText);
	     },
	     onerror : function(e) {
	     	
	     	callback(this);
	     },
	     timeout : 10000
	});
	
	client.open("POST", url+method);
	client.setRequestHeader("Content-Type", "application/json");
	client.setRequestHeader('Charset', 'utf-8');
	
	Ti.API.info(JSON.stringify(object));
	client.send(JSON.stringify(object));
}


exports.queryItems = function(table, key, properties, filter, startKey, limit, searchForward, callback){
	var object = {};
	object.applicationKey = applicationKey;
	//if (privateKey!= null) object.privateKey = privateKey;
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	object.key = key;
	if(properties) object.properties = properties;
	if(filter) object.filter = filter;
	if(startKey) object.startKey = startKey;
	if(limit) object.limit = limit;
	if(searchForward) object.searchForward = searchForward;
	object.callback = callback;
	
	MakeCall("queryItems", object, callback);	
};


//Lists all the items in table
exports.listItems = function(table, properties, filter, startKey, limit, callback){
	var object = {};
	object.applicationKey = applicationKey;
	//if (privateKey!= null) object.privateKey = privateKey;
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	if(properties) object.properties = properties;
	if(filter) object.filter = filter;
	if(startKey) object.startKey = startKey;
	if(limit) object.limit = limit;
	object.callback = callback;
	
	MakeCall("listItems", object, callback);	
};


exports.updateItem = function (table, key, item, callback){
	var object = {};
	object.applicationKey = applicationKey;
	//if (privateKey!= null) object.privateKey = privateKey;
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	object.dataKey = {
		primary: primaryKey,
		secondary: secondaryKey
	};
	object.item = item;
	object.callback = callback;
	//Ti.API.info(object);
	
	MakeCall("updateItem", object, callback);
};
/*
putItem("PuzzlesMessages", {receiverID: "Ciao2", messageID: "Bella2"},function(e){
	Ti.API.info(e);
});
*/

exports.putItem = function (table, item, callback){
	var object = {};
	object.applicationKey = applicationKey;
	//if (privateKey!= null) object.privateKey = privateKey;
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	object.item = item;
	object.callback = callback;
	//Ti.API.info(object);
	
	MakeCall("putItem", object, callback);
};


exports.increaseItem = function (table, key, propertyToIncrease, value, callback){
	var object = {};
	object.applicationKey = applicationKey;
	
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	object.key = key;
	object.property = propertyToIncrease;
	//object.callback = callback;
	object.value = value;
	
	MakeCall("incr", object, callback);
};


exports.deleteItem = function (table, key, properties, callback){
	var object = {};
	object.applicationKey = applicationKey;
	
	if (authenticationToken != null) object.authenticationToken = authenticationToken;
	object.table = table;
	object.key = key;
	if(properties) object.property = properties;
	//object.callback = callback;
	
	MakeCall("deleteItem", object, callback);
};


exports.authenticate = function(authenticationToken, timeout, roles, policies, callback){
	 if (timeout == "never") timeout = 9999999999; //~300 years;
	 var object = {
		applicationKey: applicationKey,
		privateKey: privateKey,
		// The token name
		authenticationToken: authenticationToken,
		// The token is valid for 900 seconds
		timeout: timeout,
		// The token will inherit the policies of the ReadOnly role
		roles: roles,//["ReadOnly"],
		// The additional update policy for ProductCatalog table
		policies: policies//{tables: {"ProductCatalog": { allow: "U"}}}             
	}; 
	
	MakeCall("authenticate", object, callback);
};
