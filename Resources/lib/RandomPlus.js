exports.ShuffleBagStandard = function(){
  var object = {};
  object.data=[];
  object.cursor=-1;

  function add(thingy){
    if(arguments.length>1){
      for(var i=arguments[1];i>0;--i)
        object.data.push(thingy);
    }else{
      object.data.push(thingy);
    }
    object.cursor=object.data.length-1;
  }
  
  function remove(item){
  	object.data.splice(object.data.indexOf(item),1);
  	if (object.cursor > object.data.length-1) object.cursor = object.data.length-1;
  }

  function next(){
    if(object.cursor<1){
      object.cursor=object.data.length-1;
      return object.data[0];
    }
    var grab=Math.floor(Math.random()*(object.cursor+1));
    var temp=object.data[grab];
    object.data[grab]=object.data[object.cursor];
    object.data[object.cursor]=temp;
    object.cursor--;
    return temp;
  }

  object.add=add;
  object.remove = remove;
  object.next=next;
  
  return object;
};


exports.FakeShuffleBag = function(){
	this.data=[];
  	this.cursor=-1; // -1 Cause I call this for the first phrase. otherwise it gives me the second.

	  function add(thingy){
	    if(arguments.length>1){
	      for(var i=arguments[1];i>0;--i)
	        this.data.push(thingy);
	    }else{
	      this.data.push(thingy);
	    }
	  }
	  
	  function remove(item){
	  	this.data.splice(this.data.indexOf(item),1);
	  	if (this.cursor > this.data.length-1) this.cursor = this.data.length-1;
	  }
	
	  function next(){
	  	this.cursor++;
	   	if (this.cursor > this.data.length-1) this.cursor = 0;
	   	return this.data[this.cursor];
	  }
	
	  this.add=add;
	  this.remove = remove;
	  this.next=next;
};


exports.ShuffleArray = function(o){ //v1.0
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

exports.ShuffleArrayCopy = function(array){ //v1.0
	var o = JSON.parse(JSON.stringify(array));
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};


exports.RandomRange = function (minIncluded,maxExcluded){
	var value = Math.round(Math.random()*(maxExcluded-1-minIncluded))+minIncluded;
	return value;
};
