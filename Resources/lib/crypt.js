exports.encrypt = function (inText){
	var securely = require('bencoding.securely');
	var stringCrypto = securely.createXPlatformCrypto();
	var outText = stringCrypto.encrypt(KeyGen(), inText);
	
	return outText;
};


exports.decrypt = function (inText){
	var securely = require('bencoding.securely');
	var stringCrypto = securely.createXPlatformCrypto();
    var outText = stringCrypto.decrypt(KeyGen(), inText);
	
	return outText;
};


//Prevent some hacking;
function KeyGen(){
	var one = "te";
	var two = "lla";
	var three = "nu";
	return three+one+two;
}