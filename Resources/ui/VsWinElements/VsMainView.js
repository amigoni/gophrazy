function Create(){
	var yourTurnArray = [];
	var theirTurnArray = [];
	var finishedArray = [];
	
	globals.gameData.userName = "mozza1";
	
	var gamesData =[
		{
			id: "123456",
			type: "singlePvP", //singlePvP
			state: "open", //open,cancelled,finished
			winner: null, //p1UserName,p2UserName
			winType: null,//normal,resigned
			rounds:[
				{number: 0, p1Score: null, p2Score: null, state: "open", wonBy: null, phraseID: 123},
				{number: 1, p1Score: null, p2Score: null, state: "open", wonBy: null, phraseID: 124},
				{number: 2, p1Score: null, p2Score: null, state: "open", wonBy: null, phraseID: 125}
			],
			totalScore:{p1: null, p2: null},
			currentRound: 0,
			currentTurn: "mozza1", //p1userName, p2userName
			p1UserName: "mozza1",
			p2UserName: "mozza2",
			p1Data: {
				userName: "mozza1",
				userFacebookID: null,
				userTwitterID: null,
				roundsData: [
					{number: 0, score: null},
					{number: 1, score: null},
					{number: 2, score: null}
				]
			},
			p2Data: {
				userName: "mozza2",
				userFacebookID: null,
				userTwitterID: null,
				roundsData: [
					{number: 0, score: null},
					{number: 1, score: null},
					{number: 2, score: null}
				]
			},
			timeStarted: "2014-12-15T00:00:00Z", //time
			timeLastPlay: "2014-12-15T00:02:00Z", //time
			timeForfeit: "2014-12-17T00:02:00Z", //time
			timeFinished: null, //time
			timeCancelled: null //time
		},
		{
			type: "singlePvP", //singlePvP
			state: "open", //open,cancelled,finished
			winner: null, //p1UserName,p2UserName
			winType: null,//normal,resigned
			rounds:[
				{number: 0, p1Score: 5210, p2Score: null, state: "finished", wonBy: "p1", phraseID: 123},
				{number: 1, p1Score: null, p2Score: null, state: "open", wonBy: null, phraseID: 124},
				{number: 2, p1Score: null, p2Score: null, state: "open", wonBy: null, phraseID: 125}
			],
			totalScore:{p1: 5210, p2: null},
			currentRound: 1,
			currentTurn: "mozza2", //p1userName, p2userName
			p1Data: {userName: "mozza1"},
			p2Data: {userName: "mozza2"},
			timeStarted: "2014-12-15T00:00:00Z", //time
			timeLastPlay: "2014-12-15T00:02:00Z", //time
			timeForfait: "2014-12-17T00:02:00Z", //time
			timeFinished: null, //time
			timeCancelled: null //time
		},
		{
			type: "singlePvP", //singlePvP
			state: "finished", //open,cancelled,finished
			winner: "mozza1", //p1UserName,p2UserName
			winType: "normal",//normal,resigned
			rounds:[
				{number: 0, p1Score: 5210, p2Score: 6210, state: "finished", wonBy: "p1", phraseID: 123},
				{number: 1, p1Score: 5210, p2Score: 6210, state: "finished", wonBy: "p1", phraseID: 124},
				{number: 2, p1Score: 5210, p2Score: 6210, state: "finished", wonBy: "p1", phraseID: 125}
			],
			totalScore:{p1: 5210*3, p2: 6210*3},
			currentRound: 2,
			currentTurn: "mozza1", //p1userName, p2userName
			p1Data: {userName: "mozza1"},
			p2Data: {userName: "mozza2"},
			timeStarted: "2014-12-15T00:00:00Z", //time
			timeLastPlay: "2014-12-15T00:02:00Z", //time
			timeForfait: "2014-12-17T00:02:00Z", //time
			timeFinished: "2014-12-15T00:15:00Z", //time
			timeCancelled: null //time
		}
	];
	
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight-44,
		top: 0,
		left: 0
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: view.height,
		contentWidth: view.width,
		contentHeight: "auto"
	});
	
	var container;
	
	view.add(scrollView);
	
	
	function Update(gamesData){
		if(container != null) scrollView.remove(container);
		container = null;
		container = Ti.UI.createView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			layout: "vertical",
			top: 0
		});
		
		container.addEventListener("postlayout", function(){
			if(container.size.height <= view.height) scrollView.contentHeight = view.height+1;
		});
		
		//Create Sections
		for(var i = 0; i < gamesData.length; i++){
			var game = gamesData[i];
			if(game.state == "open"){
				//Your Turn
				if(game.currentTurn == globals.gameData.userName) yourTurnArray.push(game);
				//Their Turn
				else theirTurnArray.push(game);
			}
			//Finished
			else finishedArray.push(game);
		}
		
		if(yourTurnArray.length > 0) container.add(CreateSection("Your Turn", globals.colors.green, yourTurnArray));
		if(theirTurnArray.length > 0) container.add(CreateSection("Their Turn", globals.colors.red, theirTurnArray));
		if(finishedArray.length > 0) container.add(CreateSection("Finished", globals.colors.black, finishedArray));
		
		scrollView.add(container);
	}
	
	
	function MoveLeft (){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: view.left-globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}
	
	
	function MoveRight (){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}), function(){Close();});
	}
	
	
	function Close(){
		
	}
	
	view.MoveLeft = MoveLeft;
	view.MoveRight = MoveRight;
	view.Close = Close;
	
	Update(gamesData);
	
	
	return view;
}

module.exports = Create;


function CreateHeader(){
	var view = Ti.UI.createView({
		
	});
	
	return view;
}


function CreateSection(title, color, gameDataArray){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: 20
	});
	
	var bg = Ti.UI.createView({
		backgroundColor: color,
		borderRadius: 8,
		width: globals.platform.width-20,
		height: 50,
		top: 0
	});
	
	var container = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: 0,
		layout: "vertical"
	});
	
	container.addEventListener("postlayout", function(){
		bg.height = container.size.height;
	});
	
	var header = Ti.UI.createLabel({
		text: title,
		font: globals.fonts.bold25,
		color: globals.colors.white,
		textAlign:"center",
		width: globals.platform.width-20,
		height: 30,
		top: 2
	});
	
	container.add(header);
	
	for(var i = 0; i < gameDataArray.length; i++){
		var row = CreateGameRow(gameDataArray[i]);
		container.add(row);
	}
	
	container.add(Ti.UI.createView({height:10}));
	
	view.add(bg);
	view.add(container);
	
	
	return view;
}


function CreateGameRow(rowData){
	var green = "#468C00";
	var red = "#CD4E3A";
	var opponentName = "";
	var scoreColor = "#468C00";
	var scoreText = "";
	var roundText = "";
	
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.white,
		borderColor: globals.colors.black,
		borderWidth: 2,
		borderRadius: 10,
		width: globals.platform.width-10,
		height: 55,
		top: 5,
		rowData: rowData
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("touchend", function(){
		view.fireEvent("openMatchDetail", {matchData: rowData});
	});
	
	var icon = Ti.UI.createView({
		backgroundColor: "black",
		borderColor: 10,
		width: 40,
		height: 40,
		left: 10
	});
	
	var label1 = Ti.UI.createLabel({
		text: opponentName,
		font: globals.fonts.bold13,
		color: globals.colors.black,
		textAlign: "left",
		width: globals.platform.width-100,
		height: 15,
		top: 5,
		left: 60,
	});
	
	var label2 = Ti.UI.createLabel({
		text: "Record Wins 10-3 Losses",
		font: globals.fonts.regular13r,
		color: globals.colors.black,
		textAlign: "left",
		width: globals.platform.width-100,
		height: 15,
		top: 20,
		left: 60,
	});
	
	var label3 = Ti.UI.createLabel({
		text: "1 Day Left to Play",
		font: globals.fonts.regular12r,
		color: globals.colors.black,
		textAlign: "left",
		width: globals.platform.width-100,
		height: 13,
		top: 35,
		left: 60
	});
	
	var label4 = Ti.UI.createLabel({
		text: roundText,
		font: globals.fonts.bold15,
		color: globals.colors.black,
		textAlign: "right",
		width: Ti.UI.SIZE,
		height: 17,
		top: 5,
		right: 10
	});
	
	var label5 = Ti.UI.createLabel({
		text: scoreText,
		font: globals.fonts.bold15,
		color: scoreColor,
		textAlign: "right",
		width: Ti.UI.SIZE,
		height: 17,
		top: 30,
		right: 10
	});
	
	
	view.add(icon);
	view.add(label1);
	view.add(label2);
	view.add(label3);
	view.add(label4);
	view.add(label5);
	
	
	function Update(){
		if(rowData.p1Data.userName == globals.gameData.userName){
			opponentName = rowData.p2Data.userName;
			if(rowData.totalScore.p1 > rowData.totalScore.p2) scoreColor = green;
			else scoreColor = red;
			scoreText = FormatTimeText(rowData.totalScore.p1-rowData.totalScore.p2);
		}
		else{
			opponentName = rowData.p1Data.userName;
			if(rowData.totalScore.p2 > rowData.totalScore.p1) scoreColor = green;
			else scoreColor = red;
			scoreText = FormatTimeText(rowData.totalScore.p2-rowData.totalScore.p1);
		}
		
		
		if(rowData.state == "open"){
			roundText = "Round: "+(rowData.currentRound+1);
		}
		else{
			if(rowData.winner == globals.gameData.userName ) roundText = "You Won";
			else roundText = "You Lost";
		}
		
		function FormatTimeText(time){
			var sign = "";
			if(time > 0) sign = "-";
			time = Math.abs(time);
			var string = sign;
			var minutes = Math.floor(Math.floor(time/1000)/60);
			var seconds = Math.floor(time/1000)%60;
			if(minutes > 0 && seconds < 10) seconds = "0"+seconds.toString();
			var milliseconds = time%1000;
			var centiseconds = Math.round(milliseconds/10);
			if(centiseconds < 10) centiseconds = "0"+centiseconds.toString();
			
			if(minutes > 0) string += minutes+":";
			string += seconds+"."+centiseconds+"s";
			
			return string;
		}
		
		label1.text = opponentName;
		label5.text = scoreText;
		label5.color = scoreColor;
		label4.text = roundText;
	}
	
	view.Update = Update;
	
	Update();
	
	
	return view;
}
