function Create(){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight-44,
		top: 0,
		left: globals.platform.width
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: view.height,
		contentWidth: view.width,
		contentHeight: "auto"
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	container.addEventListener("postlayout", function(){
		if(container.size.height <= view.height) scrollView.contentHeight = view.height+1;
	});
	
	
	var header = CreateHeader({
		you:{picture: null, userName: "mozza1"},
		opponent:{picture: null, userName: "mozza2"}
	});
		
	var roundData =[
		{number:0, p1Score: 5210, p2Score: 6210, state: "finished", wonBy: "p1"},
		{number:1, p1Score: 5210, p2Score: 6210, state: "finished", wonBy: "p1"},
		{number:2, p1Score: 5210, p2Score: 6210, state: "finished", wonBy: "p1"}
	];
	
	var roundsViewsData = [
		{roundNumber: 0, leftText: "1:53s", leftTextColor: globals.colors.red, leftDifferenceText: "+23s", rightText: "1:30s", rightTextColor: globals.colors.green},
		{roundNumber: 1, leftText: "1:53s", leftTextColor: globals.colors.red, leftDifferenceText: "+23s", rightText: "1:30s", rightTextColor: globals.colors.green},
		{roundNumber: 2, leftText: "1:53s", leftTextColor: globals.colors.red, leftDifferenceText: "+23s", rightText: "1:30s", rightTextColor: globals.colors.green}
	];
	
	var totalRowData = {leftText: "1:53s", leftTextColor: globals.colors.red, leftDifferenceText: "+23s", rightText: "1:30s", rightTextColor: globals.colors.green};		
	
	container.add(header);
	
	var roundRows = [];
	for(var i = 0; i < roundsViewsData.length; i++){
		roundRows[i] = CreateRoundRow(roundsViewsData[i]);
		if(i == 0) roundRows[i].top = 10;
		container.add(roundRows[i]);
	}
	
	var totalRow = CreateTotalRow(totalRowData);
	var buttonBar = CreateButtonBar(true);
	
	container.add(totalRow);
	container.add(buttonBar);
	
	
	scrollView.add(container);
	view.add(scrollView);
	
	
	function Update(gamesData){
		
		
	}
	
	//Update(gamesData);
	
	function MoveLeft (){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: view.left-(globals.platform.width),
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}
	
	
	function MoveRight (){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: view.left+globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}), function(){Close();});
	}
	
	
	function Close(){
		
	}
	
	view.MoveLeft = MoveLeft;
	view.MoveRight = MoveRight;
	view.Close = Close;
	
	
	return view;
}

module.exports = Create;



function CreateHeader(peopleData){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: 10
	});
	
	var leftContainer = CreatePersonThing(peopleData.you.picture, peopleData.you.userName);
	leftContainer.left = 0;
	
	var rightContainer = CreatePersonThing(peopleData.opponent.picture, peopleData.opponent.userName);;
	rightContainer.right = 0;
	
	view.add(leftContainer);
	view.add(rightContainer);
	
	
	
	function CreatePersonThing(picture, userName){
		var container = Ti.UI.createView({
			width: globals.platform.width/2,
			height: Ti.UI.SIZE,
			layout: "vertical",
			top: 0
		});
		
		var picture = Ti.UI.createView({
			backgroundImage: picture,
			backgroundColor: globals.colors.white,
			borderRadius: 5,
			borderWidth: 1,
			borderColor: globals.colors.black,
			width: 55,
			height: 55
		});
		
		var nameLabel = Ti.UI.createLabel({
			text: userName,
			font: globals.fonts.bold15,
			color: globals.colors.darkBrown,
			textAlign:"center",
			width: Ti.UI.SIZE,
			height: 17,
			top: 2
		});
		
		container.add(picture);
		container.add(nameLabel);
		
		return container;
	};
	
	
	return view;
}


function CreateRoundRow(data){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 5
	});
	
	var header = Ti.UI.createLabel({
		text: "Round "+data.roundNumber,
		font: globals.fonts.bold13,
		color: globals.colors.darkBrown,
		textAlign:"center",
		width: Ti.UI.SIZE,
		height: 15
	});
	
	var container1 = Ti.UI.createView({
		width: globals.platform.width,
		height: 40
	});
	
	var bg = Ti.UI.createView({
		backgroundColor: "black",
		borderRadius: 7,
		width: globals.platform.width-20,
		height: 40,
		opacity: .1
	});
	
	var leftScore = CreateScoreView(data.leftText, data.leftTextColor, data.leftDifferenceText);
	leftScore.left = 0;
	var rightScore = CreateScoreView(data.rightText, data.rightTextColor, null);
	rightScore.right = 0;
	
	container1.add(bg);
	container1.add(leftScore);
	container1.add(rightScore);
	
	view.add(header);
	view.add(container1);
	
	
	
	return view;
}


function CreateTotalRow(data){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: 40,
		top: 5
	});
	
	var leftScore = CreateScoreView(data.leftText, data.leftTextColor, data.leftDifferenceText);
	leftScore.left = 0;
	var rightScore = CreateScoreView(data.rightText, data.rightTextColor, null);
	rightScore.right = 0;
	
	var titleLabel = Ti.UI.createLabel({
		text: "Total",
		font: globals.fonts.bold20,
		color: globals.colors.darkBrown,
		textAlign:"center",
		width: Ti.UI.SIZE,
		height: 15
	});
	
	view.add(titleLabel);
	view.add(leftScore);
	view.add(rightScore);
	
	
	return view;
}


function CreateButtonBar(showPlayButton){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: 5
	});
	
	var rightContainer = Ti.UI.createView({
		width: globals.platform.width/2,
		height: Ti.UI.SIZE,
		right: 0,
		layout: "vertical"
	});
	
	var playButton = Ti.UI.createView({
		backgroundImage:"/images/playSignNoShadow.png",
		width: 86*.9,
		height: 43*.9,
		buttonID: "vsMatchDetailViewPlayButton"
	});
	
	Common.MakeButton(playButton);
	
	playButton.addEventListener("touchend", function(e){
		
	});
	
	var timeLeftLabel = Ti.UI.createLabel({
		text: "1 Day left",
		font: globals.fonts.regular12,
		color: globals.colors.black,
		textAlign: "center",
		width: Ti.UI.SIZE,
		height: 15,
		top: 0
	});
	
	rightContainer.add(playButton);
	rightContainer.add(timeLeftLabel);
	
	
	var leftContainer = Ti.UI.createView({
		width: globals.platform.width/2,
		height: Ti.UI.SIZE,
		left: 0
	});
	
	var resignButton = Ti.UI.createView({
		backgroundImage:"/images/playSignNoShadow.png",
		width: 85*.7,
		height: 43*.7,
		buttonID: "vsMatchDetailViewResignButton"
	});
	
	Common.MakeButton(resignButton);
	
	resignButton.addEventListener("touchend", function(e){
		
	});
	
	leftContainer.add(resignButton);
	
	
	view.add(leftContainer);
	view.add(rightContainer);
	
	
	
	return view;
}


function CreateScoreView(score, color, difference){
	var view = Ti.UI.createView({
		width: (globals.platform.width-20)/2,
		height: Ti.UI.SIZE
	});
	
	var container = Ti.UI.createView({
		width:  Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal"
	});
	
	var scoreLabel = Ti.UI.createLabel({
		text: score,
		font: globals.fonts.bold25,
		color: color,
		textAlign:"center",
		width: Ti.UI.SIZE,
		height: 27
	});
	
	var differenceLabel = Ti.UI.createLabel({
		text: difference,
		font: globals.fonts.bold13,
		color: globals.colors.darkBrown,
		textAlign:"center",
		width: Ti.UI.SIZE,
		height: 17,
		bottom: 0
	});
	
	container.add(scoreLabel);
	container.add(differenceLabel);
	
	view.add(container);
	
	
	return view;
}
