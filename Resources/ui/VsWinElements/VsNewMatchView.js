function Create(){
	var rowsData = [
		{iconImage:"/images/faceBookSquareIcon.png", text:"Random Opponent", callback: NewMatchRandom},
		{iconImage:"/images/faceBookSquareIcon.png", text:"Find by Username", callback: NewMatchFind},
		{iconImage:"/images/faceBookSquareIcon.png", text:"Facebook Friend", callback: NewMatchFacebook},
		{iconImage:"/images/faceBookSquareIcon.png", text:"Twitter Follower", callback: NewMatchTwitter}
	];
	
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight-44,
		top: 0,
		left: globals.platform.width
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: view.height,
		contentWidth: view.width,
		contentHeight: "auto"
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	container.addEventListener("postlayout", function(){
		if(container.size.height <= view.height) scrollView.contentHeight = view.height+1;
	});
	
	
	container.add(Ti.UI.createLabel({
		text: "New Game",
		font: globals.fonts.bold30,
		color: globals.colors.black,
		textAlign:"center",
		width: Ti.UI.SIZE,
		height: 35,
		top: 10
	}));
	
	for(var i = 0; i < rowsData.length; i++){
		container.add(CreateRow(rowsData[i]));
	}
	
	scrollView.add(container);
	view.add(scrollView);
	
	
	function NewMatchRandom(){
		alert("Random");
	}
	
	function NewMatchFind(){
		alert("Find");	
	}
	
	function NewMatchFacebook(){
		alert("Facebook");
	}
	
	function NewMatchTwitter(){
		alert("Twitter");
	}
	
	
	function MoveLeft (){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: view.left-globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}
	
	
	function MoveRight (){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: view.left+globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}), function(){Close();});
	}
	
	
	function Close(){
		
	}
	
	view.MoveLeft = MoveLeft;
	view.MoveRight = MoveRight;
	view.Close = Close;
	
	
	return view;
}

module.exports = Create;


function CreateRow(data){
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.white,
		borderRadius: 7,
		borderWidth: 2,
		borderColor: globals.colors.black,
		backgroundImage:"",
		width: globals.platform.width-20,
		height: 55,
		top: 5,
		buttonID: "vsNewMatchRow"
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("touchend", function(){
		data.callback();
	});
	
	var icon = Ti.UI.createImageView({
		//backgroundColor: "green",
		image: data.iconImage,
		height: 45,
		left: 10
	});
	
	var label = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: data.text,
		font: globals.fonts.bold20,
		color: globals.colors.black,
		textAlign:"left",
		width: view.width-80,
		height: 25,
		left: 70
	});
	
	view.add(icon);
	view.add(label);
	
	
	return view;
}