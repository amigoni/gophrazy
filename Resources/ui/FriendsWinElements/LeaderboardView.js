exports.createView = function(data, topSpace){
	if(!topSpace) topSpace = 0;
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44-topSpace,
		top: 44+topSpace,
		separatorColor: "transparent",
		separatorStyle: false,
		opacity: 1
		//left: type == "main" ? 0 : globals.platform.width,
	});
	
	
	var tableData = [];
	for(var i = 0; i < data.length; i++){
		var rank = i+1;
		//if (rank == 4)rank = 99;
		var row = CreateRow(data[i],rank);
		tableData.push(row);
	}
	
	var inviteRow = CreateInviteRow();
	tableData.push(inviteRow);
	tableView.data = tableData;
	
	
	function CreateRow(rowData,rank){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: 40,
			selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		});
		
		view.addEventListener("singletap", function(){
			view.fireEvent("clicked",{rowData:rowData});
		});
		
		var rankContainer = Ti.UI.createView({
			//backgroundColor: "green",
			width: Ti.UI.SIZE,
			height: 35,
			layout: "horizontal",
			left: 10
		});
		
		
		var rankFont = globals.fonts.bold15;
		if (rank == 1) rankFont = globals.fonts.bold30;
		else if (rank == 2)rankFont = globals.fonts.bold25;
		else if (rank == 3)rankFont = globals.fonts.bold20;
		
		var rankLabel1 = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rank,
			font: rankFont,
			color: rowData.isMe == false ? globals.colors.darkBrown : "#BF593F",
			width: Ti.UI.SIZE,
			height: 35
		});
		
		var rankLabelText = "th";
		var rankText = rank.toString();
		var lastDigit = rankText[rankText.length-1];
		if (lastDigit == "1") rankLabelText = "st";
		else if (lastDigit == "2") rankLabelText = "nd";
		if (lastDigit == "3") rankLabelText = "rd";
		
		var rankLabel2 = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rankLabelText ,
			font: globals.fonts.bold10,
			color: rowData.isMe == false ? globals.colors.darkBrown : "#BF593F",
			textAlign: "right",
			height: 35
		});
		
		rankContainer.add(rankLabel1);
		rankContainer.add(rankLabel2);
		
		var nameLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.isMe == false ? rowData.name : "Me",
			font: globals.fonts.bold15,
			color: rowData.isMe == false ? globals.colors.darkBrown : "#BF593F",
			textAlign: "left",
			width: globals.platform.width-110,
			height: 20,
			left: 45
		});
		
		var scoreLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.score,
			font: globals.fonts.bold15,
			color:  rowData.isMe == false ? globals.colors.darkBrown : "#BF593F",
			textAlign: "right",
			width: 70,
			height: 20,
			right: 10
		});
		
		view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1,bottom: 0}));
		view.add(rankContainer);
		view.add(nameLabel);
		/*
		view.add(Ti.UI.createView({
			backgroundImage:"/images/phrasesIcon.png",
			width: 24,
			height: 24,
			right: 38
		}));
		*/
		view.add(scoreLabel);
		
		
		return view;
	}
	
	return tableView;
};


function CreateInviteRow(){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 40,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
	});
	
	view.addEventListener("singletap", function(){
		globals.rootWin.OpenWindow("inviteWin");
	});
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 34,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/friendsIconLarge.png",
		width: 37,
		height: 34
	}));
	
	title.add(Ti.UI.createLabel({
		text: "Invite your Friends",
		font: globals.fonts.bold15,
		color: globals.colors.darkBrown,
		textAlign: "left"
	}));
	
	view.add(title);
	
	return view;
};
