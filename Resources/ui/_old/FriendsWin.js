exports.loadWin = function() {
	var GroupsController = require("controller/GroupsController");
	var FriendsController = require("controller/FriendsController");
	var AddMemberWin = require("ui/FriendsWinElements/AddMemberWin");
	var ButtonBar = require("ui/FriendsWinElements/ButtonBar");
	var InviteWin = require("ui/FriendsWinElements/InviteWin");
	var LeaderboardView = require("ui/FriendsWinElements/LeaderboardView");
	var NavBar = require("ui/FriendsWinElements/NavBar");
	var SignaturesView = require("ui/FriendsWinElements/SignaturesView");
	var TeamView = require("ui/FriendsWinElements/TeamView");

	var leaderboardData = FriendsController.GetFriendsWithAppByScore();
	var teamData = FriendsController.GetTeamMembers();

	var view = Titanium.UI.createView({
		backgroundImage : "/images/beigeBg.png",
		width : globals.platform.width,
		height : globals.platform.height - globals.adsBannerSpace,
		//opacity: 0,
		bubbleParent : false,
		top : -(globals.platform.height - globals.adsBannerSpace),
		zIndex: 1
	});

	view.addEventListener("closeWin", function() {
		Close();
	});

	view.addEventListener("openInvitePopUp", function() {
		globals.rootWin.OpenWindow("inviteWin");
	});

	view.addEventListener("openAddMemberPopUp", function() {
		var addMememberWin = AddMemberWin.createView(FriendsController.GetAddMememberData());
		addMememberWin.addEventListener("closed", function() {
			globals.rootWin.remove(addMememberWin);
			addMememberWin = null;
		});

		globals.rootWin.add(addMememberWin);
		addMememberWin.Open();
	});

	var navBar = NavBar.createView();

	var buttonBar = ButtonBar.createView();
	buttonBar.addEventListener("clicked", function(e) {
		if (e.button == "leaderboard") {
			leaderboardView.opacity = 1;
			signaturesView.opacity = 0;
			teamView.opacity = 0;
		} else if (e.button == "signatures") {
			leaderboardView.opacity = 0;
			signaturesView.opacity = 1;
			teamView.opacity = 0;
		} else if (e.button == "team") {
			leaderboardView.opacity = 0;
			signaturesView.opacity = 0;
			teamView.opacity = 1;
		}
	});

	var leaderboardView = LeaderboardView.createView(leaderboardData);
	var signaturesView = SignaturesView.createView();
	var teamView = TeamView.createView(teamData);

	view.add(navBar);
	view.add(buttonBar);
	view.add(leaderboardView);
	view.add(signaturesView);
	view.add(teamView);

	function Open() {
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}

	function Close() {
		navBar.Close();
		teamView.Close();
		DisappearAnimation();
	}

	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}


	view.Open = Open;
	view.Close = Close;

	return view;
};
