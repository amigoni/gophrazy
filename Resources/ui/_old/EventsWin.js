exports.loadWin = function(){
	var GroupsController = require("controller/GroupsController");
	var UpgradeController = require("controller/UpgradeController");
	var MainTableView = require("ui/EventsWinElements/MainTableView");
	var EventTableView = require("ui/EventsWinElements/EventTableView");
	
	var eventsData = GroupsController.GetEventsData();
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: -(globals.platform.actualHeight)
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	var navBar = NavBar();
	
	var tableViewsController = CreateTableViewsController();
	
	tableViewsController.addEventListener("openGroup", function(e){
		var eventPhrasesCount = globals.col.phrases.count({eventID:e.rowData.eventID});
		if (eventPhrasesCount != e.rowData.phrasesCount){
			UpgradeController.GetFile("https://s3.amazonaws.com/Wordhop/event_"+e.rowData.eventID+".txt", function(e1){
				GroupsController.UpgradeEventPhrases(JSON.parse(e1.responseText),e.rowData.eventID);
				OpenGroup();
			});
		}
		else OpenGroup();
		
		function OpenGroup(){
			var groupData = GroupsController.CreateGroup("event","",e.rowData.$id);
			tableViewsController.Add(EventTableView.createView(groupData));
		}
	});
	
	var mainTableView = MainTableView.createView("main",eventsData);
	tableViewsController.Add(mainTableView);
	
	view.add(navBar);
	view.add(tableViewsController);
	
	
	function Open(){
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}
	
	function Close(){
		mainTableView.Close();
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Animator.animate(view,{
			duration: 500,
			top: 0,
			easing:Animator['EXP_OUT']
		});},200);
	}
	
	function DisappearAnimation(){
		Animator.animate(view,{
			duration: 500,
			top: -(globals.platform.actualHeight),
			easing:Animator['EXP_IN']
		},function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};



function CreateTableViewsController (){
	var controller = Ti.UI.createView({
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
	    top: 44
	});
	
	/*
	controller.addEventListener("openCategory", function(e){
		controller.Add(CreateTableView("category",e.rowData));
	});
	*/
	
	controller.addEventListener("back", function(e){
		Back();
	});
	
	var stack = [];
	
	function Add(newTableView){
		stack.push(newTableView);
		controller.add(newTableView);
		if(stack.length >1){
			for (var i = 0; i < stack.length; i++){
				stack[i].MoveLeft();
			}	
		}
	}
	
	function Back (){
		for (var i = 0; i < stack.length; i++){
			stack[i].MoveRight();
		}
		setTimeout(function(){
			controller.remove(stack[stack.length-1]);
			stack.pop();
		}, 1000);
		
	}
	
	controller.Add = Add;
	controller.Back = Back;
	
	
	return controller;
};


function NavBar (){
	var view = Ti.UI.createView({
		backgroundColor: "#CD4E3A",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID: "eventsWinCloseButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 30,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/eventsIconLarge.png",
		width: 28,
		height: 30
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/eventsSign.png",
		width: 83,
		height: 26,
		left: 5
	}));
	
	view.add(title);
	view.add(closeButton);
	
	
	return view;
}
