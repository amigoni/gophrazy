var LocalData = require("model/LocalData");

exports.loadMe = function (phraseData){
	var PhrasesController = require("/controller/PhrasesController");
	
	var hintCost = PhrasesController.GetPhraseHintCost(phraseData.numberOfWords);
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor:"transparent",
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    fullscreen:true,
	    navbarHiddend: true,
	    width:globals.platform.width,
	    height:globals.platform.height-50,
	    top:0,
	    opacity:0
	});
	
	var background = Ti.UI.createView({
		backgroundColor: "black",
		width: win.width,
		height: win.height,
		opacity: 0.7
	});
	
	var hintIndicator = CreateHintIndicator();
	
	var popUpBG = Ti.UI.createView({
		backgroundColor: globals.colors.background,
		borderColor: globals.colors.borderLine2,
		borderWidth: 3,
		width: 250,
		height: Ti.UI.SIZE,
		layout: "vertical",
		bubbleParent: false,
		transform: Ti.UI.create2DMatrix().translate(0,globals.platform.height)
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "Skip Puzzle",
		font: globals.fonts.words3,
		color: globals.colors.warning,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "center",
		top: 10
		//top: groupData.type != "dailyChallenge" ? 4:null
	});
	
	
	var skipButton = CreateSkipButton(hintCost);
	skipButton.addEventListener("singletap", function(){
		var hints =  LocalData.GetGameData().hints;
		if (hints - hintCost >= 0){
			win.fireEvent("skip");
			Close();
		}
		else{
			var BuyHintPopUp = require("/ui/GameWinElements/BuyHintsPopUp");
			var win1 = BuyHintPopUp.loadWin();
			win1.open();
		}
	});
	
	var askFriendsButton = CreateAskFriendsButton();
	askFriendsButton.addEventListener("singletap", function(){
		win.fireEvent("help");
		Close();
	});
	
	var cancelButton = CreateCancelButton();
	cancelButton.addEventListener("singletap", function(e){
		Close();
	});
	
	
	popUpBG.add(titleLabel);
	popUpBG.add(skipButton);
	if(globals.multiplayerEnabled == true) popUpBG.add(askFriendsButton);
	popUpBG.add(cancelButton);
	popUpBG.add(Ti.UI.createView({height:20}));
	
	win.add(background);
	win.add(popUpBG);
	win.add(hintIndicator);
	
	
	function Close(){
		popUpBG.animate(Ti.UI.createAnimation(
			{duration: globals.style.animationSpeeds.speed1, 
			transform: Ti.UI.create2DMatrix().translate(0,globals.platform.height)}
		));
		win.animate(Ti.UI.createAnimation({duration: globals.style.animationSpeeds.speed1,opacity: 0}));
		setTimeout(function(){win.close();},255);
	}
	
	win.addEventListener("close", function(){
		hintIndicator.Close();
		titleLabel = null;
		skipButton = null;
		askFriendsButton = null;
		cancelButton = null;
		background = null;
		popUpBG = null;
		hintIndicator = null;
	});
	
	win.addEventListener("open", function(){
		popUpBG.animate(Ti.UI.createAnimation(
			{duration: globals.style.animationSpeeds.speed1, 
			transform: Ti.UI.create2DMatrix().translate(0,0)}
		));
		win.animate(Ti.UI.createAnimation({duration: globals.style.animationSpeeds.speed1, opacity:1}));
	});
	
	
	win.open();
	
	return win;
};


function CreateHintIndicator (){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		top: 5,
		right: 5
	});
	
	var hintIcon = Ti.UI.createView({
		backgroundImage: globals.colors.theme == "dark" ? "/images/hintButtonDark.png" :"/images/hintButton.png",
		width: 35,
		height: 34,
		top: 0,
		//right: 5,
		transform: Ti.UI.create2DMatrix().scale(0.9,0.9)
	});
	
	var hintCount = Ti.UI.createLabel({
		text: LocalData.GetGameData().hints,
		font: globals.fonts.boldSmall,
		color: globals.colors.background,
		//top: 9
	});
	
	hintIcon.add(hintCount);
	
	view.add(Ti.UI.createLabel({
		text: "Hints",
		font: globals.fonts.boldSmall,
		color: globals.colors.text,
		top: 32,
		//right: 10
	}));
	
	view.add(hintIcon);
	
	function UpdateHintIndicator(e){
		hintCount.text = e.value;
	}
	
	function Close(){
		Ti.App.removeEventListener("updateHintCount", UpdateHintIndicator);
	}
	
	Ti.App.addEventListener("updateHintCount", UpdateHintIndicator);
	
	view.Close = Close;
	
	
	return view;
};


function CreateSkipButton(hintCost){
	var button =  Ti.UI.createView({
		backgroundColor: globals.colors.background,
		borderColor: globals.colors.borderLine2,
		borderWidth: 1,
		width: 190,
		height: 50,
		top: 10,
		bubbleParent: false
	});
	
	button.add(Ti.UI.createLabel({
		text: "Skip",
		font: globals.fonts.words3,
		color: globals.colors.text,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "left",
		top: 5,
		left: 10
		//top: groupData.type != "dailyChallenge" ? 4:null
	}));
	
	button.add(Ti.UI.createLabel({
		text: "Skipping costs you hints.",
		font: globals.fonts.regularSmall,
		color: globals.colors.textMenus,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "left",
		bottom: 8,
		left: 10
		//top: groupData.type != "dailyChallenge" ? 4:null
	}));
	
	var hintIcon = Ti.UI.createView({
		backgroundImage: globals.colors.theme == "dark" ? "/images/hintButtonDark.png" :"/images/hintButton.png",
		width: 35,
		height: 34,
		top: 2,
		right: 5,
		transform: Ti.UI.create2DMatrix().scale(0.9,0.9)
	});
	
	var hintCount = Ti.UI.createLabel({
		text: hintCost,
		font: globals.fonts.boldSmall,
		color: globals.colors.background,
		//top: 9
	});
	
	hintIcon.add(hintCount);
	
	button.add(Ti.UI.createLabel({
		text: "Hints",
		font: globals.fonts.boldSmall,
		color: globals.colors.text,
		bottom: 1,
		right: 8
	}));
	
	button.add(hintIcon);
	
	
	return button;
}


function CreateAskFriendsButton(){
	var button =  Ti.UI.createView({
		backgroundColor: globals.colors.background,
		borderColor: globals.colors.borderLine2,
		borderWidth: 1,
		width: 190,
		height: 50,
		top:10,
		bubbleParent: false
	});
	
	button.add(Ti.UI.createLabel({
		text: "Ask Friends",
		font: globals.fonts.words3,
		color: globals.colors.text,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "left",
		top: 5,
		left: 10
		//top: groupData.type != "dailyChallenge" ? 4:null
	}));
	
	button.add(Ti.UI.createLabel({
		text: "Ask to up to 3 friends.",
		font: globals.fonts.regularSmall,
		color: globals.colors.textMenus,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "left",
		bottom: 8,
		left: 10
		//top: groupData.type != "dailyChallenge" ? 4:null
	}));
	
	var icon = Ti.UI.createView({
		backgroundImage: globals.colors.theme == "dark" ? "/images/questionMarkDark.png" :"/images/questionMark.png",
		width: 28,
		height: 29,
		top: 5,
		right: 9,
		transform: Ti.UI.create2DMatrix().scale(0.9,0.9)
	});
	
	button.add(Ti.UI.createLabel({
		text: "0 left",
		font: globals.fonts.boldSmall,
		color: globals.colors.text,
		bottom: 1,
		right: 7
	}));
	
	button.add(icon);
	
	
	return button;
}


function CreateCancelButton(){
	var button =  Ti.UI.createView({
		backgroundColor: globals.colors.background,
		borderColor: globals.colors.borderLine2,
		borderWidth: 1,
		width: 190,
		height: 50,
		top:10,
		bubbleParent: false
	});
	
	button.add(Ti.UI.createLabel({
		text: "Cancel",
		font: globals.fonts.words3,
		color: globals.colors.text,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "left",
		//top: 5,
		//left: 5
		//top: groupData.type != "dailyChallenge" ? 4:null
	}));
	
	return button;
}
