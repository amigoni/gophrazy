exports.CreateDot = function(dotData,levelData,worldData){
	var LocalData = require("model/LocalData");
	var FriendsController = require("/controller/FriendsController");
	var PhrasesController = require("/controller/PhrasesController");
	var friendsData = [];
	var dotVisible = false;
	var dotInsideColor = worldData.dotFillColor;
	var gameData = globals.gameData;
	
	var view = Ti.UI.createView({
		//backgroundColor:"red",
		width: 40,
		height: 40,
		left: dotData.x-5,
		top: dotData.y-10,
		//visible: dotVisible,
		levelData: levelData
	});
	
	var selectedDot = Ti.UI.createView({
		backgroundColor: "white",
		width: 26,
		height: 26,
		borderRadius: 13,
		opacity: 1
	});
	
	var dot = Ti.UI.createView({
		width: 26,
		height: 26,
		borderRadius: 13,
		backgroundColor: worldData.lineColor,
		//left: dotData.x+2,
		//top: dotData.y,
		levelData: levelData
	});
	
	dot.addEventListener("touchstart", function(){
		dotOverlay.visible = true;
	});
	
	dot.addEventListener("touchend", function(){
		setTimeout(function(){dotOverlay.visible = false;},150);
	});
	
	dot.addEventListener("singletap",function(e){		
		view.fireEvent("dotPressed",{levelData:levelData});
		if (worldData.worldID != "moon" && levelData.type != "riddle") LocalData.SetCurrentGlobalLevelNumber(levelData.levelID);
	});
	
	var dotOverlay = Ti.UI.createView({
		width: dot.width,
		height: dot.height,
		borderRadius: dot.radius,
		backgroundColor: "black",
		opacity: 0.4,
		visible: false
	});
	
	var dotInside = Ti.UI.createView({
		width: 16,
		height: 16,
		borderRadius: 8,
		backgroundColor: dotInsideColor
	});
	
	var riddleLabel = Ti.UI.createLabel({
		text: "R",
		font: globals.fonts.boldSmall,
		color: worldData.lineColor,
		textAlign:"center",
		top: 8
	});
	
	var halfDot = Ti.UI.createView({
		backgroundImage: "/images/halfFilledCircle.png",
		width: 8,
		height: 16,
		left: 5
		//visible: true
	});
	
	var icon = Ti.UI.createView({
		backgroundImage: "/images/crystal1.png",
		width:39,
		height: 51,
		transform: Ti.UI.create2DMatrix().scale(0.75,0.75),
		opacity: 0
	});
	
	
	function Refresh(){
		if (levelData.type == "level") {
			levelData = LocalData.GetGroupByLevelID(levelData.levelID);
		}
		else if (levelData.type == "riddle"){levelData = PhrasesController.GetRiddleGroup(worldData.worldID);}
		
		if (levelData && levelData.locked == false) dotVisible = true;
		view.visible = dotVisible;
		
		if (levelData.complete == true) dotInsideColor = globals.colors.red;
		dotInside.backgroundColor = dotInsideColor;
		
		if (levelData.phrasesCompleteCount > 0 && levelData.complete == false) halfDot.visible = true;
		else halfDot.visible = false;
		
		if (levelData.complete == true){
			if (levelData.levelID == "moon0"){icon.backgroundImage = "/images/crystal1.png"; icon.opacity = 1;}
			else if (levelData.levelID == "moon1"){icon.backgroundImage = "/images/crystal2.png"; icon.opacity = 1;}
			else if (levelData.levelID == "moon2"){icon.backgroundImage = "/images/crystal3.png"; icon.opacity = 1;}
			else if (levelData.levelID == "moon3"){icon.backgroundImage = "/images/crystal4.png"; icon.opacity = 1;}
			else if (levelData.levelID == "moon4"){icon.backgroundImage = "/images/crystal5.png"; icon.opacity = 1;}
			else if (levelData.levelID == "moon5"){icon.backgroundImage = "/images/crystal6.png"; icon.opacity = 1;}
			else if (levelData.levelID == "moon6"){icon.backgroundImage = "/images/crystal7.png"; icon.opacity = 1;}
		}
	}
	
	function Unlock(){
		dot.transform = Ti.UI.create2DMatrix().scale(0,0);
		Refresh();
		dot.animate(Ti.UI.createAnimation({duration: 200, transform: Ti.UI.create2DMatrix().scale(1,1)}));
	}
	
	
	var flashInterval;
	function SelectOn(){
		function Animate(){
			flashInterval = setInterval(
				function(){
					Animator.animate(selectedDot,{
						duration: 750,
						transform: Ti.UI.create2DMatrix().scale(1.5,1.5),
						opacity: 0,
						easing: Animator["EXP_OUT"]
					}, 
					function(){selectedDot.transform = Ti.UI.create2DMatrix().scale(1.0,1.0);selectedDot.opacity = 1;}
				);},
			900);
			
			/*
			selectedDot.animate(Ti.UI.createAnimation({
				duration: 750,
				transform: Ti.UI.create2DMatrix().scale(1.5,1.5),
				opacity: 0,
				repeat:9999,
				delay:150
			}));
			*/
		}
		
		setTimeout(function(){Animate();},250);
	}
	
	function Unselect(){
		clearInterval(flashInterval);
		//selectedDot.animate({opacity:0,duration:1});
		selectedDot.opacity = 0;
	}
	
	
	dot.add(dotInside);
	dot.add(halfDot);
	if(levelData.type == "riddle") dot.add(riddleLabel);
	dot.add(dotOverlay);
	
	view.add(selectedDot);
	view.add(dot);
	view.add(icon);
	
	view.Refresh = Refresh;
	view.Unlock = Unlock;
	view.SelectOn = SelectOn;
	view.Unselect = Unselect;
	
	
	Refresh();
	if (levelData.levelID == gameData.currentGlobalLevelNumber) SelectOn();
	
	
	
	
	return view; 
};