exports.Create = function(itemData){
	//var itemData = { title: "The Power of Love", description:"Mark your favoirte phrases and search all teh ones that you have already solved."};
	var view = Ti.UI.createView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height: globals.platform.height - globals.adsBannerSpace,
		//transform: Ti.UI.create2DMatrix().scale(0,0),
		bubbleParent: false
	});
	
	var backgroundBlack = Ti.UI.createView({
		backgroundColor: "black",
		width: globals.platform.width,
		height: globals.platform.height - globals.adsBannerSpace,
		opacity: 0,
		bubbleParent: false
	});
	
	var popUpContainer = Ti.UI.createView({
		width: view.width,
		height: view.height,
		transform: Ti.UI.create2DMatrix().scale(0,0)
	});
	
	var popUpBg = Ti.UI.createView({
		backgroundColor: "#FFF7E5",
		borderRadius: 30,
		borderWidth: 3,
		borderColor: "#FFFFFF",
		width: globals.platform.width-50,
		height: globals.platform.height - globals.adsBannerSpace-50,
		bubbleParent: false
	});
	
	popUpBg.add(Ti.UI.createView({
		backgroundImage: "/images/gradientOverlay.png",
		width: popUpBg.width,
		height: popUpBg.height
	}));
	
	
	var container = Ti.UI.createView({
		width: popUpBg.width,
		height: popUpBg.height,
		layout: "vertical",
	});
	
	var unlockedLabel = Ti.UI.createImageView({
		image:"/images/unlockedLabel.png",
		top: 20
	});

	var icon = Ti.UI.createImageView({
		image: "/images/"+itemData.icon+".png",
		top: 30
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: itemData.title,
		font: globals.fonts.boldMedium31,
		color: globals.colors.textOpposite,
		width: globals.platform.width-80,
		textAlign:"center",
		minimumFontSize: 20,
		top: 15
	});
	
	var descriptionLabel = Ti.UI.createLabel({
		text: itemData.description,
		font: globals.fonts.words2,
		color: globals.colors.textOpposite,
		width: globals.platform.width-80,
		textAlign:"center",
		top: -5
	});
	
	
	var okButton = Ti.UI.createImageView({
		image:"/images/closeButtonCartoon.png",
		bottom: 20,
		//right: 20
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		view.fireEvent("close");
	});
	
	
	container.add(unlockedLabel);
	container.add(icon);
	container.add(titleLabel);
	container.add(descriptionLabel);
	
	popUpBg.add(container);
	popUpBg.add(okButton);
	popUpContainer.add(popUpBg);
	
	view.add(backgroundBlack);
	view.add(popUpContainer);
	
	
	
	function Open(){
		backgroundBlack.animate(Ti.UI.createAnimation({opacity:0.7, duration:500}));
		popUpContainer.animate(Ti.UI.createAnimation({
			transform: Ti.UI.create2DMatrix().scale(1,1),
			duration: globals.style.animationSpeeds.speed1
		}));
	}
	
	function Close(){
		backgroundBlack.animate(Ti.UI.createAnimation({opacity:0, duration: 500}));
		popUpContainer.animate(Ti.UI.createAnimation({
			transform: Ti.UI.create2DMatrix().scale(0,0),
			duration: globals.style.animationSpeeds.speed1
		}));
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
}; 