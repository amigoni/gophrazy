exports.CreateWorldMap = function(worldID,isDown){
	var Common = require("ui/Common");
	var LocalData = require("model/LocalData");
	var WorldsController = require("/controller/WorldsController");
	var LevelsController = require("controller/LevelsController");
	var PhrasesController = require("controller/PhrasesController");
	var FriendsController = require("/controller/FriendsController");
	var BrowsePhrasesWin = require("/ui/BrowsePhrasesWin");
	var Dot = require("/ui/MapWinElements/Dot");
	var Thumb = require("/ui/MapWinElements/Thumb");
	var Portal = require("/ui/MapWinElements/Portal");
	var worldData = LocalData.GetWorldData(worldID);
	var data = LocalData.GetLevelsForWorld(worldID);
	var gameData = LocalData.GetGameData();
	
	var view = Ti.UI.createView({
		backgroundColor: worldData.bgColor,
		width: 320,
		height: globals.platform.height - globals.adsBannerSpace,
		transform: isDown == false ? Ti.UI.create2DMatrix().translate(0,-globals.platform.height) : null
	});
	
	var navBar = Ti.UI.createView({
		width: globals.platform.width,
		height: 50,
		top: 0
	});
	
	var worldNameLabel = Ti.UI.createLabel({
		text: worldData.worldName,
		font: globals.fonts.boldMedium31,
		color: worldData.fontColor,
		shadowOffset:{x:1,y:1},
		shadowColor: "#222222",
		width: globals.platform.width-100,
		minimumFontSize: 30,
		textAlign: "center",
		top: 8
	});
	
	var closeButton = Ti.UI.createView({
		backgroundImage: "/images/map"+capitaliseFirstLetter(worldID)+"CloseButton.png",
		width: 45,
		height: 35,
		right: 5,
		top: 5
	});
	
	Common.MakeButton(closeButton);
	
	closeButton.addEventListener("singletap", function(e){
		view.fireEvent("closePressed", {worldID:worldID});
	});
	
	var mapButton = Ti.UI.createView({
		backgroundImage: "/images/map"+capitaliseFirstLetter(worldID)+"MapButton.png",
		width: 45,
		height: 35,
		left: 5,
		top: 5
	});
	
	Common.MakeButton(mapButton);
	
	mapButton.addEventListener("singletap", function(){
		view.fireEvent("mapPressed");
	});
	
	navBar.add(worldNameLabel);
	navBar.add(closeButton);
	if (worldID != "moon") navBar.add(mapButton);
	
	var container = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.height - globals.adsBannerSpace - 50,
		top: 50
	});
	
	var mapContainer = Ti.UI.createView({
		width: 320,
		height: 370,
	});
	
	var mapView = Ti.UI.createView({
		backgroundImage: "/images/map"+capitaliseFirstLetter(worldID)+".png",
		width: 320,
		height: 370,
		top: 0
	});
	
	mapContainer.add(mapView);
	
	var dots = [];
	for (var i= 0; i < worldData.dotsData.length; i++){
		var dotData = worldData.dotsData[i];
		var levelData = data[i];
		var friendsData = FriendsController.GetFriendsForLevel(levelData.levelID);
		var dot = Dot.CreateDot(dotData,levelData,worldData);
		dot.addEventListener("dotPressed", function(e){
			levelData = e.levelData;
			for(var i = 0; i < dots.length; i++){
				if(dot[i] != this)dots[i].Unselect();
			}
			this.SelectOn();
			DotPressed(this,levelData);
		});
		
		dot.addEventListener("gameWinClosed", function(e){
			if (levelData.sceneEnd != "none") OpenDialog(levelData.sceneEnd, false);
			else CheckToUnlockSomething();//UnlockNextLevel();
		});
		
		var thumb;
		if (friendsData.length > 0) thumb = Thumb.CreateThumb(levelData,dotData);
		 
		dots.push(dot);
		mapContainer.add(dot);
		if(friendsData.length >0) mapContainer.add(thumb);
	}
	
	if(worldID != "moon"){
		var riddleData = PhrasesController.GetRiddleGroup(worldID);
	
		var riddleDot = Dot.CreateDot({x: worldData.riddleDotPosition.x, y:worldData.riddleDotPosition.y} , riddleData, worldData);
		riddleDot.addEventListener("dotPressed", function(e){
			levelData = e.levelData;
			DotPressed(this,levelData);
		});
		
		mapContainer.add(riddleDot);
	}
	
	
	var portal = Portal.CreatePortal(worldData);
	portal.addEventListener("singletap", function (){
		if(worldData.portalLocked == false){
			var levelToUnlock;	
			if(worldData.worldID == "meadows") levelToUnlock = LevelsController.GetLevelForWorld("moon",0);
			else if(worldData.worldID == "sea") levelToUnlock = LevelsController.GetLevelForWorld("moon",1);
			else if(worldData.worldID == "volcano") levelToUnlock = LevelsController.GetLevelForWorld("moon",2);
			else if(worldData.worldID == "desert") levelToUnlock = LevelsController.GetLevelForWorld("moon",3);
			else if(worldData.worldID == "storm") levelToUnlock = LevelsController.GetLevelForWorld("moon",4);
			else if(worldData.worldID == "cave") levelToUnlock = LevelsController.GetLevelForWorld("moon",5);
			else if(worldData.worldID == "space") levelToUnlock = LevelsController.GetLevelForWorld("moon",6);
			if (levelToUnlock && levelToUnlock.locked == true) LevelsController.InitLevel(levelToUnlock.levelNumberInWorld,levelToUnlock.worldID);
			
			view.fireEvent("portalPressed");
		}
	});
	
	mapContainer.add(portal);
	
	container.add(mapContainer);
	
	view.add(navBar);
	view.add(container);
	
	
	
	/////Functions
	function DotPressed(dot,levelData){
		if(levelData.locked == false && levelData.complete == false){
			globals.currentLevelID = levelData.levelID;
			
			if(levelData.type == 'level') PhrasesController.CreateShuffleBagOfPhrases(levelData,true);
			
			//PhrasesController.FakeCompletePhrase(levelData);
			//if (levelData.sceneEnd != "none") OpenDialog(levelData.sceneEnd, false);
			//else CheckToUnlockSomething();//UnlockNextLevel();
			
			if(levelData.sceneStart != "none"){ OpenDialog(levelData.sceneStart,true);}
			else{OpenLevel();}			
		}
		else if (levelData.locked == true){
			alert("Level Locked.\nPlay the previous Level to unlock it.");
		}
		else if (levelData.complete == true){
			//alert("Level Complete.");
			var win1 = BrowsePhrasesWin.loadWin(levelData);
			win1.open();
		}
	}
	
	function OpenDialog(scene,isStart){
		var DialogScene = require("/ui/Scenes/DialogScene");
		var dialogScene = DialogScene.loadMe(scene);
		dialogScene.bubbleParent = false;
		dialogScene.addEventListener("close", function(){
			dialogScene.DisappearAnimation(function(){view.remove(dialogScene);});
			if (isStart) OpenLevel();
			else if (isStart == false) CheckToUnlockSomething();
		});
		view.add(dialogScene);
		dialogScene.Open();
	}
	
	function OpenLevel(){
		Common.CreateButtonBlocker(false);
		var GameWin = require("ui/GameWin");
		var win1 = GameWin.loadWin(levelData, false);
		win1.addEventListener("closed", function(){
			this.fireEvent("gameWinClosed");
			if (levelData.sceneEnd != "none") OpenDialog(levelData.sceneEnd, false);
		});
		
		win1.addEventListener("open", function(){
			Ti.App.fireEvent("closeButtonBlocker");
		});
		win1.open();
	}
	
	function AppearAnimation(){
		Animator.animate(view,{
			duration: 500,
			transform:Ti.UI.create2DMatrix().translate(0,0),
			easing:Animator['EXP_OUT']
		});
	}
	
	function DisappearAnimation(){
		Animator.animate(view,{
			duration: 500,
			transform:Ti.UI.create2DMatrix().translate(1,1),
			easing:Animator['EXP_IN']
		});
	}
	
	function Refresh(e){
		if (e.worldID == worldID){
			for	(var i = 0; i < dots.length; i++){
				if (dots[i].levelData.levelID == e.levelID){
					dots[i].Refresh();
					break;		
				} 
			}
			
			if(riddleDot) riddleDot.Refresh();
			worldData = LocalData.GetWorldData(worldID);
			if(worldData.portalEnabled == true && worldData.portalLocked == true) EnablePortal();
		}		
	}
	
	//This one is used for world style progression. All Levels unlocked in a world.
	function CheckToUnlockSomething (){
		if (worldID != "moon"){
			var unlockWorld = WorldsController.CheckToUnlockNextSomething(worldID);
			if(unlockWorld == true){
				riddleDot.Unlock();
				setTimeout(function(){WorldsController.CheckToUnlockNextWorld();},300);
			}
		}
	}
	
	
	//This one is used for linear progression. 
	/*
	function UnlockNextLevel(){
		var gameData = LocalData.GetGameData();
		
		if (worldID != "moon"){
			var unlockLevel = WorldsController.CheckToUnlockNextLevelInWorld(worldID);
			if(unlockLevel.unlock == true){
				if (unlockLevel.levelNumberInWorld == "riddle"){
					riddleDot.Unlock();
					setTimeout(function(){WorldsController.CheckToUnlockNextWorld();},500);
				}
				else{
					for (var i = 0; i < dots.length; i++){
						if (dots[i].levelData.levelNumberInWorld == unlockLevel.levelNumberInWorld){
							dots[i].Unlock();
							LocalData.CheckForNewAbilities(dots[i].levelData);
							break;
						}
					}	
				}
			}
		}
	}
	*/
	
	function Close(){
		Ti.API.debug("Cleaned World Map");
		Ti.App.removeEventListener("refreshMap", Refresh);
		//Ti.App.removeEventListener("backToMap", UnlockNextLevel);
	}
	
	function EnablePortal(){
		var portalProgressIndicator = CreatePortalProgressIndicator();
		portalProgressIndicator.top = portal.top;
		portalProgressIndicator.left = portal.left+10;
		container.add(portalProgressIndicator);
		
		if(worldID == "meadows"){
			var touchSensor = Ti.UI.createView({
				width: 50, 
				height: 50,
				top: portal.top-10, 
				left: portal.left-10,
				bubbleParent: false
			});
			
			var touchCount = 0;
			var portalTimer;
			
			touchSensor.addEventListener("touchend", function (){
				if(touchCount == 0 ){
					//portal.Flash();
					portalTimer = setTimeout(function(){
						touchCount = 0;
						portalProgressIndicator.FillBar(touchCount/3);
						
					},2000);
				}
				touchCount++;
				
				portalProgressIndicator.FillBar(touchCount/3);
				
				if(touchCount == 3) {
					clearTimeout(portalTimer);
					portal.Unlock();
					container.remove(touchSensor);
					container.remove(portalProgressIndicator);
				}	
			});
			
			container.add(touchSensor);
		}
		else if(worldID == "sea"){
			var changeCount = 0;
			
			function SeaPortalRiddle(e){
			    changeCount++;
			    portalProgressIndicator.FillBar(changeCount/4);
			    if (changeCount == 4){
			    	portal.Unlock();
			    	container.remove(portalProgressIndicator);
			    	Ti.Gesture.removeEventListener('orientationchange',SeaPortalRiddle);
			    }
			}
			
			Ti.Gesture.addEventListener('orientationchange',SeaPortalRiddle);
		}
		else if(worldID == "volcano"){
			Titanium.Media.audioSessionMode = Ti.Media.AUDIO_SESSION_MODE_PLAY_AND_RECORD;
			var recorder = Ti.Media.createAudioRecorder();
			recorder.start();
			Ti.Media.startMicrophoneMonitor();
			var microphoneInterval = setInterval(function(){
				//Ti.API.info(Ti.Media.averageMicrophonePower);
				portalProgressIndicator.FillBar(Ti.Media.averageMicrophonePower/0.5);
				if(Ti.Media.averageMicrophonePower > 0.5){
					recorder.stop();
					portal.Unlock();
					container.remove(portalProgressIndicator);
					Ti.Media.stopMicrophoneMonitor();
					clearInterval(microphoneInterval);
				}
			},100);
		}	
		else if(worldID == "desert"){
			portalProgressIndicator.top = portal.top-50;
			portalProgressIndicator.left = portal.left+40;
			
			var clock = Ti.UI.createView({
				//backgroundColor: "red",
				width: 70,
				height: 70,
				top: 250,
				left: 65,
				visible: false
			});
			
			var hourHand = Ti.UI.createView({
				backgroundColor: "#5A554A",
				width: 3,
				height: 20,
				anchorPoint:{x:0.5, y:1},
				top: 15,
				left: 35
			});
			
			var minuteHand = Ti.UI.createView({
				backgroundColor: "#5A554A",
				width: 3,
				height: 35,
				anchorPoint:{x:0.5, y:1},
				top: 0,
				left: 35
			});
			
			var secondsHand = Ti.UI.createView({
				backgroundColor: "#5A554A",
				width: 1,
				height: 35,
				anchorPoint:{x:0.5, y:1},
				top: 0,
				left: 35
			});
			
			clock.add(hourHand);
			clock.add(minuteHand);
			clock.add(secondsHand);
			
			var clockInterval = setInterval(function(){
				var hours = new Date().getHours();
				var minutes = new Date().getMinutes();
				var seconds = new Date().getSeconds();
				
				var hoursAngle = 360/12*hours;
				var minutesAngle = 360/60*minutes;
				var secondsAngle = 360/60*seconds;
				
				hourHand.transform = Ti.UI.create2DMatrix().rotate(hoursAngle);
				minuteHand.transform = Ti.UI.create2DMatrix().rotate(minutesAngle);
				secondsHand.transform = Ti.UI.create2DMatrix().rotate(secondsAngle);
				
				var minuteValue = minutes+30; 
				if (minuteValue > 60) minuteValue -= 60;
				portalProgressIndicator.FillBar(minuteValue/60);
				
				if (minutes == 30){
					clearInterval(clockInterval);
					portal.Unlock();
					container.remove(portalProgressIndicator);
					container.remove(clock);
				}
				clock.visible = true;
			},1000);
		
			container.add(clock);
		}
		else if (worldID == "storm"){
			var overlay = Ti.UI.createView({
				width: view.width,
				height: view.height,
				top: 0,
				opacity: 0
			});
			
			var bg = Ti.UI.createView({
				backgroundColor:"black",
				width: view.width,
				height: view.height,
				top: 0,
				opacity: 0.7
			});
			
			var portal2 = Ti.UI.createImageView({
				image:"/images/portal.png",
			});
			
			portal2.addEventListener("singletap", function(){
				Titanium.Geolocation.removeEventListener('orientationchange',DisplayOverlay);
				portal.Unlock();
				container.remove(portalProgressIndicator);
				clearInterval(portal2Interval);
				view.remove(overlay);
			});
			
			overlay.add(bg);
			overlay.add(portal2);
			
			view.add(overlay);
			
			var animationLength = 10000;
			var portal2Interval;
			setTimeout(function(){Animator.animate(portal2, {
			        rotate : 360,
			        duration : animationLength-1,
			    });
			    
			    portal2Interval = setInterval(function(){Animator.animate(portal2,{
			        rotate : 360,
			        duration : animationLength-1,
			    });},animationLength);}
			,500);
			
			function DisplayOverlay(e){
			    if (e.orientation == 1) overlay.animate(Ti.UI.createAnimation({duration:250, opacity:1}));	
			    else overlay.animate(Ti.UI.createAnimation({duration:250, opacity:0}));
			}
			
			Ti.Gesture.addEventListener('orientationchange',DisplayOverlay);
		}
		else if (worldID == "cave"){
			var gestureSensor = Ti.UI.createView({
				width: container.width,
				height: container.height,
			});
			
			var sequence = ["right","left","up","down"];
			var currentSwipe = 0;
			gestureSensor.addEventListener("swipe",function(e){
				if (e.direction == sequence[currentSwipe]) currentSwipe++;
				else currentSwipe = 0;
				
				if(currentSwipe >= sequence.length){
					portal.Unlock();
					container.remove(portalProgressIndicator);
					container.remove(gestureSensor);
				}
				else portalProgressIndicator.FillBar(currentSwipe/sequence.length); 
			});
			
			container.add(gestureSensor);
		}
		else if (worldID == "space"){
			var counter = 0;
			var limit = 60*3;
			var compass = Ti.UI.createView({
				//backgroundColor: "red",
				width: 70,
				height: 70,
				top: portalProgressIndicator.top,
				left: portalProgressIndicator.left
			});
			
			var hand = Ti.UI.createView({
				backgroundColor: "white",
				width: 3,
				height: 20,
				anchorPoint:{x:0.5, y:1},
				top: 15,
				left: 35
			});
			
			compass.add(hand);
			container.add(compass);
			
			function UpdateHeading(e){
				if (e.success === undefined || e.success) {
					var value = e.heading.magneticHeading;
					//label.text = value;
					hand.transform = Ti.UI.create2DMatrix().rotate(value);
					if (value < 15 || value > 345){
						counter++;
						if (counter > limit){
							Titanium.Geolocation.removeEventListener('heading',UpdateHeading);
							portal.Unlock();
							container.remove(portalProgressIndicator);
							container.remove(compass);
						}
					}
					else counter = 0;
					
					portalProgressIndicator.FillBar(counter/limit);
				}
			}
			
			Titanium.Geolocation.addEventListener('heading', UpdateHeading);
		}
	}
	/////end Functions
	
	view.AppearAnimation = AppearAnimation;
	view.DisappearAnimation = DisappearAnimation;
	view.Close = Close;
	view.mapView = mapView;
	
	Ti.App.addEventListener("refreshMap", Refresh);
	//Ti.App.addEventListener("backToMap", UnlockNextLevel);
	
	if(worldData.portalEnabled == true && worldData.portalLocked == true) EnablePortal();
	//UnlockNextLevel();
	//WorldsController.CheckToUnlockNextWorld();
	CheckToUnlockSomething();
	
	
	return view;
};


function CreatePortalProgressIndicator(){
	var view = Ti.UI.createView({
		height: 30,
		width: 10
	});
	
	var bg = Ti.UI.createView({
		width: 10,
		height: 30,
		borderColor: "#F4E2B9",
		borderWidth: 2,
		borderRadius: 5
	});
	
	var fill = Ti.UI.createView({
		backgroundColor:"#F4E2B9",
		width: 10,
		height: 2,
		borderRadius: 5,
		bottom: 0
	});
	
	bg.add(fill);
	view.add(bg);
	
	
	function FillBar(percentage){
		var height = bg.height*percentage;
		if (height < 2) height = 2;
		fill.height = height;
	}
	
	view.FillBar = FillBar;
	
	
	return view;
}

function capitaliseFirstLetter(string){
	return string.charAt(0).toUpperCase() + string.slice(1);
};	