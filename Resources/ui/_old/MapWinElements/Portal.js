exports.CreatePortal = function (worldData){
	var WorldsController = require("/controller/WorldsController");
	
	var view = Ti.UI.createView({
		//backgroundColor:"red",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		left: worldData.portalPosition.x,
		top: worldData.portalPosition.y,
		touchEnabled: worldData.portalEnabled
	});
	
	var image = Ti.UI.createImageView({
		image:"/images/portal.png",
		anchorPoint : {
	        x : 0.45,
	        y : 0.45
	    },
		visible: !worldData.portalLocked
	});
	
	view.add(image);
	
	var animationLength = 10000;
	setTimeout(function(){Animator.animate(view, {
	        rotate : 360,
	        duration : animationLength-1,
	    });
	    
	    setInterval(function(){Animator.animate(view,{
	        rotate : 360,
	        duration : animationLength-1,
	    });},animationLength);}
	,500);
	
	
	function Enable (){
		worldData = WorldsController.GetWorldData(worldData.worldID);
		if (worldData.portalEnabled == true) view.touchEnabled = true;
	}
	
	function Unlock(){
		WorldsController.UnlockPortal(worldData.worldID);
		worldData = WorldsController.GetWorldData(worldData.worldID);
		if (worldData.portalLocked == false) image.visible = true;
		if (worldData.portalEnabled == true) view.touchEnabled = true;
	}
	
	function Flash(){
		image.visible = true;
		setTimeout(function(){image.visible = false;},50);
	}
	
	
	view.Enable = Enable;
	view.Unlock = Unlock;
	view.Flash = Flash;
	
	
	return view;
};
