exports.CreateThumb = function(levelData,dotData,friendsData){
	var FriendsController = require("/controller/FriendsController");
	var thumbnailImage = "";
	
	var view = Ti.UI.createView({
		backgroundImage:"/images/mapFacebookThumbnailBGSingle.png",
		width: 31,
		height: 31
	});
	
	var thumbnail = Ti.UI.createImageView({
		image: thumbnailImage,
		top:2,
		left:2,
		width: 25,
		height: 25
	});
	
	var thumbnailLabel =  Ti.UI.createLabel({
		text: "",
		//borderColor: "#C6B895",
		font: globals.fonts.boldSmall,
		color: "#C6B895",
		top:2,
		left:2,
		width: 25,
		height:25,
		textAlign:"center",
	});
	
	view.add(thumbnailLabel);
	view.add(thumbnail);
	
	if (levelData.thumbPosition == "up") {view.top = dotData.y-33; view.left = dotData.x;}
	else if (levelData.thumbPosition == "down") {view.top = dotData.y+25; view.left = dotData.x;}
	else if (levelData.thumbPosition == "left")  {view.top = dotData.y-4; view.left = dotData.x-28;}
	else if (levelData.thumbPosition == "right")  {view.top = dotData.y-4; view.left = dotData.x+29;}
	
	function Refresh(){
		friendsData = FriendsController.GetFriendsForLevel(levelData.levelID);
		if (friendsData[0]){
			if(friendsData[0].thumbnailURL) thumbnail.image = friendsData[0].thumbnailURL;
			else{
				thumbnailLabel.text = friendsData[0].firstName[0]+friendsData[0].lastName[0];
			}
			
			if (friendsData.length > 1){
				view.backgroundImage = "/images/mapFacebookThumbnailBGMultiple.png";
				view.width = 32;
				view.height = 32;
				thumbnail.top = 3;
				thumbnail.left = 2;
			}
			
			view.visible = true;
		}
		else{
			view.visible = false;
		}
	}
	
	view.Refresh = Refresh;
	
	Refresh();
	
	return view;
};
