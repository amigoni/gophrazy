exports.CreateBigMap = function(closeImmediately){
	var worldsData = globals.col.worlds.getAll();
	
	var view = Ti.UI.createView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height: globals.platform.height - globals.adsBannerSpace,
		//transform: Ti.UI.create2DMatrix().scale(0,0),
		bubbleParent: false
	});
	
	var backgroundBlack = Ti.UI.createView({
		backgroundColor: "black",
		width: globals.platform.width,
		height: globals.platform.height - globals.adsBannerSpace,
		opacity: 0,
		bubbleParent: false
	});
	
	var popUpContainer = Ti.UI.createView({
		//backgroundColor: "red",
		width: view.width,
		height: 410,
		top:globals.platform.height
	});
	
	var popUpBg = Ti.UI.createView({
		backgroundImage:"/images/bigMapBg.png",
		width: 289,
		height: 389,
		top: 15,
		bubbleParent: false
	});
	
	var mapTitle = Ti.UI.createImageView({
		image:"/images/mapTitle.png",
		top: 0
	});
	
	var closeButton = Ti.UI.createImageView({
		image:"/images/closeButtonCartoon.png",
		top: 5,
		right: 10
	});
	
	Common.MakeButton(closeButton);
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("close");
	});
	
	var container = Ti.UI.createView({
		width: 270,
		height: 370
		//layout: "vertical",
	});

	var pathsData = [
		{x:82,y:315},
		{x:210,y:197},
		{x:134,y:218},
		{x: 17,y:166},
		{x: 30,y: 58},
		{x: 160,y: 30}
	];
	
	var paths = [];
	
	for (var i = 0; i < worldsData.length; i++){
		var worldData = worldsData[i];
		//if (worldData.locked == false)
		{ 
			var button = CreateWorldButton(worldData);
			container.add(button);
			if (i > 0 && i < 7){
				var path = Ti.UI.createImageView({image:"/images/bigMapPath"+(i-1)+".png",left: pathsData[i-1].x, top:pathsData[i-1].y});
				container.add(path);
				paths.push(path);
			} 
		}
	}
	
	popUpBg.add(container);
	
	popUpContainer.add(popUpBg);
	popUpContainer.add(mapTitle);
	popUpContainer.add(closeButton);
	
	view.add(backgroundBlack);
	view.add(popUpContainer);
	
	
	function CreateWorldButton(worldData){
		var isNew = false;
		if(worldData.levelCompleteCount == 0) isNew = true;
		var button = Ti.UI.createView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			layout: "vertical",
			left:  worldData.mapPosition.x,
			top: worldData.mapPosition.y,
			transform: isNew == true ? Ti.UI.create2DMatrix().scale(0,0) : null,
			touchEnabled: !closeImmediately
		});
		
		Common.MakeButton(button);
		
		button.addEventListener("singletap",function(){
			view.fireEvent("worldSelected", {worldID:worldData.worldID});
		});
		
		var icon = Ti.UI.createImageView({
			image: "/images/mapIcon"+capitaliseFirstLetter(worldData.worldID)+".png",
			bottom:5
		});
		
		var nameLabel = Ti.UI.createLabel({
			text: worldData.worldName,
			font: globals.fonts.boldMediumSmall,
			color: "#453D32",
			minimumFontSize: 30,
			textAlign: "center",
			bottom: 5
		});
		
		var friendsLabel = Ti.UI.createLabel({
			text: worldData.worldName,
			font: globals.fonts.boldMediumSmall,
			color: worldData.mapFontColor,
			minimumFontSize: 30,
			textAlign: "center",
			bottom: 5
		});
		
		function NewAnimation(){
			setTimeout(function(){button.animate(Ti.UI.createAnimation({
				transform: Ti.UI.create2DMatrix().scale(1,1),
				duration: globals.style.animationSpeeds.speed1
			}));},500);
		}
		
		
		button.add(icon);
		button.add(nameLabel);
		//button.add(friendsLabel);
		
		if(isNew == true)NewAnimation();

		return button;
	}
	
	function Open(){
		Animator.animate(backgroundBlack, {opacity: 0.80, duration:500});
		Animator.animate(popUpContainer, {top: (view.height-410)/2, duration: 500, easing: Animator["EXP_OUT"]});
		
		if(closeImmediately == true) setTimeout(function(){view.fireEvent("close");},2000);
	}
	
	function Close(){
		Animator.animate(backgroundBlack,{opacity:0, duration:500});
		Animator.animate(popUpContainer,{ top: globals.platform.height, duration: 500,easing: Animator["EXP_IN"]});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};

function capitaliseFirstLetter(string){
	return string.charAt(0).toUpperCase() + string.slice(1);
};