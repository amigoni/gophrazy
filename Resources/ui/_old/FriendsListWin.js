exports.loadWin = function(){
	var Common = require('ui/Common');
	var LocalData = require("model/LocalData");
	var ServerData = require("model/ServerData");
	
	var facebookFriendsData = [];
	var friendsData = ServerData.GetFacebookFriends(UpdateTableData,function(e){alert("Oh no!!! There was an error!");});
	var tableData = [];
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor: globals.colors.background,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:0,
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 0
	});
	
	var navBar = Ti.UI.createView({
		height: 44,
		top: 0	
	});
	
	
	var titleLabel = Ti.UI.createLabel({
		//text: groupData.name+" "+groupData.completeCount+"/"+groupData.count,
		//text: groupData.title+" - "+groupData.completeCount,
		text: "Friends",
		font: globals.fonts.words3,
		color: globals.colors.text,
		width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "center"
	});
	
	
	var backButton = Common.CreateBackButton();
	
	backButton.addEventListener("singletap", function(){
		Ti.App.fireEvent("friendSelected",{data:"cancel"});
		win.animate(Common.disappearAnimation());
		setTimeout(function(){win.close();},550);
	});
	
	navBar.add(titleLabel);
	navBar.add(backButton);
	
	var tableView = Ti.UI.createTableView({
		width: globals.platform.width,
		height:Ti.Platform.displayCaps.platformHeigth-globals.adsBannerSpace-44,
		backgroundColor: "transparent",
		//separatorStyle: false,
		top: 44
	});
	
	
	tableView.footerView = Ti.UI.createView({height: 1});
	
	win.add(navBar);
	win.add(tableView);	
	
	
	win.addEventListener("open", function(){
		var Common = require('ui/Common');
		win.animate(Common.appearAnimation());
	});
	
	
	function UpdateTableData (data){
		Ti.API.info("Facebook friends: "+data.length);
		facebookFriendsData = data;
		function sortAZ(ob1,ob2) {
	    	var n1 = ob1.name;
	    	var n2 = ob2.name;
	    	if (n1 > n2) {return 1;}
			else if (n1 < n2){return -1;}
		    else { return 0;}//nothing to split
		};
		
		facebookFriendsData = facebookFriendsData.sort(sortAZ);
		
		tableData = [];
		for (var i=0; i < facebookFriendsData.length; i++){
			facebookFriendsData[i].userID = facebookFriendsData[i].id;
			if (facebookFriendsData[i].installed) tableData.push(CreateRow(facebookFriendsData[i],SelectRowCallback));
		}
		tableView.data = tableData;
	}
	
	function SelectRowCallback(data){
		Ti.App.fireEvent("friendSelected",{data:data});
		win.animate(Common.disappearAnimation());
		setTimeout(function(){win.close();},550);
	}
	
	
	return win;
};	


function CreateRow(data,selectCallRowCallback){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 45,
		backgroundColor: "transparent",
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.GRAY,
		data: data
	});
	
	row.addEventListener("singletap",function(e){
		selectCallRowCallback(data);
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: data.name,
		color: globals.colors.text,
		//width: globals.platform.width-20,
		font: globals.fonts.words2,
		left: 10,
		top: 5
	});
	
	var subtitleLabel = Ti.UI.createLabel({
		text: data.installed ? "Using Word Hop": "Invite to Word Hop",
		color:  data.installed ? globals.colors.warning : globals.colors.textMenus,
		//width: globals.platform.width-20,
		font: globals.fonts.regular2,
		left: 10,
		bottom: 5
	});
	
	
	row.add(titleLabel);
	row.add(subtitleLabel);
	
	return row;
};
