exports.loadWin = function(){
	var Common = require('ui/Common');
	var LocalData = require("model/LocalData");
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor: globals.colors.background,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:0,
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 0
	});
	
	var closeButton = Common.CreateCloseButton();
	closeButton.top = 10;
	
	closeButton.addEventListener("singletap", function(){
		win.animate(Common.disappearAnimation());
		setTimeout(function(){win.close();},550);
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: globals.platform.width,
		height:  globals.platform.actualHeight
	});
	
	var centerContainer = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var emailField = Ti.UI.createTextField({
		backgroundColor: "white",
		width: globals.platform.width-60,
		height: 40,
		paddingLeft: 10,
		font: globals.fonts.words1,
		hintText: "enter your email",
		keyboardType: Ti.UI.KEYBOARD_EMAIL,
		autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, 
		autocorrect: false,
		top: 0
	});
	
	emailField.addEventListener("change", function(){
		if (validateEmail(emailField.value) == true){
			emailField.color = "black";
		}
		else{
			emailField.color = "red";
		}
		CheckSubmitButton();
	});
	
	var usernameField = Ti.UI.createTextField({
		backgroundColor: "white",
		width: globals.platform.width-60,
		height: 40,
		paddingLeft: 10,
		font: globals.fonts.words1,
		hintText: "choose a username",
		autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, 
		autocorrect: false,
		top: 20
	});
	
	usernameField.addEventListener("change", function(){
		if (usernameField.value.length >= 5){
			usernameField.color = "black";
		}
		else{
			usernameField.color = "red";
		}
		CheckSubmitButton();
	});
	
	var passwordField = Ti.UI.createTextField({
		backgroundColor: "white",
		width: globals.platform.width-60,
		height: 40,
		paddingLeft: 10,
		font: globals.fonts.words1,
		hintText: "choose a password",
		color: "red",
		autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, 
		autocorrect: false,
		top: 20
	});
	
	passwordField.addEventListener("change", function(){
		//Ti.API.info(passwordField.value.length);
		if (passwordField.value.length >= 8){
			passwordField.color = "black";
		}
		else{
			passwordField.color = "red";
		}
		//Ti.API.info(passwordField.value.length);
		CheckSubmitButton();
	});
	
	var confirmPasswordField = Ti.UI.createTextField({
		backgroundColor: "white",
		width: globals.platform.width-60,
		height: 40,
		paddingLeft: 10,
		font: globals.fonts.words1,
		hintText: "confirm password",
		autocapitalization: Ti.UI.TEXT_AUTOCAPITALIZATION_NONE, 
		autocorrect: false,
		top: 20
	});
	
	confirmPasswordField.addEventListener("change", function(){
		if (confirmPasswordField.value.length >= 8 && confirmPasswordField.value == passwordField.value){
			confirmPasswordField.color = "black";
		}
		else{
			confirmPasswordField.color = "black";
		}
		CheckSubmitButton();
	});
	
	
	var submitButton = Ti.UI.createView({
		backgroundColor: globals.colors.wordBoxesColors[0],
		width: 80,
		height: 40,
		top: 20,
		enabled: false,
		opacity: 0.5
	});
	
	submitButton.add(Ti.UI.createLabel({
		text: "Submit",
		font: globals.fonts.words1,
		//left:7,
		//height: 40,
		color: globals.colors.wordsInBoxesUnselected,
		touchEnabled:false,
	}));
	
	submitButton.addEventListener("singletap", function(e){
		if (submitButton.enabled == true)
		{
			var ServerData = require('model/ServerData');
			function SuccessCallback(e){
				LocalData.SaveServerUserInfo(usernameField.value, passwordField.value,emailField.value);
			}
			
			function ErrorCallback(e){
				
			}
			
			ServerData.Signup(usernameField.value, passwordField.value,emailField.value, SuccessCallback, ErrorCallback);
		}
		else{
			alert("Please fix the errors");
		}
	});
	
	function CheckSubmitButton (){
		var emailVerified = validateEmail(emailField.value);
		if (usernameField.value.length > 5 &&
			emailVerified == true &&
			passwordField.value == confirmPasswordField.value &&
			passwordField.value.length >= 8){
				submitButton.enabled = true;
				submitButton.opacity = 1;
			}
			else{
				submitButton.enabled = false;
				submitButton.opacity = 0.5;
			} 
	}
	
	centerContainer.add(emailField);
	centerContainer.add(usernameField);
	centerContainer.add(passwordField);
	centerContainer.add(confirmPasswordField);
	centerContainer.add(submitButton);
	
	scrollView.add(centerContainer);
	
	win.add(closeButton);
	win.add(scrollView);
	
	win.addEventListener("open", function(){
		win.animate(Common.appearAnimation());
	});
	
	
	function validateEmail(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	} 
	
	return win;
};	


function CreateRow(data){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		data: data,
		layout:"vertical"
	});
	
	var resultLabel = Ti.UI.createLabel({
		text: '"'+data.text+'"',
		width: globals.platform.width-20,
		font: globals.fonts.words2,
		color: globals.colors.text,
		textAlign: "center",
		top: 20
	});
	
	var authorLabel = Ti.UI.createLabel({
		text: "- "+data.author,
		//width: ,
		font: globals.fonts.regular1,
		color: globals.colors.textMenus,
		textAlign: "right",
		top: 2,
		right: 20
	});
		
	row.add(resultLabel);
	row.add(authorLabel);
	row.add(Ti.UI.createView({height:10}));

	
	return row;
};