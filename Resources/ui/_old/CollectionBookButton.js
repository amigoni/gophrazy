exports.createView = function(){
	var Bubble = require("ui/Common/Bubble");
	var gameData = globals.gameData;
	var userLevel = globals.gameData.userLevel;
	
	var view = Ti.UI.createView({
		width: 80,
		height: 70,
		//bottom: -300,
		//left: 80
	});
	
	var image = "/images/starterPackButton.png";
	if (globals.gameData.abilities.collectionsBook.enabled == true) image = "/images/newspaperIcon.png";
	var icon = Ti.UI.createImageView({
		image: image,
		//width: 54,
		//height: 49,
		bottom: globals.gameData.abilities.collectionsBook.enabled == true ? 15 : 5,
		touchEnabled: true,
		buttonID: "mainMenuCollectionsBookButton"
	});
	
	icon.addEventListener("singletap",function(e){
		if (icon.touchEnabled == true) setTimeout(function(){
			view.fireEvent("pressed");
			if (globals.gameData.abilities.collectionsBook.enabled == false){
				if(globals.gameData.energy <= 0) globals.rootWin.OpenWindow("smallSquarePopUp","outOfEnergy");
				else{
					var GroupsController = require("controller/GroupsController");
					var rowData = globals.col.groups.find({groupID: "main"})[0];
					var group = GroupsController.CreateGroup("collection","", rowData.$id);
					globals.rootWin.OpenWindow("gameWin", group);
				}
			}
			else{
				Common.CreateButtonBlocker(true);
				globals.rootWin.OpenWindow("collectionsBookWin");
			}
			
			globals.gameData.newIssueIconPresent = false;
			globals.col.gameData.commit();
			RefreshCollectionBookIcon();
		},200);
	});
	
	Common.MakeButton(icon);
	
	var newSign = Ti.UI.createImageView({
		image:"/images/newSign.png",
		top: 5,
		left: 5,
		visible: globals.gameData.newIssueIconPresent
	});
	
	view.add(icon);
	view.add(newSign);
	
	
	function RefreshCollectionBookIcon (e){
		//if(gameData.newCollectionContent == true && userLevel >= 5) redCircle.visible = true;
		if (globals.gameData.abilities.collectionsBook.enabled == true) icon.image = "/images/newspaperIcon.png";
		newSign.visible = globals.gameData.newIssueIconPresent;
	}
	
	function Close(){
		Ti.App.removeEventListener("levelUp", RefreshCollectionBookIcon);
	}
	
	Ti.App.addEventListener("levelUp", RefreshCollectionBookIcon);
	
	/*
	setTimeout(function(){
		Animator.animate(view,{
			duration: 1000,
			bottom: 10,
			easing: Animator["EXP_OUT"]
		});
	}, 1550);
	*/
	RefreshCollectionBookIcon();
	
	view.Close = icon.Close;
	
	
	return view;
};
