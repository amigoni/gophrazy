exports.createView = function(type,data){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 0,
		separatorColor: "transparent",
		separatorStyle: false,
		left: 0,
		//data: data ? data: null
		dataIn: data
	});
	
	var tableData = [];
	
	if(data.currentEvents.length > 0) tableData.push(CreateHeaderRow("Current"));
	for (var i = 0; i < data.currentEvents.length; i++){
		var row = CreateGroupRow(data.currentEvents[i]);
		row.addEventListener("clicked", function(e){
			tableView.fireEvent("openGroup", {rowData: e.rowData});
		});
		tableData.push(row);
	}
	
	if(data.expiredEvents.length > 0) tableData.push(CreateHeaderRow("Expired"));
	for (var i = 0; i < data.expiredEvents.length; i++){
		var row = CreateGroupRow(data.expiredEvents[i]);
		row.addEventListener("clicked", function(e){
			tableView.fireEvent("openGroup", {rowData: e.rowData});
		});
		tableData.push(row);
	}
	
	tableView.data = tableData;
	
	function MoveLeft (){
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
	}
	
	function MoveRight (){
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left+globals.platform.width,
			easing:Animator['EXP_IN']
		}, function(){Close();});
	}
	
	function Close(){
		Ti.App.removeEventListener("updateEvent", UpdateTableView);
	}
	
	function UpdateTableView(e){
		for (var i = 0; i < tableData.length; i++){
			if(tableData[i].rowData && tableData[i].rowData.eventID == e.data.eventID){
				tableData[i].Update(e.data);
			}
		}
	}
	
	
	var clockInterval = setInterval(function(){
		var now = clock.getNow();
		for (var i = 0; i < tableData.length; i++){
			var row = tableData[i];
			if (row.data){
				if( now.valueOf() < row.data.endDate){
					row.UpdateTime(exports.GetTimeLeftString(now, row.endDate));
				}
			}
		}
	}, 1000);
	
	tableView.MoveLeft = MoveLeft;
	tableView.MoveRight = MoveRight;
	tableView.Close = Close;
	
	Ti.App.addEventListener("updateEvent", UpdateTableView);
	
	
	return tableView;
};


exports.GetTimeLeftString = function(now, endDate){
	var diff = new moment.duration(endDate.diff(now));
	var text = "";
	if (diff.asSeconds() > 0){
		var days = diff.days();
		if (days == 0) days = "";
		else days = days+"d ";
		var hours = diff.hours();
		if (hours < 10) hours = "0"+hours;
		var minutes = diff.minutes();
		if (minutes < 10) minutes = "0"+minutes;
		var seconds = diff.seconds();
		if (seconds < 10) seconds = "0"+seconds;
		text = days+hours+":"+minutes+":"+seconds;
	}
	else{
		text = endDate.calendar();
	}
	
	return text;
};
	

function CreateHeaderRow(name){
	var view = Ti.UI.createTableViewRow({
		//backgroundColor: "red",
		width: globals.platform.width,
		height: 30,
		selectionStyle: false
	});
	
	var label = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: name,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE,
		bottom: 0
	});
	
	view.add(label);
	
	return view;
}


function CreateGroupRow(rowData){	
	var GroupsController = require("controller/GroupsController");
	var UpgradeController = require("controller/UpgradeController");
	
	var now = clock.getNow();
	var expired = now.isAfter(rowData.endDate);
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 50,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		rowData: rowData,
		endDate: new moment(rowData.endDate)
	});
	
	view.addEventListener("touchstart", function(){
		overlay.opacity = 0.3;
		setTimeout(function(){
			overlay.opacity = 0;
		},500);
	});
	
	view.addEventListener("singletap", function(e){
		view.fireEvent("openGroup",{rowData: rowData});
	});
	
	var label1 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.name,
		font: globals.fonts.bold15,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: globals.platform.width - 110,
		height: 20,
		left: 10,
		top: 15
	});
	
	var label2 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.phrasesCompleteCount+"/"+rowData.phrasesCount,
		font: globals.fonts.bold12,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		textAlign: "right",
		height: 20,
		right: 70,
		top: 15
	});
	
	var label3 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.description,
		font: globals.fonts.regular9,
		minimumFontSize: 10,
		color: globals.colors.mediumBrown,
		textAlign: "left",
		width:  globals.platform.width - 110,
		height: 20,
		left: 10, 
		top: 25
	});
	
	var label4 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: exports.GetTimeLeftString(clock.getNow(),view.endDate),
		font: globals.fonts.regular11,
		minimumFontSize: 10,
		color: globals.colors.mediumBrown,
		textAlign: "right",
		width: Ti.UI.SIZE,
		height: 20,
		right: expired ? 10 : 10, 
		top: expired ? 25 : 35,
		visible: !rowData.complete
	});
	
	var newSign = Ti.UI.createView({
		backgroundImage: "/images/newSign.png",
		width: 25,
		height: 18,
		top: 5,
		left: 2,
		visible: rowData.isNew
	});
	
	var completeSign = Ti.UI.createView({
		backgroundImage: "/images/completeSign.png",
		width: 47,
		height: 12,
		right: 8,
		bottom: 5,
		visible: rowData.complete
	});
	
	var expiredSignVisible = expired;
	if (rowData.complete == true) expiredSignVisible = false;
	var expiredSign = Ti.UI.createImageView({
		image: "/images/expiredSign.png",
		width: 47,
		//height: 12,
		right: 8,
		top: 17,
		visible: expiredSignVisible
	});
	
	var playButton = Ti.UI.createImageView({
		image: "/images/playButton.png",
		width: 50,
		//height: 40,
		top: 13,
		right: 12,
		visible: !expired,
		bubbleParent: false,
		buttonID: "eventsWinRowPlayButton"
	});
	
	Common.MakeButton(playButton);
	
	playButton.addEventListener("singletap", function(){
		setTimeout(function(){
			var eventPhrasesCount = globals.col.phrases.count({eventID:rowData.eventID});
			if (eventPhrasesCount != rowData.phrasesCount){
				UpgradeController.GetFile("https://s3.amazonaws.com/Wordhop/event_"+rowData.eventID+".txt", function(e1){
					GroupsController.UpgradeEventPhrases(JSON.parse(e1.responseText),rowData.eventID);
					PlayEvent();
				});
			}
			else PlayEvent();
		
		},200);
		
		function PlayEvent(){
			if(globals.gameData.energy > 0){ 
				//view.fireEvent("updateCurrentRow", {rowData:rowData});
				Common.CreateButtonBlocker(false);
				var GroupsController = require("controller/GroupsController");
				var groupData = GroupsController.CreateGroup("event","",rowData.$id);
				globals.rootWin.OpenWindow("gameWin", groupData);
			}	
			else globals.rootWin.OpenWindow("smallSquarePopUp", "outOfEnergy");
		}
	});
	
	var overlay = Ti.UI.createView({backgroundColor: "black", opacity: 0, width: Ti.UI.FILL, height: Ti.UI.FILL});
	
	
	view.add(label1);
	view.add(label2);
	view.add(label3);
	view.add(label4);
	view.add(newSign);
	view.add(completeSign);
	view.add(expiredSign);
	view.add(playButton);
	view.add(overlay);
	
	function UpdateTime (text){
		label4.text = text;	
	}
	
	
	function Update(newData){
		rowData = newData;
		label2.text = (rowData.phrasesCompleteCount+"/"+rowData.phrasesCount);
		if (rowData.phrasesCompleteCount >= rowData.phrasesCount) completeSign.visible = true;
	}
	
	view.Update = Update;
	view.UpdateTime = UpdateTime;
	
	var timeInterval = setInterval(function(){
		var now = clock.getNow();
		if (now.valueOf() > view.endDate.valueOf()){
			//completeSign.visible = true;
			playButton.visible = false;
			if (rowData.complete == false) expiredSign.visible = true;
			label4.visible = true;
		}
		else{
			label4.text = exports.GetTimeLeftString(now,view.endDate);
		}
	},1000);
	
	Ti.App.addEventListener("updateEvent", Update);
	
	
	return view;
};
