exports.createView = function(groupData,loadingIncrement,emptyText){
	var tableData = [];
	
	var tableView = Ti.UI.createTableView({
		width: globals.platform.width,
		height:Ti.Platform.displayCaps.platformHeigth-globals.adsBannerSpace-44,
		backgroundColor: "transparent",
		separatorStyle: false,
		top: 0,
		left: globals.platform.width,
		updating: false,
		lastDistance: 0,
		tableData: [],
		phrases: groupData.phrases,
		currentLastItemNumber: -1,
		dataIn: groupData
	});
	
	
	/*
	var loadingRow = Ti.UI.createTableViewRow({title:"Loading...", color:"white"});
	var updating = false;
	tableView.BeginUpdate = function(){
		updating = true;
		//navActInd.show();
		tableView.appendRow(loadingRow);
		tableView.loadingRowPosition = tableView.currentLastItemNumber+1;
		
		setTimeout(function(){tableView.UpdateData();},100);
	};
	
	tableView.UpdateData = function(){
		//Ti.API.info("Loading: "+tableView.loadingRowPosition);
		if (tableView.currentLastItemNumber < phrases.length-1){
			Ti.API.info("Ciao")
			var newLastItemNumber = tableView.currentLastItemNumber+loadingIncrement;
			if (newLastItemNumber >= tableView.phrases.length) newLastItemNumber = phrases.length - 1;
			
			for (var i = tableView.currentLastItemNumber+1; i <= newLastItemNumber ; i++)
			{
				Ti.API.info("Ciao")
				var row = CreatePhraseRow(tableView.phrases[i]);
				row.addEventListener("singletap", function(e){
					tableView.lastTouchedRowIndex = i-1;
				});
				
				tableView.appendRow(row);
			}
			tableView.currentLastItemNumber = newLastItemNumber;
			//tableView.deleteRow(tableView.loadingRowPosition,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
			
			
			//Ti.API.info("Last: "+tableView.currentLastItemNumber);	
			//tableView.scrollToIndex(tableView.currentLastItemNumber-loadingIncrement-1,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});	
		}
		
		updating = false;
	};
	
	tableView.addEventListener('scroll',function(e){
		var offset = e.contentOffset.y;
		var height = e.size.height;
		var total = offset + height;
		var theEnd = e.contentSize.height;
		var distance = theEnd - total;
		// going down is the only time we dynamically load,
		// going up we can safely ignore -- note here that
		// the values will be negative so we do the opposite
		if (distance < tableView.lastDistance)
		{
			// adjust the % of rows scrolled before we decide to start fetching
			var nearEnd = theEnd * 0.75;
			if (updating == false && (total >= nearEnd))
			{
				//tableView.BeginUpdate();
				updating = true;
				setTimeout(function(){tableView.UpdateData();},500);
			}
		}
		tableView.lastDistance = distance;
	});
	
	function RemoveRow(e){
		Ti.API.info(e);
		if (e.isFavoriteNow == false) tableView.deleteRow(tableView.lastTouchedRowIndex,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
	};
	
	//Ti.App.addEventListener("favoriteCount", RemoveRow);
	*/
	tableView.footerView = Ti.UI.createView({height: 1});
	
	function UpdateTableView(){
		tableData = [];
		tableData.push(CreateNavRow("group",groupData));
		var completeCount = 0;
		for (var i = 0; i < groupData.phrases.length; i++){
			if(groupData.phrases[i].complete == true){
				completeCount++;
				var row = CreatePhraseRow(groupData.phrases[i]);
				row.addEventListener("singletap", function(e){
					tableView.lastTouchedRowIndex = i-1;
				});
				
				tableData.push(row);
			}
		}
		if (completeCount == 0) tableData.push(CreateNoPhrasesRow());
		tableView.data = tableData;
	}
	
	function MoveLeft (){
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
	}
	
	function MoveRight (){
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left+globals.platform.width,
			easing:Animator['EXP_IN']
		}, function(){Close();});
	}
	
	function Close(){
		//tableView.fireEvent("closed");
		Ti.App.removeEventListener("updateEvent", UpdateTableView);
	}
	
	tableView.MoveLeft = MoveLeft;
	tableView.MoveRight = MoveRight;
	tableView.Close = Close;
	tableView.UpdateTableView = UpdateTableView;
	//tableView.UpdateData();
	
	Ti.App.addEventListener("updateEvent", UpdateTableView);
	
	tableView.UpdateTableView();
	
	
	return tableView;
};


function CreateNavRow(type,data){
	var MainTableView = require("ui/EventsWinElements/MainTableView");
	var endDate = new moment(data.endDate);
	var now = clock.getNow();
	var view = Ti.UI.createTableViewRow({
		//backgroundColor: "red",
		width: globals.platform.width,
		height: 80,
		selectionStyle: false
	});
	
	var backButton = Ti.UI.createView({
		backgroundImage: "/images/backButton.png",
		width: 30,
		height: 26,
		left: 10,
		top: 20
	});
	
	Common.MakeButton(backButton);
	
	backButton.addEventListener("singletap", function(){
		setTimeout(function(){view.fireEvent("back");},200);
	});
	
	var expired = now.isAfter(data.endDate);
	var expiredSign = Ti.UI.createImageView({
		image: "/images/expiredSign.png",
		top: 15,
		visible: expired
	});
	
	var completeSign = Ti.UI.createImageView({
		image: "/images/completeSign.png",
		top: 15,
		visible: false
	});

	var playButton = Ti.UI.createView({
		backgroundImage: "/images/playButton.png",
		width: 75,
		height: 42,
		top:15,
		visible: !expired
	});
	
	Common.MakeButton(playButton);
	
	playButton.addEventListener("singletap", function(){
		setTimeout(function(){
			if(globals.gameData.energy > 0){ 
				Common.CreateButtonBlocker(false);
				var GroupsController = require("controller/GroupsController");
				data = GroupsController.CreateGroup("collection","",data.$id);
				globals.rootWin.OpenWindow("gameWin", data);
			}	
			else globals.rootWin.OpenWindow("smallSquarePopUp", "outOfEnergy");
		},200);
	});
	
	var countLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: data.phrasesCompleteCount+"/"+data.phrasesCount,
		font: globals.fonts.bold15,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		textAlign: "right",
		width: 40,
		height: 20,
		right: 10,
		top: 25
		//bottom: 10
	});
	
	var label3 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: MainTableView.GetTimeLeftString(clock.getNow(),endDate),
		font: globals.fonts.regular15,
		minimumFontSize: 10,
		color: globals.colors.mediumBrown,
		textAlign: "right",
		width: Ti.UI.SIZE,
		height: 20,
		top: 55
	});
	
	
	var timeInterval = setInterval(function(){
		var now = clock.getNow();
		if (now.valueOf() > endDate.valueOf()){
			//completeSign.visible = true;
			playButton.visible = false;
			expiredSign.visible = true;
			label3.visible = true;
		}
		else{
			label3.text = MainTableView.GetTimeLeftString(now,endDate);
		}
	},1000);
	
	
	view.add(expiredSign);
	view.add(completeSign);	
	view.add(backButton);
	view.add(playButton);
	view.add(countLabel);
	view.add(label3);
	
	function UpdateNavRow(e){
		countLabel.text = e.phrasesCompleteCount+"/"+data.phrasesCount;
		if (e.phrasesCompleteCount >= data.phrasesCount){
			completeSign.visible = true;
			playButton.visible = false;
			expiredSign.visible = false;
		}
	}
	
	function Close(){
		Ti.App.removeEventListener("updateEvent", UpdateNavRow);
		clearInterval(timeInterval);
	}
	
	view.UpdateNavRow = UpdateNavRow;
	view.Close = Close;
	
	Ti.App.addEventListener("updateEvent", UpdateNavRow);
	
	UpdateNavRow(data);
	
	return view;
}


function CreatePhraseRow(data){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		data: data,
		layout:"vertical"
	});
	
	row.addEventListener("singletap", function(){
		//OpenSharePopUp(data);
	});
	
	var resultLabel = Ti.UI.createLabel({
		text: '"'+crypt.decrypt(startingPhrase)+'"',
		width: globals.platform.width-20,
		font: globals.fonts.words2,
		color: globals.colors.darkBrown,
		textAlign: "center",
		top: 20
	});
	
	var author = data.author != "" ? data.author : "Anonymous";
	var authorLabel = Ti.UI.createLabel({
		text: "- "+author,
		//width: ,
		font: globals.fonts.regular1,
		color: globals.colors.darkBrown,
		textAlign: "right",
		top: 2,
		right: 20
	});
		
	row.add(resultLabel);
	row.add(authorLabel);
	row.add(Ti.UI.createView({height:10}));

	
	return row;
};


function CreateNoPhrasesRow (){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 60,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		layout:"vertical"
	});
	
	var label = Ti.UI.createLabel({
		text: 'No phrases completed.\nCompleted phrases will show here.',
		width: globals.platform.width-20,
		font: globals.fonts.bold13,
		color: globals.colors.mediumBrown,
		textAlign: "center",
	});
	
	row.add(label);
	
	return row;
}
