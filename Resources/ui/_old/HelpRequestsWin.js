exports.loadWin = function(dataIn){
	var Common = require('ui/Common');
	var LocalData = require("model/LocalData");
	var ServerData = require("model/ServerData");
	var WMessagesController = require("controller/WMessagesController");
	
	var gameData = LocalData.GetGameData();
	var dataIn = dataIn;
	var inboxPhrasesData = [];
	var sentPhrasesData = [];
	var phrasesData = [];
	var type = dataIn.type;
	
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor: globals.colors.background,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:0,
	    transform: Ti.UI.create2DMatrix().translate(globals.platform.width,0),
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 1
	});
	
	var navBar = Ti.UI.createView({
		height: 44,
		top: 0	
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: dataIn.title,
		font: globals.fonts.words3,
		color: globals.colors.text,
		width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "center",
		lastText: dataIn.title
	});
	
	var backButton = Common.CreateBackButton();
	
	backButton.addEventListener("singletap", function(){
		Common.WindowCloseAnimation(win);
		globals.lastWindow.BackToThisWindow();
		setTimeout(function(){win.close();},550);
	});
	
	navBar.add(titleLabel);
	navBar.add(backButton);
	
	
	var tableView = CreateTableView();
	
	win.add(navBar);
	win.add(tableView);	
	
	
	win.addEventListener("open", function(){
		var Common = require('ui/Common');
		Common.WindowOpenAnimation(win);
		globals.lastWindow.OpenAnotherWindow();
	});
	
	win.addEventListener("close", function(){
		Ti.App.removeEventListener("updateWMessages", UpdateTableData);
	});
	
	
	function UpdateTableData(dataIn){
		var data;
		if (type == "inbox") data = dataIn.results.inbox.data;
		else if (type == "sent")data = dataIn.results.sent.data;
	
		phrasesData = [];
		for (var i = 0; i < data.length; i++){	
			phrasesData.push(data[i]);
		}
		
		tableView.Update(phrasesData,SelectRowCallback);
		//titleLabel.text = titleLabel.lastText;
	}
	
	function UpdateTableDataSplit (dataIn){
		var data = dataIn.results.data;
		sentPhrasesData = [];
		inboxPhrasesData = [];
		
		for (var i = 0; i < data.length; i++){	
			/* PARSE.COM IMPLEMENTATION
			var phraseInfo = data[i].attributes;
			phraseInfo.name = GetFriendName(phraseInfo.receiverID);
			phraseInfo.date = new Date(data[i].createdAt).getTime();
			phraseInfo.serverID = data[i].id;
			phraseInfo.shuffledWords = data[i].attributes.shuffledWords;
			*/
			
			/*Ti.Cloud Direct
			//var phraseInfo = data[i];
			//alert(new Date(data[i].created_at));
			//phraseInfo.name = GetFriendName(phraseInfo.receiverID);
			//phraseInfo.date = new moment(data[i].createdAt).valueOf();
			///phraseInfo.serverID = data[i].id;
			//phraseInfo.shuffledWords = data[i].shuffledWords;
			*/
			
			//From Cache
			if(phraseInfo.sent == true) sentPhrasesData.push(phraseInfo);
			else inboxPhrasesData.push(phraseInfo);
		}
		
		inboxTableView.Update(inboxPhrasesData,SelectRowCallback);
		sentTableView.Update(sentPhrasesData,SelectRowCallback);
		//titleLabel.text = titleLabel.lastText;
	}
	
	
	function GetFriendName(facebookID){
		var firstName = facebookID;
		var lastName = facebookID;
		if (facebookID != gameData.userFacebookID){
			for (var i = 0; i < gameData.userFacebookFriends.length; i++){
				if (gameData.userFacebookFriends[i].id == facebookID){
					firstName = gameData.userFacebookFriends[i].first_name;
					lastName = gameData.userFacebookFriends[i].last_name;
					break;
				} 
			}
		}
		else {
			firstName = "You";
			lastName = "";
		}	
		
		return {firstName: firstName, lastName: lastName};
	}
	
	function SelectRowCallback(data){
		var GameWin = require("ui/GameWin");
		var win1 = GameWin.loadWin(LocalData.CreateHelpOthersPhrasesGroup(data),data.completed);
		win1.open();
	}
	
	Ti.App.addEventListener("updateWMessages", UpdateTableData);
	
	//UpdateTableData({results:WMessagesController.GetAllWMessages()});
	var fake = {};
	if (type == "inbox") fake.inbox = dataIn;
	else fake.sent = dataIn;
	UpdateTableData({results:fake});
	
	return win;
};	




function CreateLoadingRow(){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 60,
		backgroundColor: "transparent",
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "Loading...",
		color: globals.colors.text,
		//width: globals.platform.width-20,
		font: globals.fonts.words2,
		left: 10,
		top: 5
	});
	
	row.add(titleLabel);
	
	return row;
}


function CreateEmptyRow(){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 60,
		backgroundColor: "transparent",
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "There are no Hop Messages.",
		color: globals.colors.text,
		//width: globals.platform.width-20,
		font: globals.fonts.words2,
		left: 10,
		top: 5
	});
	
	row.add(titleLabel);
	
	
	return row;
}


function CreateRow(data,selectCallRowCallback){
	var WMessagesController = require("controller/WMessagesController");
	
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 60,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine2,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.GRAY,
		data: data
	});
	
	row.addEventListener("singletap",function(e){
		newDot.visible = false;
		checkMark.backgroundImage = "/images/checkMark.png";
		WMessagesController.MarkWMessageOpened(data.$id);
		
		if (data.sent == false){
			selectCallRowCallback(data);
		}
		else{
			if (data.type == "helpRequest"){
				selectCallRowCallback(data);
			}
			else alert("Mmm. They haven't solved it yet!");
		}
	});
	
	var background = Ti.UI.createView({
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine2,
		width: row.width-10,
		height: row.height-10
	});
	
	var newDot = Ti.UI.createView({
		backgroundColor: globals.colors.warning,
		borderRadius: 6,
		width: 12,
		height: 12,
		top: 10,
		left: 15,
		visible: (data.completed == false && data.opened == false) ? true : false
	});
	
	var checkMark = Ti.UI.createView({
		backgroundImage: (data.opened == false && data.completedByUserID != globals.gameData.userFacebookID) ? "/images/checkMarkYellow.png":"/images/checkMark.png",
		width: 16,
		height: 13,
		top: 10,
		left: 13,
		visible: data.completed == true ? true : false
	});
	
	var helpIcon = Ti.UI.createView({
		backgroundImage:  (data.completed == false) ? "/images/questionMarkYellow.png":"/images/questionMark.png",
		width: 19,
		height: 19,
		bottom: 10,
		left: 11
	});
	
	var thumbIcon = Ti.UI.createView({
		backgroundImage: (data.completed == false) ? "/images/thumbIconYellow.png": "/images/thumbIcon.png",
		width: 17,
		height: 16,
		bottom: 10,
		left: 13
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: data.titleText,
		color: globals.colors.text,
		width: globals.platform.width-90,
		font: globals.fonts.words2,
		minimumFontSize: 12,
		left: 35,
		top: 5
	});
	
	var puzzleLabel = Ti.UI.createLabel({
		text: CreatePuzzleLabel(data.shuffledWords),
		color: globals.colors.textMenus,
		width: globals.platform.width-40,
		height: 20,
		font: globals.fonts.regular2,
		left: 35,
		bottom: 18
	});
	
	var dateCreatedLabel = Ti.UI.createLabel({
		text: ParseDate(data.createdAt),
		color: globals.colors.textMenus,
		//width: globals.platform.width-20,
		font: globals.fonts.regularSmall,
		right: 10,
		top: 10
	});
	
	//var completedByText = data.completedBy == 
	
	var completedLabel = Ti.UI.createLabel({
		text: data.completed == true ? "Solved by "+data.completedBy.firstName+" "+data.completedBy.lastName+" , "+ParseDate(data.completedAt) : "Not solved.",
		color: globals.colors.text,
		width: globals.platform.width-20,
		font: globals.fonts.regularSmall,
		left: 35,
		bottom: 10
	});
	
	var typeLabel = Ti.UI.createLabel({
		text: data.type,
		color: globals.colors.warning,
		//width: globals.platform.width-20,
		font: globals.fonts.boldSmall,
		right: 10,
		bottom: 5
	});
	
	row.add(background);
	if (data.sent == false) row.add(newDot);
	if (data.completed == true) row.add(checkMark);
	if (data.type == "helpRequest") row.add(helpIcon);
	if (data.type == "suggestion") row.add(thumbIcon);
	row.add(titleLabel);
	row.add(puzzleLabel);
	row.add(dateCreatedLabel);
	row.add(completedLabel);
	//row.add(typeLabel);
	
	function CreatePuzzleLabel(shuffledWords){
		var puzzlePhrase = "";
		for (var i = 0; i < shuffledWords.length; i++){
			puzzlePhrase += shuffledWords[i]+"/";
		}
		
		return puzzlePhrase;
	}
	
	return row;
};


function CreateTableView(){
	var tableData = [];
	
	var tableView = Ti.UI.createTableView({
		width: globals.platform.width,
		height:Ti.Platform.displayCaps.platformHeigth-globals.adsBannerSpace-44,
		backgroundColor: "transparent",
		separatorStyle: false,
		top: 44
	});
	
	tableView.data = [CreateLoadingRow()];
	//tableView.footerView = Ti.UI.createView({height: 1});
	
	function Update(phrasesDataIn,SelectRowCallback){
		var tableData = [];
		var orderedData = [];
		var completePhrases = [];
		var incompletePhrases = [];
		
		for (var i = 0; i < phrasesDataIn.length; i++){
			if(phrasesDataIn[i].completed) completePhrases.push(phrasesDataIn[i]);
			else incompletePhrases.push(phrasesDataIn[i]);
		}
		
		var sort_by = function(field, reverse, primer){
		   var key = primer ? 
		       function(x) {return primer(x[field]);} : 
		       function(x) {return x[field];};
		
		   reverse = [-1, 1][+!!reverse];
		
		   return function (a, b) {
		       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
		   }; 
		};
		
		completePhrases.sort(sort_by("createdAt",false));
		incompletePhrases.sort(sort_by("createdAt",false));
		orderedData = incompletePhrases.concat(completePhrases);
		
		for (var i = 0; i < orderedData.length; i++){
			tableData.push(CreateRow(orderedData[i],SelectRowCallback));
		}
		
		if (tableData.length == 0) tableData.push(CreateEmptyRow());
		
		tableView.data = tableData;
	}	
	
	tableView.Update = Update;
	
	return tableView;
}



function ParseDate(unixDate){
	var dateString = "";
	var then = moment(unixDate);
	var now = moment();
	/*
	
	//Today show hour
	if (then.date() == now.date() && then.month() == now.month() && then.year() == now.year())//also check month and year.
		dateString = then.format(LT);
	else if (now.diff(then, 'days') < 7){
		dateString = then.day();
	}
	else if (now.diff(then, 'days') < 7){
		dateString = then.format(l);
	}	
	*/
	
	return then.calendar();
};


function FolderBar(){
	var folderBar = Ti.UI.createView({
		height: 44,
		width: Ti.UI.SIZE,
		top: 44,
		layout: "horizontal"	
	});
	
	var inboxButton = Ti.UI.createView({
		borderColor: globals.colors.borderLine,
		borderWidth: 2,
		width: 70,
		height: 40
	});
	
	inboxButton.add(Ti.UI.createLabel({
		text: "Inbox",
		font: globals.fonts.words2,
		color: globals.colors.text,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "center"
	}));
	
	inboxButton.addEventListener("singletap", function(){
		titleLabel.text = "Hopmail - Inbox";
		titleLabel.lastText = "Hopmail - Inbox";
		inboxTableView.visible = true;
		sentTableView.visible = false;
	});
	
	
	var sentButton = Ti.UI.createView({
		borderColor: globals.colors.borderLine,
		borderWidth: 2,
		width: 70,
		height: 40,
		left: 20
	});
	
	sentButton.add(Ti.UI.createLabel({
		text: "Sent",
		font: globals.fonts.words2,
		color: globals.colors.text,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "center"
	}));
	
	sentButton.addEventListener("singletap", function(){
		titleLabel.text = "Hopmail - Sent";
		titleLabel.lastText = "Hopmail - Sent";
		inboxTableView.visible = false;
		sentTableView.visible = true;
	});
	
	folderBar.add(inboxButton);
	folderBar.add(sentButton);
	
	return folderBar;
};
