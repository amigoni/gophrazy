var Common = require('ui/Common');

exports.loadWin = function(groupData){
	var LocalData = require("model/LocalData");
	var nextUnlockCondition = "Complete 10 Quotes to unlock next category";
	var currentLastItemNumber = 0;
	var loadingIncrement = 20;
	var allPhrases = globals.col.phrases.find({complete:false},{$sort:{completeTime:-1}});
	var favoritePhrases = globals.col.phrases.find({favorite:true},{$sort:{completeTime:-1}});

	var win = Titanium.UI.createWindow({ 
	    backgroundColor: "transparent",
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:0,
	    fullscreen:true,
	    navBarHidden: true,
	    //transform: Ti.UI.create2DMatrix().translate(globals.platform.width,0)
	    opacity:0
	});
	
	var background = Ti.UI.createView({
		backgroundColor:globals.colors.background,
		width: win.width,
		height:win.height,
		opacity: 0.93 
	});
	
	win.addEventListener("open", function(){
		win.animate(Common.appearAnimation());
		//Common.WindowOpenAnimation(win);
		//globals.mainMenuWin.OpenAnotherWindow();
	});
	
	var navBar = CreateNavBar(win);
	
	var tableView = CreateDynamicLoadingTableView(groupData.phrasesIDs,loadingIncrement);
	//tableView.opacity = 0;
	//var favoritesTableView = CreateDynamicLoadingTableView(favoritePhrases,loadingIncrement);

	win.add(background);
	win.add(navBar);
	win.add(tableView);	
	//win.add(favoritesTableView);	
	

	//favoritesTableView.UpdateData();
	tableView.UpdateData();
	
	
	return win;
};	


function CreateDynamicLoadingTableView(phrases,loadingIncrement,emptyText){
	var tableView = Ti.UI.createTableView({
		width: globals.platform.width,
		height:Ti.Platform.displayCaps.platformHeigth-44,
		backgroundColor: "transparent",
		separatorStyle: false,
		top: 44,
		updating: false,
		lastDistance: 0,
		tableData: [],
		phrases: phrases,
		currentLastItemNumber: -1
	});
	
	
	var loadingRow = Ti.UI.createTableViewRow({title:"Loading...", color:"white"});
	var updating = false;
	tableView.BeginUpdate = function(){
		updating = true;
		//navActInd.show();
		tableView.appendRow(loadingRow);
		tableView.loadingRowPosition = tableView.currentLastItemNumber+1;
		
		setTimeout(function(){tableView.UpdateData();},100);
	};
	
	tableView.UpdateData = function(){
		//Ti.API.info("Loading: "+tableView.loadingRowPosition);
		if (tableView.currentLastItemNumber < phrases.length-1){
			var newLastItemNumber = tableView.currentLastItemNumber+loadingIncrement;
			if (newLastItemNumber >= tableView.phrases.length) newLastItemNumber = phrases.length - 1;
			
			for (var i = tableView.currentLastItemNumber+1; i <= newLastItemNumber ; i++)
			{
				var row = CreateRow(tableView.phrases[i]);
				row.addEventListener("singletap", function(e){
					tableView.lastTouchedRowIndex = i-1;
				});
				
				tableView.appendRow(row);
			}
			tableView.currentLastItemNumber = newLastItemNumber;
			//tableView.deleteRow(tableView.loadingRowPosition,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
			
			
			//Ti.API.info("Last: "+tableView.currentLastItemNumber);	
			//tableView.scrollToIndex(tableView.currentLastItemNumber-loadingIncrement-1,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});	
		}
		
		updating = false;
	};
	
	tableView.addEventListener('scroll',function(e){
		var offset = e.contentOffset.y;
		var height = e.size.height;
		var total = offset + height;
		var theEnd = e.contentSize.height;
		var distance = theEnd - total;
		// going down is the only time we dynamically load,
		// going up we can safely ignore -- note here that
		// the values will be negative so we do the opposite
		if (distance < tableView.lastDistance)
		{
			// adjust the % of rows scrolled before we decide to start fetching
			var nearEnd = theEnd * 0.75;
			if (updating == false && (total >= nearEnd))
			{
				//tableView.BeginUpdate();
				updating = true;
				setTimeout(function(){tableView.UpdateData();},500);
			}
		}
		tableView.lastDistance = distance;
	});
	
	function RemoveRow(e){
		Ti.API.info(e);
		if (e.isFavoriteNow == false) tableView.deleteRow(tableView.lastTouchedRowIndex,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
	};
	
	Ti.App.addEventListener("favoriteCount", RemoveRow);
	
	tableView.footerView = Ti.UI.createView({height: 1});
	
	return tableView;
}


function CreateNavBar(win){
	var navBar = Ti.UI.createView({
		width: globals.platform.width,
		height: 44,
		top: 0	
	});
	
	var icon =  Ti.UI.createView({
		backgroundImage: globals.colors.theme == "light" ? "/images/favoriteButton.png" : "/images/favoriteButtonDark.png",
		width: 27,
		height: 24,
		left: 7
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "Favorites",
		color: globals.colors.text,
		font: globals.fonts.words3,
		//left: 50,
		//top: 7
	});
	
	var backButton = Ti.UI.createView({
		width: 60,
		height: 44,
		right: 0
	});
	
	Common.MakeButton(backButton);
	
	backButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.boldMedium31,
		color: globals.colors.text,
		textAlign: "center",
		//top: groupData.type != "dailyChallenge" ? 4:null
	}));
	
	backButton.addEventListener("singletap", function(){
		win.animate(Common.disappearAnimation());
		setTimeout(function(){win.close();},550);
	});
	
	//navBar.add(icon);
	//navBar.add(titleLabel);
	navBar.add(backButton);
	
	return navBar;
}


function CreateRow(data){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		data: data,
		layout:"vertical"
	});
	
	row.addEventListener("singletap", function(){
		OpenSharePopUp(data);
	});
	
	var resultLabel = Ti.UI.createLabel({
		text: '"'+data.text+'"',
		width: globals.platform.width-20,
		font: globals.fonts.words2,
		color: globals.colors.text,
		textAlign: "center",
		top: 20
	});
	
	var author = data.author != "" ? data.author : "Anonymous";
	var authorLabel = Ti.UI.createLabel({
		text: "- "+author,
		//width: ,
		font: globals.fonts.regular1,
		color: globals.colors.textMenus,
		textAlign: "right",
		top: 2,
		right: 20
	});
		
	row.add(resultLabel);
	row.add(authorLabel);
	row.add(Ti.UI.createView({height:10}));

	
	return row;
};


function OpenSharePopUp(phraseData){
	var Common = require('ui/Common');
	var ServerData = require("/model/ServerData");
	var PhrasesController = require("/controller/PhrasesController");
	var RandomPlus = require("/lib/RandomPlus");
	var win = Titanium.UI.createWindow({ 
	    //backgroundColor: globals.colors.background,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    width: globals.platform.width,
	    top:0,
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 0
	});
	
	/*
	win.addEventListener("singletap", function (){
		CloseAnimation();
	});
	*/
	
	var background = Ti.UI.createView({
		backgroundColor: "black",
		height: win.height,
		width: win.width,
		opacity: 0.95
	});
	
	var closeButton = Ti.UI.createView({
		width: 60,
		height: 44,
		right: 0,
		top: 0
	});
	
	Common.MakeButton(closeButton);
	
	closeButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.boldMedium31,
		color: globals.colors.text,
		textAlign: "center",
		//top: groupData.type != "dailyChallenge" ? 4:null
	}));
	
	closeButton.addEventListener("singletap", function (){
		CloseAnimation();
	});
	
	var phraseView = CreatePhraseView(phraseData.text, phraseData.author);
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		bottom: 100
	});
	
	var shareButton = Ti.UI.createView({
		//backgroundColor: globals.colors.buttonGameToolbarBGColor,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: 70,
		height: 60,
		left: 0,
		bubbleParent: false
	});
	
	shareButton.add(Ti.UI.createLabel({
		text: "Share Phrase",
		font: globals.fonts.boldXS,
		color: globals.colors.buttonTextMenus,
		textAlign:"center",
		top: 35
	}));
	
	shareButton.add(Ti.UI.createView({
		backgroundImage: "/images/shareButton.png",
		width: 33,
		height: 29,
		tranform: Ti.UI.create2DMatrix().scale(0.9,0.9),
		top: 2
	}));
	
	shareButton.addEventListener("singletap", function(e){
		Common.shareMenu({picture:Common.CreateSharePhraseImage(phraseData.text,"- "+phraseData.author).toImage()});
		CloseAnimation();
	});

	
	var favoriteButton = Ti.UI.createView({
		//backgroundColor: globals.colors.buttonGameToolbarBGColor,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: 70,
		height: 60,
		left: 10,
		bubbleParent: false
	});
	
	favoriteButton.add(Ti.UI.createLabel({
		text: "Favorites",
		font: globals.fonts.boldXS,
		color: globals.colors.buttonTextMenus,
		textAlign:"center",
		top: 30
	}));
	
	favoriteButton.add(Ti.UI.createView({
		backgroundImage: globals.colors.theme == "dark" ? "/images/favoriteButtonDark.png" :"/images/favoriteButton.png",
		width: 27,
		height: 24,
		top: 2
	}));
	
	favoriteButton.addEventListener("singletap", function(e){
		var isNowFavorite = PhrasesController.TogglePhraseFavorite(phraseData.phraseID);
		Common.CreateDropdown({type: "favorite", text:isNowFavorite == true ? "Added to Favorites" : "Removed from Favorites"});
	
		CloseAnimation();
	});
	
	
	var sendButton = Ti.UI.createView({
		//backgroundColor: globals.colors.buttonGameToolbarBGColor,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: 70,
		height: 60,
		left: 10,
		bubbleParent: false
	});
	
	sendButton.add(Ti.UI.createLabel({
		text: "Send puzzle\nto a friend",
		font: globals.fonts.boldXS,
		color: globals.colors.buttonTextMenus,
		textAlign:"center",
		top: 30
	}));
	
	sendButton.add(Ti.UI.createView({
		backgroundImage: globals.colors.theme == "dark" ? "/images/sendPuzzleButtonDark.png" : "/images/sendPuzzleButton.png",
		width: 52,
		height: 23,
		top: 2
	}));
	
	sendButton.addEventListener("singletap", function(e){
		var FriendsListWin = require("/ui/FriendsListWin");
		var win1 = FriendsListWin.loadWin();
		win1.open();
		
		function FriendSelected (e1){
			var correctWordsTextArray = phraseData.text.match(/\S+\s/g);
			phraseData.shuffledWords = RandomPlus.ShuffleArray(correctWordsTextArray);
			alert(phraseData.shuffledWords);
			phraseData.type = "suggestion";
			ServerData.SendwMessage(phraseData, e1.data, function(e2){alert("Puzzle Sent!!!");});
			Ti.App.removeEventListener("friendSelected", FriendSelected);
			
			CloseAnimation();
		}
		
		Ti.App.addEventListener("friendSelected", FriendSelected);
	});
	
	container.add(shareButton);
	//container.add(favoriteButton);
	//container.add(sendButton);
	
	
	win.add(background);
	win.add(closeButton);
	win.add(phraseView);
	win.add(container);
	
	function CloseAnimation(){
		Animator.animate(container,{duration: 500,opacity: 0});
		win.animate(Common.disappearAnimation());
		setTimeout(function(){win.close();},550);
	}
	
	win.addEventListener("open", function(){
		Animator.animate(container,{duration: 500,opacity: 1});
		win.animate(Common.appearAnimation());
	});
	
	win.open();
}


function CreatePhraseView(text, author){
	var resultView = Ti.UI.createView({
		width:globals.platform.width-20,
		height: 107,
		top: 64,
		opacity: 0
	});
	
	var resultLabel = Ti.UI.createLabel({
		text: '"'+text+'"',
		width: globals.platform.width-20,
		font: globals.fonts.words2,
		color: globals.colors.text,
		textAlign: "center",
		top: 40
	});
	
	var authorLabel = Ti.UI.createLabel({
		text: author,
		width: globals.platform.width-20,
		font: globals.fonts.regular1,
		color: globals.colors.textMenus,
		textAlign: "right",
		//top: 5,
		opacity: 1,
		bottom: 0
	});
	
	resultView.add(resultLabel);
	resultView.add(authorLabel);
	
	resultView.resultLabel = resultLabel;
	resultView.authorLabel = authorLabel;
	
	setTimeout (function(){
		Animator.animate(resultView,{duration: 500, opacity:1});
		Animator.animate(resultView,{duration: 1500, top:globals.platform.height/2-resultView.height});
	},10);

	
	return resultView;
};
