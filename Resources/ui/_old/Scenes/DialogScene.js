exports.loadMe = function(dialogName){
	var speedMultiplier = .1;
	var dialogData = [];
	
	var dialogs = JSON.parse(GetJsonText("model/Dialogs.txt"));
	
	for (var i = 0; i < dialogs.length; i++){
		if (dialogs[i].dialog == dialogName) dialogData.push(dialogs[i]);
	}

	var view = Ti.UI.createView({ 
	    backgroundColor: "transparent",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //top:0,
	    //transform: Ti.UI.create2DMatrix().translate(globals.platform.width,0)
	    opacity:0
	});
	
	var background = Ti.UI.createView({
		backgroundColor:"black",
		width: view.width,
		height:view.height,
		opacity: 0.93 
	});
	
	var dialogView = CreateDialogView(dialogData,speedMultiplier,view);
	//dialogView.addEventListener("touchend", function(){view.fireEvent("closeDialogScene");});
	dialogView.addEventListener("finished",function(){
		nextButton.opacity = 1;
	});
	
	var nextButton = Ti.UI.createView({
		width: 70,
		height: 70,
		//left: 0
		bottom:0,
		opacity: 0
	});
	
	Common.MakeButton(nextButton);
	
	nextButton.add(Ti.UI.createView({
		backgroundImage: "/images/skipButton.png",
		width: 21,
		height: 20,
	}));
	
	nextButton.addEventListener("singletap", function(e){
		view.fireEvent("close");
	});
		
	view.add(background);
	view.add(dialogView);
	view.add(nextButton);
	
	
	function DisappearAnimation(callback){
		Animator.animate(view,{
			opacity: 0,
			duration: 1000
		},callback);
	}
	
	function AppearAnimation(){
		Animator.animate(view,{
			opacity: 1,
			duration: 1000
		});
	}
	
	function Open(){
		AppearAnimation();
	}
	
	view.DisappearAnimation = DisappearAnimation;
	view.AppearAnimation = AppearAnimation;
	view.Open = Open;
	
	
	return view;
};	


function CreateDialogView (textData,speedMultiplier,parentView,startDelay){
	var clockCounter = 0;
	var currentLine = 0;
	var isPaused = false;
	var clockInterval;
	startDelay = startDelay? startDelay : 2000;
	
	var scrollView = Ti.UI.createScrollView({
		width: parentView.width,
		height: parentView.height,
		contentWidth: parentView.width,
		contentHeight: "auto",
		//layout: "vertical",
		touchEnabled:false
	}); 
	
	var containerBig = Ti.UI.createView({
		width: scrollView.width,
		height: Ti.UI.SIZE,
		bottom: (globals.platform.actualHeight)/2
	});
	
	var container = Ti.UI.createView({
		width: scrollView.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		//bottom: 0
		bottom: (globals.platform.actualHeight)/2
	});
	
	var timer = 0;
	for (var i = 0; i < textData.length; i++){
		textData[i].startTime = timer;
		timer += (textData[i].waitTimeAfter+ textData[i].text.length*textData[i].typeSpeed)*speedMultiplier;
	}
	
	
	function Start(){
		clockInterval = setInterval(function(){
			if (isPaused == false){
				if(textData[currentLine].startTime <= clockCounter){
					var showAuthor = true;
					if (currentLine > 0 && textData[currentLine].authorName == textData[currentLine-1].authorName) showAuthor = false;
					var line = CreateDialogLine(textData[currentLine], showAuthor,speedMultiplier);
					line.addEventListener("finishedLine", function(){
						if (currentLine >= textData.length){
							scrollView.touchEnabled = true;
							scrollView.fireEvent("finished");
						}
					});
					container.add(line);
					AdjustOffset();
					currentLine++;	
				}
				clockCounter += 100;
				if(currentLine >= textData.length) {
					clearInterval(clockInterval);
					setTimeout(function(){clearInterval(offsetInterval);},500);
				}
			}
		},100);
	}
	
	
	var offsetInterval = setInterval(function(){AdjustOffset();},30);
	
	var lastcontentOffsetY = 0;
	function AdjustOffset(){
		var contentOffsetY = container.rect.height - (globals.platform.actualHeight)/2; 
		if (contentOffsetY < 0) contentOffsetY = 0;
		if (contentOffsetY != lastcontentOffsetY){
			scrollView.setContentOffset({x:0, y:(contentOffsetY)},{animated: false});
			lastcontentOffsetY = contentOffsetY;
		}
	}
	
	//containerBig.add(container);
	scrollView.add(container);
	
	scrollView.Start = Start;
	
	setTimeout(function(){Start();},startDelay);
	
	
	return scrollView;
};


function CreateDialogLine(lineData,isNewAuthor,speedMultiplier){
	var view = Ti.UI.createView({
		//borderColor: "white",
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		lineData: lineData,
		top: isNewAuthor == true ? 10 : 0,
		layout: "vertical"
	});
	
	var authorLabel = Ti.UI.createLabel({
		text: lineData.authorName+":",
		font: globals.fonts.words1,
		minimumFontSize: 8,
		//shadowOffset:{x:1,y:1},
		//shadowColor:"black",
		left: 10,
		top: 0,
		width: 40,
		//height: 40,
		
		color: lineData.fontColor,
		touchEnabled:false
	});
	
	var textLabel = Ti.UI.createLabel({
		text: "",
		font: globals.fonts.boldMediumSmall,
		//shadowOffset:{x:1,y:1},
		//shadowColor:"black",
		left: 20,
		//top: 40,
		width: globals.platform.width-40,
		height: Ti.UI.SIZE,
		top:3,
		color: lineData.fontColor,
		touchEnabled:false
	});
	
	var textCharacterCounter = 0;
	var textWritingInterval = setInterval(function(){
		textCharacterCounter++;
		var authorText = "";
		if (lineData.dialogTagEnabled == 1) authorText = lineData.authorName+":    ";
		var actualText = lineData.text.substring(0,textCharacterCounter);
		if(lineData.authorName != "Narrator" && lineData.authorName != "Flashback") actualText = '"'+actualText+'"';
		textLabel.text = authorText+actualText;
		
		if(textCharacterCounter > lineData.text.length) {
			clearInterval(textWritingInterval);
			view.fireEvent("finishedLine");
		}
	},lineData.typeSpeed*speedMultiplier);
	
	
	//if (isNewAuthor == true) view.add(authorLabel);
	view.add(textLabel);
	
	
	return view;
}

function GetJsonText (fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
 
	if (readFile.exists()) {
		var readContents = readFile.read();
	   	return readContents.text;
	}
};
