exports.loadMe = function(){
	var DialogScene = require("/ui/Scenes/DialogScene");
	var textData = [];
	
	var dialogScene; 
	var crystalScene;
	var beamScene;
	var creditsScene;
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor: "transparent",
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:0,
	    fullscreen:true,
	    navBarHidden: true,
	    //transform: Ti.UI.create2DMatrix().translate(globals.platform.width,0)
	    opacity:1
	});
	
	
	//1. Show dialog
	dialogScene = DialogScene.loadMe("endscene1");
	dialogScene.addEventListener("finished", function(){
	//2. open Crystal Win
		crystalScene = CreateCrystalScene();
		crystalScene.addEventListener("finished", function(){
	//3. Open EndScene
			beamScene = CreateBeamScene();
			beamScene.addEventListener("finished", function(){
	//4. Open Credits
				creditsScene = CreateCreditsScene();
				creditsScene.addEventListener("finished", function(){
					win.close();
				});
				win.add(creditsScene);
				win.remove(beamScene);
				beamScene = null;
			});
			win.add(beamScene);
			win.remove(crystalScene);
			crystalScene = null;
		});
		win.add(crystalScene);
		win.remove(dialogScene);
		dialogScene = null;
	});
	
	win.add(dialogScene);
	
	
	return win;
};

function CreateCrystalScene(){
	var Common = require("ui/Common");
	var view = Ti.UI.createView({
		backgroundColor: "#666666",
		width:  globals.platform.width,
		height: globals.platform.actualHeight,
	});	
	
	var ring = Ti.UI.createView({
		backgroundImage: "/images/crystalSceneRing.png",
		width:271,
		height: 276
	});
	
	var button = Ti.UI.createView({
		backgroundImage: "/images/crystalSceneButton.png",
		width: 131,
		height: 132,
		opacity: 0,
		transform: Ti.UI.create2DMatrix().scale(0.5,0.5)
	});
	
	button.addEventListener("singletap", function(){
		view.fireEvent("finished");
	});
	
	Common.MakeButton(button);
	
	
	var locations = [
		{x: 211, y: 42},
		{x: 234, y: 146},
		{x: 172, y: 226},
		{x: 64, y: 226},
		{x: -1, y: 146},
		{x: 21, y: 42},
		{x: 118, y: 0}
	];
	//Animate the crystal insertion
	for (var i = 1; i <=7; i++){
		ring.add(CreateCrystal(1000+i*500));
	}
	
	function CreateCrystal(delay){
		var crystal = Ti.UI.createImageView({
			backgroundImage:"/images/crystal"+i+".png",
			width:39,
			height:51,
			left:locations[i-1].x,
			top: locations[i-1].y,
			transform: Ti.UI.create2DMatrix().scale(0.5,0.5),
			opacity: 0,
			anchorPoint:{x:0.5,y:0.5}
		});
		
		setTimeout(function(){
			Animator.animate(crystal,{
				duration: 500,
				transform: Ti.UI.create2DMatrix().scale(1,1),
				opacity:1,
				easing:Animator['BOUNCE_OUT']
			});
		},delay);
		
		return crystal;
	}
	
	setTimeout(function(){
		Animator.animate(button,{
				duration: 500,
				transform: Ti.UI.create2DMatrix().scale(1,1),
				opacity:1,
				easing:Animator['BOUNCE_OUT']
			});
	}, 6000);

	
	view.add(ring);
	view.add(button);
	
	return view;
}
	
function CreateBeamScene(){
	var view = Ti.UI.createView({
		backgroundColor: "#003040",
		width:  globals.platform.width,
		height: globals.platform.actualHeight,
	});	
	
	var background = Ti.UI.createView({
		backgroundImage: "/images/endScenebg.png",
		width: 279,
		height: 392,
		opacity: 0
	});	
	
	Animator.animate(background,{
		duration: 500,
		opacity:1,
		//easing:Animator['BOUNCE_OUT']
	});
	
	var earthRing = Ti.UI.createView({
		backgroundImage:'/"+globals.imagesPath+"/images/endsceneEarthRing.png',
		width:130,
		height: 130,
		left: 52,
		top: 218
	});
	
	var beam = Ti.UI.createView({
		backgroundImage:'/"+globals.imagesPath+"/images/endsceneBeam.png',
		width: 92,
		height: 110,
		left: 152,
		top: 123,
		opacity: 0
	});
	
	setTimeout(function(){beam.opacity = 1;}, 2000);
	setTimeout(function(){earthRing.opacity = 0; beam.opacity = 0;}, 5000);
	setTimeout(function(){view.fireEvent("finished");},8000);
	
	
	view.add(background);
	view.add(beam);
	view.add(earthRing);
	
	
	return view;	
}
	
function CreateCreditsScene(){
	var height = 0;
	var items = [];
	
	var view = Ti.UI.createView({
		backgroundColor:"#003040",
		width:  globals.platform.width,
		height: globals.platform.actualHeight,
		top: 0,
		opacity: 0
	});	
	
	
	var label1 = Ti.UI.createLabel({
		text: "You Saved the World!",
		font: globals.fonts.boldMediumSmall,
		color: "white",
		textAlign: "center",
		left: 10,
		top: globals.platform.height,
		width: globals.platform.width-20,
		touchEnabled:false,
		duration: 10000
	});
	items.push(label1);
	
	var label2 = Ti.UI.createLabel({
		text: "The End",
		font: globals.fonts.boldMediumSmall,
		color: "white",
		textAlign: "center",
		left: 10,
		top: globals.platform.height,
		width: globals.platform.width-20,
		touchEnabled:false,
		duration: 10000
	});
	items.push(label2);
	
	var stamp = Ti.UI.createView({
		backgroundImage: "/images/mozzarelloStamp.png",
		width: 114,
		height: 114,
		top: globals.platform.height,
		duration: 10000
	});
	items.push(stamp);
	
	var specialThanks = Ti.UI.createView({
		backgroundImage: "/images/creditsSceneSpecialThanks.png",
		width: 286,
		height: 226,
		top: globals.platform.height,
		duration: 20000
	});
	items.push(specialThanks);
	
	var label3 = Ti.UI.createLabel({
		text: "Ciao Belli",
		font: globals.fonts.boldMediumSmall,
		color: "white",
		textAlign: "center",
		left: 10,
		top: globals.platform.height,
		width: globals.platform.width-20,
		touchEnabled:false,
		duration: 10000
	});
	items.push(label3);
	
	var label4 = Ti.UI.createLabel({
		text: "P.S. Find all the secrets!",
		font: globals.fonts.boldMediumSmall,
		color: "white",
		textAlign: "center",
		left: 10,
		top: globals.platform.height,
		width: globals.platform.width-20,
		touchEnabled:false,
		duration: 10000
	});
	items.push(label4);
	
	view.add(label1);
	view.add(label2);
	view.add(stamp);
	view.add(specialThanks);
	view.add(label3);
	view.add(label4);
	
	var duration = 15000;
	function AnimateItem(item){
		Animator.animate(item,{
			duration: duration,
			top: -item.rect.height
			//easing:Animator['BOUNCE_OUT']
		});
		currentItem++;
	}
	
	var currentItem = 0;
	setTimeout(function(){
		AnimateItem(items[currentItem]);
		var scalettaInterval = setInterval(function(){
			AnimateItem(items[currentItem]);
			if (currentItem >= items.length){
				clearInterval(scalettaInterval);
				setTimeout(function(){
					Animator.animate(view,{
						duration: 5000,
						opacity: 0,
					});
					 setTimeout(function(){
					 	view.fireEvent("finished");
					 },5000);
				},duration);	 
			}	 
		}, duration);
	},20);
	
	
	Animator.animate(view,{
		duration: 2000,
		opacity:1,
	});
	
	
	
	return view;	
}