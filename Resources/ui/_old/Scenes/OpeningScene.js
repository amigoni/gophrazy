exports.loadMe = function(){
	var DialogScene = require("/ui/Scenes/DialogScene");
	var textData = [];
	
	var dialogScene; 
	var crystalScene;
	var beamScene;
	var creditsScene;
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor: "transparent",
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:0,
	    fullscreen:true,
	    navBarHidden: true,
	    //transform: Ti.UI.create2DMatrix().translate(globals.platform.width,0)
	    opacity:1
	});
	
	
	dialogScene = DialogScene.loadMe("opening");
	dialogScene.addEventListener("close", function(){
		dialogScene.DisappearAnimation(function(){win.close();});
	});
	
	win.add(dialogScene);
	
	dialogScene.Open();
	
	
	return win;
};