exports.createView = function (data){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 44+50,
		separatorColor: "transparent",
		separatorStyle: false,
		dataIn: data,
		opacity: 0
	});
	
	
	data = [
		{name: "Leonardo Amigoni", signaturesCount: 12},
		{name: "Cristina Amigoni", signaturesCount: 11},
		{name: "Vittorio Amigoni", signaturesCount: 10},
		{name: "Angela Amigoni", signaturesCount: 9}
	];
	
	var totalSignatures = 0;
	var tableData = [];
	for (var i = 0; i < data.length; i ++){
		var row = CreateRow(data[i]);
		tableData.push(row);
		totalSignatures += data[i].signaturesCount;
	}
	
	tableData.push(CreateFooter());
	
	tableView.data = tableData;
	
	function CreateRow(rowData){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: 40,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		
		view.addEventListener("singletap", function(){
			//view.fireEvent("clicked",{rowData:rowData});
		});
		
		var nameLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.name,
			font: globals.fonts.bold15,
			color: globals.colors.darkBrown,
			textAlign: "left",
			width: globals.platform.width-70,
			height: 20,
			left: 10
		});
		
		var penIcon = Ti.UI.createView({
			backgroundImage: "/images/penButton.png",
			width: 15,
			height: 15,
			right: 35
		});
		
		var scoreLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.signaturesCount,
			font: globals.fonts.bold13,
			color: globals.colors.darkBrown,
			textAlign: "right",
			width: 25,
			height: 20,
			right: 10
		});
		
		
		view.add(nameLabel);
		view.add(penIcon);
		view.add(scoreLabel);
		view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1,bottom: 0}));
		
		
		return view;
	}
	
	
	function CreateFooter(){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: Ti.UI.SIZE,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
			touchEnabled: false
		});
		
		var signaturesLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: "Total Signatures: "+totalSignatures,
			font: globals.fonts.bold15,
			color: globals.colors.darkBrown ,
			textAlign: "right",
			width: Ti.UI.SIZE,
			height: 20,
			right: 10,
			top: 5
		});
		
		var playersLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: "Total Players: "+data.length,
			font: globals.fonts.bold15,
			color: globals.colors.darkBrown ,
			textAlign: "left",
			width: Ti.UI.SIZE,
			height: 20,
			left: 10,
			top: 5
		});
		
		
		view.add(signaturesLabel);
		view.add(playersLabel);
		
		
		return view;	
	}
	
	
	return tableView;
};