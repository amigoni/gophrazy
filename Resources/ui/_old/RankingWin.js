exports.loadWin = function() {
	var GroupsController = require("controller/GroupsController");
	var FriendsController = require("controller/FriendsController");
	var LeaderboardView = require("ui/FriendsWinElements/LeaderboardView");

	var leaderboardData = FriendsController.GetFriendsWithAppByScore();

	var view = Titanium.UI.createView({
		backgroundImage : "/images/beigeBg.png",
		width : globals.platform.width,
		height : globals.platform.height - globals.adsBannerSpace,
		//opacity: 0,
		bubbleParent : false,
		top : globals.platform.height,//-(globals.platform.height - globals.adsBannerSpace),
		zIndex: 1
	});

	view.addEventListener("closeWin", function() {
		Close();
	});

	view.addEventListener("openInvitePopUp", function() {
		globals.rootWin.OpenWindow("inviteWin");
	});

	var navBar = CreateNavBar();

	var leaderboardView = LeaderboardView.createView(leaderboardData);
	
	view.add(navBar);
	view.add(leaderboardView);
	

	function Open() {
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}

	function Close() {
		navBar.Close();
		DisappearAnimation();
	}

	function AppearAnimation() {
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}

	function DisappearAnimation() {
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}


	view.Open = Open;
	view.Close = Close;


	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: "#578392",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var inviteButton = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 44,
		left: 0
	});
	
	inviteButton.add(Ti.UI.createLabel({
		text: "invite",
		color: globals.colors.white,
		textAlign: "left",
		font: globals.fonts.boldMediumSmall,
		minimumFontSize: 10,
		left: 10
	}));
	
	Common.MakeButton(inviteButton);
	
	inviteButton.addEventListener("singletap", function(){
		setTimeout(function(){view.fireEvent("openInvitePopUp");},200);
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 34,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/podiumIcon.png",
		width: 37,
		height: 34
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/rankingSign.png",
		width: 110,
		height: 30,
		left: 3
	}));
	
	view.add(inviteButton);
	view.add(title);
	view.add(closeButton);
	
	function Close(){
		
	}
	
	view.Close = Close;
	
	
	return view;
};