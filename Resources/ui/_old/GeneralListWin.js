exports.loadWin = function(data){
	var LocalData = require("model/LocalData");
	
	var win = Titanium.UI.createWindow({
	    backgroundColor: globals.colors.background,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 0
	});
	
	var tableView = Ti.UI.createTableView({
		width: globals.platform.width,
		height:Ti.Platform.displayCaps.platformHeigth,
		backgroundColor: "transparent",
		top: 0
	});
	
	var tableData = [];
	for (var i = 0; i < data.length; i++){
		var row = CreateRow(data[i]);
		tableData.push(row);
	}
	
	tableView.data = tableData;
	
	win.add(tableView);	
	
	win.addEventListener("open", function(){
		var Common = require('ui/Common');
		win.animate(Common.appearAnimation());
	});
	
	
	return win;
};	


function CreateRow(data){
	var LocalData = require("model/LocalData");
	
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 50,
		backgroundColor: "transparent",
		data: data
	});
	
	row.addEventListener("singletap",function(e){
		LocalData.CreateShuffleBagOfPhrases({type:"author", name:data.name},true);
		var GameWin = require("ui/GameWin");
		var win = GameWin.loadWin(data);
		win.open();
	});
	
	var textLabel = Ti.UI.createLabel({
		text: data.name,
		color: globals.colors.text,
		//width: globals.platform.width-20,
		font: globals.fonts.words2,
		left: 10
	});
	
	var countLabel = Ti.UI.createLabel({
		text: data.completeCount+"/"+data.count,
		color: globals.colors.text,
		//width: globals.platform.width-20,
		font: globals.fonts.words2,
		right: 10
	});
	
	row.add(textLabel);
	row.add(countLabel);
	
	return row;
}
