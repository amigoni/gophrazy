exports.createView = function(teamMembers){
	var FriendsController = require("controller/FriendsController");
	var totalScore = 0;
	var tableData = [];
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44-50,
		top: 44+50,
		separatorColor: "transparent",
		separatorStyle: false,
		opacity: 0
		//left: type == "main" ? 0 : globals.platform.width,
	});
	
	tableView.addEventListener("kickOut", function(e){
		for(var i = 0; i < tableData.length; i++){
			if(tableData[i].SetEnableKickOut)tableData[i].SetEnableKickOut(false);
		}
	});
	
	
	function Refresh(e){
		teamMembers = e.teamMembers;
		totalScore = 0;
		
		for(var i = 0; i<teamMembers.length; i++){
			totalScore += teamMembers[i].score;
		}
		
		tableData = null;
		tableData = [];
		tableData.push(CreateHeaderRow());
		for(var i = 0; i < teamMembers.length; i++){
			var rank = i+1;
			//if (rank == 4)rank = 99;
			var row = CreateRow(teamMembers[i],rank);
			tableData.push(row);
		}
		
		tableData.push(CreateAddPeopleRow(5-teamMembers.length));
		tableData.push(CreateFooterRow());
		
		tableView.data = tableData;
	}
	
	function CreateHeaderRow(){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: 40,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		
		var label1 = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: "Team Score: "+totalScore,
			font: globals.fonts.bold25,
			minimumFontSize:10,
			color: globals.colors.darkBrown,
			textAlign: "center",
			width: Ti.UI.SIZE
		});
		
		function Refresh(totalScore){
			
		}
		
		view.add(label1);
		
		view.Refresh = Refresh;
		
		//Refresh(data.totalScore);
		
		return view;
	}
	
	
	function CreateRow(rowData,rank){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: 40,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		
		view.addEventListener("singletap", function(){
			view.fireEvent("clicked",{rowData:rowData});
		});
		
		var nameLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.name,
			font: globals.fonts.bold15,
			minimumFontSize:10,
			color: rowData.isMe == false ? globals.colors.darkBrown : globals.colors.mediumBrown,
			textAlign: "left",
			width: globals.platform.width-130,
			height: 20,
			left: 10
		});
		
		var scoreLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.score,
			font: globals.fonts.bold13,
			color:  rowData.isMe == false ? globals.colors.darkBrown : globals.colors.mediumBrown,
			textAlign: "right",
			width: 40,
			height: 20,
			right: 72
		});
		
		var kickOutButton = Ti.UI.createView({
			backgroundImage: "/images/kickOutButton.png",
			width: 64,
			height: 36, 
			right: 5
		});
		
		Common.MakeButton(kickOutButton);
		
		kickOutButton.addEventListener("singletap", function(e){
			FriendsController.RemoveFriendFromTeam(rowData);
			view.fireEvent("kickOut",{id: rowData.id});
			//alert("kickOut");
		});
		
		view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1,bottom: 0}));
		view.add(nameLabel);
		view.add(Ti.UI.createView({
			backgroundImage:"/images/phrasesIcon.png",
			width: 24,
			height: 24,
			right: 97
		}));
		if(rowData.isMe == false)view.add(kickOutButton);
		view.add(scoreLabel);
		
		function SetEnableKickOut(value){
			if (value == true){
				kickOutButton.touchEnabled = true;
				kickOutButton.opacity = 1;
			}
			else if (value == false){
				kickOutButton.touchEnabled = false;
				kickOutButton.opacity = 0.5;
			}
		}
		
		view.SetEnableKickOut = SetEnableKickOut;
		
		return view;
	}
	
	function CreateAddPeopleRow(numberOfPeopleLeft){
		var text;
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: 40,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		
		var label1 = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: text,
			font: globals.fonts.bold15,
			minimumFontSize:10,
			color: globals.colors.darkBrown,
			textAlign: "center",
			width: globals.platform.width,
			height: 40
		});
		
		label1.addEventListener("singletap", function(){
			setTimeout(function(){
				if(numberOfPeopleLeft > 0){view.fireEvent("openAddMemberPopUp");}
				else{alert("Your Team is full");}
			},200);
		});
		
		Common.MakeButton(label1);
		
		function Refresh(numberOfPeopleLeft){
			if( numberOfPeopleLeft > 0){
				text = "add "+numberOfPeopleLeft+" more ";
				if (numberOfPeopleLeft > 1) text += "people";
				else text += "person";
			}
			else{
				text = "Your team is full.";
			}

			label1.text = text;
		};
		
		view.add(label1);
		
		view.Refresh = Refresh;
		
		Refresh(numberOfPeopleLeft);
		
		return view;
	}
	
	
	function CreateFooterRow(){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: Ti.UI.SIZE,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		
		return view;
	}
	
	function Close(){
		Ti.App.addEventListener("updateTeam", Refresh);	
	}
	
	tableView.Close = Close;
	
	Ti.App.addEventListener("updateTeam", Refresh);
	
	Refresh({teamMembers:teamMembers});
	
	
	return tableView;
};