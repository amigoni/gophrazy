exports.loadWin = function(){
	var GroupsController = require("controller/GroupsController");
	var IssuesController = require("controller/IssuesController");
	var NavBar = require("ui/CollectionBookWinElements/NavBar");
	var MainTableView = require("ui/CollectionBookWinElements/MainTableView");
	var CollectionTableView = require("ui/CollectionBookWinElements/CollectionTableView");
	
	//var collectionBookData = GroupsController.GetCollectionBookData();
	var collectionBookData = IssuesController.GetIssuesData();
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight//-(globals.platform.actualHeight)
	});
	
	
	view.addEventListener("updateCurrentRow", function(e){
		mainTableView.UpdateCurrentCollection(e.rowData);
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	var navBar = NavBar.createView();
	
	var tableViewsController = CreateTableViewsController();
	
	tableViewsController.addEventListener("openGroup", function(e){
		var groupData = GroupsController.CreateGroup("collection","",e.rowData.$id);
		tableViewsController.Add(CollectionTableView.createView(groupData));
	});
	
	var mainTableView = MainTableView.createView("main", collectionBookData);
	tableViewsController.Add(mainTableView);
	
	view.add(navBar);
	view.add(tableViewsController);
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		navBar.Close();
		tableViewsController.Close();
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};



function CreateTableViewsController (){
	var controller = Ti.UI.createView({
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
	    top: 44
	});
	
	controller.addEventListener("back", function(e){
		Back();
	});
	
	var stack = [];
	
	function Add(newTableView){
		stack.push(newTableView);
		controller.add(newTableView);
		if(stack.length >1){
			for (var i = 0; i < stack.length; i++){
				stack[i].MoveLeft();
			}	
		}
	}
	
	function Back (){
		for (var i = 0; i < stack.length; i++){
			stack[i].MoveRight();
		}
		
		setTimeout(function(){
			var last = stack[stack.length-1];
			last.Close();
			controller.remove(last);
			stack.pop();
		}, 1000);
	}
	
	function Close(){
		for(var i = 0; i < stack.length; i++){
			stack[i].Close();
			Ti.API.info(stack[i]);
		}
		stack = null;
	}
	
	controller.Add = Add;
	controller.Back = Back;
	controller.Close = Close;
	
	
	return controller;
};