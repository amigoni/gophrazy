exports.createView = function(data){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 0,
		separatorColor: "transparent",
		separatorStyle: false,
		left: 0,
		//data: data ? data: null
		dataIn: data
	});
	
	
	var tableData = [];
	
	for (var i = 0; i < data.length; i++){
		var row = CreateFamilyRow(data[i]);
		row.addEventListener("clicked", function(e){
			tableView.fireEvent("openGroup", {rowData: e.rowData});
		});
		tableData.push(row);
	}
	
	tableView.data = tableData;
	
	
	
	function MoveLeft (){
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
	}
	
	function MoveRight (){
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left+globals.platform.width,
			easing:Animator['EXP_IN']
		}, function(){Close();});
	}
	
	function Close(){
		Ti.App.removeEventListener("updateMonsters", UpdateTableView);
	}
	
	function UpdateTableView(e){
		for (var i = 0; i < tableData.length; i++){
			if(tableData[i].rowData && tableData[i].rowData.eventID == e.data.eventID){
				tableData[i].Update(e.data);
			}
		}
	}
	
	
	tableView.MoveLeft = MoveLeft;
	tableView.MoveRight = MoveRight;
	tableView.Close = Close;
	
	Ti.App.addEventListener("updateMonsters", UpdateTableView);
	
	
	return tableView;
};


function CreateHeaderRow(name){
	var view = Ti.UI.createTableViewRow({
		//backgroundColor: "red",
		width: globals.platform.width,
		height: 30,
		selectionStyle: false
	});
	
	var label = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: name,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE,
		bottom: 0
	});
	
	view.add(label);
	
	return view;
}


function CreateFamilyRow(rowData){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 50,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		rowData: rowData
	});
	
	view.addEventListener("touchstart", function(){
		overlay.opacity = 0.3;
		setTimeout(function(){
			overlay.opacity = 0;
		},500);
	});
	
	view.addEventListener("singletap", function(e){
		view.fireEvent("openGroup",{rowData: rowData});
	});
	
	var image  = rowData.monstersData[0].image.toString();
	if (image.length == 1) image = "0"+image;
	var icon = Titanium.UI.createMaskedImage({
	    //mask : 'mask.png', // alpha mask
		image: "/images/monsters/"+image+".png",
	    //mode : Titanium.UI.iOS.BLEND_MODE_OVERLAY,
		width: 45,
		height: 45,
		left: 10,
		tint: "#CD4E3A"
	});
	
	var titleLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "The "+rowData.name,
		font: globals.fonts.bold25,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: globals.platform.width - 125,
		height: 30,
		left: 60,
		//top: 15
	});
	
	var countLabel = Ti.UI.createLabel({
		//backgroundColor: "blue",
		text: rowData.monstersUnlockedCount+"/"+rowData.monstersCount,
		font: globals.fonts.bold15,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		textAlign: "right",
		height: 20,
		right: 20,
		//top: 15
	});
	
	
	var arrow = Ti.UI.createView({
		backgroundColor: "green",
		width: 10,
		height: 20,
		right: 5
	});
	
	var overlay = Ti.UI.createView({backgroundColor: "black", opacity: 0, width: Ti.UI.FILL, height: Ti.UI.FILL});
	
	
	view.add(icon);
	view.add(arrow);
	view.add(titleLabel);
	view.add(countLabel);
	view.add(overlay);
	

	
	return view;
};
