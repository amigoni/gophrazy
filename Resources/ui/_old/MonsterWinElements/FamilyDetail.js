exports.createView = function(familyData){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height:Ti.Platform.displayCaps.platformHeigth-globals.adsBannerSpace-44,
		dataIn: familyData,
		left: globals.platform.width,
		layout: "vertical"
	});
	
	var navRow = CreateNavRow(familyData);
	
	var scrollView = Ti.UI.createScrollView({
		//backgroundColor: "green",
		width: view.width,
		height: view.height-navRow.height,
		top: 0,
		contentWidth: globals.platform.width-20,
		contentHeight: "auto"
	});
	
	var container = Ti.UI.createView({
		//backgroundColor: "red",
		width: 285,
		height: Ti.UI.SIZE,
		layout:"horizontal"
	});
	
	for (var i = 0; i < familyData.monstersData.length; i++){
		var monsterData = familyData.monstersData[i];
		container.add(CreateMonster(monsterData));
	}
	
	scrollView.add(container);
	
	view.add(navRow);
	view.add(scrollView);
	
	function MoveLeft (){
		Animator.animate(view,{
			duration: 500,
			left: view.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
	}
	
	function MoveRight (){
		Animator.animate(view,{
			duration: 500,
			left: view.left+globals.platform.width,
			easing:Animator['EXP_IN']
		}, function(){});
	}
	
	view.MoveLeft = MoveLeft;
	view.MoveRight = MoveRight;
	
	
	return view;
};


function CreateNavRow(data){
	var MainTableView = require("ui/EventsWinElements/MainTableView");
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: globals.platform.width,
		height: 80
	});
	
	var backButton = Ti.UI.createView({
		backgroundImage: "/images/backButton.png",
		width: 30,
		height: 26,
		left: 10,
		top: 20
	});
	
	Common.MakeButton(backButton);
	
	backButton.addEventListener("singletap", function(){
		setTimeout(function(){view.fireEvent("back");},200);
	});
	
	var titleLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "The "+data.name,
		font: globals.fonts.bold25,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: globals.platform.width - 115,
		height: 30,
		textAlign: "center",
		//left: 60,
		top: 15
	});
	
	var countLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: data.monstersUnlockedCount+"/"+data.monstersCount,
		font: globals.fonts.bold15,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		textAlign: "right",
		width: 40,
		height: 20,
		right: 10,
		top: 25
		//bottom: 10
	});
	
	
	view.add(backButton);
	view.add(titleLabel);
	view.add(countLabel);
	
	
	return view;
}


function CreateMonster(monsterData){
	var view = Ti.UI.createView({
		//backgroundColor: "blue",
		width: 95,
		height: 110,
		data: monsterData
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap", function(){
		setTimeout(function(){globals.rootWin.OpenWindow("monsterDetailWin", monsterData);},200);
	});
	
	
	
	var image = monsterData.image;
	if (image < 10) image = "0"+image;
	/*
	var icon = Ti.UI.createImageView({
		image: "/images/monsters/"+image+".png",
		width: 80,
		visible: !monsterData.locked
	});
	*/
	var icon = Titanium.UI.createMaskedImage({
	    //mask : 'mask.png', // alpha mask
		image: "/images/monsters/"+image+".png",
	    //mode : Titanium.UI.iOS.BLEND_MODE_OVERLAY,
		width: 80,
		height: 80,
		tint: "#CD4E3A",
		//visible: !monsterData.locked
	});
	
	var nameLabel = Ti.UI.createLabel({
		text: monsterData.name,
		font: globals.fonts.bold15,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		//width: globals.platform.width-45,
		height: 20,
		//left: 10,
		bottom: 0
	});
	
	view.add(icon);
	view.add(nameLabel);
	
	
	return view;
}

