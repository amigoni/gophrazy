exports.loadWin = function(monsterData){
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: -(globals.platform.actualHeight),
	  	zIndex: 101
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	var navBar = CreateNavBar(monsterData);
	
	var image = monsterData.image;
	if (image < 10) image = "0"+image;
	
	var centerContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var icon = Titanium.UI.createMaskedImage({
	    //mask : 'mask.png', // alpha mask
		image: "/images/monsters/"+image+".png",
	    //mode : Titanium.UI.iOS.BLEND_MODE_OVERLAY,
		width: 300,
		height: 300,
		tint: "#CD4E3A",
		//visible: !monsterData.locked
		//top: 60
	});
	
	centerContainer.add(icon);
	
	view.add(navBar);
	view.add(centerContainer);
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Animator.animate(view,{
			duration: 500,
			top: 0,
			easing:Animator['EXP_OUT']
		});},200);
	}
	
	function DisappearAnimation(){
		Animator.animate(view,{
			duration: 500,
			top: -(globals.platform.actualHeight),
			easing:Animator['EXP_IN']
		},function(){
			view.fireEvent("closed");
			}
		);
	}
	
	
	view.Open = Open;
	view.Close = Close;
	
	return view;
};


function CreateNavBar(monsterData){
	var view = Ti.UI.createView({
		//backgroundColor: "#CD4D39",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 60,
		height: 44,
		right: 0
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var titleLabel = Ti.UI.createLabel({
		text: monsterData.name,
		font: globals.fonts.bold35,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		width: globals.platform.width-100,
		height: 44,
		//left: 10,
		bottom: 0,
		textAlign: "center"
	});
	
	view.add(titleLabel);
	view.add(closeButton);
	
	
	return view;
};