exports.createView = function(){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 35,
		layout: "horizontal",
		top: 70
	});
	
	var featuredButton = CreateButton(0,"Latest", "latest");
	var browseButton = CreateButton(1,"Past", "past");
	var completeButton = CreateButton(2,"Complete", "complete");
	
	view.add(featuredButton);
	view.add(browseButton);
	view.add(completeButton);
	
	function CreateButton (i,name,eventName){
		var button = Ti.UI.createView({
			borderColor: globals.colors.darkBrown,
			backgroundColor: i == 0 ? "#D5C7A3": "transparent",
			borderWidth: 2,
			borderRadius: 4,
			width: 102,
			height: 30,
			bottom: 0,
			left: i > 0 ? 2 : 0,
			buttonID: "collectionBooWiButtonBar"+name+"Button"
		});
		
		button.addEventListener("singletap", function(){
			ResetAllButtons();
			this.backgroundColor = "#D5C7A3";
			view.fireEvent("clicked", {button: eventName});
		});
		
		Common.MakeButton(button);
		
		button.add(Ti.UI.createLabel({
			text: name,
			font: globals.fonts.bold25,
			minimumFontSize: 13,
			width: button.width-20,
			color: globals.colors.darkBrown,
			textAlign: "center"
		}));
		
		return button;
	}
	
	function ResetAllButtons(){
		featuredButton.backgroundColor = "transparent";
		browseButton.backgroundColor = "transparent";
		completeButton.backgroundColor = "transparent";
	}
	
	
	return view;
};