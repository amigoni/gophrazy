exports.createView = function(){
	var gameData = globals.gameData;
	
	var view = Ti.UI.createView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var moneyButton = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 25,
		layout:"horizontal",
		left: 5,
		buttonID: "collectionBookWinNavBarMoneyButton"
	});
	
	Common.MakeButton(moneyButton);
	
	moneyButton.addEventListener("singletap", function(){
		if(moneyButton.touchEnabled == true){
			globals.rootWin.OpenWindow("storePopUp", {type: "money" });
			//view.fireEvent("openStorePopUp",{kind:"money"});
		}
	});
	
	var moneyIcon = Ti.UI.createImageView({
		image: "/images/moneyIconSmall.png",
		//backgroundImage:"/images/hintIconSmall.png",
		//width: 24,
		//height: 24,
		left: 0
	});
	
	var moneyLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: gameData.money,//gameData.hints,
		color: globals.colors.darkBrown,
		textAlign: "left",
		font: globals.fonts.boldMediumSmall,
		minimumFontSize: 8,
		width: 40,
		left: 5
	});
	
	moneyButton.add(moneyIcon);
	moneyButton.add(moneyLabel);
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID: "collectionBookWinCloseButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 25,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/newspaperIcon.png",
		width: 23,
		height: 17
	}));
	
	title.add(Ti.UI.createImageView({
		image:"/images/thePuzzlePostSign.png",
		left: 5
	}));
	
	view.add(moneyButton);
	view.add(title);
	view.add(closeButton);
	
	function UpdateMoneyCollectionNavBar(e){
		moneyLabel.text = e.amount;
	}
	
	
	function Close(){
		Ti.App.removeEventListener("updateMoney", UpdateMoneyCollectionNavBar);
	}
	
	Ti.App.addEventListener("updateMoney", UpdateMoneyCollectionNavBar);
	
	view.Close = Close;
	
	
	return view;
};