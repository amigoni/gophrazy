exports.createView = function(type, data){
	var GroupsController = require("controller/GroupsController");
	var ButtonBar = require("ui/CollectionBookWinElements/ButtonBar");
	
	var view = Ti.UI.createView({
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
	    top: 0,
	    left: 0
	});
	
	var issuesTableView = CreateTableView(data, CreateHeaderRow);
	setTimeout(function(){issuesTableView.data[0].rows[1].ToggleOpen();},200);
	
	view.add(issuesTableView);
	
	
	function MoveLeft (){
		Animator.animate(view,{
			duration: 500,
			left: view.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
	}
	
	function MoveRight (){
		Animator.animate(view,{
			duration: 500,
			left: view.left+globals.platform.width,
			easing:Animator['EXP_IN']
		});
	}
	
	function UpdateCurrentCollection(rowData){
		issuesTableView.UpdateCurrentCollection(rowData);
	}
	
	function Close(){
		issuesTableView.Close();
		currentLabel = null;
		currentCollectionView = null;
		issuesLabel = null;
		issuesTableView = null;
	}
	
	view.MoveLeft = MoveLeft;
	view.MoveRight = MoveRight;
	view.UpdateCurrentCollection = UpdateCurrentCollection;
	view.Close = Close;
	
	///Mark all as not new
	GroupsController.MarkCollectionsAsViewed();
	
	
	return view;
};



function CreateTableView(data){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 0,
		separatorColor: "transparent",
		separatorStyle: false,
		//left: type == "main" ? 0 : globals.platform.width,
		//data: data ? data: null
		dataIn: data
	});
	
	function CreateHeader(){
		return CreateNewHeaderRow(data);
	}
	
	Common.MakeDynamicTableView(tableView, data.past, 20, exports.CreateIssueRow, CreateBottomRow, CreateHeader);
	
	function UpdateTableData(e){
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.rowData && row.rowData.name == e.data.name){
				row.Update(e.data);
			}
		}
	}
	
	function Close(){
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.Close) row.Close();
		}
	}
	
	function UpdateCurrentCollection(rowData){
		//currentCollectionView.Change(rowData);
		tableView.data[0].rows[0].UpdateCurrentCollection(rowData);
	}
	
	tableView.UpdateTableData = UpdateTableData;
	tableView.UpdateCurrentCollection = UpdateCurrentCollection;
	tableView.Close = Close;
	
	
	return tableView;
}


function CreateNewHeaderRow(data){
	var view = Ti.UI.createTableViewRow({
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		width: globals.platform.width,
		heigh: Ti.UI.SIZE
	});
	
	var currentLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "Current Collection",
		font: globals.fonts.bold12,
		//minimumFontSize: 12,
		color: globals.colors.mediumDarkBrown,
		width: Ti.UI.SIZE,
		top: 0
	});
	
	var currentCollectionView = exports.CreateGroupRow(data.lastCollection, true);
	currentCollectionView.top = 5;
	
	var issuesLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "Issues",
		font: globals.fonts.bold15,
		//minimumFontSize: 12,
		color: globals.colors.mediumDarkBrown,
		width: Ti.UI.SIZE,
		top: 50
	});
	
	view.add(currentLabel);
	view.add(currentCollectionView);
	view.add(issuesLabel);
	
	function UpdateCurrentCollection(rowData){
		currentCollectionView.Change(rowData);
	}
	
	function Close(){
		currentCollectionView.Close();
	}
	
	view.UpdateCurrentCollection = UpdateCurrentCollection;
	view.Close = Close;
	
	
	return view;
}


function CreateHeaderRow(name){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 30,
		selectionStyle: false
	});
	
	var label = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: name,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumDarkBrown,
		width: Ti.UI.SIZE
	});
	
	view.add(label);
	
	function Close(){
		label = null;
	}
	
	row.Close = Close;
	
	
	return view;
}


exports.CreateIssueRow = function (rowData,open){
	var GroupsController = require("controller/GroupsController");
	var collections = globals.col.groups.find({groupID:{$in: rowData.collectionsIDs}});
	
	var view = Ti.UI.createTableViewRow({
		//backgroundColor: "green",
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		rowData: rowData,
		buttonID: "collectionsIssueRow",
		open: open ? open : false
	});
	
	Common.MakeButton(view);

	var topContainer = Ti.UI.createView({
		width: view.width,
		height: 40,
		top: 0	
	});
	
	topContainer.addEventListener("singletap", function(){
		ToggleOpen();
	});
	
	var label1 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.name,
		font: globals.fonts.bold15,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: globals.platform.width - 120,
		height: 20,
		left: 10,
		top: 10
	});
	
	var label2 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.phrasesCompleteCount+"/"+rowData.phrasesCount,
		font: globals.fonts.bold12,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		textAlign: "right",
		height: 20,
		right: 70,
		top: 11
	});
	
	var label3 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.description,
		font: globals.fonts.regular9,
		minimumFontSize: 7,
		color: globals.colors.mediumBrown,
		width: globals.platform.width - 80,
		height: 12,
		left: 10,
		top: 25
	});
	
	var rightContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: topContainer.height,
		right: 0,
		touchEnabled: true
	});
	
	rightContainer.addEventListener("singletap", function(){
		if(Titanium.Network.online == true){
				var SmallSquarePopUp = require("ui/PopUps/SmallSquarePopUp");
				var buyConfirmationPopUp = SmallSquarePopUp.CreatePopUp("buyConfirmation");
				buyConfirmationPopUp.addEventListener("closed", function(){
					globals.rootWin.remove(buyConfirmationPopUp);
					buyConfirmationPopUp = null;
				});
				
				buyConfirmationPopUp.addEventListener("yes", function(e){
					if (globals.gameData.money >= rowData.cost){
						if(Titanium.Network.online == true){
							if (CheckIfAlreadyDownloaded() == false){
								var UpgradeController = require("controller/UpgradeController");
								UpgradeController.GetFile("https://s3.amazonaws.com/Wordhop/"+globals.systemData.currenti18n+"/", "issues_phrases_"+rowData.issueID+".txt", true, function(e1){
									var issuePhrases = JSON.parse(e1);
									for(var i = 0; i < issuePhrases.length; i++){
										GroupsController.UpgradeCollectionPhrases(issuePhrases[i].phrases, issuePhrases[i].groupID);
									}
									BuyIt();
								});
							}
							else BuyIt();
						}
						else NotConnectedPopUp();
					}
					else globals.rootWin.OpenWindow("storePopUp",{type: "money"});
					
					
					function BuyIt(){
						globals.storeController.BuyIssue(rowData.issueID);
						Update(rowData);
						
						for (var i = 0; i < collections.length; i++){
							collections[i].bought = true;
						}
						globals.col.groups.commit();
						
						for (var i = 0; i < bottomContainerRows.length; i++){
							bottomContainerRows[i].Update({data: collections[i]});
						}
						
						buyConfirmationPopUp.fireEvent("close");
					}
					
					function CheckIfAlreadyDownloaded(){
						var alreadyDownloaded = true;
						for (var i = 0; i < collections.length; i++){
							if (collections[i].phrasesCount != globals.col.phrases.count({groupID: collections[i].groupID})){
								alreadyDownloaded = false;
								break;
							}
						}
						return alreadyDownloaded;
					}
				});
				
				globals.rootWin.add(buyConfirmationPopUp);	
			}
			else NotConnectedPopUp();
			
			function NotConnectedPopUp(){
				alert("Upps.\nYou have to be connected to buy a collection.");
			}
	});
	
	var moneyIcon = Ti.UI.createImageView({
		image: "/images/moneyIconSmall.png",
		right: 38,
		visible: !rowData.bought,
		//top: 8
	});
	
	var cost = rowData.cost.toString();
	if (cost.length == 2) cost = "0"+cost;
	else if (cost.length == 1) cost = "00"+cost;
	
	var priceLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: cost,
		font: globals.fonts.bold13,
		minimumFontSize: 8,
		color: globals.colors.mediumBrown,
		height: 12,
		right: 5,
		width: 30,
		top: 7,
		textAlign:"right",
		//top: 20,
		visible: rowData.purchasable,
	});
	
	var saleSign = Ti.UI.createLabel({
		backgroundColor: "#FFBF00",
		borderColor: globals.colors.darkBrown,
		text: "-30%",
		font: globals.fonts.bold10,
		minimumFontSize: 8,
		color: globals.colors.darkBrown,
		height: 14,
		right: 5,
		width: 30,
		bottom: 5,
		textAlign:"center",
		//top: 20,
		visible: rowData.purchasable,
	});
	
	var newSign = Ti.UI.createView({
		backgroundImage: "/images/newSign.png",
		width: 25,
		height: 18,
		top: 0,
		left: 2,
		visible: rowData.isNew
	});
	
	var completeSign = Ti.UI.createView({
		backgroundImage: "/images/completeSign.png",
		width: 47,
		height: 12,
		right: 8,
		top: 1,
		visible: rowData.complete
	});
	
	var progressLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: (Math.ceil(rowData.collectionsCompleteCount/rowData.collectionsIDs.length*100))+"%",
		font: globals.fonts.bold13,
		minimumFontSize: 8,
		color: globals.colors.mediumBrown,
		height: 12,
		right: 5,
		width: 50,
		textAlign:"center",
		//top: 20,
		visible: !rowData.purchasable
	});
	
	if(rowData.purchasable == true) rightContainer.add(moneyIcon);
	if(rowData.purchasable == true) rightContainer.add(priceLabel);
	if(rowData.purchasable == true) rightContainer.add(saleSign);
	if(rowData.purchasable == false) rightContainer.add(progressLabel);
	
	//view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, top:0}));
	//view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, bottom:0}));
	//var overlay = Ti.UI.createView({backgroundColor: "black", opacity: 0, width: Ti.UI.FILL, height: Ti.UI.FILL});
	
	topContainer.add(label1);
	topContainer.add(label2);
	topContainer.add(label3);
	topContainer.add(rightContainer);
	topContainer.add(newSign);
	topContainer.add(completeSign);
	
	
	var bottomContainer = Ti.UI.createView({
		//backgroundColor:  globals.colors.white,
		//borderColor: globals.colors.darkBrown,
		width: view.width,
		height: Ti.UI.SIZE,
		top: 40,
		layout: "vertical",
		bubbleParent: false	
	});
	
	///Had to do this to make sure the event wasn't getting polluted. Weird.
	bottomContainer.addEventListener("openGroup",function(e){
		view.fireEvent("openGroup", {rowData: e.rowData});
	});
	
	bottomContainer.addEventListener("updateCurrentRow",function(e){
		view.fireEvent("updateCurrentRow", {rowData:e.rowData});
	});	
	
	var bottomContainerRows = [];
	
	view.add(topContainer);
	//view.add(bottomContainer);
	//view.add(overlay);
	
	function CreateLabel1Text(){
		var text = "";
		if (rowData.bought == false) text = rowData.name+" ("+rowData.collectionsCompleteCount+")";
		else text = rowData.name;
		
		return text;
	}
	
	function Update(e){
		//alert(e);
		var newRowData = e;//.data;
		if(newRowData.issueID == rowData.issueID){
			rowData = newRowData;
			label1.text = rowData.name;
			label3.text = rowData.description;
			label2.text = rowData.collectionsCompleteCount+"/"+rowData.collectionsIDs.length;
			progressLabel.text = (Math.ceil(rowData.collectionsCompleteCount/rowData.collectionsIDs.length*100))+"%";
			
			if (rowData.isNew == false) newSign.visible = false;
			else newSign.visible = true;
			
			if (rowData.complete == true){
				rightContainer.visible = false;
				completeSign.visible = true;
			}
			else{
				completeSign.visible = false;
				
				if (rowData.purchasable == false){
					rightContainer.touchEnabled = false;
					moneyIcon.visible = false;
					priceLabel.visible = false;
					saleSign.visible = false;
					progressLabel.visible = true;
				}
				else{
					rightContainer.touchEnabled = true;
					moneyIcon.visible = true;
					priceLabel.visible = true;
					saleSign.visible = true;
					progressLabel.visible = false;
				}
			}
		}
	}
	
	function ToggleOpen(){
		if (view.open == false){
			
			//bottomContainer.height = Ti.UI.SIZE;
			for(var i = 0; i < collections.length; i++){
				var row = exports.CreateGroupRow(collections[i], true);
				bottomContainer.add(row);
				bottomContainerRows.push(row);
			}
			bottomContainer.backgroundImage = "/images/rowExpansionBg.png";
			bottomContainer.backgroundTopCap = 20;
			view.add(bottomContainer);
			view.open = true;
		}
		else{
			//bottomContainer.height = 0;
			bottomContainer.backgroundImage = null;
			bottomContainer.backgroundTopCap = null;
			view.remove(bottomContainer);
			for(var i = 0; i < bottomContainerRows.length; i++){
				bottomContainer.remove(bottomContainerRows[i]);
			}
			bottomContainerRows = [];
			view.open = false;
		}
	}
	
	function Close(){
		Ti.App.removeEventListener("updateIssue", Update);
		label1 = null;
		label2 = null;
		label3 = null;
		moneyIcon = null;
		priceLabel = null;
		saleSign = null;
		progressLabel = null;
		rightContainer = null;
		newSign = null;
		completeSign = null;
		topContainer = null;
		bottomContainer = null;
		
		for (var i = 0; i < bottomContainerRows.length; i++){
			bottomContainerRows[i].Close();
			bottomContainerRows[i] = null;
		}
		bottomContainerRows = null;
	}
	
	view.Update = Update;
	view.ToggleOpen = ToggleOpen;
	view.Close = Close;
	
	Ti.App.addEventListener("updateIssue", Update);
	
	Update(rowData);
	
	
	return view;
};


exports.CreateGroupRow = function (rowData, isNormalView){	
	var GroupsController = require("controller/GroupsController");
	var view;
	if(isNormalView && isNormalView == true){
		view = Ti.UI.createView({
			//backgroundColor: globals.colors.white,
			width: globals.platform.width,
			height: 40,
			//selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
			rowData: rowData,
			buttonID: "collectionsBookRow",
			shrinkOnTouch: false,
			//bubbleParent: false
		});
	}
	else{
		view = Ti.UI.createTableViewRow({
			//backgroundColor: "green",
			width: globals.platform.width,
			height: 50,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
			rowData:rowData,
			buttonID: "collectionsBookRow"
		});
	}
	
	
	Common.MakeButton(view);
	
	/*
	view.addEventListener("touchstart", function(){
		overlay.opacity = 0.3;
		setTimeout(function(){
			overlay.opacity = 0;
		},500);
	});
	*/
	
	view.addEventListener("singletap", function(e){
		if(rowData.bought == true){
			view.fireEvent("openGroup",{rowData: rowData});
		}
		else if(rowData.bought == false){
			//view.fireEvent("openBuyConfirmation",{itemData:rowData});
			if(Titanium.Network.online == true){
				var SmallSquarePopUp = require("ui/PopUps/SmallSquarePopUp");
				var buyConfirmationPopUp = SmallSquarePopUp.CreatePopUp("buyConfirmation");
				buyConfirmationPopUp.addEventListener("closed", function(){
					globals.rootWin.remove(buyConfirmationPopUp);
					buyConfirmationPopUp = null;
				});
				
				buyConfirmationPopUp.addEventListener("yes", function(e){
					if (globals.gameData.money >= rowData.cost){
						if(Titanium.Network.online == true){
							if (globals.col.phrases.count({groupID:rowData.groupID}) != rowData.phrasesCount)
							{
								var UpgradeController = require("controller/UpgradeController");
								UpgradeController.GetFile("https://s3.amazonaws.com/Wordhop/"+globals.systemData.currenti18n+"/", "collection_"+rowData.name+".txt", true, function(e1){
									GroupsController.UpgradeCollectionPhrases(JSON.parse(e1),rowData.groupID);
									BuyIt();
								});
							}
							else BuyIt();
						}
						else NotConnectedPopUp();
						
					}
					else{
						globals.rootWin.OpenWindow("storePopUp",{type: "money" });
					}
					
					function BuyIt(){
						globals.storeController.BuyCollection(rowData.groupID);
						
						Update({data: rowData});
						buyConfirmationPopUp.fireEvent("close");
						//view.fireEvent("openGroup",{rowData: rowData});
						OpenFromPlay();
					}
				});
				
				globals.rootWin.add(buyConfirmationPopUp);	
			}
			else NotConnectedPopUp();
			
			
			
			function NotConnectedPopUp(){
				alert("Upps.\nYou have to be connected to buy a collection.");
			}
		}
	});
	
	
	var label1 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.name,
		font: globals.fonts.bold15,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: globals.platform.width - 120,
		height: 20,
		left: 10,
		top: 10
	});
	
	var label2 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.phrasesCompleteCount+"/"+rowData.phrasesCount,
		font: globals.fonts.bold12,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		textAlign: "right",
		height: 20,
		right: 70,
		top: 11
	});
	
	var label3 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.description,
		font: globals.fonts.regular9,
		minimumFontSize: 7,
		color: globals.colors.mediumBrown,
		width: globals.platform.width - 80,
		height: 12,
		left: 10,
		top: 25
	});
	
	var moneyIcon = Ti.UI.createImageView({
		image: "/images/moneyIconSmall.png",
		right: 38,
		visible: !rowData.bought,
		//top: 8
	});
	
	var cost = rowData.cost.toString();
	if (cost.length == 2) cost = "0"+cost;
	else if (cost.length == 1) cost = "00"+cost;
	
	var priceLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: cost,
		font: globals.fonts.bold13,
		minimumFontSize: 8,
		color: globals.colors.mediumBrown,
		height: 12,
		right: 5,
		width: 30,
		textAlign:"right",
		//top: 20,
		visible: !rowData.bought
	});	
	
	var newSign = Ti.UI.createView({
		backgroundImage: "/images/newSign.png",
		width: 25,
		height: 18,
		top: 0,
		left: 2,
		visible: rowData.isNew
	});
	
	var completeSign = Ti.UI.createView({
		backgroundImage: "/images/completeSign.png",
		width: 47,
		height: 12,
		right: 8,
		top: 1,
		visible: rowData.complete
	});
	
	var playButton = Ti.UI.createImageView({
		image: "/images/playButton.png",
		width: 50,
		//height: 40,
		//top: 13,
		right: 7,
		bottom: 2,
		visible: rowData.bought,
		bubbleParent: false,
		buttonID: "collectionsBookRowPlayButton"
	});
	
	Common.MakeButton(playButton);
	
	playButton.addEventListener("singletap", function(){
		OpenFromPlay();
	});
	
	
	//view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, top:0}));
	//view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, bottom:0}));
	//var overlay = Ti.UI.createView({backgroundColor: "black", opacity: 0, width: Ti.UI.FILL, height: Ti.UI.FILL});
	
	view.add(label1);
	view.add(label2);
	view.add(label3);
	view.add(moneyIcon);
	view.add(priceLabel);
	view.add(newSign);
	view.add(playButton);
	view.add(completeSign);
	//view.add(overlay);
	
	
	function OpenFromPlay(){
		setTimeout(function(){
			if(globals.gameData.energy > 0){ 
				Common.CreateButtonBlocker(true);
				globals.gameData.lastGroupID = rowData.groupID;
				globals.col.gameData.commit();
				view.fireEvent("updateCurrentRow", {rowData:rowData});
				
				var GroupsController = require("controller/GroupsController");
				var group = GroupsController.CreateGroup("collection","",rowData.$id);
				globals.rootWin.OpenWindow("gameWin", group);
			}	
			else {
				globals.rootWin.OpenWindow("smallSquarePopUp", "outOfEnergy");
			}	
		},200);
	}
	
	
	function CreateLabel1Text(){
		var text = "";
		if (rowData.bought == false) text = rowData.name+" ("+rowData.phrasesCount+")";
		else text = rowData.name;
		
		return text;
	}
	
	function Update(e){
		var newRowData = e.data;
		if(newRowData.groupID == rowData.groupID){
			rowData = newRowData;
			label1.text = rowData.name;
			label3.text = rowData.description;
			label2.text = rowData.phrasesCompleteCount+"/"+rowData.phrasesCount;
			
			if (rowData.isNew == false) newSign.visible = false;
			else newSign.visible = true;
			
			if (rowData.complete == true){
				//playButton.visible = false;
				completeSign.visible = true;
			}
			else{
				playButton.visible = true;
				completeSign.visible = false;
				
				if (rowData.bought == true){
					playButton.visible = true;
					moneyIcon.visible = false;
					priceLabel.visible = false;
				}
				else{
					playButton.visible = false;
					moneyIcon.visible = true;
					priceLabel.visible = true;
				}
			}
		}
	}
	
	function Change(newRowData){
		rowData = newRowData;
		Update({data: rowData});
	}
	
	function Close(){
		Ti.App.removeEventListener("updateCollection", Update);
		
		label1 = null;
		label2 = null;
		label3 = null;
		moneyIcon = null;
		newSign = null;
		playButton = null;
		completeSign = null;
	}
	
	view.Update = Update;
	view.Change = Change;
	view.Close = Close;
	
	Ti.App.addEventListener("updateCollection", Update);
	
	Update({data: rowData});
	
	
	return view;
};


function CreateBottomRow(nextRowToLoad, totalRows){
	var text;
	if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
	else text = "("+nextRowToLoad+"/"+totalRows+")";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		height: 55,
		selectionStyle: false
	});
	
	view.addEventListener("singletap", function(e){
		view.fireEvent("more");
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var bg = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE
	});
	
	view.add(label);
	view.add(bg);
	
	function Update(nextRowToLoad, totalRows){
		if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
		else text = "("+nextRowToLoad+"/"+totalRows+")";
		label.text = text;
	}
	
	function Close(){
		bg = null;
		label = null;
	}
	
	view.Update = Update;
	view.Close = Close;
	
	
	return view;
};