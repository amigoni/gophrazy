exports.createView = function(groupData,loadingIncrement,emptyText){
	var tableData = [];
	
	var tableView = Ti.UI.createTableView({
		width: globals.platform.width,
		height:Ti.Platform.displayCaps.platformHeigth-globals.adsBannerSpace-44,
		backgroundColor: "transparent",
		separatorStyle: false,
		top: 0,
		left: globals.platform.width,
		updating: false,
		lastDistance: 0,
		tableData: [],
		phrases: groupData.phrases,
		currentLastItemNumber: -1,
		dataIn: groupData
	});
	
	
	/*
	var loadingRow = Ti.UI.createTableViewRow({title:"Loading...", color:"white"});
	var updating = false;
	tableView.BeginUpdate = function(){
		updating = true;
		//navActInd.show();
		tableView.appendRow(loadingRow);
		tableView.loadingRowPosition = tableView.currentLastItemNumber+1;
		
		setTimeout(function(){tableView.UpdateData();},100);
	};
	
	tableView.UpdateData = function(){
		//Ti.API.info("Loading: "+tableView.loadingRowPosition);
		if (tableView.currentLastItemNumber < phrases.length-1){
			Ti.API.info("Ciao")
			var newLastItemNumber = tableView.currentLastItemNumber+loadingIncrement;
			if (newLastItemNumber >= tableView.phrases.length) newLastItemNumber = phrases.length - 1;
			
			for (var i = tableView.currentLastItemNumber+1; i <= newLastItemNumber ; i++)
			{
				Ti.API.info("Ciao")
				var row = CreatePhraseRow(tableView.phrases[i]);
				row.addEventListener("singletap", function(e){
					tableView.lastTouchedRowIndex = i-1;
				});
				
				tableView.appendRow(row);
			}
			tableView.currentLastItemNumber = newLastItemNumber;
			//tableView.deleteRow(tableView.loadingRowPosition,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
			
			
			//Ti.API.info("Last: "+tableView.currentLastItemNumber);	
			//tableView.scrollToIndex(tableView.currentLastItemNumber-loadingIncrement-1,{animated:true,position:Ti.UI.iPhone.TableViewScrollPosition.BOTTOM});	
		}
		
		updating = false;
	};
	
	tableView.addEventListener('scroll',function(e){
		var offset = e.contentOffset.y;
		var height = e.size.height;
		var total = offset + height;
		var theEnd = e.contentSize.height;
		var distance = theEnd - total;
		// going down is the only time we dynamically load,
		// going up we can safely ignore -- note here that
		// the values will be negative so we do the opposite
		if (distance < tableView.lastDistance)
		{
			// adjust the % of rows scrolled before we decide to start fetching
			var nearEnd = theEnd * 0.75;
			if (updating == false && (total >= nearEnd))
			{
				//tableView.BeginUpdate();
				updating = true;
				setTimeout(function(){tableView.UpdateData();},500);
			}
		}
		tableView.lastDistance = distance;
	});
	
	function RemoveRow(e){
		Ti.API.info(e);
		if (e.isFavoriteNow == false) tableView.deleteRow(tableView.lastTouchedRowIndex,{animationStyle:Titanium.UI.iPhone.RowAnimationStyle.NONE});
	};
	
	//Ti.App.addEventListener("favoriteCount", RemoveRow);
	*/
	tableView.footerView = Ti.UI.createView({height: 1});
	
	function UpdateTableView(){
		tableData = [];
		tableData.push(CreateNavRow("group",groupData));
		var completeCount = 0;
		for (var i = 0; i < groupData.phrases.length; i++){
			if(groupData.phrases[i].complete == true){
				completeCount++;
				var row = CreatePhraseRow(groupData.phrases[i]);
				row.addEventListener("singletap", function(e){
					tableView.lastTouchedRowIndex = i-1;
				});
				
				tableData.push(row);
			}
		}
		if (completeCount == 0) tableData.push(CreateNoPhrasesRow());
		tableView.data = tableData;
	}
	
	function MoveLeft (){
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
	}
	
	function MoveRight (){
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left+globals.platform.width,
			easing:Animator['EXP_IN']
		}, function(){Close();});
	}
	
	function Close(){
		//tableView.fireEvent("closed");
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.Close) row.Close();
		}
		tableData = null;
		Ti.App.removeEventListener("updateCollection", UpdateTableView);
	}
	
	tableView.MoveLeft = MoveLeft;
	tableView.MoveRight = MoveRight;
	tableView.UpdateTableView = UpdateTableView;
	tableView.Close = Close;
	
	//tableView.UpdateData();
	
	Ti.App.addEventListener("updateCollection", UpdateTableView);
	
	tableView.UpdateTableView();
	
	
	return tableView;
};


function CreateNavRow(type,data){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 40,
		selectionStyle: false
	});
	
	var backButton = Ti.UI.createView({
		width: 50,
		height: 40,
		left: 0
	}); 
	
	backButton.add(Ti.UI.createView({
		backgroundImage: "/images/backButton.png",
		width: 30,
		height: 26,
		left: 10
	}));
	
	Common.MakeButton(backButton);
	
	backButton.addEventListener("singletap", function(){
		setTimeout(function(){view.fireEvent("back");},200);
	});
	

	var imagePath;
	var nameSign;
	var playButton;
	var completeSign = Ti.UI.createImageView({
		image: "/images/completeSign.png",
		top: 15,
		visible: false
	});
	
	if(type == "category"){
		if(data.name == "Topics") imagePath = "/images/topicsSign.png";
		else if(data.name == "Authors") imagePath = "/images/authorsSign.png";
		else if(data.name == "Fortune Cookies") imagePath = "/images/fortuneCookiesSign.png";
		else if(data.name == "Movie Lines") imagePath = "/images/movieLinesSign.png";
		else if(data.name == "Song Lyrics") imagePath = "/images/songLyricsSign.png";
		else if(data.name == "Misc") imagePath = "/images/miscSign.png";
		
		nameSign = Ti.UI.createImageView({
			image: imagePath
		});
	}
	else if (type == "group"){
		playButton = Ti.UI.createView({
			backgroundImage: "/images/playButton.png",
			width: 55,
			height: 29
		});
		
		playButton.addEventListener("singletap", function(){
			setTimeout(function(){
				if(globals.gameData.energy > 0){ 
					Common.CreateButtonBlocker(true);
					globals.gameData.lastGroupID = data.groupID;
					globals.col.gameData.commit();
					view.fireEvent("updateCurrentRow", {rowData:data});
					
					var GroupsController = require("controller/GroupsController");
					data = GroupsController.CreateGroup("collection","",data.$id);
					globals.rootWin.OpenWindow("gameWin", data);
				}	
				else {
					globals.rootWin.OpenWindow("smallSquarePopUp", "outOfEnergy");
				}	
			},200);
		});
		
		Common.MakeButton(playButton);
	}
	
	var countLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: data.phrasesCompleteCount+"/"+data.phrasesCount,
		font: globals.fonts.bold15,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		textAlign: "right",
		width: 40,
		height: 20,
		right: 10,
		//bottom: 10
	});
	
	
	view.add(backButton);
	if(type == "category") view.add(nameSign);
	if(type == "group") {
		view.add(playButton);
		view.add(completeSign);
	}
	view.add(countLabel);
	
	function UpdateNavRow(e){
		countLabel.text = e.phrasesCompleteCount+"/"+data.phrasesCount;
		if (e.phrasesCompleteCount >= data.phrasesCount){
			completeSign.visible = true;
			playButton.visible = false;
		}
		else{
			completeSign.visible = false;
			playButton.visible = true;
		}
	}
	
	
	function Close(){
		Ti.App.removeEventListener("updateCollection", UpdateNavRow);
		backButton = null;
		completeSign = null;
		playButton = null;
		countLabel = null;
	}
	
	view.UpdateNavRow = UpdateNavRow;
	view.Close = Close;
	
	Ti.App.addEventListener("updateCollection", UpdateNavRow);
	
	UpdateNavRow(data);
	
	
	return view;
}


function CreatePhraseRow(rowData){
	var RowButtonBar = require("ui/Common/RowButtonBar");
	var author = rowData.author != "" ? rowData.author : "Anonymous";
	
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		rowData: rowData
	});
	
	row.addEventListener("singletap", function(e){
		var rowButtonBar = RowButtonBar.createView({shareButton: true, sendButton: true});
		rowButtonBar.addEventListener("share", function(){
			Common.shareMenu({picture: Common.CreateSharePhraseImage(rowData.text, author).toImage()});
		});
		
		rowButtonBar.addEventListener("send", function(){
			globals.rootWin.OpenWindow("friendPickerMessagesPopUp", {text: rowData.text, author: author});
		});
		
		row.add(rowButtonBar);
		container.ExitAnimation();
		rowButtonBar.EnterAnimation();
		
		setTimeout(function(){
			rowButtonBar.ExitAnimation();
			container.EnterAnimation();
			row.remove(rowButtonBar);
			rowButtonBar = null;
		},3000);
	});
	
	var container = Ti.UI.createView({
		width: row.width,
		height: row.height,
		layout: 'vertical'
	});
	
	container.ExitAnimation = function(){
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			easing: Animator["EXP_OUT"]
		});
	};
	
	container.EnterAnimation = function(){
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			easing: Animator["EXP_OUT"]
		});
	};
	
	var resultLabel = Ti.UI.createLabel({
		text: '"'+ crypt.decrypt(rowData.text)+'"',
		width: globals.platform.width-20,
		font: globals.fonts.words2,
		color: globals.colors.darkBrown,
		textAlign: "center",
		top: 20
	});
	
	var authorLabel = Ti.UI.createLabel({
		text: "- "+author,
		//width: ,
		font: globals.fonts.regular1,
		color: globals.colors.darkBrown,
		textAlign: "right",
		top: 2,
		right: 20
	});
		
	container.add(resultLabel);
	container.add(authorLabel);
	container.add(Ti.UI.createView({height:10}));

	row.add(container);
	
	function Close(){
		author = null;
		container = null;
		resultLabel = null;
		authorLabel = null;	
	}
	
	row.Close = Close;
	
	
	return row;
};


function CreateNoPhrasesRow (){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 60,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		layout:"vertical"
	});
	
	var label = Ti.UI.createLabel({
		text: 'No phrases completed.\nCompleted phrases will show here.',
		width: globals.platform.width-20,
		font: globals.fonts.bold13,
		color: globals.colors.mediumBrown,
		textAlign: "center",
	});
	
	row.add(label);
	
	function Close(){
		label = null;
	}
	
	row.Close = Close;
	
	return row;
}
