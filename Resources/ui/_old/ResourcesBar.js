exports.createView = function(type){
	var LocalData = require("model/LocalData");
	var gameData = globals.gameData;
	var levelData = LocalData.GetCurrentUserLevelDataForPoints(gameData.phrasesCompleteCountNoCookies);
	
	var view = Ti.UI.createView({
		//backgroundColor: "green",
		width: Ti.UI.SIZE,
		height: 30,
		left: 5,
		top: -50
		//top: type == "light" ? 2 : -50,
		//opacity: type == "light" ? 0 : 1
	});
	
	var container = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: 30,
		top: 0,
		layout:"horizontal"
	});
	
	var levelButton = CreateLevelButton(levelData,type);
	
	var phrasesButton = CreateButton("/images/phrasesIcon.png", gameData.phrasesCompleteCountNoCookies);
	phrasesButton.addEventListener("singletap", function(){
		//view.fireEvent("openStorePopUp",{kind:"energy"});
	});
	
	var moreLabel = Ti.UI.createLabel({
		text: "",//gameData.hints,
		color: type == "dark" ? globals.colors.textOpposite : "white",
		textAlign: "left",
		font: globals.fonts.regular11,
		//minimumFontSize: 10,
		//width: 35,
		left: 115,
		bottom: -2,
		opacity: 0,
	});
	
	container.add(levelButton);
	container.add(phrasesButton);
	view.add(container);
	view.add(moreLabel);
	
	
	////Functions
	function CreateButton(iconPath,value){
		var buttonID;
		
		if (iconPath.indexOf("phrases") > -1) buttonID= "resourceBarPhrasesButton";	
		
		var button = Ti.UI.createView({
			//backgroundColor: "red",
			width: Ti.UI.SIZE,
			height: 23,
			layout:"horizontal",
			left: 5,
			buttonID : buttonID,
			touchEnabled: true
		});
		
		Common.MakeButton(button);
		
		var icon = Ti.UI.createImageView({
			//backgroundColor: "green",
			image: iconPath,
			//backgroundImage:"/images/hintIconSmall.png",
			//width: 24,
			height: 22,
			left: 0
		});
		
		var label = Ti.UI.createLabel({
			//backgroundColor: "blue",
			text: ParseValue(value),//gameData.hints,
			color: type == "dark" ? globals.colors.textOpposite : "white",
			textAlign: "center",
			font: globals.fonts.boldMediumSmall,
			minimumFontSize: 6,
			width: 32,
			height: 20,
			right: 0
		});
		
		button.add(icon);
		button.add(label);
		
		button.label = label;
		button.icon = icon;
		
		
		return button;
	}
	
	function GetTimeLeftString(){
		var rechargeTime = new moment(gameData.startRechargingTime+gameData.rechargeInterval);
		var now = clock.getNow();
		var diff = new moment.duration(rechargeTime.diff(now));
		var minutes = diff.minutes();
		if (minutes < 10) minutes = "0"+minutes;
		var seconds = diff.seconds();
		if (seconds < 10) seconds = "0"+seconds;
		var text = minutes+":"+seconds;
		
		return text;
	}
	
	function UpdateMoney(e){
		moneyButton.label.text = ParseValue(e.amount);
	}
	
	function UpdateCount(e){
		phrasesButton.label.text = ParseValue(e.value);
		levelData = LocalData.GetCurrentUserLevelDataForPoints(gameData.phrasesCompleteCountNoCookies);
		levelButton.Update(levelData,type);
	}
	
	function ParseValue (value){
		var labelText = value;
		if (value < 10) labelText = "00"+value;
		else if (value < 100 && value >=10) labelText = "0"+value;
		return labelText;
	}
	
	function Close(){
		Ti.App.removeEventListener("updatePhrasesCompleteCount", UpdateCount);
		Ti.App.removeEventListener("networkBarAction", ResourcesBarNetworkBarAction);
		phrasesButton.Close();
		levelButton.Close();
	}
	
	function GoDown(){
		Animator.animate(view,{
			duration: globals.style.animationSpeeds.speed1,
			top: 2,
			easing: Animator["EXP_OUT"]
		});
	}
	
	function GoUp(){
		Animator.animate(view,{
			duration: globals.style.animationSpeeds.speed1,
			top: -50,
			easing: Animator["EXP_IN"]
		});
	}
	
	function ResourcesBarNetworkBarAction(e){
		if(e.isDown == true){
			Animator.animate(view,{
				duration: 500,
				opacity: 0,
			});
		}
		else{	
			Animator.animate(view,{
				duration: 500,
				opacity: 1,
			});
		}
	}
	
	///
	
	view.Close = Close;
	view.GoDown = GoDown;
	view.GoUp = GoUp;
	view.ResourcesBarNetworkBarAction = ResourcesBarNetworkBarAction;
	
	Ti.App.addEventListener("updatePhrasesCompleteCount", UpdateCount);
	Ti.App.addEventListener("networkBarAction", ResourcesBarNetworkBarAction);
	

	///Execution
	if (type == "light"){
		
	}
	else{
		setTimeout(function(){
			GoDown();
		}, 2000);
	}
	
	var timeInterval = null;
	timeInterval = setInterval(function(){
		if (globals.gameData.energy < 5){
			moreLabel.opacity = 1;
		}
		else{
			moreLabel.opacity = 0;
		}
		
		moreLabel.text = GetTimeLeftString();
	},1000);
	///
	
	return view;
};


function CreateLevelButton(levelData,type){
	var button = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: 30,
		//layout:"horizontal",
		left: 5,
		buttonID: "resourceBarLevelButton"
	});
	
	button.addEventListener("singletap", function(){
		if(label2.visible == false){
			label2.visible = true;
			bar.visible = false;
		}
		else {
			label2.visible = false;
			bar.visible = true;
		}
	});
	
	Common.MakeButton(button);
	
	var label = Ti.UI.createLabel({
		//backgroundColor: "blue",
		text: "Lv."+levelData.level,//gameData.hints,
		color: type == "dark" ? globals.colors.textOpposite : "white",
		textAlign: "center",
		font: globals.fonts.boldMediumSmall,
		minimumFontSize: 10,
		width: 32,
		height: 20,
		right: 0
	});
	
	var bar = Ti.UI.createView({
		borderColor: "#333333",
		backgroundColor: "white",
		borderWidth: 1,
		borderRadius: 3,
		width: 37,
		height: 6,
		bottom: 0
	});
	
	var filling = Ti.UI.createView({
		backgroundColor: "#CD4E3A",
		borderRadius: 2,
		width: 20,
		height: 4,
		left: 1
	});
	
	var label2 = Ti.UI.createLabel({
		//backgroundColor: "blue",
		text: globals.gameData.phrasesCompleteCountNoCookies+"/"+levelData.end,//gameData.hints,
		color: type == "dark" ? globals.colors.textOpposite : "white",
		textAlign: "center",
		font: globals.fonts.boldXS8,
		minimumFontSize: 6,
		bottom: 0,
		visible: false
	});
	
	bar.add(filling);
	
	button.add(label);
	button.add(bar);
	button.add(label2);
	
	
	function Update (levelData){
		if (levelData.fraction > 1) levelData.fraction = 1;
		filling.width = 37*levelData.fraction;
		label.text = "Lv."+levelData.level;
		label2.text = (globals.gameData.phrasesCompleteCountNoCookies-levelData.start)+"/"+(levelData.end-levelData.start+1);
	}
	
	button.Update = Update;
	
	Update(levelData);
	
	return button;
}
