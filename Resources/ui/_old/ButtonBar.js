exports.createView = function(){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		top: 50
	});
	
	var leaderboardButton = Ti.UI.createView({
		borderColor: globals.colors.darkBrown,
		backgroundColor: "#D5C7A3",
		borderWidth: 2,
		borderRadius: 4,
		width: 102,
		height: 30
	});
	
	leaderboardButton.addEventListener("singletap", function(){
		ResetAllButtons();
		this.backgroundColor = "#D5C7A3";
		view.fireEvent("clicked", {button: "leaderboard"});
	});
	
	
	Common.MakeButton(leaderboardButton);
	
	leaderboardButton.add(Ti.UI.createView({
		backgroundImage:"/images/phrasesIcon.png",
		width: 24,
		height: 24,
		left: 4
	}));
	
	leaderboardButton.add(Ti.UI.createLabel({
		text: "Leaderboard",
		font: globals.fonts.bold10,
		width: 70,
		color: globals.colors.darkBrown,
		left: 30
	}));
	
	
	var signaturesButton = Ti.UI.createView({
		borderColor: globals.colors.darkBrown,
		borderWidth: 2,
		borderRadius: 4,
		width: 100,
		height: 30,
		left: 3
	});
	
	signaturesButton.addEventListener("singletap", function(){
		ResetAllButtons();
		this.backgroundColor = "#D5C7A3";
		view.fireEvent("clicked", {button: "signatures"});
	});
	
	
	Common.MakeButton(signaturesButton);
	
	signaturesButton.add(Ti.UI.createView({
		backgroundImage: "/images/penButton.png",
		width: 15,
		height: 15,
		left: 10
	}));
	
	signaturesButton.add(Ti.UI.createLabel({
		text: "Signatures",
		font: globals.fonts.bold12,
		width: 70,
		color: globals.colors.darkBrown,
		left: 30
	}));
	
	
	var teamButton = Ti.UI.createView({
		borderColor: globals.colors.darkBrown,
		borderWidth: 2,
		borderRadius: 4,
		width: 100,
		height: 30,
		left: 3
	});
	
	teamButton.addEventListener("singletap", function(){
		ResetAllButtons();
		this.backgroundColor = "#D5C7A3";
		view.fireEvent("clicked", {button: "team"});
	});
	
	
	Common.MakeButton(teamButton);
	
	teamButton.add(Ti.UI.createView({
		backgroundImage: "/images/friendsIconSmall.png",
		width: 26,
		height: 24,
		left: 10
	}));
	
	teamButton.add(Ti.UI.createLabel({
		text: "Your\nTeam",
		font: globals.fonts.bold12,
		textAlign: "center",
		width: 70,
		color: globals.colors.darkBrown,
		left: 35
	}));
	
	function ResetAllButtons(){
		leaderboardButton.backgroundColor = "transparent";
		signaturesButton.backgroundColor = "transparent";
		teamButton.backgroundColor = "transparent";
	}
	
	
	view.add(leaderboardButton);
	view.add(signaturesButton);
	view.add(teamButton);	
	
	return view;
};