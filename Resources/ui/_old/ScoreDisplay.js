exports.CreateScoreDisplay = function(data){
	var RandomPlus = require("/lib/RandomPlus");
	
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: globals.platform.width,
		height: 80,
		left: -globals.platform.width,
		bottom: 20
	});
	
	/*
	var randomNumber = RandomPlus.RandomRange(0,5);
	
	var imageName;
	if (randomNumber == 0) imageName = "/images/greatSuccessLabel.png";
	else if (randomNumber == 1) imageName = "/images/geniusLabel.png";
	else if (randomNumber == 2) imageName = "/images/veryNiceLabel.png";
	else if (randomNumber == 3) imageName = "/images/ohYeahLabel.png";
	
	var titleLabel = Ti.UI.createImageView({
		image:imageName
	});
	
	view.add(titleLabel);
	*/
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 80,
		layout: "horizontal"
	});
	//container.add(exports.CreateRewardContainer("phrases",data.phrasesIncrement,data.phrasesCount));
	//container.add(Ti.UI.createView({height: 10,width: 50}));
	container.add(exports.CreateRewardContainer("money",data.moneyIncrement,data.moneyCount));
	view.add(container);
	
	container.addEventListener("singletap", function(){
		clearTimeout(finishTimeout);
		//DisappearAnimation();
		view.fireEvent("completedAnimation");
	});
	
	
	var finishTimeout; 
	function AppearAnimation(){
		setTimeout(function(){
			Animator.animate(view,
				{
					duration: globals.style.animationSpeeds.speed1,
					left: 0,
					easing: Animator["EXP_OUT"]
				},
				function(){
					finishTimeout = setTimeout(
						function(){
							DisappearAnimation();
						}
						, 1000
					);
				}	
			);
		},500);
	};

	function DisappearAnimation(){
		Animator.animate(view,
			{
				duration: globals.style.animationSpeeds.speed1,
				left: globals.platform.width,
				easing: Animator["EXP_IN"]
			},
			function(){view.fireEvent("completedAnimation");}
		);
	};
	
	function Close(){
		container = null;
	};
	
	view.AppearAnimation = AppearAnimation;
	view.DisappearAnimation = DisappearAnimation;
	view.Close = Close;
	
	
	return view;
};


exports.CreateRewardContainer = function(type,incrementValue,totalValue){
	var bigIcon;
	var smallIconImage;
	
	if (type == "money"){
		bigIcon = Ti.UI.createImageView({image:"/images/moneyIconMed.png"});
		smallIconImage = "/images/moneyIconSmall.png";
	}
	else if (type == "energy"){
		bigIcon = Ti.UI.createImageView({image:"/images/energyIconMed.png"});
		smallIconImage = "/images/energyIconSmall.png";
	}
	else if (type == "hints"){
		bigIcon = Ti.UI.createImageView({image:"/images/hintIconMedNoShadow.png"});
		smallIconImage = "/images/hintIconSmall.png";
	}
	else if (type == "phrases"){
		bigIcon = PhrasesIcon();
		smallIconImage = "/images/phrasesIcon.png";
	}
	
	
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var topContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 40,
		layout: "horizontal"
	});
	
	var incrementLabel = Ti.UI.createLabel({
		text: "+"+incrementValue,
		color: globals.colors.buttonTextMenus,
		textAlign: "left",
		font: globals.fonts.boldMedium31,
		left: 5
	});
	
	topContainer.add(bigIcon);
	topContainer.add(incrementLabel);
	
	var bottomContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 25,
		layout:"horizontal",
		top: 0
	});
	
	var icon = Ti.UI.createImageView({
		image: smallIconImage,
		//backgroundImage:"/images/hintIconSmall.png",
		//width: 24,
		//height: 24,
		//left: 5
	});
	
	var label = Ti.UI.createLabel({
		text: totalValue,//gameData.hints,
		color: globals.colors.buttonTextMenus,
		textAlign: "left",
		font: globals.fonts.boldMediumSmall,
		//minimumFontSize: 10,
		left: 2
	});
	
	bottomContainer.add(icon);
	bottomContainer.add(label);
	
	view.add(topContainer);
	//view.add(bottomContainer);
	
	return view;
};


function PhrasesIcon(){
	var phrasesIcon = Ti.UI.createView({
		backgroundColor: "#578392",
		borderColor: globals.colors.borderLine2,
		borderWidth: 2,
		borderRadius: 6,
		width: 82,
		height: 40,
		//left: -500,
	});
	
	var label = Ti.UI.createLabel({
		text: "Phrases",
		font: globals.fonts.words1,
		shadowOffset:{x:1,y:1},
		shadowColor:"black",
		left: 7,
		height: 40,
		color: globals.colors.wordsInBoxesUnselected,
		touchEnabled:false
	});
	
	phrasesIcon.add(label);
	
	return phrasesIcon;
}
