exports.loadWin = function(helpRequests){
	var Common = require('ui/Common');
	var LocalData = require("model/LocalData");
	var ServerData = require("model/ServerData");
	var WMessagesController = require("controller/WMessagesController");
	
	var gameData = LocalData.GetGameData();
	
	var data = WMessagesController.GetAllWMessages();
	
	var helpRequests = helpRequests;
	var inboxPhrasesData = [];
	var sentPhrasesData = [];
	
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor: globals.colors.background,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:0,
	    transform: Ti.UI.create2DMatrix().translate(globals.platform.width,0),
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 1
	});
	
	var navBar = Ti.UI.createView({
		height: 44,
		top: 0	
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "Hopmail",
		font: globals.fonts.words3,
		color: globals.colors.text,
		width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "center",
		lastText: "Hopmail - Inbox"
	});
	
	var activityIndicator = Ti.UI.createActivityIndicator({
		  color: globals.colors.text,
		  //font: {fontFamily:'Helvetica Neue', fontSize:26, fontWeight:'bold'},
		  //message: 'Loading...',
		  style:Titanium.UI.iPhone.ActivityIndicatorStyle.PLAIN,
		  //top:10,
		  right:50,
		  height:Ti.UI.SIZE,
		  width:Ti.UI.SIZE
	});
	
	
	var backButton = Common.CreateBackButton();
	
	backButton.addEventListener("singletap", function(){
		Common.WindowCloseAnimation(win);
		globals.mainMenuWin.BackToThisWindow();
		setTimeout(function(){win.close();},550);
	});
	
	var refreshButton = Ti.UI.createView({
		width: 60,
		height: 44,
		right: 0,
		top:0
	});
	
	refreshButton.add(Ti.UI.createView({
		backgroundImage: globals.colors.theme == "dark" ? "/images/refreshButtonDark.png" : "/images/refreshButton.png",
		width: 25,
		height: 25,
		right:10,
		touchEnabled:false
	}));
	
	refreshButton.addEventListener("singletap", function(){
		Refresh();
	});
	
	
	navBar.add(titleLabel);
	navBar.add(backButton);
	navBar.add(activityIndicator);
	navBar.add(refreshButton);
	
	
	var scrollView = Ti.UI.createScrollView({
		//backgroundColor: "red",
		width: globals.platform.width,
		height: globals.platform.height - 44,
		top: 44,
		contentWidth: globals.platform.width,
		contentHeight: "auto",
		layout: "vertical"
	});
	
	/*
	var helpRequestSent = CreateRow(data.helpRequestSent);
	var helpRequestInbox = CreateRow(data.helpRequestInbox);
	var tryThisInbox = CreateRow(data.tryThisInbox);
	var tryThisSent = CreateRow(data.tryThisSent);
	*/
	
	var inbox = CreateRow(data.inbox);
	var sent = CreateRow(data.sent);
	
	scrollView.add(inbox);
	scrollView.add(sent);
	
	/*
	scrollView.add(helpRequestSent);
	scrollView.add(helpRequestInbox);
	scrollView.add(tryThisInbox);
	scrollView.add(tryThisSent);
	*/
	scrollView.add(Ti.UI.createLabel({
		text: "You can send a Help Request for a puzzle while you are playing or from your favorites.",
		font: globals.fonts.regular2,
		color: globals.colors.textMenus,
		width: globals.platform.width-20,
		//height: 20,
		//left: 20,
		top: 50,
		textAlign: "center"
	}));
	
	win.add(scrollView);
	win.add(navBar);
	
	
	win.addEventListener("open", function(){
		var Common = require('ui/Common');
		Common.WindowOpenAnimation(win);
		globals.mainMenuWin.OpenAnotherWindow();
	});
	
	win.addEventListener("close", function(){
		Ti.App.removeEventListener("updateWMessages", UpdateHopmailMenu);
	});
	
	
	function Refresh(){
		titleLabel.text = "Connecting...";
		activityIndicator.show();
		ServerData.FetchWMessages();
	}
	
	function UpdateHopmailMenu(e){
		data = e.results;
		inbox.Update(data.inbox);
		sent.Update(data.sent);
		/*
		helpRequestSent.Update(data.helpRequestSent);
		helpRequestInbox.Update(data.helpRequestInbox);
		tryThisInbox.Update(data.tryThisInbox);
		tryThisSent.Update(data.tryThisSent);
		*/
		activityIndicator.hide();
		titleLabel.text = "Hopmail";
	}
	
	function OpenAnotherWindow(){
		win.animate(Ti.UI.createAnimation(
			{duration: globals.style.animationSpeeds.speed1, 
			transform: Ti.UI.create2DMatrix().translate(-globals.platform.width,0)}
		));
	}
	
	function BackToThisWindow(){
		win.animate(Ti.UI.createAnimation(
			{duration: globals.style.animationSpeeds.speed1, 
			transform: Ti.UI.create2DMatrix().translate(0,0)}
		));
	}
	
	Ti.App.addEventListener("updateWMessages", UpdateHopmailMenu);
	
	globals.lastWindow = win;
	
	win.BackToThisWindow = BackToThisWindow;
	win.OpenAnotherWindow = OpenAnotherWindow;
	
	Refresh();
	
	return win;
};	


function CreateRow(data){
	var WMessagesController = require("controller/WMessagesController");
	var row = Ti.UI.createView({
		width: globals.platform.width,
		height: 60,
		backgroundColor: "transparent",
		data: data
	});
	
	row.addEventListener("singletap",function(e){
		var HelpRequestsWin = require("ui/HelpRequestsWin");
		var win1 = HelpRequestsWin.loadWin(row.data);
		win1.open();
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: data.title,
		font: globals.fonts.words2,
		color: globals.colors.text,
		width: globals.platform.width-80,
		left: 20,
		top: 5
	});
	
	var subTitleLabel = Ti.UI.createLabel({
		text: data.subTitle,
		font: globals.fonts.regular2,
		color: globals.colors.textMenus,
		width: globals.platform.width-40,
		height: 20,
		left: 20,
		bottom: 15
	});
	
	/*
	var countBubble = Ti.UI.createView({
		backgroundColor: globals.colors.red,
		borderRadius: 10,
		width: 20,
		height: 20,
		top: 5,
		right: 10,
	});
	*/
	var countLabel = Ti.UI.createLabel({
		text: data.bubbleCount,
		font: globals.fonts.words2,
		color: globals.colors.text,
		right: 10,
		top: 5
	});
	
	//countBubble.add(countLabel);
	
	function Update(data){
		row.data = data;
		countLabel.text = data.bubbleCount;
	};
	
	
	row.add(titleLabel);
	row.add(subTitleLabel);
	row.add(countLabel);
	
	row.Update = Update;
	
	
	return row;
};


function CreateTableView(){
	var tableData = [];
	
	var tableView = Ti.UI.createTableView({
		width: globals.platform.width,
		height:Ti.Platform.displayCaps.platformHeigth-globals.adsBannerSpace-44,
		backgroundColor: "transparent",
		//separatorStyle: false,
		top: 44
	});
	
	
	return tableView;
}