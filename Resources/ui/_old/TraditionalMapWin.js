exports.loadWin = function(){
	var LocalData = require("model/LocalData");
	var Common = require("ui/Common");
	
	var gameData = globals.gameData;
	var unlockedWorldsData = globals.col.worlds.find({locked: false});
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor: "transparent",
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:-globals.platform.height,
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 1
	});
	
	win.addEventListener("open", function(){
		AppearAnimation();
	});
	
	win.addEventListener("close", function(){
		worldContainer.currentWorld.Close();
		Ti.App.removeEventListener("unlockNextWorld", UnlockNewWorld);
		Ti.App.removeEventListener("updateAbilities",UnlockItem);
	});
	
	
	var worldContainer = CreateWorldContainer();
	worldContainer.addEventListener("closePressed", function(e){
		if(e.worldID != "moon"){
			DisappearAnimation();
		}
		else {
			if (gameData.lastWorld == "moon") gameData.lastWorld = "meadows";
			worldContainer.OpenWorld(gameData.lastWorld,true);
		}	
	});
		
	worldContainer.addEventListener("mapPressed", function(){
		OpenBigMap(false);
	});
	
	worldContainer.OpenWorld(gameData.currentWorld, true);
	
	win.add(worldContainer);
	
	
	/////Functions
	function OpenBigMap(closeImmediately){
		var BigMapPopUp = require("/ui/MapWinElements/BigMapPopUp");
		var bigMap = BigMapPopUp.CreateBigMap(closeImmediately);
		
		bigMap.addEventListener("worldSelected", function(e){
			worldContainer.OpenWorld(e.worldID);
			bigMap.Close();
			setTimeout(function(){win.remove(bigMap);}, 750);
		});
		
		bigMap.addEventListener("close", function(e){
			bigMap.Close();
			setTimeout(function(){win.remove(bigMap);}, 750);
		});
		
		win.add(bigMap);
		bigMap.Open();
	}
	
	function UnlockNewWorld(){
		OpenBigMap(true);
	}
	
	function UnlockItem(e){
		var UnlockItemPopUp = require("/ui/MapWinElements/UnlockItemPopUp");
		var unlockItemPopUp = UnlockItemPopUp.Create(e.ability);
		//unlockItemPopUp.Open();
		
		unlockItemPopUp.addEventListener("close", function(e){
			unlockItemPopUp.Close();
			setTimeout(function(){win.remove(unlockItemPopUp);}, 750);
		});
		
		win.add(unlockItemPopUp);
		unlockItemPopUp.Open();
	}
	
	function AppearAnimation(){
		Animator.animate(win,{
			duration: 500,
			top: 0,
			easing:Animator['EXP_OUT']
		});
	}
	
	function DisappearAnimation(){
		Animator.animate(win,{
			duration: 500,
			top: -globals.platform.height,
			easing:Animator['EXP_IN']
		},function(){win.close();});
	}
	/////end Functions
	
	Ti.App.addEventListener("unlockNextWorld", UnlockNewWorld);
	Ti.App.addEventListener("updateAbilities",UnlockItem);
	
	
	globals.windowController.AddWindow(win);
	
		
	return win;
};	


function CreateWorldContainer(){
	var WorldsController = require("/controller/WorldsController");
	var WorldMap = require("/ui/MapWinElements/WorldMap");
	var worlds = [];
	var worldsStack = [];
	var currentWorldNumber = -1;
	
	var view = Ti.UI.createView({
		width:  globals.platform.width,
		height: (globals.platform.height - globals.adsBannerSpace),
		bottom: 0
	});
	
	function OpenWorld(worldID,isDown){
		WorldsController.SetCurrentWorld(worldID);
		
		var newWorld = WorldMap.CreateWorldMap(worldID, isDown);
		newWorld.addEventListener("portalPressed", function(){
			if(worldID != "moon") OpenWorld("moon", worldID);
			else (alert("Death Star"));
		});
		
		view.add(newWorld);
		if(view.currentWorld){
			view.currentWorld.Close();
			view.remove(view.currentWorld);
			view.currentWorld = null;
		}
		view.currentWorld = newWorld;
	}
	
	
	view.OpenWorld = OpenWorld;
	
	
	return view;
}


function capitaliseFirstLetter(string){
	return string.charAt(0).toUpperCase() + string.slice(1);
}	
