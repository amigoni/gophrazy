exports.loadWin = function(){
	var MonstersController = require("/controller/MonstersController");
	var FamiliesTableView = require("/ui/MonsterWinElements/FamiliesTableView");
	var FamilyDetail = require("ui/MonsterWinElements/FamilyDetail");
	var monstersData = MonstersController.GetFamiliesData();
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: -(globals.platform.actualHeight)
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	var navBar = CreateNavBar();
	
	var tableViewsController = CreateTableViewsController();
	tableViewsController.addEventListener("openGroup", function(e){
		tableViewsController.Add(FamilyDetail.createView(e.rowData));
	});
	
	var familiesTableView = FamiliesTableView.createView(monstersData);
	tableViewsController.Add(familiesTableView);
	
	view.add(navBar);
	view.add(tableViewsController);
	
	
	function Open(){
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}
	
	function Close(){
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Animator.animate(view,{
			duration: 500,
			top: 0,
			easing:Animator['EXP_OUT']
		});},200);
	}
	
	function DisappearAnimation(){
		Animator.animate(view,{
			duration: 500,
			top: -(globals.platform.actualHeight),
			easing:Animator['EXP_IN']
		},function(){
			view.fireEvent("closed");
			}
		);
	}
	
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: "#CD4D39",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 60,
		height: 44,
		right: 0
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 35,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/monsterBadgeSmall.png",
		width: 34,
		height: 34
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/monstersSign.png",
		width: 115,
		height: 28
	}));
	
	view.add(title);
	view.add(closeButton);
	
	
	return view;
};


function CreateTableViewsController (){
	var controller = Ti.UI.createView({
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
	    top: 44
	});
	
	controller.addEventListener("back", function(e){
		Back();
	});
	
	var stack = [];
	
	function Add(newTableView){
		stack.push(newTableView);
		controller.add(newTableView);
		if(stack.length >1){
			for (var i = 0; i < stack.length; i++){
				stack[i].MoveLeft();
			}	
		}
	}
	
	function Back (){
		for (var i = 0; i < stack.length; i++){
			stack[i].MoveRight();
		}
		setTimeout(function(){
			controller.remove(stack[stack.length-1]);
			stack.pop();
		}, 1000);
		
	}
	
	controller.Add = Add;
	controller.Back = Back;
	
	
	return controller;
};

