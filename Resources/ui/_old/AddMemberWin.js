exports.createView = function(data){
	var GroupsController = require("controller/GroupsController");
	var AddMemberWin = require("ui/FriendsWinElements/AddMemberWin");
	var ButtonBar = require("ui/FriendsWinElements/ButtonBar");
	var InviteWin = require("ui/FriendsWinElements/InviteWin");
	var LeaderboardView = require("ui/FriendsWinElements/LeaderboardView");
	var NavBar = require("ui/FriendsWinElements/NavBar");
	var SignaturesView = require("ui/FriendsWinElements/SignaturesView");
	var TeamView = require("ui/FriendsWinElements/TeamView");
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: (globals.platform.actualHeight)
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	view.addEventListener("openInvitePopUp", function(){
		alert("Invite PopUP");
	});
	
	var navBar = CreateNavbar();
	var tableView = CreateTableView(data);
	
	view.add(navBar);
	view.add(tableView);
	
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		navBar.Close();
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Animator.animate(view,{
			duration: 500,
			top: 0,
			easing:Animator['EXP_OUT']
		});},200);
	}
	
	function DisappearAnimation(){
		Animator.animate(view,{
			duration: 500,
			top: (globals.platform.actualHeight),
			easing:Animator['EXP_IN']
		},function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavbar (){
	var view = Ti.UI.createView({
		backgroundColor: "#D5C7A3",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.bold25,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 34,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/friendsIconLarge.png",
		width: 37,
		height: 34
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/addTeamMemberSign.png",
		width: 191,
		height: 23,
		left: 3
	}));
	
	
	view.add(title);
	view.add(closeButton);
	
	function Close(){
		
	}
	
	view.Close = Close;
	
	
	return view;
}


function CreateTableView(data){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 44,
		separatorColor: "transparent",
		separatorStyle: false,
		opacity: 1
		//left: type == "main" ? 0 : globals.platform.width,
	});
	
	var tableData = [];
	tableData.push(CreateHeaderRow("Playing GoPhrazy"));
	for(var i = 0; i < data.installed.length; i++){
		var row = CreatePersonRow(data.installed[i]);
		tableData.push(row);
	}
	
	tableData.push(CreateHeaderRow("Facebook Friends"));
	for(var i = 0; i < data.invite.length; i++){
		var row = CreatePersonRow(data.invite[i]);
		tableData.push(row);
	}
	
	tableView.data = tableData;
	
	
	function CreateHeaderRow(name){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: 40,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
			touchEnabled: false
		});
		
		var label = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: name,
			font: globals.fonts.bold15,
			//minimumFontSize: 12,
			color: globals.colors.mediumBrown,
			width: Ti.UI.SIZE,
			//bottom: 5
		});
		
		view.add(label);
		
		return view;
	}
	
	function CreatePersonRow(rowData,rank){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: 40,
			selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE
		});
		
		view.addEventListener("singletap", function(){
			//if(rowData.installed == true) alert("recruit");
			//else alert("Invite");
		});
		
		var nameLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.name,
			font: globals.fonts.bold15,
			color: globals.colors.darkBrown,
			textAlign: "left",
			width: globals.platform.width-110,
			height: 20,
			left: 10
		});
		
		var recruitButton = Ti.UI.createView({
			backgroundImage: "/images/recruitButton.png",
			width: 94,
			height: 36, 
			right: 5
		});
		
		Common.MakeButton(recruitButton);
		
		recruitButton.addEventListener("singletap", function(e){
			//view.fireEvent("recruit",{id: rowData.id});
			var FriendsController = require("controller/FriendsController");
			Ti.API.info(rowData);
			FriendsController.AddFriendToTeam(rowData);
			view.fireEvent("closeWin");
		});
		
		
		var inviteLabel = Ti.UI.createLabel({
			text: "invite",
			color: globals.colors.darkBrown,
			textAlign: "left",
			font: globals.fonts.boldMediumSmall,
			minimumFontSize: 10,
			right: 30
		});
		
		Common.MakeButton(inviteLabel);
		
		inviteLabel.addEventListener("singletap", function(){
			alert("invite");
		});
	
		
		view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1,bottom: 0}));
		view.add(nameLabel);
		if(rowData.installed == true) view.add(recruitButton);
		if(rowData.installed == false) view.add(inviteLabel);
		
		
		return view;
	}
	
	
	return tableView;
};
