exports.loadWin = function(){
	var LocalData = require("model/LocalData");
	var Common = require("ui/Common");
	
	var win = Titanium.UI.createWindow({ 
	    backgroundColor:"transparent",
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    height: globals.platform.actualHeight,
	    top:0,
	    fullscreen:true,
	    navBarHidden: true,
	    opacity: 0
	});
	
	
	var background = Ti.UI.createView({
		backgroundColor: "black",
		width: globals.platform.height,
		height: globals.platform.actualHeight,
		opacity: 0.8,
		top: 0
	});
	
	var popUpView = Ti.UI.createView({
		backgroundColor: globals.colors.background,
		borderColor: globals.colors.borderLine2,
		borderWidth: 3,
		width: 250,
		height: Ti.UI.SIZE,
		transform: Ti.UI.create2DMatrix().translate(0,globals.platform.height),
		layout: "vertical"
	});
	
	var navBar = Ti.UI.createView({
		width: 240,
		height: 50
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "Buy Hints",
		font: globals.fonts.words3,
		color: globals.colors.warning,
		width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "center",
		top: 10
	});
	
	var hintIcon = Ti.UI.createView({
		backgroundImage:"/images/hintButton.png",
		width: 27,
		height: 27,
		top: 10,
		left: 30
	});
	
	
	navBar.add(titleLabel);
	navBar.add(hintIcon);
	navBar.add(Ti.UI.createView({backgroundColor: globals.colors.textMenus,width: 250, height: 1,bottom:1}));

	//Rows of Purchases
	var storeData = [
		{price: 0.99, priceCurrencySymbol:"$", currensy:"USD", priceText:"$0.99", itemID: "3_hints", itemName: "3 Hints", quantity: 3, type: "hints", saleText:""},
		{price: 4.99, priceCurrencySymbol:"$", currensy:"USD", priceText:"$4.99", itemID: "25_hints", itemName: "25 Hints", quantity: 25, type: "hints", saleText:"+25%"},
		{price: 5.99, priceCurrencySymbol:"$", currensy:"USD", priceText:"$5.99", itemID: "15_hints", itemName: "15 Hints", quantity: 15, type: "hints", saleText:""}
	];
	
	
	var cancelButton = CreateCancelButton();
	cancelButton.addEventListener("singletap", function(){
		Close();
	});
	
	
	popUpView.add(navBar);
	for (var i = 0; i < storeData.length; i++){
		popUpView.add(CreateRow(storeData[i]));
	}
	popUpView.add(cancelButton);
	popUpView.add(Ti.UI.createView({height: 20}));
	
	
	win.add(background);
	win.add(popUpView);
	
	
	win.addEventListener("open", function(){
		popUpView.animate(Ti.UI.createAnimation(
			{duration: globals.style.animationSpeeds.speed1, 
			transform: Ti.UI.create2DMatrix().translate(0,0)}
		));
		win.animate(Common.appearAnimation());
	});
	
	function Close(){
		popUpView.animate(Ti.UI.createAnimation(
			{duration: globals.style.animationSpeeds.speed1, 
			transform: Ti.UI.create2DMatrix().translate(0,globals.platform.height)}
		));
		win.animate(Ti.UI.createAnimation({duration: globals.style.animationSpeeds.speed1,opacity: 0}));
		setTimeout(function(){win.close();},255);
	}
	
	
	return win;
};	


function CreateRow(data){
	var LocalData = require("model/LocalData");
	var StoreController = require("/controller/StoreController");
	var Common = require('ui/Common');
	
	var row = Ti.UI.createView({
		backgroundColor: "transparent",
		width: 240,
		height: 50,
		//bottom:20,
		data: data,
	});
	
	row.addEventListener("singletap", function(){
		StoreController.BuyItem(data);
		alert("Success, purchased "+data.itemName);
	});
	
	var priceLabel = Ti.UI.createLabel({
		text: data.priceText,
		font: globals.fonts.words1,
		color: globals.colors.textMenus,
		textAlign: "right",
		//top: 10
		left: 20
	});
	
	var itemLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: data.itemName,
		font: globals.fonts.words3,
		color: globals.colors.text,
		textAlign: "right",
		width: 100,
		//top: 10,
		left: 70
	});
	
	var saleLabel = Ti.UI.createLabel({
		text: data.saleText,
		font: globals.fonts.words2,
		color: globals.colors.warning,
		textAlign: "right",
		//top: 10,
		right: 10,
		opacity:1
	});
	
	saleLabel.animate(Ti.UI.createAnimation({
		duration: 1000,
		opacity: 0,
		autoreverse : true,
		repeat: 9999999
	}));
	
	row.add(priceLabel);
	row.add(itemLabel);
	row.add(saleLabel);
	
	return row;
}

function CreateCancelButton(){
	var button =  Ti.UI.createView({
		backgroundColor: globals.colors.background,
		borderColor: globals.colors.borderLine2,
		borderWidth: 1,
		width: 190,
		height: 50,
		top:10,
		bubbleParent: false
	});
	
	button.add(Ti.UI.createLabel({
		text: "Cancel",
		font: globals.fonts.words3,
		color: globals.colors.text,
		//width: globals.platform.width-120,
		minimumFontSize: 14,
		textAlign: "left",
		//top: 5,
		//left: 5
		//top: groupData.type != "dailyChallenge" ? 4:null
	}));
	
	return button;
}