var AnalyticsController = require("/controller/AnalyticsController");

exports.loadWin = function(){
	//var RankingView = require("ui/MainMenuWinElements/RankingView");
	var WhatsNewController = require("/controller/WhatsNewController");
	var whatsNewController = new WhatsNewController();
	var MainMenuScrollView = require("/ui/MainMenuWinElements/MainMenuScrollView");
	var AuthenticationViewContainer = require("/ui/MainMenuWinElements/AuthenticationViewContainer");
	var currentTheme = GetCurrentTheme();
	
	var win = Titanium.UI.createView({ 
	    backgroundImage: currentTheme.menuBg,
	    width: globals.platform.width,	
	    height: globals.platform.actualHeight,   
	    top: 0,
	    opacity: 1
	});
	
	var world = Ti.UI.createView({
		backgroundImage: currentTheme.worldImage,
		width: 624,
		height: 645,
		//top: (((globals.platform.actualHeight)-(480-globals.adsBannerSpace))/2+480-globals.adsBannerSpace)-250,
		bottom: -410,
		left: globals.platform.width/2-624/2-20,
		anchorPoint: {x:0.5, y: 0.5},
		opacity: 0.3
	});
	
	win.add(world);	
	
	var scrollView;
	var authenticationViewContainer;
		
	if(globals.gameData.userName != "" || globals.gameData.userName == null){
		scrollView = new MainMenuScrollView();
		win.add(scrollView);
	}
	else{
		authenticationViewContainer = new AuthenticationViewContainer("registerView");
		
		authenticationViewContainer.addEventListener("closeMe", function(){
			scrollView = new MainMenuScrollView();
			win.add(scrollView);
			win.remove(authenticationViewContainer);
			authenticationViewContainer = null;
		});
		
		win.add(authenticationViewContainer);
	}
	
	
	function Open(){
		win.animate(Ti.UI.createAnimation({
			duration: 250,
			opacity: 1
		}));
	};
	
	function UpdateViews(){
		if(scrollView != null){
			scrollView.Close();
			win.remove(scrollView);
			scrollView = null;
			scrollView = new MainMenuScrollView();
			win.add(scrollView);
		}	
	}
	
	function OpenTutorial(e){
		globals.rootWin.OpenWindow("tutorialPopUp", e.name);
	}
	
	function GetCurrentTheme(){
		var returnObject;
		var now = clock.getNow();
		
		var hour = now.hour();
		if(hour > 20 || hour <= 5) returnObject = globals.themes.night;
		else if(hour > 5 && hour <= 9) returnObject = globals.themes.dawn;
		else if(hour > 9 && hour <= 17) returnObject = globals.themes.day;
		else if(hour > 17 && hour <= 20) returnObject = globals.themes.dusk;	
		
		return returnObject;
	}
	
	function UpdateTheme(){
		win.backgroundImage = currentTheme.menuBg;
		world.tint = currentTheme.worldColor;
	}
	
	win.Open = Open;
	
	Ti.App.addEventListener("levelUp", UpdateViews);
	Ti.App.addEventListener("multiplayerOn", UpdateViews);
	Ti.App.addEventListener("openTutorial", OpenTutorial);
	
	if(whatsNewController.openPopUp == true){
		setTimeout(function(){
				globals.rootWin.OpenWindow("whatsNewPopUp", whatsNewController.data);
		},1000);
	}
	
	
	return win;
};
