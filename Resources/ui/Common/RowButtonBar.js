exports.createView = function(buttons){
	var shareButton;
	var shareButtonLarge;
	var sendPuzzleButton;
	var deleteButton;
	var editButton;
	var playButton;
	var favoriteButton;
	var leftPos = 0;
	
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: 55,
		left: globals.platform.width,
		bubbleParent: false
	});
	
	//view.addEventListener("share",function(){alert("ciao")})
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: view.height,
		layout: "horizontal"
	});
	
	if(buttons.shareButton == true){
		shareButton = CreateButton({
			buttonID: "rowButtonBarShareButton",
			eventName: "share",
			image: "/images/shareButton.png",
			title: "Share\nSolution",
			left: 0,
			width: 37,
			height: 30,
			iconTop: 0,
			iconScale: 0.7
		});
	}
	if(buttons.shareButtonLarge == true){
		shareButtonLarge = Ti.UI.createView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			left: 20,
			buttonID: "rowButtonBarShareButton",
			layout: 'horizontal'
		});
		
		Common.MakeButton(shareButtonLarge);
		
		shareButtonLarge.addEventListener("singletap", function(e){
			if(shareButtonLarge.touchEnabled == true) view.fireEvent("share");
		});
		
		var leftContainer = Ti.UI.createView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			layout: "vertical",
			//touchEnabled: false
		});
		
		leftContainer.add(Ti.UI.createLabel({
			text: "Share it",
			font: globals.fonts.bold25,
			color: globals.colors.black,
			//touchEnabled: false
		}));
		
		leftContainer.add(Ti.UI.createLabel({
			text: "with the World",
			font: globals.fonts.bold13,
			color: globals.colors.black,
			//touchEnabled: false
		}));
		
		var icon = Ti.UI.createView({
			backgroundImage: "/images/shareButton.png",
			width: 42,
			height: 33,
			left: 5
			//touchEnabled: false
		});
		
		shareButtonLarge.add(leftContainer);
		shareButtonLarge.add(icon);
	}
	if(buttons.sendButton == true){
		sendPuzzleButton = CreateButton({
			buttonID: "rowButtonBarSendButton",
			eventName: "send",
			image: "/images/sendPuzzleIcon.png",
			title: "Send\nPuzzle",
			width: 50,
			height: 23,
			left: 20,
			iconTop: 2,
			iconScale: 0.9
		});
	}
	
	if(buttons.deleteButton == true){
		deleteButton = CreateButton({
			buttonID: "rowButtonBarDeleteButton",
			eventName: "delete",
			image: " X",
			title: "Delete",
			left: 5,
			iconTop: 2,
			iconScale: 1
		});
	}
	
	if(buttons.editButton == true){
		editButton = CreateButton({
			buttonID: "rowButtonBarEditButton",
			eventName: "edit",
			image: "/images/editIcon.png",
			title: "Edit",
			width: 35,
			height: 35,
			left: 20,
			iconTop: 5,
			iconScale: 0.8
		});
	}
	
	if(buttons.playButton == true){
		playButton = CreateButton({
			buttonID: "rowButtonBarPlayButton",
			eventName: "play",
			image: "/images/playSignNoShadow.png",
			title: "",
			width: 50,
			height: 26,
			left: 20,
			//iconTop: 5,
			iconScale: 1
		});
	}
	
	if(buttons.favoriteButton == true){
		favoriteButton = CreateButton({
			buttonID: "rowButtonBarFavoriteButton",
			eventName: "favorite",
			image: "/images/playSignNoShadow.png",
			title: "Favs",
			left: 20,
			width: 36,
			height: 32,
			//iconTop: 5,
			iconScale: 1
		});
	}
	
	if(shareButton != null) container.add(shareButton);
	if(shareButtonLarge != null) container.add(shareButtonLarge);
	if(sendPuzzleButton != null) container.add(sendPuzzleButton);
	if(deleteButton != null) container.add(deleteButton);
	if(editButton != null) container.add(editButton);
	if(playButton != null) container.add(playButton);
	if(favoriteButton != null) container.add(favoriteButton);
	
	view.add(container);
	
	function CreateButton(buttonData){
		var image;
		var button = Ti.UI.createView({
			width: 50,
			height: 55,
			left: buttonData.left,
			buttonID: buttonData.buttonID,
			touchEnabled: true
		});
		
		Common.MakeButton(button);
		
		button.addEventListener("singletap", function(e){
			if (button.touchEnabled == true) view.fireEvent(buttonData.eventName);
		});
		
		
		if(buttonData.buttonID != "rowButtonBarDeleteButton"){
			image = Ti.UI.createImageView({
				image:  buttonData.image,
				width: buttonData.width,
				height: buttonData.height,
				transform: Ti.UI.create2DMatrix().scale(buttonData.iconScale, buttonData.iconScale),
				top: buttonData.iconTop
			});
		}
		else{
			image = Ti.UI.createLabel({
				text:  buttonData.image,
				font: globals.fonts.bold30,
				color: globals.colors.red,
				textAlign:"center",
				top: buttonData.iconTop,
				left: buttonData.left
			});
		}
		
		var label = Ti.UI.createLabel({
			text:  buttonData.title,
			font: globals.fonts.bold11,
			color: globals.colors.black,
			//height: 20,
			verticalAlign: Ti.Platform.name == "iPhone OS" ? "center" : null,
			textAlign:"center",
			bottom: -2,
			width: button.width
		});
		
		button.add(image);
		button.add(label);
		
		button.image = image;
		
		
		return button;
	};
	
	
	function EnterAnimation(){
		/*
		Animator.animate(view,{
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			easing: Animator["EXP_OUT"]
		});
		*/
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}
	
	function ExitAnimation(){
		/*
		Animator.animate(view,{
			duration: globals.style.animationSpeeds.speed1,
			left: globals.platform.width,
			easing: Animator["EXP_OUT"]
		});
		*/
		
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
	}
	
	view.favoriteButton = favoriteButton;
	view.EnterAnimation = EnterAnimation;
	view.ExitAnimation = ExitAnimation;
	
	
	return view;
};
