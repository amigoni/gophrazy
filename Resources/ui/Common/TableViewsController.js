exports.createView = function(){
	var stack = [];
	var controller = Ti.UI.createView({
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
	    top: 44
	});
	
	/*
	controller.addEventListener("openCategory", function(e){
		controller.Add(CreateTableView("category",e.rowData));
	});
	*/
	
	controller.addEventListener("back", function(e){
		Back();
	});
	
	
	function Add(newTableView){
		stack.push(newTableView);
		controller.add(newTableView);
		if(stack.length >1){
			for (var i = 0; i < stack.length; i++){
				stack[i].MoveLeft();
			}	
		}
	}
	
	function Back (){
		for (var i = 0; i < stack.length; i++){
			stack[i].MoveRight();
		}
		setTimeout(function(){
			controller.remove(stack[stack.length-1]);
			stack.pop();
		}, 1000);
		
	}
	
	controller.Add = Add;
	controller.Back = Back;
	
	
	return controller;
};