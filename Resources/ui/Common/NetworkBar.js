exports.createView = function(bgColor, text){
	var view = Ti.UI.createView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height: 47,
		bottom: -47,
		opacity: 1,
		bubbleParent: false,
		zIndex: 200
	});
	
	view.addEventListener("singletap", function(){
		Disappear();
	});
	
	var textBg = Ti.UI.createView({
		backgroundColor: globals.colors.red,//"#5F8291",
		width: view.width,
		height: 20,
		bottom: 0
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 20,
		layout: "horizontal"
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold14,
		width: view.width - 60,
		minimumFontSize: "12dp",
		textAlign: "center",
		color: globals.colors.white
	});
	
	var style;
	if (Ti.Platform.name === 'iPhone OS') style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;
	else style = Ti.UI.ActivityIndicatorStyle.DARK;
	
	var activityIndicator = Ti.UI.createActivityIndicator({
	  color: globals.colors.white,
	  //font: {fontFamily:'Helvetica Neue', fontSize:26, fontWeight:'bold'},
	  //message: 'Loading...',
	  style:style,
	  //bottom:100,
	  right: 10,
	  height: Ti.UI.SIZE,
	  width: Ti.UI.SIZE
	});
	
	activityIndicator.show();
	
	textBg.add(activityIndicator);
	textBg.add(label);
	view.add(textBg);
	
	
	function SentMessage(){
		activityIndicator.hide();
		label.text = "Puzzle Sent!";
		setTimeout(Close,2000);
	}
	
	function ErrorSentMessage(){
		textBg.backgroundColor = "#CD4E3A";
		activityIndicator.hide();
		label.text = "Oh no! There was an error! Please try again!";
		setTimeout(Close,2000);
	}
	
	function NoConnection(){
		textBg.backgroundColor = "#CD4E3A";
		activityIndicator.hide();
		label.text = "Oh no! There is no Internet Connection!";
		setTimeout(Close,2000);
	}
	
	function ReceivedMessages(e){
		activityIndicator.hide();
		if(e.count > 0) label.text = "Done! Got messages.";
		else label.text = "Done! There were no new messages.";
		setTimeout(Close,2000);
	}
	
	function Open(){
		Ti.App.fireEvent("networkBarAction", {isDown: true});
		
		setTimeout(function(){
			view.animate(Ti.UI.createAnimation({
				duration: globals.style.animationSpeeds.speed2,
				bottom: 0,
				curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
			}));
		},500);
	};
	
	
	function CloseNetworkBar(){
		setTimeout(Close,2000);
	}
	
	function Close(){
		Ti.App.removeEventListener("SentMessage", SentMessage);
		Ti.App.removeEventListener("ErrorSentMessage", ErrorSentMessage);
		Ti.App.removeEventListener("NoConnection", NoConnection);
		Ti.App.removeEventListener("updateMessagesCount", ReceivedMessages);
		Ti.App.removeEventListener("CloseNetworkBar", CloseNetworkBar);
		Disappear();
		setTimeout(function(){
			Ti.App.fireEvent("networkBarAction", {isDown: false});
		},100);
	}
	
	function Disappear(){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed2,
			bottom: -47,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));	
	}
	
	view.label = label;
	view.SentMessage = SentMessage;
	view.ErrorSentMessage = ErrorSentMessage;
	view.NoConnection = NoConnection;
	
	view.Open = Open;
	
	Ti.App.addEventListener("SentMessage", SentMessage);
	Ti.App.addEventListener("ErrorSentMessage", ErrorSentMessage);
	Ti.App.addEventListener("NoConnection", NoConnection);
	Ti.App.addEventListener("updateMessagesCount", ReceivedMessages);
	Ti.App.addEventListener("CloseNetworkBar", CloseNetworkBar);
	
	
	return view;
};
