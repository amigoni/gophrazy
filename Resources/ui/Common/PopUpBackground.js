function Create (hasCloseButton){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		bubbleParent: false,
		zIndex: 100,
		top: 0
	});
	
	var background = Ti.UI.createView({
		backgroundColor: "black",
		width: view.width,
		height: view.height,
		opacity: 0
	});
	
	var container = Ti.UI.createView({
		width: 200,
		height: 200,
		top: globals.platform.actualHeight
	});
	
	var popUpBg = Ti.UI.createView({
		borderColor: "black",
		backgroundColor: globals.colors.white,
		borderWidth: 2,
		borderRadius: 20,
		width: container.width - 20,
		height: container.height - 20
	});
	
	var closeX = Ti.UI.createView({
		backgroundImage:'/images/closeX.png',
		width: 28,
		height: 29,
		top: 0,
		right: 0
	});
	Common.MakeButton(closeX);
	
	closeX.addEventListener("singletap", function(){
		Close();
	});
	
	container.add(popUpBg);
	if(hasCloseButton == true)container.add(closeX);
	
	
	view.add(background);
	view.add(container);
	
	
	function Close(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			easing: Animator["EXP_IN"]
		});
		
		Animator.animate(background,{
				duration: 300,
				opacity: 0,
			},
			function(){
				if (insideView.Close) insideView.Close();
				view.fireEvent("closed");
			}
		);
		*/
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
		
		background.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			opacity: 0
			}), 
			function(){
				//if (insideView.Close) insideView.Close();
				view.fireEvent("closed");
			}
		);
	}
	
	function Open(){
		setTimeout(function(){
				/*
			Animator.animate(container,{
				duration: 500,
				top: ((globals.platform.actualHeightNumber-container.size.height)/2).toString()+"dp",
				easing: Animator["EXP_OUT"]
			});
			
			Animator.animate(background,{
				duration: 500,
				opacity: 0.75,
			});
			*/
			container.animate(Ti.UI.createAnimation({
				duration: globals.style.animationSpeeds.speed1,
				top:((globals.platform.actualHeightNumber-container.size.height)/2).toString()+"dp",
				curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
			}));
			
			background.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 0.75
			}));
		}, 200);
	}
	
	
	view.container = container;
	view.popUpBg = popUpBg;
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


module.exports = Create;