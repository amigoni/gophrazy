exports.createView = function(text){
	
	var view = Ti.UI.createView({
		backgroundImage:"/images/redCircle.png",
		width: 26,
		height: 27,
		top: 5,
		right: 0,
		//visible: text > 0 ? true : false,
		touchEnabled: false
	});
	
	var container = Ti.UI.createView({
		//backgroundColor: "green",
		width: 18,
		height: 18,
		//left: 2,
		top: 4,
		touchEnabled: false
	});
	
	var countLabel = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: text,
		color: "white",
		font: globals.fonts.bold13,
		minimumFontSize: "5sp",
		textAlign: "center",
		width: container.width,
		//height: 15,
		touchEnabled: false
	});
	
	container.add(countLabel);
	view.add(container);
	
	function Update(newText){
		countLabel.text = newText;
		if (newText === parseInt(newText)){
			if(newText > 0) view.visible = true;
			else view.visible = false;
		}
		text = newText;
	}
	
	view.Update = Update;
	
	Update(text);
	
	
	return view;
};