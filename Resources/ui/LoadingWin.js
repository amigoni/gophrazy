exports.loadWin = function(){
	var AnalyticsController = require("/controller/AnalyticsController");
	var ScreenSizeController = require("/controller/ScreenSizeController");
	var LocalData = require("model/LocalData");
	var ServerData = require("/model/ServerData");
	var Common = require("/ui/Common");
	var RootWin = require("ui/RootWin");
	var AppBadgeController = require("controller/AppBadgeController");
	var SoundController = require("/controller/SoundController");
	var GameCenterController = require("/controller/GameCenterController");
	var TipStartController = require("/controller/TipStartController");
	var ServerController = require("/controller/ServerController");
	globals.ServerController = new ServerController();
	
	
	var gameStarted = false;
	
	globals.buttonsArray = []; //Array of buttons to use for reference for tutorial.
	
	ScreenSizeController.Init(); //Sets the screen size 
	
	var win = Titanium.UI.createWindow({  
		theme: "Theme.NoActionBar",
		backgroundColor: globals.colors.white,
	    orientationModes: [ Titanium.UI.PORTRAIT ],
	    fullscreen: true,
	    navBarHidden: true,
	    opacity: 1
	});
	
	Ti.UI.orientation = Ti.UI.PORTRAIT; //Forces orientation switch no matter what
	
	win.addEventListener("open", function(){
		LocalData.InitData(false, LoadGame);
		
		var tipStartController = new TipStartController();
		loadingWin.activityLabel.text = tipStartController.GetText();
		
		if (globals.os == "ios") Ti.App.iOS.registerBackgroundService({url:'BGService.js'});
		else{
			win.activity.actionBar.hide();
		}
	});
	
	
	var loadingWin = CreateLoadingWindow();
	
	var gameContainer = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.height,
		transform: globals.platform.scale != 1 ? Ti.UI.create2DMatrix().scale(globals.platform.scale, globals.platform.scale): null,
		opacity: 0
	});
	
	gameContainer.Open = function(){
		gameContainer.animate(Ti.UI.createAnimation({duration: 150, opacity: 1, delay: 250}));
	};
	
	globals.gameContainer = gameContainer;
	
	win.add(loadingWin);
	win.add(gameContainer);
	
	
	/////Functions
	
	//This function is called also on Resume
	function LoadGame(){
		var PushNotificationsController = require("controller/PushNotificationsController");
		var UpgradeController = require("controller/UpgradeController");
		var PuzzleFactoryController = require("controller/PuzzleFactoryController");
	
		UpgradeController.CheckForUpgrade();
		PushNotificationsController.Init();
		
		if(Titanium.Network.online == true && globals.gameData.userFacebookID != "" ) ServerData.SignUpFacebook();
		if(globals.gameData.userName && globals.gameData.userName != "") PuzzleFactoryController.GetMyPuzzles(globals.gameData.userName, null, true);
	}
	
	
	//Only called on start not on resume
	function StartGame(){
		//This prevents double call and also doesn't execute on resume
		if (gameStarted == false){	
			
			gameStarted = true;
			AppBadgeController.Init();
			StyleSheet.SetColorPalette(globals.gameData.style.currentWordBoxesPalette);
			SoundController.init();
			
			if(globals.gameData.sessionsCount > 1 && globals.gameData.gameCenterData.id != "") {
				Ti.API.info("GAMECENTER");
				Ti.App.fireEvent("initGameCenter");
			}	
			
			globals.rootWin = RootWin.loadWin();
	
			//var MainMenuWin = require("ui/MainMenuWin");
			var MainMenuWin = require("/ui/NewMainMenuWin");
			var mainMenuWin = MainMenuWin.loadWin();
			globals.rootWin.viewStack.Add(mainMenuWin);
			globals.rootWin.add(mainMenuWin);
			
			//Banner Ads
			var adView = CreateAdBannerView();
			//adView.opacity = 0;
			
			function DelayedStart(){
				loadingWin.activityLabel.text = "";
				if(Ti.Platform.name != "android") loadingWin.activityIndicator.hide();
				
				loadingWin.logo.animate(
					Ti.UI.createAnimation({
						duration: globals.style.animationSpeeds.speed3,
						top: globals.platform.height*2,
						curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
					})
				);	
				
				var AdsController = require("/controller/AdsController");
				globals.AdsController = new AdsController("chartboost");
				
				if (globals.gameData.firstStartComplete == true){
					//Start it later if not first time
				}
				else{
					mainMenuWin.opacity = 0;
					setTimeout(mainMenuWin.Open, 5000);
					globals.rootWin.OpenWindow("firstStart");
				}		
				
				gameContainer.add(globals.rootWin);
				gameContainer.add(adView);
				gameContainer.Open();
				
				//Check if you come from a URL
				CustomURLController.Check();
				
				setTimeout(function(){
					win.remove(loadingWin);
					win.backgroundColor = "black";
				}, 10000);
				
				Ti.API.info("Boot Up Time: "+(new Date().getTime()- globals.startTime));
				
				//Do it here after the game has booted
				AnalyticsController.SendCachedEvents();
			}
			
			setTimeout(DelayedStart, 2000);
		}
	}
	
	
	
	function UpdateActivityLabel(e){
		loadingWin.activityLabel.text = e.value;
	}
	/////end Functions
	
	Ti.App.addEventListener("startGame", StartGame);
	Ti.App.addEventListener("finishedSetup", LoadGame);
	Ti.App.addEventListener("resume", LoadGame);
	Ti.App.addEventListener("UpdgradeStatus", UpdateActivityLabel);
	
	
	
	return win;
};	


function CreateAdBannerView(){
	var adView = Ti.UI.createView({
		backgroundColor: globals.colors.black,
		zIndex: 100,
		width: globals.platform.width,
		height: 50,
		top: globals.platform.actualHeight,
		opacity: 1
	});
	
	adView.add(Ti.UI.createImageView({
		image:"/images/mozzarelloLogoWide.png",
		height: 36,
		width: 192
	}));
	
	//iAds
	/*
	var iads = Ti.UI.iOS.createAdView({
		width: "320dp",
		height: "50dp"
	});
	
	iads.addEventListener('load', function(e){
        //iads.animate(t1);
       // Ti.API.info(e);
    });
	
	adView.add(iads);
	*/
	
	//AdMob
	if(Ti.Platform.name == "iPhone OS"){
		var Admob = require('ti.admob');
		var ad = Admob.createView({
		    top: 0, 
		    width: 320, 
		    height: 50,
		    adUnitId: 'ca-app-pub-7157276024136518/9989087987', // You can get your own at http: //www.admob.com/
		    adBackgroundColor: 'transparent',
		    testDevices: [Admob.SIMULATOR_ID],
		   // dateOfBirth: new Date(1985, 10, 1, 12, 1, 1),
		   // gender: 'male',
		   // keywords: ''
		});
		
		adView.add(ad);
	}
	else if (Ti.Platform.name == "android"){
		var Admob = require("metal.admobgoogleplay");
		var ad = Admob.createView({
			top: 0,
			//width: 320,
			//height: 50,
			adSizeType: 'BANNER',
			publisherId: 'ca-app-pub-7157276024136518/4498149582',
			testing: false
		});	
		
		ad.addEventListener('ad_received', function(e) {
			Ti.API.info("ADMOB: Received ad");
		});
		
		ad.addEventListener('ad_not_received', function(e) {
			Ti.API.info("ADMOB: NOT Received ad");
		});
		
		adView.add(ad);
	}
	
	

	return adView;
}


function CreateLoadingWindow(){
	
	
	var view = Titanium.UI.createView({ 
	    width: globals.platform.width,
	    height: Ti.Platform.displayCaps.platformHeight,
	    top: 0,
	    opacity: 1,
	});
	
	var logo = Ti.UI.createView({
		backgroundImage: "/images/defaultLogo.png",
		top: (Ti.Platform.displayCaps.platformHeight/2-64/2),
		width: 86,
		height: 64,
	});

	var activityIndicator;
	if(Ti.Platform.name != "android"){
		activityIndicator = Ti.UI.createActivityIndicator({
		  color: '#333333',
		  //font: {fontFamily:'Helvetica Neue', fontSize:26, fontWeight:'bold'},
		  //message: 'Loading...',
		  style: globals.os == "ios" ? Ti.UI.iPhone.ActivityIndicatorStyle.DARK : Ti.UI.ActivityIndicatorStyle.DARK,
		  bottom: 150,
		  height: Ti.UI.SIZE,
		  width: Ti.UI.SIZE
		});
	}
	
	var activityLabel = Ti.UI.createLabel({
		text: "",
		font: globals.fonts.bold15,
		color: globals.colors.black,
		textAlign: "center",
		width: globals.platform.width - 40,
		bottom: 80
	});
	
	if(Ti.Platform.name != "android") view.add(activityIndicator);
	view.add(activityLabel);
	view.add(logo);
	
	if(Ti.Platform.name != "android") activityIndicator.show();
	
	view.logo = logo;
	view.activityIndicator = activityIndicator;
	view.activityLabel = activityLabel;
	
	
	return view;
}
