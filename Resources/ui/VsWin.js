exports.loadWin = function(){
	var VsMainView = require("/ui/VsWinElements/VsMainView");
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.height//-(globals.platform.actualHeight)
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var tableViewsController = CreateTableViewsController();
	tableViewsController.addEventListener("openMatchDetail", function(e){
		var VsMatchDetailView = require("/ui/VsWinElements/VsMatchDetailView");
		var matchDetail = new VsMatchDetailView(e.matchData);
		tableViewsController.Add(matchDetail);
		navBar.ToggleLeftButton("back");
	});
	
	view.addEventListener("back", function(e){
		tableViewsController.Back();
		navBar.ToggleLeftButton("new");
	});
	
	view.addEventListener("newMatch", function(){
		var VsNewMatchView = require("/ui/VsWinElements/VsNewMatchView");
		var newMatch = new VsNewMatchView();
		tableViewsController.Add(newMatch);
		navBar.ToggleLeftButton("back");
	});
	
	
	var vsMainView = new VsMainView();
	
	tableViewsController.Add(vsMainView);
	
	
	view.add(navBar);
	view.add(tableViewsController);
	
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
		navBar.Close();
		
		Ti.App.removeEventListener("closePuzzlePostWin", Close);
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view, function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	Ti.App.addEventListener("closePuzzlePostWin", Close);
	
	
	return view;
};



function CreateTableViewsController (){
	var controller = Ti.UI.createView({
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
	    top: 44
	});
	
	controller.addEventListener("back", function(e){
		Back();
	});
	
	var stack = [];
	
	function Add(newTableView){
		stack.push(newTableView);
		controller.add(newTableView);
		if(stack.length >1){
			for (var i = 0; i < stack.length; i++){
				stack[i].MoveLeft();
			}	
		}
	}
	
	function Back (){
		for (var i = 0; i < stack.length; i++){
			stack[i].MoveRight();
		}
		
		setTimeout(function(){
			var last = stack[stack.length-1];
			last.Close();
			controller.remove(last);
			stack.pop();
		}, 1000);
	}
	
	function Close(){
		for(var i = 0; i < stack.length; i++){
			stack[i].Close();
		}
		stack = null;
	}
	
	controller.Add = Add;
	controller.Back = Back;
	controller.Close = Close;
	
	
	return controller;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.red,
		width: globals.platform.width,
		height: 44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		width: 44,
		height: 44,
		right: 0,
		buttonID: "vsWinCloseButton",
		touchEnabled: true
	});
	
	closeButton.addEventListener("singletap", function(){
		if(closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var newButton = Ti.UI.createView({
		width: 60,
		height: 44,
		left: 0,
		buttonID: "vsWinNewButton",
		touchEnabled: true,
		opacity: 1
	});
	
	newButton.addEventListener("singletap", function(){
		if(newButton.touchEnabled == true) view.fireEvent("newMatch");
	});
	
	newButton.add(Ti.UI.createLabel({
		text: "New",
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(newButton);
	
	
	var backButton = Ti.UI.createView({
		width: 60,
		height: 44,
		left: 0,
		buttonID: "vsWinBackButton",
		touchEnabled: true,
		opacity: 0
	});
	
	backButton.addEventListener("singletap", function(){
		if(backButton.touchEnabled == true) view.fireEvent("back");
	});
	
	backButton.add(Ti.UI.createLabel({
		text: "Back",
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(backButton);
	
	
	var title = Ti.UI.createView({
		backgroundImage:"/images/vsButton.png",
		width: 58,
		height: 38
	});
	
	view.add(title);
	view.add(closeButton);
	view.add(newButton);
	view.add(backButton);
	
	
	function ToggleLeftButton(button){
		if(button == "new") {
			newButton.opacity = 1;
			backButton.opacity = 0;
		}
		else if(button == "back") {
			newButton.opacity = 0;
			backButton.opacity = 1;
		}		
	}
	
	function Close(){}
	
	
	view.ToggleLeftButton = ToggleLeftButton;
	view.Close = Close;
	
	
	return view;
};
