var AnalyticsController = require("/controller/AnalyticsController");

function Create(){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		left: globals.platform.width,
		top: 10
	});
	
	var rows = CreateForm();
	
	var registerButton = CreateButton("Register");
	registerButton.addEventListener("touchend", function(){
		AnalyticsController.RegisterEvent({category:"design", event_id: "registerView:registerButton"});
		var userName = rows.userNameRow.textField.value.toLowerCase();
		var passWord = rows.password1Row.textField.value;
		var email = rows.emailRow.textField.value;
		
		function Check(){
			if(userName.length < 3) {
				alert('Username must be at least 3 characters');
				return false;
				
			}
			else if(userName.length > 15){
				alert('Username must less than 15 characters');
				return false;
			}	
			else okToRegister = true;
			
			if(validateUser(userName) != true){
				alert("Username can only be made of letters and numbers");
				return false;
			}
			
			if(passWord.length < 8){
				alert('Passwords must be at least 8 characters');
				return false;
			}
			
			if(rows.password1Row.textField.value != rows.password2Row.textField.value){
				alert("Your passwords don't match");
				return false;
			}
			
			if(email != null & email != ""){
				if(validateEmail(email) != true) {
					alert('Invalid E-mail Address');
					return false;
				}
			}
			
			return true;
		}	
		
		if(Check() == true){
			Common.CreateButtonBlocker(true, true);
			if(email != null & email != ""){
				globals.ServerController.Register({userName: userName, passWord: passWord, email: email, hasEmail: 1});
			}
			else {
				globals.ServerController.Register({userName: userName, passWord: passWord, hasEmail: 0});
			}
		}
	});
	
	var loginButton = Ti.UI.createLabel({
		text: "Already have an account? Click here",
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: view.width-20,
		height: 30,
		textAlign: "center",
		opacity: 0.7,
		buttonID: "loginFromRegisterButton",
		touchEnabled: true
	});
	
	Common.MakeButton(loginButton);
	
	loginButton.addEventListener("touchend", function(){
		AnalyticsController.RegisterEvent({category:"design", event_id: "registerView:loginButton"});
		view.fireEvent("changeMe", {viewToOpen: "loginView"});
	});
	
	view.add(rows);
	view.add(registerButton);
	view.add(loginButton);

	
	return view;	
}

module.exports = Create;


function CreateForm(){
	var view = Ti.UI.createView({
		backgroundImage: "/images/buttonBg.png",
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 250,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var userNameRow = CreateRow("Username", true);
	var password1Row = CreateRow("Password", true);
	var password2Row = CreateRow("Confirm Password", false);
	var emailRow = CreateRow("E-mail (Optional)", false);
	
	view.add(userNameRow);
	view.add(password1Row);
	view.add(password2Row);
	//view.add(emailRow);
	
	view.userNameRow = userNameRow;
	view.password1Row = password1Row;
	view.password2Row = password2Row;
	view.emailRow = emailRow;
	
	return view;
}


function CreateRow(hintText, hasBottomLine){
	var view = Ti.UI.createView({
		width: 250,
		height: 50
	});
	
	var textField = Ti.UI.createTextField({
		autocapitalization: false,
		autocorrect: false,
	
		hintText: hintText,
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: view.width-20,
		height: view.height
	});
	
	function TurnRed(){
		textField.color = globals.colors.red;
	}
	
	if(hasBottomLine == true) view.add(Ti.UI.createView({width: view.width-6, height: 1, bottom: 0, backgroundColor: globals.colors.black}));
	view.add(textField);
	
	view.TurnRed = TurnRed;
	view.textField = textField;
	
	return view;
};


function CreateButton(text){
	var view = Ti.UI.createView({
		backgroundImage: "/images/buttonBgRed.png",
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 250,
		height: 50,
		layout: "vertical",
		buttonID: text+"Button",
		touchEnabled: true
	});
	
	Common.MakeButton(view);
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold25,
		minimumFontSize: 20,
		color: globals.colors.white,
		width: view.width - 40,
		height: view.height,
		textAlign: "center",
	});
	
	view.add(label);
	
	
	return view;
}

function validateEmail(email){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(email) == false) 
    {
        return false;
    }

    return true;
}


function validateUser(userName){
    var reg = /^[a-z0-9]+$/i;

    if (reg.test(userName) == false) 
    {
        return false;
    }

    return true;
}
