function CreateFavoritesButton(){
	var LocalData = require("model/LocalData");
	
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 60,
		top:  200,
		left: 30,
		buttonID: "mainMenuFavoritessButton"
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap",function(e){
		Common.CreateButtonBlocker();
		var favoritesCount = globals.col.phrases.count({favorite: true});
		
		if(favoritesCount > 0){
			Flurry.logEvent('browseClicked');
			var BrowsePhrasesWin = require("ui/BrowsePhrasesWin");
			var win = BrowsePhrasesWin.loadWin();
			win.open();
		}
		else{
			alert("Add some favorites first.\n You can add a phrase to favorites once you complete it.");
		}
		Ti.App.fireEvent("closeButtonBlocker");
	});
	
	var icon = Ti.UI.createView({
		backgroundImage: "/images/favoriteIconCartoon.png",
		width: 27,
		height: 25,
		top: 0
	});
	
	view.add(icon);
	
	setTimeout(function(){
		Animator.animate(view,{
			duration: 1000,
			top: 0,
			easing: Animator["BOUNCE_OUT"]
		});
	}, 2200);
	
	
	return view;
};

function CreatePlayButton(topDifference){
	var LocalData = require("model/LocalData");
	var PhrasesController = require("controller/PhrasesController");
	
	var gameData = globals.gameData;
	
	var view = Ti.UI.createView({
		width: 150,
		height: 62,
		backgroundColor: "transparent",
		top: globals.platform.height
	});
	
	var playButton = Ti.UI.createView({
		width: 150,
		height: 62	
	});
	
	playButton.addEventListener("singletap", function(e){
		//Small Delay to make it look nice.
		setTimeout(function(){
			view.fireEvent("play");
		},200);
	});

	
	var playButtonIcon = Ti.UI.createView({
		backgroundImage: "/images/playButton.png",
		width: 109,
		height: 59,
		left: 20,
		buttonID: "mainMenuPlayButton"
	});
	Common.MakeButton(playButtonIcon);
	
	playButton.add(playButtonIcon);
	view.add(playButton);
	
	setTimeout(function(){
		Animator.animate(view,{
			duration: 1000,
			top: 220+topDifference,
			easing: Animator["EXP_OUT"]
		});
	}, 1500);
	
	
	return view;
};