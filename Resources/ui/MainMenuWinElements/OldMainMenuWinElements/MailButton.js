exports.createView = function(){
	var LocalData = require("model/LocalData");
	var Bubble = require("ui/Common/Bubble");
	var gameData = LocalData.GetGameData();
	var count =globals.col.messages.count({sent:false, complete: false});
	
	var view = Ti.UI.createView({
		width: "70dp",
		height: "70dp",
		touchEnabled: true,
		buttonID: "mainMenuMailButton"
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap",function(e){
		if (view.touchEnabled == true) setTimeout(function(){
			Common.CreateButtonBlocker(true);
			globals.rootWin.OpenWindow("mailboxWin");
			if(globals.gameData.tutorialsData.mailboxOpened == false) {
				//Timeout to avoid conflict with button and tutorial step look at Common
				setTimeout(function(){
					globals.rootWin.OpenWindow("tutorialPopUp","mailboxOpened");
					globals.gameData.tutorialsData.mailboxOpened = true;
					globals.col.gameData.commit();
				},500);
			}	
		},200);
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/mailBoxIcon.png",
		width: 46,
		height: 36,
		top: 17
	});
	
	var bubble = Bubble.createView(count);
	bubble.top = "8dp";
	
	view.add(icon);
	view.add(bubble);
	
	function UpdateMailboxIconCount(e){
		bubble.Update(e.count);
	}
	
	function Close(){
		Ti.App.removeEventListener("updateMessagesCount", UpdateMailboxIconCount);
	}
	
	view.Close = Close;
	
	Ti.App.addEventListener("updateMessagesCount", UpdateMailboxIconCount);
	UpdateMailboxIconCount({count:count});
	
	
	return view;
};
