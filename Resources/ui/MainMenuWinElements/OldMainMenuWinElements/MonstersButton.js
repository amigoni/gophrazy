exports.createView = function(){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 62,
		//bottom: -300,
		left: 30,
		//opacity: 0
	});
	
	var icon = Ti.UI.createView({
		backgroundImage: "/images/monsterCollectionButton.png",
		width: 62,
		height: 62,
		bottom: 0,
		touchEnabled: false,
		buttonID: "mainMenuMonsterButton"
	});
	
	Common.MakeButton(icon);
	
	icon.addEventListener("singletap",function(e){
		if(icon.touchEnabled == true){
			if(globals.gameData.abilities.monsters.enabled == true){
				setTimeout(function(){view.fireEvent("pressed");
				globals.rootWin.OpenWindow("monstersWin");
				},200);
			}
			else{
				setTimeout(function(){view.fireEvent("pressed");},200);
				//alert(globals.gameData.abilities.monsters.message);
			}
		}	
	});
	
	/*
	var redCircle = Ti.UI.createView({
		backgroundImage:'/"+globals.imagesPath+"/images/redCircle.png',
		width: 26,
		height: 27,
		top: 0,
		right: 0,
		visible: false
	});
	
	redCircle.add(Ti.UI.createLabel({
		text: "!",
		color: "white",
		font: globals.fonts.boldSmall,
		right: 11,
		//top: 0
	}));
	*/
	
	view.add(icon);
	//view.add(redCircle);
	
	function Update(){
		if(globals.gameData.abilities.monsters.enabled == true){
			icon.backgroundImage = "/images/monsterCollectionButton.png";
		}
		else {
			//icon.backgroundImage = "/images/monsterCollectionDisabledButton.png";
		}
	}
	
	
	view.Update = Update;
	/*
	setTimeout(function(){
		Animator.animate(view,{
			duration: 2000,
			opacity: 1,
			easing: Animator["EXP_OUT"]
		});
	}, 1600);
	*/
	
	Update();
	
	
	return view;
};