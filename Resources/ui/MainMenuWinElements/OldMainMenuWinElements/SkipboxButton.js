exports.createView = function(){
	var GroupsController = require("controller/GroupsController");
	var Bubble = require("ui/Common/Bubble");
	var skipboxGroup = GroupsController.GetskipboxGroup();
	var view = Ti.UI.createView({
		width: "55dp",
		height: "60dp",
	});
	
	var icon = Ti.UI.createView({
		backgroundImage: "/images/skipBoxIconLarge.png",
		width: 51,
		height: 45,
		bottom: 7,
		touchEnabled: true,
		buttonID: "mainMenuSkipboxButton"
	});
	
	icon.addEventListener("singletap",function(e){
		if(icon.touchEnabled == true) setTimeout(function(){
			globals.rootWin.OpenWindow("skipboxWin");
			view.fireEvent("pressed");
			if(globals.gameData.tutorialsData.skipboxOpened == false) {
				//Timeout to avoid conflict with button and tutorial step look at Common
				setTimeout(function(){
					globals.rootWin.OpenWindow("tutorialPopUp","skipboxOpened");
					globals.gameData.tutorialsData.skipboxOpened = true;
					globals.col.gameData.commit();
				},500);
			}
		},200);
	});
	
	Common.MakeButton(icon);
	
	var bubble = Bubble.createView(skipboxGroup.phrasesIDs.length);
	bubble.top = "0dp";
	if (skipboxGroup.phrasesIDs.length == 0) bubble.visible = false;
	else bubble.visible = true;
	
	
	view.add(icon);
	view.add(bubble);
	
	
	function RefreshskipboxIcon (e){
		bubble.Update(e.amount);
		if (e.amount == 0) bubble.visible = false;
		else bubble.visible = true;
	}
	
	function Close(){
		Ti.App.removeEventListener("updateskipbox", RefreshskipboxIcon);
	}
	
	
	Ti.App.addEventListener("updateskipbox", RefreshskipboxIcon);
	
	
	return view;
};