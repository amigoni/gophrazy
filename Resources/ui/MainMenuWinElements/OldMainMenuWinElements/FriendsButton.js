exports.createView = function(){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		left: 0,
		//opacity: 0,
		touchEnabled: true,
		buttonID: "mainMenuFriendsButton"
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap",function(e){
		if (view.touchEnabled == true) {
			view.fireEvent("pressed");
			globals.rootWin.OpenWindow("whyConnectWin");
		}	
	});
	
	var image = Ti.UI.createImageView({
		image: "/images/friendsIconFacebook.png",
		width: 58,
		height: 54,
		touchEnabled: false,
		bubbleParent: false
	});
	
	view.add(image);
	
	/*
	setTimeout(function(){
		Animator.animate(view,{
			duration: 2000,
			opacity: 1,
			easing: Animator["EXP_OUT"]
		});
	}, 1500);
	*/
	
	function Close(){
		
	};
	

	view.Close = Close;
	
	
	return view;
};