exports.createView = function(){
	var view = Ti.UI.createView({
		width: "80dp",
		height: "70dp"
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/collectionBookIconLarge.png",
		width: 65,
		height: 59,
		bottom: 5,
		touchEnabled: true,
		buttonID: "mainMenuPuzzleBookButton"
	});
	
	icon.addEventListener("singletap",function(e){
		if (icon.touchEnabled == true) setTimeout(function(){
			Common.CreateButtonBlocker(true);
			globals.rootWin.OpenWindow("puzzleBookWin");
			
			if(globals.gameData.tutorialsData.puzzleBookOpened == false){
				setTimeout(function(){
					globals.rootWin.OpenWindow("tutorialPopUp","puzzleBookOpened");
					globals.gameData.tutorialsData.puzzleBookOpened = true;
					globals.col.gameData.commit();
				},1000);
			}
			
		},200);
	});
	
	Common.MakeButton(icon);
	
	view.add(icon);
	
	
	return view;
};	