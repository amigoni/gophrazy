exports.createView = function() {
	var GroupsController = require("controller/GroupsController");
	var FriendsController = require("controller/FriendsController");
	var LeaderboardView = require("ui/FriendsWinElements/LeaderboardView");

	var leaderboardData = FriendsController.GetFriendsWithAppByScore();

	var view = Titanium.UI.createView({
		//backgroundImage : "/images/beigeBg.png",
		width: globals.platform.width,
		height: globals.platform.actualHeight/2-10,
		//opacity: 0,
		bubbleParent : false,
		top : "0dp",
		left: "-"+globals.platform.width,
		zIndex: 1,
		layout:"vertical"
	});

	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function() {
		Close();
	});
	
	navBar.addEventListener("openInvitePopUp", function() {
		globals.rootWin.OpenWindow("inviteWin");
	});
	
	var connectionLabel = Common.ConnectionLabel();
	var leaderboardView = CreateLeaderBoard(leaderboardData);
	
	view.add(navBar);
	view.add(connectionLabel);
	view.add(leaderboardView);
	//view.add(Ti.UI.createView({backgroundColor: "green",height:100}));
	
	

	function Open() {
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}

	function Close() {
		connectionLabel.Close();
		navBar.Close();
		DisappearAnimation();
	}

	function AppearAnimation() {
		setTimeout(function() {
			/*
			Animator.animate(view, {
				duration : 250,
				left : "0dp",
				easing : Animator['EXP_OUT']
			});
			*/
			
			view.animate(Ti.UI.createAnimation({
				duration: globals.style.animationSpeeds.speed1,
				left: "0dp",
				curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
			}));
			
		}, 200);
	}

	function DisappearAnimation() {
		/*
		Animator.animate(view, {
			duration : 150,
			left : -globals.platform.width,
			easing : Animator['EXP_IN']
		}, function() {
			view.fireEvent("closed");
		});
		*/
		
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: "-"+globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}),function(){view.fireEvent("closed");} );
	}
	
	function Update(){
		leaderboardData = FriendsController.GetFriendsWithAppByScore();
		leaderboardView.Update(leaderboardData);
	}


	view.Open = Open;
	view.Close = Close;
	
	Ti.App.addEventListener("updateLeaderboard", Update);


	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height: "44dp",
		top: "0dp"
	});
	
	var inviteButton = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height:"44dp",
		left: "0dp",
		buttonID:"rankingViewInviteButton"
	});
	
	inviteButton.add(Ti.UI.createLabel({
		text: "invite",
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.bold15,
		minimumFontSize: "10dp",
		left: "10dp"
	}));
	
	Common.MakeButton(inviteButton);
	
	inviteButton.addEventListener("singletap", function(){
		setTimeout(function(){view.fireEvent("openInvitePopUp");},200);
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: "44dp",
		height: "44dp",
		right: "0dp",
		buttonID: "rankingViewCloseButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: "12dp",
		color: globals.colors.black
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: "34dp",
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/podiumIcon.png",
		width: (46*.5).toString()+"dp",
		height: (51*.5).toString()+"dp"
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/rankingsSign.png",
		width: "114dp",
		height: "31dp",
		left: "3dp"
	}));
	
	view.add(inviteButton);
	view.add(title);
	view.add(closeButton);
	
	function Close(){
		
	}
	
	view.Close = Close;
	
	
	return view;
};


function CreateLeaderBoard(dataIn, topSpace){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		//backgroundColor: "red",
		width: globals.platform.width,
	    height: Ti.UI.FILL,
		//top: "44dp",
		separatorColor: "transparent",
		separatorStyle: false,
		opacity: 1
		//left: type == "main" ? 0 : globals.platform.width,
	});
	
	
	var tableData = [];
	
	function Update(data){
		tableData = [];
		tableView.data = null;
		
		for(var i = 0; i < data.length; i++){
			var rank = i+1;
			//if (rank == 4)rank = 99;
			var row = CreateRow(data[i],rank);
			tableData.push(row);
		}
		
		var inviteRow = CreateInviteRow();
		tableData.push(inviteRow);
		tableView.data = tableData;
	}
	
	function CreateRow(rowData,rank){
		var view = Ti.UI.createTableViewRow({
			width: globals.platform.width,
			height: "20dp",
			selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null
		});
		
		view.addEventListener("singletap", function(){
			view.fireEvent("clicked",{rowData:rowData});
		});
		
		var rankContainer = Ti.UI.createView({
			//backgroundColor: "green",
			width: Ti.UI.SIZE,
			height: "20dp",
			layout: "horizontal",
			left: "10dp"
		});
		
		var rankFont = globals.fonts.bold12;
		if (rank == 1) rankFont = globals.fonts.bold20;
		else if (rank == 2) rankFont = globals.fonts.bold15;
		else if (rank == 3) rankFont = globals.fonts.bold13;
		
		var rankLabel1 = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rank,
			font: rankFont,
			color: rowData.isMe == false ? globals.colors.black : "#BF593F",
			width: Ti.UI.SIZE,
			height: "20dp"
		});
		
		var rankLabelText = "th";
		var rankText = rank.toString();
		var lastDigit = rankText[rankText.length-1];
		if (lastDigit == "1") rankLabelText = "st";
		else if (lastDigit == "2") rankLabelText = "nd";
		if (lastDigit == "3") rankLabelText = "rd";
		
		var rankLabel2 = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rankLabelText ,
			font: globals.fonts.bold10,
			color: rowData.isMe == false ? globals.colors.black : "#BF593F",
			textAlign: "right",
			height: "20dp"
		});
		
		rankContainer.add(rankLabel1);
		rankContainer.add(rankLabel2);
		
		var nameLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.isMe == false ? rowData.name : "Me",
			font: globals.fonts.bold15,
			color: rowData.isMe == false ? globals.colors.black : "#BF593F",
			textAlign: "left",
			width: globals.platform.width-110,
			height: "20dp",
			left: "45dp"
		});
		
		var scoreLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rowData.score,
			font: globals.fonts.bold15,
			color:  rowData.isMe == false ? globals.colors.black : "#BF593F",
			textAlign: "right",
			width: "70dp",
			height: "20dp",
			right: "10dp"
		});
		
		view.add(rankContainer);
		view.add(nameLabel);
		view.add(scoreLabel);
		
		
		return view;
	}
	
	tableView.Update = Update;
	
	Update(dataIn);
	
	
	return tableView;
};


function CreateInviteRow(){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: "40dp",
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null
	});
	
	view.addEventListener("singletap", function(){
		globals.rootWin.OpenWindow("inviteWin");
	});
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: "34dp",
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/friendsIconLarge.png",
		width: "37dp",
		height: "34dp"
	}));
	
	title.add(Ti.UI.createLabel({
		text: "Invite your Friends",
		font: globals.fonts.bold15,
		color: globals.colors.black,
		textAlign: "left"
	}));
	
	view.add(title);
	
	
	return view;
};
