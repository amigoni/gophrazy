exports.createView = function(){
	var GroupsController = require("controller/GroupsController");
	var gameData = globals.gameData;
	var now = clock.getNow().valueOf();
	var lastEvent = GroupsController.GetLastEventData();
	
	var view = Ti.UI.createView({
		width: "60dp",
		height: "70dp",
		//bottom: -300,
		//right: 65
	});
	
	var icon = Ti.UI.createView({
		backgroundImage: "/images/eventsIconLarge.png",
		width: "48dp",
		height: "52dp",
		bottom: "10dp",
		touchEnabled: true,
		buttonID: "mainMenuEventsButton"
	});
	
	icon.addEventListener("singletap",function(e){
		if (icon.touchEnabled == true){
			if(globals.gameData.abilities.events.enabled == true){ 
				setTimeout(function(){view.fireEvent("pressed");},200);
			}
			else{
				setTimeout(function(){view.fireEvent("pressed");},200);
			}
		}
	});
	
	Common.MakeButton(icon);
	
	var redCircle = Ti.UI.createView({
		backgroundImage:'/"+globals.imagesPath+"/images/redCircle.png',
		width: "28dp",
		height: "29dp",
		top: "2dp",
		right: "2dp",
		visible: false
	});
	
	var redCircleLabel = Ti.UI.createLabel({
		text: "!",
		color: "white",
		font: globals.fonts.bold10,
		minimumFontSize: "7dp",
		width: "20dp",
		height: "10dp",
		left: "2dp",
		top: "7dp"
	});
	
	redCircle.add(redCircleLabel);
	
	var completeSign = Ti.UI.createImageView({
		image: "/images/completeSign.png",
		width: "45dp",
		top: "20dp",
		visible: false
	});
	
	var expiredSign = Ti.UI.createImageView({
		image: "/images/expiredSign.png",
		width: "45dp",
		top: "20dp",
		visible: false
	});
	
	var timeDisplayLabel = Ti.UI.createLabel({
		text: "20:00:55",
		color: globals.colors.textOpposite,
		font: globals.fonts.regular11,
		bottom: "0dp"
	});
	
	
	view.add(icon);
	view.add(redCircle);
	view.add(completeSign);
	view.add(expiredSign);
	view.add(timeDisplayLabel);
	
	
	function Close(){
		//Ti.App.removeEventListener("levelUp", UpdateEventsIcon);
	}
	
	function UpdateEventsIcon(){
		if(globals.gameData.abilities.events.enabled == true){
			icon.backgroundImage = "/images/eventsIconLarge.png";
			Check();
		}
		else {
			icon.backgroundImage = "/images/eventsIconLargeDisabled.png";
		}
		
		if(gameData.newEvents == true)
		{
			redCircle.visible = true;
		}
	}
	
	view.UpdateEventsIcon = UpdateEventsIcon;
	view.Close = icon.Close;
	
	
	var checkInterval;
	
	function Check(){
		checkInterval = setInterval(function(){
			now = clock.getNow();
			var endDate = new moment(lastEvent.endDate);
			
			if (lastEvent.complete == true){
				completeSign.visible = true;
				expiredSign.visible = false;
				timeDisplayLabel.visible = false;
			}
			else{
				var expired = now.isAfter(lastEvent.endDate);
				if (expired == true){
					completeSign.visible = false;
					expiredSign.visible = true;
					timeDisplayLabel.visible = false;
				}
				else{
					redCircleLabel.text = lastEvent.phrasesCompleteCount+"/"+lastEvent.phrasesCount;
					redCircle.visible = true;
					completeSign.visible = false;
					expiredSign.visible = false;
					timeDisplayLabel.text = GetTimeLeftString(now,endDate);
					timeDisplayLabel.visible = true;
				}
			}
		}, 1000);
	}
	/*
	setTimeout(function(){
		Animator.animate(view,{
			duration: 1000,
			bottom: 10,
			easing: Animator["EXP_OUT"]
		});
	}, 1600);
	*/
	
	//Ti.App.addEventListener("levelUp", UpdateEventsIcon);
	UpdateEventsIcon();
	
	
	return view;
};


var GetTimeLeftString = function(now, endDate){
	var diff = new moment.duration(endDate.diff(now));
	var text = "";
	if (diff.asSeconds() > 0){
		var days = diff.days();
		if (days == 0) days = "";
		else days = days+"d ";
		var hours = diff.hours();
		if (hours < 10) hours = "0"+hours;
		var minutes = diff.minutes();
		if (minutes < 10) minutes = "0"+minutes;
		var seconds = diff.seconds();
		if (seconds < 10) seconds = "0"+seconds;
		text = days+hours+":"+minutes+":"+seconds;
	}
	else{
		text = endDate.calendar();
	}
	
	return text;
};