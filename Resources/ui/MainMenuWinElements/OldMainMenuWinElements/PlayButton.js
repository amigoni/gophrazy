exports.createView = function(){
	var PassController = require("controller/PassController");
	var view = Ti.UI.createView({
		//backgroundColor: "green",
		width: 160,
		height: 70
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/playSignNoShadow.png",
		width: 85,
		height: 45,
		//hires: true,
		touchEnabled: true,
		buttonID: "mainMenuPlayButton",
		top: 10
	});
	
	Common.MakeButton(icon);
	
	icon.addEventListener("singletap", function(){
		if (icon.touchEnabled == true) setTimeout(function(){
			view.fireEvent("pressed");
			//globals.rootWin.OpenWindow("storePopUp", {type: "pass", callback: OpenGameWin});
			if(PassController.CheckForAccess() == true){
				OpenGameWin();
			}
			else {
				if(Titanium.Network.online == true) globals.rootWin.OpenWindow("passWin", {callback: OpenGameWin});	
				else alert("There is no internet connection.\nYou need a connection to unlock the next set of puzzles.");
			}	
		},200);
	});
	
	var countLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: globals.gameData.phrasesCompleteCountNoCookies,
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: 35,
		height: Ti.Platform.name == "iPhone OS" ? 15 : 18,
		minimumFontSize: 5,
		textAlign: "left",
		bottom:  Ti.Platform.name == "iPhone OS" ? 12 : 14,
		right: 5
		//bottom:3
		//top: group.type != "dailyChallenge" ? 4:null
	});
	
	view.add(countLabel);
	view.add(icon);
	
	
	function OpenGameWin(){
		Common.CreateButtonBlocker(true);
		var GroupsController = require("controller/GroupsController");
		var rowData = globals.col.groups.find({groupID: "main"})[0];
		var group = GroupsController.CreateGroup("general");
		//var group = GroupsController.CreateGroup("singlePhrase", 394);
		globals.rootWin.OpenWindow("gameWin", group);
		//globals.storeController.BuyStoreKitItem("skip_1",1);
	}
	
	function UpdateCount(e){
		countLabel.text = e.value;
	}
	
	function Close(){
		Ti.App.removeEventListener("updatePhrasesCompleteCount", UpdateCount);
	}
	
	view.Close = Close;
	
	Ti.App.addEventListener("updatePhrasesCompleteCount", UpdateCount);
	
	
	return view;
};
