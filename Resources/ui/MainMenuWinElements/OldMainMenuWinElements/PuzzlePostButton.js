exports.createView = function(){
	var GroupsController = require("/controller/GroupsController");
	var AnalyticsController = require("/controller/AnalyticsController");
	
	var view = Ti.UI.createView({
		width: 80,
		height: 70
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/newspaperIcon.png",
		width: 56,
		height: 42,
		bottom: 15,
		touchEnabled: true,
		buttonID: "mainMenuPuzzlePostButton"
	});
	
	icon.addEventListener("singletap",function(e){
		if (icon.touchEnabled == true) setTimeout(function(){
			AnalyticsController.RegisterEvent({category:"design", event_id: "puzzlePost:opened"});
			
			//view.fireEvent("pressed");
			Common.CreateButtonBlocker(true);
			globals.rootWin.OpenWindow("puzzlePostWin");
			if(globals.gameData.tutorialsData.puzzlePostOpened == false) {
				//Timeout to avoid conflict with button and tutorial step look at Common
				setTimeout(function(){
					globals.rootWin.OpenWindow("tutorialPopUp","puzzlePostOpened");
					globals.gameData.tutorialsData.puzzlePostOpened = true;
					//globals.col.gameData.commit();
				},500);
			}
			
			globals.col.gameData.commit();
		},200);
	});
	
	Common.MakeButton(icon);
	
	var newSign = Ti.UI.createImageView({
		image:"/images/newSign.png",
		width: 34,
		height: 25,
		top: 5,
		left: 5,
		visible: GroupsController.QueryForNewIssues().newOn
	});
	
	view.add(icon);
	view.add(newSign);
	
	
	function RefreshCollectionBookIcon (e){
		newSign.visible = e.newOn;
	}
	
	function Close(){
		//Ti.App.removeEventListener("levelUp", RefreshCollectionBookIcon);
	}
	
	view.Close = Close;
	
	//Ti.App.addEventListener("levelUp", RefreshCollectionBookIcon);
	
	/*
	setTimeout(function(){
		Animator.animate(view,{
			duration: 1000,
			bottom: 10,
			easing: Animator["EXP_OUT"]
		});
	}, 1550);
	*/
	
	Ti.App.addEventListener("UpdatePuzzlePostIcon", RefreshCollectionBookIcon);
	
	
	return view;
};
