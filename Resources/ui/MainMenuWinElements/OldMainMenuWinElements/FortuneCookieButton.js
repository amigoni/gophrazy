exports.createView = function(topDifference){
	var LocalData = require("model/LocalData");
	var DailyChallengeController = require("controller/DailyChallengeController");
	var data = DailyChallengeController.GetDailyChallengeStatusSimple();
	
	var gameData = globals.gameData;
	
	var view = Ti.UI.createView({
		width: 70,
		height: 70,
		//left: 70,
		//bottom: -300,
		bubbleParent: false,
		data: data,
		//opacity: 0.4,
		visible: true,
		touchEnabled: true,
		buttonID: "mainMenuFortuneCookieButton"
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap",function(e){
		if (view.touchEnabled == true){
			if (data.todayDailyComplete == false)
			{
				Common.CreateButtonBlocker(true);
				var GameWin = require("ui/GameWin");
				var GroupsController = require("controller/GroupsController");
				var groupData = GroupsController.CreateGroup("dailyChallenge");
				globals.rootWin.OpenWindow("gameWin",groupData);
				
				if(globals.gameData.tutorialsData.fortuneCookiesOpened == false) {
					//Timeout to avoid conflict with button and tutorial step look at Common
					setTimeout(function(){
						globals.rootWin.OpenWindow("tutorialPopUp","fortuneCookiesOpened");
						globals.gameData.tutorialsData.fortuneCookiesOpened = true;
						globals.col.gameData.commit();
					},1000);
				}	
			}
			else alert("Please come back Tomorrow.\nEvery day you get one Fortune Cookie");
		}
	});
	
	var cookieImage = Ti.UI.createImageView({
		image: "/images/fortuneCookieIconLarge.png",
		width: 46,
		height: 41,
		top: Ti.Platform.name == "iPhone OS" ? 9 : 6,
		opacity: 0.6,
		anchorPoint:{x:0.5,y:0.5},
		touchEnabled: false
	});
	
	var timeDisplayLabel = Ti.UI.createLabel({
		text: "",
		color: globals.colors.textOpposite,
		font: globals.fonts.regular11,
		bottom: 7,
		touchEnabled: false
	});
	
	var timeTitleLabel = Ti.UI.createLabel({
		text: data.todayDailyComplete == false ? "time left": "next in",
		color: globals.colors.textOpposite,
		font: globals.fonts.regular9r,
		bottom: 0,
		touchEnabled: false
	});
	
	view.add(cookieImage);
	view.add(timeTitleLabel);
	view.add(timeDisplayLabel);
	
	//Check Challenge status every second
	
	var checkInterval;
	
	function Check(){
		checkInterval = setInterval(function(){
			data = DailyChallengeController.GetDailyChallengeStatusSimple();
			timeTitleLabel.text = data.todayDailyComplete == false ? "time Left": "next in:";
			timeDisplayLabel.text = data.timeDisplayString;
			
			if (data.todayDailyComplete == false && !view.isAnimating){
				cookieImage.opacity = 1;
				/*
				cookieImage.animate(Ti.UI.createAnimation({
					duration: 50,
					autoreverse: true,
					repeat: 6,
					transform: Ti.UI.create2DMatrix().translate(-2,1).scale(1.1,1.1)
				}));
				*/
			}
			else if(data.todayDailyComplete == true){
				cookieImage.opacity = 0.6;
			}
		}, 1000);
	}
	
	
	/*
	setTimeout(function(){
		Animator.animate(view,{
			duration: 1000,
			bottom: 10,
			easing: Animator["EXP_OUT"]
		});
	}, 1550);
	*/
	
	
	function Update(){
		if(globals.gameData.abilities.fortuneCookies.enabled == true)
		{
			cookieImage.backgroundImage = "/images/fortuneCookieIconMedium.png";
			timeTitleLabel.visible = true;
			timeDisplayLabel.visible = true;
			if (checkInterval == null) Check();
		}
		else {
			cookieImage.backgroundImage = "/images/fortuneCookieIconMediumDisabled.png";
			timeTitleLabel.visible = false;
			timeDisplayLabel.visible = false;
		}
	}
	
	
	view.Update = Update;
	
	Update();
	
	
	return view;
};