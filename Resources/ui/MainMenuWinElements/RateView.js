function Create(){
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 44,
		top: -10,
		opacity: 0,
		buttonID: "mainMenuRateUsButton",
		touchEnabled: true
	});
	
	view.addEventListener("singletap", function(){
		if(view.touchEnabled == true){
			globals.gameData.ratedComplete = true;
			lobals.gameData.ratedThisVersion = true;
			globals.col.gameData.commit();
			
			setTimeout(function(){
				view.visible = false;
			}, 2000);
			
			var url;
			if(Ti.Platform.name == "iPhone OS") 
			//url = "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=903559056";
			url = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=903559056&pageNumber=0& sortOrdering=1&type=Purple+Software&mt=8";
			else if (Ti.Platform.name == "android") url = "https://play.google.com/store/apps/details?id=com.mozzarello.gophrazy&reviewId=0";
			Ti.Platform.openURL(url);
		}
	});
	
	var container = Ti.UI.createView({
		//left: "10dp",
		width: Ti.UI.SIZE,
		height: 30,
		layout:"horizontal",
		buttonID: "rateUsView"
	});
	
	Common.MakeButton(container);
	
	container.add(Ti.UI.createImageView({
		image:"/images/star.png",
		width: 26,
		height: 26
	}));
	
	container.add(Ti.UI.createLabel({
		text: "Tap to Rate Me",
		color: globals.colors.black,
		font: globals.fonts.bold12,
		//width: "260dp",//globals.platform.width - 60,
		//height: "30",
		textAlign: "center",
		verticalAlign: Ti.Platform.name == "iPhone OS" ? "center" : null,
		left: "5dp"
		//zIndex:1
	}));
	
	view.add(container);
	
	
	setTimeout(function(){
		view.animate(Ti.UI.createAnimation({
			duration: 1000,
			opacity: 1,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
		view.animate(Ti.UI.createAnimation({
			duration: 400,
			autoreverse: true,
			repeat: 999999,
			delay: 1000,
			transform: Ti.UI.create2DMatrix().scale(0.93,0.93),
			//anchorPoint: {x: 0, y: 0.5},
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}, 3000);
	
	
	
	return view;
}

module.exports = Create;