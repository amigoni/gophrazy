function Create(){
	var LogoView = require("/ui/MainMenuWinElements/LogoView");
	var viewsArray = [];
	
	var scrollView = Ti.UI.createScrollView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		contentWidth: globals.platform.width,
		contentHeight: "auto",
		top: 0
	});
	
	var container1 = Ti.UI.createView({
		width: scrollView.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	var logoContainer = new LogoView();
	
	var playContainer = Ti.UI.createView({
		width: scrollView.width,
		height: Ti.UI.SIZE,
		top: 10
	});
	
	var playBG = Ti.UI.createView({
		backgroundImage:"/images/buttonBg.png",
		backgroundLeftCap: 20,
		backgroundTopCap: 20,
		width: scrollView.width-15,
		height: 80
	});
	
	var playInsideContainer = Ti.UI.createView({
		width: scrollView.width-10,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	playInsideContainer.addEventListener("postlayout", function(){
		playBG.height = playInsideContainer.size.height;
	});
	
	var playHeader = Ti.UI.createView({
		width: scrollView.width-10,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 10
	});
	
	var playLabel = Ti.UI.createLabel({
		text: "Play",
		font: globals.fonts.bold25,
		color: globals.colors.black,
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		textAlign: "center",
	});
	
	var playDescription = Ti.UI.createLabel({
		text: "Choose a way to play",
		font: globals.fonts.regular11,
		color: globals.colors.black,
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		textAlign: "center",
		top: -5
	});
	
	playHeader.add(playLabel);
	playHeader.add(playDescription);
	
	playInsideContainer.add(playHeader);
	
	var DailyChallengeController = require("controller/DailyChallengeController");
	var dailyChallengeState = DailyChallengeController.GetDailyChallengeStatusSimple();
	
	var playRowsData = [
		{buttonID: "randomPuzzleRow", title: "Random Puzzle", icon: "randomPuzzleIcon.png", iconHeight: 30, description: "Play a random puzzle", callback: RandomPuzzleCallback},
		{buttonID: "discoverPuzzleRow", title: "Discover Community Puzzles", icon: "discoverPuzzlesIcon.png", iconHeight: 25, description: "Solve puzzles written by people like you", callback: DiscoverCallback}
	
	];
	//if(globals.gameData.abilities.puzzlePost.enabled == true) playRowsData.unshift({buttonID: "weeklyPuzzlePackRow", title: "Weekly Puzzle Pack", icon: "newspapericon.png", iconHeight: 35, description: "Fresh, amazing new puzzles every week", callback: WeeklyPuzzlePackCallback});
	if(globals.gameData.abilities.fortuneCookies.enabled == true) playRowsData.unshift({buttonID: "fortuneCookieRow", title: "Fortune Cookie", icon: "fortuneCookieIconLarge.png", iconHeight: 35, description: "Your daily fortune and challenge", callback: FortuneCookieCallback});
		
	
	if (dailyChallengeState.todayDailyComplete == true) playRowsData.push(playRowsData.shift());
	
	for(var i = 0; i < playRowsData.length; i++){
		if(playRowsData[i].buttonID == "fortuneCookieRow"){
			var fortuneCookieRow = CreatePlayRow(playRowsData[i]);
			var dailyChallengeStatus = DailyChallengeController.GetDailyChallengeStatusSimple();
			
			var labelContainer = Ti.UI.createView({
				width: Ti.UI.SIZE,
				height: 15,
				top: 10,
				right: 10,
				layout: "horizontal"
			}); 
			
			var timeDisplayLabel = Ti.UI.createLabel({
				text: "",
				color: globals.colors.textOpposite,
				font: globals.fonts.regular11,
				touchEnabled: false
			});
			
			var timeTitleLabel = Ti.UI.createLabel({
				text: dailyChallengeStatus.todayDailyComplete == false ? "time left": "next in",
				color: globals.colors.textOpposite,
				font: globals.fonts.regular9r,
				textAlign: "right",
				touchEnabled: false
			});
			
			labelContainer.add(timeDisplayLabel);
			labelContainer.add(timeTitleLabel);
			
			fortuneCookieRow.add(labelContainer);
			
			var checkInterval = setInterval(function(){
				dailyChallengeStatus = DailyChallengeController.GetDailyChallengeStatusSimple();
				timeTitleLabel.text = dailyChallengeStatus.todayDailyComplete == false ? "time Left": "next in";
				timeDisplayLabel.text = dailyChallengeStatus.timeDisplayString;
				
				if (dailyChallengeStatus.todayDailyComplete == false){
					fortuneCookieRow.opacity = 1;
				}
				else if(dailyChallengeStatus.todayDailyComplete == true){
					fortuneCookieRow.opacity = 0.6;
				}
			}, 1000);
	
			fortuneCookieRow.Close = function(){clearInterval(checkInterval);};
			
			playInsideContainer.add(fortuneCookieRow);
			viewsArray.push(fortuneCookieRow);
		}
		else if(playRowsData[i].buttonID == "weeklyPuzzlePackRow"){
			var weeklyPackRow = CreatePlayRow(playRowsData[i]);
			
			var GroupsController = require("/controller/GroupsController");
			var newSign = Ti.UI.createImageView({
				image:"/images/newSign.png",
				width: 34,
				height: 25,
				top: 5,
				left: 5,
				visible: GroupsController.QueryForNewIssues().newOn
			});
			
			weeklyPackRow.iconContainer.add(newSign);
			
			function RefreshCollectionBookIcon (e){
				newSign.visible = e.newOn;
			}
			
			weeklyPackRow.Close = function(){
				Ti.App.removeEventListener("UpdatePuzzlePostIcon", RefreshCollectionBookIcon);
			};
			
			Ti.App.addEventListener("UpdatePuzzlePostIcon", RefreshCollectionBookIcon);
			playInsideContainer.add(weeklyPackRow);
			viewsArray.push(weeklyPackRow);
		}
		else if(playRowsData[i].buttonID == "randomPuzzleRow"){
			var randomRow = CreatePlayRow(playRowsData[i]);
			var total = globals.col.phrases.count({includeInGeneral:true});
			var solved = globals.col.phrases.count({includeInGeneral:true, complete: true});
			var puzzleCountLabel = Ti.UI.createLabel({
				text: solved+"/"+total,
				font: globals.fonts.bold15,
				color: globals.colors.black,
				top: 8,
				right: 10
			});
			
			randomRow.add(puzzleCountLabel);
			
			puzzleCountLabel.UpdateCount = function(){
				total = globals.col.phrases.count({includeInGeneral:true});
				solved = globals.col.phrases.count({includeInGeneral:true, complete: true});
				puzzleCountLabel.text = solved+"/"+total;
			};
			
			randomRow.Close = function(){
				Ti.App.removeEventListener("updatePhrasesCompleteCount", puzzleCountLabel.UpdateCount);
			};
			
			Ti.App.addEventListener("updatePhrasesCompleteCount", puzzleCountLabel.UpdateCount);
			playInsideContainer.add(randomRow);
			viewsArray.push(randomRow);
		}
		else if(playRowsData[i].buttonID == "discoverPuzzleRow"){
			var discoverPuzzleRow = CreatePlayRow(playRowsData[i]);
			playInsideContainer.add(discoverPuzzleRow);
			viewsArray.push(discoverPuzzleRow);
		}
	}
	playInsideContainer.add(Ti.UI.createView({height: 20}));
	
	playContainer.add(playBG);
	playContainer.add(playInsideContainer);
	
	container1.add(logoContainer);
	container1.add(playContainer);
	
	var rowsData = [
		{buttonID: "puzzleFactoryRow", title: "Puzzle Factory", icon: "PuzzleFactoryIcon.png", iconHeight: 36, description: "Create & Share your own puzzles", callback: PuzzleFactoryCallback},
		//{buttonID: "puzzleMailboxRow", title: "Puzzle Mailbox", icon: "mailBoxIcon.png", iconHeight: 30, description: "Send & Receive Puzzles from friends", callback: PuzzleMailboxCallback},
		{buttonID: "solvedPuzzlesRow", title: "Solved Puzzles", icon: "collectionBookIconLarge.png", iconHeight: 40, description: "Collect all the puzzles out there", callback: SolvedPuzzlesCallback},
		{buttonID: "skippedBoxRow", title: "Skipped Box", icon: "skipBoxIconLarge.png", iconHeight: 32, description: "Home of your skipped puzzles", callback: SkippedBoxCallback},
		//{buttonID: "achievementsRow", title: "Achievements", icon: "checkMarkButton.png", iconHeight: 30, description: "Complete all the achievements", callback: AchievementsCallback},
		{buttonID: "rateUs", title: "Rate us", icon: "star.png", iconHeight: 30, description: "Rate us and spread the word", callback: RateUs},
		{buttonID: "facebookLikeRow", title: "Like us on Facebook", icon: "facebookSquareIcon.png", iconHeight: 35, description: "Like us to get the latests news.", callback: LikeUsOnFacebook},
		{buttonID: "followTwitter", title: "Follow us on Twitter", icon: "twitterIcon.png", iconHeight: 30, description: "Follow us to get the latests news.", callback: FollowUsOnTwitter},
		{buttonID: "settingsRow", title: "Settings", icon: "settingsIconSmall.png", iconHeight: 30, description: "Music, sounds, notifications etc.", callback: SettingsCallback},
		{buttonID: "feedbackRow", title: "Feedback & Support", icon: "feedbackIconSmall.png", iconHeight: 25, description: "Got an issue or suggestion? Talk to us", callback: FeedbackCallback},
	];
	
	for(var i = 0; i < rowsData.length; i++){
		if(rowsData[i].buttonID == "puzzleFactoryRow"){
			var factoryRow = CreateRow(rowsData[i]);
			
			var Bubble = require("ui/Common/Bubble");
			var PuzzleFactoryController = require("controller/PuzzleFactoryController");
			var count = PuzzleFactoryController.GetTotalSolvedCountDifference();
		
			var solvedBubble = Bubble.createView(count);
			solvedBubble.top = 2;
			solvedBubble.right = 5;
			
			factoryRow.iconContainer.add(solvedBubble);
			
			factoryRow.UpdateCount = function(){
				solvedBubble.Update(PuzzleFactoryController.GetTotalSolvedCountDifference());
			};
			
			factoryRow.Close = function(){
				Ti.App.removeEventListener("updateSolvedByOthersCount", factoryRow.UpdateCount);
			};
			
			Ti.App.addEventListener("updateSolvedByOthersCount", factoryRow.UpdateCount);
			
			container1.add(factoryRow);
			viewsArray.push(factoryRow);
		}
		else if(rowsData[i].buttonID == "puzzleMailboxRow"){
			var mailRow = CreateRow(rowsData[i]);
			var Bubble = require("ui/Common/Bubble");
			var count =globals.col.messages.count({sent:false, complete: false});
			var mailBubble = Bubble.createView(0);
			mailBubble.top = 2;
			mailBubble.right = 5;
			
			mailRow.iconContainer.add(mailBubble);
			
			function UpdateMailboxIconCount(e){
				mailBubble.Update(e.count);
			}
			
			mailRow.Close = function(){
				Ti.App.removeEventListener("updateMessagesCount", UpdateMailboxIconCount);
			};
			
			Ti.App.addEventListener("updateMessagesCount", UpdateMailboxIconCount);
			UpdateMailboxIconCount({count:count});
			container1.add(mailRow);
			viewsArray.push(mailRow);
		}
		else if (rowsData[i].buttonID == "solvedPuzzlesRow"){
			var solvedRow = CreateRow(rowsData[i]);
			var solvedCountLabel = Ti.UI.createLabel({
				text: globals.gameData.phrasesCompleteCount,
				font: globals.fonts.bold15,
				color: globals.colors.black,
				top: 8,
				right: 10
			});
			
			solvedRow.add(solvedCountLabel);
			
			function UpdateCount(e){
				solvedCountLabel.text = e.value;
			}
			
			solvedRow.Close = function(){
				Ti.App.removeEventListener("updatePhrasesCompleteCount", UpdateCount);
			};
			
			Ti.App.addEventListener("updatePhrasesCompleteCount", UpdateCount);
			container1.add(solvedRow);
			viewsArray.push(solvedRow);
		}
		else if (rowsData[i].buttonID == "achievementsRow"){
			var achievementsRow = CreateRow(rowsData[i]);
			var achievementsCountLabel = Ti.UI.createLabel({
				text: globals.col.achievements.count({complete: true})+"/"+globals.col.achievements.getAll().length,
				font: globals.fonts.bold15,
				color: globals.colors.black,
				top: 8,
				right: 10
			});
			
			achievementsRow.add(achievementsCountLabel);
			
			function UpdateAchievementCount(e){
				//Ti.API.info(e);
				achievementsCountLabel.text = e.value+"/"+globals.col.achievements.getAll().length;
			}
			
			achievementsRow.Close = function(){
				Ti.App.removeEventListener("updateAchievementCompleteCount", UpdateAchievementCount);
			};
			
			Ti.App.addEventListener("updateAchievementCompleteCount", UpdateCount);
			container1.add(achievementsRow);
			viewsArray.push(achievementsRow);
		}
		else if (rowsData[i].buttonID == "skippedBoxRow"){
			var skippedRow = CreateRow(rowsData[i]);
			var GroupsController = require("controller/GroupsController");
			var Bubble = require("ui/Common/Bubble");
			var skipboxGroup = GroupsController.GetskipboxGroup();
			
			var skipBubble = Bubble.createView(skipboxGroup.phrasesIDs.length);
			skipBubble.top = 2;
			skipBubble.right = 5;
			if (skipboxGroup.phrasesIDs.length == 0) skipBubble.visible = false;
			else skipBubble.visible = true;
			
			skippedRow.iconContainer.add(skipBubble);
			
			function RefreshskipboxIcon (e){
				skipBubble.Update(e.amount);
				if (e.amount == 0) skipBubble.visible = false;
				else skipBubble.visible = true;
			}
			
			skippedRow.Close = function(){
				Ti.App.removeEventListener("updateskipbox", RefreshskipboxIcon);
			};
	
			Ti.App.addEventListener("updateskipbox", RefreshskipboxIcon);
			container1.add(skippedRow);
			viewsArray.push(skippedRow);
		}
		else if (rowsData[i].buttonID == "settingsRow"){
			var settingsRow = CreateRow(rowsData[i]);
			container1.add(settingsRow);
			viewsArray.push(settingsRow);
		}
		else if (rowsData[i].buttonID == "feedbackRow"){
			var feedbackRow = CreateRow(rowsData[i]);
			container1.add(feedbackRow);
			viewsArray.push(feedbackRow);
		}
		else if (rowsData[i].buttonID == "facebookLikeRow"){
			var facebookLikeRow = CreateRow(rowsData[i]);
			container1.add(facebookLikeRow);
			viewsArray.push(facebookLikeRow);
		}
		else if (rowsData[i].buttonID == "followTwitter"){
			var followTwitter = CreateRow(rowsData[i]);
			container1.add(followTwitter);
			viewsArray.push(followTwitter);
		}
		else if (rowsData[i].buttonID == "rateUs"){
			var rateUs = CreateRow(rowsData[i]);
			container1.add(rateUs);
			viewsArray.push(rateUs);
		}
	}
	
	scrollView.add(container1);
	
	function Close(){
		for(var i = 0; i < viewsArray.length; i++){
			viewsArray[i].Close();
		}
		
		viewsArray = [];
	}
	
	scrollView.Close = Close;
	
	return scrollView;
}

module.exports = Create;


function CreatePlayRow(data){
	var view = Ti.UI.createView({
		width: globals.platform.width-30,
		height: 50,
		buttonID: data.buttonID,
		touchEnabled: true
	});
	
	Common.MakeButton(view, true);
	
	view.addEventListener("touchend", function(){
		if(view.touchEnabled == true) data.callback();
	});
	
	var iconContainer = Ti.UI.createView({
		width: 80,
		height: view.height,
		left: 0
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/"+data.icon,
		height: data.iconHeight
	});
	
	iconContainer.add(icon);
	
	var labelContainer = Ti.UI.createView({
		width: view.width - 90,
		height: Ti.UI.SIZE,
		left: 80,
		layout: "vertical"
	});
	
	var title = Ti.UI.createLabel({
		text: data.title,
		font: globals.fonts.bold15,
		minimumFontSize: 12,
		color: globals.colors.black,
		width: labelContainer.width,
		height: 20,
		textAlign: "left",
	});
	
	var description = Ti.UI.createLabel({
		text: data.description,
		font: globals.fonts.regular11,
		color: globals.colors.black,
		width: labelContainer.width,
		height: Ti.UI.SIZE,
		textAlign: "left",
		top:-3
	});
	
	labelContainer.add(title);
	labelContainer.add(description);
	
	view.add(iconContainer);
	view.add(labelContainer);
	
	
	function Close(){
		
	}
	
	view.icon = icon;
	view.iconContainer = iconContainer;
	view.Close = Close;
	
	return view;
}


function CreateRow(data){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: 50,
		buttonID: data.buttonID,
		touchEnabled: true
	});
	
	Common.MakeButton(view, true);
	
	view.addEventListener("touchend", function(){
		if(view.touchEnabled == true) data.callback();
	});
	
	var iconContainer = Ti.UI.createView({
		width: 80,
		height: view.height,
		left: 0
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/"+data.icon,
		height: data.iconHeight
	});
	
	iconContainer.add(icon);
	
	var labelContainer = Ti.UI.createView({
		width: view.width - 80,
		height: Ti.UI.SIZE,
		left: 80,
		layout: "vertical"
	});
	
	var title = Ti.UI.createLabel({
		text: data.title,
		font: globals.fonts.bold20,
		color: globals.colors.black,
		width: labelContainer.width,
		height: 23,
		textAlign: "left",
	});
	
	var description = Ti.UI.createLabel({
		text: data.description,
		font: globals.fonts.regular11,
		color: globals.colors.black,
		width: labelContainer.width,
		height: Ti.UI.SIZE,
		textAlign: "left",
		top:-3
	});
	
	labelContainer.add(title);
	labelContainer.add(description);
	
	//view.add(Ti.UI.createView({width: view.width, height: 1, top: 0, backgroundColor: "black", opacity: .2}));
	view.add(Ti.UI.createView({width: view.width, height: 1, bottom: 0, backgroundColor: "black", opacity: .3}));
	view.add(iconContainer);
	view.add(labelContainer);
	
	function Close(){
		
	}
	
	view.icon = icon;
	view.iconContainer = iconContainer;
	view.Close = Close;
	
	return view;
}



function RandomPuzzleCallback(){
	var PassController = require("controller/PassController");
	var OpenGameWin = function (){
		Common.CreateButtonBlocker(true);
		var GroupsController = require("controller/GroupsController");
		var rowData = globals.col.groups.find({groupID: "main"})[0];
		var group = GroupsController.CreateGroup("general");
		//var group = GroupsController.CreateGroup("singlePhrase", 394);
		globals.rootWin.OpenWindow("gameWin", group);
		//globals.storeController.BuyStoreKitItem("skip_1",1);
	};
	
	if(PassController.CheckForAccess() == true){
		OpenGameWin();
	}
	else{
		if(Titanium.Network.online == true) globals.rootWin.OpenWindow("passWin", {callback: OpenGameWin});	
		else alert("There is no internet connection.\nYou need a connection to unlock the next set of puzzles.");
	}	
}


function WeeklyPuzzlePackCallback(){
	setTimeout(function(){
		AnalyticsController.RegisterEvent({category:"design", event_id: "puzzlePost:opened"});
		
		//view.fireEvent("pressed");
		Common.CreateButtonBlocker(true);
		globals.rootWin.OpenWindow("puzzlePostWin");
		
		if(globals.gameData.tutorialsData.puzzlePostOpened == false) {
			//Timeout to avoid conflict with button and tutorial step look at Common
			setTimeout(function(){
				globals.rootWin.OpenWindow("tutorialPopUp","puzzlePostOpened");
				globals.gameData.tutorialsData.puzzlePostOpened = true;
				//globals.col.gameData.commit();
			},500);
		}
		
		globals.col.gameData.commit();
	},200);
}


function FortuneCookieCallback(){
	var DailyChallengeController = require("controller/DailyChallengeController");
	var data = DailyChallengeController.GetDailyChallengeStatusSimple();
	if (data.todayDailyComplete == false)
	{
		Common.CreateButtonBlocker(true);
		var GameWin = require("ui/GameWin");
		var GroupsController = require("controller/GroupsController");
		var groupData = GroupsController.CreateGroup("dailyChallenge");
		globals.rootWin.OpenWindow("gameWin",groupData);
		
		if(globals.gameData.tutorialsData.fortuneCookiesOpened == false) {
			//Timeout to avoid conflict with button and tutorial step look at Common
			setTimeout(function(){
				globals.rootWin.OpenWindow("tutorialPopUp","fortuneCookiesOpened");
				globals.gameData.tutorialsData.fortuneCookiesOpened = true;
				globals.col.gameData.commit();
			},1000);
		}	
	}
	else alert("Please come back Tomorrow.\nEvery day you get one Fortune Cookie");
}


function PuzzleMailboxCallback(){
	setTimeout(function(){
		Common.CreateButtonBlocker(true);
		globals.rootWin.OpenWindow("mailboxWin");
		if(globals.gameData.tutorialsData.mailboxOpened == false) {
			//Timeout to avoid conflict with button and tutorial step look at Common
			setTimeout(function(){
				globals.rootWin.OpenWindow("tutorialPopUp","mailboxOpened");
				globals.gameData.tutorialsData.mailboxOpened = true;
				globals.col.gameData.commit();
			},500);
		}	
	},200);
}


function PuzzleFactoryCallback(){
	setTimeout(function(){
		Common.CreateButtonBlocker(true);
		globals.rootWin.OpenWindow("puzzleFactoryWin");
		/*
		if(globals.gameData.tutorialsData.mailboxOpened == false) {
			//Timeout to avoid conflict with button and tutorial step look at Common
			setTimeout(function(){
				globals.rootWin.OpenWindow("tutorialPopUp","mailboxOpened");
				globals.gameData.tutorialsData.mailboxOpened = true;
				globals.col.gameData.commit();
			},500);
		}
		*/	
	},200);
}


function SolvedPuzzlesCallback(){
	setTimeout(function(){
		Common.CreateButtonBlocker(true);
		globals.rootWin.OpenWindow("puzzleBookWin");
		if(globals.gameData.tutorialsData.puzzleBookOpened == false){
			setTimeout(function(){
				globals.rootWin.OpenWindow("tutorialPopUp","puzzleBookOpened");
				globals.gameData.tutorialsData.puzzleBookOpened = true;
				globals.col.gameData.commit();
			},1000);
		}
		
	},200);
}


function SkippedBoxCallback(){
	setTimeout(function(){
		globals.rootWin.OpenWindow("skipboxWin");
		//view.fireEvent("pressed");
		if(globals.gameData.tutorialsData.skipboxOpened == false) {
			//Timeout to avoid conflict with button and tutorial step look at Common
			setTimeout(function(){
				globals.rootWin.OpenWindow("tutorialPopUp","skipboxOpened");
				globals.gameData.tutorialsData.skipboxOpened = true;
				globals.col.gameData.commit();
			},500);
		}
	},200);
}


function AchievementsCallback(){
	globals.rootWin.OpenWindow("achievementsWin");
}


function SettingsCallback(){
	globals.rootWin.OpenWindow("settingsWin");
}


function FeedbackCallback(){
	globals.rootWin.OpenWindow("feedbackPopUp");
}


function LikeUsOnFacebook(){
	Ti.Platform.openURL("fb://profile/132556283503835");
}

function FollowUsOnTwitter(){
	Ti.Platform.openURL("twitter:///user?screen_name=mozzarelloapps");
}

function RateUs(){
	globals.gameData.ratedComplete = true;
	globals.gameData.ratedThisVersion = true;
	globals.col.gameData.commit();
			
	var url;
	if(Ti.Platform.name == "iPhone OS") 
	//url = "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=903559056";
	url = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=903559056&pageNumber=0& sortOrdering=1&type=Purple+Software&mt=8";
	else if (Ti.Platform.name == "android") url = "https://play.google.com/store/apps/details?id=com.mozzarello.gophrazy&reviewId=0";
	Ti.Platform.openURL(url);
}

function DiscoverCallback(){
	globals.rootWin.OpenWindow("discoverWin");
}

