function Create(){
	var TipLabel = require("ui/MainMenuWinElements/TipLabel");
	
	var scaleMultiplier = 1;
	if(globals.platform.scale == 1) scaleMultiplier = globals.platform.width/320;
	
	var view = Ti.UI.createView({
		width: globals.platform.width-20,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 10,
		//transform: Ti.UI.create2DMatrix().scale(scaleMultiplier, scaleMultiplier)
	});
	
	var logo = Ti.UI.createImageView({
		image:"/images/wordHopLogo.png",
		width: 299*.8*scaleMultiplier,
		height: 92*.8*scaleMultiplier
	});
	
	var tipLabel = TipLabel.createView();
	
	view.add(logo);
	view.add(tipLabel);
	
	
	return view;
};

module.exports = Create;