var AnalyticsController = require("/controller/AnalyticsController");

function Create(){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		left: globals.platform.width,
		top: 10
	});
	
	var rows = CreateForm();
	
	var loginButton = CreateButton("Login");
	loginButton.addEventListener("touchend", function(){
		Common.CreateButtonBlocker(true, true);
		AnalyticsController.RegisterEvent({category:"design", event_id: "loginView:loginButton"});
		var userName = rows.userNameRow.textField.value.toLowerCase();
		var passWord = rows.password1Row.textField.value;
		
		globals.ServerController.Login({userName: userName, passWord: passWord});
	});
	
	var forgotButton = CreateButton("Forgot Password?");
	
	forgotButton.addEventListener("touchend", function(){
		AnalyticsController.RegisterEvent({category:"design", event_id: "loginView:forgotPasswordButton"});
		//view.fireEvent("changeMe", {viewToOpen: "forgotPasswordView"});
		var emailDialog = Ti.UI.createEmailDialog();
		emailDialog.subject = "I forgot my GoPhrazy Password" ;
		emailDialog.toRecipients = ['support@mozzarello.com'];
		emailDialog.messageBody = "Hi, I forgot my GoPhrazy Password can you help me reset it?";
		//emailDialog.addAttachment(args.picture);
		emailDialog.open();
	});
	
	var registerButton = Ti.UI.createLabel({
		text: "Don't have an account? Click here",
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: view.width-20,
		height: 30,
		textAlign: "center",
		opacity: 0.7,
		buttonID: "loginFromRegisterButton",
		touchEnabled: true
	});
	
	Common.MakeButton(registerButton);
	
	registerButton.addEventListener("touchend", function(){
		AnalyticsController.RegisterEvent({category:"design", event_id: "loginView:registerButton"});
		view.fireEvent("changeMe", {viewToOpen: "registerView"});
	});
	
	view.add(rows);
	view.add(loginButton);
	view.add(forgotButton);
	view.add(registerButton);
	
	
	return view;	
}

module.exports = Create;


function CreateForm(){
	var view = Ti.UI.createView({
		backgroundImage: "/images/buttonBg.png",
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 250,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var userNameRow = CreateRow("Username", true);
	var password1Row = CreateRow("Password", false);
	
	view.add(userNameRow);
	view.add(password1Row);
	
	view.userNameRow = userNameRow;
	view.password1Row = password1Row;
	
	return view;
}


function CreateRow(hintText, hasBottomLine){
	var view = Ti.UI.createView({
		width: 250,
		height: 50
	});
	
	var textField = Ti.UI.createTextField({
		autocapitalization: false,
		autocorrect: false,
	
		hintText: hintText,
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: view.width-20,
		height: view.height
	});
	
	function TurnRed(){
		textField.color = globals.colors.red;
	}
	
	if(hasBottomLine == true) view.add(Ti.UI.createView({width: view.width-6, height: 1, bottom: 0, backgroundColor: globals.colors.black}));
	view.add(textField);
	
	view.TurnRed = TurnRed;
	view.textField = textField;
	
	return view;
};


function CreateButton(text){
	var view = Ti.UI.createView({
		backgroundImage: "/images/buttonBgRed.png",
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 250,
		height: 50,
		layout: "vertical",
		buttonId: "authentication"+text,
		touchEnabled: true
	});
	
	Common.MakeButton(view);
	
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold25,
		minimumFontSize: 20,
		color: globals.colors.white,
		width: view.width - 40,
		height: view.height,
		textAlign: "center",
	});
	
	view.add(label);
	
	
	return view;
}
