function Create(startView){
	var LogoView = require("/ui/MainMenuWinElements/LogoView");
	var RegisterView = require("/ui/MainMenuWinElements/RegisterView");
	var LoginView = require("/ui/MainMenuWinElements/LoginView");
	var ForgotPasswordView = require("/ui/MainMenuWinElements/ForgotPasswordView");
	var registerView;
	var loginView;
	var forgotPasswordView;
	var lastView;
		
	var view = Ti.UI.createScrollView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		contentWidth: globals.platform.width,
		contentHeight: "auto"
	});
	
	view.addEventListener("changeMe", function(e){
		Update(e.viewToOpen);
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: 401,
		layout: "vertical"
	});
	
	var logoView = new LogoView();
	
	var bottomContainer = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE
	});
	
	container.add(logoView);
	container.add(bottomContainer);
	view.add(container);
	
	
	function Update(viewToOpen){
		if(lastView != null) bottomContainer.remove(lastView);
		registerView = null;
		loginView = null;
		forgotPasswordView = null;
		
		if(viewToOpen == "registerView"){
			registerView = new RegisterView();
			lastView = registerView;
		}
		else if(viewToOpen == "loginView"){
			loginView = new LoginView();
			lastView = loginView;
		}
		else if(viewToOpen == "forgotPasswordView"){
			forgotPasswordView = new ForgotPasswordView();
			lastView = forgotPasswordView;
		}
		
		bottomContainer.add(lastView);
		
		lastView.animate(Ti.UI.createAnimation({
			duration: 250,
			left: 0
		}));
		
		//Weird, the animation doesn't fire when it's first created.
		setTimeout(function(){if(lastView.left != 0) lastView.left = 0;},250);
	}
	
	function UserAuthenticationCallback(e){
		if(e.state == "Registered") {
			Close();
		}
		else if (e.state == "LoggedIn"){
			Close();
		}
		else if(e.state == "UserNameTaken"){
			alert("That Username is taken.\n Please choose a different one.");
		}
	}
	
	function Close(){
		Ti.App.removeEventListener("UserAuthentication", UserAuthenticationCallback);
		view.fireEvent("closeMe");
	}
	
	Ti.App.addEventListener("UserAuthentication", UserAuthenticationCallback);
	
	Update(startView);
	
	
	return view;
}

module.exports = Create;
