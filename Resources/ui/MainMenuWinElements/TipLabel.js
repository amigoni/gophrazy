exports.createView = function(){
	var RandomPlus = require("lib/RandomPlus");
	var shuffleBag = RandomPlus.ShuffleBagStandard();	
	/*
	var tips = [
		{text: 'Semicolons ";" are used only in the middle of a puzzle.', icon: null},
		{text: 'All puzzles start with a Capitalized word.', icon: null},
		{text: 'Every puzzle ends with either "."  "?" or "!"', icon: null},
		{text: 'to compete with your friends.', icon: "facebookConnectIcon.png"},
		{text: 'to send puzzles to your friends.', icon: "facebookConnectIcon.png"},
		{text: 'to compose your own puzzles.', icon: "facebookConnectIcon.png"}
	];
	
	for (var i=0; i < tips.length; i++){
		if(globals.gameData.userFacebookID == ""){
			shuffleBag.add(tips[i]);
		}
		else{
			if(tips[i].icon == null) shuffleBag.add(tips[i]);
		}
	}
	*/
	
	var tips = globals.gameData.tips;
	
	for (var i = 0; i < tips.length; i++){
		shuffleBag.add(tips[i]);
	}
	
	var view = Ti.UI.createView({
		width: globals.platform.width-20,
		height: 50,
		//layout: "horizontal",
		top: 10,
		opacity: 1
	});
	
	var icon = Ti.UI.createImageView({
		//backgroundColor: "blue",
		image:'/images/facebookConnectIcon.png',
		width: 54,
		height: 15,
		//zIndex: -1
		opacity: 0
	});
	
	var label = Ti.UI.createLabel({
		text: "",
		color: "black",//globals.colors.tipFont,
		font: globals.fonts.bold13,
		width: view.width,
		height: 70,
		textAlign: "center",
		verticalAlign: Ti.Platform.name == "iPhone OS" ? "center" : null,
		opacity: 0,
		//left: 5,
		//zIndex:1
	});
	
	//view.add(icon);
	view.add(label);
	
	function Appear(){
		Animator.animate(label, {
			duration: 1000,
			opacity: 0.4
		});
		Animator.animate(icon, {
			duration: 1000,
			opacity: 1
		});
	}
	
	function Disappear(){
		Animator.animate(label, {
			duration: 1000,
			opacity: 0
		});
		Animator.animate(icon, {
			duration: 1000,
			opacity: 0
		});
	}
	
	function ChangeText(){
		var next = shuffleBag.next();
		label.text = next.text;
		if(next.icon == null) {
			icon.width = 0;
			label.width = 260;
			//label.textAlign = "center";
		}	
		else {
			icon.width = 54;
			label.width = Ti.UI.SIZE;
		}	
	}
	
	setInterval(function(){
		Disappear();
		setTimeout(ChangeText,1000);
		setTimeout(Appear,1500);
	},7000);
	
	
	setTimeout(Appear,2000);
	
	ChangeText();
	
	
	return view;
};
