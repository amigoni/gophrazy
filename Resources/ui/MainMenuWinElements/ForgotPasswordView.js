var AnalyticsController = require("/controller/AnalyticsController");

function Create(){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		left: globals.platform.width
		//bottom: 30
	});
	
	var forgotLabel = Ti.UI.createLabel({
		text: "Enter your e-mail to reset your password",
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: view.width-20,
		height: 30,
		textAlign: "center",
		opacity: 0.7
	});
	
	var rows = CreateForm();
	
	var resetPasswordButton = CreateButton("Reset Password");
	
	resetPasswordButton.addEventListener("touchend", function(e){
		AnalyticsController.RegisterEvent({category:"design", event_id: "forgotPasswordView:resetPasswordButton"});
		if(validateEmail(rows.emailRow.textField.value) == true){
			Common.CreateButtonBlocker(true, true);
		}
		else alert("Your e-mail doesn't seem to be valid.\nPlease check it again.");
	});
	
	var loginButton = Ti.UI.createLabel({
		text: "Got your new password? Click here",
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: view.width-20,
		height: 30,
		textAlign: "center",
		opacity: 0.7,
		buttonID: "loginFromForgotPasswordViewButton",
		touchEnabled: true
	});
	
	Common.MakeButton(loginButton);
	
	loginButton.addEventListener("touchend", function(){
		AnalyticsController.RegisterEvent({category:"design", event_id: "forgotPasswordView:loginButton"});
		view.fireEvent("changeMe", {viewToOpen: "loginView"});
	});
	
	
	view.add(forgotLabel);
	view.add(rows);
	view.add(resetPasswordButton);
	view.add(loginButton);
	
	
	return view;	
}

module.exports = Create;


function CreateForm(){
	var view = Ti.UI.createView({
		backgroundImage: "/images/buttonBg.png",
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 250,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var emailRow = CreateRow("E-mail", false);
	
	view.add(emailRow);
	
	view.emailRow = emailRow;
	
	return view;
}


function CreateRow(hintText, hasBottomLine){
	var view = Ti.UI.createView({
		width: 250,
		height: 50
	});
	
	var textField = Ti.UI.createTextField({
		autocapitalization: false,
		autocorrect: false,
	
		hintText: hintText,
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: view.width-20,
		height: view.height
	});
	
	function TurnRed(){
		textField.color = globals.colors.red;
	}
	
	if(hasBottomLine == true) view.add(Ti.UI.createView({width: view.width-6, height: 1, bottom: 0, backgroundColor: globals.colors.black}));
	view.add(textField);
	
	view.TurnRed = TurnRed;
	view.textField = textField;
	
	return view;
};


function CreateButton(text){
	var view = Ti.UI.createView({
		backgroundImage: "/images/buttonBgRed.png",
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 250,
		height: 50,
		layout: "vertical",
		buttonID: "forgotPasswordView"+text,
		touchEnabled: true
	});
	
	Common.MakeButton(view);
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold25,
		minimumFontSize: 20,
		color: globals.colors.white,
		width: view.width - 40,
		height: view.height,
		textAlign: "center",
	});
	
	view.add(label);
	
	
	return view;
}


function validateEmail(email){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

    if (reg.test(email) == false) 
    {
        return false;
    }

    return true;
}