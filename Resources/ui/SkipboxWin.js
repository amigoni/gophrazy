exports.loadWin = function(win){
	var GroupsController = require("controller/GroupsController");
	var groupData;
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//"-"+globals.platform.actualHeight,
	  	zIndex: 1
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: view.width,
		height: view.height-44,
		top: 44,
		separatorColor: "transparent",
		separatorStyle: false
	});
	
	var tableData = [];
	
	view.add(navBar);
	view.add(tableView);
	
	
	function UpdateTableView (){
		groupData =  GroupsController.GetskipboxGroup();
		tableData = null;
		tableData = [];
		tableData.push(CreateTableHeader(groupData));
	
		for (var i = 0; i < groupData.phrases.length; i++){
			var row = CreateRow(groupData.phrases[i]);
			tableData.push(row);
		}
		
		tableData.push(CreateTableFooter());
		tableView.data = tableData;
	}
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
		Ti.App.removeEventListener("closeskipboxWin", Close);
		Ti.App.removeEventListener("updateskipbox", UpdateTableView);
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	Ti.App.addEventListener("updateskipbox", UpdateTableView);
	Ti.App.addEventListener("closeskipboxWin", Close);
	
	view.Open = Open;
	view.Close = Close;
	
	UpdateTableView();
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.red,
		width: globals.platform.width,
		height: "44dp",
		top: "0dp"
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: "44dp",
		height: "44dp",
		right: "0dp",
		buttonID: "skipboxBackButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: "12dp",
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: "35dp",
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/skipBoxIconLarge.png",
		width: 27,
		height: 24
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/skipBoxSign.png",
		width: 123,
		height: 35,
		left: 10
	}));
	
	view.add(title);
	view.add(closeButton);
	
	
	return view;
};


function CreateRow (rowData){
	var SoundController = require("/controller/SoundController");
	var RandomPlus = require("lib/RandomPlus");
	var GroupsController = require("controller/GroupsController");
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 45,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null,
		
		touchEnabled: true
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: view.height,
		buttonID: "skipboxRow"
	});
	
	if (globals.tutorialOn == true) Common.MakeButton(container);
	
	container.addEventListener("touchend", function(e){
		if (view.touchEnabled == true){
			SoundController.playSound("select");
			globals.rootWin.OpenWindow("gameWin", GroupsController.CreateGroup("skipbox", rowData.phraseID));
		} 
	});
	
	var label1 = Ti.UI.createLabel({
		text: CreatePuzzleLabel(rowData.text),
		font: globals.fonts.bold15,
		//minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: globals.platform.width-65,
		height: 20,
		left: 10,
		touchEnabled: true
	});
	
	var then = new moment(rowData.skipTime);
	var now = clock.getNow();
	var difference = now.diff(then,"days");
	var text = "";
	if (difference > 1 || difference == 0) text = difference+"\ndays";
	else text = difference+"\nday";
	
	var label2 = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold13,
		color: globals.colors.darkBrown,
		textAlign: "center",
		width: 40,
		right: 5,
		touchEnabled: true
	});
	
	//view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, top:0}));
	container.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, bottom:0, touchEnabled: false}));
	container.add(label1);
	container.add(label2);
	
	view.add(container);
	
	function CreatePuzzleLabel(startingPhrase){
		var startingPhrase = crypt.decrypt(startingPhrase)+" ";
		var correctWordsTextArray = startingPhrase.match(/\S+\s/g);	
		var shuffledWords =  RandomPlus.ShuffleArray(correctWordsTextArray);
		
		var puzzlePhrase = "";
		for (var i = 0; i < shuffledWords.length; i++){
			puzzlePhrase += shuffledWords[i]+"/";
		}
		
		return puzzlePhrase;
	}
	
	
	return view;
};


function CreateTableHeader(groupData){
	var GroupsController = require("controller/GroupsController");
	var Bubble = require("ui/Common/Bubble");
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 45,
		selectionStyle: false
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal"
	});
	
	var spots = groupData.phrasesLimit - groupData.phrasesIDs.length;
	var text = "";
	if (spots > 1) text = spots+" spots left";
	else text = spots+" spot left";
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold15,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown
	});
	
	/*
	var bubble = Bubble.createView(groupData.phrasesIDs.length+"/"+groupData.phrasesLimit);
	bubble.left = 5;
	bubble.top = 0;
	*/
	container.add(label);
	//container.add(bubble);
	
	view.add(container);
	
	return view;
}


function CreateTableFooter (){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		selectionStyle: false
	});
	
	var label = Ti.UI.createLabel({
		text:"Clear out by solving skipped puzzles if you have reached the limit.",
		font: globals.fonts.bold13,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		textAlign: "center",
		width: view.width-20,
		top: 15
	});
	
	view.add(label);
	view.add(Ti.UI.createView({height:5}));
	
	
	return view;
}
