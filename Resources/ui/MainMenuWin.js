exports.loadWin = function(){
	var LocalData = require("model/LocalData");
	var gameData = globals.gameData;
	var RankingView = require("ui/MainMenuWinElements/RankingView");
	var WhatsNewController = require("/controller/WhatsNewController");
	
	var whatsNewController = new WhatsNewController();
	var currentTheme = GetCurrentTheme();
	
	var topDifference = ((globals.platform.height-480)/2).toString()+"dp";
	
	var win = Titanium.UI.createView({ 
	    //backgroundColor: "red",
	    backgroundImage: currentTheme.menuBg,
	   // orientationModes: [ Titanium.UI.PORTRAIT ],
	    width: globals.platform.width,	
	    height: globals.platform.actualHeight,   
	    top: "0dp",
	    opacity: 0
	});
	
	var currentThemePosition = "0dp";
	/*
	win.addEventListener("singletap", function(){
		if(currentTheme.name == "dawn") currentTheme = globals.themes.day;
		else if(currentTheme.name == "day") currentTheme = globals.themes.dusk;
		else if(currentTheme.name == "dusk") currentTheme = globals.themes.night;
		else if(currentTheme.name == "night") currentTheme = globals.themes.snowyDay;
		else if(currentTheme.name == "snowyDay") currentTheme = globals.themes.dawn;
		UpdateTheme();
	});
	*/
	
	var world = Ti.UI.createView({
		backgroundImage: currentTheme.worldImage,
		width: 624,
		height: 645,
		//top: (((globals.platform.actualHeight)-(480-globals.adsBannerSpace))/2+480-globals.adsBannerSpace)-250,
		bottom: -410,
		left: globals.platform.width/2-624/2-20,
		anchorPoint: {x:0.5, y: 0.5},
	});
	
	
	function RotateWorld(worldUnlocked){
		var duration = 12000;//for 180
		/*
		if (worldUnlocked == 7) worldUnlocked = 8;
		var angle = 51.25*(worldUnlocked-1);
		setTimeout(function(){
			Animator.animate(world, {
				duration: duration,
				rotate: angle
			});
			
			
			setInterval(function(){
				Animator.animate(world, {
					duration: duration,
					rotate: angle
				});
			},duration);
		},2000);
		*/
		function Rotate(){
			world.animate(Ti.UI.createAnimation({
				duration: duration,
				//delay: 0,
				transform:Ti.UI.create2DMatrix().rotate(180),
				repeat: 999999,
				curve:  Titanium.UI.ANIMATION_CURVE_LINEAR
			}));
			
			setTimeout(function(){world.transform = Ti.UI.create2DMatrix().rotate(180);},duration);
			
			world.animate(Ti.UI.createAnimation({
				duration: duration,
				delay: duration+10,
				transform:Ti.UI.create2DMatrix().rotate(180),
				repeat: 999999,
				curve:  Titanium.UI.ANIMATION_CURVE_LINEAR
			}));	
			
			setTimeout(function(){world.transform = Ti.UI.create2DMatrix().rotate(0);},duration*2);
		}
		
		setInterval(Rotate, duration*2);
		Rotate();
	}
	
	//RotateWorld(7);
	
	
	var topArea = Ti.UI.createView({
		//backgroundColor: "red",
		width: win.width,
		height: globals.platform.actualHeight-220,//240 is half on iphone 4
		top: "0dp",
		left: "0dp"
	});
	
	topArea.MoveLeft = function(){
		/*
		Animator.animate(topArea, {
			duration : 250,
			left : "0dp",
			easing : Animator['EXP_IN']
		});
		*/
		topArea.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: "0dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	};

	topArea.MoveRight = function(){
		/*
		Animator.animate(topArea, {
			duration : 150,
			left : globals.platform.width,
			easing : Animator['EXP_OUT']
		});
		*/
		topArea.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	};
	
	var settingsButton = CreateSettingsButton();
	var feedbackButton = CreateFeedbackButton(win);
	var achievementsButton = CreateAchievementsButton();
	var gameCenterButton = CreateGameCenterButton();
	
	var logoContainer = CreateLogoContainer();
	
	logoContainer.addEventListener("singletap", function(){
		if(globals.gameData.userFacebookID != ""){
			topArea.MoveRight();
			rankingView.Open();
		}
	});
	
	topArea.add(logoContainer);
	topArea.add(settingsButton);
	topArea.add(feedbackButton);
	topArea.add(achievementsButton);
	if(Ti.Platform.name == "iPhone OS") topArea.add(gameCenterButton);
	
	
	var rankingView = RankingView.createView();
	rankingView.addEventListener("closed", function(){
		topArea.MoveLeft();
	});
	
	
	var bottomArea = CreateBottomArea();	
	
	
	
	win.add(topArea);
	win.add(rankingView);
	win.add(world);	
	win.add(bottomArea);
	
		
	
	
	function Open(){
		setTimeout(function(){
			win.animate(Ti.UI.createAnimation({
				duration: 1000,
				opacity: 1
			}));
		},200);
	};
	
	function OpenStorePopUp(kind){
		var LargeStoreItemPopUp = require("ui/PopUps/LargeStoreItemPopUp");
		var storePopUp = LargeStoreItemPopUp.CreatePopUp(kind);
		storePopUp.addEventListener("closed",function(e){
			win.remove(storePopUp);
			storePopUp = null;
		});
		win.add(storePopUp);
	}
	
	function UpdateViews(){
		bottomArea.Close();
		win.remove(bottomArea);
		bottomArea = null;
		bottomArea = CreateBottomArea();
		win.add(bottomArea);
	}
	
	function OpenTutorial(e){
		globals.rootWin.OpenWindow("tutorialPopUp", e.name);
	}
	
	function GetCurrentTheme(){
		var returnObject;
		var now = clock.getNow();
		
		var hour = now.hour();
		if(hour > 20 || hour <= 5) returnObject = globals.themes.night;
		else if(hour > 5 && hour <= 9) returnObject = globals.themes.dawn;
		else if(hour > 9 && hour <= 17) returnObject = globals.themes.day;
		else if(hour > 17 && hour <= 20) returnObject = globals.themes.dusk;	
		
		return returnObject;
	}
	
	function UpdateTheme(){
		win.backgroundImage = currentTheme.menuBg;
		world.tint = currentTheme.worldColor;
	}
	
	win.Open = Open;
	
	Ti.App.addEventListener("levelUp", UpdateViews);
	Ti.App.addEventListener("multiplayerOn", UpdateViews);
	Ti.App.addEventListener("openTutorial", OpenTutorial);
	
	//Show only if Multiplayer and if scores have changed
	if(globals.gameData.userFacebookID != ""){
		setTimeout(function(){
			topArea.MoveRight();
			rankingView.Open();
		},4000);
	}
	
	if(whatsNewController.openPopUp == true){
		setTimeout(function(){
				globals.rootWin.OpenWindow("whatsNewPopUp", whatsNewController.data);
		},1000);
	}
	
	//setTimeout(function(){globals.rootWin.OpenWindow("vsWin");},2000);
	//var VsMatchController = require("/controller/VsMatchController");
	//var vsMatchController = new VsMatchController();
	
	return win;
};	


function CreateLogoContainer(){
	var TipLabel = require("ui/MainMenuWinElements/TipLabel");
	
	var scaleMultiplier = 1;
	if(globals.platform.scale == 1) scaleMultiplier = globals.platform.width/320;
	
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: -globals.platform.height,
		transform: Ti.UI.create2DMatrix().scale(scaleMultiplier, scaleMultiplier)
	});
	
	var logo = Ti.UI.createImageView({
		image:"/images/wordHopLogo.png",
		width: 299,
		height: 92
	});
	
	var tipLabel = TipLabel.createView();
	
	view.add(logo);
	view.add(tipLabel);
	if(globals.gameData.ratedComplete == false && globals.gameData.phrasesCompleteCount >= 5){
		var rateUsView = CreateRateUsView();
		view.add(rateUsView);
	}
	
	setTimeout(function(){
		view.animate(Ti.UI.createAnimation({
			duration: 1500,
			top: globals.platform.actualHeight/4-view.size.height/2+20,
			//top: 40,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	},1000);
	
	
	return view;
};


function CreateBottomArea(){
	var view = Ti.UI.createView({
		//backgroundColor: "white",
		width: globals.platform.width,
		height: 210,
		bottom: -200
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		//bottom: 3
	});
	
	var firstRow = CreateFirstRow();
	var secondRow = CreateSecondRow();
	var thirdRow = CreateThirdRow();
	
	container.add(firstRow);
	container.add(secondRow);
	container.add(thirdRow);
	
	view.add(container);
	
	function Close(){
		firstRow.Close();
		secondRow.Close();
		thirdRow.Close();
		
		firstRow = null;
		secondRow = null;
		thirdRow = null;
	}
	
	view.Close = Close;
	
	setTimeout(function(){
		/*
		if(globals.os == "ios"){
			Animator.animate(view,{
				duration: 2000,
				bottom: "0dp",
				easing: Animator["EXP_OUT"]
			});
		}
		else view.bottom = "0dp";
		*/
		view.animate(Ti.UI.createAnimation({
			duration: 1000,
			bottom: 0,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));	
	}, 1000);
	
	
	return view;
}


function CreateFirstRow(){
	var PlayButton = require("ui/MainMenuWinElements/PlayButton");
	var FriendsButton = require("ui/MainMenuWinElements/FriendsButton");
	
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: globals.platform.width,
		height: Ti.UI.SIZE
	});
	
	var playButton = PlayButton.createView();
	
	var friendsButton = FriendsButton.createView();
	friendsButton.left = "20dp";
	
	friendsButton.addEventListener("pressed", function(){
		
	});
	
	
	view.add(playButton);
	if(globals.gameData.userFacebookID == "") view.add(friendsButton);
	
	function Close(){
		friendsButton.Close();
	}
	
	view.Close = Close;
	
	
	return view;
}


function CreateSecondRow(){
	var FortuneCookieButton = require("ui/MainMenuWinElements/FortuneCookieButton");
	var PuzzlePostButton = require("ui/MainMenuWinElements/PuzzlePostButton");
	//var EventsButton = require("ui/MainMenuWinElements/EventsButton");
	
	var topDifference = ((globals.platform.height-480)/2).toString()+"dp";
	var width = "70dp";
	var view =  Ti.UI.createView({
		//backgroundColor: "green",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		top: "-10dp",
		layout:"horizontal"
	});
	
	var fortuneCookieButton = FortuneCookieButton.createView();
	var vsButton;
	var puzzlePostButton = PuzzlePostButton.createView();
	
	
	/*
	var eventsButton = EventsButton.createView();
	eventsButton.addEventListener("pressed", function(){
		Common.CreateButtonBlocker(false);
		globals.rootWin.OpenWindow("eventsWin");
	});
	*/	
	
	//view.add(Ti.UI.createView({width:10}));
	if (globals.gameData.abilities.fortuneCookies.enabled == true){
		view.add(fortuneCookieButton);
		puzzlePostButton.left = "30dp";
	} 
	
	//if (globals.gameData.abilities.pvp.enabled == true) view.add(vsButton);
	if (globals.gameData.abilities.puzzlePost.enabled == true) {
		view.add(puzzlePostButton);
	}	
	//if (globals.gameData.abilities.events.enabled) view.add(eventsButton);
	//view.add(Ti.UI.createView({width:10}));
	
	function Close(){
		//Ti.App.removeEventListener("levelUp", Update);
		fortuneCookieButton.Close();
		puzzlePostButton.Close();
		//eventsButton.Close();
	};
	
	view.Close = Close;
	
	
	return view;
}


function CreateThirdRow(){
	var MailButton = require("ui/MainMenuWinElements/MailButton");
	var MonstersButton = require("ui/MainMenuWinElements/MonstersButton");
	var skipboxButton = require("ui/MainMenuWinElements/SkipboxButton");
	var PuzzleBookButton = require("ui/MainMenuWinElements/PuzzleBookButton");
	
	var view =  Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		top: "0dp",
		layout: "horizontal"
	});
		
	var mailButton = MailButton.createView();
	var skipboxButton = skipboxButton.createView();	
	var puzzleBookButton = PuzzleBookButton.createView();	
	
	if (globals.gameData.userFacebookID != "")
	{
		puzzleBookButton.left = "30dp";
		skipboxButton.left = "30dp";
		view.add(mailButton);	
	} 
	
	if (globals.gameData.abilities.skipbox.enabled == true)
	{
		view.add(skipboxButton);
		puzzleBookButton.left = "30dp";
	} 
	
	view.add(puzzleBookButton);
	
	
	function Close(){
		
	}
	
	view.Close = Close;
	
	
	return view;
}


function CreateFeedbackButton(){
	var view = Ti.UI.createView({
		width: "40dp",
		height: "50dp",
		top: "-200dp",
		right: 50,
		buttonID: "mainMenufeedbackButton",
		touchEnabled: true
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap",function(e){
		//globals.storeController.BuyStoreKitItem("hint_1", 1); //HERE FOR TESTING PURCHASES
		if(view.touchEnabled == true) globals.rootWin.OpenWindow("feedbackPopUp");
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/feedbackIconSmall.png",
		width: 38,
		height: 25,
		top: "5dp"
	});
	
	view.add(icon);
	
	function FeedbackButtonNetworkBarAction(e){
		if(e.isDown == true){
			view.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 0
			}));
		}
		else{	
			view.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 1
			}));
		}
	}
	
	Ti.App.addEventListener("networkBarAction", FeedbackButtonNetworkBarAction);
	
	setTimeout(function(){
		/*
		Animator.animate(view,{
			duration: 1000,
			top: "0dp",
			easing: Animator["EXP_OUT"]
		});
		*/
		view.animate(Ti.UI.createAnimation({
			duration: 1000,
			top: "0dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
	}, 2000);
	
	
	return view;
};


function CreateSettingsButton(){
	var view = Ti.UI.createView({
		width: 40,
		height: 50,
		top: -200,
		right: 5,
		buttonID: "mainMenuSettingsButton",
		touchEnabled: true
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap",function(e){
		if(view.touchEnabled == true) globals.rootWin.OpenWindow("settingsWin");
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/settingsIconSmall.png",
		width: 26,
		height: 26,
		top: 5,
		buttonID: "icon"
	});
	
	view.add(icon);
	
	function SettingsButtonNetworkBarAction(e){
		if(e.isDown == true){
			view.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 0
			}));
		}
		else{	
			view.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 1
			}));
		}
	}
	
	Ti.App.addEventListener("networkBarAction", SettingsButtonNetworkBarAction);
	
	setTimeout(function(){
		/*
		Animator.animate(view,{
			duration: 1000,
			top: "0dp",
			easing: Animator["EXP_OUT"]
		});
		*/
		view.animate(Ti.UI.createAnimation({
			duration: 1000,
			top: "0dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}, 2000);
	
	
	
	return view;	
}


function CreateAchievementsButton(){
	var view = Ti.UI.createView({
		width: 40,
		height: 50,
		top: -200,
		right: 100,
		buttonID: "mainMenuAchievementsButton",
		touchEnabled: true
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap",function(e){
		if(view.touchEnabled == true) globals.rootWin.OpenWindow("achievementsWin");
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/checkMarkButton.png",
		width: 29,
		height: 29,
		top: 5
	});
	
	view.add(icon);
	
	function AchievementsButtonNetworkBarAction(e){
		if(e.isDown == true){
			view.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 0
			}));
		}
		else{	
			view.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 1
			}));
		}
	}
	
	Ti.App.addEventListener("networkBarAction", AchievementsButtonNetworkBarAction);
	
	setTimeout(function(){
		/*
		Animator.animate(view,{
			duration: 1000,
			top: "0dp",
			easing: Animator["EXP_OUT"]
		});
		*/
		view.animate(Ti.UI.createAnimation({
			duration: 1000,
			top: "0dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}, 2000);
	
	
	return view;	
}


function CreateGameCenterButton(){
	var view = Ti.UI.createView({
		width: 40,
		height: 50,
		top: -200,
		right: 150,
		buttonID: "mainMenuGameCenterButton",
		touchEnabled: true
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("singletap",function(e){
		if(view.touchEnabled == true){
			if(globals.gameData.gameCenterData.id == ""){
				Ti.App.fireEvent("initGameCenter");
			}
			else{
				if(Ti.Platform.name == "iPhone OS"){
					var gamekit = require('com.obigola.gamekit');
					gamekit.showLeaderboard('com.mozzarello.gophrazy.puzzles_solved');	
				}
			}
		}
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/gameCenterIcon.png",
		width: 32,
		height: 32,
		top: 2
	});
	
	view.add(icon);
	
	function GameCenterButtonNetworkBarAction(e){
		if(e.isDown == true){
			view.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 0
			}));
		}
		else{	
			view.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 1
			}));
		}
	}
	
	Ti.App.addEventListener("networkBarAction", GameCenterButtonNetworkBarAction);
	
	setTimeout(function(){
		/*
		Animator.animate(view,{
			duration: 1000,
			top: "0dp",
			easing: Animator["EXP_OUT"]
		});
		*/
		view.animate(Ti.UI.createAnimation({
			duration: 1000,
			top: "0dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}, 2000);
	
	
	return view;	
}


function CreateRateUsView(){
	var view = Ti.UI.createView({
		//backgroundImage: "/images/tutorialBg.png",
		//backgroundColor: globals.colors.white,
		width: Ti.UI.SIZE,
		height: 44,
		//left: -200,
		//top: "-60dp",
		top: -10,
		opacity: 0,
		buttonID: "mainMenuRateUsButton",
		touchEnabled: true
	});
	
	view.addEventListener("singletap", function(){
		if(view.touchEnabled == true){
			var url;
			if(Ti.Platform.name == "iPhone OS") 
			//url = "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=903559056";
			url = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=903559056&pageNumber=0& sortOrdering=1&type=Purple+Software&mt=8";
			else if (Ti.Platform.name == "android") url = "https://play.google.com/store/apps/details?id=com.mozzarello.gophrazy&reviewId=0";
			Ti.Platform.openURL(url);
			
			globals.gameData.ratedComplete = true;
			lobals.gameData.ratedThisVersion = true;
			globals.col.gameData.commit();
			
			setTimeout(function(){
				view.visible = false;
			}, 2000);
		}
	});
	
	var container = Ti.UI.createView({
		//left: "10dp",
		width: Ti.UI.SIZE,
		height: 30,
		layout:"horizontal",
		buttonID: "rateUsView"
	});
	
	Common.MakeButton(container);
	
	container.add(Ti.UI.createImageView({
		image:"/images/star.png",
		width: 26,
		height: 26
	}));
	
	container.add(Ti.UI.createLabel({
		text: "Tap to Rate Me",
		color: globals.colors.black,
		font: globals.fonts.bold12,
		//width: "260dp",//globals.platform.width - 60,
		//height: "30",
		textAlign: "center",
		verticalAlign: Ti.Platform.name == "iPhone OS" ? "center" : null,
		left: "5dp"
		//zIndex:1
	}));
	
	view.add(container);
	
	
	setTimeout(function(){
		view.animate(Ti.UI.createAnimation({
			duration: 1000,
			opacity: 1,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
		view.animate(Ti.UI.createAnimation({
			duration: 400,
			autoreverse: true,
			repeat: 999999,
			delay: 1000,
			transform: Ti.UI.create2DMatrix().scale(0.93,0.93),
			//anchorPoint: {x: 0, y: 0.5},
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}, 3000);
	
	
	
	return view;
}
