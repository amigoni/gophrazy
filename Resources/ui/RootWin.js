exports.loadWin = function(){
	var win = Titanium.UI.createView({ 
	    backgroundColor: globals.colors.black,
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    top: 0,
	    opacity: 1
	});
	
	var viewStack = {};
	viewStack.stack = [];
	
	viewStack.Add = function(view){
		viewStack.stack.push(view);
		
		//Here to wait till it's fully opened to hide window behind. Would be best to fire an App level event
		setTimeout(function(){
			for(var i = 0; i < viewStack.stack.length-1; i++ ){
				viewStack.stack[i].visible = false;
			}
		}, 3000);
	};
	
	viewStack.Back = function(){
		viewStack.stack.pop();
		if(viewStack.stack[viewStack.stack.length-1]) viewStack.stack[viewStack.stack.length-1].visible = true;
	};
	
	var popUpStack = {};
	popUpStack.stack = [];
	
	popUpStack.Add = function(stepData){
		popUpStack.stack.push(stepData);
	};
	
	popUpStack.Next = function(){
		var step = popUpStack.stack[0];
		if (step != null){
			if (step.action == "openWindow") OpenWindow(step.name, step.data);
			else if (step.action == "tutorial") Ti.App.fireEvent("openTutorial", {name: step.name});
			if(popUpStack.stack[0]) popUpStack.stack.splice(0,1);
		}
	};
	
	function OpenWindow(name, data){
		if (name == "mainMenu"){
			
		}
		else if (name == "firstStart"){
			var TutorialPopUp = require("ui/PopUps/TutorialPopUp");
			var tutorial = TutorialPopUp.createView("firstStart");
			globals.rootWin.add(tutorial);
			
			//Must use this otherwise sometimes the tutorial above doesn't show
			setTimeout(function(){
				var GroupsController = require("controller/GroupsController");
				var groupData = GroupsController.CreateGroup("firstStart");
				OpenWindow("gameWin", groupData);
				setTimeout(tutorial.EnterAnimation,1000);
			},10);
		}
		else if (name == "gameWin"){
			var GameWin = require("ui/GameWin");
			var gameWin = GameWin.createView(data, false);
			gameWin.visible = true;
			gameWin.addEventListener("close", function(){
				viewStack.Back();
			});
			gameWin.addEventListener("closed", function(){
				globals.rootWin.remove(gameWin);
				gameWin = null;
			});
			globals.rootWin.add(gameWin);
			viewStack.Add(gameWin);
			gameWin.Open();
		}
		else if (name == "collectionsBookWin"){
			var CollectionBookWin = require("ui/CollectionBookWin");
			CreateWin(CollectionBookWin);
		}
		else if (name == "puzzleBookWin"){
			var PuzzleBookWin = require("ui/PuzzleBookWin");
			CreateWin(PuzzleBookWin);
		}
		else if (name == "puzzlePostWin"){
			var PuzzlePostWin = require("ui/PuzzlePostWin");
			CreateWin(PuzzlePostWin);
		}
		else if (name == "skipboxWin"){
			var skipboxWin = require("ui/SkipboxWin");
			CreateWin(skipboxWin);
		}
		else if (name == "puzzleFactoryWin"){
			var PuzzleFactoryWin = require("ui/PuzzleFactoryWin");
			CreateWin(PuzzleFactoryWin);
		}
		else if (name == "mailboxWin"){
			var MailboxWin = require("ui/MailboxWin");
			CreateWin(MailboxWin);
		}
		else if (name == "settingsWin"){
			var SettingsWin = require("ui/SettingsWin");
			CreateWin(SettingsWin);
		}
		else if (name == "passWin"){
			var PassWin = require("ui/PassWin");
			CreateWin(PassWin, data);
		}
		else if (name == "rankingWin"){
			var RankingWin = require("ui/RankingWin");
			CreateWin(RankingWin);
		}
		else if (name == "featuresWin"){
			var FeaturesWin = require("ui/PopUps/FeaturesWin");
			CreateWin(FeaturesWin);
		}
		else if (name == "whyConnectWin"){
			var WhyConnectWin = require("ui/PopUps/WhyConnectWin");
			CreateWin(WhyConnectWin);
		}
		else if (name == "achievementsWin"){
			var AchievementsWin = require("ui/AchievementsWin");
			CreateWin(AchievementsWin);
		}
		else if (name == "createPuzzleWin"){
			var CreatePuzzleWin = require("ui/PopUps/NewCreatePuzzleWin");
			CreateWin(CreatePuzzleWin, data);
		}
		else if (name == "vsWin"){
			var VsWin = require("ui/VsWin");
			CreateWin(VsWin);
		}
		else if (name == "discoverWin"){
			var DiscoverWin = require("ui/DiscoverWin");
			CreateWin(DiscoverWin);
		}
		else if (name == "sharePuzzlePopUp"){
			var SharePuzzlePopUp = require("ui/PuzzleFactoryWinElements/SharePuzzlePopUp");
			var sharePuzzlePopUp = new SharePuzzlePopUp(data);
			sharePuzzlePopUp.addEventListener("closed", function() {
				globals.rootWin.remove(sharePuzzlePopUp);
				sharePuzzlePopUp = null;
			});
	
			globals.rootWin.add(sharePuzzlePopUp);
			sharePuzzlePopUp.Open();
		}
		else if (name == "friendPickerMessagesPopUp"){
			var FriendPickerMessagesPopUp = require("ui/PopUps/FriendPickerMessagesPopUp");
			var friendPickerMessagesPopUp = FriendPickerMessagesPopUp.createView(data);
			friendPickerMessagesPopUp.addEventListener("closed", function(){
				globals.rootWin.remove(friendPickerMessagesPopUp);
				friendPickerMessagesPopUp = null;
			});
			globals.rootWin.add(friendPickerMessagesPopUp);
			friendPickerMessagesPopUp.Open();
		}
		else if (name == "inviteWin"){
			var InviteWin = require("ui/FriendsWinElements/InviteWin");
			var inviteWin = InviteWin.createView();
			inviteWin.addEventListener("closed", function() {
				globals.rootWin.remove(inviteWin);
				inviteWin = null;
			});
	
			globals.rootWin.add(inviteWin);
			inviteWin.Open();
		}
		
		else if (name == "whatsNewPopUp"){
			var WhatsNewPopUp = require("/ui/PopUps/WhatsNewPopUp");
			var whatsNewPopUp = new WhatsNewPopUp(data);
			whatsNewPopUp.addEventListener("closed", function(){
				globals.rootWin.remove(whatsNewPopUp);
				whatsNewPopUp = null;
			});
			
			globals.rootWin.add(whatsNewPopUp);
			whatsNewPopUp.Open();
		}
		else if (name == "smallSquarePopUp"){
			var SmallSquarePopUp = require("ui/PopUps/SmallSquarePopUp");
			var smallSquarePopUp = SmallSquarePopUp.CreatePopUp(data);
			smallSquarePopUp.addEventListener("closed", function(){
				globals.rootWin.remove(smallSquarePopUp);
				smallSquarePopUp = null;
			});
			
			globals.rootWin.add(smallSquarePopUp);
		}
		else if (name == "storePopUp"){
			var LargeStoreItemPopUp = require("ui/PopUps/LargeStoreItemPopUp");
			var storePopUp = LargeStoreItemPopUp.CreatePopUp(data, data.callback);//(e.kind);
			storePopUp.addEventListener("closed",function(e){
				globals.rootWin.remove(storePopUp);
				storePopUp = null;
			});
			globals.rootWin.add(storePopUp);
		}
		else if (name == "tallPopUp"){
			var TallPopUp = require("ui/PopUps/TallPopUp");
			var tallPopUp = TallPopUp.CreatePopUp(data);
			
			tallPopUp.addEventListener("closed", function(){
				globals.rootWin.remove(tallPopUp);
				tallPopUp = null;
				popUpStack.Next();
			});
			
			globals.rootWin.add(tallPopUp);
		}
		else if (name == "tutorialPopUp"){
			var TutorialPopUp = require("ui/PopUps/TutorialPopUp");
			var tutorial = TutorialPopUp.createView(data);
			tutorial.addEventListener("closed", function(){
				globals.rootWin.remove(tutorial);
				tutorial = null;
			});
			globals.rootWin.add(tutorial);
			tutorial.EnterAnimation();
		}
		else if (name == "feedbackPopUp"){
			var FeedbackPopUp = require("/ui/PopUps/FeedbackPopUp");
			var feedbackPopUp = FeedbackPopUp.CreatePopUp();
			feedbackPopUp.addEventListener("closed", function(){
				globals.rootWin.remove(feedbackPopUp);
				feedbackPopUp = null;
			});
			globals.rootWin.add(feedbackPopUp);
		}
		else if (name == "fortuneCookieRewardWin"){
			var FortuneCookieRewardWin = require("/ui/PopUps/FortuneCookieRewardWin");
			var fortuneCookieRewardWin = FortuneCookieRewardWin.loadWin(data);
			fortuneCookieRewardWin.addEventListener("closed", function(){
				globals.rootWin.remove(fortuneCookieRewardWin);
				fortuneCookieRewardWin = null;
			});
			globals.rootWin.add(fortuneCookieRewardWin);
			fortuneCookieRewardWin.Open();
		}
		else if (name == "networkBar"){
			var NetworkBar = require("ui/Common/NetworkBar");
			var networkBar = NetworkBar.createView(data.bgColor, data.text);
			networkBar.addEventListener("closed", function(){
				globals.rootWin.remove(networkBar);
				networkBar = null;
			});
			globals.rootWin.add(networkBar);
			networkBar.Open();
		}
		else if (name == "noteDisplayPopUp"){
			var NoteDisplayPopUp = require("ui/PopUps/NoteDisplayPopUp");
			noteDisplayPopUp = NoteDisplayPopUp.createView(data);
			noteDisplayPopUp.addEventListener("closed", function(){
				globals.rootWin.remove(noteDisplayPopUp);
				noteDisplayPopUp = null;
			});
			globals.rootWin.add(noteDisplayPopUp);
			noteDisplayPopUp.Open();
		}
		else if (name == "noteComposeWin"){
			var NoteComposeWin = require("ui/PopUps/NoteComposeWin");
			var noteComposeWin = NoteComposeWin.createView(data);
			noteComposeWin.addEventListener("closed", function(){
				globals.rootWin.remove(noteComposeWin);
				noteComposeWin = null;
			});
			globals.rootWin.add(noteComposeWin);
			noteComposeWin.Open();
		}
		else if (name == "topNotificationsPopUp"){
			var TopNotificationsPopUp = require("ui/PopUps/TopNotificationsPopUp");
			var topNotificationsPopUp = TopNotificationsPopUp.createView(data);
			topNotificationsPopUp.addEventListener("closed", function(){
				if(topNotificationsPopUp) globals.rootWin.remove(topNotificationsPopUp);
				topNotificationsPopUp = null;
			});
			globals.rootWin.add(topNotificationsPopUp);
			topNotificationsPopUp.Open();
		}
	}
	
	function CreateWin(module, data){
		var win1 = module.loadWin(data);
		win1.visible = true;
		win1.addEventListener("close", function(){
			viewStack.Back();
		});
		
		win1.addEventListener("closed", function(){
			globals.rootWin.remove(win1);
			win1 = null;
			
		});
		globals.rootWin.add(win1);
		viewStack.Add(win1);
		win1.Open();
	}
	
	win.OpenWindow = OpenWindow;
	win.popUpStack = popUpStack;
	win.viewStack = viewStack;
	
	
	return win;
};
