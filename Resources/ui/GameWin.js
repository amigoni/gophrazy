var RandomPlus = require("/lib/RandomPlus"); 

exports.createView = function(group, isSolution){
	var ShareController = require("/controller/ShareController");
	var SoundController = require("/controller/SoundController");
	var Common = require('ui/Common');
	var LocalData = require("model/LocalData");
	var GroupsController = require("/controller/GroupsController");
	var PhrasesController = require("/controller/PhrasesController");
	var MessagesController = require("controller/MessagesController");
	var ServerData = require("/model/ServerData");
	var PassController = require("/controller/PassController");
	var RandomWordDisplay = require("/ui/GameWinElements/RandomWordDisplay");
	var NavBar = require("ui/GameWinElements/NavBar");
	
	var GameToolBar = require("/ui/GameWinElements/GameToolBar");
	var WonToolBar = require("/ui/GameWinElements/WonToolBar");
	var StarBar = require("/ui/GameWinElements/StarBar");
	var ResultView = require("/ui/GameWinElements/ResultView");
	var SmallSquarePopUp = require("ui/PopUps/SmallSquarePopUp");
	var TopNotificationsPopUp = require("/ui/PopUps/TopNotificationsPopUp");
	var RankingBottomView = require("ui/GameWinElements/RankingBottomView");
	var TopPopUpController = require("ui/GameWinElements/TopPopUpController");
	
	var openWindowTime = new Date().getTime();
	var gameData = globals.gameData;
	var phraseData = group.currentPhrase;
	var oldPhraseData = group.currentPhrase;
	var shuffledWordsTexts; //Holds the array of words just in case you want to share
	var correctWordsArray; //Holds the array of words just in case you want to share
	var undoCount = 0;
	var bonusTimeSecs = CalculateBonusTimeSecs(phraseData.numberOfWords);
	
	
	var bottomHeight = 63;
	if(globals.platform.height > 480) bottomHeight = 63 +globals.platform.height-480;
	if(bottomHeight > 110) bottomHeight = 110;
	
	var playHeight = 320;
	if(globals.platform.height > 568) playHeight = globals.platform.actualHeight-45-bottomHeight;
	if(playHeight > 400) {
		playHeight = 400;
		bottomHeight = globals.platform.actualHeight-45-playHeight;
		if(bottomHeight > 120){
			 bottomHeight = 120;
			if(globals.platform.height > 667) bottomHeight = 140;
		} 
	}
	
	var playContainerWidth = globals.platform.width-20;
	if (playContainerWidth > 360) playContainerWidth = globals.platform.width-60;
	
	var playContainerHeight = globals.platform.actualHeight-45-bottomHeight;
	
	if(playContainerWidth > playContainerHeight) playContainerWidth = playHeight;
	
	Ti.API.info("GAMEWIN:Bottom Height: "+bottomHeight);
	Ti.API.info("GAMEWIN:Play Height: "+playHeight);
	Ti.API.info("GAMEWIN:Play Container Height: "+playContainerHeight);
	Ti.API.info("GAMEWIN:Play Container Width: "+playContainerWidth);

	phraseData.isDailyChallenge = false;
	
	var topPopUpController = TopPopUpController.createController();
	
	var win = Titanium.UI.createView({  
	    backgroundColor:"transparent",
	    height: globals.platform.actualHeight,
	    width: globals.platform.width,
	    top: globals.platform.actualHeight,
	    bubbleParent: false,
	    zIndex: 10,
	    left:"0dp" 
	});
	
	var blackShade = Ti.UI.createView({
		backgroundColor: "#333333",
		width: win.width,
		height: win.height,
		opacity: 1
	});
	
	var container = Titanium.UI.createView({  
	    backgroundColor:"transparent",
	    height: win.height,
	    width: win.width,
	    top: "0dp",//globals.platform.height,
	    bubbleParent: false,
	    layout: "vertical"
	});
	
	var navBar = NavBar.createView(group,container);
	navBar.addEventListener("back", function(){
		win.fireEvent("close");
		CloseWin();
	});
	
	var playContainer = Ti.UI.createView({
		//backgroundColor: "red",
		width: win.width,
		height: playContainerHeight,
	});
	
	var playBackground = Ti.UI.createView({
		borderColor: globals.colors.black,
		backgroundColor: globals.colors.white,
		width: playContainerWidth,
		height: playHeight,
		bottom: 0
	});
	
	
	var resultView = ResultView.CreateResultView(playContainerWidth);
	
	
	var randomWordDisplay = CreateRandomWordDisplay(playContainerWidth, phraseData, group);
		
	var starBar = new StarBar();
	starBar.addEventListener("complete", function (){
		wonToolbar.Appear();
	});
	
	
	var wonToolbar = WonToolBar.createView(playContainerWidth, group.type, phraseData);
	
	
	wonToolbar.addEventListener("share", function(e){
		if(oldPhraseData.puzzleURL) globals.rootWin.OpenWindow("sharePuzzlePopUp",{data: oldPhraseData, shareType: "general"});
		else{
			Common.CreateButtonBlocker(true, true);
			var PuzzleFactoryController = require("controller/PuzzleFactoryController");
			PuzzleFactoryController.SavePuzzleToServer(oldPhraseData.phraseID, function(){
				oldPhraseData = globals.col.phrases.find({phraseID: oldPhraseData.phraseID})[0];
				globals.rootWin.OpenWindow("sharePuzzlePopUp",{data: oldPhraseData, shareType: "general"});
			});
		}
	});
	
	wonToolbar.addEventListener("next", function(e){
		NextPhrase(false);
	});
	
	wonToolbar.addEventListener("retry", function(e){
		Retry();
	});
	
	/*
	wonToolbar.addEventListener("send", function(e){
		if(globals.gameData.userFacebookID != "") globals.rootWin.OpenWindow("friendPickerMessagesPopUp", oldPhraseData);
		else globals.rootWin.OpenWindow("whyConnectWin");
	});
	*/
	
	wonToolbar.addEventListener("favorite", function(e){
		var isNowFavorite = PhrasesController.TogglePhraseFavorite(oldPhraseData.phraseID);
	});
	
	
	playBackground.add(resultView);
	playBackground.add(randomWordDisplay);
	playBackground.add(wonToolbar);
	if(globals.gameData.starsEnabled == true) playBackground.add(starBar);
	
	playContainer.add(playBackground);
	
	var bottomContainer = Ti.UI.createView({
		//backgroundColor: "yellow",
		width: globals.platform.width,
		height: bottomHeight,
	});
	
	var gameToolBar = GameToolBar.CreateGameToolBar(group);
	gameToolBar.SetSkipButton(phraseData.skipped);
	//if(globals.platform.heightNumber == 568) gameToolBar.bottom = "30dp";
	
	var hintEnabled = false;
	gameToolBar.addEventListener("hint", function(e){
		if(hintEnabled == false){
			hintEnabled = true;
			if (LocalData.GetGameData().hints > 0) {
				randomWordDisplay.EnableHint();
			}	
			else{
				if(Titanium.Network.online == true) {
					if(globals.storeController.products.length > 0) globals.rootWin.OpenWindow("smallSquarePopUp", "hints");
					else {
						globals.storeController.Init(function(){
							globals.rootWin.OpenWindow("smallSquarePopUp", "hints");
						});
					}
				}	
				else alert("There is no internet connection.\nYou need a connection to buy hints.");
			}
		}
		else {
			randomWordDisplay.DisableHint();
			hintEnabled = false;
		}	
	});
	
	gameToolBar.addEventListener("askForHelp", function(e){
		if(phraseData.puzzleURL) globals.rootWin.OpenWindow("sharePuzzlePopUp",{data: phraseData, shareType: "help"});
		else{
			Common.CreateButtonBlocker(true, true);
			var PuzzleFactoryController = require("controller/PuzzleFactoryController");
			PuzzleFactoryController.SavePuzzleToServer(phraseData.phraseID, function(){
				phraseData = globals.col.phrases.find({phraseID: phraseData.phraseID})[0];
				globals.rootWin.OpenWindow("sharePuzzlePopUp",{data: phraseData, shareType: "help"});
			});
		}
	});
	
	gameToolBar.addEventListener("undo", function(e){
		randomWordDisplay.Undo(RefreshResultLabel);
		randomWordDisplay.DisableHint();
		hintEnabled = false;
		undoCount++;
	});
	
	gameToolBar.addEventListener("clear", function(e){
		randomWordDisplay.Clear();
		randomWordDisplay.DisableHint();
		hintEnabled = false;
		undoCount++;
	});
	
	gameToolBar.addEventListener("skip", function(e){
		if(phraseData.skipped == false){
			var skipboxGroupCopy = GroupsController.GetskipboxGroup();
			if (skipboxGroupCopy.phrasesIDs.length < globals.gameData.skipboxLimit){
				if(globals.gameData.skips > 0 || globals.tutorialOn == true){
					var skipPhrasePopUp = SmallSquarePopUp.CreatePopUp("skipIt");
				
					skipPhrasePopUp.addEventListener("skip", function(){
						//Weird. I can't seem to get this to work any other way.
						if(group.type == "collection") group = group.FetchGroup();
						phraseData = group.SkipPhrase();
						if(phraseData != null) gameToolBar.SetSkipButton(phraseData.skipped);
									
						randomWordDisplay.Disappear();
						setTimeout(function(){
							playBackground.remove(randomWordDisplay);
							NextPhrase(true);
						},1100);
						
						globals.gameData.skipsUsedCount++;
						globals.col.gameData.commit();
						
						var AnalyticsController = require("/controller/AnalyticsController");
						AnalyticsController.RegisterEvent({category:"design", event_id: "skipUsed", value:globals.gameData.skipsUsedCount});	
					});
					
					skipPhrasePopUp.addEventListener("closed", function(){
						win.remove(skipPhrasePopUp);
						skipPhrasePopUp = null;
					});
					
					win.add(skipPhrasePopUp);
				}
				else{
					if(Titanium.Network.online == true) {
						if(globals.storeController.products.length > 0) {
							globals.rootWin.OpenWindow("smallSquarePopUp", "buySkips");
						}	
						else {
							if(globals.testing == false){
								globals.storeController.Init(function(){
									globals.rootWin.OpenWindow("smallSquarePopUp", "buySkips");
								});
							}
							else globals.rootWin.OpenWindow("smallSquarePopUp", "buySkips");
						}
					}
					else alert("There is no internet connection. You need a connection to buy skips.");
				} 
			}
			else{
				if(globals.storeController.products.length > 0) globals.rootWin.OpenWindow("smallSquarePopUp", "skipboxFull");
				else {
					if(globals.testing == false){
						globals.storeController.Init(function(){
							globals.rootWin.OpenWindow("smallSquarePopUp", "skipboxFull");
						});
					}	
					else globals.rootWin.OpenWindow("smallSquarePopUp", "skipboxFull");
				}
			}
		}
	});
	
	gameToolBar.addEventListener("autoComplete", function(){
		randomWordDisplay.AutoComplete();
	});
	
	
	var rankingBottomView = RankingBottomView.createView();
	rankingBottomView.height = bottomHeight;
	
	//if (isSolution == true) container.add(replayLabel);
	//bottomContainer.add(rankingBottomView);
	if (isSolution == false) bottomContainer.add(gameToolBar);
	
	
	container.add(navBar);
	container.add(playContainer);
	container.add(bottomContainer);
	
	win.add(blackShade);
	win.add(container);

	
	/////Functions
	function CreateRandomWordDisplay(playContainerWidth, phraseData1, group){
		randomWordDisplay = RandomWordDisplay.CreateRandomWordDisplay(
			playContainerWidth,
			phraseData1,
			group,
		 	isSolution
		);
		
		shuffledWordsTexts = randomWordDisplay.shuffledWordsTexts; //Holds the array of words just in case you want to share
		correctWordsArray = randomWordDisplay.correctWordsTextArray; //Holds the array of words just in case you want to share

		
		randomWordDisplay.addEventListener("changed", function(e){
			RefreshResultLabel(e.resultString, e.finishedIncorrect);
		});
		
		randomWordDisplay.addEventListener("complete", function(e){
			if(topPopUpController) topPopUpController.Pause();
			EndTime();
			
			oldPhraseData = phraseData1;
			group.CompletePhrase();
			
			//Stars
			Ti.API.info(phraseData1.timeToSolve);
			Ti.API.info(bonusTimeSecs*1000);
			phraseData1.stars[0].complete = true;
			if(undoCount == 0) phraseData1.stars[1].complete = true;
			if(phraseData1.timeToSolve.bestTime < bonusTimeSecs*1000) phraseData1.stars[2].complete = true;
			globals.col.phrases.commit();
			
			//Weird. I can't seem to get this to work any other way.
			if(group.type == "collection") group = group.FetchGroup();
			
			randomWordDisplay.Disappear();
			if (isSolution == false) {if(globals.gameData.vibrateOn == true)  Ti.Media.vibrate([0, 1000 ]);};
			SoundController.playSound("completePhrase");
			
			//Reset ReandomWordDisplay
			playBackground.remove(randomWordDisplay);
			randomWordDisplay.Close();
			randomWordDisplay = null;
			
			resultView.CompleteAnimation(oldPhraseData);
		
			gameToolBar.disappear();
			rankingBottomView.Appear(); //The App Level event used to make it appear when done loading.
			if(globals.gameData.starsEnabled == true) starBar.Play(oldPhraseData.stars);
			else wonToolbar.Appear();	
			
			undoCount = 0;
			
			//Getting new phrase here otherwise when you start the game again you will have to do the same one as before.
			setTimeout(function(){
				if (group.type == "general" || group.type == "collection"){
					if(globals.tutorialOn == true && globals.currentTutorial != null) phraseData = group.GetNextPhrase(globals.currentTutorial.phraseID);
					else phraseData = group.GetNextPhrase();
					gameToolBar.SetSkipButton(phraseData.skipped);
					navBar.Update(group);
				} 
			},150);
		});
		
		randomWordDisplay.addEventListener("hintSelected", function(e){
			if (e.useHint == true && globals.tutorialOn != true) LocalData.ChangeHints(-1);
			gameToolBar.RefreshHintButton();
			//clearTimeout(helpPopUPTimeout);
		});
		
		randomWordDisplay.addEventListener("startTime", function(e){
			randomWordDisplay.startTime = clock.getNow();
			if(globals.gameData.timeChallengeEnabled == true) navBar.topTimeDisplay.Start(randomWordDisplay.startTime.valueOf(),phraseData1.numberOfWords, bonusTimeSecs);
		});
		
		var EndTime = function(e){
			if(randomWordDisplay != null) {
				PhrasesController.AddTimeToPhrase(randomWordDisplay.startTime, phraseData1.phraseID);
				if(globals.gameData.timeChallengeEnabled == true) navBar.topTimeDisplay.Stop();
			}	
		};
		
		randomWordDisplay.EndTime = EndTime;
		
		
		return randomWordDisplay;
	}
	
	function RefreshResultLabel(resultString, finishedIncorrect){
		resultView.Refresh(resultString.text, resultString.code, finishedIncorrect);
	};

	
	function NextPhrase(isSkipped){
		if(group.type != "skipbox" && globals.tutorialOn != true){
			if(PassController.CheckForAccess() == true){
				if(globals.tutorialOn == true && globals.currentTutorial != null) phraseData = group.GetNextPhrase(globals.currentTutorial.phraseID);
				ResetMe(isSkipped);
			}
			else{
				if(Titanium.Network.online == true) globals.rootWin.OpenWindow("passWin", {callback: NextPhrase});	
				else alert("There is no internet connection.\nYou need a connection to unlock the next set of puzzles.");
			} 
		}
		else{
			if(globals.tutorialOn == true && globals.currentTutorial != null) phraseData = group.GetNextPhrase(globals.currentTutorial.phraseID);
			ResetMe(isSkipped);
		}
	}
	
	function Retry(){
		phraseData = globals.col.phrases.find({phraseID:oldPhraseData.phraseID})[0];
		ResetMe(false);
	}
	
	function ResetMe(isSkipped){
		//Repeating Next Puzzle here for tutorial as tutorial was not on when I completed the phrase.
		bonusTimeSecs = CalculateBonusTimeSecs(phraseData.numberOfWords);
		
		resultView.ClearText();
		
		randomWordDisplay = CreateRandomWordDisplay(playContainerWidth, phraseData, group); 
		playBackground.add(randomWordDisplay);
		
		gameToolBar.appear();
		rankingBottomView.Disappear();
		//if(isSkipped == false) 
		wonToolbar.Disappear();
		if(globals.gameData.timeChallengeEnabled == true) navBar.topTimeDisplay.ResetMe();
		if(topPopUpController) topPopUpController.ResetMe();
		
		setTimeout(function(){wonToolbar.favoriteButton.SetFavoriteIcon(phraseData.favorite);},500);
	}
	
	function OpenFeatureWin(e){
		//FeaturesWin
		if(e.name != "puzzlePost"){
			globals.rootWin.popUpStack.Add({action: "openWindow", name: "featuresWin"});
			
			//Tutorial
			/*
			if(e.name == "puzzlePost"){
				var tutorialName = "puzzlePostFirstPush";
				if(globals.gameData.notifications.pushNotificationsInitialized == true || Ti.Platform.name == "android") tutorialName =  "puzzlePostNotFirstPush";
				globals.rootWin.popUpStack.Add({action: "tutorial", name: tutorialName});
			}
			else if(e.name != "composePuzzle") globals.rootWin.popUpStack.Add({action: "tutorial", name: e.name});
			*/
			if(e.name != "composePuzzle") globals.rootWin.popUpStack.Add({action: "tutorial", name: e.name});	
		}
		
		
		globals.rootWin.popUpStack.Next();
	}
	
	
	function OpenGroupCompletePopUp(e){
		var collectionData = e.data;
		globals.rootWin.OpenWindow("tallPopUp", collectionData);	
		CloseWin();
	}
	
	function CalculateBonusTimeSecs(numberOfWords){
		var timePerWord = .7;
		if(numberOfWords >= 7 && numberOfWords < 11) timePerWord = .9;
		else if(numberOfWords >= 11 && numberOfWords < 14) timePerWord = 1;
		else if(numberOfWords >= 14 && numberOfWords < 17) timePerWord = 1.2;
		else if(numberOfWords >17) timePerWord = 1.3;
		
		var seconds = Math.floor(numberOfWords*timePerWord);
		if(seconds < 5) seconds = 5;
		
		return seconds;
	}
	
	
	function CloseWin(){
		Ti.App.removeEventListener("featureUnlocked", OpenFeatureWin);
		Ti.App.removeEventListener("openGroupCompletePopUp", OpenGroupCompletePopUp);
		Ti.App.removeEventListener("resume", ResumeUpdate);
		Ti.App.removeEventListener("paused", UpdateTime);
		Ti.App.removeEventListener("boughtPass", BoughtPass);
		if (randomWordDisplay) randomWordDisplay.EndTime();
		if (topPopUpController) {
			topPopUpController.Clear();
			topPopUpController = null;
		}	
		//Must wait or there might be some animation left playing before you close;
		setTimeout(function(){
			starBar.Close();
			navBar.Close();
			rankingBottomView.Close();
			gameToolBar.Close();
			wonToolbar.Close();
			resultView.Close();
			if(randomWordDisplay != null) randomWordDisplay.Close();
			starBar = null;
			navBar = null;
			rankingBottomView = null;
			gameToolBar = null;
			wonToolBar = null;
			playBackground = null;
			blackShade = null;
			randomWordDisplay = null;
			resultView = null;
			container = null;
		}, 2000);
		
		Common.WindowCloseAnimation(win,function(){win.fireEvent("closed");});
	};	
	
	
	function Open(){
		if(group.type == "firstStart") {
			win.opacity = 0;
			win.top = 0;
			win.animate(Ti.UI.createAnimation({duration: 250, opacity: 1}));
		}	
		else Common.WindowOpenAnimation(win);	
		
		Ti.App.fireEvent("closeButtonBlocker");
	};
	
	
	function ResumeUpdate(){
		var now = clock.getNow();
		if(randomWordDisplay) randomWordDisplay.startTime = now;
		if(topPopUpController) topPopUpController.Resume();
	}
	
	
	function UpdateTime(){
		if(randomWordDisplay != null) randomWordDisplay.EndTime();
		if(topPopUpController != null) topPopUpController.Pause();
	}
	
	
	function BoughtPass(){
		if(wonToolbar) wonToolbar.Disappear();
	}
	/////end Functions
	
	
	
//Expose
	win.Open = Open;
	
	Ti.App.addEventListener("featureUnlocked", OpenFeatureWin);
	Ti.App.addEventListener("openGroupCompletePopUp", OpenGroupCompletePopUp);
	Ti.App.addEventListener("resume", ResumeUpdate);
	Ti.App.addEventListener("paused", UpdateTime);
	Ti.App.addEventListener("boughtPass", BoughtPass);
	
	
//Execution	
	if (isSolution == true) randomWordDisplay.AutoComplete();
	
	
	return win;
};




function CreateHelpPostImage(viewIn){
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.background,
		width: "320dp",
		height: "480dp"
	});
	
	view.add(Ti.UI.createView({
		backgroundImage:"/images/logo.png",
		width: "174dp",
		height: "110dp",
		top: "10dp",
		transform: Ti.UI.create2DMatrix().scale(0.5,0.5)
	}));
	
	view.add(Ti.UI.createImageView({
		image: viewIn.toImage()
	}));
	
	
	var helpLabel = Ti.UI.createLabel({
		text: "Help me solve this!",
		font: globals.fonts.words3,
		color: globals.colors.warning,
		bottom: "60dp"
		//height: 40,
		//bottom: 50
	});
	
	var instructionsLabel = Ti.UI.createLabel({
		text: "Put the words in the right order",
		font: globals.fonts.regular1,
		color: globals.colors.textMenus,
		bottom: "45dp"
		//height: 40,
		//bottom: 30
	});
	
	
	view.add(helpLabel);
	view.add(instructionsLabel);
	
	
	return view;
}


function CreateReplayLabel(){
	var label = Ti.UI.createLabel({
		text: "Replay",
		font: globals.fonts.boldMediumSmall,
		color: globals.colors.text,
		//width: globals.platform.width-120,
		minimumFontSize: "14dp",
		textAlign: "right",
		top: "0dp",
		right: "5dp",
		opacity:0
	});
	
	label.animate(Ti.UI.createAnimation({
		duration: 500, 
		opacity:1,
		autoreverse: true,
		repeat: 999999
	}));
	
	return label;
}
