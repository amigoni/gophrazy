exports.loadingOverlay = function (text){
	var win = Titanium.UI.createView({ 
	    backgroundColor: globals.colors.background,
	    //orientationModes: [ Titanium.UI.PORTRAIT ],
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	   
	    //fullscreen: true,
	    //navbarHiddend: true,
	    //height:globals.platform.height-50,
	    //top:0,
	    opacity:0,
	    bubbleParent: false
	});
	
	var mainLabel = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.words3,
		color: globals.colors.text,
	});
	
	var i = 0;
	setInterval(function(){
		var extraText = "";
		if (i == 1) extraText = ".";
		else if(i == 2) extraText = "..";
		else if(i == 3) extraText = "...";
		
		i++;
		if (i>3)i= 0;
		
		mainLabel.text = text+extraText;
	},500);
	
	win.add(mainLabel);
	
	function Close(){
		win.animate(Ti.UI.createAnimation({duration: globals.style.animationSpeeds.speed1,opacity: 0}));
		setTimeout(function(){win.close();},255);
	}
	
	win.addEventListener("close", function(){
		Ti.App.removeEventListener("closeLoadingOverlay", Close);
	});
	
	win.addEventListener("open", function(){
		win.animate(Ti.UI.createAnimation({duration: globals.style.animationSpeeds.speed1,opacity: 1}));
	}
	);
	
	Ti.App.addEventListener("closeLoadingOverlay", Close);
	
	
	return win;
};


exports.MakeButton = function(view, useSingleTap){
	var SoundController = require("/controller/SoundController");
	var TutorialController = require("controller/TutorialController");
	view.touchEnabled = true;
	var glowInterval = null;
	var glowDuration = 350;
	
	var touchMethod = "touchstart";
	if(useSingleTap == true) touchMethod = "singletap";
	
	view.addEventListener(touchMethod, function(){
		if (TutorialController.CheckIfButtonEnabled(view.buttonID) == false){
			Ti.API.info("Disabled");
			view.touchEnabled = false;
			setTimeout(function(){view.touchEnabled = true;}, 1000);
		}
		else{
			if(view.buttonID != "gameToolBarClearButton" && view.buttonID != "gameToolBarUndoButton" ) SoundController.playSound("select");
			Ti.API.info("Button touchstart ID: "+view.buttonID);
			if(view.shrinkOnTouch != false) view.transform = Ti.UI.create2DMatrix().scale(.9,.9);
		}
	});
	
	view.addEventListener("touchend", function(){
		view.transform = Ti.UI.create2DMatrix().scale(1,1);
		if(view.touchEnabled == true && globals.tutorialOn == true & globals.currentTutorial != null && view.buttonID != "_next"){
			if (globals.currentTutorial.name == "firstStart" && view.buttonID != "gameWinBackButton" && view.buttonID != "mainMenuFortuneCookieButton"){
				
			}
			else {Ti.App.fireEvent("tutorialStep");}
		}
	});
	
	view.addEventListener("singletap", function(){
		if(view.buttonID != "gameToolBarUndoButton"){
			setTimeout(function(){
				view.touchEnabled = false;
				setTimeout(function(){view.touchEnabled = true;},1000);
				},
			 100);
		}
	});
	
	
	function StartGlow(){
		if(glowInterval == null){
			setTimeout(function(){
				glowInterval = setInterval(function(){
					view.transform = Ti.UI.create2DMatrix().scale(0.8,0.8);
					setTimeout(function(){view.transform = Ti.UI.create2DMatrix().scale(1,1);},glowDuration-10);
				}, glowDuration*2+10);
			},1500);
		}
	}
	
	function StopGlow(){
		clearInterval(glowInterval);
		glowInterval = null;
	}
	
	function Close(){
		if(glowInterval!= null) clearInterval(glowInterval);
		globals.buttonsArray.splice(globals.buttonsArray.indexOf(view),1);
		//Ti.API.info(globals.buttonsArray.length);
	}
	
	view.StartGlow = StartGlow;
	view.StopGlow = StopGlow;
	view.Close = Close;
	
	if (view.buttonID != "collectionsBookRow") globals.buttonsArray.push(view);
	//Ti.API.info(globals.buttonsArray.length);
	//Ti.API.info(view.buttonID);
};


exports.MakeDynamicTableView = function(tableView, data, loadLimit, CreateRow, CreateBottomRow, CreateTopRow){
	var bottomRow;
	var topRow;
	tableView.loadLimit = loadLimit;
	tableView.nextRowToLoad = 0;
	tableView.lastLoadLastRow = 0;
	tableView.numberViewableRows = 0;
	tableView.numberOfTotalDataRows = data.length;
	
	tableView.addEventListener("more", AddMoreRows);
	//tableView.addEventListener("delete", DeleteRow);
	
	var tableData = [];
	
	function UpdateTableView (data){
		tableData = null;
		tableData = [];
		tableView.numberOfTotalDataRows = data.length;
		var upperLimit = tableView.loadLimit;
		if (upperLimit > data.length) upperLimit = data.length;
		tableView.numberViewableRows = upperLimit;
		
		if(CreateTopRow){
			tableData.push(CreateTopRow());
			tableView.nextRowToLoad++;
		}
		
		for (var i = 0; i < upperLimit; i++){
			var row = CreateRow(data[i]);
			tableData.push(row);
			tableView.nextRowToLoad++;
		}
		
		if(CreateBottomRow){
			bottomRow = CreateBottomRow(tableView.numberViewableRows, tableView.numberOfTotalDataRows); 
			tableData.push(bottomRow);
		}
		
		tableView.lastLoadLastRow = tableView.nextRowToLoad;
		tableView.data = tableData;
	}
	
	function AddMoreRows (){
		var upperLimit = tableView.nextRowToLoad + tableView.loadLimit;
		if (upperLimit >= data.length) upperLimit = data.length;
		tableView.numberViewableRows = upperLimit;
		
		if (upperLimit > tableView.nextRowToLoad){
			if(CreateBottomRow){
				tableView.deleteRow(tableView.nextRowToLoad,{});
				tableData.splice(tableView.nextRowToLoad,1);
			}
			
			for (var i = tableView.nextRowToLoad; i < upperLimit; i++){
				var newRow = CreateRow(data[i]);
				tableData.push(newRow);
				tableView.appendRow(newRow);
				tableView.nextRowToLoad++;
			}
			
			if(CreateBottomRow){
				var count = tableView.nextRowToLoad;
				if (CreateTopRow) count--;
				bottomRow = CreateBottomRow(count, data.length);
				tableView.appendRow(bottomRow);
				tableData.push(bottomRow);
			}	
			tableView.scrollToIndex(tableView.lastLoadLastRow);
			tableView.lastLoadLastRow = tableView.nextRowToLoad;
		}
	}
	
	function DeleteRow(index){
		tableData.splice(index,1);
		tableView.nextRowToLoad--;
		tableView.numberViewableRows--;
		tableView.numberOfTotalDataRows--;
		tableView.deleteRow(tableView.data[0].rows[index]);
		if(CreateBottomRow) bottomRow.Update(tableView.numberViewableRows, tableView.numberOfTotalDataRows);
	}
	
	function AddRow(newRowData, position){
		tableView.numberViewableRows++;
		tableView.numberOfTotalDataRows++;
		if(CreateBottomRow) bottomRow.Update(tableView.numberViewableRows, tableView.numberOfTotalDataRows);
		tableView.nextRowToLoad++;
		var newRow = CreateRow(newRowData);
		tableData.splice(position,0, newRow);
		tableView.insertRowAfter(position, newRow ,true);
	}
	
	function Close(){
		
	}
	
	tableView.UpdateTableView = UpdateTableView;
	tableView.DeleteRow = DeleteRow;
	tableView.AddRow = AddRow;
	tableView.bottomRow = bottomRow;
	tableView.Close = Close;
	
	UpdateTableView(data);
};


exports.CreateButtonBlocker = function (shaded, showActivity, delay){
	if(!delay) delay = 0;
	var opacity = 0.3;
	if (showActivity == true) opacity = 0.8;
	if (shaded == false) opacity = 1;
	
	var view = Titanium.UI.createView({ 
	    backgroundColor: "transparent",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    top: 0,
	    zIndex: 10000,
	    bubbleParent: false
	});
	
	var bg = Titanium.UI.createView({ 
	    backgroundColor: shaded == true? "black" : "transparent",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    top: 0,
	    opacity: 0
	});
	
	var activityIndicator = Ti.UI.createActivityIndicator({
	  color: '#333333',
	  //font: {fontFamily:'Helvetica Neue', fontSize:26, fontWeight:'bold'},
	  //message: 'Loading...',
	  style: globals.os == "ios" ? Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN : Ti.UI.ActivityIndicatorStyle.PLAIN,
	  bottom: "100dp",
	  height: Ti.UI.SIZE,
	  width: Ti.UI.SIZE
	});
	
	var activityLabel = Ti.UI.createLabel({
		text: "",
		font: globals.fonts.bold15,
		color: globals.colors.white,
		textAlign: "center",
		bottom: "50dp"
	});
	
	
	view.add(bg);
	if(showActivity == true){
		view.add(activityIndicator);
		view.add(activityLabel);
		activityIndicator.show();
	}
	
	
	function Close(){
		Ti.App.removeEventListener("closeButtonBlocker", Close);
		Ti.App.removeEventListener("changeButtonBlocker", ChangeButtonBlocker);
		
		bg.animate(
			Ti.UI.createAnimation({
				opacity: 0,
				delay: 250,
				duration: 100
			}),
			function(){
				if (view) globals.rootWin.remove(view);
				view = null;
			}
		);
	}
	
	function ChangeButtonBlocker(e){
		activityLabel.text = e.text;
	}
	
	Ti.App.addEventListener("closeButtonBlocker", Close);
	Ti.App.addEventListener("changeButtonBlocker", ChangeButtonBlocker);
	
	bg.animate(Ti.UI.createAnimation({
		opacity: opacity,
		delay: delay,
		duration: 150
	}));
	
	globals.rootWin.add(view);
};





exports.CreateBackButton = function(){
	var view = Ti.UI.createView({
		width: 60,
		height: 44,
		left: 0,
		top:0
	});
	
	view.add(Ti.UI.createView({
		backgroundImage: globals.colors.theme == "dark" ? "/images/backButtonDark.png" : "/images/backButton.png",
		width: 17,
		height: 26,
		left:10,
		touchEnabled:false
	}));
	
	return view;
};


exports.CreateCloseButton = function(){
	var view = Ti.UI.createView({
		width: 60,
		height: 44,
		right: 0,
		//top:0
	});
	
	view.add(Ti.UI.createView({
		backgroundImage: globals.colors.theme == "dark" ? "/images/closeButtonDark.png" : "/images/closeButton.png",
		width: 16,
		height: 16,
		right:12,
		touchEnabled:false
	}));
	
	return view;
};


exports.WindowOpenAnimation = function(view, callback){
	if (!callback) callback = function(){};
	
	if(Ti.Platform.name == "iPhone OS"){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top:"0dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}), callback);
	}
	else{
		Animator.animate(view,{
			duration: globals.style.animationSpeeds.speed1,
			top: "0dp",
			easing: Animator["EXP_OUT"]
		}, callback);
	}
};



exports.WindowCloseAnimation = function(view, callback){
	if (!callback) callback = function(){};
	
	if(Ti.Platform.name == "iPhone OS"){
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top:globals.platform.actualHeight,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}), callback);
	}
	else{
		Animator.animate(view,{
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.height,
			easing: Animator["EXP_IN"]
		}, callback);
	}
};


exports.CreateLogo = function (theme){
	var view = Ti.UI.createView({
		//backgroundColor: globals.colors.theme == "dark" ? "white" : "transparent",
		//borderColor: globals.colors.theme == "dark" ? globals.colors.borderLine : "transparent",
		//borderColor: "white",
		//borderWidth: 2,
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE
	});
	
	var wordLabel = Ti.UI.createImageView({
		//image: globals.colors.theme == "dark" ? "/images/wordLogoDark.png" : "/images/wordLogo.png",
		image:"/images/wordLogoCartoon.png",
		//width: 203,
		//height: 99,
		top: 0,
	});
	
	var hopLabel = Ti.UI.createLabel({
		text: "hop",
		font: globals.fonts.boldMedium31,
		color: globals.colors.wordBoxesColors[3],
		textAlign: "center",
		top: 0,
		right: 37
	});
	
	var rectanglesContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 32,
		layout: "horizontal",
		top: 90,
		left: 30
	});
	
	var rect0 = Ti.UI.createView({
		backgroundColor: globals.colors.wordBoxesColors[0],
		//borderColor: globals.colors.theme == "dark" ? globals.colors.borderLine : "transparent",
		borderColor: globals.colors.borderLine2,
		borderWidth: 2,
		borderRadius: 6,
		width: 42,
		height: 32,
	});
	
	var rect1 = Ti.UI.createView({
		backgroundColor: globals.colors.wordBoxesColors[1],
		//borderColor: globals.colors.theme == "dark" ? globals.colors.borderLine : "transparent",
		borderColor: globals.colors.borderLine2,
		borderWidth: 2,
		borderRadius: 6,
		width: 46,
		height: 32,
		left: 2,
		top: 0
	});
	
	var rect2 = Ti.UI.createView({
		backgroundColor: globals.colors.wordBoxesColors[2],
		//borderColor: globals.colors.theme == "dark" ? globals.colors.borderLine : "transparent",
		borderColor: globals.colors.borderLine2,
		borderWidth: 2,
		borderRadius: 6,
		width: 38,
		height: 32,
		left: 2,
		top: 0
	});
	
	var rect3 = Ti.UI.createView({
		backgroundColor: globals.colors.wordBoxesColors[3],
		//borderColor: globals.colors.theme == "dark" ? globals.colors.borderLine : "transparent",
		borderColor: globals.colors.borderLine2,
		borderWidth: 2,
		borderRadius: 6,
		width: 34,
		height: 32,
		left: 2,
		top: 0
	});
	
	function AddDropShadow(item){
		var shadow = Ti.UI.createView({
			backgroundColor: "black",
			borderColor: "black",
			borderWidth: 2,
			borderRadius: 6,
			width: item.width,
			height: item.height,
			left: item.left,
			top: item.top,
			opacity: .3
		});
		
		return shadow;
	}
	
	rectanglesContainer.add(rect0);
	rectanglesContainer.add(rect1);
	rectanglesContainer.add(rect2);
	rectanglesContainer.add(rect3);
	
	var dropShadowContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 32,
		layout: "horizontal",
		left: rectanglesContainer.left+1,
		top: rectanglesContainer.top+1,
	});
	
	dropShadowContainer.add(AddDropShadow(rect0));
	dropShadowContainer.add(AddDropShadow(rect1));
	dropShadowContainer.add(AddDropShadow(rect2));
	dropShadowContainer.add(AddDropShadow(rect3));
	
	view.add(wordLabel);
	//view.add(hopLabel);
	view.add(dropShadowContainer);
	view.add(rectanglesContainer);
	
	
	return view;
};








exports.GetJsonText = function(fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
 
	if (readFile.exists()) {
		var readContents = readFile.read();
	   	return readContents.text;
	}
};


exports.ConnectionLabel = function(){
	var visible = !Titanium.Network.online;
	var connectionLabel = Ti.UI.createLabel({
		text: "No connection. Ranks will update once you connect.",
		font: globals.fonts.bold11,
		color: globals.colors.red,
		width: Ti.UI.SIZE,
		textAlign: "center",
		height:  visible == true ? "12dp" : "0dp",
		visible: visible
	});
	
	connectionLabel.Display = function(show){
		if (show == true) {
			connectionLabel.height = "12dp";
			connectionLabel.visible = true;
		}
		else {
			connectionLabel.height = "0dp";
			connectionLabel.visible = false;
		}
	};
	
	
	function Update(e){
		connectionLabel.Display(e.online);
	}
	
	function Close(){
		Ti.Network.removeEventListener("change", Update);
	}
	
	connectionLabel.Close = Close;
	
	
	Ti.Network.addEventListener("change", Update);
	
	
	return connectionLabel;
};


exports.CreateOutlinedLabel = function(text, font, fontColor, borderWidth, borderColor){
	
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE
	});
	
	var bottomRight = Ti.UI.createLabel({
		color: fontColor,
		font: font,
		text: text,
		shadowOffset: {x: borderWidth, y: borderWidth},
		shadowColor: borderColor,
		textAlign: "center"
	});
	
	var topLeft = Ti.UI.createLabel({
		color: fontColor,
		font: font,
		text: text,
		shadowOffset: {x: -borderWidth, y: -borderWidth},
		shadowColor: borderColor,
		textAlign: "center"
	});
	
	var topRight = Ti.UI.createLabel({
		color: fontColor,
		font: font,
		text: text,
		shadowOffset: {x: borderWidth, y: -borderWidth},
		shadowColor: borderColor,
		textAlign: "center"
	});
	
	var bottomLeft = Ti.UI.createLabel({
		color: fontColor,
		font: font,
		text: text,
		shadowOffset: {x: -borderWidth, y: borderWidth},
		shadowColor: borderColor,
		textAlign: "center"
	});
	
	var top = Ti.UI.createLabel({
		color: fontColor,
		font: font,
		text: text,
		shadowOffset: {x: 0, y: -borderWidth},
		shadowColor: borderColor,
		textAlign: "center"
	});
	
	var bottom = Ti.UI.createLabel({
		color: fontColor,
		font: font,
		text: text,
		shadowOffset: {x: 0, y: borderWidth},
		shadowColor: borderColor,
		textAlign: "center"
	});
	
	view.add(topLeft);
	view.add(topRight);
	view.add(bottomLeft);
	view.add(bottomRight);
	view.add(top);
	view.add(bottom);
	
	
	return view;
};
