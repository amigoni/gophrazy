exports.loadWin = function(){
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	
	var scrollView = Ti.UI.createScrollView({
		//backgroundColor: "red",
		width: view.width,
		height: view.height-44,
		contentWidth: view.width,
		contentHeight: "auto",
		top: 44,
		layout: "vertical"
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "We are thrilled to present puzzles made by people like you",
		font: globals.fonts.bold15,
		color: globals.colors.black,
		width: scrollView.width -60,
		height: Ti.UI.SIZE,
		textAlign: "center",
	});
	
	var container = Ti.UI.createView({
		//backgroundColor: "yellow",
		width: scrollView.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	var rowData = [
		{buttonID: "twitter", icon: "twitterIcon.png", title: "Twitter #GoPhrazy", iconHeight: 30, description: "Find puzzles that people share on Twitter under #GoPhrazy", callback: TwitterCallback},
		{buttonID: "facebook", icon: "facebookSquareIcon.png", title: "Our Facebook Page", iconHeight: 35, description: "Play our favorite puzzles from our Facebook page", callback: FacebookCallback},
		{buttonID: "comingSoon", icon: "workInProgressIcon.png", title: "Coming Soon", iconHeight: 35, description: "We are working to bring you a curated list of the best puzzles the community has to offer."},
		{buttonID: "puzzleFactory", icon: "PuzzleFactoryIcon.png", title: "Write More", iconHeight: 36, description: "Don’t forget to contribute yourself through the Puzzle Factory. Write and share a puzzle anywhere you like.", callback: PuzzleFactoryCallback}
	];
	
	for (var i = 0; i < rowData.length; i++){
		var row = CreateRow(rowData[i]);
		
		if(rowData[i].buttonID == "twitter" || rowData[i].buttonID == "facebook"){
			row.add(Ti.UI.createLabel({
				text: "tap to open",
				font: globals.fonts.regular11,
				color: globals.colors.black,
				bottom: 5,
				right: 10,
				opacity: 0.6
			}));	
		}

		container.add(row);
	}
	
	scrollView.add(titleLabel);
	scrollView.add(Ti.UI.createView({backgroundColor: "black", opacity: 0.3, height: 1, width: scrollView.width, top: 10}));
	scrollView.add(container);
	
	view.add(navBar);
	view.add(scrollView);
	
	function TwitterCallback(){
		Ti.Platform.openURL("twitter:///search?q=#GoPhrazy");
	}
	
	function FacebookCallback(){
		Ti.Platform.openURL("fb://profile/132556283503835");
	}
	
	function PuzzleFactoryCallback(){
		setTimeout(function(){
			Common.CreateButtonBlocker(true);
			globals.rootWin.OpenWindow("puzzleFactoryWin");
			/*
			if(globals.gameData.tutorialsData.mailboxOpened == false) {
				//Timeout to avoid conflict with button and tutorial step look at Common
				setTimeout(function(){
					globals.rootWin.OpenWindow("tutorialPopUp","mailboxOpened");
					globals.gameData.tutorialsData.mailboxOpened = true;
					globals.col.gameData.commit();
				},500);
			}
			*/	
			//Close();
		},200);
	}
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
		navBar.Close();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view, function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID: "puzzlePostWinCloseButton",
		touchEnabled: true
	});
	
	closeButton.addEventListener("singletap", function(){
		if(closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.black
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 25,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/discoverPuzzlesIcon.png",
		width: 50,
		height: 20
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/discoverSign.png",
		width: 99,
		height: 23,
		left: 5
	}));
	
	view.add(title);
	view.add(closeButton);
	
	function Close(){
	}
	
	view.Close = Close;
	
	
	return view;
};


function CreateRow(data){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: 65,
		buttonID: data.buttonID,
		touchEnabled: true
	});
	
	Common.MakeButton(view, true);
	
	view.addEventListener("touchend", function(){
		if(view.touchEnabled == true && data.callback) data.callback();
	});
	
	var iconContainer = Ti.UI.createView({
		width: 80,
		height: view.height,
		left: 0
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/"+data.icon,
		height: data.iconHeight
	});
	
	iconContainer.add(icon);
	
	var labelContainer = Ti.UI.createView({
		width: view.width - 90,
		height: Ti.UI.SIZE,
		left: 80,
		layout: "vertical"
	});
	
	var title = Ti.UI.createLabel({
		text: data.title,
		font: globals.fonts.bold15,
		minimumFontSize: 12,
		color: globals.colors.black,
		width: labelContainer.width,
		height: 20,
		textAlign: "left",
	});
	
	var description = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: data.description,
		font: globals.fonts.regular11,
		color: globals.colors.black,
		width: labelContainer.width,
		height: Ti.UI.SIZE,
		textAlign: "left",
		top: -5
	});
	
	labelContainer.add(title);
	labelContainer.add(description);
	
	//view.add(Ti.UI.createView({width: view.width, height: 1, top: 0, backgroundColor: "black", opacity: .2}));
	view.add(Ti.UI.createView({width: view.width, height: 1, bottom: 0, backgroundColor: "black", opacity: .3}));
	view.add(iconContainer);
	view.add(labelContainer);
	
	function Close(){
		
	}
	
	view.icon = icon;
	view.iconContainer = iconContainer;
	view.Close = Close;
	
	return view;
}
