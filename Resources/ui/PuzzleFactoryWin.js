exports.loadWin = function(){
	var GroupsController = require("controller/GroupsController");
	var MessagesController = require("controller/MessagesController");
	var WriteTableView = require("ui/PuzzleFactoryWinElements/WriteTableView");
	var ServerData = require("model/ServerData");
	var PuzzleFactoryController = require("controller/PuzzleFactoryController");
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//-(globals.platform.actualHeight),
	  	zIndex: 1
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var writeTableView;
	
	view.add(navBar);
	
	
	
	function Update(data){
		writeTableView = WriteTableView.createView(data);
		view.add(writeTableView);
	}
	
	function Open(){
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}
	
	function Close(){
		if(writeTableView) writeTableView.Close();
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view, function(){
			view.fireEvent("closed");
		});
	}
	
	function GetData(){
		if(globals.gameData.userName && globals.gameData.userName != ""){
			PuzzleFactoryController.GetMyPuzzles(globals.gameData.userName, 
				function(e){
					Update(globals.col.phrases.find({written: true, authorID: globals.gameData.userName},{$sort:{createdDate:-1}}));
					//Reset old count
					setTimeout(PuzzleFactoryController.UpdatePuzzleSolvedCountOthersOld, 1000);
				},
				false
			);
		}
	}
	
	view.Open = Open;
	view.Close = Close;
	
	var AnalyticsController = require("/controller/AnalyticsController");
	AnalyticsController.RegisterEvent({category:"design", event_id: "openedPuzzleFactoryWin", });	
	
	GetData();
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: "#578392",
		width: globals.platform.width,
		height: "44dp",
		top: "0dp"
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: "44dp",
		height: "44dp",
		right: "0dp",
		buttonID: "mailBoxBackButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		//minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 35,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/PuzzleFactoryIcon.png",
		width: 39,
		height: 36
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/PuzzleFactorySign.png",
		width: 177,
		height: 29,
		left: 10
	}));
	
	
	var refreshButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 70,
		height: 44,
		left: 0,
		buttonID: "refreshButton"
	});
	
	refreshButton.addEventListener("singletap", function(){
		if (refreshButton.touchEnabled == true) view.fireEvent("refresh");
	});
	
	refreshButton.add(Ti.UI.createLabel({
		text: " X ",
		textAlign: "center",
		height: 25,
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(refreshButton);
	
	view.add(title);
	view.add(closeButton);
	//view.add(refreshButton);
	
	
	return view;
};