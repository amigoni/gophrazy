exports.createView = function(){
	var messages = globals.col.phrases.find({written: true},{$sort:{createdTime:3}});
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44-35,
		top: 44+35,
		separatorColor: "transparent",
		separatorStyle: false,
		bubbleParent: true
	});
	
	Common.MakeDynamicTableView(tableView, messages, 20, CreateRow, CreateBottomRow, CreateWriteNewRow);
	
	tableView.addEventListener("delete", DeletePhrase);
	
	function UpdateCreatedPhrase(e){
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.rowData != null && row.rowData.phraseID == e.data.phraseID) {
				row.Update(e.data);
				break;
			}	
		}
	}
	
	function DeletePhrase(e){
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.rowData != null && row.rowData.phraseID == e.data.phraseID) {
				tableView.DeleteRow(i);
				break;
			}	
		}
	}
	
	function AddLatestPhrase(newRowData){
		tableView.AddRow(newRowData.data, 0);
	}
	
	function Close(){
		Ti.App.removeEventListener("wrotePhrase", AddLatestPhrase);
		Ti.App.removeEventListener("updateCreatedPhrase", UpdateCreatedPhrase);
	}
	
	tableView.Close = Close;
	
	Ti.App.addEventListener("wrotePhrase", AddLatestPhrase);
	Ti.App.addEventListener("updateCreatedPhrase", UpdateCreatedPhrase);
	
	
	return tableView;
};


function CreateWriteNewRow(){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 55,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		//buttonID: "writeNewRow",
		touchEnabled: true,
		bubbleParent: true
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var bg = Ti.UI.createView({
		width: view.width,
		height: view.height,
		buttonID: "writeNewRow",
		touchEnabled: true,
	});
	
	Common.MakeButton(bg);
	
	bg.addEventListener("touchend", function(){
		if(bg.touchEnabled == true){
			if(globals.gameData.abilities.composePuzzle.enabled == true){
				globals.rootWin.OpenWindow("createPuzzleWin", {data: "new", callback: function(){}});
				if(globals.gameData.tutorialsData.composeOpened == false) {
					//Timeout to avoid conflict with button and tutorial step look at Common
					setTimeout(function(){
						globals.rootWin.OpenWindow("tutorialPopUp","composeOpened");
						globals.gameData.tutorialsData.composeOpened = true;
						globals.col.gameData.commit();
					},700);
				}	
			}	 
			else{	
				if(Titanium.Network.online == true) {
					if (Titanium.Platform.model == 'Simulator'){
						globals.rootWin.OpenWindow("smallSquarePopUp", "compose_puzzle_early_access");
					}
					else{
						
						if(globals.storeController.products.length > 0) globals.rootWin.OpenWindow("smallSquarePopUp", "compose_puzzle_early_access");
						else {
							globals.storeController.Init(function(){
								globals.rootWin.OpenWindow("smallSquarePopUp", "compose_puzzle_early_access");
							});
						}
					}
				}	
				else alert("There is no internet connection.\nYou need a connection to buy.");
			}
		}
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 30,
		layout: "horizontal",
		//touchEnabled: false
	});
	
	var puzzleIcon = Ti.UI.createImageView({
		image:"/images/defaultLogo.png",
		//touchEnabled: false,
		width: 37,
		//height: 34
	});
	
	var label = Ti.UI.createLabel({
		text: "Write New",
		font: globals.fonts.bold25,
		//minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: Ti.UI.SIZE,
		height: 30,
		left: 10,
		//touchEnabled: false
	});
	
	container.add(puzzleIcon);
	container.add(label);
	
	
	bg.add(Ti.UI.createImageView({image:"/images/arrowSmall.png", height: 22, width: 12, right: 10}));
	bg.add(container);
	view.add(bg);
	
	return view;
}


function CreateRow (rowData){
	var RowButtonBar = require("ui/Common/RowButtonBar");
	
	var then = new moment(rowData.createdTime);
	var now = clock.getNow();
	var difference = now.diff(then,"days");
	var timeText = "";
	if (difference > 1 || difference == 0) timeText = difference+"\ndays";
	else timeText = difference+"\nday";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 60,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		buttonID: "mailBoxRow",
		touchEnabled: true,
		rowData: rowData
	});
	
	if (globals.tutorialOn == true) Common.MakeButton(view);
	
	view.addEventListener("touchend", function(e){
		var author = globals.col.friends.find({facebookID: globals.gameData.userFacebookID})[0].name;
		var rowButtonBar = RowButtonBar.createView({shareButton: true, sendButton: true, deleteButton: true, editButton: true});
		rowButtonBar.addEventListener("share", function(){
			var ShareController = require("/controller/ShareController");
			var text = crypt.decrypt(rowData.text);
			ShareController.shareMenu({
				picture: ShareController.CreateSharePhraseImage(text, author).toImage(),
				text: text,
				message: "Try this one! #GoPhrazy",
				type: "help"
			});
		});
		
		rowButtonBar.addEventListener("send", function(){
			var AchievementsController = require("/controller/AchievementsController");
			AchievementsController.IncreaseAchievement("compose_send_puzzle", 1, true);
			globals.rootWin.OpenWindow("friendPickerMessagesPopUp", {text: rowData.text, author: author});
		});
		
		rowButtonBar.addEventListener("delete", function(){
			globals.col.phrases.remove({phraseID: rowData.phraseID});
			globals.col.phrases.commit();
			view.fireEvent("delete", {data: rowData});
		});
		
		rowButtonBar.addEventListener("edit", function(){
			//view.fireEvent("create", {data: rowData});
			globals.rootWin.OpenWindow("createPuzzleWin", {data: rowData, callback: function(){}});
		});
		
		view.add(rowButtonBar);
		container.ExitAnimation();
		rowButtonBar.EnterAnimation();
		
		setTimeout(function(){
			rowButtonBar.ExitAnimation();
			container.EnterAnimation();
			view.remove(rowButtonBar);
			rowButtonBar = null;
		},3000);
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	container.ExitAnimation = function(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			easing: Animator["EXP_OUT"]
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
	};
	
	container.EnterAnimation = function(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			easing: Animator["EXP_OUT"]
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	};
	
	var label1 = Ti.UI.createLabel({
		text: crypt.decrypt(rowData.text),
		font: globals.fonts.bold14,
		//minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: view.width-20,
		height: 45,
		left: 10,
		top: 5,
		touchEnabled: false
	});
	
	var authorLabel = Ti.UI.createLabel({
		//backgroundColor: "yellow",
		text: rowData.author,
		font: globals.fonts.regular10r,
		color: globals.colors.darkBrown,
		textAlign: "left",
		width: view.width-20,
		height: 12,
		//minimumFontSize: 10,
		left: 10,
		bottom: 3,
		touchEnabled: false
	});

	var timeLabel = Ti.UI.createLabel({
		//backgroundColor: "yellow",
		text: then.calendar(),
		font: globals.fonts.regular10r,
		color: globals.colors.darkBrown,
		textAlign: "right",
		width: 40,
		height: 12,
		minimumFontSize: 7,
		right: 10,
		top: 3,
		touchEnabled: false
	});
	
	view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, bottom:0, touchEnabled: false}));
	container.add(label1);
	container.add(authorLabel);
	container.add(timeLabel);
	
	view.add(container);
	
	
	function Update(newData){
		label1.text = crypt.decrypt(newData.text);
		authorLabel.text = newData.author;
	}
	
	view.Update = Update;
	
	
	return view;
};


function CreateBottomRow(nextRowToLoad, totalRows){
	var text;
	if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
	else text = "("+nextRowToLoad+"/"+totalRows+")";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		height: 55,
		selectionStyle: false
	});
	
	view.addEventListener("singletap", function(e){
		view.fireEvent("more");
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var bg = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE
	});
	
	view.add(label);
	view.add(bg);
	
	function Update(nextRowToLoad, totalRows){
		if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
		else text = "("+nextRowToLoad+"/"+totalRows+")";
		label.text = text;
	}
	
	view.Update = Update;
	
	
	return view;
};


function CreateTipLabel (){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: "20dp"
	});
	
	var label = Ti.UI.createLabel({
		text:"Send and receive any Puzzles",
		font: globals.fonts.bold13,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		textAlign: "center",
		width: view.width-20,
		top: 15
	});
	
	view.add(label);
	view.add(Ti.UI.createView({height:5}));
	
	
	return view;
}
