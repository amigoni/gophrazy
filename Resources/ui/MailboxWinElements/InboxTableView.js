exports.createView = function(messages){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44-35,
		top: 44+35,
		separatorColor: "transparent",
		separatorStyle: false,
		bubbleParent: true
	});
	
	var tipLabel = CreateTipLabel();
	
	if(messages.length == 0) tableView.add(tipLabel);
	
	Common.MakeDynamicTableView(tableView, messages, 20, CreateRow, CreateBottomRow);
	
	function UpdateTableViewComplete(e){
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.rowData.messageID == e.messageID) {
				row.Update();
				break;
			}	
		}
	}
	
	tableView.UpdateTableViewComplete = UpdateTableViewComplete;
	
	
	return tableView;
};


function CreateRow (rowData){
	var RandomPlus = require("lib/RandomPlus");
	var RowButtonBar = require("ui/Common/RowButtonBar");
	var GroupsController = require("controller/GroupsController");
	
	var message = rowData.senderMessageText;
	if(message == null || message == "null") message = "Try this!";
	
	var then = new moment(rowData.createdTime);
	var now = clock.getNow();
	var difference = now.diff(then,"days");
	var timeText = "";
	if (difference > 1 || difference == 0) timeText = difference+"\ndays";
	else timeText = difference+"\nday";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 55,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		//buttonID: "mailBoxRow",
		//touchEnabled: true,
		rowData: rowData,
		bubbleParent: true
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: view.height,
		buttonID: "mailBoxRow",
		touchEnabled: true,
	});
	
	//if (globals.tutorialOn == true)
	Common.MakeButton(container);
	
	container.addEventListener("touchend", function(e){
		if(container.touchEnabled == true){
			if(rowData.complete == false) OpenFromPlay();
			else{
				var rowButtonBar = RowButtonBar.createView({shareButton: true, sendButton: true});
				var phraseData = globals.col.phrases.find({phraseID: rowData.messageID})[0];
				rowButtonBar.addEventListener("share", function(){
					var ShareController = require("/controller/ShareController");
					
					var RandomPlus = require("/lib/RandomPlus"); 
					var correctWordsTextArray = phraseData.text.match(/\S+\s/g);	
					var shuffledWordsTexts = RandomPlus.ShuffleArray(correctWordsTextArray);
					ShareController.shareMenu({
						picture: ShareController.CreateSharePhraseImage(phraseData.text, phraseData.author).toImage(),
						shuffledWordsArray: shuffledWordsTexts, 
						correctWordsArray: correctWordsTextArray, 
						message: "Loved this one! #GoPhrazy",
						type: "help"
					});
				});
				
				rowButtonBar.addEventListener("send", function(){
					globals.rootWin.OpenWindow("friendPickerMessagesPopUp", {text: phraseData.text, author: phraseData.author});
				});
				
				view.add(rowButtonBar);
				container.ExitAnimation();
				rowButtonBar.EnterAnimation();
				
				setTimeout(function(){
					rowButtonBar.ExitAnimation();
					container.EnterAnimation();
					view.remove(rowButtonBar);
					rowButtonBar = null;
				},3000);
			}	
		}	
	});
	
	container.ExitAnimation = function(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			easing: Animator["EXP_OUT"]
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
	};
	
	container.EnterAnimation = function(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			easing: Animator["EXP_OUT"]
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	};
	
	var label1 = Ti.UI.createLabel({
		text: rowData.senderName,
		font: globals.fonts.bold14,
		//minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: globals.platform.width-115,
		height: 20,
		left: 10,
		top: 5,
		touchEnabled: false
	});
	
	var label2 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: message,
		font: globals.fonts.bold12,
		color: globals.colors.mediumBrown,
		textAlign: "left",
		width: globals.platform.width-75,
		height: 15,
		left: 10,
		top: 23,
		touchEnabled: false
	});
	
	var label3 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: CreatePuzzleLabel(rowData.puzzleText),
		font: globals.fonts.regular12r,
		color: globals.colors.mediumBrown,
		textAlign: "left",
		width: globals.platform.width-75,
		height: 15,
		left: 10,
		top: 35,
		touchEnabled: false
	});
	
	var label4 = Ti.UI.createLabel({
		//backgroundColor: "yellow",
		text: then.calendar(),
		font: globals.fonts.regular10r,
		color: globals.colors.darkBrown,
		textAlign: "right",
		width: 40,
		height: 12,
		minimumFontSize: 7,
		right: 65,
		top: 7,
		touchEnabled: false
	});
	
	var playButton = Ti.UI.createImageView({
		image: "/images/playSignNoShadow.png",
		width: 50,
		//height: 40,
		//top: 13,
		right: 10,
		//visible: rowData.bought,
		bubbleParent: false,
		buttonID: "mailboxBookRowPlayButton",
		visible: !rowData.complete
	});
	
	Common.MakeButton(playButton);
	
	playButton.addEventListener("singletap", function(){
		OpenFromPlay();
	});
	
	var sign;
	if(rowData.complete == false){
		sign = Ti.UI.createImageView({
			image: "/images/newSignHorizontal.png",
			width: 28,
			height: 13,
			top: 2,
			right: 5,
			visible: rowData.isNew
		});
	}
	else if(rowData.complete == true){
		sign = Ti.UI.createImageView({
			image: "/images/completeSign.png",
			width: 50,
			//height: 18,
			//bottom: 2,
			right: 10,
			//visible: rowData.isNew
		});
	}
	
	//view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, top:0, touchEnabled: false}));
	view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1, bottom:0, touchEnabled: false}));
	container.add(label1);
	container.add(label2);
	container.add(label3);
	container.add(label4);
	container.add(playButton);
	container.add(sign);
	
	view.add(container);
	
	function OpenFromPlay(){
		if (rowData.isNew == true) {
			var message = globals.col.messages.find({$id: rowData.$id})[0];
			message.isNew = false;
			globals.col.messages.commit();
			rowData = JSON.parse(JSON.stringify(message));
		}
		
		if(rowData.senderMessageText != null && rowData.senderMessageText != "null"){
			globals.rootWin.OpenWindow("noteDisplayPopUp",{senderMessageText:rowData.senderMessageText, senderName: rowData.senderName});
			setTimeout(function(){
				globals.rootWin.OpenWindow("gameWin", GroupsController.CreateGroup("mailbox", rowData.messageID));
			}, 500);
		}
		else{
			globals.rootWin.OpenWindow("gameWin", GroupsController.CreateGroup("mailbox", rowData.messageID));
		}
		
		setTimeout(Update, 2000);
	}
	
	function Update(){
		rowData = globals.col.messages.find({$id: rowData.$id})[0];
		if(rowData.isNew == false) sign.visible = false;
		if (rowData.complete == true){
			playButton.visible = false;
			
			sign.image = "/images/completeSign.png";
			sign.width = 50;
			sign.right = 10;
			sign.bottom = null;	
			sign.top = null;
			sign.visible = true;
		}
	}
	
	function CreatePuzzleLabel(startingPhrase){
		var startingPhrase = crypt.decrypt(startingPhrase)+" ";
		//var startingPhrase = startingPhrase+" ";
		var correctWordsTextArray = startingPhrase.match(/\S+\s/g);	
		var shuffledWords =  RandomPlus.ShuffleArray(correctWordsTextArray);
		
		var puzzlePhrase = "";
		for (var i = 0; i < shuffledWords.length; i++){
			puzzlePhrase += shuffledWords[i]+"/";
		}
		
		return puzzlePhrase;
	}
	
	view.Update = Update;
	
	
	return view;
};


function CreateBottomRow(nextRowToLoad, totalRows){
	var text;
	if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
	else text = "("+nextRowToLoad+"/"+totalRows+")";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		selectionStyle:  Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		height: 55
	});
	
	view.addEventListener("singletap", function(e){
		view.fireEvent("more");
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var bg = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE
	});
	
	view.add(label);
	view.add(bg);
	
	function Update(nextRowToLoad, totalRows){
		if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
		else text = "("+nextRowToLoad+"/"+totalRows+")";
		label.text = text;
	}
	
	view.Update = Update;
	
	
	return view;
};


function CreateTipLabel (){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: "20dp"
	});
	
	var label = Ti.UI.createLabel({
		text:"Send and receive any Puzzles",
		font: globals.fonts.bold13,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		textAlign: "center",
		width: view.width-20,
		top: 15
	});
	
	view.add(label);
	view.add(Ti.UI.createView({height:5}));
	
	
	return view;
}
