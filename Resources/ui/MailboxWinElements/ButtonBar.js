exports.createView = function(){
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: 30,
		layout: "horizontal",
		top: 44+5
	});
	
	
	var inboxButton = Ti.UI.createView({
		borderColor: globals.colors.darkBrown,
		backgroundColor: "#D5C7A3",
		borderWidth: 2,
		borderRadius: 4,
		width: 102,
		height: 30,
		bottom: 0,
		buttonID:"buttonBarInboxButton",
		touchEnabled: true
	});
	
	inboxButton.addEventListener("singletap", function(){
		if(this.touchEnabled == true){
			ResetAllButtons();
			this.backgroundColor = "#D5C7A3";
			view.fireEvent("clicked", {button: "inbox"});
		}	
	});
	
	Common.MakeButton(inboxButton);
	
	inboxButton.add(Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "Inbox",
		top: Ti.Platform.name == "iPhone OS" ? null : -3,
		font: globals.fonts.bold25,
		color: globals.colors.darkBrown
	}));
	
	
	var writeButton = Ti.UI.createView({
		borderColor: globals.colors.darkBrown,
		borderWidth: 2,
		borderRadius: 4,
		width: 100,
		height: 30,
		left: 3,
		bottom: 0,
		buttonID:"buttonBarWriteButton",
		touchEnabled: true
	});
	
	writeButton.addEventListener("singletap", function(){
		if(this.touchEnabled == true){
			ResetAllButtons();
			this.backgroundColor = "#D5C7A3";
			view.fireEvent("clicked", {button: "write"});
			if(globals.gameData.tutorialsData.writePuzzleTabOpened == false) {
				//Timeout to avoid conflict with button and tutorial step look at Common
				setTimeout(function(){
					globals.rootWin.OpenWindow("tutorialPopUp","writePuzzleTabOpened");
					globals.gameData.tutorialsData.writePuzzleTabOpened = true;
					globals.col.gameData.commit();
				},200);
			}
		}		
	});
	
	Common.MakeButton(writeButton);
	
	writeButton.add(Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "Write",
		top: Ti.Platform.name == "iPhone OS" ? null : -3,
		font: globals.fonts.bold25,
		color: globals.colors.darkBrown,
	}));
	
	
	view.add(inboxButton);
	view.add(writeButton);
	
	function ResetAllButtons(){
		inboxButton.backgroundColor = "transparent";
		writeButton.backgroundColor = "transparent";
	}
	
	
	return view;
};