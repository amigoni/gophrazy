function Create(data){
	var shareType = data.shareType;
	var puzzleData = data.data;
	
	if(shareType == "help") puzzleData.leadText = "Help me solve this. I'm stuck!";
	
	
	var ShareController = require("/controller/ShareController");
	var SMS = require('com.omorandi');
	var sms = SMS.createSMSDialog();
	
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		bubbleParent: false,
		zIndex: 1000
	});
	
	var shade = Ti.UI.createView({
		backgroundColor: "black",
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		opacity: 0
	});
	
	var closeButton = Ti.UI.createView({
		backgroundImage:"images/closeX.png",
		width: 28,
		height: 30,
		top: 0,//globals.platform.actualHeight/2-210,
		right: 0,//globals.platform.width/2-155,
		buttonID: "sharePuzzlePopUpCloseButon",
		touchEnabled: true,
		opacity: 0
	});
	
	Common.MakeButton(closeButton);
	
	closeButton.addEventListener("touchend", function(){
		view.fireEvent("close");
		Close();
	});
	
	var bgContainer = Ti.UI.createView({
		width: view.width,
		height: view.height,
		top: globals.platform.actualHeight,
	});
	
	var bg = Ti.UI.createView({
		backgroundColor: globals.colors.white,
		borderColor: globals.colors.black,
		borderWidth: 2,
		borderRadius: 15,
		width: 290,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var titleContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		top: 15
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "Share it",
		textAlign: "center",
		height: 25,
		font: globals.fonts.bold25,
		color: globals.colors.black
	});
	
	var titleIcon = Ti.UI.createView({
		backgroundImage:"images/shareButton.png",
		width: 37,
		height: 30,
		top: 0,
		left: 5
	});
	
	titleContainer.add(titleLabel);
	titleContainer.add(titleIcon);
	
	var subTitleLabel = Ti.UI.createLabel({
		text: "By clicking this link people will be able to play this puzzle.",
		textAlign: "center",
		width: bg.width-20,
		font: globals.fonts.bold15,
		color: globals.colors.black
	});
	
	var puzzleURLLabel = Ti.UI.createLabel({
		text: puzzleData.puzzleURL,
		textAlign: "center",
		width: bg.width-20,
		height: 15,
		font: globals.fonts.bold13,
		color: globals.colors.black,
		opacity: 0.5
	});
	
	var rowsData = [
		{icon:"facebookSquareIcon.png", iconHeight: 40, title: "Facebook", subtitle: "Post your puzzle for your friends to solve", callback: FacebookCallback},
		{icon:"twitterIcon.png",  iconHeight: 30, title: "Twitter", subtitle: "Tease your followers with your puzzle", callback: TwitterCallback},
		{icon:"mailBoxIcon.png",  iconHeight: 30, title: "E-mail", subtitle: "Challenge your friends directly", callback: EmailCallback},
		{icon:"messagesIcon.png", iconHeight: 35, title: "Message", subtitle: "Send it by SMS", callback: MessageCallback},
		{icon:"copyPasteIcon.png",  iconHeight: 30, title: "Copy & Paste", subtitle: "Post your puzzle anywhere you want", callback: CopyCallback},
	];
	
	var rowScrollView = Ti.UI.createScrollView({
		//backgroundColor: "red",
		width: bg.width,
		height: 295,
		top: 0,
		contentWidth: bg.width,
		contentHeight: "auto"
	});
	
	var rowContainer = Ti.UI.createView({
		width: rowScrollView.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 20
	});
	
	rowContainer.addEventListener("postlayout", function(){
		closeButton.top = globals.platform.actualHeight/2-bg.size.height/2-10;
		closeButton.right = globals.platform.width/2 -bg.size.width/2-10;
	});
	
	for(var i = 0; i < rowsData.length; i++){
		var row = CreateRow(rowsData[i], puzzleData);
		if(rowsData[i].title != "Message") rowContainer.add(row);
		else if(rowsData[i].title == "Message" && sms.isSupported() == true) rowContainer.add(row);
	}
	
	//rowScrollView.add(rowContainer);
	
	bg.add(titleContainer);
	bg.add(subTitleLabel);
	bg.add(puzzleURLLabel);
	bg.add(rowContainer);
	bg.add(Ti.UI.createView({height:10}));
	
	bgContainer.add(bg);
	bgContainer.add(closeButton);
	
	view.add(shade);
	view.add(bgContainer);
	
	
//FUNCTIONS	
	function FacebookCallback(){
		ShareController.CreateSharePublicPhraseImage(
			crypt.decrypt(puzzleData.text), 
			puzzleData.leadText, 
			"click the link to solve", 
			"facebook", 
			function(image){
				ShareController.SharePublicPuzzle({
					picture: image,
					shareType: shareType,
					channel: "facebook",
					url: puzzleData.puzzleURL
				});
			}
		);
	};
	
	function TwitterCallback(){
		ShareController.CreateSharePublicPhraseImage(
			crypt.decrypt(puzzleData.text), 
			puzzleData.leadText, 
			"click the link to solve", 
			"twitter", 
			function(image){
				ShareController.SharePublicPuzzle({
					picture: image,
					shareType: shareType,
					channel: "twitter",
					url: puzzleData.puzzleURL
				});
			}
		);
	};
	
	function EmailCallback(){
		ShareController.CreateSharePublicPhraseImage(
			crypt.decrypt(puzzleData.text), 
			puzzleData.leadText, 
			"click the link to solve", 
			"email", 
			function(image){
				ShareController.SharePublicPuzzle({
					picture: image,
					shareType: shareType,
					channel: "email",
					url: puzzleData.puzzleURL
				});
			}
		);
	};
	
	function MessageCallback(){
		ShareController.CreateSharePublicPhraseImage(
			crypt.decrypt(puzzleData.text), 
			puzzleData.leadText, 
			"click the link to solve", 
			"message", 
			function(image){
				ShareController.SharePublicPuzzle({
					picture: image,
					shareType: shareType,
					channel: "message",
					url: puzzleData.puzzleURL
				});
			}
		);
	};
	
	function CopyCallback(){
		ShareController.SharePublicPuzzle({
			shareType: shareType,
			//messageBody: "#GoPhrazy Click the link to solve! ",
			channel: "copyPaste",
			url: puzzleData.puzzleURL
		});
	};
	
	
	function Open(){
		shade.animate(Ti.UI.createAnimation({
			duration: 250,
			opacity: 0.6
		}));
		
		Common.WindowOpenAnimation(bgContainer, function(){
			closeButton.opacity = 1;
		});
	}
	
	function Close(){
		closeButton.opacity = 0;
		Common.WindowCloseAnimation(bgContainer, function(){
			view.fireEvent("closed");
		});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
}

module.exports = Create;


function CreateRow(args, puzzleData){
	var view = Ti.UI.createView({
		width: 270,
		height: 50,
		buttonID: 'sharePuzzlePopUpRow',
		touchEnabled: true
	});
	
	Common.MakeButton(view);
	
	view.addEventListener("touchend", function(){
		Common.CreateButtonBlocker(true,true);
		args.callback();
	});
	
	var iconContainer = Ti.UI.createView({
		width: 50,
		height: 40,
		left: 0	
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/"+args.icon,
		height: args.iconHeight
	});
	
	iconContainer.add(icon);
	
	var rightContainer = Ti.UI.createView({
		width: view.width-60,
		height: Ti.UI.SIZE,
		left: 60,
		layout: "vertical"
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: args.title,
		textAlign: "left",
		width: rightContainer.width,
		font: globals.fonts.bold15,
		color: globals.colors.black
	});
	
	var subtitleLabel =  Ti.UI.createLabel({
		text: args.subtitle,
		textAlign: "left",
		width: rightContainer.width,
		font: globals.fonts.regular11,
		color: globals.colors.black,
		top: - 5
	});
	
	rightContainer.add(titleLabel);
	rightContainer.add(subtitleLabel);
	
	
	view.add(iconContainer);
	view.add(rightContainer);
	
	
	return view;
};
