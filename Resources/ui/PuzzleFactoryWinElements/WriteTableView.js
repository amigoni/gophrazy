exports.createView = function(puzzlesData){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 44,
		separatorColor: "transparent",
		separatorStyle: false,
		bubbleParent: true
	});
	
	Common.MakeDynamicTableView(tableView, puzzlesData, 20, CreateRow, CreateBottomRow, CreateHeaderRow);
	
	tableView.addEventListener("delete", DeletePhrase);
	
	function UpdateCreatedPhrase(e){
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.rowData != null && row.rowData.phraseID == e.data.phraseID) {
				row.Update(e.data);
				break;
			}	
		}
	}
	
	function DeletePhrase(e){
		var dialog = Ti.UI.createAlertDialog({
		    cancel: 1,
			buttonNames: ['Delete', 'Cancel'],
			message: "Are you sure?\nPeople won't be able to find and solve this puzzle anymore.",
			title: 'Delete Puzzle?'
		});
		  
		dialog.addEventListener('click', function(e1){
			if(e1.index == 0){
				for (var i = 0; i < tableView.data[0].rows.length; i++){
					var row = tableView.data[0].rows[i];
					if(row.rowData != null && row.rowData.phraseID == e.data.phraseID) {
						var PuzzleFactoryController = require("/controller/PuzzleFactoryController");
						PuzzleFactoryController.DeletePuzzle(row.rowData.authorID, row.rowData.phraseID.toString(), function(e2){
							tableView.DeleteRow(i);
						});
						break;
					}	
				}
			}	
		   	else{
		   	
		   	}
		}); 
		dialog.show();
	}
	
	function AddLatestPhrase(newRowData){
		tableView.AddRow(newRowData.data, 0);
	}
	
	function Close(){
		Ti.App.removeEventListener("wrotePhrase", AddLatestPhrase);
		Ti.App.removeEventListener("updateCreatedPhrase", UpdateCreatedPhrase);
	}
	
	tableView.Close = Close;
	
	Ti.App.addEventListener("wrotePhrase", AddLatestPhrase);
	Ti.App.addEventListener("updateCreatedPhrase", UpdateCreatedPhrase);
	
	
	return tableView;
};


function CreateHeaderRow(){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		//buttonID: "writeNewRow",
		touchEnabled: true,
		bubbleParent: true,
		layout: "vertical"
	});
	
	
	var topLabel = Ti.UI.createLabel({
		text: "Create & Share your own puzzles",
		font: globals.fonts.bold15,
		//minimumFontSize: 12,
		color: globals.colors.black,
		width: Ti.UI.SIZE,
		height: 30,
		//touchEnabled: false
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var writeNewButton = Ti.UI.createView({
		backgroundImage: "/images/buttonBg.png",
		backgroundTopCap: 20,
		backgroundLeftCap: 20,
		width: 300,
		height: 54,
		buttonID: "writeNewRow",
		touchEnabled: true,
	});
	
	Common.MakeButton(writeNewButton);
	
	writeNewButton.addEventListener("touchend", function(){
		if(writeNewButton.touchEnabled == true){
			if(globals.gameData.abilities.composePuzzle.enabled == true){
				globals.rootWin.OpenWindow("createPuzzleWin", {data: "new", callback: function(){}});
				if(globals.gameData.tutorialsData.composeOpened == false) {
					//Timeout to avoid conflict with button and tutorial step look at Common
					setTimeout(function(){
						globals.rootWin.OpenWindow("tutorialPopUp","composeOpened");
						globals.gameData.tutorialsData.composeOpened = true;
						globals.col.gameData.commit();
					},700);
				}	
			}	 
			else{	
				if(Titanium.Network.online == true) {
					if (Titanium.Platform.model == 'Simulator'){
						globals.rootWin.OpenWindow("smallSquarePopUp", "compose_puzzle_early_access");
					}
					else{
						
						if(globals.storeController.products.length > 0) globals.rootWin.OpenWindow("smallSquarePopUp", "compose_puzzle_early_access");
						else {
							globals.storeController.Init(function(){
								globals.rootWin.OpenWindow("smallSquarePopUp", "compose_puzzle_early_access");
							});
						}
					}
				}	
				else alert("There is no internet connection.\nYou need a connection to buy.");
			}
		}
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 30,
		layout: "horizontal",
		//touchEnabled: false
	});
	
	var puzzleIcon = Ti.UI.createImageView({
		image:"/images/defaultLogo.png",
		//touchEnabled: false,
		width: 37,
		//height: 34
	});
	
	var label = Ti.UI.createLabel({
		text: "Write New Puzzle",
		font: globals.fonts.bold25,
		//minimumFontSize: 12,
		color: globals.colors.black,
		width: Ti.UI.SIZE,
		height: 30,
		left: 10,
		//touchEnabled: false
	});
	
	container.add(puzzleIcon);
	container.add(label);
	
	writeNewButton.add(container);
	
	var bottomLabel = Ti.UI.createLabel({
		text: " My Puzzles ",
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.black,
		width: Ti.UI.SIZE,
		height: 30,
		top: 15
		//touchEnabled: false
	});
	
	view.add(topLabel);
	view.add(writeNewButton);
	view.add(bottomLabel);
	view.add(Ti.UI.createView({backgroundColor: globals.colors.black, width:view.width, height:1, top:0, touchEnabled: false, opacity:.5}));	
	
	
	return view;
}


function CreateRow (rowData){
	var RowButtonBar = require("ui/Common/RowButtonBar");
	var then = new moment(rowData.createdDate);
	
	if(rowData.author == null) rowData.author = "";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		buttonID: "createPuzzleRow",
		touchEnabled: true,
		rowData: rowData
	});
	
	if (globals.tutorialOn == true) Common.MakeButton(view);
	
	view.addEventListener("touchend", function(e){
		var author = rowData.author;
		var rowButtonBar = RowButtonBar.createView({shareButtonLarge: true, deleteButton: true});
		rowButtonBar.addEventListener("share", function(){
			globals.rootWin.OpenWindow("sharePuzzlePopUp",{data: rowData, shareType:"written"});
		});
		
		rowButtonBar.addEventListener("delete", function(){
			view.fireEvent("delete", {data: rowData});
		});
		
		view.add(rowButtonBar);
		container.ExitAnimation();
		rowButtonBar.EnterAnimation();
		
		setTimeout(function(){
			rowButtonBar.ExitAnimation();
			container.EnterAnimation();
			view.remove(rowButtonBar);
			rowButtonBar = null;
		},3000);
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	container.ExitAnimation = function(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			easing: Animator["EXP_OUT"]
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
	};
	
	container.EnterAnimation = function(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			easing: Animator["EXP_OUT"]
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	};
	
	var label1 = Ti.UI.createLabel({
		text: crypt.decrypt(rowData.text),
		font: globals.fonts.bold14,
		//minimumFontSize: 12,
		color: globals.colors.black,
		width: container.width-20,
		height: Ti.UI.SIZE,
		left: 10,
		touchEnabled: false
	});
	
	label1.addEventListener("postlayout", function(){
		//if(view.size.height < 55) view.height = 55;
	});
	
	var authorLabel = Ti.UI.createLabel({
		//backgroundColor: "yellow",
		text: rowData.author.length > 0 ? "-"+rowData.author : null,
		font: globals.fonts.regular12,
		color: globals.colors.black,
		textAlign: "left",
		width: container.width-20,
		height: rowData.author.length > 0 ? 12 : 0,
		//minimumFontSize: 10,
		top: 0,
		left: 10,
		touchEnabled: false
	});

	var bottomContainer = Ti.UI.createView({
		width: container.width,
		height: 13,
		top: 5
	});
	
	var timeLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: then.calendar(),
		font: globals.fonts.regular10r,
		color: globals.colors.black,
		textAlign: "left",
		width: Ti.UI.SIZE,
		height: 12,
		//minimumFontSize: 7,
		left: 10,
		//bottom: 1,
		touchEnabled: false
	});
	
	var solvedContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 12,
		layout: "horizontal",
		right: 10
	});
	
	var solvedLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "Solved: "+rowData.solvedCountOthersOld,
		font: globals.fonts.bold12,
		color: globals.colors.black,
		textAlign: "right",
		width: Ti.UI.SIZE,
		height: 12,
		//minimumFontSize: 7,
		//bottom: 1,
		touchEnabled: false,
		opacity: 0.5
	});
	
	var difference = rowData.solvedCountOthers - rowData.solvedCountOthersOld;
	var solvedDiff = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "+"+difference,
		font: globals.fonts.bold12,
		color: globals.colors.blue,
		textAlign: "right",
		width: Ti.UI.SIZE,
		height: 12,
		touchEnabled: false
	});
	
	
	solvedContainer.add(solvedLabel);
	if(difference > 0) solvedContainer.add(solvedDiff);
	
	bottomContainer.add(timeLabel);
	bottomContainer.add(solvedContainer);
	
	
	container.add(label1);
	container.add(authorLabel);
	container.add(bottomContainer);
	container.add(Ti.UI.createView({backgroundColor: globals.colors.black, width:view.width, height:1, touchEnabled: false, opacity:.5}));
	
	view.add(container);
	
	
	function Update(newData){
		label1.text = crypt.decrypt(newData.text);
		authorLabel.text = newData.author;
	}
	
	view.Update = Update;
	
	
	return view;
};


function CreateBottomRow(nextRowToLoad, totalRows){
	var text;
	if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
	else text = "("+nextRowToLoad+"/"+totalRows+")";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null,
		height: 55,
		selectionStyle: false
	});
	
	view.addEventListener("singletap", function(e){
		view.fireEvent("more");
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var bg = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE
	});
	
	view.add(label);
	view.add(bg);
	
	function Update(nextRowToLoad, totalRows){
		if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
		else text = "("+nextRowToLoad+"/"+totalRows+")";
		label.text = text;
	}
	
	view.Update = Update;
	
	
	return view;
};


function CreateTipLabel (){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: "20dp"
	});
	
	var label = Ti.UI.createLabel({
		text:"Send and receive any Puzzles",
		font: globals.fonts.bold13,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		textAlign: "center",
		width: view.width-20,
		top: 15
	});
	
	view.add(label);
	view.add(Ti.UI.createView({height:5}));
	
	
	return view;
}
