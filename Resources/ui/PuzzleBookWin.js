exports.loadWin = function(){
	var GroupsController = require("controller/GroupsController");
	var ButtonBar = require("ui/MailboxWinElements/ButtonBar");
	var PuzzlesTableView = require("ui/PuzzleBookWinElements/PuzzlesTableView");
	
	var allSolvedPuzzles = globals.col.phrases.find({complete: true, type:{$nin: ["tutorial","message"]}},{$sort:{completeTime:-1}});
	var favsPuzzles = globals.col.phrases.find({complete: true, type:{$nin: ["tutorial"]}, favorite: true},{$sort:{completeTime:-1}});

	var groupData;
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	  	bubbleParent: false,
	  	top: globals.platform.height,//"-"+globals.platform.actualHeight,
	  	zIndex: 1
	});
	
	
	
	view.addEventListener("openGameWindow", function(e){
		/*
		if(e.senderMessageText != null && e.senderMessageText != "null"){
			globals.rootWin.OpenWindow("noteDisplayPopUp",{senderMessageText:e.senderMessageText, senderName: e.senderName});
			setTimeout(function(){
				globals.rootWin.OpenWindow("gameWin", GroupsController.CreateGroup("mailbox", e.phraseID));
			}, 500);
		}
		else{
			globals.rootWin.OpenWindow("gameWin", GroupsController.CreateGroup("mailbox", e.phraseID));
		}
		*/
	});
	
	var navBar = CreateNavBar();
	
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var buttonBar = CreateButtonBar();
	buttonBar.addEventListener("clicked", function(e) {
		if (e.button == "favs") {
			favsTableView.opacity = 1;
			favsTableView.visible = true;
			favsTableView.touchEnabled = true;
			allTableView.opacity = 0;
			allTableView.visible = false;
			allTableView.touchEnabled = false;
		} else if (e.button == "all") {
			favsTableView.opacity = 0;
			favsTableView.visible = false;
			favsTableView.touchEnabled = false;
			allTableView.opacity = 1;
			allTableView.visible = true;
			allTableView.touchEnabled = true;
		}	
	});
	
	var favsTableView = PuzzlesTableView.createView(favsPuzzles);
	var allTableView = PuzzlesTableView.createView(allSolvedPuzzles);
	allTableView.opacity = 0;
	allTableView.visible = false;
	allTableView.touchEnabled = false;
	
	
	
	view.add(navBar);
	view.add(buttonBar);
	view.add(favsTableView);
	view.add(allTableView);
	
	
	function Open(){
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}
	
	function Close(){
		Ti.App.removeEventListener("updateFavorites", UpdateFavorites);
		favsTableView.Close();
		favsTableView.Clean();
		allTableView.Close();
		allTableView.Clean();
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view, function(){view.fireEvent("closed");});
	}
	
	function UpdateFavorites(e){
		favsPuzzles = globals.col.phrases.find({complete: true, type:{$nin: ["tutorial",]}, favorite: true},{$sort:{completeTime:-1}});
		favsTableView.UpdateTableView(favsPuzzles);
	}
	
	Ti.App.addEventListener("updateFavorites", UpdateFavorites);
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: "#86684C",
		width: globals.platform.width,
		height: "44dp",
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: "44dp",
		height: "44dp",
		right: "0dp",
		touchEnabled: true,
		buttonID: "puzzleBookWinBackButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 35,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/collectionBookIconLarge.png",
		width: 65*.5,
		height: 59*.5
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/thePuzzleBookSign.png",
		width: 185,
		height: 24,
		left: 10
	}));
	
	view.add(title);
	view.add(closeButton);
	
	
	return view;
};


function CreateButtonBar(){
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: 30,
		layout: "horizontal",
		top: 44+5
	});
	
	
	var favsButton = Ti.UI.createView({
		borderColor: globals.colors.darkBrown,
		backgroundColor: "#D5C7A3",
		borderWidth: 2,
		borderRadius: 4,
		width: 102,
		height: 30,
		bottom: 0,
		layout: "horizontal"
	});
	
	favsButton.addEventListener("singletap", function(){
		ResetAllButtons();
		this.backgroundColor = "#D5C7A3";
		view.fireEvent("clicked", {button: "favs"});
	});
	
	Common.MakeButton(favsButton);
	
	favsButton.add(Ti.UI.createView({
		backgroundImage:"/images/favoriteIcon.png",
		//top: Ti.Platform.name == "iPhone OS" ? null : -3,
		width: 36*.7,
		height: 32*.7,
		left: 10,
		top: 4
	}));
	
	favsButton.add(Ti.UI.createLabel({
		text: "Favs",
		font: globals.fonts.bold25,
		top: -3,//Ti.Platform.name == "iPhone OS" ? null : -3,
		color: globals.colors.darkBrown,
		left: 5
	}));
	
	
	var allButton = Ti.UI.createView({
		borderColor: globals.colors.darkBrown,
		borderWidth: 2,
		borderRadius: 4,
		width: 100,
		height: 30,
		left: 3,
		bottom: 0
	});
	
	allButton.addEventListener("singletap", function(){
		ResetAllButtons();
		this.backgroundColor = "#D5C7A3";
		view.fireEvent("clicked", {button: "all"});
	});
	
	Common.MakeButton(allButton);
	
	allButton.add(Ti.UI.createLabel({
		text: "All",
		font: globals.fonts.bold25,
		top: Ti.Platform.name == "iPhone OS" ? null : -3,
		color: globals.colors.darkBrown,
	}));
	
	
	view.add(favsButton);
	view.add(allButton);
	
	
	function ResetAllButtons(){
		favsButton.backgroundColor = "transparent";
		allButton.backgroundColor = "transparent";
	}
	
	
	return view;
};