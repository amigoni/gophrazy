exports.loadWin = function(){
	var achievements = globals.col.achievements.getAll();
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//"-"+globals.platform.actualHeight,
	  	zIndex: 1
	});
	
	var navBar = CreateNavBar();
	
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 44,
		separatorColor: "transparent",
		separatorStyle: false,
	});
	
	var tableData = [];
	for(var i = 0; i < achievements.length; i++){
		var row = CreateRow(achievements[i]);
		tableData.push(row);
	}
	tableView.data = tableData;
	
	
	view.add(navBar);
	view.add(tableView);
	
	
	function Open(){
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}
	
	function Close(){
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.red,
		width: globals.platform.width,
		height: "44dp",
		top: "0dp"
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		touchEnabled: true,
		buttonID: "puzzleBookWinBackButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 30,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/checkMarkButton.png.png",
		width: 29,
		height: 29,
		left: 0
	}));
	
	title.add(Ti.UI.createImageView({
		image:"/images/achievementsSign.png",
		left: 10,
		width: 143,
		height: 23
	}));
	
	view.add(title);
	view.add(closeButton);
	
	
	return view;
};


function CreateRow(rowData){
	var showScores = true;
	if(rowData.complete == true) {
		showScores = false;
		rowData.showCurrent = false;
		rowData.showBest = false;
	}	
	else{
		if(rowData.goalValue == 1) showScores = false;
	}
	
	if(rowData.showCurrent == false && rowData.showBest == false) showScores = false;
	
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: "45dp",
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle: globals.os ==" ios" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null,
		rowData: rowData
	});
	
	var icon = Ti.UI.createView({
		backgroundImage: "/images/checkMarkRed.png",
		width: 28,
		height: 25,
		right: 10,
		//top: "2dp"
	});
	
	var nameLabel = Ti.UI.createLabel({
		text: rowData.name,
		width: "240dp",
		height: "20dp",
		font: globals.fonts.bold15,
		minimumFontSize: "10dp",
		color: globals.colors.darkBrown,
		textAlign: "left",
		top: showScores == true ? "5dp" : null,
		left: "10dp"
	});
	
	var currentLabel = Ti.UI.createLabel({
		text: "Current: "+rowData.currentValue,
		//width: ,
		font: globals.fonts.bold10,
		color: globals.colors.mediumBrown,
		textAlign: "left",
		bottom: "2dp",
		left: "10dp"
	});
	
	var bestLabel = Ti.UI.createLabel({
		text: "Best: "+rowData.bestValue,
		//width: ,
		font: globals.fonts.bold10,
		color: globals.colors.mediumBrown,
		textAlign: "left",
		bottom: "2dp",
		left: "75dp"
	});
		
	if(rowData.complete == true) row.add(icon);
	row.add(nameLabel);
	if(rowData.showCurrent == true)	row.add(currentLabel);
	if(rowData.showBest == true)row.add(bestLabel);
	 
	row.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width: row.width, height:"1dp", bottom:"0dp"}));
	
	
	function Close(){
		nameLabel = null;
		currentLabel = null;
		bestLabel = null;	
	}
	
	row.Close = Close;
	
	
	return row;
};