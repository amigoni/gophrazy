exports.createView = function(data){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-80,
		top: 80,
		separatorColor: "transparent",
		separatorStyle: false,
	});
	
	var tipLabel = CreateTipLabel();
	tableView.add(tipLabel);
	if(data.length > 0) tipLabel.opacity = 0;
	
	tableView.addEventListener("update", function(e){
		if(e.totalRows > 0) tipLabel.opacity = 1;
	});
	
	Common.MakeDynamicTableView(tableView, data, 20, CreatePhraseRow, CreateBottomRow);
	
	function UpdatePuzzleBookTableView(e){
		var rows = tableView.data[0].rows;
		for(var i = 0; i < rows.length; i++){
			if(rows[i].rowData.phraseID == e.phraseID) rows[i].Update();
		};
	}
	
	function Clean(){
		Ti.App.removeEventListener("phraseComplete", UpdatePuzzleBookTableView);
	}
	
	tableView.Clean = Clean;
	
	Ti.App.addEventListener("phraseComplete", UpdatePuzzleBookTableView);
	
	return tableView;
};


function CreatePhraseRow(rowData){
	var RowButtonBar = require("ui/Common/RowButtonBar");
	var ShareController = require("/controller/ShareController");
	var GroupsController = require("controller/GroupsController");
	var author = rowData.author != "" ? rowData.author : "Anonymous";
	
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle: globals.os == "ios" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null,
		rowData: rowData,
		className: "puzzleBookPhraseRow"
	});
	
	row.addEventListener("singletap", function(e){
		var rowButtonBar = RowButtonBar.createView({shareButtonLarge: true, playButton: true, favoriteButton:true});
		if(rowData.favorite == true) rowButtonBar.favoriteButton.image.image = "/images/favoriteIcon.png";
		else rowButtonBar.favoriteButton.image.image = "/images/favoriteIconGrey.png";
		
		rowButtonBar.addEventListener("share", function(){
			globals.rootWin.OpenWindow("sharePuzzlePopUp",{data: rowData, shareType: "general"});
		});
		
		rowButtonBar.addEventListener("send", function(){
			globals.rootWin.OpenWindow("friendPickerMessagesPopUp", {text: rowData.text, author: author});
		});
		
		rowButtonBar.addEventListener("play", function(){
			var groupData = GroupsController.CreateGroup("singlePhrase", rowData.phraseID);
			globals.rootWin.OpenWindow("gameWin", groupData);
		});
		
		rowButtonBar.addEventListener("favorite", function(){
			var PhrasesController = require("/controller/PhrasesController");
			var isNowFavorite = PhrasesController.TogglePhraseFavorite(rowData.phraseID);
			rowData.favorite = isNowFavorite;
			if(rowData.favorite == true) rowButtonBar.favoriteButton.image.image = "/images/favoriteIcon.png";
			else rowButtonBar.favoriteButton.image.image = "/images/favoriteIconGrey.png";
			Ti.App.fireEvent("updateFavorites");
		});
		
		row.add(rowButtonBar);
		container.ExitAnimation();
		rowButtonBar.EnterAnimation();
		
		setTimeout(function(){
			rowButtonBar.ExitAnimation();
			container.EnterAnimation();
			setTimeout(function(){
				row.remove(rowButtonBar);
				rowButtonBar = null;
			}, 1000);
		},3000);
	});
	
	var container = Ti.UI.createView({
		width: row.width,
		height: row.height,
		layout: 'vertical',
		left: 0
	});
	
	container.ExitAnimation = function(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			easing: Animator["EXP_OUT"]
		});
		*/
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: -globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
		
		starContainer.opacity = 0;
	};
	
	container.EnterAnimation = function(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			easing: Animator["EXP_OUT"]
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: 0,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
		//container.left = 0;
		starContainer.opacity = 1;
	};
	
	var resultLabel = Ti.UI.createLabel({
		text: '"'+ crypt.decrypt(rowData.text)+'"',
		width: globals.platform.width-20,
		font: globals.fonts.words2,
		color: globals.colors.darkBrown,
		textAlign: "center",
		top: 20
	});
	
	var authorLabel = Ti.UI.createLabel({
		text: "- "+author,
		//width: ,
		font: globals.fonts.regular1,
		color: globals.colors.darkBrown,
		textAlign: "right",
		top: 2,
		right: 20
	});
		
	container.add(resultLabel);
	container.add(authorLabel);
	container.add(Ti.UI.createView({height:10}));
	
	var starContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		bottom: 15,
		left: 10
	});
	
	row.add(container);
	//row.add(starContainer);
	
	function Update(){
		rowData = globals.col.phrases.find({phraseID:rowData.phraseID})[0];
		/*
		row.remove(starContainer);
		starContainer = Ti.UI.createView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			layout: "horizontal",
			left: 10,
			bottom: 15
		});
		
		for(var i = 0; i < 3; i++){
			var image =  "/images/star.png";
			if(rowData.stars[i].complete == false) image = "/images/starBigShadow.png";
			
			starContainer.add(Ti.UI.createView({
				backgroundImage:image,
				width: 15,
				height: 15,
				left: 3
			}));
		}
		
		row.add(starContainer);
		*/
	}
	
	function Close(){
		author = null;
		container = null;
		resultLabel = null;
		authorLabel = null;	
	}
	
	row.Close = Close;
	row.Update = Update;
	
	Update();
	
	return row;
};


function CreateBottomRow(nextRowToLoad, totalRows){
	var text;
	if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
	else text = "("+nextRowToLoad+"/"+totalRows+")";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		selectionStyle: globals.os == "ios" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null,
		height: 55,
		selectionStyle: false,
		className: "puzzleBookBottomRow"
	});
	
	view.addEventListener("singletap", function(e){
		view.fireEvent("more");
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var bg = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold20,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE,
		height: 25,
		opacity: totalRows == 0 ? 0 : 1
	});
	
	view.add(label);
	view.add(bg);
	
	function Update(nextRowToLoad, totalRows){
		if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
		else text = "("+nextRowToLoad+"/"+totalRows+")";
		label.text = text;
		label.opacity = totalRows == 0 ? 0 : 1;
		view.fireEvent("update",{totalRows: totalRows});
	}
	
	view.Update = Update;
	
	
	return view;
};


function CreateNoPhrasesRow (){
	var row = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 60,
		backgroundColor: "transparent",
		borderColor: globals.colors.borderLine,
		selectionStyle:  globals.os == "ios" ?  Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null,
		layout:"vertical"
	});
	
	var label = Ti.UI.createLabel({
		text: 'No phrases completed.\nCompleted phrases will show here.',
		width: globals.platform.width-20,
		height: 15,
		font: globals.fonts.bold13,
		color: globals.colors.mediumBrown,
		textAlign: "center",
		className: "puzzleBookNoPhrasesRow"
	});
	
	row.add(label);
	
	function Close(){
		label = null;
	}
	
	row.Close = Close;
	
	return row;
}


function CreateTipLabel (){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: "25dp"
	});
	
	var label = Ti.UI.createLabel({
		text:"Your solved puzzles will appear here.",
		font: globals.fonts.bold13,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		textAlign: "center",
		width: view.width-20,
		top: 15
	});
	
	view.add(label);
	view.add(Ti.UI.createView({height:5}));
	
	
	return view;
}
