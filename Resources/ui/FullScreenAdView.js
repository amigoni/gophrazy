function Create(){
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.black,
		zIndex: 1000,
		width: 320,
		height: 250,
		opacity: 0,
		adPresent: false
	});
	
	//AdMob
	/*
	if(Ti.Platform.name == "iPhone OS"){
		var Admob = require('ti.admob');
		var ad = Admob.createView({
		    top: 0, 
		    width: view.width, 
		    height: view.height,
		    adUnitId: 'ca-app-pub-7157276024136518/8463591589', // You can get your own at http: //www.admob.com/
		    adBackgroundColor: 'transparent'
		    //testDevices: [ GAD_SIMULATOR_ID ]
		   // dateOfBirth: new Date(1985, 10, 1, 12, 1, 1),
		   // gender: 'male',
		   // keywords: ''
		});
		
		ad.addEventListener('didReceiveAd', function() {
		    alert('Did receive ad!');
		    view.adPresent = true;
		});
		
	
		ad.addEventListener('didFailToReceiveAd', function() {
		    Ti.API.info('Failed to receive ad!');
		});
		ad.addEventListener('willPresentScreen', function() {
		     Ti.API.info('Presenting screen!');
		});
		ad.addEventListener('willDismissScreen', function() {
		     Ti.API.info('Dismissing screen!');
		     
		});
		ad.addEventListener('didDismissScreen', function() {
		     Ti.API.info('Dismissed screen!');
		     view.adPresent = false;
		     TurnOff();
		});
		ad.addEventListener('willLeaveApplication', function() {
		     Ti.API.info('Leaving the app!');
		});
		
		view.add(ad);
	}
	else if (Ti.Platform.name == "android"){
		var Admob = require("metal.admobgoogleplay");
		var ad = Admob.createView({
			top: 0,
			//width: 320,
			//height: 50,
			adSizeType: 'INTERSTITIALAD',
			publisherId: 'ca-app-pub-7157276024136518/4042321183',
			testing: false
		});	
		
		ad.addEventListener('ad_received', function(e) {
			Ti.API.info("ADMOB: Received ad");
		});
		
		ad.addEventListener('ad_not_received', function(e) {
			Ti.API.info("ADMOB: NOT Received ad");
		});
		
		view.add(ad);
	}
	*/
	
	//InMobi
	/*
	var Inmobi = require('ti.inmobi');

	var appId = "482754602a8d40d1838a7932ab1989f1";
	// if (OS_ANDROID)
	// {
	// 	appId = "68755e31ebf943c5baa77128523a4de4";
	// }
	
	// You can get your APP ID from www.inmobi.com
	// For Optional parameters
	// Visit: https://docs.google.com/a/inmobi.com/document/d/168iVSDEW5NPqMlWAAf4m5wZwcsiPKNQ3WwNgBFcV4CA/edit
	Inmobi.initWithParams(appId, {
		// Set User Information - Optional (for better Ad Targeting) 
		"logLevel": Inmobi.CONSTANTS.LOGLEVEL_DEBUG,
		//"gender": Inmobi.CONSTANTS.GENDER_MALE,
		//"education": Inmobi.CONSTANTS.EDUCATION_HIGHSCHOOLORLESS,
		//"ethnicity": Inmobi.CONSTANTS.ETHNICITY_ASIAN, 
		//"dob": "29-11-1984",
		//"income": "10000",
		//"age": "30",
		//"maritalStatus": Inmobi.CONSTANTS.MARITAL_STATUS_SINGLE,
		//"hasChildren": Inmobi.CONSTANTS.TRUE,
		//"sexualOrientation": Inmobi.CONSTANTS.SEXUAL_ORIENTATION_STRAIGHT,
		//"language": "eng",
		//"postalCode": "11111",
		//"areaCode": "435",
		//"interests": "swimming, adventure sports",
		//"deviceIdMasks": [Inmobi.CONSTANTS.EXCLUDE_ADVERTISER_ID, Inmobi.CONSTANTS.EXCLUDE_ODIN1, Inmobi.CONSTANTS.EXCLUDE_UDID]
	});
	
	
	var interstitialAd = Inmobi.createInterstitialAd({
	  // Optional Params
	  additionalParams: {},
	  keywords: 'music, jazz, guitar', // provides App View context.
	  "refTagKey": "",
	  "refTagValue": ""
	});
	
	// if (OS_ANDROID){
	// 	win.add(interstitialAd);	
	// }
	
	// Ad Request Lifecycle Messages
	interstitialAd.addEventListener('onAdRequestCompleted', function(){
		Ti.API.info("interstitial Ad Request Completed!");
	});
	interstitialAd.addEventListener('onAdRequestFailed', function(e){
		Ti.API.info("interstitial Ad Request Failed! "+e.message);
	});
	
	// Display-Time Lifecycle Messages
	interstitialAd.addEventListener('beforePresentScreen', function(){
		Ti.API.info("interstitial Ad will go fullscreen!");
	});
	interstitialAd.addEventListener('beforeDismissScreen', function(){
		Ti.API.info("interstitial Ad will dismiss fullscreen!");
	});
	interstitialAd.addEventListener('onDismissScreen', function(){
		Ti.API.info("interstitial Ad fullscreen dismissed!");
	});
	interstitialAd.addEventListener('onLeaveApplication', function(){
		Ti.API.info("interstitial Ad will leave application!");
	});
	interstitialAd.addEventListener('onClick', function(){
		Ti.API.info("interstitial Ad Clicked!");
	});
	
	interstitialAd.load();
	*/
	
	
	function TurnOn(){
		//interstitialAd.show();
		if(view.adPresent == true){
			view.opacity = 1;
		}
	}
	
	function TurnOff(){
		view.opacity = 0;
	}
	
	view.TurnOn = TurnOn;
	view.TurnOff = TurnOff;
	
	return view;
}

module.exports = Create;
