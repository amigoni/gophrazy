exports.loadWin = function(){
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//-(globals.platform.actualHeight),
	  	zIndex: 1000,
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: (view.height-50).toString()+"dp",
		contentWidth: view.width,
		contentHeight: "auto",
		top: "50dp"
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	var musicNSound = CreateMusicNSound();
	
	var notifications = CreateNotifications();
	
	var credits = CreateCredits();
	
	
	container.add(musicNSound);
	if(globals.gameData.notifications.pushNotificationsInitialized == true) container.add(notifications);
	container.add(credits);
	
	scrollView.add(container);
	
	view.add(navBar);
	view.add(scrollView);
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view,null);},1200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		//backgroundColor: "#CD4D39",
		width: globals.platform.width,
		height: '44dp',
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: '44dp',
		height: '44dp',
		right: '0dp',
		buttonID: "creditsWinBackButton",
		touchEnabled: true
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var titleSign = Ti.UI.createImageView({
		image:"/images/settingsSign.png",
		height: 30
	});
	
	
	
	view.add(titleSign);
	view.add(closeButton);
	
	
	return view;
};


function CreateMusicNSound(){
	var SoundController = require("controller/SoundController");
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: " Music & SFX ",
		color: globals.colors.darkBrown,
		textAlign: "left",
		font: globals.fonts.bold25,
		minimumFontSize: 10,
		width: Ti.UI.SIZE
	});
	
	var centerContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		top: 10
	});
	
	var musicButton = CreateButton("Music", globals.gameData.musicOn);
	musicButton.addEventListener("pressed", function(){
		SoundController.ToggleMusic();
	});
	
	var sfxButton = CreateButton("SFX", globals.gameData.sfxOn);
	sfxButton.left = 20;
	sfxButton.addEventListener("pressed", function(){
		SoundController.ToggleSFX();
	});
	
	var vibrateButton = CreateButton("Vibrate", globals.gameData.vibrateOn);
	vibrateButton.left = 20;
	vibrateButton.addEventListener("pressed", function(){
		if(globals.gameData.vibrateOn == true){
			globals.gameData.vibrateOn = false;
		}
		else{
			globals.gameData.vibrateOn = true;
			Ti.Media.vibrate([0, 1000 ]);
		}
		globals.col.gameData.commit();
	});
	
	
	centerContainer.add(musicButton);
	centerContainer.add(sfxButton);
	centerContainer.add(vibrateButton);
	
	
	var restorePurchasesButton = Ti.UI.createView({
		backgroundColor: globals.colors.white,
		borderColor: globals.colors.borderLine2,
		borderWidth: 2,
		borderRadius: 6,
		width: Ti.UI.SIZE,
		height: 40,
		top: 20,
		touchEnabled: true,
		buttonID: "settingsRestorePurchases",
		layout: "horizontal"
	});
	
	restorePurchasesButton.add(Ti.UI.createLabel({
		text: "Restore Purchases",
		color: globals.colors.darkBrown,
		textAlign: "center",
		font: globals.fonts.bold15,
		touchEnabled: false,
		left: "10dp"
	}));
	restorePurchasesButton.add(Ti.UI.createView({width:10}));
	
	restorePurchasesButton.addEventListener('singletap', function(){
		globals.storeController.RestorePurchases();
	});
	
	Common.MakeButton(restorePurchasesButton);
	
	view.add(titleLabel);
	view.add(centerContainer);
	view.add(restorePurchasesButton);
	
	
	return view;
}


function CreateNotifications(){
	var PushNotificationsController = require("/controller/PushNotificationsController");
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "Notifications",
		color: globals.colors.darkBrown,
		textAlign: "left",
		font: globals.fonts.bold25,
		minimumFontSize: 10,
		width: Ti.UI.SIZE,
		top: 30
	});
	
	view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, height: 1, width: 240, top: 30}));
	view.add(titleLabel);
	view.add(Ti.UI.createView({height: 15}));
	
	var notifications = globals.gameData.notifications;
	
	for (var key in notifications) {
	  if(notifications.hasOwnProperty(key)) {
	    if(notifications[key].visible == true)
	    {
	    	view.add(CreateRow(notifications[key]));
	    }
	  }
	}
	
	
	function CreateRow(data){
		var row = Ti.UI.createView({
			width: globals.platform.width,
			height: 50,
			top: 0
		});
		
		var label = Ti.UI.createLabel({
			text: data.name,
			color: globals.colors.darkBrown,
			textAlign: "left",
			verticalAlign: "center",
			font: globals.fonts.bold15,
			minimumFontSize: 10,
			left: 20,
			top: 5,
			width: view.width - 90
		});
		
		var button = CreateButton(null, data.enabled);
		button.right = 20;
		
		button.addEventListener("pressed", function(e){
			data.enabled = !data.enabled;
			globals.col.gameData.commit();
			
			if(data.enabled == true){
				PushNotificationsController.SubscribeToChannel(PushNotificationsController.GetChannelByType(data.type));
			}
			else{
				//SHOULD REALLY have a callback here for when someone is not connected or the call doesn't go through;
				PushNotificationsController.UnsubscribeToChannel(PushNotificationsController.GetChannelByType(data.type));
			}
		});
		
		row.add(label);
		row.add(button);
	
	
		return row;
	}
	
	
	return view;
}


function CreateCredits(){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	var creditsTitle = Ti.UI.createLabel({
		text: "Thank you!",
		font: globals.fonts.bold25,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		top: 30
	});
	
	var creditsLabel = Ti.UI.createLabel({
		text: "Super special thanks to family and friends who have been crucial in testing and advising me during development.\n\nAngela, Cri, Jeff, Tatiana, Paola, Stefano, Antonella, Ginevra, Guendalina, Olivia.\n\nLea, for coming up with the name.\n\nThe great guys at Urustar, for the amazing feedback and suggestions.\n\nRealtime Networks, João Parreira & Marcin Kulwikowski in particular, for the great support and backend platform.\nCheck them out at www.realtime.co\n\nSteve Rice Music, for the great soundtrack.\n\nDaniele Bertinelli for the sounds.",
		font: globals.fonts.regular13,
		width: globals.platform.width - 40,
		textAlign: "left",
		//minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	});
	
	var developerTitle = Ti.UI.createLabel({
		text: "Developement",
		font: globals.fonts.bold25,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		top: 30
	});
	
	var developerLabel = Ti.UI.createLabel({
		text: "Concept, design and development by\nLeonardo Amigoni",
		font: globals.fonts.regular13,
		width: globals.platform.width - 40,
		textAlign: "center",
		//minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	});
	
	
	var mozzarelloLogo = Ti.UI.createImageView({
		image:"/images/mozzarelloLogoWideDarkBrown.png",
		height: 36,
		width: 192,
		top: 25
	});
	
	var siteLabel = Ti.UI.createLabel({
		text: "mozzarello.com",
		font: globals.fonts.bold15,
		width: globals.platform.width - 40,
		textAlign: "center",
		top: 15,
		//minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	});
	
	siteLabel.addEventListener("click", function(){
		Ti.Platform.openURL("http://mozzarello.com");
	});
	
	var facebookPage = Ti.UI.createLabel({
		text: "facebook.com/mozzarelloApps",
		font: globals.fonts.bold15,
		width: globals.platform.width - 40,
		textAlign: "center",
		top: 0,
		//minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	});
	
	facebookPage.addEventListener("click", function(){
		Ti.Platform.openURL("https://www.facebook.com/mozzarelloapps");
	});
	
	var thirdLabel = Ti.UI.createLabel({
		text: "All quotes are for educational purposes only.",
		font: globals.fonts.bold13,
		width: globals.platform.width - 40,
		textAlign: "center",
		top: 60,
		//minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	});
	
	
	view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, height: 1, width: 240, top: 30}));
	view.add(creditsTitle);
	view.add(creditsLabel);
	view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, height: 1, width: 240, top: 30}));
	view.add(developerTitle);
	view.add(developerLabel);
	view.add(mozzarelloLogo);
	view.add(siteLabel);
	view.add(facebookPage);
	view.add(thirdLabel);
	view.add(Ti.UI.createView({height: 50}));
	
	
	return view;
};


function CreateButton(name, isOn){
	var buttonContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		isOn: isOn
	});

	var button = Ti.UI.createView({
		backgroundColor: isOn == true ? globals.colors.wordBoxesColors[2] : globals.colors.wordBoxesColors[0],
		borderColor: globals.colors.borderLine2,
		borderWidth: "2dp",
		borderRadius: "6dp",
		width: "50dp",
		height: "40dp",
		touchEnabled: true,
		buttonID: "settings"+name
	});
	
	button.addEventListener('singletap', function(){
		Toggle();
		buttonContainer.fireEvent("pressed");
	});
	
	Common.MakeButton(button);
	
	var onOffLabel = Ti.UI.createLabel({
		text: isOn == true ? "On" : "Off",
		font: globals.fonts.words1,
		shadowOffset:{x: 1, y: 1},
		shadowColor:"#555555",//"black",
		//left: globals.os == "ios" ? "4dp" : "11dp",
		height: "40dp",
		textAlign: "center",
		color: globals.colors.wordsInBoxesUnselected
	});
	
	button.add(onOffLabel);
	
	var titleLabel = Ti.UI.createLabel({
		text: name,
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15,
		minimumFontSize: "10dp",
		top: "0dp"
	});
	
	
	function Toggle(){
		if(buttonContainer.isOn == true){
			buttonContainer.isOn = false;
			onOffLabel.text = "Off";
			button.backgroundColor = globals.colors.wordBoxesColors[0];
		}
		else{
			buttonContainer.isOn = true;
			onOffLabel.text = "On";
			button.backgroundColor = globals.colors.wordBoxesColors[2];
		}
	}
	
	buttonContainer.add(button);
	buttonContainer.add(titleLabel);
	
	
	return buttonContainer;
}
