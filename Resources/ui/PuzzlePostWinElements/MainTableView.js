exports.createView = function(type, data){
	var GroupsController = require("controller/GroupsController");
	
	var view = Ti.UI.createView({
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
	    top: 0,
	    left: 0
	});
	
	var issuesTableView = CreateTableView(data, CreateHeaderRow);
	
	view.add(issuesTableView);
	
	
	function MoveLeft (){
		Animator.animate(view,{
			duration: 500,
			left: view.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
	}
	
	function MoveRight (){
		Animator.animate(view,{
			duration: 500,
			left: view.left+globals.platform.width,
			easing:Animator['EXP_IN']
		});
	}
	
	function Close(){
		issuesTableView.Close();
		currentLabel = null;
		currentCollectionView = null;
		issuesLabel = null;
		issuesTableView = null;
	}
	
	view.MoveLeft = MoveLeft;
	view.MoveRight = MoveRight;
	view.Close = Close;
	
	///Mark all as not new
	GroupsController.MarkIssuesAsViewed();
	
	
	return view;
};


function CreateTableView(data){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 0,
		separatorColor: "transparent",
		separatorStyle: false,
		dataIn: data
	});
	
	Common.MakeDynamicTableView(tableView, data, 20, exports.CreateGroupRow, CreateBottomRow);
	
	function UpdateTableData(e){
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.rowData && row.rowData.name == e.data.name){
				row.Update(e.data);
			}
		}
	}
	
	function Close(){
		for (var i = 0; i < tableView.data[0].rows.length; i++){
			var row = tableView.data[0].rows[i];
			if(row.Close) row.Close();
		}
	}
	
	tableView.UpdateTableData = UpdateTableData;
	tableView.Close = Close;
	
	
	return tableView;
}


function CreateHeaderRow(name){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 30,
		selectionStyle:  globals.os == "ios" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null,
		className: "puzzlePostHeaderRow"
	});
	
	var label = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: name,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumDarkBrown,
		width: Ti.UI.SIZE
	});
	
	view.add(label);
	
	function Close(){
		label = null;
	}
	
	row.Close = Close;
	
	
	return view;
}


exports.CreateGroupRow = function (rowData){	
	var GroupsController = require("controller/GroupsController");
	var PassController = require("/controller/PassController");
	
	var	view = Ti.UI.createTableViewRow({
		//backgroundColor: "green",
		width: globals.platform.width,
		height: 50,
		selectionStyle: globals.os == "ios" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null,
		rowData:rowData,
		className: "puzzlePostGroupRow"
	});
	
	//Here for Android otherwise the singletap event doesn't register
	var background = Ti.UI.createView({touchEnabled: true, width: view.width, height: view.height, buttonID: "puzzlePostRow"});

	Common.MakeButton(background);
	
	background.addEventListener("touchstart", function(){
		background.opacity = .1;
		background.color = "black";
	});
	
	background.addEventListener("touchend", function(){
		background.opacity = 1;
		background.color = "trasparent";
	});
	
	background.addEventListener("singletap", function(e){
		Ti.API.info("1");
		if (globals.col.phrases.count({groupID: rowData.groupID}) != rowData.phrasesCount){
			Ti.API.info("2");
			if(Titanium.Network.online == true){
				Ti.API.info("3");
				var UpgradeController = require("controller/UpgradeController");
				UpgradeController.GetFile(globals.upgradeBucket+globals.systemData.currenti18n+"/", "group_"+rowData.groupID+".txt", true, function(e1){
					Ti.API.info("4");
					GroupsController.UpgradeCollectionPhrases(JSON.parse(e1),rowData.groupID);
					OpenFromPlay();
				});
			}
			else NotConnectedPopUp();
		}
		else OpenFromPlay();
			
		function NotConnectedPopUp(){
			alert("Upps.\nYou have to be connected to download this issue.");
		}
	});
	
	
	var dateString = new moment(rowData.releaseDate).format("ddd, MMM Do"); //.calendar();
	var label1 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.name,
		font: globals.fonts.bold15,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: globals.platform.width - 130,
		height: 20,
		left: 10,
		top: 10,
		touchEnabled: false
	});
	
	var dateLabel = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: dateString,
		font: globals.fonts.regular11,
		minimumFontSize: 9,
		color: globals.colors.darkBrown,
		width: 70,
		height: 20,
		right: 50,
		top: 10,
		touchEnabled: false
	});
	
	var label2 = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: rowData.phrasesCompleteCount+"/"+rowData.phrasesCount,
		font: globals.fonts.bold12,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		textAlign: "right",
		height: 20,
		width: Ti.UI.SIZE,
		right: 10,
		top: 11,
		touchEnabled: false
	});
	
	var label3 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.description,
		font: globals.fonts.regular9,
		minimumFontSize: 7,
		color: globals.colors.mediumBrown,
		width: globals.platform.width - 80,
		height: 12,
		left: 10,
		top: 27,
		touchEnabled: false
	});
	
	var newSign = Ti.UI.createView({
		backgroundImage: "/images/newSign.png",
		width: 25,
		height: 18,
		top: 0,
		left: 2,
		visible: rowData.isNew
	});
	
	var completeSign = Ti.UI.createView({
		backgroundImage: "/images/completeSign.png",
		width: 47,
		height: 12,
		right: 8,
		top: 27,
		visible: rowData.complete
	});
	
	var playButton = Ti.UI.createImageView({
		image: "/images/playButton.png",
		width: 50,
		//height: 40,
		//top: 13,
		right: 7,
		bottom: 2,
		visible: rowData.bought,
		bubbleParent: false,
		buttonID: "collectionsBookRowPlayButton",
		touchEnabled: false
	});
	
	Common.MakeButton(playButton);
	
	playButton.addEventListener("singletap", function(){
		OpenFromPlay();
	});
	
	view.add(background);	
	view.add(label1);
	view.add(dateLabel);
	view.add(label2);
	view.add(label3);
	view.add(newSign);
	//view.add(playButton);
	view.add(completeSign);
	
	
	function OpenFromPlay(){
		Ti.API.info("5");
		setTimeout(function(){
			Ti.API.info("6");
			if(PassController.CheckForAccess() == true){
				Ti.API.info("7");
				OpenIt();
			}
			else {
				if(Titanium.Network.online == true) globals.rootWin.OpenWindow("passWin", {callback: OpenIt});	
				else alert("There is no internet connection.\nYou need a connection to unlock the next set of puzzles.");
			}	
		},200);
		
		function OpenIt(){
			Ti.API.info("8");
			Common.CreateButtonBlocker(true);
			globals.gameData.lastGroupID = rowData.groupID;
			globals.col.gameData.commit();
			view.fireEvent("updateCurrentRow", {rowData:rowData});
			
			var GroupsController = require("controller/GroupsController");
			var group = GroupsController.CreateGroup("collection","", rowData.$id);
			globals.rootWin.OpenWindow("gameWin", group);
		}
	}

	
	function Update(e){
		var newRowData = e.data;
		if(newRowData.groupID == rowData.groupID){
			rowData = newRowData;
			label1.text = rowData.name;
			dateLabel.text = dateString;
			label3.text = rowData.description;
			label2.text = rowData.phrasesCompleteCount+"/"+rowData.phrasesCount;
			
			if (rowData.isNew == false) newSign.visible = false;
			else newSign.visible = true;
			
			if (rowData.complete == true){
				playButton.visible = false;
				completeSign.visible = true;
			}
			else{
				playButton.visible = true;
				completeSign.visible = false;
			}
		}
	}
	
	function Change(newRowData){
		rowData = newRowData;
		Update({data: rowData});
	}
	
	function Close(){
		Ti.App.removeEventListener("updateCollection", Update);
		
		label1 = null;
		label2 = null;
		label3 = null;
		newSign = null;
		playButton = null;
		completeSign = null;
	}
	
	view.Update = Update;
	view.Change = Change;
	view.Close = Close;
	
	Ti.App.addEventListener("updateCollection", Update);
	
	Update({data: rowData});
	
	
	return view;
};


function CreateBottomRow(nextRowToLoad, totalRows){
	var text;
	if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
	else text = "("+nextRowToLoad+"/"+totalRows+")";
	
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		selectionStyle: globals.os == "ios" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null,
		height: 55,
		selectionStyle: false,
		className: "puzzlePostBottomRow"
	});
	
	view.addEventListener("singletap", function(e){
		view.fireEvent("more");
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var bg = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.bold20,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE
	});
	
	view.add(label);
	view.add(bg);
	
	function Update(nextRowToLoad, totalRows){
		if(nextRowToLoad < totalRows) text = "more...   ("+nextRowToLoad+"/"+totalRows+")";
		else text = "("+nextRowToLoad+"/"+totalRows+")";
		label.text = text;
	}
	
	function Close(){
		bg = null;
		label = null;
	}
	
	view.Update = Update;
	view.Close = Close;
	
	
	return view;
};