var RandomPlus = require("/lib/RandomPlus");

var GetGroboldFont = function(){
	var name = "Groboldov";
	if(Ti.Platform.name == "android") name = "Groboldov7.2Pro";
	return name;
};

var GetHelveticaNeue = function(){
	var name = "Helvetica Neue";
	if(Ti.Platform.name == "android") name = "HelveticaNeue";
	return name;
};


globals.themes = {
	dawn:{name: "dawn", menuBg:"/images/mainMenuBgDawn.png", worldImage:"/images/mainMenuWorldDawn.png", worldColor: "#578392"},
	day:{name: "day", menuBg:"/images/mainMenuBgDay.png", worldImage:"/images/mainMenuWorldDay.png", worldColor: "#D9A300"},
	dusk:{name: "dusk", menuBg:"/images/mainMenuBgDusk.png", worldImage:"/images/mainMenuWorldDusk.png", worldColor: "#CD7423"},
	night:{name: "night", menuBg:"/images/mainMenuBgNight.png", worldImage:"/images/mainMenuWorldNight.png", worldColor: "#006F96"},
	snowyDay:{name: "snowyDay", menuBg:"/images/mainMenuBgSnowyDay.png", worldImage:"/images/mainMenuWorldSnowyDay.png", worldColor: "#E0DED3"},
};


//cartoon
globals.fonts = {
	words13: {fontSize: "13dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	words1: {fontSize: "17dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	words2: {fontSize: "17dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	words2Double: {fontSize: "34dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	words3: {fontSize: "23dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	regular1: {fontSize: "17dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	regular1Double: {fontSize: "34dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	regular2: {fontSize: "13dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	regular7r: {fontSize: "7dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	regular9: {fontSize: "9dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regular9r: {fontSize: "9dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	regular10r: {fontSize: "10dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	regular11: {fontSize: "11dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regular12: {fontSize: "12dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regular12r: {fontSize: "12dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	regular13r: {fontSize: "13dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	regular13: {fontSize: "13dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regular14: {fontSize: "14dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regular15: {fontSize: "15dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regular20: {fontSize: "20dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regular30: {fontSize: "30dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regular40: {fontSize: "40dp", fontFamily: GetHelveticaNeue(),fontWeight:"bold"},
	regularSmall: {fontSize: "10dp", fontFamily: GetHelveticaNeue(),fontWeight:"regular"},
	boldXS8: {fontSize: "8dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	boldXS: {fontSize: "9dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	boldSmall: {fontSize: "12dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	boldMediumSmall: {fontSize: "15dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold8: {fontSize: "8dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold10: {fontSize: "10dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold11: {fontSize: "11dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold12: {fontSize: "12dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold13: {fontSize: "13dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold14: {fontSize: "13dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold15: {fontSize: "15dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold20: {fontSize: "20dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold25: {fontSize: "25dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold30: {fontSize: "30dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	bold35: {fontSize: "35dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	boldMedium31: {fontSize: "31dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	boldMedium: {fontSize: "34dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	boldXL: {fontSize: "45dp", fontFamily:GetGroboldFont(),fontWeight:"regular"},
	boldXL50: {fontSize: "50dp", fontFamily:GetGroboldFont(),fontWeight:"regular"}
};

globals.colors = {
	theme: "light",
	mainMenuBackground: "#FFBF00",
	background: "#F3DEBC",
	borderLine: "transparent", //"#777777"
	borderLine2: "#453D32",
	text: "#453D32",
	textOpposite:"#444444",
	textMenus: "#FFFFFF",
	buttonTextMenus: "#453D32",
	buttonGameToolbarBGColor: "transparent",
	wordBoxes: "#333333",
	wordsInBoxesSelected: "#888888",
	wordsInBoxesUnselected: "#FFFFFF",
	warning: "#FFBF00",
	hint: "#B20000",
	blue: "#5F8291",
	green: "#228C00",
	red: "#CD4E3A",
	darkBrown: "#3F3826",
	mediumBrown: "#A59A7F",
	mediumDarkBrown: "#9F9279",
	white:"#FFF8E6",
	playBackground: "#FECF47",
	tipFont: "#AC7D0C",
	black:"#333333"
};

globals.style = {};
globals.style.animationSpeeds ={};
globals.style.animationSpeeds.speed1 = 200;
globals.style.animationSpeeds.speed2 = 250;
globals.style.animationSpeeds.speed3 = 500;

/*
globals.colors = {
	theme: "light",
	background: "#000000",
	borderLine: "transparent", //"#777777"
	borderLine2: "FFFFFF",
	text: "#FFFFFF",
	textMenus: "#999999",
	buttonTextMenus: "#CCCCCC",
	buttonGameToolbarBGColor: "transparent",
	wordBoxes: "#333333",
	wordsInBoxesSelected: "#888888",
	wordsInBoxesUnselected: "#FFFFFF",
	warning: "#FFBF00",
	green: "#228C00",
	red: "#CD4E3A",
	wordBoxesColors: [
		"#CD4E3A", //Orange-Red 
		"#00B285", //Turquoise
		"#497876", //Blue/Green Pistachio
		"#EEB200" //Yellow
	]
};
*/

/*
globals.colors = {
	theme: "dark",
	background: "#EEEEEE",
	borderLine: "#444444",
	text: "#444444",
	textMenus: "#555555",
	buttonTextMenus: "#444444",
	buttonGameToolbarBGColor: "#FFFFFF",
	wordBoxes: "#333333",
	wordsInBoxesSelected: "#888888",
	wordsInBoxesUnselected: "#FFFFFF",
	warning: "#FFBF00",
	green: "#228C00",
	red: "#CD4E3A",
	wordBoxesColors: [
		"#CD4E3A", //Orange-Red 
		"#00B285", //Turquoise
		"#497876", //Blue/Green Pistachio
		"#EEB200" //Yellow
	]
};
*/

/*
globals.colors.wordBoxesColors = [
	"#D96D00", //Orange-Red //#497876
	"#00B285", //Turquoise
	"#85B200", //Green Pistachio
	"#FFBF00" //Yellow
];
*/

exports.SetColorPalette = function (palletteName){
	var currentWordBoxesPalette = globals.col.colors.find({name: palletteName})[0];
	var colors = [
		currentWordBoxesPalette.color0,
		currentWordBoxesPalette.color1,
		currentWordBoxesPalette.color2,
		currentWordBoxesPalette.color3
	];
	
	globals.colors.wordBoxesColors = colors;
};


exports.InitColors = function(){
	var colors = JSON.parse(GetJsonText("model/ColorsData.txt"));
	
	for (var i =0; i < colors.length; i++){
		globals.col.colors.save({
			name: colors[i].name,
			color0: colors[i].color0,
			color1: colors[i].color1,
			color2: colors[i].color2,
			color3: colors[i].color3,
		});
	}
	
	globals.col.colors.commit();
};


function GetJsonText (fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
 
	if (readFile.exists()) {
	     readContents = readFile.read();
	     Ti.API.info('File Exists');  
	}
	 
	var doc = readContents.text;
	return doc;
};

