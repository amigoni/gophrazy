exports.loadWin = function(){
	var featuresData = globals.gameData.abilities;
	
	//Need to sort them by unlockCount;
	var featuresArray = [];
	for (var key in featuresData) {
	  if (featuresData.hasOwnProperty(key)) featuresArray.push(featuresData[key]);
	}
	
	function sortByUnlockCount(ob1,ob2) {
    	var n1 = ob1.unlockCount;
    	var n2 = ob2.unlockCount;
    	if (n1 > n2) {return 1;}
		else if (n1 < n2){return -1;}
	    else { return 0;}//nothing to split
	};
	
	featuresArray.sort(sortByUnlockCount);
	
	
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//-(globals.platform.actualHeight),
	  	zIndex: 99,
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: view.height-50,
		contentWidth: view.width,
		contentHeight: "auto",
		top: 50
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	for (var i = 0; i < featuresArray.length; i++){
	    var data = JSON.parse(JSON.stringify(featuresArray[i])); //Clone to avoid pollution;
		if(data.unlockable == true){
			if(globals.gameData.phrasesCompleteCountForPass >= data.unlockCount) data.enabled = true;
			if(globals.gameData.phrasesCompleteCountForPass == data.unlockCount) data.current = true;
			var row = CreateRow(data);
			container.add(row);
		}
	}
	
	scrollView.add(container);
	
	view.add(navBar);
	view.add(scrollView);
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){
			Common.WindowOpenAnimation(view, null);
			var SoundController = require("/controller/SoundController");
			SoundController.playSound("unlockFeature");
		},1200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view, function(){
			view.fireEvent("closed");
			globals.rootWin.popUpStack.Next();
		});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		//backgroundColor: "#CD4D39",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID: "mailBoxBackButton",
		touchEnabled: true
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var titleSign = Ti.UI.createImageView({
		image: "/images/featuresSign.png",
		width: 107,
		height: 25
	});
	
	view.add(titleSign);
	view.add(closeButton);
	
	
	return view;
};


function CreateRow(data){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: "60dp",
		borderWidth: data.current == true ? "2dp" : "0dp",
		borderColor: globals.colors.red
	});
	
	view.addEventListener("singletap", function(){
		if(data.name == "composePuzzle"){
			if(globals.gameData.userFacebookID == ""){
				globals.rootWin.OpenWindow("whyConnectWin");
			}
			else{
				if(data.enabled == false){
					if(Titanium.Network.online == true) {
						if(globals.storeController.products.length > 0) globals.rootWin.OpenWindow("smallSquarePopUp", "compose_puzzle_early_access");
						else {
							globals.storeController.Init(function(){
								globals.rootWin.OpenWindow("smallSquarePopUp", "compose_puzzle_early_access");
							});
						}
					}	
					else alert("There is no internet connection.\nYou need a connection to buy.");
				}
			}
		}
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/"+data.icon,
		width: 40,
		left: 15
	});
	
	var label1 = Ti.UI.createLabel({
		text: data.title,
		font: globals.fonts.bold20,
		minimumFontSize: "12dp",
		color: globals.colors.darkBrown,
		left: "70dp",
		width: "170dp",
		top: "5dp"
	});
	
	var label2 = Ti.UI.createLabel({
		text: data.description,
		font: globals.fonts.regular11,
		//minimumFontSize: 12,
		color: globals.colors.darkBrown,
		left: "70dp",
		width: "170dp",
		top: "25dp"
	});
	
	var rightContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		right: "10dp"
	});
	
	var lockContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		right: "0dp"
	});
	
	var numberLabel = Ti.UI.createLabel({
		text: data.unlockCount,
		font: globals.fonts.bold20,
		top: "10dp",
		color: globals.colors.darkBrown
	});
	
	var lockIcon = Ti.UI.createImageView({
		image: "/images/lockIcon.png",
		width: 23,
		height: 29,
		left: 5
	});
	
	lockContainer.add(numberLabel);
	lockContainer.add(lockIcon);
	
	var checkMark = Ti.UI.createImageView({
		image: "/images/checkMarkRed.png",
		width: 32,
		height: 27
	});
	
	var buyNowLabel = Ti.UI.createLabel({
		text: "Buy Now",
		font: globals.fonts.bold15,
		//minimumFontSize: 12,
		color: globals.colors.darkBrown,
		top: "3dp"
	});
	
	if(data.enabled == false){
		rightContainer.add(lockContainer);
		if(data.name == "composePuzzle") rightContainer.add(buyNowLabel);
	}
	else{
		rightContainer.add(checkMark);
	}
	
	view.add(icon);
	view.add(label1);
	view.add(label2);
	view.add(rightContainer);
	
	
	return view;
}
