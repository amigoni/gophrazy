exports.CreatePopUp = function(type){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		bubbleParent: false,
		zIndex: 100,
		top: "0dp"
	});
	
	var background = Ti.UI.createView({
		backgroundColor: "black",
		width: view.width,
		height: view.height,
		opacity: 0
	});
	
	var container = Ti.UI.createView({
		width: "200dp",
		height: "200dp",
		top: globals.platform.actualHeight
	});
	
	var popUpBg = Ti.UI.createView({
		//backgroundImage:'"+globals.imagesPath+"/images/popUpBg.png',
		borderColor: "black",
		backgroundColor: globals.colors.white,
		borderWidth: "2dp",
		borderRadius: "20dp",
		width: "180dp",
		height: "180dp"
	});
	
	var insideView;
	if(type == "outOfEnergy") insideView = CreateOutOfEnergy();
	else if (type == "skipIt") insideView = CreateSkip();
	else if (type == "buySkips") insideView = CreateBuySkips();
	else if (type == "buyResult") insideView = CreateBuyResult("error");
	else if (type == "buyConfirmation") insideView = CreateBuyConfirmation();
	else if (type == "hints") insideView = CreateBuyHints();
	else if (type == "skipboxFull") insideView = CreateskipboxFull();
	else if (type == "fortuneCookieReward") insideView = CreateFortuneCookieReward();
	else if (type == "compose_puzzle_early_access") insideView = CreateComposePuzzleEarlyAccess();
	else if (type == "one_hint") insideView = CreateItemReward("hints");
	else if (type == "one_skip") insideView = CreateItemReward("skips");
	else if (type == "settings") insideView = CreateSettings();
	
	insideView.addEventListener("close", function(){
		Close();
	});
	
	insideView.addEventListener("skip", function(){
		view.fireEvent("skip");
	});
	
	popUpBg.add(insideView);
	
	var closeX = Ti.UI.createView({
		backgroundImage:'/images/closeX.png',
		width: "28dp",
		height: "29dp",
		top: "0dp",
		right: "0dp"
	});
	Common.MakeButton(closeX);
	
	closeX.addEventListener("singletap", function(){
		Close();
	});
	
	container.add(popUpBg);
	container.add(closeX);
	
	
	view.add(background);
	view.add(container);
	
	
	function Close(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			easing: Animator["EXP_IN"]
		});
		
		Animator.animate(background,{
				duration: 300,
				opacity: 0,
			},
			function(){
				if (insideView.Close) insideView.Close();
				view.fireEvent("closed");
			}
		);
		*/
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
		
		background.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			opacity: 0
			}), 
			function(){
				if (insideView.Close) insideView.Close();
				view.fireEvent("closed");
			}
		);
	}
	
	setTimeout(function(){
		/*
		Animator.animate(container,{
			duration: 500,
			top: ((globals.platform.actualHeightNumber-container.size.height)/2).toString()+"dp",
			easing: Animator["EXP_OUT"]
		});
		
		Animator.animate(background,{
			duration: 500,
			opacity: 0.75,
		});
		*/
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top:((globals.platform.actualHeightNumber-container.size.height)/2).toString()+"dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
		background.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity: 0.75
		}));
	},200);
	
	
	return view;
};


function CreateSkip(){
	var view = Ti.UI.createView({
		width: "180dp",
		height: "180dp",
		bubbleParent: false
	});
	
	var skipItSign = Ti.UI.createView({
		backgroundImage:"/images/skipItSign.png",
		width: "137dp",
		height: "44dp",
		top: "60dp"
	});
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: "44dp",
		height: "29dp",
		bottom: "10dp",
		buttonID: "smallSquarePopUpOkButton",
		bubbleParent: true
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		view.fireEvent("skip");
		view.fireEvent("close");
	});
	
	
	view.add(skipItSign);
	view.add(okButton);
	
	
	return view;
}


function CreateBuySkips(){
	var skipData = globals.col.storeItems.find({itemID:"skip_1"})[0];
	var product = globals.storeController.FindProductBySKU("skip_1", true);
	
	var view = Ti.UI.createView({
		width: "180dp",
		height: "180dp"
	});
	
	var buySign = Ti.UI.createImageView({
		image:"/images/buyButton.png",
		width: 49,
		height: 29,
		top: 10
	});
	
	var centerContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		top: "40dp"
	});
	
	centerContainer.add(Ti.UI.createLabel({
		text:  skipData.quantity+" x",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold35,
		top: "10dp"
	}));
	
	centerContainer.add(Ti.UI.createImageView({
		image:"/images/skipButtonDark.png",
		left: "10dp",
		top: "22dp",
		width: "30dp",
		height: "30dp"
	}));
	
	var moneyLabel = Ti.UI.createLabel({
		text: product.priceAsString,
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15,
		bottom: "45dp"
	});
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: "44dp",
		height: "29dp",
		bottom: "10dp",
		buttonID: "smallSquarePopUpOkButton"
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		globals.storeController.BuyStoreKitItem("skip_1", 1);
		view.fireEvent("close");
	});
	
	
	view.add(buySign);
	view.add(centerContainer);
	view.add(moneyLabel);
	view.add(okButton);
	
	
	return view;
}


function CreateskipboxFull(){
	var itemData = globals.col.storeItems.find({itemID:"skipbox_upgrade_1"})[0];
	var GroupsController = require("controller/GroupsController");
	var Bubble = require("ui/Common/Bubble");
	var skipboxGroup = GroupsController.GetskipboxGroup();
	var product = globals.storeController.FindProductBySKU("skipbox_upgrade_1", true);

	
	var view = Ti.UI.createView({
		width: 180,
		height: 180
	});
	
	/*
	var titleSign = Ti.UI.createImageView({
		image: globals.gameData.skipboxLimit == 9 ? "/images/skipItSign.png" : "/images/buyButton.png",
		top: 5
	});
	*/
	var titleSign = Ti.UI.createLabel({
		text: "Full",
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: globals.fonts.bold25,
		top: 5
	});
	
	var iconContainer = Ti.UI.createView({
		width: 55,
		height: 60,
		//bottom: -300,
		//opacity: 0
		top: globals.gameData.skipboxLimit == globals.gameData.skipboxUpgradeLimit ? "32dp": "28dp"
	});
	
	var icon = Ti.UI.createView({
		backgroundImage: "/images/skipBoxIconLarge.png",
		width: 50,
		height: 44,
		bottom: 2,
		touchEnabled: false
	});
	var bubble = Bubble.createView(skipboxGroup.phrasesIDs.length+"/"+skipboxGroup.phrasesLimit);
	
	iconContainer.add(icon);
	iconContainer.add(bubble);
	
	var label1 = Ti.UI.createLabel({
		text: globals.gameData.skipboxLimit == globals.gameData.skipboxUpgradeLimit ? "Go solve puzzles from the skipbox.": "Upgrade to "+globals.gameData.skipboxUpgradeLimit+"spots\n + 3 Skips?",
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: globals.fonts.bold15,
		//minimumFontSize: 8,
		width: view.width - 10,
		bottom: globals.gameData.skipboxLimit == globals.gameData.skipboxUpgradeLimit ? 42 : 56//58
	});
	
	var moneyLabel = Ti.UI.createLabel({
		text: product.priceAsString,
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: globals.fonts.bold10,
		bottom: "42dp"
	});
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: "44dp",
		height: "29dp",
		bottom: "10dp",
		buttonID: "smallSquarePopUpOkButton"
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		if(globals.gameData.skipboxLimit < globals.gameData.skipboxUpgradeLimit ){
			globals.storeController.BuyStoreKitItem("skipbox_upgrade_1", 1);
		}
		view.fireEvent("close");
	});
	
	
	view.add(titleSign);
	view.add(iconContainer);
	view.add(label1);
	if(globals.gameData.skipboxLimit < globals.gameData.skipboxUpgradeLimit ) view.add(moneyLabel);
	view.add(okButton);
	
	
	return view;
}


function CreateFortuneCookieReward(){	
	var data = globals.gameData.fortuneCookieData;
	
	var rightLabelText;
	if( data.current.name == "hints") rightLabelText = data.current.count+"/"+data.data.hints.max;
	else rightLabelText = data.current.count+"/"+data.data.skips.max;
	
	
	var view = Ti.UI.createView({
		width: "180dp",
		height: "180dp"
	});
	
	var rewardSign = Ti.UI.createImageView({
		image: "/images/rewardSign.png",
		width: 95,
		height: 26,
		top: "10dp"
	});
	
	var centerContainer = Ti.UI.createView({
		//backgroundColor: "red",
		width: "120dp",
		height: "65dp",
		//layout: "horizontal",
		top: "55dp"
	});
	
	
	var leftContainer = Ti.UI.createView({
		width: "50dp",
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: "0dp",
		left: "0dp"
	});
	
	var leftLabel = Ti.UI.createLabel({
		text: data.current.name == "hints" ? "Hint" : "Skip",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15
	}); 
	
	var leftIcon = Ti.UI.createImageView({
		image: data.current.name == "hints" ? "/images/hintIconLarge.png" : "/images/skipButtonBrown.png",
		height: "32dp",
		top: "0dp"
	});
	
	leftContainer.add(leftLabel);
	leftContainer.add(leftIcon);
	
	
	var rightContainer = Ti.UI.createView({
		width: "50dp",
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: "0dp",
		left: "70dp"
	});
	
	var rightLabel = Ti.UI.createLabel({
		text: rightLabelText,
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15
	}); 
	
	var fortuneMeterView = CreateFortuneMeterView(data.current.count == 0 ? 3 : data.current.count);
	
	rightContainer.add(rightLabel);
	rightContainer.add(fortuneMeterView);
	
	centerContainer.add(leftContainer);
	centerContainer.add(rightContainer);
	
	var bottomLabel = Ti.UI.createLabel({
		text: "See you tomorrow :)",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.regular11,
		bottom: "50dp"
	}); 
	
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: "44dp",
		height: "29dp",
		bottom: "10dp",
		buttonID: "fortuneCookieRewardWinOkButton"
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		view.fireEvent("close");
	});
	
	
	view.add(rewardSign);
	view.add(centerContainer);
	view.add(bottomLabel);
	view.add(okButton);
	
	
	return view;
}


function CreateItemReward(itemName){	
	var data = globals.gameData.fortuneCookieData;	
	
	var view = Ti.UI.createView({
		width: "180dp",
		height: "180dp"
	});
	
	var rewardSign = Ti.UI.createImageView({
		image: "/images/rewardSign.png",
		top: "10dp"
	});
	
	var centerContainer = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: "55dp"
	});
	
	var topLabel = Ti.UI.createLabel({
		text: itemName == "hints" ? "Hint" : "Skip",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15
	}); 
	
	var bottomContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		top: "0dp",
		//left: "0dp"
	});
	
	var leftIcon = Ti.UI.createImageView({
		image: itemName == "hints" ? "/images/hintIconLarge.png" : "/images/skipButtonBrown.png",
		height: "32dp",
		//top: "0dp"
	});
	
	var rightLabel = Ti.UI.createLabel({
		text:"+1",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold25
	}); 
	
	
	bottomContainer.add(leftIcon);
	bottomContainer.add(rightLabel);
	
	centerContainer.add(topLabel);
	centerContainer.add(bottomContainer);
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: "44dp",
		height: "29dp",
		bottom: "10dp",
		buttonID: "fortuneCookieRewardWinOkButton"
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		view.fireEvent("close");
	});
	
	
	view.add(rewardSign);
	view.add(centerContainer);
	view.add(okButton);
	
	
	return view;
}


function CreateBuyHints(){
	var hintData = globals.col.storeItems.find({itemID:"hint_1"})[0];
	var product = globals.storeController.FindProductBySKU("hint_1", true);
	
	var view = Ti.UI.createView({
		width: "180dp",
		height: "180dp"
	});
	
	var buySign = Ti.UI.createImageView({
		image:"/images/buyButton.png",
		width: 49,
		height: 29,
		top: 10
	});
	
	var centerContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		top: "40dp"
	});
	
	centerContainer.add(Ti.UI.createLabel({
		text:  hintData.quantity+" x",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold35,
		top: "10dp"
	}));
	
	centerContainer.add(Ti.UI.createImageView({
		image:"/images/hintIconLarge.png",
		height: "50dp",
		left: "5dp"
	}));
	
	var moneyLabel = Ti.UI.createLabel({
		text: product.priceAsString,
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: globals.fonts.bold15,
		bottom: "45dp"
	});
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: "44dp",
		height: "29dp",
		bottom: "10dp",
		buttonID: "smallSquarePopUpOkButton"
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		globals.storeController.BuyStoreKitItem("hint_1", 1);
		view.fireEvent("close");
	});
	
	
	view.add(buySign);
	view.add(centerContainer);
	view.add(moneyLabel);
	view.add(okButton);
	
	
	return view;
}


function CreateComposePuzzleEarlyAccess(){
	var skipData = globals.col.storeItems.find({itemID:"compose_puzzle_early_access"})[0];
	var product = globals.storeController.FindProductBySKU("compose_puzzle_early_access", true);
	var view = Ti.UI.createView({
		width: "180dp",
		height: "180dp"
	});
	
	var buySign = Ti.UI.createImageView({
		image:"/images/buyButton.png",
		width: 49,
		height: 29,
		top: 10
	});
	
	var centerContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: "50dp"
	});
	
	centerContainer.add(Ti.UI.createImageView({
		image:"/images/composeIcon.png",
		//left: "10dp",
		//top: "22dp",
		width: 50,
		height: 20
	}));
	
	centerContainer.add(Ti.UI.createLabel({
		text:  "Write Your Own\nUnlock at "+globals.gameData.abilities.composePuzzle.unlockCount+" or",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15,
		top: "5dp"
	}));
	
	var moneyLabel = Ti.UI.createLabel({
		text: product.priceAsString,
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15,
		bottom: "40dp"
	});
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: "44dp",
		height: "29dp",
		bottom: "10dp",
		buttonID: "smallSquarePopUpOkButton"
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		globals.storeController.BuyStoreKitItem("compose_puzzle_early_access", 1);
		view.fireEvent("close");
	});
	
	
	view.add(buySign);
	view.add(centerContainer);
	view.add(moneyLabel);
	view.add(okButton);
	
	
	return view;
}


function CreateBuyResult(resultType){
	var view = Ti.UI.createView({
		width: 180,
		height: 180
	});
	
	var resultSign = Ti.UI.createImageView({
		image: resultType == "success" ? "/images/successSign.png" : "/images/ohNoSign.png",
		height: 39,
		top: 60
	});
	
	var label1 = Ti.UI.createLabel({
		text: "Please try again",
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: globals.fonts.boldMediumSmall,
		top:100
	});
	
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: 44,
		height: 29,
		bottom: 10
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		view.fireEvent("close");
		alert("ok");
	});
	
	
	view.add(resultSign);
	if(resultType == "error") view.add(label1);
	view.add(okButton);
	
	
	return view;
}


function CreateBuyConfirmation(){
	var view = Ti.UI.createView({
		width: 180,
		height: 180
	});
	
	var buyItSign = Ti.UI.createView({
		backgroundImage: "/images/buyItSign.png",
		width: 135,
		height: 45,
		top: 50
	});
	
	var yesButton = Ti.UI.createView({
		backgroundImage: "/images/yesButton.png",
		width: 52,
		height: 28,
		bottom: 15,
		left: 25
	});
	
	Common.MakeButton(yesButton);
	
	yesButton.addEventListener("singletap", function(){
		view.fireEvent("yes");
	});
	
	var noButton = Ti.UI.createView({
		backgroundImage: "/images/noButton.png",
		width: 41,
		height: 28,
		bottom: 15,
		right: 30
	});
	
	Common.MakeButton(noButton);
	
	noButton.addEventListener("singletap", function(){
		view.fireEvent("close");
	});
	
	
	view.add(buyItSign);
	view.add(yesButton);
	view.add(noButton);
	
	
	return view;
}


function CreateFortuneMeterView(quantity){
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: "0dp",
		bubbleParent: false
	});
	
	var animationCompleteCounter = 0;
	view.addEventListener("animationComplete", function(e){
		animationCompleteCounter++;
		if (animationCompleteCounter >= quantity){view.fireEvent("meterAnimationComplete");};
	});
	
	var topContainer = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal"
	});
	
	var barContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: "44dp",
		layout: "horizontal",
		//left: 0,
		//bottom: 10
	});
	
	var barConfig = [
		//{height: 20, color: "#1ABBB4"},
		//{height: 22, color: "#00B200"},
		//{height: 24, color: "#3BB878"},
		//{height: 26, color: "#7CC576"},
		{height: "28dp", color: "#ACD372"},
		{height: "30dp", color: "#FFF467"},
		{height: "32dp", color: "#FBAF5C"}
		//{height: 34, color: "#F68E55"},
		//{height: 36, color: "#F26C4F"},
		//{height: 38, color: "red"},
	];
	
	for (var i = 0; i < barConfig.length; i++){
		var isEmpty = false;
		if (i+1 > quantity) isEmpty = true;
		barContainer.add(CreateBar(barConfig[i].height, "#FFBF00", isEmpty,i));
	}

	topContainer.add(barContainer);
	
	view.add(topContainer);
	
	
	function CreateBar (height, color, isEmpty,i){
		var bar = Ti.UI.createView({
			borderColor: globals.colors.black,
			borderWidth: "2dp",
			borderRadius: "6dp",
			backgroundColor: "transparent" ,
			width: "12dp",
			height: height,
			left: "1dp",
			bottom: "0dp",
			transform: isEmpty == false ? Ti.UI.create2DMatrix().scale(0.8,0.8) : null
		});
		
		if(isEmpty == false){
			setTimeout(function(){
				bar.transform = Ti.UI.create2DMatrix().scale(0.8,0.8);
				bar.backgroundColor = color;
				/*
				Animator.animate(bar,{
					duration: globals.style.animationSpeeds.speed1,
					transform: Ti.UI.create2DMatrix().scale(1,1),
					opacity: 1,
					easing:Animator['BOUNCE_OUT']
				},function(){
					bar.fireEvent("animationComplete");
					}
				);
				*/
				bar.animate(Ti.UI.createAnimation({
					duration: globals.style.animationSpeeds.speed1,
					transform: Ti.UI.create2DMatrix().scale(1,1),
					opacity: 1,
					curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
				}),function(){bar.fireEvent("animationComplete");});
				
			}, 1000+i*350);
		}
		
		
		return bar;
	}
	
	return view;
};


function CreateSettings(){
	var SoundController = require("controller/SoundController");
	var view = Ti.UI.createView({
		width: "180dp",
		height: "180dp"
	});
	
	var sign = Ti.UI.createImageView({
		image:"/images/settingsSign.png",
		height: "22dp",
		top: "7dp"
	});
	
	var centerContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		top: "35dp"
	});
	
	var musicButton = CreateButton("Music", globals.gameData.musicOn);
	musicButton.addEventListener("pressed", function(){
		SoundController.ToggleMusic();
	});
	
	var sfxButton = CreateButton("SFX", globals.gameData.sfxOn);
	sfxButton.left = "5dp";
	sfxButton.addEventListener("pressed", function(){
		SoundController.ToggleSFX();
	});
	
	var vibrateButton = CreateButton("Vibrate", globals.gameData.vibrateOn);
	vibrateButton.left = "5dp";
	vibrateButton.addEventListener("pressed", function(){
		if(globals.gameData.vibrateOn == true){
			globals.gameData.vibrateOn = false;
		}
		else{
			globals.gameData.vibrateOn = true;
			Ti.Media.vibrate([0, 1000 ]);
		}
		globals.col.gameData.commit();
	});
	
	centerContainer.add(musicButton);
	centerContainer.add(sfxButton);
	centerContainer.add(vibrateButton);
	
	
	var restorePurchasesButton = Ti.UI.createView({
		backgroundColor: globals.colors.white,
		borderColor: globals.colors.borderLine2,
		borderWidth: "2dp",
		borderRadius: "6dp",
		width: Ti.UI.SIZE,
		height: "36dp",
		bottom: "45dp",
		touchEnabled: true,
		buttonID: "settingsRestorePurchases",
		layout: "horizontal"
	});
	
	restorePurchasesButton.add(Ti.UI.createLabel({
		text: "Restore Purchases",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15,
		touchEnabled: false,
		left: "10dp"
	}));
	restorePurchasesButton.add(Ti.UI.createView({width:"10dp"}));
	
	restorePurchasesButton.addEventListener('singletap', function(){
		globals.storeController.RestorePurchases();
	});
	
	Common.MakeButton(restorePurchasesButton);
	
	
	var creditsButton = Ti.UI.createView({
		backgroundColor: globals.colors.white,
		borderColor: globals.colors.borderLine2,
		borderWidth: "2dp",
		borderRadius: "6dp",
		width: Ti.UI.SIZE,
		height: "36dp",
		bottom: "5dp",
		touchEnabled: true,
		buttonID: "settingsCreditsButton",
		layout: "horizontal"
	});
	
	creditsButton.add(Ti.UI.createLabel({
		text: "Credits",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15,
		touchEnabled: false,
		left: "10dp"
	}));
	creditsButton.add(Ti.UI.createView({width:"10dp"}));
	
	Common.MakeButton(creditsButton);
	
	creditsButton.addEventListener("singletap", function(){
		globals.rootWin.OpenWindow("creditsWin");
	});
	
	
	view.add(sign);
	view.add(centerContainer);
	view.add(restorePurchasesButton);
	view.add(creditsButton);
	
	function CreateButton(name, isOn){
		var buttonContainer = Ti.UI.createView({
			width: Ti.UI.SIZE,
			height: Ti.UI.SIZE,
			layout: "vertical",
			isOn: isOn
		});

		var button = Ti.UI.createView({
			backgroundColor: isOn == true ? globals.colors.wordBoxesColors[2] : globals.colors.wordBoxesColors[0],
			borderColor: globals.colors.borderLine2,
			borderWidth: "2dp",
			borderRadius: "6dp",
			width: "50dp",
			height: "40dp",
			touchEnabled: true,
			buttonID: "settings"+name
		});
		
		button.addEventListener('singletap', function(){
			Toggle();
			buttonContainer.fireEvent("pressed");
		});
		
		Common.MakeButton(button);
		
		var onOffLabel = Ti.UI.createLabel({
			text: isOn == true ? "On" : "Off",
			font: globals.fonts.words1,
			shadowOffset:{x: 1, y: 1},
			shadowColor:"#555555",//"black",
			//left: globals.os == "ios" ? "4dp" : "11dp",
			height: "40dp",
			textAlign: "center",
			color: globals.colors.wordsInBoxesUnselected
		});
		
		button.add(onOffLabel);
		
		var titleLabel = Ti.UI.createLabel({
			text: name,
			color: globals.colors.black,
			textAlign: "center",
			font: globals.fonts.bold15,
			minimumFontSize: "10dp",
			top: "0dp"
		});
		
		
		function Toggle(){
			if(buttonContainer.isOn == true){
				buttonContainer.isOn = false;
				onOffLabel.text = "Off";
				button.backgroundColor = globals.colors.wordBoxesColors[0];
			}
			else{
				buttonContainer.isOn = true;
				onOffLabel.text = "On";
				button.backgroundColor = globals.colors.wordBoxesColors[2];
			}
		}
		
		buttonContainer.add(button);
		buttonContainer.add(titleLabel);
		
		
		return buttonContainer;
	}
	
	
	return view;
}