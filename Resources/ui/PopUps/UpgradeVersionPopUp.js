function Create(){
	var PopUpBackground = require("/ui/Common/PopUpBackground");
	var view = new PopUpBackground();
	
	var titleSign = Ti.UI.createView({
		backgroundImage:"/images/newVersionSign.png",
		width: 144,
		height: 25,
		top: 10
	});
	
	var centerLabel = Ti.UI.createLabel({
		text: "There is a new version of GoPhrazy with great stuff.\nPress ok to upgrade!",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15,
		width: view.popUpBg.width-20,
		top: 45
	});
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: 44,
		height: 29,
		bottom: 10,
		buttonID: "smallSquarePopUpOkButton",
		bubbleParent: true
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		var url;
		if(Ti.Platform.name == "iPhone OS") url = "itms-apps://itunes.apple.com/app/id903559056";
		else if (Ti.Platform.name == "android") url = "https://play.google.com/store/apps/details?id=com.mozzarello.gophrazy";
		Ti.Platform.openURL(url);
	});
	
	
	view.popUpBg.add(titleSign);
	view.popUpBg.add(centerLabel);
	view.popUpBg.add(okButton);
	
	
	return view;
}

module.exports = Create;