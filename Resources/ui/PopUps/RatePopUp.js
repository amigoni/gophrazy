function Create(){
	var dialog = Ti.UI.createAlertDialog({
	    cancel: 1,
	    title: globals.gameData.ratePopUpTitle,
	    message: globals.gameData.ratePopUpMessage,
	    buttonNames: ['Rate us!', 'Not now'],
	});
 	
  	dialog.addEventListener('click', function(e){
  		if(e.index == 0){
  			var url;
			if(Ti.Platform.name == "iPhone OS") //url = "itms-apps://itunes.apple.com/app/id903559056";
			url = "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=903559056&pageNumber=0& sortOrdering=1&type=Purple+Software&mt=8";
			else if (Ti.Platform.name == "android") url = "https://play.google.com/store/apps/details?id=com.mozzarello.gophrazy&reviewId=0";
			
			globals.gameData.ratedComplete = true;
			globals.gameData.ratedThisVersion = true;
			globals.col.gameData.commit();
			
			Ti.Platform.openURL(url);
  		}
  	});
  	
  	dialog.show();
}

module.exports = Create;
