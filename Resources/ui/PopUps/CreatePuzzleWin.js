exports.loadWin = function(data){
	var ServerData = require("model/ServerData");
	var SoundController = require("/controller/SoundController");
	var PhrasesController = require("controller/PhrasesController");
	var letterLimit = 90;
	var creatingWordBoxes = false;
	var colorPicker = CreateWordBoxColorPicker();
	var yourName = "";
	if(globals.col.friends.find({facebookID: globals.gameData.userFacebookID})[0] != null)yourName = globals.col.friends.find({facebookID: globals.gameData.userFacebookID})[0].name;
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: (globals.platform.actualHeight),
	  	zIndex: 100
	});
	
	
	var navBar = CreateNavbar();
	navBar.addEventListener("closeWin", function(){
		Close();
	});
	
	navBar.addEventListener("done", function(){
		//Check for words with Uppercase that are not at the beginning.
		var capWords = 0;
		for(var i = 0; i < wordsTextArray.length; i++){
			if (i != 0){
				var ch = wordsTextArray[i][0];
				if((ch.match(/[ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð]/) || ch.match(/\w/)) &&
					ch.toUpperCase() == ch
				) capWords++; 
			} 
		}
		
		if(capWords > 0){
			var text;
			if (capWords == 1) text = "There is a word that start with an uppercase but not at the begining of the phrase, is this ok?";
			else text =  "There are "+capWords+" words that start with an uppercase but not at the begining of the phrase, is this ok?";
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
				buttonNames: ['Ok', 'No'],
				message: text,
				title: 'Uppercase?'
			});
			  
			dialog.addEventListener('click', function(e){
			   if(e.index == 1) {
			 
			   }	
			   else{
			   		if(data.data == "new") SavePhrase();
			   		else UpdatePhrase();
			   		data.callback(textArea.value);
					Close();
			   }
			}); 
			dialog.show();	
		}
		else{
			if(data.data == "new") SavePhrase();
			else UpdatePhrase();
			data.callback(textArea.value);
			Close();
		}
	});
	
	var textArea = Ti.UI.createTextArea({
		//backgroundColor: "red",
		backgroundColor: globals.colors.white,
		borderColor: globals.colors.mediumBrown,
		value: '',
		font: globals.fonts.regular13r,
		width: view.width-20,
		height: 60,
		autocorrect: false,
		autocapitalization: false,
		//hintText: "type a short message for your friend",
		top: 54,
		lastString: ""
	});
	
	var hintLabel = Ti.UI.createLabel({
		text: "type your puzzle here, don't type too fast",
		font: globals.fonts.regular13r,
		opacity: data.data != "new"? 0 : .3,
		visible: data.data != "new" ? false : true,
		top: 5,
		left: 5
	});
	
	textArea.add(hintLabel);
	
	var wordsTextArray = [];
	var wordLenghtLimit = 25;
	var charsLimit = 90;
	var wordsLimit = 20;
	var blockTyping = false;
	
	var wordBoxesInterval = setInterval(function(){
		//if(creatingWordBoxes == false) 
		previewScrollView.CreateWordBoxes(wordsTextArray);
	}, 1000);
			
	
	textArea.addEventListener("change", function(e){
		//This is checking also for the hold delete.
		if(e.value.length > 0){
			hintLabel.opacity = 0;
			hintLabel.visible = false;
		}
		else{
			hintLabel.opacity = 0.3;
			hintLabel.visible = true;
		}
		//else previewScrollView.CreateWordBoxes([]);
		if(e.value != textArea.lastString) CheckString({value:e.value});
		
		textArea.lastString = e.value;
	});
	
	//Fix for keyboard
	/*
	var first = true;
	
	textArea.addEventListener('focus', function f(e){
	    if(first){
	        first = false;
	        textArea.blur();
	    }else{
	        textArea.removeEventListener('focus', f);
	    }
	});
	*/
	
	textArea.Enable = function(){
		navBar.doneButton.Enable();
		statusLabel.text = "Good!";
		statusLabel.color = globals.colors.darkBrown;
	};
	
	textArea.Disable = function(error){
		navBar.doneButton.Disable();
		statusLabel.text = error;
		statusLabel.color = globals.colors.red;
	};
	
	
	
	var statusContainer = Ti.UI.createView({
		width: view.width,
		height: 20,
		top: 115
	});
	
	var statusLabel = Ti.UI.createLabel({
		text: "Make a great puzzle!",
		font: globals.fonts.bold12,
		minimumFontSize: 12,
		textAlign: "left",
		color: globals.colors.darkBrown,
		left: 10,
	});
	
	var wordCountLabel = Ti.UI.createLabel({
		text: "Words: "+wordsLimit,
		font: globals.fonts.bold12,
		minimumFontSize: 12,
		textAlign: "right",
		color: globals.colors.darkBrown,
		right: 10,
	});
	
	var charsCountLabel = Ti.UI.createLabel({
		text: "Chars: "+ charsLimit,
		font: globals.fonts.bold12,
		minimumFontSize: 12,
		textAlign: "right",
		color: globals.colors.darkBrown,
		right: 75,
	});
	
	statusContainer.add(statusLabel);
	statusContainer.add(wordCountLabel);
	statusContainer.add(charsCountLabel);
	
	
	
	var previewScrollView = Ti.UI.createScrollView({
		//backgroundColor: "red",
		width: textArea.width,
		height: view.height-145-200,
		contentWidth: textArea.width,
		contentHeight: "auto",
		top: 180
	});
	
	var previewContainer = Ti.UI.createView({
		width: previewScrollView.width,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		top: 0
	});
	
	previewScrollView.add(previewContainer);
	
	var wordBoxes = [];
	previewScrollView.CreateWordBoxes = function(words){
		creatingWordBoxes = true;
		//if(previewContainer != null) previewScrollView.remove(previewContainer);
		for(var i = 0; i < words.length; i++){
			var wordBox = wordBoxes[i];
			if(wordBox == null){
				wordBox = CreateWordBox(words[i], colorPicker.Next());
				previewContainer.add(wordBox);
				wordBoxes.push(wordBox);
			}
			else{
				wordBox.Update(words[i]);
			}	
		}
		
		for(var i = wordBoxes.length-1; i >= 0 ; i--){
			if(i >= words.length) {
				previewContainer.remove(wordBoxes[i]);
				wordBoxes.pop();
			}
			else break;
		}
		creatingWordBoxes = false;
	};
	
	
	var authorTextField = Ti.UI.createTextField({
		borderColor: globals.colors.mediumBrown,
		backgroundColor: globals.colors.white,
		font: globals.fonts.regular13r,
		width: view.width - 20,
		autocorrect: false,
		autocapitalization: false,
		maxLength: 50,
		height: 35,
		hintText: "type an author, if empty it will be your full name",
		value: data.data != "new" ? data.data.author : "",
		paddingLeft: 10,
		paddingRight: 10,
		top: 140
	});
	
	view.add(navBar);
	view.add(textArea);
	view.add(statusContainer);
	view.add(authorTextField);
	view.add(previewScrollView);
	
	
	
	function CheckString(e){
		var lastChar;
		var oneBefore;
		var oneWordTooLong = false;
		var disable = false;
		var disableReason = "";
		var lastApproved = true;
		
		/////AUTOCORRECTIONS
		//Remove if over 90
		var string = e.value;
		if(string.length > charsLimit) {
			string = string.substring(0, charsLimit);
		}	
		
		lastChar = string[string.length-1];
		if(string.length > 2) oneBefore = string[string.length-2];
		
		//Remove if not a number,letter, . ? ! , or space
		if(
			string.length > 1 &&
			!lastChar.match(/[ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð$€£¢¥₩!?,.'`%-]/) && 
			!lastChar.match(/\w/) && 
			!lastChar.match(/\s/)
		){
		//if(!lastChar.match(/([ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð$!?,.])(\w)(\s)/)){
			PopLastChar();
		}	
		
		//Remove if double punctuation
		if(string.length > 2 && oneBefore != null && oneBefore.match(/[!?.,'`%-]/) && lastChar.match(/[!?.,'`%-]/)){
			PopLastChar();
		}
		
		//Remove if double space
		if(string.length > 2 && oneBefore != null && oneBefore.match(/\s/) && lastChar.match(/\s/)){
			PopLastChar();
		}
		
		//Remove if space before punctuation
		if( oneBefore != null && oneBefore.match(/\s/) && lastChar.match(/[!?.,]/)){
			string = string.slice(0,string.length-2)+string.slice(string.length-1, string.length);
			lastChar = string[string.length-1];
			if(string.length > 2) oneBefore = string[string.length-2];
		}
		
		//Capitalize first letter
		string =  string.charAt(0).toUpperCase() + string.slice(1);
		/////
		
		wordsTextArray = string.match(/\S+\s*/g);
		if (wordsTextArray == null) wordsTextArray = [];
		
		/////Warnings
		//Last char is punctuation
		if(lastChar != "." && lastChar != "?" && lastChar != "!"){
			disable = true;
			disableReason = "Need to end: . ? or !";
		}	
		
		//Length of Chars
		if(string.length == charsLimit){
			if(globals.gameData.vibrateOn == true) Ti.Media.vibrate([ 0, 200, 200, 200]);
			blockTyping = true;
			charsCountLabel.color = globals.colors.red;
		} 
		else if(string.length < 4) {
			blockTyping = false;
			disable = true;
			disableReason = "Need more chars.";
			charsCountLabel.color = globals.colors.red;
		}	
		else{
			blockTyping = false;
			charsCountLabel.color = globals.colors.darkBrown;
		}
		
		//Number of words
		if(wordsTextArray.length < 3) {
			disable = true;
			disableReason = "Need at least 3 words.";
			wordCountLabel.color = globals.colors.red;
		}
		else if(wordsTextArray.length > 20) {
			disable = true;
			disableReason = "Too many words, max 20.";
			wordCountLabel.color = globals.colors.red;
		}
		else{
			wordCountLabel.color = globals.colors.darkBrown;
		}
		
		//Word too long
		for(var i = 0; i < wordsTextArray.length; i++){
			if(wordsTextArray[i].length > 25) {
				oneWordTooLong = true;
				break;
			}	
		}
			
		if(oneWordTooLong == true){
			disable = true;
			disableReason = "One word is too long. Max "+wordLenghtLimit;
		}	
		/////
			
			
		var charsLeft = charsLimit - textArea.value.length;	
		charsCountLabel.text = "Chars: "+charsLeft;
		
		var wordsLeft = wordsLimit - wordsTextArray.length;
		wordCountLabel.text = "Words: "+wordsLeft;
		
		if (disable == true) textArea.Disable(disableReason);
		else textArea.Enable();
		if(e.value != string) textArea.value = string;
		
		//if(creatingWordBoxes == false) previewScrollView.CreateWordBoxes(wordsTextArray);
			
		function PopLastChar(){
			string = string.substring(0, string.length-1);
			lastChar = string[string.length-1];
			if(string.length > 2) oneBefore = string[string.length-2];
		}
	}
	
	
	function SavePhrase(){
		var authorText = authorTextField.value;
		if (authorText == "") authorText = yourName;
		
		var now = clock.getNow();
		var phrase = PhrasesController.SavePhrase({
   			phraseID: now.valueOf(),
   			text: crypt.encrypt(textArea.value),
   			author: authorText, //my name,
   			type: "written",
   			length: textArea.value.length,
   			numberOfWords: wordsTextArray.length,
   			difficulty: wordsTextArray.length,
   			isFunny: false,
   			complete: false,
   			isBeingUsed: false,
   			favorite: false,
   			receiverIDs: [],
   			skipped: false,
   			hide: false,
   			written: true,
   			createdDate: now.valueOf(),
   			solvedCountOthers: 0
   		});
  
   		
   		Ti.App.fireEvent("wrotePhrase",{data:globals.col.phrases.find({phraseID: now.valueOf()})[0]});
	
		var AnalyticsController = require("/controller/AnalyticsController");
		AnalyticsController.RegisterEvent({category:"design", event_id: "messageComposed", value:globals.col.phrases.count({type:"written"})});
	
		//Server Code
		Common.CreateButtonBlocker(true, true);
		var PuzzleFactoryController = require("/controller/PuzzleFactoryController");
		PuzzleFactoryController.SavePuzzleToServer(now.valueOf(), function(puzzleData){
			globals.rootWin.OpenWindow("sharePuzzlePopUp",{data: puzzleData});
		});
	}
	
	
	function UpdatePhrase(){
		var authorText = authorTextField.value;
		if (authorText == "") authorText = yourName;
		
		var phrase = globals.col.phrases.find({phraseID:data.data.phraseID})[0];
		phrase.text = crypt.encrypt(textArea.value);
		phrase.author = authorText;
		globals.col.phrases.commit();
		Ti.App.fireEvent("updateCreatedPhrase",{data: phrase});
	}
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		view.fireEvent("close");
		navBar.Close();
		DisappearAnimation();
		clearInterval(wordBoxesInterval);
	}
	
	function AppearAnimation(){
		Common.WindowOpenAnimation(view);
		setTimeout(function(){
			textArea.focus();
			if(data.data != "new") CheckString({value: crypt.decrypt(data.data.text)});
		},550);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	
	view.Open = Open;
	view.Close = Close;
	
	
	//Forces TextArea check for Edit Puzzle
	//if(data.data != "new") setTimeout(function(){CheckString({value:crypt.decrypt(data.data.text)});},500);
	
	
	return view;
};


function CreateNavbar (){
	var view = Ti.UI.createView({
		backgroundColor: "##D5C7A3",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var doneButton = Ti.UI.createLabel({
		text: "Done",
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		textAlign: "center",
		width: 70,
		height: 44,
		left: 0,
		buttonID: "noteComposeWinSendButton",
		touchEnabled: false,
		opacity: 0.5
	});
	
	doneButton.Enable = function(){
		doneButton.opacity = 1;
		doneButton.touchEnabled = true;
	};
	
	doneButton.Disable = function(){
		doneButton.opacity = 0.5;
		doneButton.touchEnabled = false;
	};
	
	Common.MakeButton(doneButton);
	
	doneButton.addEventListener("singletap", function(){
		if(doneButton.touchEnabled == true) {
			view.fireEvent("done");
		}	
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID:"composeWinCloseButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if(closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	/*
	var title = Ti.UI.createLabel({
		text: "Create Puzzle",
		textAlign: "center",
		font: globals.fonts.bold13,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: view.width - 120
	});
	*/
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 25,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createImageView({
		image:"/images/defaultLogo.png",
		width: 34,
		height: 25
	}));
	
	title.add(Ti.UI.createImageView({
		image:"/images/writePuzzleSign.png",
		width: 126,
		height: 24,
		left: 7
	}));
	
	
	view.add(doneButton);
	view.add(title);
	view.add(closeButton);
	
	
	
	function Close(){
		
	}
	
	view.Close = Close;
	view.doneButton = doneButton;
	
	
	return view;
}


function CreateWordBox(text, color){
	var view = Ti.UI.createView({
		backgroundColor: color,
		borderColor: globals.colors.borderLine2,
		borderWidth: 2,
		borderRadius: 6,
		width: Ti.UI.SIZE,
		height: 40,
		left: 5,
		top: 5,
		opacity: 1,
		sizeIsSet: false,
		text: Trim(text),
		touchEnabled: false
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.words1,
		shadowOffset:{x:1,y:1},
		shadowColor:"black",
		left: 12,
		height: 40,
		color: globals.colors.wordsInBoxesUnselected,
		touchEnabled: false,
		bubbleParent: false
	});
	
	/*
	label.addEventListener("postlayout", function(e){
		//Postlayout event is a bit weird it doesn't always set on first time
		//We check for width here to make sure that it's been set and then we
		//flip the sizeIsSet to prevent and infinate loop
		if (view.sizeIsSet == false && view.size.width > 12){
			this.width = this.rect.width;
			view.width = this.rect.width+8;
			view.sizeIsSet = true; 
		}
	});
	*/
	
	view.add(label);
	
	function Update(newText){
		newText = Trim(newText);
		if(newText != text){
			view.sizeIsSet = false;
			text = newText;
			view.text = newText;
			label.text = newText;
		}
	}
	
	function Trim(textToTrim){
		var text2 = textToTrim;
		if(textToTrim[textToTrim.length-1] == " ") text2 = textToTrim.substring(0, textToTrim.length - 1);
		return text2;
	}
	
	view.Update = Update;
	
	
	return view;	
}


function CreateWordBoxColorPicker (){
	var RandomPlus = require("lib/RandomPlus");
	var picker = {};
	var wordBoxColors = globals.colors.wordBoxesColors;//globals.colors.wordBoxesColorShuffleBag.next()
	var wordBoxColorsArrayPosition = 0;//RandomPlus.RandomRange(0, wordBoxColors.length);
	
	function Next(){
		var color = wordBoxColors[wordBoxColorsArrayPosition];
		wordBoxColorsArrayPosition++;
		if (wordBoxColorsArrayPosition >= wordBoxColors.length) wordBoxColorsArrayPosition = 0;
		return color;
	}
	
	picker.Next = Next;
	
	
	return picker;
}
