exports.loadWin = function(data){
	var ServerData = require("model/ServerData");
	var SoundController = require("/controller/SoundController");
	var PhrasesController = require("controller/PhrasesController");
	var letterLimit = 90;
	var creatingWordBoxes = false;
	var colorPicker = CreateWordBoxColorPicker();
	var yourName = globals.gameData.userName;
	var textFieldBorderColor = "#E3E1D3";
	var published = 0;
	
	if(globals.col.friends.find({facebookID: globals.gameData.userFacebookID})[0] != null) yourName = globals.col.friends.find({facebookID: globals.gameData.userFacebookID})[0].name;
	
	var view = Titanium.UI.createView({ 
	    backgroundColor: globals.colors.black,
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	  	bubbleParent: false,
	  	top: (globals.platform.actualHeight),
	  	zIndex: 100
	});
	
	
	var navBar = CreateNavbar();
	navBar.addEventListener("closeWin", function(){
		Close();
	});
	
	navBar.addEventListener("done", function(){
		
		//if(leadTextField.value.length == 0) alert("There is no Lead Text?\nWhy no put something?");
		if(leadTextField.okToPublish == false){
			if(leadTextField.value.length > 90) alert("Please limit your Lead Text to 90 characters");
		}
		
		//if(completeTextField.value.length == 0) alert("There is no Conclusion Text?\nWhy no put something?");
		if(completeTextField.okToPublish == false){
			if(completeTextField.value.length > 90) alert("Please limit your Conclusion Text to 90 characters");
		}
		
		if(navBar.doneButton.enabled == false){
			alert("Please check your puzzle");
		}
		
		//Check for words with Uppercase that are not at the beginning.
		if(leadTextField.okToPublish == true && completeTextField.okToPublish && navBar.doneButton.enabled == true){
			var capWords = 0;
			for(var i = 0; i < wordsTextArray.length; i++){
				if (i != 0){
					var ch = wordsTextArray[i][0];
					if((ch.match(/[ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð]/) || ch.match(/\w/)) &&
						ch.toUpperCase() == ch
					) capWords++; 
				} 
			}
			
			if(capWords > 0){
				var text;
				if (capWords == 1) text = "There is a word that start with an uppercase but not at the begining of the phrase, is this ok?";
				else text =  "There are "+capWords+" words that start with an uppercase but not at the begining of the phrase, is this ok?";
				var dialog = Ti.UI.createAlertDialog({
				    cancel: 1,
					buttonNames: ['Ok', 'No'],
					message: text,
					title: 'Uppercase?'
				});
				  
				dialog.addEventListener('click', function(e){
					if(e.index == 0) CheckForSubmition();
				}); 
				dialog.show();	
			}
			else{
				CheckForSubmition();
			}
		}

	});
	
	
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: view.height-44,
		top: 44,
		contentWidth: view.width,
		contentHeight: "auto",
		layout: "vertical"
	});
	
	var languageLabel = Ti.UI.createLabel({
		text: "English",
		font: globals.fonts.bold20,
		textAlign: "left",
		color: globals.colors.white,
		width: 270,
		left: 10,
		top: 0
	});
	
	Common.MakeButton(languageLabel);
	
	languageLabel.addEventListener("touchend", function(){
		var LanguagePickerView = require("/ui/PopUps/LanguagePickerView");
		var languagePickerView = new LanguagePickerView();
		
		languagePickerView.addEventListener("selected", function(e){
			languageLabel.text = e.data.name;
			view.remove(languagePickerView);
			languagePickerView = null;
		});
		
		languagePickerView.addEventListener("closeWin", function(e){
			view.remove(languagePickerView);
			languagePickerView = null;
		});
		
		
		view.add(languagePickerView);
	});
	
	
	
	var container = Ti.UI.createView({
		backgroundColor: globals.colors.white,
		width: 300,
		height: Ti.UI.SIZE,
		top: 10,
		layout: "vertical"
	});
	
	var leadTextTitleLabel = Ti.UI.createLabel({
		text: "Lead Text (Optional)",
		font: globals.fonts.bold15,
		textAlign: "center",
		color: globals.colors.black,
		width: 270,
		top: 10
	});
	
	var leadTextField = Ti.UI.createTextArea({
		backgroundColor: globals.colors.white,
		borderColor: textFieldBorderColor,
		value: '',
		font: globals.fonts.regular14,
		width: 270,
		height: 60,
		autocorrect: false,
		autocapitalization: false,
		textAlign: "left",
		top: 0,
		lastString: "",
		okToPublish: true
	});
	
	leadTextField.countLabel = Ti.UI.createLabel({
		text: "",
		font: globals.fonts.regular12,
		minimumFontSize: 12,
		textAlign: "right",
		color: globals.colors.black,
		right: 4,
		bottom: 0
	});
	
	leadTextField.add(leadTextField.countLabel);
	
	leadTextField.addEventListener("change", function(e){
		var string = e.value;
		if(e.value.length > 0) leadTextField.hintLabel.opacity = 0;
		else leadTextField.hintLabel.opacity = 0.3;
		
		var difference = 90 - e.value.length;
		
		if(difference < 0) {
			leadTextField.countLabel.color = globals.colors.red;
			leadTextField.color = globals.colors.red;
		}	
		else {
			leadTextField.countLabel.color = globals.colors.black;
			leadTextField.color = globals.colors.black;
		}
		
		if(difference == 0) leadTextField.countLabel.text = "";
		
		if(difference >= 0) leadTextField.okToPublish = true;
		else  leadTextField.okToPublish = false;
		
		leadTextField.countLabel.text = difference;
		
		leadTextField.value =  string.charAt(0).toUpperCase() + string.slice(1);
	});
	
	leadTextField.hintLabel = Ti.UI.createLabel({
		text: 'write something intriguing to tempt people to solve your puzzle, place hash tags here\n*Defaults to: "Solve my puzzle if you can!"',
		textAlign: "center",
		font: globals.fonts.regular13r,
		opacity: data.data != "new"? 0 : .3,
		visible: data.data != "new" ? false : true,
		width: leadTextField.width
	});
	
	leadTextField.add(leadTextField.hintLabel);
	
	
	var puzzleTextTitleContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 30,
		layout: "horizontal",
		top: 10
	});
	
	var puzzleTextTitleLabel = Ti.UI.createLabel({
		text: "Puzzle Text",
		font: globals.fonts.bold20,
		textAlign: "center",
		color: globals.colors.black,
		//width: 270,
		height: 30,
		left: 5
		//top: 10
	});
	
	puzzleTextTitleContainer.add(Ti.UI.createView({
		backgroundImage: "/images/defaultLogo.png",
		width: 40*.7,
		height: 30*.7
	}));
	puzzleTextTitleContainer.add(puzzleTextTitleLabel);
	
	var textArea = Ti.UI.createTextArea({
		//backgroundColor: "red",
		backgroundColor: globals.colors.white,
		borderColor: textFieldBorderColor,
		value: '',
		font: globals.fonts.regular14,
		width: 270,
		height: 60,
		autocorrect: false,
		autocapitalization: false,
		textAlign: "left",
		top: 0,
		lastString: ""
	});
	
	textArea.addEventListener("focus", function(){
		//Puts the scrollview to the right place
		setTimeout(function(){scrollView.scrollTo(0,100);},500);
	}); 
	
	textArea.addEventListener("blur", function(){
		if(navBar.doneButton.enabled == false) alert("Please check your puzzle");
	});
	
	var hintLabel = Ti.UI.createLabel({
		text: "write your amazing puzzle here,\ndon't type too fast",
		textAlign: "center",
		font: globals.fonts.regular13r,
		opacity: data.data != "new"? 0 : .3,
		visible: data.data != "new" ? false : true,
		width: textArea.width
	});
	
	textArea.add(hintLabel);
	
	var wordsTextArray = [];
	var wordLenghtLimit = 25;
	var charsLimit = 90;
	var wordsLimit = 20;
	var blockTyping = false;
	
	
	var wordBoxesInterval = setInterval(function(){
		//if(creatingWordBoxes == false) 
		previewContainer.CreateWordBoxes(wordsTextArray);
	}, 1000);
	
	textArea.addEventListener("change", function(e){
		//This is checking also for the hold delete.
		if(e.value.length > 0){
			hintLabel.opacity = 0;
			hintLabel.visible = false;
		}
		else{
			hintLabel.opacity = 0.3;
			hintLabel.visible = true;
		}
		
		if(e.value != textArea.lastString) CheckString({value:e.value});
		
		textArea.lastString = e.value;
	});
	
	//Fix for keyboard
	/*
	var first = true;
	
	textArea.addEventListener('focus', function f(e){
	    if(first){
	        first = false;
	        textArea.blur();
	    }else{
	        textArea.removeEventListener('focus', f);
	    }
	});
	*/
	
	textArea.Enable = function(){
		navBar.doneButton.Enable();
		statusLabel.text = "Good!";
		statusLabel.color = globals.colors.black;
		textArea.color = globals.colors.black;
	};
	
	textArea.Disable = function(error){
		navBar.doneButton.Disable();
		statusLabel.text = error;
		statusLabel.color = globals.colors.red;
		textArea.color = globals.colors.red;
	};
	
	
	
	var statusContainer = Ti.UI.createView({
		width: 270,
		height: 20,
		top: 0
	});
	
	var statusLabel = Ti.UI.createLabel({
		text: "Make a great puzzle!",
		font: globals.fonts.regular12,
		minimumFontSize: 12,
		textAlign: "left",
		color: globals.colors.black,
		left: 0,
	});
	
	var wordCountLabel = Ti.UI.createLabel({
		text: "Words: "+wordsLimit,
		font: globals.fonts.regular12,
		minimumFontSize: 12,
		textAlign: "right",
		color: globals.colors.black,
		right: 0,
	});
	
	var charsCountLabel = Ti.UI.createLabel({
		text: "Chars: "+ charsLimit,
		font: globals.fonts.regular12,
		minimumFontSize: 12,
		textAlign: "right",
		color: globals.colors.black,
		right: 65,
	});
	
	statusContainer.add(statusLabel);
	statusContainer.add(wordCountLabel);
	statusContainer.add(charsCountLabel);
	
	var previewContainer = Ti.UI.createView({
		width: textArea.width,
		height: Ti.UI.SIZE,
		layout: "horizontal",
		//top: 0
	});
	
	var wordBoxes = [];
	previewContainer.CreateWordBoxes = function(words){
		creatingWordBoxes = true;
		
		for(var i = 0; i < words.length; i++){
			var wordBox = wordBoxes[i];
			if(wordBox == null){
				wordBox = CreateWordBox(words[i], colorPicker.Next());
				previewContainer.add(wordBox);
				wordBoxes.push(wordBox);
			}
			else{
				wordBox.Update(words[i]);
			}	
		}
		
		for(var i = wordBoxes.length-1; i >= 0 ; i--){
			if(i >= words.length) {
				previewContainer.remove(wordBoxes[i]);
				wordBoxes.pop();
			}
			else break;
		}
		creatingWordBoxes = false;
	};
	
	
	
	
	var completeTextTitleLabel = Ti.UI.createLabel({
		text: "Conclusion Text (Optional)",
		font: globals.fonts.bold15,
		textAlign: "center",
		color: globals.colors.black,
		width: 270,
		top: 10
	});
	
	var completeTextField = Ti.UI.createTextArea({
		backgroundColor: globals.colors.white,
		borderColor: textFieldBorderColor,
		value: '',
		font: globals.fonts.regular14,
		width: 270,
		height: 60,
		autocorrect: false,
		autocapitalization: false,
		textAlign: "left",
		top: 0,
		okToPublish: true
	});
	
	completeTextField.countLabel = Ti.UI.createLabel({
		text: "90",
		font: globals.fonts.regular12,
		minimumFontSize: 12,
		textAlign: "right",
		color: globals.colors.black,
		right: 4,
		bottom: 0
	});
	
	completeTextField.add(completeTextField.countLabel);
	
	completeTextField.addEventListener("change", function(e){
		var string = e.value;
		if(e.value.length > 0) completeTextField.hintLabel.opacity = 0;
		else completeTextField.hintLabel.opacity = 0.3;
		
		var difference = 90 - e.value.length;
		if(difference < 0) {
			completeTextField.countLabel.color = globals.colors.red;
			completeTextField.color = globals.colors.red;
		}	
		else{
			completeTextField.countLabel.color = globals.colors.black;
			completeTextField.color = globals.colors.black;
		}	
		
		if(difference == 0) completeTextField.countLabel.text = "";
		
		if(difference >= 0) completeTextField.okToPublish = true;
		else completeTextField.okToPublish = false;
		
		completeTextField.countLabel.text = difference;
	
		completeTextField.value =  string.charAt(0).toUpperCase() + string.slice(1);
	});
	
	completeTextField.hintLabel = Ti.UI.createLabel({
		text: 'write what you want to reveal after the puzzle is solved, like the author of a quote\n*Defaults to: '+yourName,
		textAlign: "center",
		font: globals.fonts.regular13r,
		opacity: data.data != "new"? 0 : .3,
		visible: data.data != "new" ? false : true,
		width: completeTextField.width
	});
	
	completeTextField.add(completeTextField.hintLabel);
	
	
	container.add(leadTextTitleLabel);
	container.add(leadTextField);
	container.add(puzzleTextTitleContainer);
	container.add(textArea);
	container.add(statusContainer);
	container.add(previewContainer);
	container.add(completeTextTitleLabel);
	container.add(completeTextField);
	container.add(Ti.UI.createView({height: 20}));
	
	scrollView.add(languageLabel);
	scrollView.add(container);
	scrollView.add(CreateExampleView());
	scrollView.add(Ti.UI.createView({height: 10}));
	
	view.add(navBar);
	
	view.add(scrollView);
	
	
	//Functions
	function CheckString(e){
		var lastChar;
		var oneBefore;
		var oneWordTooLong = false;
		var disable = false;
		var disableReason = "";
		var lastApproved = true;
		
		/////AUTOCORRECTIONS
		//Remove if over 90
		var string = e.value;
		if(string.length > charsLimit) {
			string = string.substring(0, charsLimit);
		}	
		
		lastChar = string[string.length-1];
		if(string.length > 2) oneBefore = string[string.length-2];
		
		//Remove if not a number,letter, . ? ! , or space
		if(
			string.length > 1 &&
			!lastChar.match(/[ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð$€£¢¥₩!?,.'`%-]/) && 
			!lastChar.match(/\w/) && 
			!lastChar.match(/\s/)
		){
		//if(!lastChar.match(/([ÀÈÌÒÙàèìòùÁÉÍÓÚÝáéíóúýÂÊÎÔÛâêîôûÃÑÕãñõÄËÏÖÜäëïöüçÇßØøÅåÆæÞþÐð$!?,.])(\w)(\s)/)){
			PopLastChar();
		}	
		
		//Remove if double punctuation
		if(string.length > 2 && oneBefore != null && oneBefore.match(/[!?.,'`%-]/) && lastChar.match(/[!?.,'`%-]/)){
			PopLastChar();
		}
		
		//Remove if double space
		if(string.length > 2 && oneBefore != null && oneBefore.match(/\s/) && lastChar.match(/\s/)){
			PopLastChar();
		}
		
		//Remove if space before punctuation
		if( oneBefore != null && oneBefore.match(/\s/) && lastChar.match(/[!?.,]/)){
			string = string.slice(0,string.length-2)+string.slice(string.length-1, string.length);
			lastChar = string[string.length-1];
			if(string.length > 2) oneBefore = string[string.length-2];
		}
		
		//Capitalize first letter
		string =  string.charAt(0).toUpperCase() + string.slice(1);
		/////
		
		wordsTextArray = string.match(/\S+\s*/g);
		if (wordsTextArray == null) wordsTextArray = [];
		
		/////Warnings
		//Last char is punctuation
		if(lastChar != "." && lastChar != "?" && lastChar != "!"){
			disable = true;
			disableReason = "Need to end: . ? or !";
		}	
		
		//Length of Chars
		if(string.length == charsLimit){
			if(globals.gameData.vibrateOn == true) Ti.Media.vibrate([ 0, 200, 200, 200]);
			blockTyping = true;
			if(string.length > charsLimit) charsCountLabel.color = globals.colors.red;
		} 
		else if(string.length < 4) {
			blockTyping = false;
			disable = true;
			disableReason = "Need more chars.";
			charsCountLabel.color = globals.colors.red;
		}	
		else{
			blockTyping = false;
			charsCountLabel.color = globals.colors.black;
		}
		
		//Number of words
		if(wordsTextArray.length < 3) {
			disable = true;
			disableReason = "Need at least 3 words.";
			wordCountLabel.color = globals.colors.red;
		}
		else if(wordsTextArray.length > 20) {
			disable = true;
			disableReason = "Too many words, max 20.";
			wordCountLabel.color = globals.colors.red;
		}
		else{
			wordCountLabel.color = globals.colors.black;
		}
		
		//Word too long
		for(var i = 0; i < wordsTextArray.length; i++){
			if(wordsTextArray[i].length > 25) {
				oneWordTooLong = true;
				break;
			}	
		}
			
		if(oneWordTooLong == true){
			disable = true;
			disableReason = "One word is too long. Max "+wordLenghtLimit;
		}	
		/////
			
			
		var charsLeft = charsLimit - textArea.value.length;	
		charsCountLabel.text = "Chars: "+charsLeft;
		
		var wordsLeft = wordsLimit - wordsTextArray.length;
		wordCountLabel.text = "Words: "+wordsLeft;
		
		if (disable == true) {
			textArea.Disable(disableReason);
		}	
		else textArea.Enable();
		if(e.value != string) textArea.value = string;
				
		function PopLastChar(){
			string = string.substring(0, string.length-1);
			lastChar = string[string.length-1];
			if(string.length > 2) oneBefore = string[string.length-2];
		}
	}
	
	function CheckForSubmition(){
		var dialog = Ti.UI.createAlertDialog({
		    cancel: 1,
			buttonNames: ['Yes', 'No'],
			message: "Would you like to submit your puzzle for a chance to be featured?",
			title: 'Submit to Featured Puzzles?'
		});
		  
		dialog.addEventListener('click', function(e){
		   if(e.index == 0) {
				published = 1;
		   }	
		   else{
		   		published = 0;
		   }
		   
		   	if(data.data == "new") SavePhrase();
	   		else UpdatePhrase();
	   		data.callback(textArea.value);
			Close();
		}); 
		dialog.show();
	}
	
	
	function SavePhrase(){
		var completeText = completeTextField.value;
		if (completeText == "" || completeText == null) completeText = yourName;
		
		var leadText = leadTextField.value;
		if(leadText == "" || completeText == null) leadText = "Solve my puzzle if you can!";
		
		var now = clock.getNow();
		var phrase = PhrasesController.SavePhrase({
   			author: completeText, //my name,
   			authorID: globals.gameData.userName,
   			complete: false,
   			completeText: completeText,
   			createdDate: now.valueOf(),
   			difficulty: wordsTextArray.length,
   			favorite: false,
   			hide: false,
   			isBeingUsed: false,
   			isFunny: false,
   			leadText: leadText,
   			length: textArea.value.length,
   			numberOfWords: wordsTextArray.length,
   			phraseID: now.valueOf().toString(),
   			receiverIDs: [],
   			skipped: false,
   			solvedCountOthers: 0,
   			text: crypt.encrypt(textArea.value),
   			type: "written",
   			written: true,
   			language: languageLabel.text,
   			published: published
   		});
  
   		
   		
	
		var AnalyticsController = require("/controller/AnalyticsController");
		AnalyticsController.RegisterEvent({category:"design", event_id: "messageComposed", value:globals.col.phrases.count({type:"written"})});
	
		//Server Code
		Common.CreateButtonBlocker(true, true);
		var PuzzleFactoryController = require("/controller/PuzzleFactoryController");
		PuzzleFactoryController.SavePuzzleToServer(now.valueOf(), function(puzzleData){
			globals.rootWin.OpenWindow("sharePuzzlePopUp",{data: globals.col.phrases.find({phraseID:puzzleData.puzzleID})[0], shareType: "written"});
			Ti.App.fireEvent("wrotePhrase",{data:globals.col.phrases.find({phraseID: now.valueOf()})[0]});
		});
	}
	
	
	function UpdatePhrase(){
		var completeText = completeTextField.value;
		if (completeText == "") completeText = yourName;
		
		var leadText = leadTextField.value;
		if(leadText == "") leadText = "Solve my puzzle if you can!";
		
		var phrase = globals.col.phrases.find({phraseID:data.data.phraseID})[0];
		phrase.leadText = leadText;
		phrase.text = crypt.encrypt(textArea.value);
		phrase.author = completeText;
		globals.col.phrases.commit();
		Ti.App.fireEvent("updateCreatedPhrase",{data: phrase});
	}
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		view.fireEvent("close");
		navBar.Close();
		DisappearAnimation();
		//clearInterval(wordBoxesInterval);
	}
	
	function AppearAnimation(){
		Common.WindowOpenAnimation(view);
		setTimeout(function(){
			//textArea.focus();
			if(data.data != "new") CheckString({value: crypt.decrypt(data.data.text)});
		},550);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	
	view.Open = Open;
	view.Close = Close;
	
	
	//Forces TextArea check for Edit Puzzle
	//if(data.data != "new") setTimeout(function(){CheckString({value:crypt.decrypt(data.data.text)});},500);
	
	
	return view;
};


function CreateNavbar (){
	var view = Ti.UI.createView({
		//backgroundColor: "##D5C7A3",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var doneButton = Ti.UI.createLabel({
		text: "Done",
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		color: globals.colors.white,
		textAlign: "center",
		width: 70,
		height: 44,
		left: 0,
		buttonID: "noteComposeWinSendButton",
		touchEnabled: false,
		opacity: 0.3,
		enabled: false
	});
	
	doneButton.Enable = function(){
		doneButton.enabled = true;
		doneButton.opacity = 1;
		//doneButton.touchEnabled = true;
	};
	
	doneButton.Disable = function(){
		doneButton.enabled = false;
		doneButton.opacity = 0.5;
		//doneButton.touchEnabled = false;
	};
	
	Common.MakeButton(doneButton);
	
	doneButton.addEventListener("singletap", function(){
		if(doneButton.touchEnabled == true) {
			if (doneButton.enabled == true) view.fireEvent("done");
			else alert("Please check your puzzle.");	
		}
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID:"composeWinCloseButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if(closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var titleLabel = Ti.UI.createLabel({
		text: "Write a Puzzle",
		font: globals.fonts.bold20,
		color: globals.colors.warning,
		top: 0,
		width: 240,
		height: 44,
		minimumFontSize: 5,
		textAlign: "center"
	});
	
	
	view.add(doneButton);
	view.add(titleLabel);
	view.add(closeButton);
	
	
	
	function Close(){
		
	}
	
	view.Close = Close;
	view.doneButton = doneButton;
	
	
	return view;
}


function CreateWordBox(text, color){
	var view = Ti.UI.createView({
		backgroundColor: color,
		borderColor: globals.colors.borderLine2,
		borderWidth: 2,
		borderRadius: 6,
		width: Ti.UI.SIZE,
		height: 40,
		left: 5,
		top: 5,
		opacity: 1,
		sizeIsSet: false,
		text: Trim(text),
		touchEnabled: false
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: globals.fonts.words1,
		shadowOffset:{x:1,y:1},
		shadowColor:"black",
		left: 12,
		height: 40,
		color: globals.colors.wordsInBoxesUnselected,
		touchEnabled: false,
		bubbleParent: false
	});
	
	/*
	label.addEventListener("postlayout", function(e){
		//Postlayout event is a bit weird it doesn't always set on first time
		//We check for width here to make sure that it's been set and then we
		//flip the sizeIsSet to prevent and infinate loop
		if (view.sizeIsSet == false && view.size.width > 12){
			this.width = this.rect.width;
			view.width = this.rect.width+8;
			view.sizeIsSet = true; 
		}
	});
	*/
	
	view.add(label);
	
	function Update(newText){
		newText = Trim(newText);
		if(newText != text){
			view.sizeIsSet = false;
			text = newText;
			view.text = newText;
			label.text = newText;
		}
	}
	
	function Trim(textToTrim){
		var text2 = textToTrim;
		if(textToTrim[textToTrim.length-1] == " ") text2 = textToTrim.substring(0, textToTrim.length - 1);
		return text2;
	}
	
	view.Update = Update;
	
	
	return view;	
}


function CreateWordBoxColorPicker (){
	var RandomPlus = require("lib/RandomPlus");
	var picker = {};
	var wordBoxColors = globals.colors.wordBoxesColors;//globals.colors.wordBoxesColorShuffleBag.next()
	var wordBoxColorsArrayPosition = 0;//RandomPlus.RandomRange(0, wordBoxColors.length);
	
	function Next(){
		var color = wordBoxColors[wordBoxColorsArrayPosition];
		wordBoxColorsArrayPosition++;
		if (wordBoxColorsArrayPosition >= wordBoxColors.length) wordBoxColorsArrayPosition = 0;
		return color;
	}
	
	picker.Next = Next;
	
	
	return picker;
}


function CreateExampleView(){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 10
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: "Examples",
		font: globals.fonts.bold20,
		textAlign: "center",
		color: globals.colors.white,
		width: view.width,
		//left: 10,
		top: 0
	});
	
	var subTitleLabel = Ti.UI.createLabel({
		text: "We can't wait to see what you create",
		font: globals.fonts.regular12,
		textAlign: "center",
		color: globals.colors.white,
		width: view.width,
		//left: 10,
		top: 0
	});
	
	
	var data = [
		{title: "Quote", leadText:"#peace Wise words from a great one", puzzleText: "Peace begins with a smile.", conclusionText: "Mother Theresa"},
		{title: "Witty Joke", leadText:"#LOL #witty sounds like my life.", puzzleText: "There’s no one I’d rather be in bed with.", conclusionText: "your iPhone"},
		{title: "Story", leadText:"Manuel looked into her big brown eyes.", puzzleText: "All he could think about was kissing her!", conclusionText: "Little did he know that she was his long lost sister."},
	];
	
	
	view.add(titleLabel);
	view.add(subTitleLabel);
	
	for(var i = 0; i < data.length; i++){
		view.add(CreateExample(data[i]));
	}
	
	
	
	function CreateExample(args){
		var exampleView = Ti.UI.createView({
			width: globals.platform.width-20,
			height: Ti.UI.SIZE,
			top: 10,
			layout: "vertical"
		});
		
		var titleLabel = Ti.UI.createLabel({
			text: args.title,
			font: globals.fonts.bold15,
			textAlign: "left",
			color: globals.colors.white,
			width: 90,
			left: 0,
			top: 0
		});
		
		exampleView.add(titleLabel);
		exampleView.add(CreateLine("Lead Text:", args.leadText));
		exampleView.add(CreateLine("Puzzle Text:", args.puzzleText));
		exampleView.add(CreateLine("Conclusion Text:", args.conclusionText));
		
		
		return exampleView;
	};
	
	
	function CreateLine(leftText, rightText){
		var lineView = Ti.UI.createView({
			width: globals.platform.width-20,
			height: Ti.UI.SIZE
		});
		
		var leftLabel = Ti.UI.createLabel({
			//backgroundColor: "green",
			text: leftText,
			font: globals.fonts.regular12,
			textAlign: "left",
			color: globals.colors.white,
			//width: 95,
			left: 0,
			top: 0
		});
		
		leftLabel.addEventListener("postlayout", function(){
			rightLabel.width = lineView.width - leftLabel.size.width - 2;
		});
		
		var rightLabel = Ti.UI.createLabel({
			//backgroundColor: "red",
			text: rightText,
			font: globals.fonts.regular12r,
			textAlign: "right",
			verticalAlign: "top",
			color: globals.colors.white,
			width: lineView.width - leftLabel.width - 5,
			right: 0,
			top: 0
		});
		
		lineView.add(leftLabel);
		lineView.add(rightLabel);
		
		return lineView;
	}
	
	
	return view;
}
