function Create(){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		top: globals.platform.height,
		backgroundColor: globals.colors.black
	});
	
	var navBar = CreateNavBar();
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: globals.platform.actualHeight-44,
		top: 44,
		contentWidth: view.width,
		contentHeight: "auto",
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	container.addEventListener("postlayout", function(){
		if(container.size.height < view.height-44) scrollView.contentHeight = globals.platform.actualHeight-44+1;
	});
	
	
	
	var data = [
		{name: "English"},
		{name: "Spanish"},
		{name: "Italian"},
		{name: "French"},
		{name: "German"},
		{name: "Portuguese"},
		{name: "Russian"},
		{name: "Japanese"},
		{name: "Dutch"}
	];
	
	for(var i = 0; i < data.length; i++){
		container.add(CreateRow(data[i]));
	}
	
	scrollView.add(container);
	
	view.add(navBar);
	view.add(scrollView);
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	Open();
	
	return view;
}

module.exports = Create;


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.black,
		width: globals.platform.width,
		height: 44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		touchEnabled: true,
		buttonID: "puzzleBookWinBackButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createLabel({
		text: "Pick a Language",
		font: globals.fonts.bold20,
		color: globals.colors.white,
		height: 30
	});
	
	
	view.add(title);
	view.add(closeButton);
	
	
	return view;
};


function CreateRow(data){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: 45,
		buttonID: "languageRow",
		touchEnabled: true
	});
	
	Common.MakeButton(view, true);
	
	view.addEventListener("singletap", function(){
		view.fireEvent("selected", {data: data});
	});
	
	var label = Ti.UI.createLabel({
		text: data.name,
		font: globals.fonts.bold15,
		color: globals.colors.white,
		left: 10
	});
	
	view.add(label);
	view.add(Ti.UI.createView({width:view.width, height:1, bottom: 0, backgroundColor: globals.colors.white}));
	
	
	return view;
};
