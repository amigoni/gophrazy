exports.createView = function(name){
	var TutorialController = require("controller/TutorialController");
	var tutorialController = TutorialController.CreateTutorialController(name);
	globals.tutorialOn = true;
	globals.tutorialButtonReady = true;
	globals.currentTutorial = tutorialController;
	var lastPosition = tutorialController.currentPosition;
	
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		backgroundImage: "/images/tutorialBg.png",
		backgroundTopCap: "30dp",
		backgroundLeftCap: "30dp",
		width: globals.platform.width-5,
		height: Ti.UI.SIZE,
		top: -300,
		opacity: 0,
		layout: "vertical",
		zIndex: 1000
	});
	
	view.addEventListener("singletap", function(){
		if(tutorialController.currentButtonID == "_next" && globals.tutorialButtonReady == true) NextStep(); 
	});
	
	view.addEventListener("postlayout", function(){
		//if(view.size.height < 100) view.height = 100;
	});
	
	
	var progressLabel = Ti.UI.createLabel({
		text: "1/"+tutorialController.steps.length,//"",
		//backgroundColor: "red",
		font: globals.fonts.regular10r,
		color: globals.colors.black,
		//width: (globals.platform.width-100).toString()+"dp",
		height: 12,
		right: 7,
		top: 4
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
	});
	
	
	var	monster = Titanium.UI.createImageView({
		image: "/images/monsterTutorial.png",
		width: 42,
		height: 53,
		left: 12
	});
	
	var container2 = Ti.UI.createView({
		width: globals.platform.width-80,
		height: Ti.UI.SIZE,
		left: 60,
		layout: "vertical"
	});
	
	var text = CreateDialogLabel(tutorialController.currentText);
	text.addEventListener("postlayout", function(){
		//if(text.size.height < 50) text.height
	});
	
	var nextButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 60,
		height: 25,
		right: 10,
		visible: tutorialController.currentButtonID == "_next" ? true : false,
		buttonID: "_next",
		//bottom: Ti.Platform.name == "iPhone OS" ? 0 : 5
		//bubbleParent: false
	});
	
	nextButton.add(Ti.UI.createLabel({
		//backgroundColor: "green",
		text: "Next",
		font: globals.fonts.bold20,
		color: globals.colors.textOpposite,
		height: 25,
		right: 0
	}));
	
	Common.MakeButton(nextButton);
	
	nextButton.addEventListener("singletap", function(){
		//NextStep();
	});
	
	
	container2.add(text);
	container2.add(Ti.UI.createView({height: 10}));
	
	container.add(monster);
	container.add(container2);
	
	//view.add(Ti.UI.createView({height: "5dp"}));
	view.add(progressLabel);
	view.add(container);
	view.add(nextButton);
	view.add(Ti.UI.createView({height: 10}));
	
	function Close(){
		nextButton.Close();
		Ti.App.removeEventListener("tutorialStep", NextStep);
		globals.tutorialOn = false;
		globals.tutorialButtonReady = null;
		globals.currentTutorial = null;
		globals.rootWin.remove(view);	
		//if(playInterval) clearInterval(playInterval);
	}
	
	function NextStep(){
		var nextStep = tutorialController.NextStep();
		globals.tutorialButtonReady = false;
		if (nextStep == "update"){
			view.animate(Ti.UI.createAnimation({
				opacity: 0,
				duration: 100
			}), function(){
				progressLabel.text = (tutorialController.currentStep+1)+"/"+tutorialController.steps.length;
				text.UpdateNoTimer(tutorialController.currentText);
				if (tutorialController.currentPosition != lastPosition) UpdatePosition();
				//text.Update(tutorialController.currentText);
				view.animate(Ti.UI.createAnimation({
					opacity: 1,
					duration: 150
				}));
			});
			
			
			if(tutorialController.currentButtonID == "_next") {
				nextButton.visible = true;
				nextButton.height = 25;
			}	
			else {
				nextButton.visible = false;
				nextButton.height = 0;
			}	
			
		}
		else{
			ExitAnimation();
			setTimeout(Close,250);
			Ti.App.fireEvent("tutorialComplete", name);
		}
	}
	
	function UpdatePosition(){
		if (tutorialController.currentPosition == "top") {
			view.bottom = null;
			view.top = 2;
		}
		else if (tutorialController.currentPosition == "center"){
			view.bottom = null;
			view.top = globals.platform.actualHeight/2-view.size.height/2-50;
		}
		else if (tutorialController.currentPosition == "bottom"){
			view.top = null;
			view.bottom = 2;
		}
		lastPosition = tutorialController.currentPosition;
	}
	
	function ExitAnimation(){
		view.animate(Ti.UI.createAnimation({
			opacity: 0,
			duration: 200
		}));
	}
	
	function EnterAnimation(){
		if(Ti.Platform.name == "iPhone OS"){
			view.animate(Ti.UI.createAnimation({
				opacity: 1,
				duration: 500
			}));
		}
		else view.opacity = 1; //Problems with Android animation
	}
	
	function MoveAnimation(){
		ExitAnimation();
		setTimeout(function(){
			UpdatePosition();
			EnterAnimation();
		},300);
	}
	
	view.NextStep = NextStep;
	view.EnterAnimation = EnterAnimation;
	
	Ti.App.addEventListener("tutorialStep", NextStep);
	
	UpdatePosition();
	//EnterAnimation();
	setTimeout(nextButton.StartGlow,500);
	
	//var playInterval = setInterval(function(){text.PlayInterval();},100);
	
	
	return view;
};


function CreateDialogLabel(text){
	var text = text; 
	var textCharacterCounter = 0;
	
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: globals.platform.width-80,
		height: Ti.UI.SIZE,
		left: 0,
		//top: "10dp",
		playEnabled: true
	});
	
	var label = Ti.UI.createLabel({
		text: text,//"",
		//backgroundColor: "green",
		font: globals.fonts.regular14,
		color: globals.colors.textOpposite,
		//width: view.width,
		height: Ti.UI.SIZE,
		verticalAlign:"center"
		//left: 0
	});
	
	label.addEventListener("postlayout", function(e){
		//var height = label.size.height;
		//if(height < 90) height = 90;
		//view.height = height;
	});
	
	view.add(label);
	
	function PlayInterval(){
		if (view.playEnabled == true){
			textCharacterCounter += 3;
			var authorText = "";
			var actualText = text.substring(0,textCharacterCounter);
			label.text = actualText;//'"'+actualText+'"';
			//label.text = text;
	
			if(textCharacterCounter >= text.length) {
				view.playEnabled = false;
				//clearInterval(textWritingInterval);
				label.fireEvent("finishedLine");
				globals.tutorialButtonReady = true;
			}
			//view.scrollToBottom();
		}
	}
	
	function Update(newText){
		text = newText;
		textCharacterCounter = 0;
		label.text = "";
		view.playEnabled = true;
	}
	
	function UpdateNoTimer(newText){
		label.opacity = 0;
		label.text = newText;
		globals.tutorialButtonReady = true;
		//view.scrollToBottom();
		setTimeout(function(){
			label.opacity = 1;
		},250);
	}
	
	view.UpdateNoTimer = UpdateNoTimer;
	view.Update = Update;
	view.PlayInterval = PlayInterval;
	
	
	return view;
}
