exports.createView = function(data){
	var view = Ti.UI.createView({
  		width: globals.platform.width,
  		height: "60dp",
  		top: "-60dp",
		zIndex: 100
  	});
  	
  	var background = Ti.UI.createView({
  		backgroundImage: "/images/tutorialBg.png",
		backgroundTopCap: "30dp",
  		width: globals.platform.width,
  		height: "60dp"
  	});
  	
  	var imagePath;
  	if (data.type == "hint" || data.type == "useHint") imagePath = "/images/hintIconLarge.png";
  	else if (data.type == "askFriend") imagePath = "/images/friendsIconLarge.png";
  	else if (data.type == "skip") imagePath = "/images/skipButtonDark.png";
  	else if (data.type == "achievement") imagePath = "/images/checkMarkRed.png";
  	
  	
  	var container = Ti.UI.createView({
  		//backgroundColor: "red",
  		width: globals.platform.width,
  		height: Ti.UI.SIZE,
  		//left: "15dp",
  		//layout: "horizontal"
  	});
  	
  	var icon = Ti.UI.createImageView({
  		image: imagePath,
  		width: "35dp",
  		left: "10dp"
  	});
  	
  	var rightContainer = Ti.UI.createView({
  		//backgroundColor: "red",
  		width: globals.platform.width-80,
  		left: "60dp",
  		height: Ti.UI.SIZE,
  		layout: "vertical"
  	});
  	
  	var topSign = Ti.UI.createImageView({
  		image: "/images/achievementCompleteSign.png",
  		width: 174,
  		height: 19
  	});
  	
  	var text;
  	if (data.type == "hint") text = "Good time to use a Hint?";
  	else if (data.type == "askFriend") text = "Stuck? Hand it to a friend.";
  	else if(data.type == "useHint") text = "Select any word to fix its position!";
  	else if(data.type == "skip") text = "You can always skip it for now!";
  	else if(data.type == "achievement") text = data.name;
  	else if(data.type == "freePuzzle") text = "You are out of Puzzles.\nEnjoy this free Puzzle of the day!\nOh! Fortune cookies are always free too!"; 
  	
  	var titleLabel = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: text,
		font:  data.type == "freePuzzle" ? globals.fonts.bold12 : globals.fonts.bold14,
		//minimumFontSize: "8dp",
		color: globals.colors.black,
		//width: globals.platform.width-70,
		height: data.type == "freePuzzle" ? "60dp" : "22dp",
		width: "240dp",
		textAlign: "center",
	});
	
	
	if(data.type == "achievement") rightContainer.add(topSign);
	rightContainer.add(titleLabel);
	
	container.add(icon);
	container.add(rightContainer); 
	
	background.add(container);
	
	view.add(background);
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		view.fireEvent("closed");
	}
	
	function AppearAnimation(){
		setTimeout(function(){
	  		/*
	  		Animator.animate(view,{
		  		easing : Animator["EXP_OUT"],
		  		duration: globals.style.animationSpeeds.speed1,
		  		top: 0
	  		});
	  		*/
	  		view.animate(Ti.UI.createAnimation({
				duration: globals.style.animationSpeeds.speed1,
				top: "0dp",
				curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
			}));
	  	},200);
	}
	
	function DisappearAnimation(){
		/*
		Animator.animate(view,{
	  		easing : Animator["EXP_IN"],
	  		duration: globals.style.animationSpeeds.speed1,
	  		top: -60
	  	},Close);
	  	*/
	  	view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top: "-60dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}), Close);
	}
	
	function NotificationAction(e){
		if(e.action == "close") DisappearAnimation();
	}
	
  	
	view.AppearAnimation = AppearAnimation;
	view.DisappearAnimation = DisappearAnimation;
	view.Open = Open;  
  
  	if (data.type != "useHint"){
  		setTimeout(function(){
  			DisappearAnimation();
  		},4000);
  	}
  	
  	Ti.App.addEventListener("topNotificationsPopUpAction", NotificationAction);
  	
  	
  	return view;
};