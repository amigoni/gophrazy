exports.createView = function(data){
	var view = Titanium.UI.createView({  
	    backgroundColor:"black",
	    height: globals.platform.actualHeight,
	    width: globals.platform.width,
	    bubbleParent: false,
	    zIndex: 100,
	    opacity: 0 
	});
	
	var container = Ti.UI.createView({
		width: view.width-40,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var textLabel = Ti.UI.createLabel({
		text: data.senderMessageText,
		color: globals.colors.white,
		textAlign: "center",
		font: globals.fonts.regular15,
		//minimumFontSize: 10,
		width: view.width - 40,
		//left: 20,
	});
	
	var senderLabel = Ti.UI.createLabel({
		text: data.senderName,
		color: globals.colors.white,
		textAlign: "right",
		font: globals.fonts.regular13r,
		//minimumFontSize: 10,
		width: view.width-40,
		right: 20,
		top: 20
	});
	
	var okButton =  Ti.UI.createLabel({
		text: "ok",
		color: globals.colors.white,
		textAlign: "center",
		font: globals.fonts.bold35,
		//minimumFontSize: 10,
		width: 80,
		height: 40,
		//right: 20,
		top: 30,
		buttonID: "noteDisplayPopUpOkButton",
		touchEnabled: false
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		Close();
	});
	
	container.add(textLabel);
	container.add(senderLabel);
	container.add(okButton);
	
	view.add(container);
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){
			view.animate(Ti.UI.createAnimation({
				duration: globals.style.animationSpeeds.speed1,
				opacity: 1
			}));
		},10);
	}
	
	function DisappearAnimation(){
		view.animate(Ti.UI.createAnimation({
				duration: 150,
				opacity: 0
		}),function(){view.fireEvent("closed");});
	}
	
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};
