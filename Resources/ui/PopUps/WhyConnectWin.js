exports.loadWin = function(){
	var data = [
		{title: "Who's the best?", description:"See how many puzzles your friends have solved and compete for the top spot.", icon:"podiumIcon.png"},
		{title: "Send eachother Puzzles", description:"Found a great puzzle that you would like to send to someone? You can!", icon:"sendPuzzleIcon.png"},
		{title: "Compose You Own Puzzles", description:"Compose your own puzzles and send them to your friends. Fun!!!", icon:"composeIcon.png"}
	];
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//-(globals.platform.actualHeight),
	  	zIndex: 99,
	});
	
	var navBar = CreateNavBar();
	
	navBar.addEventListener("closeWin", function(){
		Close();
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: view.height-40,
		contentWidth: view.width,
		contentHeight: "auto",
		top: 40
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	var titleSign = Ti.UI.createLabel({
		text: "Want to play with your Friends?",
		font: globals.fonts.bold30,
		textAlign: "center",
		//minimumFontSize: "12dp",
		width: view.width-40,
		color: globals.colors.darkBrown,
		top:0
	});
	
	var bottomContainer = Ti.UI.createView({
		width: view.width-40,
		height: Ti.UI.SIZE,
		top: 10,
		layout: "horizontal"
	});
	
	bottomContainer.add(Ti.UI.createImageView({
		image: "/images/friendsIconFacebook.png",
		width: 40,
		height: 37,
		touchEnabled: false,
		bubbleParent: false
	}));
	
	bottomContainer.add(Ti.UI.createLabel({
		text: "We will never post anything automatically.",
		font: globals.fonts.bold14,
		//minimumFontSize: "12dp",
		width: bottomContainer.width-70,
		color: globals.colors.red,
		textAlign: "center",
		//top: 20,
		left: 10
	}));
	
	
	container.add(titleSign);
	container.add(Ti.UI.createView({height:20}));
	for (var i = 0; i < data.length; i++){
		var row = CreateRow(data[i]);
		container.add(row);
	}
	container.add(bottomContainer);
	container.add(Ti.UI.createView({height:20}));
	
	var okButton = Ti.UI.createImageView({
		image:"/images/okButton.png",
		width: 59,
		height: 38,
		bottom: 15,
		buttonID: "whyConnectWinOkButton"
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		var ServerData = require("model/ServerData");
		ServerData.SignUpFacebook(Close);
	});
		
	
	scrollView.add(container);
	
	view.add(navBar);
	view.add(scrollView);
	view.add(okButton);
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		view.fireEvent("close");
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view,null);},1200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view, function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		//backgroundColor: "#CD4D39",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID: "whyConnectWinBackButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	
	view.add(closeButton);
	
	
	return view;
};


function CreateRow(data){
	var view = Ti.UI.createView({
		width: globals.platform.width-30,
		height: 55,
		touchEnabled: false
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/"+data.icon,
		width: 40,
		left: 0
	});
	
	var label1 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: data.title,
		font: globals.fonts.bold20,
		minimumFontSize: "12dp",
		color: globals.colors.darkBrown,
		left: 55,
		width: view.width-55,
		top: 5
	});
	
	var label2 = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: data.description,
		font: globals.fonts.regular11,
		//minimumFontSize: 12,
		color: globals.colors.darkBrown,
		left: 55,
		width: view.width-55,
		top: 25
	});
	
	view.add(icon);
	view.add(label1);
	view.add(label2);
	
	
	return view;
}
