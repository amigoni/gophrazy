exports.createView = function(phraseData){
	var FriendsController = require("controller/FriendsController");
	var ServerData = require("model/ServerData");
	var TableViewsController = require("ui/Common/TableViewsController");
	var MessagesController = require("controller/MessagesController");
	var data = FriendsController.GetMessageFriendsData();
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: (globals.platform.actualHeight),
	  	zIndex: 100
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	view.addEventListener("selected", function(e){
		if(e.rowData.installed == true){
			if(MessagesController.CheckIfMessageWasAlreadySent(e.rowData.facebookID, phraseData.text) == true){
				var dialog = Ti.UI.createAlertDialog({
				    cancel: 1,
				    buttonNames: ['Yes', 'No'],
				    message: 'You already sent this puzzle to this person. Wanna send it again?',
				    title: 'Sent Before'
				  });
				  dialog.addEventListener('click', function(e1){
				  		if(e1.index == 0) OpenNote();
				  });
				  dialog.show();
			}
			else OpenNote();
			
			function OpenNote(){
				globals.rootWin.OpenWindow("noteComposeWin", function(senderMessageText){
					e.rowData.senderMessageText = senderMessageText;
					globals.rootWin.OpenWindow("networkBar",{bgColor: "#333333", text:"Sending Message"});
					ServerData.SendCustomPuzzleMessage(e.rowData.facebookID, phraseData.text, phraseData.author, e.rowData.senderMessageText);
					Close();
				});
			}
		}
		else ServerData.InviteFacebookFriends([e.rowData.facebookID]);
	});
	
	var navBar = CreateNavbar();
	var tableViewsController = TableViewsController.createView();
	
	tableViewsController.addEventListener("openLetter", function(e){
		tableViewsController.Add(CreateLetterTableView(e.rowData));
	});
	
	var mainTableView = CreateMainTableView(data);
	
	tableViewsController.Add(mainTableView);
	
	view.add(navBar);
	view.add(tableViewsController);
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		navBar.Close();
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavbar (){
	var view = Ti.UI.createView({
		backgroundColor: "##D5C7A3",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID: "friendPickerPopUpCloseButton",
		touchEnabled: true
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 34,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/friendsIconLarge.png",
		width: 37,
		height: 34
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/sendPuzzleSign.png",
		width: 122,
		height: 22,
		left: 7
	}));
	
	
	view.add(title);
	view.add(closeButton);
	
	function Close(){
		
	}
	
	view.Close = Close;
	
	
	return view;
}


function CreateMainTableView(data){
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 0,
		left: 0,
		separatorColor: "transparent",
		separatorStyle: false,
		opacity: 1
		//left: type == "main" ? 0 : globals.platform.width,
	});
	
	var tableData = [];
	
	if(data.installed.length > 0){
		tableData.push(CreateHeaderRow("Playing GoPhrazy"));
		for(var i = 0; i < data.installed.length; i++){
			if (data.installed[i].facebookID != globals.gameData.userFacebookID){
				var row = CreatePersonRow(data.installed[i]);
				tableData.push(row);
			}
		}
	}
	
	
	tableData.push(CreateHeaderRow("Invite Facebook Friends"));
	for(var i = 0; i < data.letters.length; i++){
		var row = CreateLetterRow(data.letters[i]);
		tableData.push(row);
	}
	
	tableView.data = tableData;
	
	function MoveLeft (){
		/*
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
		*/
		tableView.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: tableView.left-(globals.platform.width),
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
	}
	
	function MoveRight (){
		/*
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left+globals.platform.width,
			easing:Animator['EXP_IN']
		});
		*/
		
		tableView.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: tableView.left,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
	}
	
	tableView.MoveLeft = MoveLeft;
	tableView.MoveRight = MoveRight;
	
	
	return tableView;
};


function CreateLetterTableView(data){
	//Ti.API.info(data);
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
		top: 0,
		left: globals.platform.width,
		separatorColor: "transparent",
		separatorStyle: false,
		opacity: 1
		//left: type == "main" ? 0 : globals.platform.width,
	});
	
	var tableData = [];
	tableData.push(CreateNavRow());
	for(var i = 0; i < data.friends.length; i++){
		if (data.friends[i].facebookID != globals.gameData.userFacebookID){
			var row = CreatePersonRow(data.friends[i]);
			tableData.push(row);
		}
	}
	
	tableView.data = tableData;
	
	function MoveLeft (){
		/*
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left-(globals.platform.width),
			easing:Animator['EXP_IN']
		});
		*/
		
		tableView.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: tableView.left-(globals.platform.width),
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
	}
	
	function MoveRight (){
		/*
		Animator.animate(tableView,{
			duration: 500,
			left: tableView.left+globals.platform.width,
			easing:Animator['EXP_IN']
		});
		*/
		
		tableView.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: tableView.left+globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
	}
	
	tableView.MoveLeft = MoveLeft;
	tableView.MoveRight = MoveRight;
	
	
	return tableView;
};


function CreatePersonRow(rowData){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 40,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		touchEnabled: true
	});
	
	view.addEventListener("singletap", function(){
		if(view.touchEnabled == true) setTimeout(function(){view.fireEvent("selected", {rowData: rowData});},200);
	});
	
	//Here to capture the touch event. Can't figure out why the row doesn't catch it.
	var bg = Ti.UI.createView({
		width: view.width,
		height: view.height
	});
	
	var nameLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.name,
		font: globals.fonts.bold15,
		color: globals.colors.darkBrown,
		textAlign: "left",
		width: globals.platform.width-110,
		height: 20,
		left: 10,
		touchEnabled: false
	});
	
	/*
	var recruitButton = Ti.UI.createView({
		backgroundImage: "/images/recruitButton.png",
		width: 94,
		height: 36, 
		right: 5
	});
	
	Common.MakeButton(recruitButton);
	
	recruitButton.addEventListener("singletap", function(e){
		//view.fireEvent("recruit",{id: rowData.id});
		var FriendsController = require("controller/FriendsController");
		Ti.API.info(rowData);
		FriendsController.AddFriendToTeam(rowData);
		view.fireEvent("closeWin");
	});
	*/
	
	var inviteLabel = Ti.UI.createLabel({
		text: "invite",
		color: globals.colors.darkBrown,
		textAlign: "left",
		font: globals.fonts.boldMediumSmall,
		minimumFontSize: 10,
		right: 30,
		buttonID: "friendPickerInviteLabel"
	});
	
	Common.MakeButton(inviteLabel);
	
	inviteLabel.addEventListener("singletap", function(){
		//alert("invite");
	});

	
	view.add(bg);
	view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1,bottom: 0, touchEnabled: false}));
	view.add(nameLabel);
	//if(rowData.installed == true) view.add(recruitButton);
	if(rowData.installed == false) view.add(inviteLabel);
	
	
	return view;
}


function CreateHeaderRow(name){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 40,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE,
		touchEnabled: false
	});
	
	var label = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: name,
		font: globals.fonts.bold15,
		//minimumFontSize: 12,
		color: globals.colors.mediumBrown,
		width: Ti.UI.SIZE,
		//bottom: 5
	});
	
	view.add(label);
	
	return view;
}
		
	
function CreateLetterRow(rowData){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 40,
		selectionStyle: Titanium.UI.iPhone.TableViewCellSelectionStyle.GRAY,
		touchEnabled: true
	});
	
	view.addEventListener("singletap", function(){
		if(view.touchEnabled == true) setTimeout(function(){view.fireEvent("openLetter", {rowData: rowData});},200);
	});
	
	var nameLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.name,
		font: globals.fonts.bold15,
		color: globals.colors.darkBrown,
		textAlign: "left",
		width: globals.platform.width-110,
		height: 20,
		left: 10
	});
	
	var numberText = "("+rowData.friends.length+" friends)";
	if(rowData.friends.length == 1) numberText = "("+rowData.friends.length+" friends)";
	var numberLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: numberText,
		font: globals.fonts.regular10r,
		color: globals.colors.darkBrown,
		textAlign: "right",
		//width: globals.platform.width-110,
		height: 20,
		right: 10
	});
	
	view.add(Ti.UI.createView({backgroundColor: globals.colors.darkBrown, width:view.width, height:1,bottom: 0, touchEnabled: false}));
	view.add(nameLabel);
	view.add(numberLabel);
	
	
	return view;
}	


function CreateNavRow(){
	var view = Ti.UI.createTableViewRow({
		//backgroundColor: "red",
		width: globals.platform.width,
		height: 40,
		selectionStyle: false
	});
	
	var backButton = Ti.UI.createView({
		backgroundImage: "/images/backButton.png",
		width: 30,
		height: 26,
		left: 10,
		//top: 20
	});
	
	Common.MakeButton(backButton);
	
	backButton.addEventListener("singletap", function(){
		setTimeout(function(){view.fireEvent("back");},200);
	});
	
		
	view.add(backButton);

	
	return view;
}

