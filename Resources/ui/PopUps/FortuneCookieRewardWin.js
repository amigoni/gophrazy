exports.loadWin = function(data){
	var DailyChallengeController = require("controller/DailyChallengeController");
	var fortuneCookieData = data.fortuneCookieData;
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: -(globals.platform.actualHeight),
	  	zIndex: 101,
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var navBar = CreateNavBar();
	var meterView = CreateFortuneMeterView(fortuneCookieData.inStock);
	meterView.addEventListener("meterAnimationComplete", function(){
		weekView.StartAnimation();
	});
	
	var weekView = CreateThisWeekView(fortuneCookieData);
	weekView.addEventListener("weekAnimationComplete", function(){
		okButton.visible = true;
	});
	
	var rewardsView = CreateRewardsView(data.bonusData);
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: 59,
		height: 38,
		bottom: 10,
		visible: false,
		buttonID: "fortuneCookieRewardWinOkButton",
		touchEnabled: false
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		Close();
	});
	
	view.add(navBar);
	container.add(meterView);
	container.add(weekView);
	container.add(rewardsView);
	view.add(okButton);
	
	view.add(container);
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
		OpenRewardsPopUps();
		DailyChallengeController.ProcessFortuneCookie();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Animator.animate(view,{
			duration: 500,
			top: 0,
			easing:Animator['EXP_OUT']
		});},200);
	}
	
	function DisappearAnimation(){
		Animator.animate(view,{
			duration: 500,
			top: -(globals.platform.actualHeight),
			easing:Animator['EXP_IN']
		},function(){
			view.fireEvent("closed");
			}
		);
	}
	
	function OpenRewardsPopUps(){
		var TallPopUp = require("ui/PopUps/TallPopUp");
		
		/*
		var tallPopUpData = {
			type: "weekComplete",
			subtitle: "",
			bonusData: data.bonusData
		};
		
		globals.rootWin.popUpStack.Add({action: "openWindow", name: "tallPopUp", data:tallPopUpData});
		*/
		if (data.monster != null) globals.rootWin.popUpStack.Add({action: "openWindow", name: "monsterDetailWin", data: data.monster});
		globals.rootWin.popUpStack.Next();
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		//backgroundColor: "#CD4D39",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 60,
		height: 44,
		right: 0
	});
	
	var titleSign = Ti.UI.createImageView({
		image: "/images/fortuneCookiesSign.png"
	});
	
	view.add(titleSign);
	
	
	return view;
};


function CreateFortuneMeterView(quantity){
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0,
		bubbleParent: false
	});
	
	var animationCompleteCounter = 0;
	view.addEventListener("animationComplete", function(e){
		animationCompleteCounter++;
		if (animationCompleteCounter >= quantity){view.fireEvent("meterAnimationComplete");};
	});
	
	var topContainer = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal"
	});
	
	var leftCookie = Ti.UI.createImageView({
		image: "/images/fortuneCookieIconLarge.png",
		height: 40
	});
	
	var barContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 44,
		layout: "horizontal",
		left: 0,
		bottom: 10
	});
	
	var barConfig = [
		//{height: 20, color: "#1ABBB4"},
		//{height: 22, color: "#00B200"},
		//{height: 24, color: "#3BB878"},
		{height: 26, color: "#7CC576"},
		{height: 28, color: "#ACD372"},
		{height: 30, color: "#FFF467"},
		{height: 32, color: "#FBAF5C"},
		{height: 34, color: "#F68E55"},
		{height: 36, color: "#F26C4F"},
		{height: 38, color: "red"},
	];
	
	for (var i = 0; i < barConfig.length; i++){
		var isEmpty = false;
		if (i+1 > quantity) isEmpty = true;
		barContainer.add(CreateBar(barConfig[i].height, barConfig[i].color, isEmpty,i));
	}
	
	var rightLabel = Ti.UI.createLabel({
		text: quantity,
		font: globals.fonts.bold35,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		left: 10
	});
	
	topContainer.add(leftCookie);
	topContainer.add(barContainer);
	topContainer.add(rightLabel);
	
	
	var bottomLabel = Ti.UI.createLabel({
		text: "Complete the Week or get to 7 to get a Monster.",
		font: globals.fonts.bold12,
		minimumFontSize: 10,
		color: globals.colors.mediumBrown,
		top: -5
	});
	
	view.add(topContainer);
	view.add(bottomLabel);
	
	
	function CreateBar (height, color, isEmpty,i){
		var bar = Ti.UI.createView({
			borderColor: globals.colors.darkBrown,
			borderWidth:2,
			borderRadius: 6,
			backgroundColor: "transparent" ,
			width: 12,
			height: height,
			left: 1,
			bottom: 0,
			transform: Ti.UI.create2DMatrix().scale(0.8,0.8)
		});
		
		if(isEmpty == false){
			setTimeout(function(){
				bar.transform = Ti.UI.create2DMatrix().scale(0.8,0.8);
				bar.backgroundColor = color;
				
				bar.animate(Ti.UI.createAnimation({
					duration: globals.style.animationSpeeds.speed1,
					transform: Ti.UI.create2DMatrix().scale(1,1),
					opacity: 1,
					curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
				}),function(){bar.fireEvent("animationComplete");});
				
				/*
				Animator.animate(bar,{
					duration: globals.style.animationSpeeds.speed1,
					transform: Ti.UI.create2DMatrix().scale(1,1),
					opacity: 1,
					easing:Animator['BOUNCE_OUT']
					},function(){bar.fireEvent("animationComplete");}
				);
				*/
			}, 1000+i*250);
		}
		
		
		return bar;
	}
	
	return view;
};


function CreateThisWeekView(data){
	var today = clock.getNow().day()-1;
	if (today == -1) today = 6;
	
	var cookies = [];
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 20,
		bubbleParent: false
	});
	
	var label1 = Ti.UI.createLabel({
		text: "This Week",
		font: globals.fonts.bold25,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
	});
	
	var label2 = Ti.UI.createLabel({
		text: "Completing the week gives you extra rewards.",
		font: globals.fonts.bold12,
		minimumFontSize: 10,
		color: globals.colors.mediumBrown,
	});
	
	var daysContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	var topContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal"
	});
	
	var bottomContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal"
	});
	
	for (var i = 0; i < 7; i++){
		var day = data.week[i];
		var isToday = false;
		if (i == today) isToday = true;
		var cookie = CreateFortuneCookieIcon(day.day, day.complete, isToday, i);
		if (i < 4) topContainer.add(cookie);
		else bottomContainer.add(cookie);
		cookies.push(cookie);
	}
	
	daysContainer.add(topContainer);
	daysContainer.add(bottomContainer);
	
	view.add(label1);
	view.add(label2);
	view.add(daysContainer);
	
	
	function StartAnimation(){
		var animationStack = [];
		for (var i = 0; i < 7; i++){
			var day = data.week[i];
			if (day.complete == true) animationStack.push(cookies[i]);
		}	
		
		for (var i = 0; i < animationStack.length; i++){
			var cookie = animationStack[i];
			var last = false;
			if (i == animationStack.length-1) last = true;
			cookie.EnterAnimation(250+250*i,last);
		}
		animationStack = null;
	}
	
	view.StartAnimation = StartAnimation;
	
	
	return view;
}


function CreateFortuneCookieIcon(dayOfTheWeek,complete,isToday,i){
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: 75,
		height: 75,
		dayOfTheWeek: dayOfTheWeek,
		complete: complete,
		isToday: isToday,
		i: i
	});
	
	var iconEmboss = Ti.UI.createImageView({
		image: "/images/fortuneCookieIconEmboss.png",
		//width: 44,
		height: 40,
		top: 7
	});
	
	var icon = Ti.UI.createImageView({
		image:  "/images/fortuneCookieIconLarge.png",
		//width: 44,
		height: 40,
		top: 7,
		transform: Ti.UI.create2DMatrix().scale(0.4,0.4),
		opacity: 0,
		visible: complete 
	});
	
	var label1 = Ti.UI.createLabel({
		text: dayOfTheWeek,
		font: globals.fonts.bold13,
		minimumFontSize: 10,
		color: globals.colors.darkBrown,
		bottom: 13
	});
	
	var label2 = Ti.UI.createLabel({
		text: 'Today',
		font: globals.fonts.bold13,
		minimumFontSize: 10,
		color: globals.colors.mediumBrown,
		bottom: 0,
		visible: isToday
	});
	
	view.add(iconEmboss);
	view.add(icon);
	view.add(label1);
	view.add(label2);
	
	function EnterAnimation(delay, last){
		setTimeout(function(){
			Animator.animate(icon,{
				duration: 500,
				transform: Ti.UI.create2DMatrix().scale(1,1),
				opacity: 1,
				easing:Animator['BOUNCE_OUT']
			},function(){ if (last == true) view.fireEvent("weekAnimationComplete");}
			);
		}, delay);
	}
	
	view.EnterAnimation = EnterAnimation;
	
	 
	return view;
}


function CreateRewardsView(data){
	var ScoreDisplay = require("ui/GameWinElements/ScoreDisplay");
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: 40,
		top: 10,
		layout: "horizontal"
	});
	
	for (var i = 0; i < data.length; i++){
		var reward = data[i];
		view.add(ScoreDisplay.CreateRewardContainer(reward.type, reward.increment, 0));
		if (i > 0) view.add(Ti.UI.createView({height: 10,width: 50}));
	}
	
	return view;
}
