function Create(data){
	var PopUpBackground = require("/ui/Common/PopUpBackground");
	var view = new PopUpBackground(false);
	view.container.width = 320;
	view.popUpBg.width = 300;
	view.container.height = 400;
	view.popUpBg.height = Ti.UI.SIZE;
	view.popUpBg.layout = "vertical";
	
	var titleSign = Ti.UI.createView({
		backgroundImage:"/images/whatsnewsign.png",
		width: 146,
		height: 27,
		top: 10
	});
	
	var scrollView = Ti.UI.createScrollView({
		top: 10,
		width: 270,
		height: Ti.UI.SIZE,
		contentWidth: 270,
		contentHeight: 'auto'
	});
	
	var contentContainer = Ti.UI.createView({
		top: 0,
		width: 270,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	contentContainer.addEventListener("postlayout", function(){
		if(contentContainer.size.height > 400) scrollView.height = 400;
	});
	
	if(data.updates != null) contentContainer.add(UpdatesRow(data.updates));
	if(data.puzzlePost != null) contentContainer.add(NormalRow(data.puzzlePost));
	if(data.friends != null) contentContainer.add(NormalRow(data.friends));
	
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: 44,
		height: 29,
		top: 10,
		buttonID: "smallSquarePopUpOkButton",
		bubbleParent: true
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		view.Close();
	});
	
	
	view.popUpBg.add(titleSign);
	view.popUpBg.add(contentContainer);
	view.popUpBg.add(okButton);
	view.popUpBg.add(Ti.UI.createView({height: 20}));
	
	
	return view;	
}


function NormalRow(data){
	var view = Ti.UI.createView({
		width: 270,
		height: Ti.UI.SIZE,
		top: 10
	});
	
	var icon = Ti.UI.createView(data.iconData);
	icon.left = 0;
	
	var rightContainer = Ti.UI.createView({
		width: view.width - 50,
		height: Ti.UI.SIZE,
		left: 50,
		layout: "vertical"
	});
	
	var title = Ti.UI.createLabel({
		text: data.titleText,
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.bold15,
		left: 0,
		top: 0,
		height: 20
	});
	
	var subtitle = Ti.UI.createLabel({
		text: data.subtitleText,
		color: globals.colors.black,
		textAlign: "left",
		verticalAlign: "top",
		font: globals.fonts.regular12r,
		left: 0,
		top: 0,
		width: view.width-40,
		height: Ti.UI.SIZE,
	});
	
	rightContainer.add(title);
	rightContainer.add(subtitle);
	
	view.add(icon);
	view.add(rightContainer);

	
	
	return view;
};


function UpdatesRow(data){
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: 270,
		height: Ti.UI.SIZE,
		layout:"vertical"
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE
	});
	
	var icon = Ti.UI.createView({
		backgroundImage:"/images/newSign.png",
		width: 34,
		height: 25,
		left: 0,
		top: 0
	});
	
	var title = Ti.UI.createLabel({
		text: "Updates",
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.bold15,
		left: 40,
		top: 0,
		height: 20,
	});
	
	container.add(icon);
	container.add(title);
	
	
	var scrollView = Ti.UI.createScrollView({
		//backgroundColor: "red",
		width: view.width,
		height: Ti.UI.SIZE,
		contentWidth: view.width,
		contentHeight: "auto",
		top: 0
	});
	
	
	var subtitle = Ti.UI.createLabel({
		text: data.subtitleText,
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.regular12r,
		width: view.width
	});
	
	var addedMoreLabel = false;
	subtitle.addEventListener("postlayout", function(){
		if(scrollView.size.height > 123){
			scrollView.height = 123;
			scrollView.showVerticalScrollIndicator  = true;
			if(addedMoreLabel == false){
				addedMoreLabel = true;
				view.add(Ti.UI.createLabel({
					text: "scroll for more",
					color: globals.colors.black,
					textAlign: "center",
					font: globals.fonts.regular12,
					width: view.width,
					top: 5
				}));
			}
		}
	});
	
	scrollView.add(subtitle);
	
	view.add(container);
	view.add(scrollView);
	
	
	return view;
}

module.exports = Create;
