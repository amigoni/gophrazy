exports.loadWin = function(){
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//-(globals.platform.actualHeight),
	  	zIndex: 1000,
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		Close();
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: (view.height-50).toString()+"dp",
		contentWidth: view.width,
		contentHeight: "auto",
		top: "50dp"
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: '0dp'
	});
	
	var creditsTitle = Ti.UI.createLabel({
		text: "Thank you!",
		font: globals.fonts.bold20,
		minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	});
	
	var creditsLabel = Ti.UI.createLabel({
		text: "Super special thanks to family and friends who have been crucial in testing and advising me during development.\n\nAngela, Cri, Jeff, Tatiana, Paola, Stefano, Antonella, Ginevra, Guendalina, Olivia.\n\nLea, for coming up with the name.\n\nThe great guys at Urustar, for the amazing feedback and suggestions.\n\nRealtime Networks, João Parreira in particular, for the great support and backend platform.\n\nSteve Rice Music, for the great soundtrack.\n\nDaniele Bertinelli for the sounds.\n\n\nAll quotes are for educational purposes only.",
		font: globals.fonts.regular13,
		width: globals.platform.width - 40,
		textAlign: "left",
		//minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	});
	
	container.add(creditsTitle);
	container.add(creditsLabel);
	
	
	scrollView.add(container);
	
	view.add(navBar);
	view.add(scrollView);
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view,null);},1200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		//backgroundColor: "#CD4D39",
		width: globals.platform.width,
		height: '44dp',
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: '44dp',
		height: '44dp',
		right: '0dp',
		buttonID: "creditsWinBackButton",
		touchEnabled: true
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: '12dp',
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var titleSign = Ti.UI.createImageView({
		image: "/images/creditsSign.png",
		width: 93,
		height: 26
	});
	
	view.add(titleSign);
	view.add(closeButton);
	
	
	return view;
};