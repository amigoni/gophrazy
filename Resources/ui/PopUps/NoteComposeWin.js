exports.createView = function(callback){
	var ServerData = require("model/ServerData");
	var letterLimit = 90;
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: (globals.platform.actualHeight),
	  	zIndex: 100
	});
	
	view.addEventListener("closeWin", function(){
		Close();
	});
	
	view.addEventListener("send", function(){
		callback(textArea.value);
		Close();
	});
	
	
	var navBar = CreateNavbar();
	
	
	var textArea = Ti.UI.createTextArea({
		//backgroundColor: "red",
		borderColor: globals.colors.mediumBrown,
		font: globals.fonts.regular13r,
		width: view.width-20,
		height: 100,
		//hintText: "type a short message for your friend",
		top: 54
	});
	
	textArea.addEventListener("change", function(e){
		if(e.value.length > 90) {
			textArea.value = e.value.substring(0,90);
			if(globals.gameData.vibrateOn == true) Ti.Media.vibrate([ 0, 200, 200, 200 ]);
		}	
		countLabel.text = letterLimit-textArea.value.length;
		if(textArea.value.length > 0) hintLabel.visible = false;
		else hintLabel.visible = true;
	});
	
	var hintLabel = Ti.UI.createLabel({
		text: "type a short message or just presss Send",
		font: globals.fonts.regular13r,
		color: "#666666",
		top: 8,
		left: 5
	});
	
	textArea.add(hintLabel);
	
	
	var countLabel = Ti.UI.createLabel({
		text: letterLimit,
		font: globals.fonts.bold25,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		right: 10,
		top: 44+10+100+10.
	});
	
	
	view.add(navBar);
	view.add(textArea);
	view.add(countLabel);
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		navBar.Close();
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		Common.WindowOpenAnimation(view,function(){textArea.focus();});
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	
	view.Open = Open;
	view.Close = Close;
	
	
	return view;
};


function CreateNavbar (){
	var view = Ti.UI.createView({
		backgroundColor: "##D5C7A3",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	
	var sendButton = Ti.UI.createLabel({
		text: "Send",
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		textAlign: "center",
		width: 70,
		height: 44,
		left: 0,
		buttonID: "noteComposeWinSendButton",
		touchEnabled: true
	});
	
	Common.MakeButton(sendButton);
	
	sendButton.addEventListener("singletap", function(){
		view.fireEvent("send");
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0
	});
	
	closeButton.addEventListener("singletap", function(){
		view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: "X",
		font: globals.fonts.bold30,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createLabel({
		text: "Message",
		textAlign: "center",
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		color: globals.colors.darkBrown,
		width: view.width - 120
	});
	/*
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 34,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/friendsIconLarge.png",
		width: 37,
		height: 34
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/sendPuzzleSign.png",
		width: 122,
		height: 22,
		left: 7
	}));
	*/
	
	view.add(sendButton);
	view.add(title);
	view.add(closeButton);
	
	function Close(){
		
	}
	
	view.Close = Close;
	
	
	return view;
}