exports.CreatePopUp = function(data, callback){
	var type = data.type;
	var heightSubtract = 0;
	var opened = false;
	if(type == "energy") heightSubtract = 70;
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		bubbleParent: false,
		zIndex: 100,
		top: "0dp"
	});
	
	var background = Ti.UI.createView({
		backgroundColor: "black",
		width: view.width,
		height: view.height,
		opacity: 0
	});
	
	var container = Ti.UI.createView({
		width: 310,
		height: (320 - heightSubtract),
		top: globals.platform.actualHeight
	});
	
	var popUpBg = Ti.UI.createView({
		//backgroundImage:'"+globals.imagesPath+"/images/popUpBg.png',
		//backgroundTopCap: 100,
		borderColor: "black",
		backgroundColor: globals.colors.white,
		borderWidth: "2dp",
		borderRadius: "20dp",
		width: "290dp",
		height: (290-heightSubtract).toString()+"dp",
		//top: "10dp"
	});
	
	var panel;
	if(type.indexOf("ability") > -1) panel = CreateUnlockPanel(data);
	else if(type.indexOf("ability") > -1 == false && type != "pass") panel = CreateStorePanel(type);
	//else if(type.indexOf("ability") > -1 == false && type == "pass") panel = CreatePassPanel(callback);
	
	panel.addEventListener("close", function(){
		Close();
	});
	
	popUpBg.add(panel);
	
	var closeX = Ti.UI.createView({
		backgroundImage:'/images/closeX.png',
		width: "28dp",
		height: "29dp",
		top: "7dp",
		right: "5dp"
	});
	Common.MakeButton(closeX);
	
	closeX.addEventListener("singletap", function(){
		Close();
	});
	
	container.add(popUpBg);
	container.add(closeX);
	
	
	view.add(background);
	view.add(container);
	
	
	function Close(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			easing: Animator["EXP_IN"]
		});
		
		Animator.animate(background,{
			duration: 500,
			opacity: 0,
			}, function(){ panel.Close(); view.fireEvent("closed");}
		);
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
		
		background.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity: 0
		}),function(){panel.Close(); view.fireEvent("closed");});
	}
	
	setTimeout(function(){
		if(opened == false){
			/*
			Animator.animate(container,{
				duration: 500,
				top: ((globals.platform.actualHeightNumber-container.size.height)/2).toString()+"dp",
				easing: Animator["EXP_OUT"]
			});
			
			Animator.animate(background,{
				duration: 500,
				opacity: .75,
			})
			*/

			container.animate(Ti.UI.createAnimation({
				duration: globals.style.animationSpeeds.speed1,
				top: ((globals.platform.actualHeightNumber-container.size.height)/2).toString()+"dp",
				curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
			}));
			
			background.animate(Ti.UI.createAnimation({
				duration: 500,
				opacity: 0.75
			}));
		}
	},100);
	
	
	return view;
};



function CreateUnlockPanel(item){
	var unlockedData = item.bonusData[0];
	
	var view = Ti.UI.createView({
		width: "290dp",
		height: "290dp",
		//layout: "vertical"
	}); 
	
	var unlockedSign = Ti.UI.createImageView({
		image: "/images/unlockedSign.png",
		top: "20dp",
	});
	
	/*
	var subtitleLabel =  Ti.UI.createLabel({
		text: "30 more Puzzles",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		top: 10
	});
	*/
	
	var container = Ti.UI.createView({
		//backgroundColor: "red",
		width: "270dp",
		height: Ti.UI.SIZE,
		//top: 120
		layout: "vertical"
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/"+unlockedData.image,
		height: "50dp",
		top: "0dp",
		//left: 10
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: unlockedData.title,
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: globals.fonts.bold25,
		top: "10dp",
		//left: 80
	});
	
	var descriptionLabel = Ti.UI.createLabel({
		text: unlockedData.description,
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: globals.fonts.regular13,
		width: "230dp",
		top: "0dp",
		//left: 80
	});
	
	container.add(icon);
	container.add(titleLabel);
	container.add(descriptionLabel);
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: "59dp",
		height: "38dp",
		bottom: "20dp"
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		view.fireEvent("close");
		globals.rootWin.popUpStack.Next();
	});
	
	
	view.add(unlockedSign);
	//view.add(subtitleLabel);
	view.add(container);
	view.add(okButton);
	
	function Close(){
		
	}
	
	view.Close = Close;	
	
	
	return view;
}
