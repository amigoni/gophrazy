exports.CreatePopUp = function(data){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		bubbleParent: false,
		zIndex: 100
	});
	
	view.addEventListener("close", function(){
		Close();
	});
	
	var background = Ti.UI.createView({
		backgroundColor: "black",
		width: view.width,
		height: view.height,
		opacity: 0
	});
	
	var container = Ti.UI.createView({
		width: 310,
		height: 420,
		top: globals.platform.actualHeight
	});
	
	var popUpBg = Ti.UI.createView({
		//backgroundTopCap: 40,
		//backgroundImage:'"+globals.imagesPath+"/images/popUpBg.png',
		backgroundColor: globals.colors.white,
		borderWidth:2,
		borderRadius: 20,
		width: 290,
		height: 400
	});
	
	var content = CreateContent(data);
	popUpBg.add(content);
	
	var closeX = Ti.UI.createView({
		backgroundImage:'"+globals.imagesPath+"/images/closeX.png',
		width: 28,
		height: 29,
		top: 0,
		right: 0
	});
	Common.MakeButton(closeX);
	
	closeX.addEventListener("singletap", function(){
		Close();
	});
	
	container.add(popUpBg);
	container.add(closeX);
	
	
	view.add(background);
	view.add(container);
	
	
	function Close(){
		//if (content.tutorialName != null) Ti.App.fireEvent("openTutorial", {name: content.tutorialName});
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			easing: Animator["EXP_IN"]
		});
		
		Animator.animate(background,{
			duration: 300,
			opacity: 0,
		});
		
		setTimeout(function(){view.fireEvent("closed");},350);
	}
	
	setTimeout(function(){
		Animator.animate(container,{
			duration: 500,
			top: (globals.platform.actualHeight-container.height)/2,
			easing: Animator["EXP_OUT"]
		});
		
		Animator.animate(background,{
			duration: 500,
			opacity: .65,
		});
	},200);
	
	
	return view;
};



function CreateContent(data){
	var configuration = 0;
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: 290,
		height: 400,
		tutorialName: data.bonusData[0].name != "monster" ? data.bonusData[0].name : null
	});
	
	var titleImage;
	if (data.type == "levelUp") {
		titleImage = "/images/levelUpSign.png";
		if (data.bonusData[0].type == "monster") configuration = 1;
	}
	else if (data.type == "teamLevelUp") {
		titleImage = "/images/teamLevelUpSign.png";
		configuration = 1;
	}	
	else if (data.type == "group") {
		titleImage = "/images/completeSign.png";
		configuration = 1;
	}	
	else if (data.type == "weekComplete") {
		titleImage = "/images/weekCompleteSign.png";
		configuration = 1;
		if (data.bonusData.length < 3) configuration = 2;
	}	
	
	
	if (configuration == 0){
		view.add(UnlockedView(data.bonusData[0]));
		view.add(BonusView(data.bonusData, 250));
	}
	else if (configuration == 1){
		var monsterView = MonsterView(data.bonusData[0],1);
		monsterView.top = 90;
		view.add(monsterView);
		view.add(BonusView(data.bonusData, 250));
	}
	else if (configuration == 2){
		
	}	
	
	
	
	var titleLabel = Ti.UI.createImageView({
		image: titleImage,
		top: 10
	});
	
	var subtitleLabel = Ti.UI.createLabel({
		text: data.subtitle,
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		width: 270,
		height: 20,
		top: 50
	});
	
	var unlockedLabel = Ti.UI.createImageView({
		image: "/images/unlockedSign.png",
		top: 85,
		visible: configuration == 0 ? true : false
	});
	
	var lootLabel = Ti.UI.createImageView({
		image: "/images/lootSign.png",
		top: 210,
		visible: configuration == 0 ? true : false
	});
	
	
	
	var okButton = Ti.UI.createView({
		backgroundImage: "/images/okButton.png",
		width: 59,
		height: 38,
		bottom: 20
	});
	
	Common.MakeButton(okButton);
	
	okButton.addEventListener("singletap", function(){
		view.fireEvent("close");
	});
	
	view.add(titleLabel);
	view.add(subtitleLabel);
	view.add(unlockedLabel);
	view.add(lootLabel);
	view.add(okButton);
	
	
	return view;
}


function BonusView(bonusData,top){
	var ScoreDisplay = require("ui/GameWinElements/ScoreDisplay");
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: Ti.UI.SIZE,
		top: top//bonusData.length > 2 ? 250 : null
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 80,
		layout: "horizontal"
	});

	
	for (var i = 0; i < bonusData.length; i++){
		if (bonusData[i].position >1){
			if(bonusData[i].position > 2) container.add(Ti.UI.createView({height: 80,width: 50}));
		
			var item;
			if(bonusData[i].type != "monster") {
				var total;
				if (bonusData[i].type == "hints") total = globals.gameData.hints;
				
				item = ScoreDisplay.CreateRewardContainer(bonusData[i].type,bonusData[i].increment,total);
			}
			else item = MonsterView(bonusData[i], 0.7);
			container.add(item);
		}
	}
	
	view.add(container);
	
	
	return view;
}


function MonsterView(monsterData,scale){
	monsterData = globals.col.monsters.find({monsterID:monsterData.monsterID})[0];
	var view = Ti.UI.createView({
		backgroundImage: "/images/monsterBadge.png",
		width: 113*scale,
		height: 113*scale
	});
	
	var image = monsterData.image;
	if (image < 10) image = "0"+image;
	/*
	var monsterIcon = Ti.UI.createImageView({
		image: "/images/monsters/"+image+".png",
		height: 60*scale,
		bottom: 30*scale
	});
	*/
	
	var monsterIcon = Titanium.UI.createMaskedImage({
	    //mask : 'mask.png', // alpha mask
		image: "/images/monsters/"+image+".png",
	    //mode : Titanium.UI.iOS.BLEND_MODE_OVERLAY,
		width: 60*scale,
		height: 60*scale,
		bottom: 35*scale,
		tint: "#CD4E3A",
		//visible: !monsterData.locked
	});
	
	var nameLabel = Ti.UI.createLabel({
		text: monsterData.name,
		//backgroundColor: "red",
		color: globals.colors.textOpposite,
		textAlign: "center",
		font: scale == 1 ? globals.fonts.bold15 : globals.fonts.bold13,
		minimumFontSize: 10,
		width: 60*scale,
		bottom: 25*scale
	});
	
	view.add(monsterIcon);
	view.add(nameLabel);
	
	
	return view;
}


function UnlockedView(unlockedData){
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: 270,
		height: Ti.UI.SIZE,
		top: 120
	});
	
	var icon = Ti.UI.createImageView({
		image: "/images/"+unlockedData.image,
		height: 50,
		top: 10,
		left: 10
	});
	
	var titleLabel = Ti.UI.createLabel({
		text: unlockedData.title,
		color: globals.colors.textOpposite,
		textAlign: "left",
		font: globals.fonts.bold15,
		top: 10,
		left: 80
	});
	
	var descriptionLabel = Ti.UI.createLabel({
		text: unlockedData.description,
		color: globals.colors.textOpposite,
		textAlign: "left",
		font: globals.fonts.regular11,
		width: 180,
		top: 25,
		left: 80
	});
	
	view.add(icon);
	view.add(titleLabel);
	view.add(descriptionLabel);
	
	
	return view;
}
