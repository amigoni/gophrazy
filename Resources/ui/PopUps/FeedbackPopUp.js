exports.CreatePopUp = function(){
	var view = Ti.UI.createView({
		width: globals.platform.width,
		height: globals.platform.actualHeight,
		bubbleParent: false,
		zIndex: 100,
		top: "0dp"
	});
	
	var background = Ti.UI.createView({
		backgroundColor: "black",
		width: view.width,
		height: view.height,
		opacity: 0
	});
	
	var container = Ti.UI.createView({
		width: "250dp",
		height: "250dp",
		top: globals.platform.actualHeight
	});
	
	var popUpBg = Ti.UI.createView({
		borderColor: "black",
		backgroundColor: globals.colors.white,
		borderWidth: "2dp",
		borderRadius: "20dp",
		width: "240dp",
		height: "240dp"
	});
	
	var feedbackIcon =  Ti.UI.createView({
		backgroundImage:"/images/feedbackIconSmall.png",
		width: 38,
		height: 25,
		top: 25
	});
	
	var text =  Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "Got ideas?\nSuggestions or issues?\n\nWe’d love to hear from you.\nClick below and write us an email.",//gameData.hints,
		width: "200dp",
		height: "155dp",
		color: globals.colors.darkBrown,
		textAlign: "center",
		font: globals.fonts.regular13,
		//minimumFontSize: 10,
	});
	
	var emailUsButton = Ti.UI.createView({
		backgroundImage:"/images/emailUsButton.png",
		width: "114dp",
		height: "30dp",
		bottom: "15dp"
	});
	Common.MakeButton(emailUsButton);
	
	emailUsButton.addEventListener("singletap", function(){
		Common.CreateButtonBlocker();
		var emailDialog = Ti.UI.createEmailDialog();
		emailDialog.toRecipients = ['support@mozzarello.com'];
		emailDialog.subject = "GoPhrazy Feedback";
		emailDialog.open();
		/*
		emailDialog.addEventListener("open", function(){
			
		});
		*/
		setTimeout(function(){
			Close();
			Ti.App.fireEvent("closeButtonBlocker");
		},2000);
	});
	
	
	popUpBg.add(feedbackIcon);
	popUpBg.add(text);
	popUpBg.add(emailUsButton);
	
	var closeX = Ti.UI.createView({
		backgroundImage:"/images/closeX.png",
		width: "28dp",
		height: "29dp",
		top: "0dp",
		right: "0dp"
	});
	Common.MakeButton(closeX);
	
	closeX.addEventListener("singletap", function(){
		Close();
	});
	

	container.add(popUpBg);
	container.add(closeX);
	
	
	view.add(background);
	view.add(container);
	
	
	function Close(){
		/*
		Animator.animate(container,{
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			easing: Animator["EXP_IN"]
		});
		
		Animator.animate(background,{
			duration: 300,
			opacity: 0,
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top: globals.platform.actualHeight,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));
		
		background.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			opacity: 0
		}));
		
		setTimeout(function(){view.fireEvent("closed");},350);
	}
	
	setTimeout(function(){
		/*
		Animator.animate(container,{
			duration: 500,
			top: ((globals.platform.actualHeightNumber-container.size.height)/2).toString()+"dp",
			easing: Animator["EXP_OUT"]
		});
		
		Animator.animate(background,{
			duration: 500,
			opacity: .92,
		});
		*/
		
		container.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			top: ((globals.platform.actualHeightNumber-container.size.height)/2).toString()+"dp",
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
		
		background.animate(Ti.UI.createAnimation({
			duration: 500,
			opacity: .75
		}));
	},200);
	
	
	return view;
};
