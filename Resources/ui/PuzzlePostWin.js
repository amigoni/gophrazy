exports.loadWin = function(){
	var GroupsController = require("controller/GroupsController");
	var MainTableView = require("ui/PuzzlePostWinElements/MainTableView");
	var CollectionTableView = require("ui/PuzzlePostWinElements/CollectionTableView");
	
	var puzzlePostData = GroupsController.GetPuzzlePostData();
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.height//-(globals.platform.actualHeight)
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var tableViewsController = CreateTableViewsController();
	
	tableViewsController.addEventListener("openGroup", function(e){
		var groupData = GroupsController.CreateGroup("collection","",e.rowData.$id);
		tableViewsController.Add(CollectionTableView.createView(groupData));
	});
	
	var mainTableView = MainTableView.createView("main", puzzlePostData);
	tableViewsController.Add(mainTableView);
	
	view.add(navBar);
	view.add(tableViewsController);
	
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		DisappearAnimation();
		navBar.Close();
		tableViewsController.Close();
		
		Ti.App.removeEventListener("closePuzzlePostWin", Close);
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view, function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	Ti.App.addEventListener("closePuzzlePostWin", Close);
	
	
	return view;
};



function CreateTableViewsController (){
	var controller = Ti.UI.createView({
		width: globals.platform.width,
	    height: globals.platform.actualHeight-44,
	    top: 44
	});
	
	controller.addEventListener("back", function(e){
		Back();
	});
	
	var stack = [];
	
	function Add(newTableView){
		stack.push(newTableView);
		controller.add(newTableView);
		if(stack.length >1){
			for (var i = 0; i < stack.length; i++){
				stack[i].MoveLeft();
			}	
		}
	}
	
	function Back (){
		for (var i = 0; i < stack.length; i++){
			stack[i].MoveRight();
		}
		
		setTimeout(function(){
			var last = stack[stack.length-1];
			last.Close();
			controller.remove(last);
			stack.pop();
		}, 1000);
	}
	
	function Close(){
		for(var i = 0; i < stack.length; i++){
			stack[i].Close();
			Ti.API.info(stack[i]);
		}
		stack = null;
	}
	
	controller.Add = Add;
	controller.Back = Back;
	controller.Close = Close;
	
	
	return controller;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: "transparent",
		width: globals.platform.width,
		height:44,
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 44,
		height: 44,
		right: 0,
		buttonID: "puzzlePostWinCloseButton",
		touchEnabled: true
	});
	
	closeButton.addEventListener("singletap", function(){
		if(closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: 12,
		color: globals.colors.darkBrown
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 25,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/newspaperIcon.png",
		width: 23,
		height: 17
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/thePuzzlePostSign.png",
		width: 134,
		height: 20,
		left: 5
	}));
	
	view.add(title);
	view.add(closeButton);
	
	function Close(){
	}
	
	view.Close = Close;
	
	
	return view;
};
