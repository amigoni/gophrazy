exports.loadWin = function(){
	var GroupsController = require("controller/GroupsController");
	var MessagesController = require("controller/MessagesController");
	var ButtonBar = require("ui/MailboxWinElements/ButtonBar");
	var InboxTableView = require("ui/MailboxWinElements/InboxTableView");
	var WriteTableView = require("ui/MailboxWinElements/WriteTableView");
	var ServerData = require("model/ServerData");
	ServerData.GetPuzzlesMessages(true);
	
	var messages;
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//-(globals.platform.actualHeight),
	  	zIndex: 1
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	
	var buttonBar = ButtonBar.createView();
	buttonBar.opacity = 0;
	buttonBar.addEventListener("clicked", function(e) {
		if (e.button == "inbox") {
			inboxTableView.opacity = 1;
			inboxTableView.visible = true;
			inboxTableView.touchEnabled = true;
			writeTableView.opacity = 0;
			writeTableView.visible = false;
			writeTableView.touchEnabled = false;
		} else if (e.button == "write") {
			inboxTableView.opacity = 0;
			inboxTableView.visible = false;
			inboxTableView.touchEnabled = false;
			writeTableView.opacity = 1;
			writeTableView.visible = true;
			writeTableView.touchEnabled = true;
		}	
	});
	
	var inboxTableView;
	var writeTableView;
	
	view.add(navBar);
	view.add(buttonBar);
	
	
	
	function Update(){
		buttonBar.opacity = 1;
		messages = MessagesController.getMessagesForMailbox();
		
		inboxTableView = InboxTableView.createView(messages);
		writeTableView = WriteTableView.createView();
		writeTableView.visible = false;
		writeTableView.touchEnabled = false;
		writeTableView.opacity = 0;
		
		view.add(inboxTableView);
		view.add(writeTableView);
	}
	
	function Open(){
		AppearAnimation();
		Ti.App.fireEvent("closeButtonBlocker");
	}
	
	function Close(){
		if(writeTableView)writeTableView.Close();
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view, function(){Ti.App.fireEvent("closeButtonBlocker");});},200);
	}
	
	function DisappearAnimation(){
		Common.WindowCloseAnimation(view, function(){
			Ti.App.removeEventListener("updateMailboxComplete", UpdateTableViewComplete);
			Ti.App.removeEventListener("GotMessages", Update);
			view.fireEvent("closed");
		});
	}
	
	function UpdateTableViewComplete(e){
		inboxTableView.UpdateTableViewComplete(e);
	}
	
	Ti.App.addEventListener("updateMailboxComplete", UpdateTableViewComplete);
	Ti.App.addEventListener("GotMessages", Update);
	
	view.Open = Open;
	view.Close = Close;
	
	globals.gameData.openedMessagesWinCount++;
	globals.col.gameData.commit();

	var AnalyticsController = require("/controller/AnalyticsController");
	AnalyticsController.RegisterEvent({category:"design", event_id: "openedMessagesWin", value:globals.gameData.openedMessagesWinCount});	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		backgroundColor: "#578392",
		width: globals.platform.width,
		height: "44dp",
		top: "0dp"
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: "44dp",
		height: "44dp",
		right: "0dp",
		buttonID: "mailBoxBackButton"
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		//minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(closeButton);
	
	var title = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 35,
		layout: "horizontal"
	});
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/mailBoxIcon.png",
		width: 32,
		height: 23
	}));
	
	title.add(Ti.UI.createView({
		backgroundImage:"/images/mailBoxSign.png",
		width: 123,
		height: 33,
		left: 10
	}));
	
	/*
	var createButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: 70,
		height: 44,
		left: 0,
		buttonID: "mailBoxCreateButton"
	});
	
	createButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("create");
	});
	
	createButton.add(Ti.UI.createLabel({
		text: "Create",
		textAlign: "center",
		height: 25,
		font: globals.fonts.bold20,
		minimumFontSize: 12,
		color: globals.colors.white
	}));
	
	Common.MakeButton(createButton);
	*/
	view.add(title);
	view.add(closeButton);
	//view.add(createButton);
	
	
	return view;
};