function Create(){
	var startTime;
	
	var view = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: 44,
		layout: "vertical"
	});
	
	var timeLabel = Ti.UI.createLabel({
		text: "0:00",
		font: globals.fonts.bold25,
		color: globals.colors.white,
		height: 27,
		width: 100,
		top: 0,
		textAlign: "center"
	});
	
	var bonusTimeLabel = Ti.UI.createLabel({
		text: "",
		font: globals.fonts.bold15,
		color: globals.colors.warning,
		height: 17,
		top: -3,
		textAlign: "center"
	});
	
	view.add(timeLabel);
	view.add(bonusTimeLabel);
	
	
	//
	var ticker;
	function Start(startTime, numberOfWords, bonusTimeSecs){
		startTime = startTime;
		
		var bonusTime = new moment.duration(bonusTimeSecs,"s");
		bonusTimeLabel.text = bonusTime.minutes()+":"+FormatText(bonusTime.seconds());
		
		ticker = setInterval(function(){
			var now = clock.getNow().valueOf();
			var difference = new moment.duration(now-startTime);
			timeLabel.text = difference.minutes()+":"+FormatText(difference.seconds());
		},1000);
	}
	
	function Stop(){
		clearInterval(ticker);
	}
	
	function ResetMe(){
		startTime = null;
		timeLabel.text = "0:00";
		bonusTimeLabel.text = "";
	}
	
	function FormatText(duration){
		var string = duration.toString();
		if (string.length == 1) string = "0"+string;
		return string;
	}
	
	view.Start = Start;
	view.Stop = Stop;
	view.ResetMe = ResetMe;
	
	return view;
}

module.exports = Create;
