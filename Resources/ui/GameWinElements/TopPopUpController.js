exports.createController = function(){
	var RandomPlus = require("lib/RandomPlus");
	var timer = 0;
	var hintPopUpShowedCount = 0;
	var friendPopUpShowedCount = 0;
	var skipPopUpShowedCount = 0;
	var intervalInSeconds = 1;
	var controller = {};
	var isPaused = false;
	var notifications = [];
	var notificationPeriod = 180; //in seconds
	
	notifications.push("askFriend");
	if(globals.gameData.abilities.skipbox.enabled == true) notifications.push("skip");
	if(globals.gameData.abilities.hints.enabled == true) notifications.push("hint");
	
	var randomNotificationController = RandomPlus.ShuffleBagStandard();
	for(var i = 0; i < notifications.length; i++){
		randomNotificationController.add(notifications[i]);
	}
	
	var interval = setInterval(
		function(){
			if(isPaused == false){
				timer += intervalInSeconds;
				if(timer > notificationPeriod){
					timer = 0; //Reset Timer
					var notification = randomNotificationController.next();
					
					if(notification == "hint"){
						hintPopUpShowedCount++;
						globals.rootWin.OpenWindow("topNotificationsPopUp", {type:"hint"});
					}
					else if(notification == "skip"){
						skipPopUpShowedCount++;
						globals.rootWin.OpenWindow("topNotificationsPopUp", {type:"skip"});
					}
					else if(notification == "askFriend"){
						friendPopUpShowedCount++;
						globals.rootWin.OpenWindow("topNotificationsPopUp", {type:"askFriend"});
					}	
				}
			}
		}
	, intervalInSeconds*1000);
	
	function Clear (){
		clearInterval(interval);
	}
	
	function ResetMe(){
		isPaused = false;
		timer = 0;
		hintPopUpShowedCount = 0;
		friendPopUpShowedCount = 0;
		skipPopUpShowedCount = 0;
	}
	
	function Pause(){
		isPaused = true;
	}
	
	function Resume(){
		isPaused = false;
	}
	
	
	controller.Clear = Clear;
	controller.ResetMe = ResetMe; 
	controller.Pause = Pause;
	controller.Resume = Resume;
	
	
	return controller;
};
