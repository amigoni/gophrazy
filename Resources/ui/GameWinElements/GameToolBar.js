exports.CreateGameToolBar = function(groupData){
	var SoundController = require("/controller/SoundController");
	var LocalData = require("model/LocalData");
	var gameData = globals.gameData;
	var hintButtonPresent = false;
	var skipButtonPresent = false;
	
	var margin = 10;
	var scaleMultiplier = 1;
	if(globals.platform.scale == 1) scaleMultiplier = globals.platform.width/320;
	
	var skipEnabled = false;
	if ((groupData.type == "general" || groupData.type == "collection") && gameData.abilities.skipbox.enabled == true) skipEnabled = true;

	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: globals.platform.width-20,
		height: 50,
		//top: 0,
		transform: Ti.UI.create2DMatrix().scale(scaleMultiplier,scaleMultiplier)
	});
	
	view.appear = function(){
		Animator.animate(view,{duration: 500,opacity:1});
	};
	
	view.disappear = function(){
		Animator.animate(view,{duration: globals.style.animationSpeeds.speed1,opacity:0});
	};
	
	var container = Ti.UI.createView({
		//backgroundColor: "green",
		width: Ti.UI.SIZE,
		height: 50,
		layout: "horizontal"
	});
	
	var undoButton = Ti.UI.createView({
		backgroundColor: globals.colors.buttonGameToolbarBGColor,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: "50dp",
		height: container.height,
		bottom: "0dp",
		opacity: 0,
		touchEnabled: true,
		buttonID: "gameToolBarUndoButton"
	});
	
	Common.MakeButton(undoButton);
	
	undoButton.add(Ti.UI.createView({
		//backgroundImage: globals.colors.theme == "dark" ? "/images/undoButtonDark.png" : "/images/undoButton.png",
		backgroundImage: "/images/undoButton.png",
		width: "30dp",
		height: "26dp",
		bottom: "18dp",
		transform: Ti.UI.create2DMatrix().scale(0.9,0.9)
	}));
	
	undoButton.add(Ti.UI.createLabel({
		text: "Undo",
		font: globals.fonts.boldSmall,
		color: globals.colors.white,
		bottom: "1dp"
	}));
	
	undoButton.addEventListener("singletap", function(e){
		if (undoButton.touchEnabled == true) {
			SoundController.playSound("undo");
			view.fireEvent("undo");
		}	
	});
	
	var clearButton = Ti.UI.createView({
		backgroundColor: globals.colors.buttonGameToolbarBGColor,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: "50dp",
		left: margin,
		height: container.height,
		bottom: "0dp",
		opacity: 0,
		touchEnabled: true,
		buttonID: "gameToolBarClearButton"
	});
	
	Common.MakeButton(clearButton);
	
	clearButton.add(Ti.UI.createView({
		//backgroundImage: globals.colors.theme == "dark" ? "/images/clearButtonDark.png" :"/images/clearButton.png",
		backgroundImage: "/images/clearButton.png",
		width: "21dp",
		height: "20dp",
		bottom: "19dp"
	}));
	
	clearButton.add(Ti.UI.createLabel({
		text: "Clear",
		font: globals.fonts.boldSmall,
		color: globals.colors.white,
		bottom: "1dp"
	}));
	
	clearButton.addEventListener("singletap", function(e){
		if (clearButton.touchEnabled == true) {
			SoundController.playSound("clear");
			view.fireEvent("clear");
		}	
	});
	
	var skipButton = Ti.UI.createView({
		backgroundColor: globals.colors.buttonGameToolbarBGColor,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: "50dp",
		left: margin,
		height: container.height,
		bottom: "0dp",
		opacity: 0,
		touchEnabled: true,
		buttonID: "gameToolBarSkipButton"
	});
	
	Common.MakeButton(skipButton);
	
	var skipIcon = Ti.UI.createView({
		//backgroundImage: globals.colors.theme == "dark" ? "/images/skipButtonDark.png" :"/images/skipButton.png",
		backgroundImage: "/images/skipButtonWhite.png",
		width: "20dp",
		height: "20dp",
		bottom: "19dp"
	});
	
	var skipLabel = Ti.UI.createLabel({
		text: "Skip",
		font: globals.fonts.boldSmall,
		color: globals.colors.white,
		bottom: "0dp"
	});
	
	var skipCount = Ti.UI.createLabel({
		text: globals.gameData.skips < 100 ?  globals.gameData.skips : "99+",
		font: globals.fonts.boldSmall,
		color: globals.colors.white,
		top: "24dp",
		right: "2dp"
	});
	
	skipButton.add(skipIcon);
	skipButton.add(skipLabel);
	skipButton.add(skipCount);
	
	skipButton.Refresh = function (){
		var count = globals.gameData.hints;
		if (count > 99) count = 99;
		hintCount.text = count;
	};
	
	skipButton.addEventListener("singletap", function(e){
		if (skipButton.touchEnabled == true) view.fireEvent("skip");
	});
	
	var hintButton = Ti.UI.createView({
		backgroundColor: globals.colors.buttonGameToolbarBGColor,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: "50dp",
		left: margin,
		height: container.height,
		bottom: "0dp",
		opacity: 0,
		touchEnabled: true,
		buttonID: "gameToolBarHintButton"
	});
	
	Common.MakeButton(hintButton);
	
	var hintIcon = Ti.UI.createView({
		//backgroundImage: globals.colors.theme == "dark" ? "/images/hintButtonDark.png" :"/images/hintButton.png",
		backgroundImage: "/images/hintButton.png",
		width: "35dp",
		height: "34dp",
		bottom: "16dp",
		transform: Ti.UI.create2DMatrix().scale(0.9,0.9)
	});
	
	var hintCount = Ti.UI.createLabel({
		text: LocalData.GetGameData().hints < 100 ?  LocalData.GetGameData().hints: "99+",
		font: globals.fonts.boldSmall,
		color: "#444444",
		top: "10dp"
	});
	
	hintIcon.add(hintCount);
	
	var hintLabel = Ti.UI.createLabel({
		text: "Hint",
		font: globals.fonts.boldSmall,
		color: globals.colors.white,
		bottom: "0dp"
	});
	
	hintButton.add(hintIcon);
	hintButton.add(hintLabel);
	
	hintButton.addEventListener("singletap", function(e){
		if (hintButton.touchEnabled == true) view.fireEvent("hint");
	});
	
	hintButton.Refresh = function (){
		var count = LocalData.GetGameData().hints;
		if (count > 99) count = "99+";
		hintCount.text = count.toString();
	};
	
	var autoCompleteButton = Ti.UI.createView({
		backgroundColor: globals.colors.white,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: "50dp",
		left: margin,
		height: container.height,
		bottom: "0dp",
		opacity: 0,
		touchEnabled: true,
		buttonID: "gameToolBarAutocompleteButton"
	});
	
	Common.MakeButton(autoCompleteButton);	
	
	autoCompleteButton.addEventListener("singletap", function(e){
		if (autoCompleteButton.touchEnabled == true) {
			autoCompleteButton.touchEnabled = false;
			autoCompleteButton.opacity = 0.5;
			view.fireEvent("autoComplete");
		}
	});
	
	
	var askForHelpButton = Ti.UI.createView({
		//backgroundColor: globals.colors.white,
		//borderColor: globals.colors.borderline,
		//borderWidth: 2,
		width: "50dp",
		left: margin,
		height: container.height,
		bottom: "0dp",
		opacity: 0,
		touchEnabled: true,
		buttonID: "gameToolBarAskForHelpButton"
	});
	
	Common.MakeButton(askForHelpButton);
	
	askForHelpButton.add(Ti.UI.createView({
		backgroundImage: "/images/helpButton.png",
		width: "34dp",
		height: "24dp",
		bottom: "19dp",
		//transform: Ti.UI.create2DMatrix().scale(0.9,0.9)
	}));
	askForHelpButton.add(Ti.UI.createLabel({
		text: "Friend",
		font: globals.fonts.bold8,
		color: globals.colors.white,
		textAlign: "center",
		bottom: "8dp"
	}));
	
	askForHelpButton.add(Ti.UI.createLabel({
		text: "Help",
		font: globals.fonts.bold8,
		color: globals.colors.white,
		textAlign: "center",
		bottom: "0dp"
	}));	
	
	askForHelpButton.addEventListener("singletap", function(e){
		if (askForHelpButton.touchEnabled == true) view.fireEvent("askForHelp");
	});
	
	
	container.add(undoButton);
	container.add(clearButton);
	if (gameData.abilities.hints.enabled == true) {
		container.add(hintButton);
		hintButtonPresent = true;
	}
	if (skipEnabled == true) {
		container.add(skipButton);
		skipButtonPresent = true;
	}	
	//if(globals.testing == true) 
	//container.add(autoCompleteButton);
	if(groupData.type != "firstStart") container.add(askForHelpButton);
	
	view.RefreshHintButton = hintButton.Refresh;
	
	view.add(container);
	
	function UpdateHintCount(e){
		var amount = e.amount;
		if(amount > 99) amount = "99+";
		hintCount.text = amount;
	}
	
	function UpdateSkipCount(e){
		var amount = e.amount;
		if(amount > 99) amount = "99+";
		skipCount.text = amount;
	}	
	
	function Close (){
		Ti.App.removeEventListener("updateHints", UpdateHintCount);
		Ti.App.removeEventListener("updateSkips", UpdateSkipCount);
		Ti.App.removeEventListener("levelUp", UpdateGameToolBar);
		CloseIconAnimation(undoButton);
		CloseIconAnimation(clearButton);
		CloseIconAnimation(askForHelpButton);
		CloseIconAnimation(autoCompleteButton);
		CloseIconAnimation(hintButton);
		CloseIconAnimation(skipButton);
		
		undoButton = null;
		clearButton = null;
		autoCompleteButton = null;
		askForHelpButton = null;
		hintButton = null;
		skipButton = null;
		container = null;
	}
	
	function OpenIconAnimation(icon){
		Animator.animate(icon,{
			duration: 1500,
			opacity: 1,
		});
	}
	
	function CloseIconAnimation(icon){
		Animator.animate(icon,{
			duration: 150,
			opacity: 0,
		});
		icon.Close();
	}
	
	function UpdateGameToolBar(){
		if (gameData.abilities.hints.enabled == true && hintButtonPresent == false){
			container.add(hintButton);
			OpenIconAnimation(hintButton);
			hintButtonPresent == true;
		}
		
		if (gameData.abilities.skipbox.enabled == true && skipButtonPresent == false){
			container.add(skipButton);
			OpenIconAnimation(skipButton);
			skipButtonPresent = true;
		}	
	}
	
	function SetSkipButton(skipped){
		if(skipped == true){
			skipCount.visible = false;
			skipIcon.visible = false;
			skipLabel.text = "Skipped";	
			skipLabel.color = globals.colors.warning;
		}
		else{
			skipCount.visible = true;
			skipIcon.visible = true;
			skipLabel.text = "Skip";
			skipLabel.color = globals.colors.white;		
		}
	};
	
	Ti.App.addEventListener("updateHints", UpdateHintCount);
	Ti.App.addEventListener("updateSkips", UpdateSkipCount);
	Ti.App.addEventListener("levelUp", UpdateGameToolBar);
	
	view.SetSkipButton = SetSkipButton;
	view.Close = Close;
	
	
	setTimeout(function(){
		OpenIconAnimation(undoButton);
		OpenIconAnimation(clearButton);
		OpenIconAnimation(askForHelpButton);
		OpenIconAnimation(autoCompleteButton);
		OpenIconAnimation(hintButton);
		OpenIconAnimation(skipButton);
	},2000);
	
	
	return view;
};
