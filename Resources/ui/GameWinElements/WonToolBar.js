exports.createView = function(width, type, phrase){	
	var gameData = globals.gameData;
	
	var view = Ti.UI.createView({
		width: width-10,
		height: "65dp",
		bottom: "25dp",
		left: "-"+globals.platform.width
	});
	
	var container = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: "65dp",
		layout: "horizontal"
	});
	
	var shareButton = Ti.UI.createView({
		width: "60dp",
		height: view.height,
		left: "0dp",
		buttonID: "wonToolBarShareButton"
	});
	
	Common.MakeButton(shareButton);
	
	shareButton.add(Ti.UI.createLabel({
		text: "Share",
		font: globals.fonts.boldSmall,
		color: globals.colors.black,
		textAlign:"center",
		top: "30dp"
	}));
	
	shareButton.add(Ti.UI.createView({
		//backgroundImage: globals.colors.theme == "dark" ? "/images/shareButtonDark.png" :"/images/shareButton.png",
		backgroundImage: "/images/shareButton.png",
		width: "37dp",
		height: "29dp",
		tranform: Ti.UI.create2DMatrix().scale(0.9,0.9),
		top: "0dp"
	}));
	
	shareButton.addEventListener("singletap", function(e){
		if (shareButton.touchEnabled == true) view.fireEvent("share");
	});
	
	
	/*
	var sendButton = Ti.UI.createView({
		width: "60dp",
		height: view.height,
		left: "15dp",
		buttonID: "wonToolBarSendButton"
	});
	
	Common.MakeButton(sendButton);
	
	sendButton.add(Ti.UI.createLabel({
		text: "Send\nPuzzle",
		font: globals.fonts.boldSmall,
		color: globals.colors.black,
		textAlign:"center",
		top: "30dp"
	}));
	
	sendButton.add(Ti.UI.createView({
		backgroundImage: "/images/sendPuzzleIcon.png",
		width: "54dp",
		height: "25dp",
		top: "4dp"
	}));
	
	sendButton.addEventListener("singletap", function(e){
		if (sendButton.touchEnabled == true) view.fireEvent("send");
	});
	*/
	
	
	var nextButton = Ti.UI.createView({
		width: "60dp",
		height: view.height,
		left: "15dp",
		buttonID: "wonToolBarNextButton"
	});
	
	Common.MakeButton(nextButton);
	
	nextButton.add(Ti.UI.createLabel({
		text: "Next\nPuzzle",
		font: globals.fonts.boldSmall,
		color: globals.colors.black,
		textAlign:"center",
		top: "30dp"
	}));
	
	nextButton.add(Ti.UI.createImageView({
		image: "/images/nextButton.png",
		width: 29,
		height: 29,
		top: 2
	}));
	
	nextButton.addEventListener("singletap", function(e){
		if (nextButton.touchEnabled == true) view.fireEvent("next");
	});
	
	
	
	var favoriteButton = Ti.UI.createView({
		width: "60dp",
		height: view.height,
		left: "15dp",
		buttonID: "wonToolBarFavoritesButton",
		favorite: phrase.favorite
	});
	
	Common.MakeButton(favoriteButton);
	
	favoriteButton.add(Ti.UI.createLabel({
		text: "Favs",
		font: globals.fonts.boldSmall,
		color: globals.colors.buttonTextMenus,
		textAlign:"center",
		top: "30dp"
	}));
	
	var favoriteIcon = Ti.UI.createImageView({
		image: phrase.favorite == true ? "/images/favoriteIcon.png" : "/images/favoriteIconGrey.png",
		width: 30,
		top: 2
	});
	
	favoriteButton.add(favoriteIcon);
	
	favoriteButton.addEventListener("singletap", function(e){
		if (favoriteButton.touchEnabled == true){
			 if(favoriteButton.favorite == false) favoriteButton.SetFavoriteIcon(true);
			 else favoriteButton.SetFavoriteIcon(false);
			 view.fireEvent("favorite");
		}	 
	});
	
	favoriteButton.SetFavoriteIcon = function(value){
		 if(value == true){
		 	favoriteButton.favorite = true;
		 	favoriteIcon.image = "/images/favoriteIcon.png";
		 }
		 else{
		 	favoriteButton.favorite = false;
		 	favoriteIcon.image = "/images/favoriteIconGrey.png";
		 }
	};
	
	
	var retryButton = Ti.UI.createView({
		width: "60dp",
		height: view.height,
		left: "15dp",
		buttonID: "wonToolBarRetryButton"
	});
	
	Common.MakeButton(retryButton);
	
	retryButton.add(Ti.UI.createLabel({
		text: "Retry",
		font: globals.fonts.boldSmall,
		color: globals.colors.black,
		textAlign:"center",
		top: "30dp"
	}));
	
	retryButton.add(Ti.UI.createImageView({
		image: "/images/nextButton.png",
		width: 29,
		height: 29,
		top: 2
	}));
	
	retryButton.addEventListener("singletap", function(e){
		if (retryButton.touchEnabled == true) view.fireEvent("retry");
	});
	
	
	//if(type !="dailyChallenge") container.add(retryButton);
	container.add(shareButton);
	//if (gameData.abilities.sendPuzzle.enabled == true) container.add(sendButton);
	//if (gameData.abilities.favoriteEnabled == true) 
	container.add(favoriteButton);
	if (type == "level" || type == "quote" || type == "general" || type == "collection" || type == "event") container.add(nextButton);
	
	view.add(container);
	
	function Appear(){
		view.left = -globals.platform.width;
		setTimeout(function(){
			/*
			Animator.animate(view,{
				duration: 500,
				left: "0dp",
				easing: Animator["EXP_OUT"]
			});}
			*/
			view.animate(Ti.UI.createAnimation({
				duration: globals.style.animationSpeeds.speed1,
				left: 0,
				curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
			}));}
		, 250);
	};
	
	function Disappear(){
		/*
		Animator.animate(view,{
			duration: globals.style.animationSpeeds.speed1, 
			left: globals.platform.width,
			easing: Animator["EXP_IN"]
			}, function(){view.left = "-"+globals.platform.width;});
		*/	
		view.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			left: globals.platform.width,
			curve: Titanium.UI.ANIMATION_CURVE_EASE_IN
		}));	
	};
	
	function Close (){
		shareButton.Close();
		favoriteButton.Close();
		//sendButton.Close();
		nextButton.Close();
		
		shareButton = null;
		favoriteButton = null;
		//sendButton = null;
		nextButton = null;
		container = null;
	}
	
	view.shareButton = shareButton;
	view.nextButton = nextButton;
	view.favoriteButton = favoriteButton;
	view.Appear = Appear;
	view.Disappear = Disappear;
	view.Close = Close;
	
	
	return view;
};