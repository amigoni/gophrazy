function Create(){
	var starArray = [];
	var starSettings = [
		{textComplete: "Complete", textIncomplete: ""},
		{textComplete: "No Undos", textIncomplete: "Complete with no Undos"},
		{textComplete: "Fast Time", textIncomplete: "Complete within Bonus Time"}
	];
	var timeouts = [];
	
	var view = Ti.UI.createView({
		//backgroundColor: "red",
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "vertical",
		opacity: 0,
		bottom: 40
	});
	
	view.addEventListener("touchend",function(){
		view.fireEvent("complete");
		ResetMe();
	});
	
	var starContainer = Ti.UI.createView({
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE,
		layout: "horizontal"
	});
	
	for(var i = 0; i < 3; i++){
		starArray.push(CreateStar(i));
		starContainer.add(starArray[i]);
	}
	
	view.add(starContainer);
	
	
	
	function CreateStar(i){
		var starView = Ti.UI.createView({
			width: 70,
			height: Ti.UI.SIZE,
			left: i > 0 ? 10:0,
			state: "empty",
		});
		
		var starShadow = Ti.UI.createView({
			backgroundImage: "/images/starBigShadow.png",
			//backgroundColor: "black",
			width: 50,
			height: 50,
		});
		
		var starFull = Ti.UI.createView({
			backgroundImage: "/images/starBig.png",
			width: 50,
			height: 50,
			opacity: 0,
			transform: Ti.UI.create2DMatrix().scale(.5,.5)
		});
		
		var label = Ti.UI.createLabel({
			text: starSettings[i].textComplete,
			font: globals.fonts.bold13,
			color: globals.colors.black,
			height: 15,
			width: Ti.UI.SIZE,
			top: 70,
			textAlign: "center",
			//transform: Ti.UI.create2DMatrix().translate(-500,0),
			opacity: .5
		});
		
		starView.add(starShadow);
		starView.add(starFull);
		starView.add(label);
		
		
		function CompleteAnimation(){
			starFull.animate(Ti.UI.createAnimation({duration:globals.style.animationSpeeds.speed1, opacity:1, transform: Ti.UI.create2DMatrix().scale(1,1)}));
			label.opacity = 1;
		}
		
		function Set(state){
			if(state == "full"){
				CompleteAnimation();
			}
			else if(state == "empty"){
				starFull.opacity = 0;
				label.opacity = .5;
			}
		}
		
		starView.Set = Set;
		
		
		return starView;
	}
	

	function Play(results){
		view.animate(Ti.UI.createAnimation({opacity: 1, duration: globals.style.animationSpeeds.speed1}));
		var i = 0;
		
		var startDelay = 500;
		var loopTime = 350;
		var endDelay = 1000;
		
		function Loop(){
			var actualLoopTime = startDelay;
			if(i > 0) actualLoopTime = loopTime;
			timeouts.push(setTimeout(function(){
				if(results[i].complete == true) starArray[i].Set("full");
					
				if(i == 2) {
					timeouts.push(setTimeout(function(){
						view.fireEvent("complete");
						ResetMe();
					},endDelay));
				}
				
				i++;
				if(i < starSettings.length) Loop();
			}, actualLoopTime));
		}
		
		Loop();
	}
	
	
	function ResetMe(){
		for(var i = 0; i < starArray.length; i++){
			starArray[i].Set("empty");
		}
		
		for(var i = 0; i < timeouts.length; i++){
			clearTimeout(timeouts[i]);
		}
		view.animate(Ti.UI.createAnimation({opacity: 0, duration: globals.style.animationSpeeds.speed2}));
	}
	
	function Close(){
		for(var i = 0; i < timeouts.length; i++) clearTimeout(timeouts[i]);
		timeouts = [];
		starArray = [];
		starSettings = null;
	}
	
	view.Play = Play;
	view.ResetMe = ResetMe;
	view.Close = Close;
	
	
	return view;
}

module.exports = Create;
