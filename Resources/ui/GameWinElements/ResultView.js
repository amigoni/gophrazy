exports.CreateResultView = function (width){
	var appearDelay = 1500; //Set for Android to not show bars;
	
	var resultView = Ti.UI.createView({
		//backgroundColor: "green",
		width: width,
		height: 120,
		top: 0,
		opacity: 0
	});
	
	var htmlCode = "";
	var resultLabel = Titanium.UI.createWebView({
		html:'<html><body><div style="text-align: center;">'+htmlCode+'</div></body></html>',
		borderRadius: Ti.Platform.name == "android" ? 1 : 0,
		borderColor:  globals.colors.white,
		backgroundColor: 'transparent',
		//backgroundColor: "red",
		//backgroundColor: globals.colors.theme == "dark" ? "white":"transparent",
		width: width-20,
		height: "80dp",
		//top:'20',
		//left:'0',
		//top: 40
		touchEnabled:false,
		text:"",
		top: 0
	});
	
	
	var authorLabel = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: "- author",
		width: width-40,
		height: "20dp",
		font: globals.fonts.bold15,
		minimumFontSize: "8dp",
		color: globals.colors.black,
		textAlign: "right",
		//top: 5,
		opacity: 0,
		bottom: "20dp"
	});
	
	authorLabel.appear = function(){
		//authorLabel.animate(appearAnimation);
	};
	
	authorLabel.disappear = function(){
		//authorLabel.animate(disappearAnimation);
	};
	
	var thirdLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "",
		width: width-40,
		height: "20dp",
		font: globals.fonts.regular1,
		minimumFontSize: "8dp",
		color: globals.colors.black,
		textAlign: "right",
		//top: 5,
		opacity: 0,
		bottom: "0dp"
	});
	
	
	resultView.add(resultLabel);
	resultView.add(authorLabel);
	resultView.add(thirdLabel);
	

//Functions
	function CompleteAnimation (phraseData){
		resultView.authorLabel.text = (phraseData.author != "" && phraseData.author != null) ? "- "+phraseData.author : "- Anonymous";
		resultView.thirdLabel.text = phraseData.topicTitle == null ? "" : phraseData.topicTitle;
		
		setTimeout (function(){
			/*
			Animator.animate(resultView,{
				duration: 1500, 
				top: "60dp"
			});
			Animator.animate(authorLabel,{duration: 500, opacity:1});
			Animator.animate(thirdLabel,{duration: 500, opacity:1});
			*/
			resultView.animate(Ti.UI.createAnimation({
				duration: 1500,
				top: "60dp",
			}));
			authorLabel.animate(Ti.UI.createAnimation({duration: 500, opacity: 1}));
			thirdLabel.animate(Ti.UI.createAnimation({ duration: 500, opacity: 1}));
			
		},100);
	};
	
	
	function Refresh(text, code, finishedIncorrect){
		resultLabel.text = text;
		resultLabel.html = code;
	}
	
	function Init(text, code, finishedIncorrect){
		setTimeout(function(){
			resultLabel.text = text;
			resultLabel.html = code;
			Ti.API.info(code);
			Ti.API.info(text);
		}, appearDelay+100);
	}
	
	
	function ClearText(){
		/*
		Animator.animate(authorLabel,{duration: globals.style.animationSpeeds.speed1,opacity:0});
		Animator.animate(thirdLabel,{duration: globals.style.animationSpeeds.speed1,opacity:0});
		Animator.animate(
			resultLabel,
			{duration: globals.style.animationSpeeds.speed1,opacity:0},
			function(){
				resultView.opacity = 0;
				htmlCode = "";
				resultLabel.html = '<html><body><div style="text-align: center;">'+htmlCode+'</div></body></html>';
				//resultLabel.text = '""';
				resultView.top = "0dp";
				
				//There seems to be a delay in the webview to update so the text still appear. 
				//Putting this delay to fix this issue. 
				setTimeout(function(){
					resultView.opacity = 1;
					resultLabel.opacity = 1;
				},100);
			}
		);
		*/
		authorLabel.animate(Ti.UI.createAnimation({duration: globals.style.animationSpeeds.speed1, opacity: 0}));
		thirdLabel.animate(Ti.UI.createAnimation({duration: globals.style.animationSpeeds.speed1, opacity: 0}));
		
		resultLabel.animate(Ti.UI.createAnimation({
			duration: globals.style.animationSpeeds.speed1,
			opacity: 0
			}), 
			function(){
				resultView.opacity = 0;
				htmlCode = "";
				resultLabel.html = '<html><body><div style="text-align: center;">'+htmlCode+'</div></body></html>';
				//resultLabel.text = '""';
				resultView.top = "0dp";
				
				//There seems to be a delay in the webview to update so the text still appear. 
				//Putting this delay to fix this issue. 
				setTimeout(function(){
					resultView.opacity = 1;
					resultLabel.opacity = 1;
				},100);
			}
		);
	};
	
	
	function Close(){
		resultLabel = null;
		authorLabel = null;
	}


//Expose
	resultView.resultLabel = resultLabel;
	resultView.authorLabel = authorLabel;
	resultView.thirdLabel = thirdLabel;	
	
	resultView.CompleteAnimation = CompleteAnimation;
	resultView.Refresh = Refresh;
	resultView.Init = Init;
	resultView.ClearText = ClearText;
	resultView.Close = Close;


//Execution	
	//This is for android not to show the scrolling lines of the webview
	setTimeout(function(){
		resultView.opacity = 1;
		},appearDelay
	);
	
	
	return resultView;
};