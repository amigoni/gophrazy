exports.CreateRandomWordDisplay = function(width, phraseData, groupData, callback){
	var SoundController = require("/controller/SoundController");
	var RandomPlus = require("/lib/RandomPlus"); 
	var PhrasesController = require("controller/PhrasesController");
	var LocalData = require("model/LocalData");
	var WordButton = require("ui/GameWinElements/WordButton");
	var phraseData = phraseData;
	var hintCount = 0;
	var wordsLabels = [];
	var correctOrderLabels = [];
	var wordsSelected = [];
	var hintedWordsArray = [];
	var finalWordOrderArray = [];
	var shuffledWordsTexts = [];
	var hintEnabled = false;
	var autoCompleteTime = 500;
	var colorPicker = CreateWordBoxColorPicker();
	
	
	//phraseData.text = "This is a very very long phrase to test how the scrollview will do in case there are too many words to display on the screen, so that the scrollview will scroll down just in case. I hope that it will work.";
	//phraseData.text = "One of the most sincere forms of respect is actually listening to what another has to say.Bada Bing";
	var startingPhrase =  crypt.decrypt(phraseData.text)+" ";
	var correctWordsTextArray = startingPhrase.match(/\S+\s/g);	
	var wordSelectToneController = SoundController.createWordSelectToneController();
	
	var view = Ti.UI.createScrollView({
		//backgroundColor: "red",
		width: width,
		height: globals.platform.height-276,
		//height: "224dp",//+globals.platform.actualHeight-430,
		contentWidth: width,
		contentHeight: "auto",
		data: phraseData,
		bottom: 0,
		complete: false,
		suggestedHint: false
	});

	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
		bottom: 15
	});
	
	//Since the word layout is Asynch we must continusly check till all of them
	//are set. Once they are we arrange them.
	var checkLoop = setInterval(function(){
		var allSet = true;
		for(var i = 0; i < wordsLabels.length; i++){
			if(wordsLabels[i] && wordsLabels[i].sizeIsSet == false) allSet = false;	
		}
		
		if (allSet == true){
			ArrangeWords();
			clearInterval(checkLoop);
		}
	},100);
	
	for (var i = 0; i < correctWordsTextArray.length;i++){
		var wordView = WordButton.CreateWordButton(phraseData, correctWordsTextArray[i], i, true, colorPicker.Next());
		
		wordView.addEventListener("wordSelected", function(e){
			if (wordsSelected.indexOf(this) == -1){
				wordSelectToneController.PlayNextTone();
				wordsSelected.push(this);
			} 
			Refresh();
		});
		
		wordView.addEventListener("wordUnselected", function(e){
			wordsSelected.splice(wordsSelected.indexOf(this),1);
			wordSelectToneController.GoBack();
			Refresh();
		});
		
		wordView.addEventListener("wordHinted", function(e){
			if (this.selected == true && wordsSelected.indexOf(this)!= -1) wordsSelected.splice(wordsSelected.indexOf(this),1);
			hintedWordsArray.push(this);
			Refresh();
			DisableHint();
			view.fireEvent("hintSelected",{useHint: e.useHint});
			hintCount++;
		});
		
		//Check if it's a saved hint
		if (phraseData.hintPositions){
			for (var j = 0; j < phraseData.hintPositions.length; j++){
				wordView.CheckIfHint(phraseData.hintPositions[j]);
			}
		}
		
		wordsLabels.push(wordView);
	}
	
	
	//Create a new array to keep the correct order.
	correctOrderLabels = wordsLabels.slice(0); //Clone array, instead of pointer
	
	var shuffledWordsLabels = RandomPlus.ShuffleArray(wordsLabels);
	
	for (var i = 0; i < shuffledWordsLabels.length; i++){
		shuffledWordsTexts.push(shuffledWordsLabels[i].text);
		shuffledWordsLabels[i].ChangeColor(colorPicker.Next());
		container.add(shuffledWordsLabels[i]);
	}
	
	view.add(container);
			
	/////Functions
	function Refresh(){
		var result = CreateStringFromWordsArray();
		view.fireEvent("changed", {resultString: result, finishedIncorrect: result.finishedIncorrect});
		
		if(result.text == startingPhrase){
			view.complete = true;
			view.fireEvent("complete");
		}
	}
	
	function ArrangeWords(){
		var xPos = 5;
		var yPos = 0;
		var margin = 4;
		var appearInitialDelay = 500;
		var appearDelay = 100;
		
		for (var j = 0; j < wordsLabels.length; j++){
			
			var rightEdge = xPos + wordsLabels[j].size.width+margin;
			//Ti.API.info(rightEdge+" "+(globals.platform.widthNumber-5));
			if (rightEdge > width+6){
				xPos = 5;
				yPos += wordsLabels[j].size.height+margin;	
				//Ti.API.info("y: "+yPos);
			}
			
			wordsLabels[j].left = xPos;//+"dp";
			wordsLabels[j].top = yPos;//+"dp";
			
			xPos += wordsLabels[j].size.width+margin;
		}
		
		for (var j = 0; j < wordsLabels.length; j++){
			wordsLabels[j].Appear(appearInitialDelay+j*appearDelay);
		}
		
		//Set start time for length of time spent of on phrase.
		//When they are all displayed.
		setTimeout(function(){view.fireEvent("startTime");},(10 + appearInitialDelay + wordsLabels.length*appearDelay ));
	}
	
	function CreateStringFromWordsArray(){
		var newString = "";
		var newStringArray = [];
		finalWordOrderArray = [];
		
		for (var i = 0; i < wordsLabels.length; i++){
			newStringArray.push("");
			finalWordOrderArray.push({text:""});
		}
		
		////SORTING CODE IF YOU HAVE MORE THAN A HINT
		function sortPosition(ob1,ob2) {
	    	var n1 = ob1.position;
	    	var n2 = ob2.position;
	    	if (n1 > n2) {return 1;}
			else if (n1 < n2){return -1;}
		    else { return 0;}//nothing to split
		};
		
		hintedWordsArray = hintedWordsArray.sort(sortPosition);
		
		
		for (var i = 0; i < hintedWordsArray.length; i++){
			newStringArray[hintedWordsArray[i].position] = hintedWordsArray[i].text;
			finalWordOrderArray[hintedWordsArray[i].position] = hintedWordsArray[i];
		}
		
		for (var i = 0; i < wordsSelected.length; i++){
			//Find if there are duplicate words not selected and exchange positions.
			if (i == wordsSelected.length-1){ 
				var originalWord = wordsSelected[i];
				var originalPosition = originalWord.position;
				for (var j = 0; j < correctOrderLabels.length; j++){
					if (correctOrderLabels[j].text == originalWord.text &&
						correctOrderLabels[j].selected == false &&
						originalWord.position > correctOrderLabels[j].position){
							originalWord.position = correctOrderLabels[j].position;
							correctOrderLabels[j].position = originalPosition;
							correctOrderLabels.sort(sortPosition);
							break;
					}
				}
			}
			
			//Find the first position where I could place it.
			var firstPosition = 0;
			for(var j = 0; j < hintedWordsArray.length; j++){
				if (wordsSelected[i].position > hintedWordsArray[j].position) firstPosition = hintedWordsArray[j].position+1;
			}
			
			//Find the first open spot where I could place it.
			for(var t = firstPosition; t < newStringArray.length; t++){
				if (newStringArray[t] == ""){
					newStringArray[t] = wordsSelected[i].text;
					finalWordOrderArray[t] = wordsSelected[i];
					break;
				}
			}
		}
		
		//Create String
		for (var i = 0; i < newStringArray.length; i++){
			newString += newStringArray[i];
		}
		
		var finishedIncorrect = false;
		if((wordsSelected.length + hintedWordsArray.length) == wordsLabels.length && newString != startingPhrase) {
			finishedIncorrect = true;
			SoundController.playSound("failedPhrase");
		}
		
		//Create HTML for rich text to have yellow hint.
		var newHTMLCode ="";
		for (var i = 0; i < finalWordOrderArray.length; i++){
			if (finalWordOrderArray[i].text != ""){
				var color = globals.colors.text;
				if(finishedIncorrect == true ) color = globals.colors.red;
				if (finalWordOrderArray[i].hinted == true) {
					color = globals.colors.green;
				}	
	
				newHTMLCode += '<span style="text-align: center;"><span style="font-family:'+globals.fonts.words2.fontFamily+';"><span style="font-size:'+globals.fonts.words2.fontSize+'px;"><span style="font-weight:'+globals.fonts.words2.fontWeight+';"><span style="color:'+color+';">'+finalWordOrderArray[i].text+'</span>';		
			}
		}
		
		
		return {text:newString, code: newHTMLCode, finishedIncorrect: finishedIncorrect};
	}
	
	
	function Clear(){
		for (var i = 0; i < wordsLabels.length; i++){
			if (wordsLabels[i].hinted == false) {
				wordsLabels[i].Refresh();
			}	
		}
		wordsSelected = new Array();	
		view.fireEvent("changed", {resultString: CreateStringFromWordsArray(), finishedIncorrect: false});
		wordSelectToneController.ResetCurrentTone();
	}
	
	function EnableHint(){
		if (hintEnabled == false){
			if (hintCount < 3){
				hintEnabled = true;
				for (var i = 0; i < wordsLabels.length; i++){
					wordsLabels[i].EnableHint();
				}
				
				if (globals.tutorialOn != true) globals.rootWin.OpenWindow("topNotificationsPopUp", {type:"useHint"});
			}
			else{
				alert("You already used all the hints for this phrase.");
			}
		}
	}
	
	function DisableHint(){
		for (var i = 0; i < wordsLabels.length; i++){
			wordsLabels[i].DisableHint();
		}
		hintEnabled = false;
		Ti.App.fireEvent("topNotificationsPopUpAction", {action: "close"});
	}
	
	function Undo(callback){
		if (wordsSelected.length > 0){
			var word; 
			for (var i = finalWordOrderArray.length-1; i >= 0; i--){
				if (finalWordOrderArray[i].text != "" && finalWordOrderArray[i].hinted == false) {
					word = finalWordOrderArray[i];
					finalWordOrderArray.splice(i,1);
					break;
				}	
			}
			
			word.Refresh();
			
			for (var i = 0; i < wordsSelected.length; i++){
				if (wordsSelected[i] == word){
					wordsSelected.splice(i,1);
					break;
				} 
			}
			view.fireEvent("changed",{resultString: CreateStringFromWordsArray(), finishedIncorrect: false});
			wordSelectToneController.GoBack();
		}
	}
	
	function Disappear (){
		for (var i = 0; i < wordsLabels.length; i++){
			wordsLabels[i].Disappear();
		}
	}
	
	
	function AutoComplete (autoCompleteTime){
		var currentPosition = 0;
		setTimeout(function(){
			var autoCompleteInterval = null;
			autoCompleteInterval = setInterval(
				function(){
				var wordButton = correctOrderLabels[currentPosition];
				wordButton.opacity = 0.25;	
				wordButton.fireEvent("wordSelected",{text:wordButton.label.text, position:wordButton.position});;
				currentPosition++;
				if (currentPosition >= correctOrderLabels.length) clearInterval(autoCompleteInterval);
			},autoCompleteTime);
		},
		1500);
	}
	
	function StartGlow(){
		setTimeout(function(){
			glowInterval = setInterval(function(){
				view.transform = Ti.UI.create2DMatrix().scale(0.9,0.9);
				setTimeout(function(){view.transform = Ti.UI.create2DMatrix().scale(1,1);},glowDuration-10);
			},glowDuration*2+10);
		},2000);
	}
	
	function StopGlow(){
		clearInterval(glowInterval);
		glowInterval = null;
	}
	
	var glowInterval = null;
	var glowDuration = 500;
	var currentWordText;
	
	function UpdateFromTutorial(e){
		currentWordText = e.currentWordText; 
		if(glowInterval == null && wordsLabels != null){
			glowInterval = setInterval(function(){
				for (var i = 0; i < wordsLabels.length; i++){
					if(wordsLabels[i] != null){ //Safety in case it was closed and nulled already
						if (currentWordText == wordsLabels[i].text){
							 wordsLabels[i].ToggleSize();	
						}	 
						else wordsLabels[i].transform = Ti.UI.create2DMatrix().scale(1,1);
					}
				}
			},glowDuration);
		}
	}
	
	function Close(){
		clearInterval(checkLoop);
		checkLoop = null;
		Ti.App.removeEventListener("tutorialUpdateRandomWordDisplay", UpdateFromTutorial);
		Ti.App.removeEventListener("EnableHint", EnableHint);
		for (var i = 0; i < wordsLabels.length; i++){
			wordsLabels[i].Close();
			wordsLabels[i] = null;
		}
		if(glowInterval != null) clearInterval(glowInterval);
 		setTimeout(function(){
 			wordsLabels = null;
			correctOrderLabels = null;
			wordsSelected = null;
			hintedWordsArray = null;
			finalWordOrderArray = null;
			shuffledWordsTexts = null;
			
			shuffledWordsLabels = null;
			correctOrderLabels = null;
			
			container = null;
 		},50);
	}
	/////end Functions
	view.correctWordsTextArray = correctWordsTextArray;
	view.shuffledWordsTexts = shuffledWordsTexts;
	view.hintEnabled = hintEnabled;
	
	view.Clear = Clear;
	view.Undo = Undo;
	view.Disappear = Disappear;
	view.EnableHint = EnableHint;
	view.DisableHint = DisableHint;
	view.AutoComplete = AutoComplete;
	view.Close = Close;
	view.CreateStringFromWordsArray = CreateStringFromWordsArray;
	
	Ti.App.addEventListener("tutorialUpdateRandomWordDisplay", UpdateFromTutorial);
	Ti.App.addEventListener("EnableHint", EnableHint);
	
	//AutoComplete(100);
	
	//This is to display Hinted text on android when you open
	setTimeout(function(){
		var result = CreateStringFromWordsArray();
		view.fireEvent("changed", {resultString: result, finishedIncorrect: result.finishedIncorrect});
	},1600);
	
	
	return view;
};


function CreateWordBoxColorPicker (){
	var RandomPlus = require("lib/RandomPlus");
	var picker = {};
	var wordBoxColors = globals.colors.wordBoxesColors;//globals.colors.wordBoxesColorShuffleBag.next()
	var wordBoxColorsArrayPosition = 0;//RandomPlus.RandomRange(0, wordBoxColors.length);
	
	function Next(){
		var color = wordBoxColors[wordBoxColorsArrayPosition];
		wordBoxColorsArrayPosition++;
		if (wordBoxColorsArrayPosition >= wordBoxColors.length) wordBoxColorsArrayPosition = 0;
		return color;
	}
	
	picker.Next = Next;
	
	
	return picker;
}



