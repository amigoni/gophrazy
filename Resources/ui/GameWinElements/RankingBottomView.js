exports.createView = function(){
	var FriendsController = require("controller/FriendsController");
	var leaderboardData;
	var appearTimeout;
	
	var view = Ti.UI.createView({
		width: globals.platform.width,
		//height: 63 + (50-globals.adsBannerSpace)+(globals.platform.height-480)/2,
		height: Ti.UI.FILL,
		//bottom: 0,
		layout: "vertical",
		opacity: 0
	});
	
	var connectionLabel = Common.ConnectionLabel();
	
	var tableView = Ti.UI.createTableView({
		backgroundColor: "transparent",
		width: globals.platform.width,
	    //height: 63 + (50-globals.adsBannerSpace)+(globals.platform.height-480)/2,
	    height: Ti.UI.FILL,
		bottom: 0,
		separatorColor: "transparent",
		separatorStyle: false,
		//left: type == "main" ? 0 : globals.platform.width,
	});
	
	view.add(connectionLabel);
	view.add(tableView);
	
	function Update(){
		var tableData = [];
		var data = FriendsController.GetFriendsWithAppByScore();
		var meIndex = 0;
		for(var i = 0; i < data.length; i++){
			if(data[i].isMe == true) meIndex = i;
			var rank = i+1;
			var row = CreateRow(data[i],rank);
			tableData.push(row);
		}
		
		if (data.length == 0) tableData.push(Ti.UI.createTableViewRow({touchEnabled:false, selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE: null, height:10 }));
		
		var inviteRow = CreateInviteRow();
		tableData.push(inviteRow);
		tableView.data = tableData;
		tableView.scrollToIndex(meIndex,{animated: false});
	}
	
	function UpdateTableView(){
		Update();
	}
	
	function Appear(){
		appearTimeout = setTimeout(function(){Animator.animate(view, {duration: 500, opacity: 1});},2000);
	}
	
	function Disappear(){
		if(appearTimeout != null) clearTimeout(appearTimeout);
		Animator.animate(view, {duration: 150, opacity: 0});
		tableView.data = [];
	}
	
	function Close(){
		if(appearTimeout != null) clearTimeout(appearTimeout);
		Ti.App.removeEventListener("updateLeaderboard", UpdateTableView);
		connectionLabel.Close();
	}
	
	view.UpdateTableView = UpdateTableView;
	view.Appear = Appear;
	view.Disappear = Disappear;
	view.Close = Close;
	
	Ti.App.addEventListener("updateLeaderboard", UpdateTableView);
	
	Update();
	
	
	return view;
};


function CreateRow(rowData, rank){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 20,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null
	});
	
	view.addEventListener("singletap", function(){
		view.fireEvent("clicked",{rowData:rowData});
	});
	
	var rankContainer = Ti.UI.createView({
		//backgroundColor: "green",
		width: Ti.UI.SIZE,
		height: 20,
		layout: "horizontal",
		left: 10
	});
	
	var rankFont = globals.fonts.bold12;
	if (rank == 1) rankFont = globals.fonts.bold20;
	else if (rank == 2) rankFont = globals.fonts.bold15;
	else if (rank == 3) rankFont = globals.fonts.bold13;
	
	var rankLabel1 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rank,
		font: rankFont,
		color: rowData.isMe == false ? globals.colors.white : "#BF593F",
		width: Ti.UI.SIZE,
		height: 20
	});

	var rankLabelText = "th";
	var rankText = rank.toString();
	var lastDigit = rankText[rankText.length-1];
	if (lastDigit == "1") rankLabelText = "st";
	else if (lastDigit == "2") rankLabelText = "nd";
	if (lastDigit == "3") rankLabelText = "rd";
	
	var rankLabel2 = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rankLabelText ,
		font: globals.fonts.bold10,
		color: rowData.isMe == false ? globals.colors.white : "#BF593F",
		textAlign: "right",
		height: 20
	});
	
	rankContainer.add(rankLabel1);
	rankContainer.add(rankLabel2);
	
	var nameLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.isMe == false ? rowData.name : "Me",
		font: globals.fonts.bold15,
		color: rowData.isMe == false ? globals.colors.white : "#BF593F",
		textAlign: "left",
		width: globals.platform.width-110,
		height: 20,
		left: 45
	});

	var scoreLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: rowData.score,
		font: globals.fonts.bold15,
		color:  rowData.isMe == false ? globals.colors.white : "#BF593F",
		textAlign: "right",
		width: 70,
		height: 20,
		right: 10
	});
	
	view.add(rankContainer);
	view.add(nameLabel);
	view.add(scoreLabel);
	
	
	return view;
}
	
	
function CreateInviteRow(){
	var view = Ti.UI.createTableViewRow({
		width: globals.platform.width,
		height: 40,
		selectionStyle: Ti.Platform.name == "iPhone OS" ? Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE : null
	});
	
	view.addEventListener("singletap", function(){
		if(globals.tutorialOn != true){
			Common.CreateButtonBlocker(true);
			if (globals.gameData.userFacebookID == ""){
				var ServerData = require("model/ServerData");
				var PushNotificationsController = require("controller/PushNotificationsController");
				ServerData.SignUpFacebook(function(){
					ServerData.GetFacebookFriendsInfo(function(){	
						Ti.App.fireEvent("multiplayerOn");
						ServerData.GetPuzzlesMessages(true);
						PushNotificationsController.SubscribeToMyChannel();
						Ti.App.fireEvent("closeButtonBlocker");
						Update();
					});		
				});
			}
			else{
				globals.rootWin.OpenWindow("inviteWin");
				Ti.App.fireEvent("closeButtonBlocker");
			}
		}
	});
	
	var title;
	
	function Update(){
		if (title != null) view.remove(title);
		
		if (globals.gameData.userFacebookID == ""){
			title = Ti.UI.createView({
				width: Ti.UI.SIZE,
				height: 38,
				layout: "horizontal",
				buttonID: "rankingBottomViewInviteButton"
			});
			
			Common.MakeButton(title);
			
			title.add(Ti.UI.createView({
				backgroundImage: "/images/friendsIconFacebook.png",
				width: 39,
				height: 38
			}));
			
			title.add(Ti.UI.createLabel({
				text: "Connect to play\nwith your Friends",
				font: globals.fonts.bold15,
				color: globals.colors.white,
				textAlign: "center",
				left: 15
			}));
		}
		else{
			title = Ti.UI.createView({
				width: Ti.UI.SIZE,
				height: 34,
				layout: "horizontal",
				buttonID: "rankingBottomViewInviteButton"
			});
			
			Common.MakeButton(title);
			
			title.add(Ti.UI.createView({
				backgroundImage:"/images/friendsIconLarge.png",
				width: 37,
				height: 34
			}));
			
			title.add(Ti.UI.createLabel({
				text: "Invite your Friends",
				font: globals.fonts.bold15,
				color: globals.colors.white,
				textAlign: "left"
			}));
		}	
		
		view.add(title);
	}
	
	Update();
	
	
	return view;
};