exports.CreateWordButton = function(phraseData, text, position, touchEnabled, color){
	var SoundController = require("/controller/SoundController");
	var PhrasesController = require("/controller/PhrasesController");
	var TutorialController = require("controller/TutorialController");
	var glowInterval;
	var glowDuration = 350;
	
	var view = Ti.UI.createView({
		backgroundColor: color,
		borderColor: globals.colors.borderLine2,
		borderWidth: "2dp",
		borderRadius: "6dp",
		width: Ti.UI.SIZE,
		height: "40dp",
		left: "-500dp",
		opacity: 0,
		sizeIsSet: false,
		selected: false,
		hintEnabled: false,
		hinted: false,
		text: text,
		position: position,
		phraseData: phraseData,
		transform:Ti.UI.create2DMatrix().scale(0.5,0.5),
		actuallyPressed: false,
		touchEnabled: true,
		layout: "horizontal"
	});
	
	view.addEventListener("touchstart", function(){
		if (TutorialController.CheckIfCorrectWord(text) == false){
			Ti.API.info("Disabled");
			view.touchEnabled = false;
			setTimeout(function(){view.touchEnabled = true;}, 1000);
		}
		else{
			//Ti.API.info("Enabled");
			if (globals.currentTutorial != null && globals.currentTutorial.currentWordText == text) Ti.App.fireEvent("tutorialStep");
		}
		
		Pressed();
	});
	
	
	var label = Ti.UI.createLabel({
		//backgroundColor: "green",
		text: text,
		font: globals.fonts.words1,
		shadowOffset:{x: 1, y: 1},
		shadowColor: Ti.Platform.name == "iPhone OS" ? "#222222" : "#111111",//"black",
		left: Ti.Platform.name == "iPhone OS" ? 4 : 12,
		top: Ti.Platform.name == "iPhone OS" ? null : 2,
		height: view.height,
		textAlign: "center",
		color: globals.colors.wordsInBoxesUnselected,
		touchEnabled: false,
		bubbleParent: true
	});
	
	label.addEventListener("postlayout", function(e){
		//Postlayout event is a bit weird it doesn't always set on first time
		//We check for width here to make sure that it's been set and then we
		//flip the sizeIsSet to prevent and infinate loop
		
		if (view.sizeIsSet == false && view.size.width > 12){
			//this.width = this.rect.width;
			//view.width = this.rect.width+8;
			//view.width = (this.rect.width+8).toString()+"dp";
			view.sizeIsSet = true;
		}
		//view.sizeIsSet = true;
	});
	
	
	view.add(label);
	view.add(Ti.UI.createView({width: Ti.Platform.name == "iPhone OS" ? 4 : 9, touchEnabled:false}));
	
	
	/////Functions
	function Pressed(){
		if (touchEnabled && view.touchEnabled == true){
			if (view.hintEnabled == false){
				if (view.selected == false){
					view.selected = true;
					SelectAnimation();
					view.fireEvent("wordSelected",{text:label.text, position:view.position});
				}
				else if (view.selected == true && view.hinted == false){
					view.selected = false;
					UnselectAnimation();
					view.fireEvent("wordUnselected",{text:label.text, position:view.position});
				}
			}
			else{
				if (view.hinted == false) {
					view.actuallyPressed = true;
					MakeHint();
					//SoundController.playSound("hintWordSelected");
					PhrasesController.SaveHintPositionForPhrase(view.position, view.phraseData.phraseID);
				}
				else{
					alert("This word is already a hint!");
				}
			}
			
			//if (globals.currentTutorial != null && globals.currentTutorial.currentWordText == text) Ti.App.fireEvent("tutorialStep");
		}
	}
	
	
	function Refresh(){
		if (view.hinted == false){
			label.color = globals.colors.wordsInBoxesUnselected;
			UnselectAnimation();
			view.selected = false;
		}
	}
	
	
	function EnableHint(){
		if (view.hinted == false) { 
			view.borderColor = "#FFBF00";
			view.borderWidth = "2dp";
			view.hintEnabled = true;
		}	
	}
	
	
	function DisableHint(){
		if (view.hinted == false) {
			view.borderColor = globals.colors.borderLine2;
			view.borderWidth = "2dp";
		}	
		view.hintEnabled = false;
	}
	
	function Appear(appearDelay){
		setTimeout(function(){
			if(view.hinted == false){
				SoundController.playSound("pop");
				
				if(Ti.Platform.name == "iPhone OS"){
					Animator.animate(view,{
						duration: 800,
						transform: Ti.UI.create2DMatrix().scale(1,1),
						easing: Animator['ELASTIC_OUT']
					});
					
					view.animate(Ti.UI.createAnimation({
						duration: 100,
						opacity: 1
					}));
				}
				else{
					view.animate(Ti.UI.createAnimation({
						duration: 100,
						transform: Ti.UI.create2DMatrix().scale(1,1),
						//curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT,
						opacity: 1
					}));	
				}
				
			}
		}, appearDelay);
	}
	
	
	function Disappear(){
		setTimeout(function(){
			view.animate(Ti.UI.createAnimation({
				duration: globals.style.animationSpeeds.speed1,
				transform: Ti.UI.create2DMatrix().scale(0.5,0.5),
				curve: Titanium.UI.ANIMATION_CURVE_EASE_IN,
				opacity: 0
			}));
		},10);
	};
	
	
	function SelectAnimation(){
		view.animate(Ti.UI.createAnimation({
			duration: 50,
			opacity: 0.3,
			transform: Ti.UI.create2DMatrix().scale(0.9, 0.9),
		}));
	}
	
	
	function UnselectAnimation(){
		view.animate(Ti.UI.createAnimation({
			duration: 50,
			opacity: 1,
			transform: Ti.UI.create2DMatrix().scale(1, 1),
			curve: Titanium.UI.ANIMATION_CURVE_EASE_OUT
		}));
	}
	
	function MakeHint(){
		view.fireEvent("wordHinted",{text:label.text, position: view.position, useHint: view.actuallyPressed});
		view.touchEnabled = false;
		view.borderColor = globals.colors.hint;
		view.hinted = true;	
		view.opacity = 0.3;
		view.transform = Ti.UI.create2DMatrix().scale(0.9,0.9);
	}
	
	function CheckIfHint(positionToCheck){
		if (positionToCheck == view.position) setTimeout(MakeHint,1000); //Delay so that it's not present when you first open the window
	}
	
	var size = 1;
	function ToggleSize(){
		if(size == 1) size = .9;
		else size = 1;
		view.transform = Ti.UI.create2DMatrix().scale(size,size);
	}
	
	function ChangeColor(newColor){
		view.backgroundColor = newColor;
	}
	
	function Close(){
		label = null;
	}
	/////end Functions
	
	
	view.Refresh = Refresh;
	view.EnableHint = EnableHint;
	view.DisableHint = DisableHint;
	view.Disappear = Disappear;
	view.Appear = Appear;
	view.MakeHint = MakeHint;
	view.CheckIfHint = CheckIfHint;
	view.label = label;
	view.ToggleSize = ToggleSize;
	view.ChangeColor = ChangeColor;
	view.Close = Close;
	
	
	return view;
};