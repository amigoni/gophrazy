exports.createView = function (group, win){
	var LocalData = require("model/LocalData");
	var TopTimeDisplay = require("/ui/GameWinElements/TopTimeDisplay");
	var gameData = globals.gameData;
	var titleDown = true;
	var animating = false;
	var title;
	
	var navBar = Ti.UI.createView({
		height: "50dp",
		top: "0dp",
		opacity: 0	
	});
	
	var backButton = Ti.UI.createView({
		top: "0dp",
		width: "44dp",
		height: "44dp",
		right: "0dp",
		buttonID: "gameWinBackButton"
	});
	
	Common.MakeButton(backButton);
	
	backButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		color: globals.colors.white,
		//textAlign: "center",
		//bubbleParent: false
		//top: group.type != "dailyChallenge" ? 4:null
	}));
	
	backButton.addEventListener("singletap", function(){
		if(backButton.touchEnabled == true) navBar.fireEvent("back");
	});
	
	var topTimeDisplay = new TopTimeDisplay();
	
	var titleLabel = Ti.UI.createLabel({
		text: "",
		font: globals.fonts.bold30,
		color: globals.colors.warning,
		top: "0dp",
		width: "240dp",//globals.platform.width-80,
		height: "44dp",
		minimumFontSize: "5dp",
		textAlign: "center",
		
		//top: "10dp"
		//bottom:3
		//top: group.type != "dailyChallenge" ? 4:null
	});
	
	/*
	titleLabel.addEventListener("singletap", function(){
		if (animating == false){
			animating = true;
			Toggle();
			setTimeout(function(){
				Toggle();
				setTimeout(function(){animating = false;},250);
			},3000);
		}
	});
	*/
	
	titleLabel.GoDown = function (){
		Animator.animate(titleLabel,{
			duration: globals.style.animationSpeeds.speed1,
			top: "10dp",
			easing: Animator["EXP_OUT"]
		});
	};
	
	titleLabel.GoUp = function(){
		Animator.animate(titleLabel,{
			duration: globals.style.animationSpeeds.speed1,
			top: "-50dp",
			easing: Animator["EXP_IN"]
		});
	};
	
	navBar.add(titleLabel);
	if(globals.gameData.timeChallengeEnabled == true) navBar.add(topTimeDisplay);
	navBar.add(backButton);
	
	
	function Toggle(){
		if(titleDown == true){
			titleLabel.GoUp();
			titleDown = false;
		}
		else{
			setTimeout(titleLabel.GoDown,250);
			titleDown = true;
		}
	}
	
	function Update(newGroup){
		Ti.API.info(newGroup.phrasesCompleteCount);
		group = newGroup;
		var title;
		if (group.type == "collection") title = group.title+" ("+group.phrasesCompleteCount+"/"+group.phrasesCount+")";
		else if (group.type == "general") title = globals.gameData.phrasesCompleteCount; //title = group.phrasesCompleteCount+"/"+group.phrasesCount;
		else title = group.title;
		titleLabel.text = title;
	}
	
	function Close(){
		Animator.animate(navBar,{
			duration: globals.style.animationSpeeds.speed1,
			opacity: 0,
		});
		
		Ti.App.removeEventListener("networkBarAction", GameWinNetworkBarAction);
		backButton.Close();	
		titleLabel = null;
		backButton = null;
	};
	
	function GameWinNetworkBarAction(e){
		if(e.isDown == true){
			/*
			Animator.animate(titleLabel,{
				duration: 500,
				opacity: 0,
			});
			*/
			titleLabel.animate(Ti.UI.createAnimation({
				opacity: 0,
				duration: 500
			}));
		}
		else{
			/*	
			Animator.animate(titleLabel,{
				duration: 500,
				opacity: 1,
			});
			*/
			titleLabel.animate(Ti.UI.createAnimation({
				opacity: 1,
				duration: 500
			}));
		}
	}
	
	function GameWinNetworkBarOff(){
		
	}
	
	//navBar.titleLabel = titleLabel;
	navBar.topTimeDisplay = topTimeDisplay;
	navBar.backButton = backButton;
	navBar.Update = Update;
	navBar.Close = Close;	
	
	
	setTimeout(function(){
		/*
		Animator.animate(navBar,{
			duration: 1000,
			opacity: 1,
		});},
		*/
		navBar.animate(Ti.UI.createAnimation({
			opacity: 1,
			duration: 1000
		}));}
	,2000);
	
	Ti.App.addEventListener("networkBarAction", GameWinNetworkBarAction);
	
	Update(group);
	
	
	return navBar;
};
