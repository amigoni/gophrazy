exports.loadWin = function(data){
	var PassController = require("controller/PassController");
	var FriendsController = require("controller/FriendsController");
	
	var singlePass = globals.storeController.FindProductBySKU("pass_1", true);
	var subscription = globals.storeController.FindProductBySKU("subscription_1", true);
	
	var numberOfFriendsAtUnlock = globals.gameData.passData.numberOfFriendsAtUnlock;
	var currentNumberOfFriends = FriendsController.GetFriendsWithAppByScore().length-1; //It includes me.
	if(currentNumberOfFriends < 0) currentNumberOfFriends = 0;
	var friendsIncrement =  globals.gameData.passData.friendsIncrement;
	var friendsLeft = numberOfFriendsAtUnlock+friendsIncrement-currentNumberOfFriends;
	
	var askFriendsSubtitle1text;
	if(friendsLeft > 0) askFriendsSubtitle1text = "Get "+friendsLeft+" more friends to join";
	else askFriendsSubtitle1text = "Thanks for spreading the word!";
	
	var askFriendsSubtitle2text;
	if(friendsLeft > 0) askFriendsSubtitle2text = "Current "+currentNumberOfFriends+" - Goal "+(numberOfFriendsAtUnlock+friendsIncrement);
	else askFriendsSubtitle2text = "Press to continue!";
	
	
	var view = Titanium.UI.createView({ 
	    backgroundImage:"/images/beigeBg.png",
	    width: globals.platform.width,
	    height: globals.platform.actualHeight,
	    //opacity: 0,
	  	bubbleParent: false,
	  	top: globals.platform.actualHeight,//-(globals.platform.actualHeight),
	  	zIndex: 1000,
	});
	
	var navBar = CreateNavBar();
	navBar.addEventListener("closeWin", function(){
		view.fireEvent("close");
		Close();
	});
	
	var scrollView = Ti.UI.createScrollView({
		width: view.width,
		height: view.height,
		contentWidth: view.width,
		contentHeight: "auto",
		top: 0
	});
	
	var container = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 0
	});
	
	
	var title1 = Ti.UI.createLabel({
		text: "Wanna Continue?",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold30,
		width: 280,
		top: 50
	});	
	
	var title2 = Ti.UI.createLabel({
		text: "You can:",
		color: "#555555",
		textAlign: "left",
		font: globals.fonts.bold15,
		width: 280,
		top: 25,
		left: 10
	});	
	
	var spreadTheWordArea = CreateSpreadTheWordArea(1, askFriendsSubtitle1text, askFriendsSubtitle2text);
	
	spreadTheWordArea.askFriendsButton.addEventListener("singletap", function(){
		if(globals.gameData.passData.passes > 0){
			PassController.UsePass(data.callback);
			view.fireEvent("close");
		}
		else {
			if (globals.gameData.userFacebookID == ""){
				Common.CreateButtonBlocker(true);
				var ServerData = require("model/ServerData");
				var PushNotificationsController = require("controller/PushNotificationsController");
				var FriendController = require("controller/FriendsController");
				ServerData.SignUpFacebook(function(){
					ServerData.GetFacebookFriendsInfo(function(){	
						Ti.App.fireEvent("multiplayerOn");
						ServerData.GetPuzzlesMessages(true);
						PushNotificationsController.SubscribeToMyChannel();
						//friendsButton.Refresh();
						//globals.rootWin.OpenWindow("inviteWin");
						Ti.App.fireEvent("closeButtonBlocker");
						globals.gameData.passData.numberOfFriendsAtUnlock = FriendController.GetNumberOfFriendsWithApp();
						globals.col.gameData.commit();	
						view.fireEvent("close");
					});		
				});
			}
			else{
				globals.rootWin.OpenWindow("inviteWin");
			}
		}
	});
	
	var supportArea = CreateSupportArea(1, singlePass, subscription);
	var waitArea = CreateWaitArea(2);
	
	
	container.add(title1);
	//container.add(title2);
	//container.add(spreadTheWordArea);
	//if(friendsLeft > 0){
		container.add(supportArea);
	//}
	container.add(waitArea);
	container.add(Ti.UI.createView({height: 15}));
	
	
	
	scrollView.add(container);
	
	
	view.add(scrollView);
	view.add(navBar);
	
	
	//Functions
	function TryToPass(){
		if(globals.gameData.passData.passes > 0){
			PassController.UsePass(data.callback);
			Close();
		}
		else{
			globals.storeController.BuyStoreKitItem("pass_1", 1);
		}
	}
	
	function Open(){
		AppearAnimation();
	}
	
	function Close(){
		Ti.App.removeEventListener("boughtPass", TryToPass);
		DisappearAnimation();
	}
	
	function AppearAnimation(){
		setTimeout(function(){Common.WindowOpenAnimation(view,null);},1200);
	}
	
	function DisappearAnimation(){
		view.fireEvent("close"); //For viewStack
		Common.WindowCloseAnimation(view,function(){view.fireEvent("closed");});
	}
	
	view.Open = Open;
	view.Close = Close;
	
	
	view.addEventListener("TryToPass", TryToPass);
	
	Ti.App.addEventListener("boughtPass", TryToPass);
	
	
	return view;
};


function CreateNavBar(){
	var view = Ti.UI.createView({
		//backgroundColor: "#CD4D39",
		width: globals.platform.width,
		height: '44dp',
		top: 0
	});
	
	var closeButton = Ti.UI.createView({
		//backgroundColor: "red",
		width: '44dp',
		height: '44dp',
		right: '0dp',
		buttonID: "creditsWinBackButton",
		touchEnabled: true
	});
	
	closeButton.addEventListener("singletap", function(){
		if (closeButton.touchEnabled == true) view.fireEvent("closeWin");
	});
	
	closeButton.add(Ti.UI.createLabel({
		text: " X ",
		font: globals.fonts.boldMedium31,
		minimumFontSize: '12dp',
		color: globals.colors.black
	}));
	
	Common.MakeButton(closeButton);
	
	view.add(closeButton);
	
	
	return view;
};


function CreateAskFriendsButton(askFriendsSubtitle1text, askFriendsSubtitle2text){
	var view = Ti.UI.createView({
		//backgroundColor: globals.colors.white,
		//borderWidth: "2dp",
		//borderColor: globals.colors.black,
		//borderRadius: 7,
		backgroundImage:"/images/buttonBg.png",
		backgroundLeftCap: 30,
		backgroundTopCap: 30,
		width: 270,
		height: Ti.UI.SIZE,
		top: 10,
		touchEnabled: true,
		buttonID: "passAskFriendsButton"
	});
	
	Common.MakeButton(view);
	
	var facebookIcon = Ti.UI.createImageView({
		image:"/images/facebookSquareIcon.png",
		width: 38,
		height: 38,
		left: 20
	});
	
	var labelContainer = Ti.UI.createView({
		width: view.width-60,
		height: Ti.UI.SIZE,
		layout: "vertical",
		left: 60
	});
	
	var askFriendTitle = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: "Invite more friends to get\n"+globals.gameData.passData.curveFacebook+" Puzzles",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold15,
		width: labelContainer.width,
		height: 45,
		top: 10
	});
	
	var askFriendSubtitle1 = Ti.UI.createLabel({
		text: askFriendsSubtitle1text,
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold11,
		minimumFontSize: "12dp",
		width: labelContainer.width,
		height: 15,
		top: 0
	});
	
	var askFriendSubtitle2 = Ti.UI.createLabel({
		text: askFriendsSubtitle2text,
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold11,
		minimumFontSize: "12dp",
		width: labelContainer.width,
		height: 15,
		top: "-3dp"
	});
	
	labelContainer.add(askFriendTitle);
	labelContainer.add(askFriendSubtitle1);
	//labelContainer.add(askFriendSubtitle2);
	labelContainer.add(Ti.UI.createView({height:10}));
	
	view.add(facebookIcon);
	view.add(labelContainer);
	
	
	return view;
}


function CreateBuyOneTimeButton(price){
	var view = Ti.UI.createView({
		//backgroundColor: globals.colors.white,
		//borderWidth: "2dp",
		//borderColor: globals.colors.black,
		//borderRadius: "7dp",
		backgroundImage:"/images/buttonBg.png",
		backgroundLeftCap: 30,
		backgroundTopCap: 30,
		width: 270,
		height: 70,
		top: 10,
		touchEnabled: true,
		buttonID: "passBuyNowButton"
	});
	
	Common.MakeButton(view);
	
	var logo = Ti.UI.createImageView({
		image: "/images/defaultLogo.png",
		width: 35,
		height: 27,
		left: 15
	});
	
	var buyNowTitle = Ti.UI.createLabel({
		text: "Get "+globals.gameData.passData.curveBought+" Puzzles ",
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.bold15,
		width: view.width-50-50,
		left: 65,
		top: 10
	});
	
	var priceLabel = Ti.UI.createLabel({
		text: price,
		color: globals.colors.black,
		textAlign: "right",
		font: globals.fonts.bold15,
		width: 80,
		right: 15,
		top: 10
	});
	
	var label3 = Ti.UI.createLabel({
		text: "Also removes interstitial ads while you play these.",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.bold11,
		width: view.width-70,
		left: 60,
		top: 30
	});
	
	view.add(logo);
	view.add(buyNowTitle);
	view.add(priceLabel);
	view.add(label3);
	
	
	return view;
}


function CreateSubscribeButton(price){
	var container = Ti.UI.createView({
		//backgroundColor: "red",
		width: 280,
		height: 90,
		top: 10
	});
	
	var view = Ti.UI.createView({
		//backgroundColor: globals.colors.white,
		//borderWidth: 2,
		//borderColor: globals.colors.black,
		//borderRadius: 7,
		backgroundImage:"/images/buttonBg.png",
		backgroundLeftCap: 30,
		backgroundTopCap: 30,
		width: 270,
		height: Ti.UI.SIZE,
		top: 10,
		touchEnabled: true,
		buttonID: "passSubscribeButton"
	});
	
	Common.MakeButton(view);
	
	var logo = Ti.UI.createImageView({
		image: "/images/defaultLogo.png",
		width: 35,
		height: 27,
		left: 15
	});
	
	var container1 = Ti.UI.createView({
		width: view.width- 75,
		left: 70,
		top: 10,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var title1 = Ti.UI.createLabel({
		text: "Subscribe",
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.bold15,
		left: 0,
		top: 0
	});
	
	var title2 = Ti.UI.createLabel({
		text: "Unlimited Puzzles, Cancel Anytime",
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.regular13,
		left: 0,
		top: 0
	});
	
	container1.add(title1);
	container1.add(title2);
	container1.add(Ti.UI.createView({height:10}));
	
	var title3 = Ti.UI.createLabel({
		text: price+"/month",
		color: globals.colors.black,
		textAlign: "right",
		font: globals.fonts.bold15,
		right: 10,
		top: 10
	});
	
	view.add(logo);
	view.add(container1);
	view.add(title3);
	
	var bestDeal = Ti.UI.createImageView({
		image: "/images/bestDealSign.png",
		width: 81,
		height: 17,
		top : 0,
		right: 0
	});
	
	
	
	container.add(view);
	container.add(bestDeal);
	
	
	return container;
}


function CreateSpreadTheWordArea(number, askFriendsSubtitle1text, askFriendsSubtitle2text){
	var view = Ti.UI.createView({
		width: globals.platform.width - 20,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 00
	});
	
	var title1 = Ti.UI.createLabel({
		text: number+") Spread the word",
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.bold20,
		width: view.width,
		top: 0
	});
	
	var askFriendsButton = CreateAskFriendsButton(askFriendsSubtitle1text, askFriendsSubtitle2text);
	
	
	view.add(title1);
	view.add(askFriendsButton);
	
	view.askFriendsButton = askFriendsButton;
	
	
	return view;
}


function CreateSupportArea(number, singlePass, subscription){
	var view = Ti.UI.createView({
		width: globals.platform.width - 20,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 20
	});
	
	var container1 = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE,
		top: 0,
		left: 0
	});
	
	var title1 = Ti.UI.createLabel({
		text: number+") Support the game",
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.bold20,
		width: view.width,
		top: 0,
		left: 0
	});
	
	var title2 = Ti.UI.createLabel({
		text: "Mozzarello loves this the best!",
		color: "#555555",
		textAlign: "left",
		font: globals.fonts.bold13,
		top: 25,
		left: 27
		//width: view.width-60
	});
	
	var heartLogo = Ti.UI.createView({
		backgroundImage: "/images/favoriteIcon.png",
		right: 20,
		width: 30,
		height: 26
	});
	
	container1.add(title1);
	container1.add(title2);
	container1.add(heartLogo);
	
	var subscriptionPrice = "";
	if(subscription) subscriptionPrice = subscription.priceAsString;
	var subscribeButton = CreateSubscribeButton(subscriptionPrice);
	subscribeButton.addEventListener("singletap", function(){
		alert("Subscribe");
	});
	
	var buyOneTimeButton = CreateBuyOneTimeButton(singlePass.priceAsString);
	
	buyOneTimeButton.addEventListener("singletap", function(){
		//TryToPass();
		view.fireEvent("TryToPass");
	});
	
	
	view.add(container1);
	view.add(buyOneTimeButton);
	if(subscription != null)
	{
		view.add(subscribeButton);
	}
	
	view.askFriendsButton;
	
	
	return view;
}


function CreateWaitArea(number){
	var view = Ti.UI.createView({
		width: globals.platform.width - 20,
		height: Ti.UI.SIZE,
		layout: "vertical",
		top: 20
	});
	
	
	var container1 = Ti.UI.createView({
		width: view.width,
		height: Ti.UI.SIZE
	});
	
	var title1 = Ti.UI.createLabel({
		text: number+") Wait and be bored",
		color: globals.colors.black,
		textAlign: "left",
		font: globals.fonts.bold20,
		width: view.width,
		top: 0
	});
	
	var worstOptionSign = Ti.UI.createView({
		backgroundImage: "/images/worstOptionSign.png",
		width: 135,
		height: 37,
		top: 5,
		//right: 0
	});
	
	container1.add(title1);
	
	
	var title2 = Ti.UI.createLabel({
		text: "Come back tomorrow for\n"+globals.gameData.passData.freeDailyPuzzlesAllowed.toString()+" free puzzles",
		color: "#555555",
		textAlign: "center",
		font: globals.fonts.bold20,
		width: view.width,
		top: 10
	});
	
	var title3 = Ti.UI.createLabel({
		text: "+ your Fortune Cookie",
		color: globals.colors.black,
		textAlign: "center",
		font: globals.fonts.regular12,
		width: view.width,
		//top: 0
	});
	
	
	view.add(container1);
	view.add(worstOptionSign);
	view.add(title2);
	view.add(title3);
	
	
	return view;
}
