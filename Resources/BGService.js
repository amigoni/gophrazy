var globals = {};
globals.os = Ti.Platform.osname != "android" ? "ios" : "android";
var moment = require("lib/moment.min");
moment.lang('en', {
    calendar : {
        lastDay : '[Yesterday]',
        sameDay : 'LT',
        nextDay : '[Tomorrow]',
        lastWeek : 'dddd',
        nextWeek : 'dddd',
        sameElse : 'L'
    }
});
var clock = require("lib/clock");

var LocalData = require("/model/LocalData");
LocalData.InitData(true);

globals.gameData.lastClosedTime = clock.getNow().toISOString();
Ti.API.info(globals.gameData.lastClosedTime);
globals.col.gameData.commit();

var LocalNotificationController = require("/controller/LocalNotificationController");
LocalNotificationController.SetAllNotifications();
Ti.App.currentService.stop();
