//var Parse = require('lib/TiParse')({applicationId:"2a1pz8Jl1CCfXqmyns23q9HYEaFQxtaRB2nGS5Mw",javascriptkey:"m6kQ0Ug8M36fX9fxrb5V35awrcqYBVXeH0peXHxH"});
var FriendsController = require("/controller/FriendsController");
var fb;
if(globals.platform.name != "iPhone OS") fb = require("facebook");


exports.LogoutFacebook = function(){
	Ti.Network.createHTTPClient().clearCookies('https://m.facebook.com');
	Ti.Network.createHTTPClient().clearCookies('http://m.facebook.com');
};


exports.SignUpFacebook = function (successCallback){
	if(Titanium.Network.online == true)
	{
	    if (globals.gameData.userFacebookID != ""){
	    	var loggedIn = false;
	    	if(globals.platform.name == "iPhone OS") loggedIn = Ti.Facebook.loggedIn;
	    	else loggedIn = fb.loggedIn;
	    	
	    	if(loggedIn == true) exports.GetFacebookFriendsInfo();
	    	else exports.GetMyFacebookInfo(exports.GetFacebookFriendsInfo);
	    }
	    else{
	    	Ti.API.info('Facebook: First Login');
			Common.CreateButtonBlocker(true, true);
			Ti.App.fireEvent("changeButtonBlocker", {text:"Connecting to Facebook\nPlease wait"});
			
			exports.GetMyFacebookInfo(
				function(){
					Ti.App.fireEvent("changeButtonBlocker", {text:"Connected!\nGetting friends scores"});
					exports.GetFacebookFriendsInfo(
						function(){	
							var PushNotificationsController = require("/controller/PushNotificationsController");
							PushNotificationsController.CheckForPushNotificationsTutorialCompletion();
							
							Ti.App.fireEvent("multiplayerOn");
							exports.GetPuzzlesMessages(true);
							
							Ti.App.fireEvent("ConnectedToFacebook");
							Ti.App.fireEvent("changeButtonBlocker", {text:"All Done!"});
							setTimeout(function(){Ti.App.fireEvent("closeButtonBlocker");},1000);
							successCallback();
							exports.PostScoreToFacebook(globals.gameData.phrasesCompleteCountNoCookies);
							
							var AnalyticsController = require("/controller/AnalyticsController");
							AnalyticsController.RegisterEvent({category:"design", event_id: "connectedFacebook"});
						}
					);		
				}
			);      
	    }
    }
	else{
		alert("Oh no. Error Connecting!\nTry again later.");
		Ti.App.fireEvent("closeButtonBlocker");
	}
};


exports.GetMyFacebookInfo = function(successCallback){
	var LocalData = require("model/LocalData");
	
	if(Titanium.Network.online == true){
		if(Titanium.Platform.name == "iPhone OS"){
			Ti.Facebook.appid = 593595274047653;
			Ti.Facebook.permissions = ["publish_actions"];
			Ti.Facebook.forceDialogAuth = false;
			Ti.Facebook.addEventListener('login', ProcessLogin);
			Ti.Facebook.authorize();
		}
		else{
			fb.appid = "593595274047653";
			fb.permissions = ["publish_actions"];
			fb.addEventListener('login', ProcessLogin);
			fb.authorize();
		}
	}
	
	function ProcessLogin(e){
		if (e.success) {
			//Forces authorization every time.
			//fb.forceDialogAuth = true;
			
			Ti.API.info("Facebook: Success in Loggin In");
	  		if(Ti.Platform.name == "iPhone OS") Ti.Facebook.requestWithGraphPath('me?', {fields:"id,name,first_name,last_name"}, 'Get', ProcessResponse );
			else fb.requestWithGraphPath(fb.uid, {fields:"id,name,first_name,last_name"}, 'Get', ProcessResponse );
			
			function ProcessResponse(e1){
				if (e1.success) {
			    	Ti.API.info("Facebook: Success getting ME info");
			    	var data = JSON.parse(e1.result);
			    	LocalData.SaveUserFacebookInfo(data);
			    	if(successCallback) setTimeout(successCallback,250);
			    }
			    else {
			        if (e1.error){
			        	Ti.API.info("Facebook: error getting ME info:"+e1.error);
			        	alert("Oh no!!! There was an error connecting with Facebook.\nPlease try again later.");
			        }	
			       	else Ti.API.info("Facebook: error getting ME info: Unkown result");
			    }
			}
		}
		else{
			Ti.API.info("Facebook: Error in Loggin In");
			Ti.API.info("Oh no!!! There was an error connecting with Facebook.\nPlease try again later.");       
			Ti.App.fireEvent("closeButtonBlocker");
		}
	}
};


exports.GetFacebookFriendsInfo = function(successCallback, errorCallback){
	var LocalData = require("model/LocalData");
	var gameData = LocalData.GetGameData();
	
	if(Ti.Network.online == true){
		if(Ti.Platform.name == "iPhone OS") Ti.Facebook.requestWithGraphPath('me/friends?', {fields:"id,name,first_name,last_name,picture,installed"}, 'Get', ProcessResponse);
		else fb.requestWithGraphPath(fb.uid+"/friends", {fields:"id,name,first_name,last_name,picture,installed"}, 'Get', ProcessResponse);
	}
	else Ti.API.info("No connection");
	
	function ProcessResponse(e){
		 if (e.success) {
	    	var friendsList = JSON.parse(e.result).data;
	    	Ti.API.info("Facebook: Success in getting Friends");
	    	//Ti.API.info(friendsList);
	    	FriendsController.UpdateFriendsInfo(friendsList);
	    	setTimeout(function(){exports.GetScoreOfFriendFromFacebook(successCallback);},250);
	    }
	    else {
	        if (e.error){
	        	Ti.API.info("Facebook: error getting Friends: "+e.error);
	        	Ti.API.info("Oh no!!! There was an error connecting with Facebook.\nPlease try again later.");
	        }	
	       	else Ti.API.info("Facebook: error getting Friends Unkown result");
	       	if(errorCalback) errorCallback();
	    }
	}
};


exports.GetScoreOfFriendFromFacebook = function(successCallback){
	var FriendsController = require("/controller/FriendsController");
	if(Titanium.Network.online == true){
		if(Ti.Platform.name == "iPhone OS") Ti.Facebook.requestWithGraphPath('593595274047653/scores?', {}, 'Get', ProcessResponse);
		else fb.requestWithGraphPath('593595274047653/scores', {}, 'Get', ProcessResponse);
	}
	
	function ProcessResponse(e1){
		 if (e1.success) {
	    	Ti.API.info("Facebook: Success Getting Score");
	    	var data = JSON.parse(e1.result);
	    	FriendsController.UpdateFriendsScore(data.data);
	    	if(successCallback) setTimeout(function(){successCallback(FriendsController.GetFriendsWithAppByScore);},250);
	    }
	    else {
	        if (e1.error){
	        	Ti.API.info("Facebook: error Getting friends Score info:"+e1.error);
	        	//alert("Oh no!!! There was an error connecting with Facebook.\nPlease try again later.");
	        }	
	       	else Ti.API("Facebook: error Getting friends Score Unkown result");
	    }
	}
};


exports.InviteFacebookFriends = function(facebookIDs){
	if(Titanium.Network.online == true){
		if(Ti.Platform.name == "iPhone OS") Ti.Facebook.dialog('apprequests', {message: 'Come play GoPhrazy!', to: facebookIDs}, function(data) { 
			Ti.API.info(data); 
		});
		else fb.dialog('apprequests', {message: 'Come play GoPhrazy!', to: facebookIDs}, function(data) { 
			Ti.API.info(data); 
		});
	}	
};


exports.PostScoreToFacebook = function(score){
	if(Titanium.Network.online == true){
		if(Ti.Platform.name == "iPhone OS") Ti.Facebook.requestWithGraphPath('me/scores?', {score:score}, 'Post', ProcessResponse);
		else fb.requestWithGraphPath(fb.uid+'/scores', {score:score}, 'Post', ProcessResponse);
	}	
	
	function ProcessResponse(e1){
	    if (e1.success) {
	    	Ti.API.info("Facebook: Success Posting Score");
	    	var data = JSON.parse(e1.result);
	    	setTimeout(exports.GetScoreOfFriendFromFacebook,250);
	    }
	    else {
	        if (e1.error){
	        	Ti.API.info("Facebook: error posting Score info:"+e1.error);
	        }	
	       	else Ti.API.info("Facebook: error posting Score Unknown Error");
	    }
	}
};


exports.GetPuzzlesMessages = function(showBar){
	var RTS = require("lib/RTS");
	if(showBar == true) globals.rootWin.OpenWindow("networkBar",{bgColor: "#FFD24D", text:"Getting Messages!"});
	
	RTS.queryItems("PuzzlesMessages", {primary: globals.gameData.userFacebookID},null,null,null,null,false, 
		function(e){
			Ti.API.info("RTS: Got Messages");
			Ti.API.info(e);
			if(e.text != null){
				var result = JSON.parse(e.text);
				if(result.error == null && result != null){
					var MessagesController = require("controller/MessagesController");
					MessagesController.parseMessages(result.data.items);
					globals.gameData.messagesLastUpdateTimeStamp = clock.getNow().toISOString();
					globals.col.gameData.commit();
					Ti.App.fireEvent("GotMessages");
				}
				else Ti.App.fireEvent("ErrorSentMessage");
			}
			else Ti.App.fireEvent("ErrorSentMessage");
		}
	);
	/*
queryItems("PuzzlesMessages", {primary: "1234567"}, null, null, null, null, false, function(e){
	Ti.API.info(e);
});
*/
};


exports.SendCustomPuzzleMessage = function(receiverID, puzzleText, author, senderMessageText, callback){
	var RTS = require("lib/RTS");
	var PushNotificationsController = require("controller/PushNotificationsController");
	var MessagesController = require("controller/MessagesController");
	var pushLengthLimit = 70;//82; Setting it lower as it depends on the chanel name.
	
	//Doing this otherwise server error
	if (senderMessageText == "") senderMessageText = null;
	if (author == "") author = "unknown";
	
	var now = clock.getNow();
	if (Titanium.Network.online == false){
		setTimeout(function(){Ti.App.fireEvent("NoConnection");},1000);
	}
	else{
		RTS.putItem("PuzzlesMessages", {receiverID: receiverID, senderID: globals.gameData.userFacebookID, messageID: now.toISOString(), puzzleText: puzzleText, author: author, senderMessageText: senderMessageText}, 
			function(e){
				Ti.API.info(e);
				if(e.text != null){
					var response = JSON.parse(e.text);
					setTimeout(function(){
						if (response.error == null){
							MessagesController.saveSentMessage(receiverID, puzzleText, now.toISOString(), senderMessageText);
							Ti.App.fireEvent("SentMessage");
							globals.gameData.messagesSentCount++;
							globals.col.gameData.commit();
							
							var AnalyticsController = require("/controller/AnalyticsController");
							AnalyticsController.RegisterEvent({category:"design", event_id: "messageSent", value:globals.gameData.messagesSentCount});
						}
						else {
							Ti.API.info(response);
							Ti.App.fireEvent("ErrorSentMessage");
						}	
					},1000);
					
					if(callback) callback(e);
				}
			}
		);
		
		var sender = globals.col.friends.find({facebookID: globals.gameData.userFacebookID})[0];
		var pushMessage = sender.name+": "+senderMessageText; 
		if (senderMessageText == null || senderMessageText == "") pushMessage = sender.name+": sent you a puzzle.";
		if (pushMessage.length > pushLengthLimit) pushMessage = pushMessage.substring(0,pushLengthLimit);
		PushNotificationsController.SendPushNotificationToUser(receiverID.toString(), pushMessage);
	}
};


exports.SharePhotoOnFacebook = function(dataIn){
	if(globals.gameData.userFacebookID == ""){
		exports.SignUpFacebook(function(){
			exports.GetFacebookFriendsInfo(function(){	
				Ti.App.fireEvent("multiplayerOn");
				exports.GetPuzzlesMessages(true);
				Share();
			});
			//friendsButton.Refresh();
		});
	}
	else{
		Share();
	}
	
	function Share(){
		/*
		fb.requestWithGraphPath('me/photos', dataIn, 'POST', function(e){
		    if (e.success) {
				globals.gameData.shareSolutionData.shareFacebookCount++;
				globals.col.gameData.commit();	       

		        var AnalyticsController = require("/controller/AnalyticsController");
				AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:facebook", area: dataIn.phraseID,value:globals.gameData.shareSolutionData.shareFacebookCount});
		        
		        alert("Great Success!!!\nPosted on Facebook");
		    } else {
		        if (e.error) alert("Mmmm. Some error happend with Facebook.");
		        else alert("Something weird happened.\nPlease try again.");   
		    }
		   // loadingOverlay.close();
		});
		*/
		
		fb.dialog("feed", dataIn, function(e) {
		    if(e.success && e.result) {
		        alert("Success! New Post ID: " + e.result);
		    } else {
		        if(e.error) {
		            alert(e.error);
		        } else {
		            alert("User canceled dialog.");
		        }
		    }
		});
	}
};
