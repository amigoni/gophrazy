var crypt = require("lib/crypt");
var PhrasesController = require("/controller/PhrasesController");
var GroupsController = require("controller/GroupsController");
var DailyChallengeController = require("/controller/DailyChallengeController");
var RandomPlus = require("/lib/RandomPlus");
var LocalNotificationController = require("/controller/LocalNotificationController");
var FriendsController = require("/controller/FriendsController");
var MonstersController = require("/controller/MonstersController");
var StoreController = require("/controller/StoreController");
var Common = require('ui/Common');
var AchievementsController = require("/controller/AchievementsController");
var AnalyticsController = require("/controller/AnalyticsController");
var AdConversionTrackingController = require("controller/AdConversionTrackingController");

var currenti18n = "en";
var localCurrentVersion = Common.GetJsonText("model/"+currenti18n+"/CurrentVersion.txt");
var currentVersionObject = JSON.parse(localCurrentVersion);

globals.testing = false;
globals.completeCountThisSession = 0;


globals.stockData = {
	analytics:{
		userID: "",
		sessionID: ""
	},
	abilities:{ //TUNABLE
		fortuneCookies: { name: "fortuneCookiesUnlocked", unlockable: true,  enabled: false, unlockCount: 2,  visible: true,   title: "Fortune Cookies", description: "Your own daily fortune in a puzzle with special rewards.",   icon: "fortuneCookieIconLarge.png" },
		skipbox:   		{ name: "skipboxUnlocked", 	      unlockable: true,  enabled: false, unlockCount: 5,       			   title: "Skips", 			 description: "Really stuck? How annoying! Skip a puzzle and do it later.", icon: "skipBoxIconLarge.png"},
		hints:     		{ name: "hints", 				  unlockable: true,  enabled: false, unlockCount: 10, 				   title: "Hints", 	 		 description: "Help is here, hints make life easier when you need them.",   icon: "hintIconLarge.png"},
		puzzlePost:		{ name: "puzzlePost", 	  		  unlockable: false,  enabled: false, unlockCount: 15, visible: false,  title: "The Puzzle Post", description: "The free weekly issue of the latest puzzles.", 				icon: "newspaperIcon.png" },
		composePuzzle: 	{ name: "composePuzzle",		  unlockable: false, enabled: true,  unlockCount: 75, 				   title: "Write Your Own",  description: "Write your own puzzles and send them to your friends.",		icon:"composeIcon.png" },
		favorite: 		{ name: "favorite",				  unlockable: false, enabled: true },
		sendPuzzle: 	{ name: "sendPuzzle", 			  unlockable: false, enabled: true }
	},
	ads:{
		bannersEnabled: true,
		interstitialsEnabled: true,
		firstInterstitialAppearsAtPuzzle: 11,
		lastInterstitialDispayTime: "2014-04-13T00:00:00Z",
		puzzlesCompleteCountSinceLastInterstitialAd: 0,
		interstitialCompleteCountFrequency: 500, //5//500 CID 7
		interstitialTimeDeltaFrequency: 1000000, //minutes//10//CID 7 1000000
		interstitialMinimumTimeDeltaFrequency: 3, //Added v1.4
		interstitialCompleteCountFrequencyBoughtPass: 6, //Currently not uses as I show no ads if bought
		interstitialTimeDeltaFrequencyBoughtPass: 20, //minutes. Currently not uses as I show no ads if bought
	},
	currentPhraseID: "",
	currentPhraseDifficultyPosition: 0,
	dailyChallengeCurrentPhraseID: null,
	dailyChallengeComplete: false,
	dailyChallengeExpirationTime: 0, //clock.getNow().hours(13).minutes(3).seconds(33).add("d",1),
	fortuneCookieData: {
		//Reward info
		current:{
			name: "hints",
			count: 0,
			completeCounter: 0
		},
		data: {
			 hints: {max: 3, freq: 1}, //TUNABLE  //HISTORY (v1.2 freq: 4)
			 skips: {max: 3, freq: 1}  //TUNABLE 
		},
		completeCount: 0,
		completeRate: 0,
		unlockDate: "none"
	},
	gameCenterData: {
		id: "",
		alias: ""
	},
	hints: 3,
	hintsUsedCount: 0,
	hintsBoughtCount: 0,
	hintsEarnedCount: 0,
	i18n: "en",
	firstStartComplete: false,
	lastGroupID: "main", // This might need to be cleaned up
	lastPlayedTime: "2014-04-13T00:00:00Z",
	lastClosedTime: clock.getNow().toISOString(),
	lastFortuneCookieSolvedTime: "2014-04-13T00:00:00Z",
	lastNewPuzzleSolvedTime: "2014-04-13T00:00:00Z",
	messagesLastUpdateTimeStamp: "2014-04-13T00:00:00Z",
	messagesSentCount: 0,
	musicOn: true,
	notifications:{
		pushNotificationsInitialized: false,
		announcements: 			{name: "New Puzzle Post Issue", 	 	  type: "announcements", 		  visible: false, enabled: false}, 
		messages: 	   			{name: "A friend sends me a puzzle", 	  type: "messages", 			  visible: false, enabled: false},
		beatScore:     			{name: "A friend beats my score", 		  type: "beatScore", 			  visible: false, enabled: false},
		solvedPuzzle:     		{name: "Someone solves your puzzle", 	  type: "solvedPuzzle", 		  visible: false, enabled: false},//added v1.6
		fortuneCookieAvailable: {name: "Fortune Cookie is Ready", 	 	  type: "fortuneCookieAvailable", visible: false, enabled: false, time: {hour: 9, minute: 14}},
		fortuneCookieExpires:   {name: "Fortune Cookie is gonna expire",  type: "fortuneCookieExpires",   visible: false, enabled: false, time: {hour: 20, minute: 58}}
	},
	openedMessagesWinCount: 0,
	passData:{
		curveBought: 50,//first time and then //TUNABLE //HISTORY (v1.2 - [30,30])
		curveFacebook: 30, //Facebook unlocks 30
		currentlyOnBoughtPass: false,
		currentPassType: "none",
		friendsIncrement: 3,//CID 7: 3 //TUNABLE
		freeDailyPuzzlesAllowed: 3, //TUNABLE //HISTORY (v1.2 - 1)
		freeDailyPuzzlesUsed: 0,
		numberOfFriendsAtUnlock: 0,
		nextPassAt: 50, //This also sets the initial pass
		passOpen: true,
		passBoughtCount: 0,
		passesFriendsCount: 0,
		passes: 0
	},
	phrasesCount: 0,
	phrasesCompleteCount: 0,
	phrasesCompleteCountNoCookies: 0,
	phrasesCompleteCountForPass: 0, //No cookies, no mail.
	phrasesDifficultyCurve: { //TUNABLE ALL THIS OBJECT
		phraseDifficultyRanges: [
			{"low":  0, "high":  4},
			{"low":  5, "high":  7},
			{"low":  8, "high": 10},
			{"low": 11, "high": 13},
			{"low": 14, "high": 16},
			{"low": 17, "high": 99}
		],
		ftu: [
			{"number":  0, "phraseDifficulty": 0},
			{"number":  1, "phraseDifficulty": 0},
			{"number":  2, "phraseDifficulty": 0},
			{"number":  3, "phraseDifficulty": 1},
			{"number":  4, "phraseDifficulty": 1},
			{"number":  5, "phraseDifficulty": 0},
			{"number":  6, "phraseDifficulty": 1},
			{"number":  7, "phraseDifficulty": 1},
			{"number":  8, "phraseDifficulty": 0},
			{"number":  9, "phraseDifficulty": 2}
		],
		ftuPhrases:[ //CID 8
			154,
			422,
			76,
			235,
			104,
			28,
			377,
			215,
			771,
			264
		],
		ftuPhrasesA: [
			322,
			593,
			651,
			586,
			365, // Loosing not for difficulty, Might be too easy
			668, // Loosing not for difficulty, Might be too easy,
			581, // Loosing not for difficulty, Might be too easy, or previous content
			378,
			427,
			251,
			197,
			154,
			144,
			699,
			685,
			604,
			471,
			304,
			468,
			470,
			405,
			350,
			700,
			567,
			137,
			204,
			674,
			701,
			388,
			396,
			72,
			76,
			428,
			440,
			171,
			201,
			227,
			255,
			366,
			531,
			662
		],	
		ftuPhrasesB:[ //CID 8
			154,
			422,
			76,
			235,
			104,
			28,
			377,
			215,
			771,
			264
		],
		ftuPhrases: [
			322,
			593,
			651,
			586,
			365, // Loosing not for difficulty, Might be too easy
			668, // Loosing not for difficulty, Might be too easy,
			581, // Loosing not for difficulty, Might be too easy, or previous content
			378,
			427,
			251,
			197,
			154,
			144,
			699,
			685,
			604,
			471,
			304,
			468,
			470,
			405,
			350,
			700,
			567,
			137,
			204,
			674,
			701,
			388,
			396,
			72,
			76,
			428,
			440,
			171,
			201,
			227,
			255,
			366,
			531,
			662
		],	
		regularA: [
			{number:  0, phraseDifficulty: 0},
			{number:  1, phraseDifficulty: 2},
			{number:  2, phraseDifficulty: 1},
			{number:  3, phraseDifficulty: 3},
			{number:  4, phraseDifficulty: 1},
			{number:  5, phraseDifficulty: 3},
			{number:  6, phraseDifficulty: 1},
			{number:  7, phraseDifficulty: 2},
			{number:  8, phraseDifficulty: 3},
			{number:  9, phraseDifficulty: 1},
			{number: 10, phraseDifficulty: 2},
			{number: 11, phraseDifficulty: 4},
			{number: 12, phraseDifficulty: 1},
			{number: 13, phraseDifficulty: 3},
			{number: 14, phraseDifficulty: 1},
			{number: 15, phraseDifficulty: 5}
		],
		//ChangeID 7 2014-12-22/ Became RegularB in v 1.6
		regularB: [
			{number:  0, phraseDifficulty: 0},
			{number:  1, phraseDifficulty: 2},
			{number:  2, phraseDifficulty: 2},
			{number:  3, phraseDifficulty: 4},
			{number:  4, phraseDifficulty: 1},
			{number:  5, phraseDifficulty: 3},
			{number:  6, phraseDifficulty: 2},
			{number:  7, phraseDifficulty: 2},
			{number:  8, phraseDifficulty: 5},
			{number:  9, phraseDifficulty: 1},
			{number: 10, phraseDifficulty: 3},
			{number: 11, phraseDifficulty: 4},
			{number: 12, phraseDifficulty: 2},
			{number: 13, phraseDifficulty: 4},
			{number: 14, phraseDifficulty: 2},
			{number: 15, phraseDifficulty: 5}
		],
		regular: [ //Old Set
			{number:  0, phraseDifficulty: 0},
			{number:  1, phraseDifficulty: 2},
			{number:  2, phraseDifficulty: 1},
			{number:  3, phraseDifficulty: 3},
			{number:  4, phraseDifficulty: 1},
			{number:  5, phraseDifficulty: 3},
			{number:  6, phraseDifficulty: 1},
			{number:  7, phraseDifficulty: 2},
			{number:  8, phraseDifficulty: 3},
			{number:  9, phraseDifficulty: 1},
			{number: 10, phraseDifficulty: 2},
			{number: 11, phraseDifficulty: 4},
			{number: 12, phraseDifficulty: 1},
			{number: 13, phraseDifficulty: 3},
			{number: 14, phraseDifficulty: 1},
			{number: 15, phraseDifficulty: 5}
		]
	},
	playerLevelsData: [ //TUNABLE
	    {
	        "levelID": 1,
	        "name": "Level 1",
	        "startScore": 0,
	        "endScore": 4
	    },
	    {
	        "levelID": 2,
	        "name": "Level 2",
	        "startScore": 5,
	        "endScore": 9,
	        "bonusData": [
	        	{"type": "ability", "name": "skipbox", "position": 1}
	        ]
	    },
	    {
	        "levelID": 3,
	        "name": "Level 3",
	        "startScore": 10,
	        "endScore": 16,
	        "bonusData": [
	        	{"type": "ability", "name": "hints", "position": 1},
	        ]
	    },
	    {
	        "levelID": 4,
	        "name": "Level 4",
	        "startScore": 17,
	        "endScore": 24,
	        "bonusData": [
	        	{"type": "ability", "name": "puzzlePost", "position": 1},
	        ]
	    },
	     {
	        "levelID": 5,
	        "name": "Level 5",
	        "startScore": 25,
	        "endScore": 49,
	        "bonusData": [
	        	{"type": "ability", "name": "fortuneCookies", "position": 1},
			]
	    },
	     {
	        "levelID": 6,
	        "name": "Level 6",
	        "startScore": 50,
	        "endScore": 999999,
	        "bonusData": [
	        	{"type": "ability", "name": "composePuzzle", "position": 1},
			]
	    }
	],
	ratedComplete: false,
	ratedThisVersion: false,//Added v1.4
	ratePopUpFirst: 7, //number of sessions Added v1.5 
	ratePopUpFrequency: 7, //number of sessions Added v1.5
	ratePopUpTitle: "Please Rate GoPhrazy!",
	ratePopUpMessage: "Love GoPhrazy? Please Rate us and help spread the word",
	sessionsCount: 0,
	sfxOn: true,
	shareSolutionData:{
		shareEmailCount: 0,
		shareFacebookCount: 0,
		shareTwitterCount: 0
	},
	skipboxLimit: 5, //TUNABLE
	skipboxUpgradeLimit: 15, //TUNABLE
	skips: 3,
	skipsBoughtCount: 0,
	skipsEarnedCount: 0,
	skipsUsedCount: 0,
	starsEnabled: false, //added v1.6
	storeData:[
		{itemID:"hint_1", name:"Hint", type:"hints", icon:"", quantity:3, price: 0.99, isBestDeal:"FALSE", isMostPopular:"FALSE"},
		{itemID:"pass_1",name:"Pass", type:"pass", icon :"", quantity:1, price: 0.99, isBestDeal:"FALSE", isMostPopular:"FALSE"},
		{itemID:"skip_1",name:"Skip",type:"skip", icon:"", quantity:3, price: 0.99, isBestDeal:"FALSE", isMostPopular:"FALSE"},
		{itemID:"skipbox_upgrade_1",name:"skipbox Upgrade", type:"upgrade", icon:"skipbox", quantity:1, price: 1.99, isBestDeal:"FALSE", isMostPopular:"FALSE"},
		{itemID:"compose_puzzle_early_access", name:"Compose Puzzles Early Access", type:"upgrade", icon:"",quantity:1, price: "0.99", isBestDeal:"FALSE", isMostPopular:"FALSE"},
		{itemID:"mail_upgrade_1", name:"Mailbox Upgrade", type:"upgrade", icon:"", quantity:1, price: 1.99, isBestDeal:"FALSE", isMostPopular:"FALSE"}
	],
	style: {
		currentWordBoxesPalette: "standard"
	},
	timeChallengeEnabled: false, //added v1.6
	tips:[ //Added 1.5, changed CID 10
		{text:"Have you sent a puzzle to a friend yet?", frequency: 5, beforeConnection: true},
		{text:"Express yourself!\nMake your own puzzle in the Puzzle Factory!", frequency: 5, beforeConnection: true},
		{text:"When you create a puzzle a link is generated.\nPeople can click on it to solve it.", frequency: 3, beforeConnection: true},
		{text:"You can see how many people solved your puzzles in the Puzzle Factory", frequency: 3, beforeConnection: true},
		{text:"Want to play with your friends?\nShare your puzzles!\nFacebook, Twitter, Email, SMS ", frequency: 2, beforeConnection: true},
		{text:"Like a puzzle you solved?\nShare it by Email, Facebook or Twitter.", frequency: 3, beforeConnection: false},
		{text:'Need help solving a puzzle?\nPress the "Friends Help"', frequency: 3, beforeConnection: false},
		{text:"Send a puzzle to a friend today.\nThey'll be happy!", frequency: 5, beforeConnection: false},
		//{text:"Complete any Puzzle Post to get\nfree Hints or Skips", frequency: 5, beforeConnection: false},
		{text:"Why not write a puzzle to a loved one!", frequency: 5, beforeConnection: false},
		//{text:"Pass your friends' score and show off your greatness.", frequency: 2, beforeConnection: false},
		{text:"The word boxes colors of a puzzle are random.", frequency: 1, beforeConnection: true},
		{text:"Puzzles start with a Capitalized word.", frequency: 1, beforeConnection: true},
		{text:"Puzzles always end with a . or ? or !", frequency: 1, beforeConnection: true},
		{text:"The ; is used to split phrases.", frequency: 1, beforeConnection: true},
		{text:"Got an issue or idea?\nClick the megaphone to email us!", frequency: 1, beforeConnection: true},
	],
	tutorialOn: false,
	currentTutorial: null,
	tutorialsData:{ //Added v1.4
		fortuneCookiesOpened: false,
		mailboxOpened: false,
		writePuzzleTabOpened: false,
		composeOpened: false,
		puzzleBookOpened: false,
		puzzlePostOpened: false,
		skipboxOpened: false
	},
	upgradeTime: "2014-04-13T00:00:00Z",
	upgradeTimeLast: "2014-04-10T00:00:00Z",
	userName: "", //added v1.6
	userFacebookID: "",
	userFacebookName:"",
	userLevel: 1,
	vibrateOn: true,
	versions:{
		appVersion: currentVersionObject.appVersion,
		phrasesVersion: currentVersionObject.phrasesVersion,
		groupsVersion: currentVersionObject.groupsVersion,
		changesVersion: 0, //    Leave at 0 so that if it's not there it starts from the first change. From v1.3 on first version didn't have it. Used to make sure you are at the rigth tuned version
		gameDataVersion: 0, //   THESE TWO SHOULD BE THE SAME
		ABTestingGroup: ""
	}
};


exports.InitData = function(fromBackground, callback){
	var quella = KeyGen();
	//Prevent some hacking;
	function KeyGen(){
		var one = "uno";
		var two = "due";
		return Ti.Utils.base64encode(one+two);
	}
	
	var jsondb = require("lib/com.irlgaming.jsondb");
	jsondb.debug(false);
	
	//Create Language Folder for Data
	var newFolder = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, currenti18n);
	if( !newFolder.exists() )  newFolder.createDirectory();
	var languageFolder = newFolder.resolve();

	globals.col = {};
	globals.col.gameData = jsondb.factory('gameData', quella);
	globals.col.phrases = jsondb.factory('phrases', quella, languageFolder);
	globals.col.phrases.ensureIndex({phraseID:1});
	globals.col.issues = jsondb.factory('issues', quella, languageFolder);
	globals.col.groups = jsondb.factory('groups', quella, languageFolder);
	globals.col.worlds = jsondb.factory('worlds', quella, languageFolder);
	globals.col.colors =  jsondb.factory('colors', quella, languageFolder);
	globals.col.notifications =  jsondb.factory('notifications', quella, languageFolder);
	globals.col.wMessages = jsondb.factory('wMessages', quella, languageFolder);
	globals.col.messages = jsondb.factory('messages', quella, languageFolder);
	globals.col.friends = jsondb.factory('friends', quella, languageFolder);
	globals.col.friends.ensureIndex({facebookID:1});
	globals.col.monsters = jsondb.factory('monsters', quella, languageFolder);
	globals.col.storeItems = jsondb.factory("storeItems", quella, languageFolder);
	globals.col.receipts = jsondb.factory('receipts', quella, languageFolder);
	globals.col.achievements = jsondb.factory("achievements", quella, languageFolder);
	globals.col.changes = jsondb.factory("changes", quella, languageFolder);
	globals.col.analyticsEvents = jsondb.factory("analyticsEvents", quella);
	
	globals.storeController = new StoreController();
	
	//First Boot Setup
	if (globals.col.gameData.sizeOf() == 0){
		var now = clock.getNow();
		var weekOf = clock.getNow().day(1).hour(0).minute(0).second(0).millisecond(1);
		if(now.day() == 0) weekOf.subtract("d",7);
			
		globals.col.gameData.save({
			systemData: true,
			currenti18n: currenti18n,
		});
		globals.systemData = globals.col.gameData.find({systemData: true})[0];
		
		StyleSheet.InitColors();
		
		//Initialize Local Data
		globals.stockData.analytics.userID = crypt.encrypt(Ti.Platform.id); //exports.GenerateGUID();
		globals.stockData.analytics.sessionID = exports.GenerateGUID();
		globals.stockData.phrasesCount = globals.col.phrases.sizeOf();
		globals.col.gameData.save(globals.stockData);
		globals.col.gameData.commit();
		globals.gameData = globals.col.gameData.find({i18n:globals.systemData.currenti18n})[0];
		
		PhrasesController.InitPhrases(callback);
		GroupsController.InitGroups(); 	
		//For some reason teh first time you go the background the BGService is not initialized.
		LocalNotificationController.InitNotifications();
		globals.storeController.InitLocalStoreItems();
		AdConversionTrackingController.MakeCall();
	}
	
	globals.systemData = globals.col.gameData.find({systemData: true})[0];
	globals.gameData = globals.col.gameData.find({i18n:globals.systemData.currenti18n})[0];
	
	if(fromBackground == false) {
		globals.gameData.sessionsCount++;
		RefreshSessionID();
		globals.storeController.Init(); //Do it here After The local have been initialized first
		
		//Must stay here to be run every time you boot.
		//AchievementsController.Init();
		
		//Game Settings
		globals.gameSettings = {};
		globals.gameSettings.dailyChallengeData = [
			{dayCount:1, value: 25},
			{dayCount:2, value: 35},
			{dayCount:3, value: 55},
			{dayCount:4, value: 75},
			{dayCount:5, value: 100}
		];
		
		//Register User Event
		AnalyticsController.RegisterEvent({
			category: "user",
			//user_id already in default
			//session_id already in default
		    friend_count : FriendsController.GetNumberOfFriendsWithApp(),
		    facebook_id: globals.gameData.userFacebookID,
		    ios_id: crypt.encrypt(Ti.Platform.id),
		    platform : Ti.Platform.osname,
		    device   : Ti.Platform.model,
		    //os_major : ,
		    os_minor : Ti.Platform.version,
		    //install_publisher: "Chartboost",
		    //install_site : "Facebook",
		    //install_campaign : "Launch",
		    //install_ad : "Launch Ad 1" 
		});
		
		//Temporary fix for bad date my mistake
		var change2 = globals.col.changes.find({changeID:2})[0];
		if(change2 != null) change2.timeStamp = new moment("2014-11-19T00:00:00Z").valueOf();
		globals.col.changes.commit();
		
		globals.col.gameData.commit();
		
		PhrasesController.CheckPhrasesForStars();
	
		callback();
	}
	
	
	function RefreshSessionID(){
		globals.gameData.analytics.sessionID = exports.GenerateGUID();
		globals.col.gameData.commit();
	}
	
	//Assign A/B Testing Group
	if(globals.gameData.versions.ABTestingGroup == ""){
		var randomNumber = Math.random();
		if( randomNumber < 0.5) globals.gameData.versions.ABTestingGroup = "A";
		else globals.gameData.versions.ABTestingGroup = "B";
		globals.col.gameData.commit();
	}
};


exports.UpgradeGameDataFromLocal = function(){
	//Fills in all the values that are not present. 
	//WARNING this does not delete nor Update existing values
	//a solutions which does that would be preferable
	FillObject(globals.stockData, globals.gameData);
	//DON'T UPDATE ONLY FILL AND CLEAN FROM STOCKDATA AS IT WILL OVERRIDE ALL THE PLAYERS VALUES 
	//THIS MEANS THAT THE GAMEDATA VERSION NUMBER AND UPDATES WILL ONLY COME THROUGH THE CHANGE FILE. 
	//SO YOU MUST INCLUDE THE CHANGES YOU MAKE IN THE CHANGES FILE TOO 
	CleanObject(globals.gameData, globals.stockData);
	globals.gameData.ratedThisVersion = false;
	globals.col.gameData.commit();
	
	Ti.API.info("UPGRADED GAMEDATA from client file, GAMEDATA:");	
	//Ti.API.info(globals.gameData);
};


exports.UpgradeGameDataFromChangesFile = function(changesData){
	Ti.API.info("UPGRADING GAMEDATA from ChangesFile, CHANGES:");	
	//Ti.API.info(changesData);
	
	FillObject(changesData.changes, globals.gameData);
	UpdateObject(changesData.changes, globals.gameData);
	globals.gameData.versions.gameDataVersion = changesData.version;
	if(globals.gameData.ads.lastInterstitialDispayTime == "2014-04-13T00:00:00Z") globals.gameData.ads.lastInterstitialDispayTime = clock.getNow().toISOString();
	globals.col.gameData.commit();
	
	Ti.API.info("UPGRADED GAMEDATA from ChangesFile");	
	//Ti.API.info(globals.gameData);
};


function FillObject(from, to) {
    for (var key in from) {
        if (from.hasOwnProperty(key)) {
            if (Object.prototype.toString.call(from[key]) === '[object Object]') {
                if (!to.hasOwnProperty(key)) {
                    to[key] = {};
                }
                FillObject(from[key], to[key]);
            }
            else if (!to.hasOwnProperty(key)) {
                to[key] = from[key];
            }
        }
    }
};


function CleanObject(starting, finalObject){
	for (var key in starting) {
        if (starting.hasOwnProperty(key)){
        	if (Object.prototype.toString.call(starting[key]) === '[object Object]'){
        		if (!finalObject.hasOwnProperty(key)) delete starting[key];
        		else CleanObject(starting[key], finalObject[key]);
        	}	
        	else if (!finalObject.hasOwnProperty(key)) delete starting[key];
        }
    }
};


function UpdateObject(newData, oldData){
	for (var key in newData) {
        if (newData.hasOwnProperty(key)) {
            if (Object.prototype.toString.call(newData[key]) === '[object Object]') {
                UpdateObject(newData[key], oldData[key]);
            }
            else if (oldData.hasOwnProperty(key)) {
                oldData[key] = newData[key];
            }
        }
    }
};

/*
function TestUpdate(){
	var startingData = {
		test1:{test1a:0},
		test2: 0
	};
	
	var updateData = {
		test1:{test1a:1},
		test2: 1
	};

	Ti.API.info(startingData);

	UpdateObject(updateData,startingData);	
 
	Ti.API.info(startingData);
}
*/
//TestUpdate();

exports.ResetData = function(){
	var documentsFolder = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory);
	var files = documentsFolder.getDirectoryListing(); 
    //Ti.API.info(files);
    for (var i = 0; i < files.length; i++){ 
    	var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, files[i]);
    	file.deleteFile();
    	file.deleteDirectory();
    }
};


exports.GenerateGUID = function(){
    //Got this on Stack overflow
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x7|0x8)).toString(16);
    });
    return uuid;
};


exports.SetCurrentGlobalLevelNumber = function(currentGlobalLevelNumber){
	globals.col.gameData.update(
		{$id: globals.gameData.$id},
		{$set : {currentGlobalLevelNumber:currentGlobalLevelNumber}}, 
		{}, 
		true
	);
	
	globals.col.gameData.commit();
	
	if (globals.gameData.userFacebookID != ""){
		var ServerData = require('model/ServerData');
		ServerData.PostLevelToFacebook(currentGlobalLevelNumber);
	}
	
	Ti.API.info("CurrentGlobalLevelNumber: "+currentGlobalLevelNumber);
}; 


exports.GetGameData = function (){
	return globals.gameData;
};


exports.GetCurrentUserLevelDataForPoints = function(points){
	var levels = globals.gameData.playerLevelsData;
	
	var returnObject = {};
	
	for (var i = 0; i < levels.length; i++){
		var data = levels[i];
		if (points >= data.startScore && points <= data.endScore)
		{
			returnObject.range = data.endScore-data.startScore+1;
			returnObject.fraction = (points-data.startScore)/returnObject.range;
			returnObject.end = data.endScore;
			returnObject.start = data.startScore;
			returnObject.level = data.levelID;
			returnObject.bonusData = data.bonusData;
			break;
		}
	}
	
	return returnObject;
};


exports.SaveUserFacebookInfo = function(data){
	var gameData = globals.gameData;
	
	gameData.userFacebookID = data.id;
	gameData.userFacebookName = data.name;
	globals.gameData.abilities.sendPuzzle.enabled = true;
	globals.col.gameData.commit();
	Ti.API.info(data);
	var me = globals.col.friends.find({facebookID: data.id})[0];
	if(me == null){
		globals.col.friends.save({
			facebookID: data.id,
			firstName: data.first_name,
			lastName: data.last_name,
			name: data.name,
			installed:true,
			//thumbnailURL: data.picture.data.url,
			onTeam: true,
			isMe: true
			}
		);
		
		globals.col.friends.commit();
	}
};


exports.ChangeHints = function(number){
	var AnalyticsController = require("/controller/AnalyticsController");
	var gameData = globals.gameData;
	var hints = gameData.hints+number;
	
	if (number == -1) {
		gameData.hintsUsedCount++;
		
		AnalyticsController.RegisterEvent({category:"design", event_id: "hintsUsed",  value: gameData.hintsUsedCount});
	}	
	if (hints < 0 ) hints = 0;
	globals.col.gameData.update(
		{$id:gameData.$id},
		{$set : {hints:hints}},
		{},
		true
	);
	
	globals.col.gameData.commit();
	
	Ti.App.fireEvent("updateHints", {amount:globals.gameData.hints});
};


/////Phrases
exports.CreateShuffleBagOfPhrases = function(data, isFake){
	return PhrasesController.CreateShuffleBagOfPhrases(data, isFake);
};

exports.GetNextPhrase = function(){
	return PhrasesController.GetNextPhrase();
};

exports.RemovePhraseFromShuffleBag = function(item){
	PhrasesController.RemovePhraseFromShuffleBag(item);
};


exports.CreateHelpOthersPhrasesGroup = function (helpOthersPhrases, selectedPhrase){
	return PhrasesController.CreateHelpOthersPhrasesGroup(helpOthersPhrases, selectedPhrase);
};


exports.CompleteHelpOthersMyPhrase = function(phraseData){
	PhrasesController.CompleteHelpOthersMyPhrase(phraseData);
};	


exports.CompletePhrase = function (data,groupData){
	PhrasesController.CompletePhrase(data,groupData);
};

/////
function GetJsonText (fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
	if (readFile.exists()) {
		var readContents = readFile.read();
	    return readContents.text;  
	}
};


