var image_name = "GoPhrazy.png";
var globals = {};
globals.platform = {};
globals.startTime = new Date().getTime();
globals.multiplayerEnabled = false;
globals.os = Ti.Platform.osname != "android" ? "ios" : "android";
if (globals.os == "ios"){
	if (Ti.Platform.displayCaps.platformHeight == 568) globals.device = "iPhone5";
	else if(Ti.Platform.displayCaps.platformHeight == 480) globals.device = "iPhone4";
}

globals.network = {};
globals.network.online = Ti.Network.online;
globals.network.typeName = Ti.Network.networkTypeName;

Ti.Network.addEventListener("change", function(e){
	globals.network.online = Ti.Network.online;
	globals.network.typeName = Ti.Network.networkTypeName;
});


var _ = require("lib/underscore-min");
var Animator = require("com.animecyc.animator");
var StyleSheet = require("ui/StyleSheet");
var Common = require('ui/Common');
var fb = require('facebook');
var crypt = require("lib/crypt");
var moment = require("lib/moment.min");
moment.lang('en', {
    calendar : {
        lastDay : '[Yesterday]',
        sameDay : 'LT',
        nextDay : '[Tomorrow]',
        lastWeek : 'dddd',
        nextWeek : 'dddd',
        sameElse : 'L'
    }
});

var clock = require("lib/clock");
var CustomURLController = CustomURLController = require("/controller/CustomURLController");


(function(){
	var LoadingWin = require("ui/LoadingWin");
	var win = LoadingWin.loadWin();
	win.open();
})();
