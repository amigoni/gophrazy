function Create(){
	var returnObject = {
		openPopUp: false,
		data: {}
	};
	
	var upgradeTime = new moment(globals.gameData.upgradeTimeLast);
	Ti.API.info("WHATSNEWCONTROLLER: UPGRADE TIME "+upgradeTime.toISOString());
	//Save the new upgrade Date so the page doesn't display every time
	
	var changes = globals.col.changes.find({timeStamp:{$gte:upgradeTime.valueOf()}},{$sort: {timeStamp: -1}});
	Ti.API.info("WHATSNEWCONTROLLER: CHANGES");
	//Ti.API.info(changes);
	//var changes = globals.col.changes.getAll();	
	if(changes.length > 0){
		var updatesSubtitleText = "";
		for(var i = 0; i < changes.length; i++){
			updatesSubtitleText += changes[i].description+"\n";
		}
		
		returnObject.data.updates = {};
		returnObject.data.updates.subtitleText = updatesSubtitleText;
	}
	
	var puzzlePosts = globals.col.groups.find({type: "collection", kind: "issue", releaseDate:{$gte: upgradeTime.valueOf()}});
	Ti.API.info("WHATSNEWCONTROLLER: PUZZLE POSTS");
	//Ti.API.info(puzzlePosts);
	//var puzzlePosts = globals.col.groups.find({type: "collection", kind: "issue"});
	if(puzzlePosts.length > 0){
		var subtitleText = "";
		for(var i = 0; i < puzzlePosts.length; i++){
			subtitleText += '"'+puzzlePosts[i].title+'"';
			if(i < puzzlePosts.length-1) subtitleText += ", ";
		}
		
		returnObject.data.puzzlePost = {
			iconData: {
				backgroundImage:"/images/newspaperIcon.png",
				width: 40,
				height: 30,
				top: 10
			},
			titleText: "Puzzle Post +"+puzzlePosts.length,
			subtitleText: subtitleText
		};
	}
	
	var friends = globals.col.friends.find({installed: true, lastScoreGain:{$gt:0}},{$sort:{lastScoreGain:-1}});
	//var friends = globals.col.friends.find({installed: true});
	Ti.API.info("WHATSNEWCONTROLLER: FRIENDS");
	//Ti.API.info(friends);
	var friendsShouldDisplay = false;
	if(friends[0] != null){
		friendsShouldDisplay = true;
		var subtitleText = "";
		for(var i = 0; i < friends.length; i++){
			subtitleText += friends[i].firstName+" +"+friends[i].lastScoreGain;
			if(i < friends.length-1) subtitleText += ", ";
		};	
			
		returnObject.data.friends = {
			iconData: {
				backgroundImage:"/images/friendsIconLarge.png",
				width: 40,
				height: 37,
				top: 5
			},
			titleText: "Top Gainers",
			subtitleText: subtitleText
		};
	}
	
	if(globals.gameData.firstStartComplete == true && (changes.length > 0 || puzzlePosts.length > 0 || friendsShouldDisplay == true)) returnObject.openPopUp = true;
	
	function UpdateUpgradeLastTime(){
		globals.gameData.upgradeTimeLast = globals.gameData.upgradeTime;
		globals.col.gameData.commit();	
	}
	
	returnObject.UpdateUpgradeLastTime = UpdateUpgradeLastTime;
	
	//This is here to avoid the update of the time before the window opens.
	setTimeout(UpdateUpgradeLastTime, 10000);
	
	
	return returnObject;
}

module.exports = Create;