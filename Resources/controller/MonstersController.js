exports.InitMonsters = function(){
	var monsters = JSON.parse(GetJsonText("model/MonstersData.txt"));
	
	for (var i = 0; i < monsters.length; i++){
		var monster = monsters[i];
		
		globals.col.monsters.save({
			monsterID: monster.monsterID,
			name: monster.name,
			description: monster.description,
			gender: monster.gender,
			family: monster.family,
			image: monster.image,
			locked: true,
			forRandom: monster.forRandom == "false" ? false : true
		});
	}
	
	globals.col.monsters.commit();
};


exports.GetMonstersData = function(){
	return globals.col.monsters.getAll();
};


exports.GetMonster = function(monsterID){
	var returnObject;
	if (monsterID == null || monsterID != "random") returnObject = globals.col.monsters.find({monsterID: monsterID})[0];
	else if (monsterID == "random"){
		returnObject = globals.col.monsters.find({locked: true, forRandom: true}, {$sort: {monsterID: 0}})[0];
		if (returnObject == null) Ti.API.info("You got all the monsters already");
	}
	
	if (returnObject.locked == true){
		returnObject.locked = false;
		globals.col.monsters.commit();
	}
	
	return returnObject;
};


function GetJsonText (fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
 
	if (readFile.exists()) {
		var readContents = readFile.read();
	   	return readContents.text;
	}
};



exports.GetFamiliesData = function(){
	var returnObject = [];
	var monsters = globals.col.monsters.getAll();
	var families = [];
	
	for (var i = 0; i < monsters.length; i++){
		var monster = monsters[i];
		var familyName = monster.family;
		
		var familyExists = false;
		for (var j = 0; j < families.length; j++){
			if (familyName == families[j].name) {
				AddMonsterDataToFamily(monster, families[j]);
				familyExists = true;
				break;
			}	
		}
		
		if (familyExists == false){
			var newFamily = CreateFamilyObject(familyName);
			AddMonsterDataToFamily(monster, newFamily);
			families.push(newFamily);
		}
	}
	
	returnObject = families;
	
	function CreateFamilyObject(name){
		var object = {
			name: name,
			monstersCount: 0,
			monstersUnlockedCount: 0,
			monstersData: []
		};
		
		return object;
	}
	
	function AddMonsterDataToFamily(monster, family){
		family.monstersCount++;
		if (monster.locked == false) family.monstersUnlockedCount++;
		family.monstersData.push(monster);
	}
	
	return returnObject;
};
