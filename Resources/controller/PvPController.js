/*
The following things need to happen for a pvp
- Initiate a game
 - Random Phrases get picked, time of game and other info is set.
- Select Opponent or Pick Randomly from server
- Initiator Player plays the first round.
 -Result gets updated on server.
- Game is initiated on server. 
 -Opponent gets notified.

- Opponent plays first round.
 - Server updated.
 - Initiator gets notified.
--This repeats three times.


----- Match Object
var match = {
	status: "open", //open, complete
	startedAt: 1234,
	completedAt: 1234,
	updatedAt: 1234,
	initiatorID: "123",
	opponentID: "123"
	maxRounds: 3, // is length
	currentRound: 1, //starts at 0
	currentTurn: "opponent",
	winner: "nobody", //ID of Winner
	results:[
		{initiatorScore: 123, opponentScore: 122, winner: "initiator"},
		{initiatorScore: 122, opponentScore: null, winner: null}
	]
} 


--- Methods

-InitiateMatch
-UpdateMatch
-GetMatches
  
*/