exports.parseMessages = function(messages){
	var SoundController = require("controller/SoundController");
	var FriendsController = require("controller/FriendsController");
	var mailboxGroup = globals.col.groups.find({type:"mailbox"})[0];
	
	var oldCount = globals.col.messages.count({sent:false, complete: false});
	
	for (var i = 0; i < messages.length; i++){
		var message = messages[i];
		var match = globals.col.messages.find({messageID: message.messageID})[0];
		if (match == null){
			var sender = globals.col.friends.find({facebookID: message.senderID})[0];
			var receiver = globals.col.friends.find({facebookID: message.receiverID})[0];
	
			globals.col.messages.save({
				receiverID: message.receiverID,
				receiverName: receiver.name, 
				senderID: message.senderID,
				senderName:sender.name,
				messageID: message.messageID,
				puzzleText: message.puzzleText,
				senderMessageText: message.senderMessageText == "null" ? null : message.senderMessageText,
				createdTime: message.messageID,
				isNew: true,
				complete: false,
				sent: false
			});
			
			var textArray = crypt.decrypt(message.puzzleText).match(/\S+\s/g);	
			
			globals.col.phrases.save({
				createdDate: message.messageID, // The ID is the UTC time in ISOString format
				phraseID: message.messageID,
				text: message.puzzleText,
				author: message.author,
				mediaName: null,
				link: null,
				type: "message",
				topic: null,
				topicTitle: null,
				length: message.puzzleText.length,
				numberOfWords: textArray.length,
				difficulty: textArray.length,
				isFunny: null,
				includeInGeneral: false,
				complete: false,
				completeCount: 0,
				isBeingUsed: false,
				favorite: null,
				receiverIDs: [],
				skipped: false,
				groupID: null,
				eventID: null,
				hide: false,
				written: false,
				timeToSolve: {
					firstTime: 0,
					lastTime: 0,
					bestTime: 0,
					currentTime: 0
				}
			});
			
			var group = globals.col.groups.find({type:"mailbox"})[0];
			
			var phraseIDMatch = false;
			for (var i = 0; i < group.phrasesIDs.length; i++){
				var phraseID = group.phrasesIDs[i];
				if (phraseID == message.messageID) {
					phraseIDMatch = true;
					break;
				}
			}
			
			if (phraseIDMatch == false) group.phrasesIDs.push(message.messageID);
		}
	}
	
	globals.col.messages.commit();
	globals.col.phrases.commit();
	globals.col.groups.commit();
	
	//Delay otherwise the icon listener is not ready at startup
	var newCount = globals.col.messages.count({sent:false, complete: false});
	
	setTimeout( function(){
		if(newCount != oldCount) {
			if(globals.gameData.vibrateOn == true) Ti.Media.vibrate([ 0, 1000]);
			SoundController.playSound("wordsAppear");
		}
		Ti.App.fireEvent("updateMessagesCount", {count: newCount});
	},2000);
};


exports.saveSentMessage = function(receiverID, puzzleText, messageID, senderMessageText){
	globals.col.messages.save({
		receiverID: receiverID,
		receiverName: globals.col.friends.find({facebookID:receiverID})[0].name, 
		senderID: globals.gameData.userFacebookID,
		senderName: globals.col.friends.find({facebookID:globals.gameData.userFacebookID})[0].name, 
		messageID: messageID,
		puzzleText: puzzleText,
		senderMessageText: senderMessageText,
		createdTime: messageID,
		isNew: false,
		complete: true,
		sent: true
	});
	
	globals.col.messages.commit();
};


exports.getMessages = function(){
	return JSON.parse(JSON.stringify(globals.col.messages.find({sent: false})));
};

exports.getMessagesForMailbox = function(){
	var returnObject = [];
	var messages = globals.col.messages.find({sent:false},{$sort:{createdTime:3}});
	var incomplete = [];
	var complete =[];
	
	for (var i = 0; i < messages.length; i++){
		var message = messages[i];
		if (message.complete == false) incomplete.push(message);
		else complete.push(message);
	}
	
	returnObject = incomplete.concat(complete);
	
	return returnObject;
};


exports.CheckIfMessageWasAlreadySent = function(receiverID, puzzleText){
	var alreadySent = false;
	var match = globals.col.messages.find({sent: true, receiverID: receiverID, puzzleText: puzzleText});
	if (match.length > 0) alreadySent = true;
	return alreadySent;
};


exports.CompletePhraseSendNotification = function (phraseID){
	var PushNotificationsController = require("/controller/PushNotificationsController");
	var message = globals.col.messages.find({messageID:phraseID})[0];
	var me = globals.col.friends.find({isMe:true})[0];
	PushNotificationsController.SendPushNotificationToUser(message.senderID, me.firstName+" just solved your puzzle!");
};
