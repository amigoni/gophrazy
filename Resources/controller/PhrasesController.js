var RandomPlus = require("/lib/RandomPlus");

exports.CompletePhrase = function (data, groupData){
	var SoundController = require("controller/SoundController");
	var LocalData = require("model/LocalData");
	var ServerData = require("model/ServerData");
	var PassController = require("controller/PassController");
	var MessagesController = require("controller/MessagesController");
	var AchievementsController = require("controller/AchievementsController");
	var AnalyticsController = require("/controller/AnalyticsController");
	var PuzzleFactoryController = require("/controller/PuzzleFactoryController");
	var PushNotificationsController = require("/controller/PushNotificationsController");
	
	var phrase = globals.col.phrases.find({$id:data.$id})[0];
	var gameData = globals.gameData;
	var now = clock.getNow();
	
	//if (phrase.complete == false)
	{
		//Phrase Work
		globals.completeCountThisSession++;
		phrase.complete = true;
		phrase.completeCount++;
		if(phrase.completeCount == 1) phrase.timeToSolve.firstTime = phrase.timeToSolve.currentTime;
		if (phrase.timeToSolve.currentTime < phrase.timeToSolve.bestTime || phrase.timeToSolve.bestTime == 0) phrase.timeToSolve.bestTime = phrase.timeToSolve.currentTime;
		phrase.timeToSolve.lastTime = phrase.timeToSolve.currentTime;
		phrase.timeToSolve.currentTime = 0;
		if(phrase.completeCount <= 1) phrase.completeTime = now.valueOf();
		//if(phrase.completeCount <= 1)
		{
			//Group Work
			if (groupData.type == "skipbox"){
				Processskipbox();
				ProcessCollection();
				ProcessLevel();
			}
			else if (groupData.type == "general" && globals.tutorialOn != true){
				globals.gameData.currentPhraseDifficultyPosition = GetNewDifficultyPosition();
				Ti.API.info("Next Phrase Difficulty Position "+globals.gameData.currentPhraseDifficultyPosition);
				Processskipbox();
				ProcessLevel();
			} 
			else if ((groupData.type == "collection") && globals.tutorialOn != true){
				Processskipbox();
				ProcessCollection();
				ProcessLevel();
			}
			else if (groupData.type == "mailbox" && globals.tutorialOn != true){
				var message = globals.col.messages.find({messageID: groupData.currentPhrase.phraseID})[0];
				message.complete = true;
				globals.col.messages.commit();
				MessagesController.CompletePhraseSendNotification(phrase.phraseID);
				Ti.App.fireEvent("updateMailboxComplete",{messageID: message.messageID});
				Ti.App.fireEvent("updateMessagesCount", {count: globals.col.messages.count({sent: false, complete: false})});
			}
			else if (groupData.type == "dailyChallenge"){
				var DailyChallengeController = require("/controller/DailyChallengeController");
				DailyChallengeController.CompleteDailyChallengeSimple(data.phraseID);
			}
		} 
		
	
		//GameData Work
		if(phrase.completeCount <= 1) {
			globals.gameData.phrasesCompleteCount = globals.col.phrases.count({complete: true, type:{$nin: ["tutorial","message"]}});
			globals.gameData.phrasesCompleteCountNoCookies = globals.col.phrases.count({complete: true, type:{$nin:["fortuneCookie","message","tutorial","written"]}});
			globals.gameData.phrasesCompleteCountForPass = globals.gameData.phrasesCompleteCountNoCookies;
			Ti.API.info("Pass Count:"+globals.gameData.phrasesCompleteCountForPass);
			
			//Use the freedaily pass if the phrase is in general(also in the skipbox) or it's part of a group
			if(globals.gameData.passData.passOpen == false && (phrase.includeInGeneral == true || phrase.groupID != null)) {
				Ti.API.info("Free Daily Pass: Using a daily Pass");
				globals.gameData.passData.freeDailyPuzzlesUsed++;
			}	
			
			//Achievements
			if(phrase.type != "tutorial"){
				//AchievementsController.PhraseCompleteCheck(phrase); //LEAVE HERE BEFORE YOU UPDATE THE TIMES
				globals.gameData.lastNewPuzzleSolvedTime = now.toISOString();
				globals.AdsController.CheckToShowInterstitial();
				if(phrase.type == "fortuneCookie") globals.gameData.lastFortuneCookieSolvedTime = now.toISOString();
			}

			AnalyticsController.RegisterEvent({category:"design", event_id: "completePuzzle:totalCount", area: phrase.phraseID.toString(), value: globals.gameData.phrasesCompleteCount});
			AnalyticsController.RegisterEvent({category:"design", event_id: "completePuzzle:completeCount", area: phrase.phraseID.toString(), value: phrase.completeCount});
			AnalyticsController.RegisterEvent({category:"design", event_id: "completePuzzle:completeTime", area: phrase.phraseID.toString(), value: phrase.completeTime});
			
			//Funnel OnBoarding
			//Using phrasesCompleteCountNoCookies to see at number of phrases I am loosing people on the onboarding to tune the first set
			if(globals.gameData.phrasesCompleteCountNoCookies <= 100){
				AnalyticsController.RegisterEvent({category:"design", event_id: "funnel_completePuzzle:"+globals.gameData.phrasesCompleteCountNoCookies});
			}
			else if(globals.gameData.phrasesCompleteCountNoCookies > 100 && globals.gameData.phrasesCompleteCountNoCookies <= 1000 && globals.gameData.phrasesCompleteCountNoCookies % 20 == 0){
				//Go Every 20 till 1000
				AnalyticsController.RegisterEvent({category:"design", event_id: "funnel_completePuzzle:"+globals.gameData.phrasesCompleteCountNoCookies});
			}
			else if(globals.gameData.phrasesCompleteCountNoCookies > 1000 && globals.gameData.phrasesCompleteCountNoCookies % 100 == 0){
				//Go Every 100
				AnalyticsController.RegisterEvent({category:"design", event_id: "funnel_completePuzzle:"+globals.gameData.phrasesCompleteCountNoCookies});
			}
			
			
			Ti.App.fireEvent("updateLeaderboard");
			
			if(Ti.Platform.name == "iPhone OS") {
				var gamekit = require('com.obigola.gamekit');
				gamekit.submitScore('com.mozzarello.gophrazy.puzzles_solved', globals.gameData.phrasesCompleteCountNoCookies);	
			}	
		}
		
		//RatePopUp
		if(globals.gameData.ratedComplete == false && 
			globals.gameData.sessionsCount%3 == 0 && globals.completeCountThisSession == 1)
			//(globals.gameData.phrasesCompleteCount == globals.gameData.ratePopUpFirst || (globals.gameData.phrasesCompleteCount%globals.gameData.ratePopUpFrequency == 0 && globals.gameData.phrasesCompleteCount != 0)))
		{
			var RatePopUp = require("/ui/PopUps/RatePopUp");
			var ratePopUp = new RatePopUp();
		}
	
		
		/*
		if(groupData.groupID && (groupData.groupID == "main" || groupData.groupID == "main2") && globals.tutorialOn != true){
			globals.gameData.currentPhraseDifficultyPosition = exports.GetNewDifficultyPosition();
		}
		*/
		
		//Change back to skipped fals here at the end after all the work is done.
		phrase.skipped = false;
		
		if(phrase.type == "written" && phrase.completeCount == 1 && phrase.authorID != globals.gameData.userName)
		{
			if(!phrase.solveCountOthers){
				phrase.solveCountOthers = 0;
				phrase.solveCountOthersOld = 0;
			}	
			//Should increase Solve count
			phrase.solvedCountOthers++;
			//phrase.solvedCountOthersOld++;
			PuzzleFactoryController.IncreasePuzzleSolvedCount(phrase.authorID, phrase.phraseID);
			PushNotificationsController.SendSolvedPuzzleNotificationToUser(phrase.authorID, {solvedCountOthers: phrase.solvedCountOthers});
		}
		
		globals.col.phrases.commit();
		globals.col.groups.commit();
		globals.col.gameData.commit();	
		
		PassController.CompletePhrasePassCheck();
		
		/////
		
		function Processskipbox(){
			var skipboxGroup = globals.col.groups.find({type:"skipbox"})[0];
			for (var i = 0; i < skipboxGroup.phrasesIDs.length; i++){
				if(skipboxGroup.phrasesIDs[i] == data.phraseID) {
					skipboxGroup.phrasesIDs.splice(i,1);
					break;
				}	
			}
		
			Ti.App.fireEvent("updateskipbox",{amount: skipboxGroup.phrasesIDs.length});
		}
		
		
		function ProcessCollection(){
			if(phrase.groupID != null && phrase.completeCount <= 1){
				var collection = globals.col.groups.find({groupID: phrase.groupID})[0];
				if(collection){
					collection.phrasesCompleteCount = globals.col.phrases.count({groupID: collection.groupID, complete:true});
					
					if(collection.phrasesCompleteCount >= collection.phrasesCount)
					{
						collection.phrasesCompleteCount = collection.phrasesCount;
						collection.complete = true;
						
						//Randomly give reward
						if(collection.phrasesCompleteCount % 2 == 0){
							globals.gameData.hints++;
							globals.gameData.hintsEarnedCount++;
							globals.rootWin.OpenWindow("smallSquarePopUp", "one_hint");
						}
						else{
							globals.gameData.skips++;
							globals.gameData.skipsEarnedCount++;
							globals.rootWin.OpenWindow("smallSquarePopUp", "one_skip");
						}
						
						//AchievementsController.GroupCheck();
						
						SoundController.playSound("unlockFeature");
						
						if(collection.kind == "issue" && collection.complete == true) {
							var issuesCompleteCount = globals.col.groups.count({kind:"issue", complete:true});
							var issuesCount =  globals.col.groups.count({kind:"issue"});
							var issueCompleteRate = issuesCompleteCount/issuesCount;
							AnalyticsController.RegisterEvent({category:"design", event_id: "puzzlePost:puzzlePostCompleteCount", area: phrase.groupID.toString(), value: issuesCompleteCount});
							//AnalyticsController.RegisterEvent({category:"design", event_id: "completeIssue:issuesCompleteCount", area: phrase.groupID.toString(), value: issuesCompleteCount});
						}		
					}
					if (collection.type == "collection") Ti.App.fireEvent("updateCollection", {data: collection});
				}	
			}
		}
		
		
		function ProcessLevel(){
			var currentLevel = LocalData.GetCurrentUserLevelDataForPoints(globals.gameData.phrasesCompleteCountNoCookies+1);
			//var currentLevel = LocalData.GetCurrentUserLevelDataForPoints(10);
			
			for (var key in globals.gameData.abilities) {
		        if (globals.gameData.abilities.hasOwnProperty(key)){
		        	if (Object.prototype.toString.call(globals.gameData.abilities[key]) === '[object Object]'){
						var ability = globals.gameData.abilities[key];
		
						if(globals.gameData.phrasesCompleteCountNoCookies+1 >= ability.unlockCount && ability.enabled == false){
							if (ability.name == "hints") {
								globals.gameData.abilities.hints.enabled = true;
								//AchievementsController.IncreaseAchievement("hints_unlocked", 1, true);
							}	
							else if (ability.name == "skipboxUnlocked") {
								globals.gameData.abilities.skipbox.enabled = true;
								//AchievementsController.IncreaseAchievement("skips_unlocked", 1, true);
							}	
							else if (ability.name == "fortuneCookiesUnlocked") {
								globals.gameData.abilities.fortuneCookies.enabled = true;
								globals.gameData.notifications.pushNotificationsInitialized = true;
								globals.gameData.notifications.fortuneCookieAvailable.visible = true;
								globals.gameData.notifications.fortuneCookieAvailable.enabled = true;
								globals.gameData.notifications.fortuneCookieExpires.visible = true;
								globals.gameData.notifications.fortuneCookieExpires.enabled = true;
								//AchievementsController.IncreaseAchievement("fortuneCookies_unlocked", 1, true);
							}	
							else if (ability.name == "puzzlePost") {
								globals.gameData.abilities.puzzlePost.enabled = true;
								//Delay to open the proper tutorial
								setTimeout(function(){globals.gameData.notifications.pushNotificationsInitialized = true;
									globals.col.gameData.commit();
								},2000);
								globals.gameData.notifications.announcements.visible = true;
								globals.gameData.notifications.announcements.enabled = true;
								globals.PushNotificationsChannelController.Refresh();
								globals.PushNotificationsChannelController.SubscribeToNextChannel();
								//AchievementsController.IncreaseAchievement("puzzlePost_unlocked", 1, true);
							}
							else if (ability.name == "composePuzzle") {
								globals.gameData.abilities.composePuzzle.enabled = true;
								//AchievementsController.IncreaseAchievement("composePuzzle_unlocked", 1, true);
							}
							Ti.App.fireEvent("levelUp");
							Ti.App.fireEvent("featureUnlocked", {name: ability.name});
				 			Ti.App.fireEvent("closeskipboxWin");
				 			Ti.App.fireEvent("closePuzzlePostWin");
							globals.gameData.userLevel = currentLevel.level;
							break; //Prevents unlocking more than one thing at a time.
						}
					}
				}
			}
			
			
			//WhyConnectWin
			//if((globals.gameData.phrasesCompleteCountNoCookies+1 == 7 || globals.gameData.phrasesCompleteCountNoCookies+1 == 30) && globals.gameData.userFacebookID == "") setTimeout(function(){globals.rootWin.OpenWindow("whyConnectWin");},1500);
		}
		
		
		function GetNewDifficultyPosition (){
			var gameData = globals.gameData;
			//Control the difficulty Position;
			var currentPhraseDifficultyPosition = gameData.currentPhraseDifficultyPosition+1;
			var ftu = gameData.phrasesDifficultyCurve.ftu;
			var regular = gameData.phrasesDifficultyCurve.regular;
			
			if(gameData.phrasesCompleteCountNoCookies == ftu.length) currentPhraseDifficultyPosition = 0;
			else if(gameData.phrasesCompleteCountNoCookies > ftu.length && currentPhraseDifficultyPosition >= regular.length ) currentPhraseDifficultyPosition = 0;
			
			return currentPhraseDifficultyPosition;
		};
		
		Ti.App.fireEvent("updatePhrasesCompleteCount", {value: globals.gameData.phrasesCompleteCount});
		Ti.App.fireEvent("updateGameData",{});
		Ti.App.fireEvent("phraseComplete",{phraseID:phrase.phraseID});
		if(globals.gameData.userFacebookID != "") ServerData.PostScoreToFacebook(globals.gameData.phrasesCompleteCount);
		
		Ti.API.info("LocalData: Completed Phrase");
	}
};


exports.AddTimeToPhrase = function(startTime, phraseID){
	var phrase = globals.col.phrases.find({phraseID: phraseID})[0];
	if(startTime != null){
		var addedTime = clock.getNow().valueOf()-startTime.valueOf();
		phrase.timeToSolve.currentTime += addedTime;
		
		Ti.API.info("Current Time to Solve: "+phrase.timeToSolve.currentTime);
		globals.col.phrases.commit();
	}
};


exports.FakeCompletePhrase = function(groupData){
	for(var i = 0; i < groupData.phrasesIDs.length; i++){
		exports.CompletePhrase(groupData.phrasesIDs[i],groupData); 
	};
};


exports.SkipPhrase = function(phraseID){
	var LocalData = require("/model/LocalData");
	var phrase = globals.col.phrases.find({phraseID:phraseID})[0];
	var gameData = globals.gameData;
	
	//Only gets skipped if it was incomplete otherwise it will not charge you and just go to the next phrase.
	//if(phrase.complete == false)
	{
		phrase.skipped = true;
		phrase.skippedTime = clock.getNow().valueOf();
		globals.col.phrases.commit();
		
		var skipboxGroup = globals.col.groups.find({type:"skipbox"})[0];	
		skipboxGroup.phrasesIDs.push(phraseID);
		globals.col.groups.commit();
		
		
		if (!globals.tutorialOn || globals.tutorialOn == false){
			globals.gameData.skips--;
			//if(phrase.groupID && phrase.groupID == "main") globals.gameData.currentPhraseDifficultyPosition = GetNewDifficultyPosition();
			globals.col.gameData.commit();
		}
		
		Ti.API.info("Skip Phrase");
		Ti.App.fireEvent("updateskipbox", {amount: skipboxGroup.phrasesIDs.length});
		Ti.App.fireEvent("updateSkips", {amount: gameData.skips});
	}
};


exports.GetPhraseHintCost = function(phraseNumberOfWords){
	var hintCost = 2;
	if(phraseNumberOfWords > 12) hintCost = 3;
	return hintCost;
};


exports.SaveHintPositionForPhrase = function (hintPosition,phraseID){
	var phrase = globals.col.phrases.find({phraseID: phraseID})[0];
	
	var hintPositions = [];
	if (phrase.hintPositions) hintPositions = phrase.hintPositions;
	hintPositions.push(hintPosition);
	
	globals.col.phrases.update(
		{phraseID: phraseID},
		{$set:{hintPositions: hintPositions}},
		{},true
	);

	globals.col.phrases.commit();
};


exports.AddReceiverForPhrase = function (phraseID,receiverID){
	var phrase = globals.col.phrases.find({phraseID: phraseID})[0];
	
	var receiverIDs = phrase.receiverIDs;
	receiverIDs.push(receiverID);
	
	globals.col.phrases.update(
		{phraseID: phraseID},
		{$set:{receiverIDs: receiverIDs}},
		{},true
	);
	
	globals.col.phrases.commit();
	
	return globals.col.phrases.find({phraseID:phraseID})[0];
};


exports.TogglePhraseFavorite = function (phraseID){
	var phrase = globals.col.phrases.find({phraseID:phraseID})[0];
	phrase.favorite = !phrase.favorite;
	globals.col.phrases.commit();
	return phrase.favorite;
};


exports.CreateShuffleBagOfPhrases = function(data, isFake){
	var phrases = [];
	if (data.type == "all" || data.type == "quotes"){
		if (data.level == "easy") phrases = globals.col.phrases.find({complete:false, numberOfWords: {$lte:7},type:{$ne:"fortuneCookie"}});
		else if (data.level == "medium") phrases = globals.col.phrases.find({complete:false, numberOfWords: {$lte:12},type:{$ne:"fortuneCookie"}});
		else phrases = globals.col.phrases.find({complete:false,type:{$ne:"fortuneCookie"}});
	}
	
	//else if (data.type == "quotes") phrases = globals.col.phrases.find({type: "quote",complete:false});
	else if (data.type == "author") phrases = globals.col.phrases.find({author:data.name, complete:false});
	else if (data.type == "category") phrases = globals.col.phrases.find({category:data.name, complete:false});
	else if (data.type == "level"){
		for (var i = 0; i < data.phrasesIDs.length; i++){
			var phrase = globals.col.phrases.find({$id:data.phrasesIDs[i].$id})[0];
			if (phrase.complete == false) phrases.push(phrase);
		}
	} 
	
	if (isFake == false) globals.phrasesShuffleBag = new RandomPlus.ShuffleBagStandard();
	else globals.phrasesShuffleBag = new RandomPlus.FakeShuffleBag();
	
	for(var i = 0; i < phrases.length; i++){	
		globals.phrasesShuffleBag.add(phrases[i]);
	};
	
	Ti.API.info("Shuffle Bag Phrases length: "+phrases.length);
	
	return globals.phrasesShuffleBag.data.length;
};


exports.GetNextPhrase = function(){
	return globals.phrasesShuffleBag.next();
};


exports.RemovePhraseFromShuffleBag = function(item){
	globals.phrasesShuffleBag.remove(item);
};


exports.GetNewNextPhrase = function(){
	var gameData = globals.gameData;
	var currentLevelData = gameData.levelsGeneralData[gameData.currentPositionInLevelGeneralData];
	var newPhrase = FindPhrase(currentLevelData.phraseDifficulty, currentLevelData.phraseDifficulty >= 4 ? "down": "up");
	var nextPositionInLevelGeneral = gameData.currentPositionInLevelGeneralData+1;
	if (nextPositionInLevelGeneral >= gameData.levelsGeneralData.length) nextPositionInLevelGeneral = 0;
	
	
	globals.col.gameData.update(
		{$id:gameData.$id},
		{$set:{currentPhraseID: newPhrase.phraseID, 
			currentPositionInLevelGeneralData: nextPositionInLevelGeneral}
		},
		{},true
	);
	globals.col.gameData.commit();
	
	Ti.API.info("PhrasesController: Set new current phrase "+newPhrase.phraseID);
	
	return newPhrase;
};


function FindPhrase(difficulty,contingency){
	var RandomPlus = require("/lib/RandomPlus");
	var originalDifficulty = difficulty;
	var phrases;
	var phrase;
	var difficulties = [
		{low: 0,high: 7},
		{low: 8,high: 11},
		{low: 12,high:14},
		{low:15,high:17},
		{low:17,high:99}
	];
	
	while (phrase == null){
		var low = difficulties[difficulty].low;
		var high = difficulties[difficulty].high;
		
		phrases = globals.col.phrases.find({complete: false, difficulty:{$gt:low,$lte:high}, type:{$ne: "fortuneCookie"}});
		phrase = phrases[RandomPlus.RandomRange(0,phrases.length)];
		
		if (contingency == "up") difficulty++;
		else difficulty --;
		
		//If you can't find any get a phrase you already used. 
		if (difficulty < 0 || difficulty > 4) {
			difficulty = originalDifficulty;
			getUsedPhrases = true;
			//break;
		}
	}
	
	return phrase;
};


exports.InitRiddleGroup = function (worldID){
	var phrase = globals.col.phrases.find({type:"riddle", topic:worldID})[0];
	var sceneStart = worldID+"Riddle";
	Ti.API.info(sceneStart);
	
	globals.col.groups.save({
		type:"riddle",
		title: "Riddle",
		subtitle: "Riddle",
		count: 1,
		completeCount: 0,
		locked: true,
		phrasesIDs: [phrase],
		currentPhrase: phrase,
		complete: phrase.complete,
		worldID: worldID,
		sceneStart: sceneStart,
		sceneEnd: "none"
	});
	
	globals.col.groups.commit();
};


exports.GetRiddleGroup = function(worldID){
	return globals.col.groups.find({type:"riddle",worldID:worldID})[0];
};


exports.CreateGeneralPhraseGroup = function (){
	var RandomPlus = require("/lib/RandomPlus");
	var gameData = globals.gameData;
	var currentLevelData = gameData.levelsGeneralData[gameData.currentPositionInLevelGeneralData];
	var groupData = {
		type:"general",
		title: "General",
		subtitle: "All phrases.",
		count: gameData.phrasesCount,
		completeCount: gameData.phrasesCompleteCountNoCookies,
		locked: false,
		phrasesData: [],
		currentPhrase: null
	};
	
	function NextPhrase (){
		groupData.currentPhrase = exports.GetNewNextPhrase();
		groupData.completeCount = globals.gameData.phrasesCompleteCountNoCookies;
		return groupData.currentPhrase;
	}
	
	groupData.NextPhrase = NextPhrase;
	groupData.currentPhrase = gameData.currentPhraseID == "" ? NextPhrase() : globals.col.phrases.find({phraseID: gameData.currentPhraseID})[0];
	
	
	return groupData;
};


exports.CreateHelpOthersPhrasesGroup = function (wMessage){
	var gameData = globals.gameData;
	
	var groupData = {
		type:"helpOthers",
		title: "",
		subtitle: "",
		count: 1,
		completeCount: 0,
		locked: false,
		phrasesData: [],
		wMessage: wMessage
	};
	
	var phrase = globals.col.phrases.find({phraseID:wMessage.phraseID})[0];
	phrase.serverID = wMessage.serverID;
	phrase.senderID = wMessage.senderID;
	phrase.receiverID = wMessage.receiverID;
	
	groupData.isMine = phrase.senderID.toString() == gameData.userFacebookID.toString() ? true : false;
	groupData.phrasesData.push(phrase);
	
	if (groupData.isMine == true){
		groupData.title = "Help Request to "+wMessage.receiverName.firstName;
	}
	else{
		groupData.title = "Help Request from "+wMessage.senderName.firstName;
	}
	
	if (wMessage.completed == true){
		groupData.subtitle = "Completed by "+wMessage.completedBy.firstName;
	}
	
	
	
	/* IMPLEMENTATION TO BE ABLE TO SKIP FORWARD
	var incompletePhrases =[];
	var phrases = [];
	
	function CreateArray (){
		phrases = [];
		var initialise = incompletePhrases.length == 0 ? true : false;
		for (var i = 0; i < helpOthersPhrases.length; i++){
			var phrase = globals.col.phrases.find({phraseID:helpOthersPhrases[i].phraseID})[0];
			phrase.serverID = helpOthersPhrases[i].serverID;
			phrase.senderID = helpOthersPhrases[i].senderID;
			phrase.receiverID = helpOthersPhrases[i].receiverID;
			phrases.push(phrase);
			if (initialise) incompletePhrases.push(phrase);
		}
		groupData.phrasesData = phrases;
	}
	
	function CompletePhrase(phraseData){
		var length = incompletePhrases.length;
		for (var i = 0; i < length; i++){
			incompletePhrases.splice(incompletePhrases.indexOf(phraseData),1);
		}
		groupData.phraseData = phrases;
		
		Ti.API.info(incompletePhrases);
		
		return groupData;
	}
	
	CreateArray();
	
	groupData.currentPhrase = phrases[helpOthersPhrases.indexOf(selectedPhrase)];
	groupData.CompletePhrase = CompletePhrase;
	*/
	
	return groupData;
};


exports.CompleteHelpOthersMyPhrase = function(phraseData){
	var levels = globals.col.groups.find({type:"level"});
	
	for (var i = 0; i < levels.length; i++){
		for (var j = 0; j < levels[i].phrasesIDs.length; j++){
			if (levels[i].phrasesIDs[j].phraseID == phraseData.phraseID){
				var LocalData = require("/model/LocalData");
				LocalData.CompletePhrase(phraseData,levels[i]);
				Ti.API.info("CompletedHelpOthersMyPhrase");
				break;
			}
		}
	}
};


exports.InitPhrases = function(callback){
	var startTime = new Date().getTime();
	
	var phrases = JSON.parse(GetJsonText("model/"+globals.systemData.currenti18n+"/PhrasesDB.txt"));
	
	for (var i = 0; i < phrases.length; i++){
		exports.SavePhrase(phrases[i]);
	};
	
	globals.col.phrases.commit();
	
	
	
	callback({text:"Time to init: "+(new Date().getTime()-startTime)});
	Ti.API.info("Time to init: "+(new Date().getTime()-startTime));
};


exports.SavePhrase = function(phraseData){
	function trim1 (str) {
	    return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}
	//alert(phraseData);
	globals.col.phrases.save({
		phraseID: phraseData.phraseID,
		authorID: phraseData.authorID,
		leadText: phraseData.leadText,
		text: trim1(phraseData.text),
		author: phraseData.author,
		completeText: phraseData.completeText,
		mediaName: phraseData.mediaName,
		link: phraseData.link,//?
		type: phraseData.type,
		topic: phraseData.topic, //?
		topicTitle: phraseData.topicTitle == "none" ? null : phraseData.topicTitle,//?
		length: phraseData.length,
		numberOfWords: phraseData.numberOfWords,
		numberOfDots: phraseData.numberOfDots,
		difficulty: phraseData.difficulty,
		isFunny: phraseData.isFunny,
		includeInGeneral: phraseData.includeInGeneral,
		complete: false,
		completeCount: 0,
		isBeingUsed: false,
		favorite: false,
		receiverIDs: [],//?
		skipped: false,
		groupID: phraseData.groupID,
		eventID: phraseData.eventID,
		hide: false,
		written: phraseData.written, //?
		favorite: false,
		thumbs: null, //?
		timeToSolve: {
			firstTime: 0,
			lastTime: 0,
			bestTime: 0,
			currentTime: 0
		},
		stars:[
			{complete: false},
			{complete: false},
			{complete: false}
		],
		createdDate: phraseData.createdDate != null ? phraseData.createdDate : clock.getNow().valueOf(),
		solvedCountOthers: phraseData.solvedCountOthers,
		solvedCountOthersOld: 0,
		puzzleURL: phraseData.puzzleURL,
		source: phraseData.source ? phraseData.source : "local", // local, online
		language: phraseData.language ? phraseData.language : "English",
		reportStatus: phraseData.reportStatus ? phraseData.reportStatus : "clear",
		published: phraseData.published ? phraseData.published : 0
	});
	
	return globals.col.phrases.find({phraseID:phraseData.phraseID})[0];
};


exports.UpgradePhrases = function(phrasesData, versionNumber){
	var startTime = new Date().getTime();
	var saveCount = 0;
	var updateCount = 0;
	for (var i = 0; i < phrasesData.length; i++){
		var existingPhrase = globals.col.phrases.find({phraseID: phrasesData[i].phraseID})[0];
		if (existingPhrase == null){
			exports.SavePhrase(phrasesData[i]);
			saveCount++;
		}
		else{
			existingPhrase.hide = phrasesData[i].hide;
			updateCount++;
		}
	}
	
	Ti.API.info("UPGRADE: Saved Phrases Count "+saveCount);
	Ti.API.info("UPGRADE: Update Phrases Count "+updateCount);
	globals.col.phrases.commit();
	globals.gameData.versions.mainPhrasesVersion = versionNumber;
	globals.col.gameData.commit();
};


exports.UpdatePhrases = function(updateData){
	for(var i = 0; i < updateData.changes.length; i++){
		var phraseUpdateData = updateData.changes[i];
		var phrase = globals.col.phrases.find({phraseID: phraseUpdateData.phraseID})[0];
		if(phrase != null){
			FillObject(phraseUpdateData, phrase);
			UpdateObject(phraseUpdateData, phrase);
		}
		
		Ti.API.info("UPDATE PHRASE: "+phrase.phraseID);
	}
	
	globals.col.phrases.commit();
	
	function FillObject(from, to) {
	    for (var key in from) {
	        if (from.hasOwnProperty(key)) {
	            if (Object.prototype.toString.call(from[key]) === '[object Object]') {
	                if (!to.hasOwnProperty(key)) {
	                    to[key] = {};
	                }
	                FillObject(from[key], to[key]);
	            }
	            else if (!to.hasOwnProperty(key)) {
	                to[key] = from[key];
	            }
	        }
	    }
	};
	
	function UpdateObject(newData, oldData){
		for (var key in newData) {
	        if (newData.hasOwnProperty(key)) {
	            if (Object.prototype.toString.call(newData[key]) === '[object Object]') {
	                UpdateObject(newData[key], oldData[key]);
	            }
	            else if (oldData.hasOwnProperty(key)) {
	                oldData[key] = newData[key];
	            }
	        }
	    }
	};
};


exports.CheckPhrasesForStars = function(){
	var phrasesNoStars = globals.col.phrases.find({stars:{$exists:false}});
	for(var i = 0; i < phrasesNoStars.length; i++){
		phrasesNoStars[i].stars = [
			{complete: phrasesNoStars[i].complete},
			{complete: false},
			{complete: false}
		];
	}
	
	globals.col.phrases.commit();
};

function GetJsonText (fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
 
	if (readFile.exists()) {
		var readContents = readFile.read();
	   	return readContents.text;
	}
};