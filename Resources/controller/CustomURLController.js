var AnalyticsController = require("/controller/AnalyticsController");

var cmd = Ti.App.getArguments();

var toType = function(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
};


if ((toType(cmd) == 'object') && cmd.hasOwnProperty('url') ) {
    Ti.App.launchURL = cmd.url;
    Ti.API.info( 'CUSTOMURLCONTROLLER: Launched with url = ' + Ti.App.launchURL );
}
 
// Save launch URL at the time last paused
Ti.App.addEventListener( 'pause', function(e) {
    
});
 
// After app is fully resumed, recheck if launch arguments
// have changed and ignore duplicate schemes.
Ti.App.addEventListener('resumed', function(e) {
	cmd = Ti.App.getArguments();
	if ((toType(cmd) == 'object') && cmd.hasOwnProperty('url') ) {
	    Ti.App.resumedURL = cmd.url;
	    Ti.API.info( 'CUSTOMURLCONTROLLER: Launched with url = ' + Ti.App.launchURL );
	}
	if(Ti.App.resumedURL != Ti.App.launchURL) exports.Check();
});


exports.Check = function(){
	if((toType(cmd) == 'object') && cmd.hasOwnProperty('url') ) {
    	var parsedObject = ParseURL(cmd.url);
    	var PuzzleFactoryController = require("/controller/PuzzleFactoryController");
    	if(parsedObject.category == "playerPuzzle"){
    		Common.CreateButtonBlocker(true, true, 1000);
    		PuzzleFactoryController.GetPuzzle(parsedObject);
    		AnalyticsController.RegisterEvent({category:"design", event_id: "openedFromPlayerPuzzle", value: parsedObject.primary+"/"+parsedObject.secondary});				
    	}	
    }
};


function ParseURL(string){
	var returnObject = {};
	var original = string;
	var trimmed = string.substring(11,string.length);
	
	var dashPosition = trimmed.indexOf("/");
	returnObject.category = trimmed.slice(0, dashPosition);
	trimmed = trimmed.substring(dashPosition+1, trimmed.length);
	
	if(returnObject.category == "playerPuzzle"){
		dashPosition = trimmed.indexOf("/");
		returnObject.primary = trimmed.slice(0, dashPosition);
		trimmed = trimmed.substring(dashPosition+1, trimmed.length);
		returnObject.secondary = trimmed;
	}
	
	return returnObject;
}