var sounds = [];

exports.init = function(){
	sounds.push(createSound("themeSongDrums","youre-only-dream-away-dixieland-drums.mp3", true, 0.15));
	
	//SFX
	sounds.push(createSound("select", "1 - Select.wav", false, 0.4));
	for (var i = 11; i <= 36; i++){
		sounds.push(createSound("wordSelectTone"+i, "selectTones/Go Phrazy - FX 2 - "+i+".wav", false, 1.0));
	}
	sounds.push(createSound("undo", "3 - Undo.wav", false, 0.7));
	sounds.push(createSound("clear", "4 - Clear.wav", false, 0.7));
	//sounds.push(createSound("completePhrase", "5 - CompletePhrase.wav", false));
	sounds.push(createSound("completePhrase", "Complete Phrase3.wav", false, 0.5));
	sounds.push(createSound("failedPhrase", "6 - FailedPhrase.wav", false, 0.7));
	sounds.push(createSound("wordsAppear", "7 - WordsAppear.wav", false, 1.0));
	sounds.push(createSound("hintWordSelected", "8 - HintWordSelected.wav", false, 0.7));
	sounds.push(createSound("unlockFeature", "9 - UnlockFeature.wav", false, 0.7));
	//sounds.push(createSound("buySuccess", "10 - BuySuccess.wav", false));
	sounds.push(createSound("gotMessage", "11 - GotMessage.wav", false, 0.7));
	//sounds.push(createSound("swoosh", "12 - Swoosh.wav", false, 0.7));
	sounds.push(createSound("pop", "pop.wav", false, 1.0));
	
	
	exports.playSound("themeSongDrums");
};


var createSound = function(name, fileName, isMusic, volume){
	var sound = {};
	sound.name = name;
	sound.isMusic = isMusic;
	
	sound.media = Ti.Media.createSound({url:"/sounds/"+fileName, });
	sound.media.volume = volume;
	if(isMusic == true) {
		sound.media.looping = true;
		
		if(Ti.Platform.name == "android"){
			sound.media.addEventListener("complete", function(){
				Ti.API.info(sound.media.playing);
				sound.media.play();
			});
		}
	}	
	
	return sound;
};


exports.playSound = function(soundName){
	
	//Don't play in Android for now till audio is fixed
	/*
	if(Ti.Platform.name != "iPhone OS") {
		globals.gameData.musicOn = false;
		globals.gameData.sfxOn = false;
	}
	*/
	
	
	for(var i = 0; i < sounds.length; i++){
		if(sounds[i].name == soundName) {
			if(sounds[i].isMusic == true && globals.gameData.musicOn == true) sounds[i].media.play();
			else if(sounds[i].isMusic == false && globals.gameData.sfxOn == true) sounds[i].media.play();
			break;
		}	
	}
};


exports.createWordSelectToneController = function(){
	var controller = {
		startTone: 11,
		maxTone: 36,
		currentTone: 11
	};
	
	function PlayNextTone(){
		exports.playSound("wordSelectTone"+controller.currentTone);
		controller.currentTone++;
		if(controller.currentTone > controller.maxTone) controller.currentTone = controller.maxTone;
	}
	
	function GoBack(){
		controller.currentTone--;
		if(controller.currentTone < controller.startTone) controller.currentTone = controller.startTone;
	}
	
	function ResetCurrentTone(){
		controller.currentTone = controller.startTone;
	}
	
	controller.PlayNextTone = PlayNextTone;
	controller.ResetCurrentTone = ResetCurrentTone;
	controller.GoBack = GoBack;
	
	return controller;
};

exports.getSoundByName = function(soundName){
	var returnObject;
	for(var i = 0; i < sounds.length; i++){
		if(sounds[i].name == soundName) {
			returnObject = sounds[i];
			break;
		}	
	}
	
	return returnObject;
};


exports.ToggleMusic = function(){
	if(globals.gameData.musicOn == true){
		for(var i = 0; i < sounds.length; i++){
			if(sounds[i].isMusic == true)  sounds[i].media.pause();
		}
		globals.gameData.musicOn = false;
	}
	else{
		for(var i = 0; i < sounds.length; i++){
			if(sounds[i].isMusic == true)  sounds[i].media.play();
			break;
		}
		globals.gameData.musicOn = true;
	}
	
	globals.col.gameData.commit();
};


exports.ToggleSFX = function(){
	if(globals.gameData.sfxOn == true) globals.gameData.sfxOn = false;
	else globals.gameData.sfxOn = true;
	globals.col.gameData.commit();
};

