exports.UpdateWMessages = function (dataIn){
	var WMessagesController = require("controller/WMessagesController");
	var PhrasesController = require("controller/PhrasesController");
	var gameData = globals.gameData;
	
	for (var i = 0; i < dataIn.length; i ++){
		var wMessage = dataIn[i];
		var match = globals.col.wMessages.find({serverID: wMessage.id})[0];
		var opened = false;
		if(wMessage.completedByUserID){
			var completedByMe = wMessage.completedByUserID.toString() == gameData.userFacebookID.toString() ? true : false;
			if(match && match.opened == true){
				if(completedByMe == false) opened = true;
				else opened = false;
			} 
		}
		
		if (match == null){
			globals.col.wMessages.save({
				type: wMessage.type,
				phraseID: wMessage.phraseID,
				serverID: wMessage.id,
				senderID: wMessage.senderID,
				receiverID: wMessage.receiverID,
				shuffledWords: wMessage.shuffledWords,
				hasExpired: wMessage.hasExpired,
				//user: wMessage.user,
				createdAt: new moment(wMessage.created_at).valueOf(),
				updatedAt: wMessage.updated_at ? new moment(wMessage.updated_at).valueOf(): null,
				completedByUserID:  wMessage.completedByUserID ? wMessage.completedByUserID : null,
				completedAt: wMessage.completedAt ? new moment(wMessage.completedAt).valueOf(): null,
				opened: opened
			});
		}
		else{
			if(wMessage.updated_at != wMessage.created_at){
				globals.col.wMessages.update({serverID:wMessage.id},
				{$set:{
					updatedAt: wMessage.updated_at ? new moment(wMessage.updated_at).valueOf(): null,
					completedByUserID: wMessage.completedByUserID ? wMessage.completedByUserID : null,
					completedAt: wMessage.completedAt ? new moment(wMessage.completedAt).valueOf(): null,
					hasExpired: wMessage.hasExpired,
					opened: opened
					}
				},{},true
				);
			}
		}
	}
	
	globals.col.wMessages.commit();
	
	var newMessages = exports.GetNewWMessages();
	
	Ti.App.fireEvent("updateWMessages",{results:exports.GetAllWMessages()});
};


exports.GetAllWMessages = function (){
	var gameData = globals.gameData;
	var returnObject = {};
	
	returnObject.sent = {};
	returnObject.sent.type = "sent";
	returnObject.sent.title = "Sent";
	returnObject.sent.subTitle = "Have your friends solved any of your puzzles";
	returnObject.sent.data = globals.col.wMessages.find({senderID: gameData.userFacebookID});  
	returnObject.sent.bubbleCount = 0;//globals.col.wMessages.count({senderID: gameData.userFacebookID, opened:false}); 
	
	var sentBubbleCount = 0;
	for (var i = 0; i < returnObject.sent.data.length; i++){
		if (returnObject.sent.data[i].completedAt == null) sentBubbleCount++;
		ProcessWMessageData(returnObject.sent.data[i]);
	}
	returnObject.sent.bubbleCount = sentBubbleCount;
	
	returnObject.inbox = {};
	returnObject.inbox.type = "inbox";
	returnObject.inbox.title = "Inbox";
	returnObject.inbox.subTitle = "Puzzles for you";
	var inboxArray = globals.col.wMessages.find({receiverID: gameData.userFacebookID});
	var completeHelpArray = [];
	var completeHelpRequestsArray = globals.col.wMessages.find({senderID: gameData.userFacebookID, completedAt: {$ne: null}, opened: false});
	for (var i = 0; i < completeHelpRequestsArray.length; i++){
		if (completeHelpRequestsArray[i].completedByUserID != gameData.userFacebookID) completeHelpArray.push(completeHelpRequestsArray[i]);
	}
	returnObject.inbox.data = inboxArray.concat(completeHelpArray);
	
	var inboxBubbleCount = 0;
	for (var i = 0; i < returnObject.inbox.data.length; i++){
		var wMessage = returnObject.inbox.data[i];
		//Get Bubble Count
		if ((wMessage.opened == false && wMessage.completedByUserID != gameData.userFacebookID) ||
			(wMessage.opened == true &&  !wMessage.completedAt)) inboxBubbleCount++;
		
		ProcessWMessageData(returnObject.inbox.data[i]);
	}
	
	returnObject.inbox.bubbleCount = inboxBubbleCount;
	
	
	function GetFriendName(facebookID){
		var firstName = facebookID;
		var lastName = facebookID;
		if (facebookID != gameData.userFacebookID){
			for (var i = 0; i < gameData.userFacebookFriends.length; i++){
				if (gameData.userFacebookFriends[i].id == facebookID){
					firstName = gameData.userFacebookFriends[i].first_name;
					lastName = gameData.userFacebookFriends[i].last_name;
					break;
				} 
			}
		}
		else {
			firstName = "You";
			lastName = "";
		}	
		
		return {firstName: firstName, lastName: lastName};
	}
	
	
	function ProcessWMessageData(wMessage){
		wMessage.senderName = GetFriendName(wMessage.senderID); 
		wMessage.receiverName = GetFriendName(wMessage.receiverID); 
		
		if(wMessage.completedAt != null){
			wMessage.completed = true;
			wMessage.completedAt = new Date(wMessage.completedAt).getTime();
			wMessage.completedBy = GetFriendName(wMessage.completedByUserID);
		}	
		else{
			wMessage.completed = false;
		}
		
		if (wMessage.senderID.toString() == gameData.userFacebookID.toString()) wMessage.sent = true;
		else wMessage.sent = false; 
		
		if (wMessage.type == "helpRequest"){
			if(wMessage.completed == true){
				if(wMessage.sent == true) {
					 if(wMessage.completedByUserID == gameData.userFacebookID) wMessage.titleText = "You completed your own Help Request";
					 else wMessage.titleText = wMessage.receiverName.firstName+ " completed your Help Request";
				}
				else{
					 if(wMessage.completedByUserID == wMessage.receiverID) wMessage.titleText = "You completed "+wMessage.senderName.firstName+"'s Help Request";
					 else if(wMessage.completedByUserID == wMessage.senderID) wMessage.titleText = wMessage.senderName.firstName+" completed his own Help Request";
				}
			}
			else{
				if(wMessage.sent == true) wMessage.titleText = "Your Help Request to "+wMessage.receiverName.firstName;
				else  wMessage.titleText = "Help Request from "+wMessage.senderName.firstName;
			}
		}
		else if (wMessage.type == "suggestion"){
			if(wMessage.completed == true){
				if(wMessage.sent == true) wMessage.titleText = wMessage.receiverName.firstName+ " completed your Suggestion";
				else wMessage.titleText = "You completed "+wMessage.senderName.firstName+"'s Suggestion";	 
			}
			else{
				if(wMessage.sent == true) wMessage.titleText = "Your Suggestion to "+wMessage.receiverName.firstName;
				else  wMessage.titleText = "Suggestion from "+wMessage.senderName.firstName;
			}
		}
	}
	
	
	return returnObject;
	/*
	returnObject.helpRequestSent = {};
	returnObject.helpRequestSent.type = "helpRequestSent";
	returnObject.helpRequestSent.title = "Help Request - Sent";
	returnObject.helpRequestSent.subTitle = "Check if your friends solved any of your puzzles";
	returnObject.helpRequestSent.data = globals.col.wMessages.find({type:"helpRequest", senderID: gameData.userFacebookID});  
	returnObject.helpRequestSent.newCount = globals.col.wMessages.count({type:"helpRequest", senderID: gameData.userFacebookID, opened:false}); 
	
	returnObject.helpRequestInbox = {};
	returnObject.helpRequestInbox.type = "helpRequestInbox";
	returnObject.helpRequestInbox.title = "Help Request - Inbox";
	returnObject.helpRequestInbox.subTitle = "Your friends thought you'd like these";
	returnObject.helpRequestInbox.data = globals.col.wMessages.find({type:"helpRequest", receiverID: gameData.userFacebookID});  
	returnObject.helpRequestInbox.newCount = globals.col.wMessages.count({type:"helpRequest", receiverID: gameData.userFacebookID, opened:false}); 
	
	returnObject.tryThisSent = {};
	returnObject.tryThisSent.type = "tryThisSent";
	returnObject.tryThisSent.title = "Try this - Sent";
	returnObject.tryThisSent.subTitle = "See how your friends are doing on phrase you sent. ";
	returnObject.tryThisSent.data = globals.col.wMessages.find({type:"tryThis", senderID: gameData.userFacebookID});  
	returnObject.tryThisSent.newCount = globals.col.wMessages.count({type:"tryThis", senderID: gameData.userFacebookID, opened:false}); 
	
	returnObject.tryThisInbox = {};
	returnObject.tryThisInbox.type = "tryThisInbox";
	returnObject.tryThisInbox.title = "Try this - Inbox";
	returnObject.tryThisInbox.subTitle = "Your friends thought you'd like these";
	returnObject.tryThisInbox.data = globals.col.wMessages.find({type:"tryThis", receiverID: gameData.userFacebookID});  
	returnObject.tryThisInbox.newCount = globals.col.wMessages.count({type:"tryThis", receiverID: gameData.userFacebookID, opened:false}); 
	*/
};


exports.GetNewWMessages = function (){
	var gameData = globals.gameData;
	return globals.col.wMessages.find({opened: false, senderID:{$ne:gameData.userFacebookID.toString()}});
};


exports.MarkWMessageOpened = function (WMessageID){
	globals.col.wMessages.update({$id:WMessageID},
	{$set:{
		opened: true
		}
	},{},true
	);
	globals.col.wMessages.commit();
	
	Ti.App.fireEvent("updateWMessages",{results:exports.GetAllWMessages()});
};


exports.MarkWMessageComplete = function (WMessageID){
	globals.col.wMessages.update({$id:WMessageID},
	{$set:{
		completedByUserID: globals.gameData.userFacebookID,
		completedAt: clock.getNow().valueOf(),
		}
	},{},true
	);
	globals.col.wMessages.commit();
	
	Ti.App.fireEvent("updateWMessages",{results:exports.GetAllWMessages()});
};