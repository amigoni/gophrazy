exports.UpgradeIssues = function(newIssues){
	var GroupsController = require("controller/GroupsController");
	for (var i = 0; i < newIssues.length; i++){
		var newIssue  = newIssues[i];
		if(globals.col.issues.find({issueID: newIssue.issueID})[0] == null){
			newIssue.isNew = true;
			exports.InitIssue(newIssue);
			GroupsController.UpgradeCollections(newIssue.collections);
		}
	}
	
	globals.col.issues.commit();
	
	exports.ResetNewLabelForIssues();
	
	globals.gameData.newIssueIconPresent = true;
	globals.col.gameData.commit();
};


exports.InitIssue = function(data){
	globals.col.issues.save({
		issueID: data.issueID,
		name: data.name,
		description: data.description,
		createdDate: new moment(data.createdDate).valueOf(),
		releaseDate: new moment(data.releaseDate).valueOf(),
		isNew: data.isNew,
		complete: false,
		collectionsIDs: data.collectionsIDs,
		collectionsCompleteCount: 0,
		phrasesCost: data.phrasesCost,
		cost: Math.floor(data.phrasesCost*0.7),//data.cost,
		purchasable: data.issueID != -1 ? true: false //If you bought any collections or bought the issue this is set to false
	});
};


exports.InitIssues = function(){
	var issues = JSON.parse(Common.GetJsonText("model/"+globals.systemData.currenti18n+"/AllIssues.txt"));
	exports.UpgradeIssues(issues);
};

//Data formatted to get for the Newspaper window;
exports.GetIssuesData = function(){
	var returnObject = {};
	var main = globals.col.groups.find({groupID:"main"})[0];
	returnObject.lastCollection = globals.col.groups.find({groupID: globals.gameData.lastGroupID})[0];
	returnObject.latest = [globals.col.issues.find({issueID:{$ne: "Niente"} },{$sort: {releaseDate: -1}})[0]];
	returnObject.past = globals.col.issues.find({issueID:{$ne: returnObject.latest.issueID} },{$sort: {releaseDate: -1}});
	returnObject.complete = globals.col.groups.find({type: "collection", complete: true});
	
	return returnObject;
};

exports.ResetNewLabelForIssues = function (){
	var issues = globals.col.issues.find({issueID:{$ne: "Niente"} },{$sort: {releaseDate: -1}});
	for( var i = 0; i < issues.length; i++){
		issues[i].isNew = false;
		if(i == 0) issues[i].isNew = true;
	}
	
	globals.col.issues.commit();
};
