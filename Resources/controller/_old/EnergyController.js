exports.CreateEnergyChecker = function (){
	var gameData = globals.gameData;
	setInterval(function(){
		if(gameData.energy < gameData.rechargeLimit && gameData.startRechargingTime!=0){
			var timeDifference = new Date().getTime() - gameData.startRechargingTime;
			var rechargeInterval = gameData.rechargeInterval;
			if (timeDifference > rechargeInterval){
				var recharges = Math.floor(timeDifference/rechargeInterval);
				var newEnergy = gameData.energy+recharges;
				if (newEnergy > gameData.rechargeLimit) newEnergy = gameData.rechargeLimit;
				var newStartRechargingTime = gameData.startRechargingTime+recharges*rechargeInterval;
				globals.col.gameData.update({$id: gameData.$id},{$set:{
					energy: newEnergy, 
					startRechargingTime: newStartRechargingTime
					}},{},true);
				globals.col.gameData.commit();
				
				Ti.API.info("Recharged Energy");
				Ti.App.fireEvent("updateEnergy",{amount:gameData.energy});
			}
		}
	},1000);
};
