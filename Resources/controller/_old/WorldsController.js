var LevelsController = require("/controller/LevelsController");

exports.InitWorlds = function(){
	var PhrasesController= require("/controller/PhrasesController");
	var worlds = JSON.parse(GetJsonText("model/WorldData.txt"));
	var gameData = globals.gameData;
	
	for (var i = 0; i < worlds.length; i++){
		globals.col.worlds.save({
			worldID: worlds[i].worldID,
			worldNumber: worlds[i].worldNumber,
			worldName: worlds[i].worldName,
			levelCount:globals.col.groups.count({type:"level", worldID: worlds[i].worldID}),
			levelCompleteCount: globals.col.groups.count({type:"level", worldID: gameData.currentWorld, complete: true}),
			locked: worlds[i].locked == 0 ? false : true,
			complete: false,
			bgColor: worlds[i].bgColor,
			fontColor: worlds[i].fontColor,
			mapFontColor: worlds[i].mapFontColor,
			lineColor: worlds[i].lineColor,
			mapPosition: worlds[i].mapPosition,
			dotFillColor: worlds[i].dotFillColor,
			dotsData: worlds[i].dotsData,
			portalEnabled: worlds[i].portalEnabled,
			portalLocked:  worlds[i].portalLocked,
			portalPosition: worlds[i].portalPosition,
			portalPhraseText: worlds[i].portalPhraseText,
			riddleText : worlds[i].riddleText,
			riddleDotPosition: worlds[i].riddleDotPosition
		});
		
		//Initialize Riddle Groups for each world
		if (worlds[i].worldID != "moon") PhrasesController.InitRiddleGroup(worlds[i].worldID);
	}
	
	globals.col.worlds.commit();
	
	LevelsController.InitAllLevelsForWorld("meadows");
;};


exports.GetAllWorldsData = function (){
	return globals.col.worlds.getAll();
};


exports.GetWorldData = function(worldID){
	return globals.col.worlds.find({worldID: worldID})[0];
};


exports.SetCurrentWorld = function(newWorldID){
	var gameData = globals.gameData;
	globals.col.gameData.update(
		{$id:gameData.$id},
		{$set : {
			lastWorld: gameData.currentWorld,
			currentWorld: newWorldID}},
		{},
		true
	);
	globals.col.gameData.commit();
};

//Used for linear progression.
/*
exports.CheckToUnlockNextLevelInWorld = function(worldID){
	var LevelsController = require("/controller/LevelsController");
	var worldData = globals.col.worlds.find({worldID:worldID})[0];
	var unlockLevel = {unlock:false, levelNumberInWorld: null};
	
	var currentLevel = globals.col.groups.find({worldID:worldID, locked:false, complete:false})[0];
	if (currentLevel == null && worldData.levelCompleteCount < worldData.levelCount) unlockLevel = LevelsController.UnlockNextLevel(worldID);
	
	if (worldData.levelCompleteCount == worldData.levelCount) { 
		globals.col.groups.update({type: "riddle", worldID: worldID},
			{$set:{locked: false}},
			{},
			true
		);
		globals.col.groups.commit();
		unlockLevel = {unlock:true, levelNumberInWorld: "riddle"};
	}
	
	return unlockLevel;
};
*/

//Used for world Style Progression
exports.CheckToUnlockNextSomething = function(worldID){
	var worldData = globals.col.worlds.find({worldID:worldID})[0];
	var unlockLevel = false;
	if (worldData.levelCompleteCount >= worldData.levelCount){
		unlockLevel = true;
		globals.col.groups.update({type: "riddle", worldID: worldID},
			{$set:{locked: false}},
			{},
			true
		);
		globals.col.groups.commit();
	}
	
	return unlockLevel;
};


exports.UnlockPortal = function(worldID){
	globals.col.worlds.update(
		{worldID: worldID},
		{$set : {portalLocked: false}},
		{},
		true
	);
	globals.col.worlds.commit();
};


exports.CheckToUnlockNextWorld = function(){
	var LocalData = require('model/LocalData');
	var gameData = globals.gameData;
	
	var currentOpenWorld = globals.col.worlds.find({complete:false,locked:false,worldID:{$ne:"moon"}})[0];
	var lastCompleteWorld = globals.col.worlds.find({complete:true},{$sort:{worldNumber:-1}})[0];
	
	if (currentOpenWorld == null && lastCompleteWorld && lastCompleteWorld.worldID != "space"){
		var newWorldID;
		var finishedGame = false;
		
		if (lastCompleteWorld.worldID == "meadows") newWorldID = "sea";
		else if (lastCompleteWorld.worldID == "sea") newWorldID = "volcano";
		else if (lastCompleteWorld.worldID == "volcano") newWorldID = "desert";
		else if (lastCompleteWorld.worldID == "desert") newWorldID = "storm";
		else if (lastCompleteWorld.worldID == "storm") newWorldID = "cave";
		else if (lastCompleteWorld.worldID == "cave") newWorldID = "space";
		else if (lastCompleteWorld.worldID == "space") {
			Ti.API.info("WorldsController-UnlockNextWorld: Finished Game");
			finishedGame = true;
			alert("Congratulations!!!\nYou finished all the phrases.\nWe'll have new ones coming soon!");
		}
		
		globals.col.worlds.update(
			{worldID: newWorldID},
			{$set : {locked: false}},
			{},
			true
		);
		globals.col.worlds.commit();
		
		Ti.API.info("WorldsController-UnlockWorldLevel: New world Set: "+newWorldID);
		
		globals.col.gameData.update(
			{$id: globals.gameData.$id},
			{$set : {finishedGame: finishedGame}},
			{},
			true
		);
		globals.col.gameData.commit();
		
		
		if (finishedGame == false){
			Ti.App.fireEvent("unlockNextWorld");
			//LocalData.InitLevel(0, newWorldID);
			LevelsController.InitAllLevelsForWorld(newWorldID);
		}
	}
};


function GetJsonText (fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
 
	if (readFile.exists()) {
	     readContents = readFile.read();
	}
	 
	var doc = readContents.text;
	return doc;
};
