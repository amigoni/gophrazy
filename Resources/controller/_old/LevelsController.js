exports.GetLevelByLevelID = function(levelID){
	return globals.col.groups.find({levelID:levelID})[0];
};


exports.GetAllLevels = function(){
	return globals.col.groups.find({type:"level"}, {$sort:{levenNumber:1}});
};


exports.GetLevelsForWorld = function(worldID){
	return globals.col.groups.find({type:"level", worldID:worldID}, {$sort:{levelNumberInWorld:1}});
};


exports.GetLevelForWorld = function(worldID,leveNumberInWorld){
	return globals.col.groups.find({type:"level", worldID:worldID, levelNumberInWorld:leveNumberInWorld})[0];
};


exports.InitLevel = function(levelNumberInWorld, worldID){
	var gameData = globals.gameData;
	var levelData = globals.col.groups.find({worldID: worldID, levelNumberInWorld:levelNumberInWorld})[0];
	
	var phrases = FetchPhrases(levelData);
	
	for (var i = 0; i < phrases.length; i++){
		globals.col.phrases.update(
			{$id : phrases[i].$id}, 
			{$set : {isBeingUsed : true}
			}, 
			{}, 
			true
		);	
	}
	globals.col.phrases.commit();
	
	globals.col.groups.update(
		{levelNumberInWorld : levelNumberInWorld, worldID: worldID}, 
		{$set : {phrasesIDs : phrases, locked:false}
		}, 
		{}, 
		true
	);
	globals.col.groups.commit();
	
	if(worldID != "moon"){
		globals.col.gameData.update(
			{$id: globals.gameData.$id},
			{$set : {currentGlobalLevelNumber: levelData.levelID}},
			{},
			true
		);
		globals.col.gameData.commit();	
	}		
};


exports.InitAllLevelsForWorld = function(worldID){
	var gameData = globals.gameData;
	var worldData = globals.col.worlds.find({worldID:worldID})[0];
	
	var currentGlobalLevelNumber = 0;

	for (var j = 0; j < worldData.levelCount; j++){
		var levelData = globals.col.groups.find({worldID: worldID, levelNumberInWorld:j})[0];
		if (j == 0) currentGlobalLevelNumber = levelData.levelID;
		
		var phrases = FetchPhrases(levelData);
		for (var i = 0; i < phrases.length; i++){
			globals.col.phrases.update(
				{$id : phrases[i].$id}, 
				{$set : {isBeingUsed : true}
				}, 
				{}, 
				true
			);	
		}
		
		globals.col.groups.update(
			{levelNumberInWorld : j, worldID: worldID}, 
			{$set : {phrasesIDs : phrases, locked:false}
			}, 
			{}, 
			true
		);
	}

	if(worldID != "moon"){
		globals.col.gameData.update(
			{$id: globals.gameData.$id},
			{$set : {currentGlobalLevelNumber: currentGlobalLevelNumber}},
			{},
			true
		);
	}	
	
	globals.col.phrases.commit();
	globals.col.groups.commit();
	globals.col.gameData.commit();	
};


function FetchPhrases (levelData){
	var levelDifficulty = levelData.levelDifficulty;
	
	var phrases = [];
	
	for (var i =0; i < levelData.numberOfPhrases; i++){
		var presetPhrases = JSON.parse(levelData.presetPhrases);
		if (presetPhrases[i] != null){
			var phrase = globals.col.phrases.find({phraseID:presetPhrases[i]})[0];
			phrases.push(phrase);
		}
		else{
			var contingency = "up";
			if (levelDifficulty == 4)  contingency = "down";
			phrases.push(FindPhrase(levelDifficulty,contingency));
		}
	}
	
	function FindPhrase(difficulty,contingency){
		var originalDifficulty = difficulty;
		var getUsedPhrases = false;
		var phrase;
		/*
		var difficulties = [
			{low:  0,high: 8},
			{low:  9,high: 13},
			{low: 14,high: 16},
			{low: 17,high: 19},
			{low: 20,high: 99}
		];
		*/
		var difficulties = [
			{low: 0,high: 7},
			{low: 8,high: 11},
			{low: 12,high:14},
			{low:15,high:17},
			{low:17,high:99}
		];
		
		while (phrase == null){
			var low = difficulties[difficulty].low;
			var high = difficulties[difficulty].high;
			
			phrase = globals.col.phrases.find({isBeingUsed: getUsedPhrases, difficulty:{$gte:low,$lte:high}, type:{$nin: ["fortuneCookie","riddle"]}},{$sort:{$id: 0}})[0];
			
			if (contingency == "up") difficulty++;
			else difficulty --;
			
			//If you can't find any get a phrase you already used. 
			if (difficulty < 0 || difficulty > 4) {
				difficulty = originalDifficulty;
				getUsedPhrases = true;
				//break;
			}
		}
		
		return phrase;
	};
	
	return phrases;
}


exports.UnlockNextLevel = function(worldID){
	var LocalData = require('model/LocalData');
	var gameData = globals.gameData;
	var unlockNextLevel = false;
	//if (gameData.unlockNextLevel == true)
	{
		var currentWorld = globals.col.worlds.find({worldID: worldID})[0];
		var newLevel = gameData.currentLevel;
		var unlockNextWorld = false;
		var newWorldID = currentWorld.worldID;
		
		if (currentWorld.levelCompleteCount >= currentWorld.levelCount){
			newLevel = 0;
			unlockNextWorld = true;
		}
		else {
			var completeLevels = globals.col.groups.find({worldID:worldID, complete: true },{$sort:{levelNumberInWorld:-1}});
			newLevel = completeLevels[0].levelNumberInWorld+1;
			unlockNextLevel = true;
		}	
		
		
		globals.col.gameData.update(
			{$id:globals.gameData.$id},
			{$set : 
				{
					currentLevel: newLevel,
					unlockNextWorld: unlockNextWorld,
				}
			},
			{},
			true
		);
		globals.col.gameData.commit();
		
		//Initialize next level if you are not going to the new world. Otherwise do it 
		//When you unlock the new world. 
		if(unlockNextWorld == false){
			exports.InitLevel(newLevel, worldID);
			Ti.API.info("UnlockNextLevel: New Level Set: "+newLevel);
		}
	}
	
	return {unlock: unlockNextLevel, levelNumberInWorld:newLevel};
};



function GetJsonText (fileName){
	var readFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory,fileName);        
 
	if (readFile.exists()) {
		var readContents = readFile.read();
	   	return readContents.text;
	}
};


//Counter for the Phrases Needed
exports.LevelCount = function(){
	var levels = JSON.parse(GetJsonText("model/LevelData.txt"));
	
	var level0Count = 0;
	var level1Count = 0;
	var level2Count = 0;
	var level3Count = 0;
	var level4Count = 0;
	
	for (var i = 0; i < levels.length; i++){
		InitLevel(levels[i]);
	}
	
	function InitLevel(levelData){
		var levelDifficulty = levelData.levelDifficulty;
		if (levelData.numberOfPhrases == 1){
		if (levelDifficulty == 0)phrases = [FindPhrase(0,"up")];
			else if (levelDifficulty == 1) phrases = [FindPhrase(1,"up")];
			else if (levelDifficulty == 2) phrases = [FindPhrase(2,"up")];
			else if (levelDifficulty == 3) phrases = [FindPhrase(3,"up")];
			else if (levelDifficulty == 4) phrases = [FindPhrase(3,"up")];
			else if (levelDifficulty == 5) phrases = [FindPhrase(4,"up")];
			else if (levelDifficulty == 6) phrases = [FindPhrase(4,"up")];
		}
		else if (levelData.numberOfPhrases == 2){
			if (levelDifficulty == 0) phrases = [FindPhrase(0,"up"),FindPhrase(0,"up")];
			else if (levelDifficulty == 1) phrases = [FindPhrase(1,"up"),FindPhrase(1,"up")];
			else if (levelDifficulty == 2) phrases = [FindPhrase(2,"up"),FindPhrase(2,"up")];
			else if (levelDifficulty == 3) phrases = [FindPhrase(2,"up"),FindPhrase(3,"up")];
			else if (levelDifficulty == 4) phrases = [FindPhrase(3,"up"),FindPhrase(3,"up")];
			else if (levelDifficulty == 5) phrases = [FindPhrase(3,"up"),FindPhrase(4,"up")];
			else if (levelDifficulty == 6) phrases = [FindPhrase(4,"up"),FindPhrase(4,"up")];
		}
		else if (levelData.numberOfPhrases == 3){
			if (levelDifficulty == 0) phrases = [FindPhrase(0,"up"),FindPhrase(1,"up"),FindPhrase(2,"down")];
			else if (levelDifficulty == 1) phrases = [FindPhrase(1,"up"),FindPhrase(2,"up"),FindPhrase(2,"down")];
			else if (levelDifficulty == 2) phrases = [FindPhrase(2,"up"),FindPhrase(2,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 3) phrases = [FindPhrase(2,"up"),FindPhrase(3,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 4) phrases = [FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 5) phrases = [FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(4,"down")];
			else if (levelDifficulty == 6) phrases = [FindPhrase(4,"up"),FindPhrase(4,"up"),FindPhrase(4,"down")];
		}
		else if (levelData.numberOfPhrases == 4){
			if (levelDifficulty == 0) phrases = [FindPhrase(0,"up"),FindPhrase(0,"up"),FindPhrase(1,"up"),FindPhrase(2,"down")];
			else if (levelDifficulty == 1) phrases = [FindPhrase(1,"up"),FindPhrase(2,"up"),FindPhrase(2,"up"),FindPhrase(2,"down")];
			else if (levelDifficulty == 2) phrases = [FindPhrase(2,"up"),FindPhrase(2,"up"),FindPhrase(2,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 3) phrases = [FindPhrase(2,"up"),FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 4) phrases = [FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 5) phrases = [FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(4,"up"),FindPhrase(4,"down")];
			else if (levelDifficulty == 6) phrases = [FindPhrase(4,"up"),FindPhrase(4,"up"),FindPhrase(4,"up"),FindPhrase(4,"down")];
		}
		else if (levelData.numberOfPhrases == 5){
			if (levelDifficulty == 0)phrases = [FindPhrase(0,"up"),FindPhrase(0,"up"),FindPhrase(0,"up"),FindPhrase(1,"up"),FindPhrase(2,"down")];
			else if (levelDifficulty == 1) phrases = [FindPhrase(1,"up"),FindPhrase(2,"up"),FindPhrase(2,"up"),FindPhrase(2,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 2) phrases = [FindPhrase(2,"up"),FindPhrase(2,"up"),FindPhrase(2,"up"),FindPhrase(3,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 3) phrases = [FindPhrase(2,"up"),FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 4) phrases = [FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"down")];
			else if (levelDifficulty == 5) phrases = [FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(3,"up"),FindPhrase(4,"up"),FindPhrase(4,"down")];
			else if (levelDifficulty == 6) phrases = [FindPhrase(4,"up"),FindPhrase(4,"up"),FindPhrase(4,"up"),FindPhrase(4,"up"),FindPhrase(4,"down")];
		}
	}
	
	
	function FindPhrase(difficulty,contingency){
		if (difficulty == 0) level0Count++;
		else if (difficulty == 1) level1Count++;
		else if (difficulty == 2) level2Count++;
		else if (difficulty == 3) level3Count++;
		else if (difficulty == 4) level4Count++;
	};
	
	Ti.API.info("Level 0: "+level0Count);
	Ti.API.info("Level 1: "+level1Count);
	Ti.API.info("Level 2: "+level2Count);
	Ti.API.info("Level 3: "+level3Count);
	Ti.API.info("Level 4: "+level4Count);
	Ti.API.info("Total: "+(level0Count+level1Count+level2Count+level3Count+level4Count));
};
