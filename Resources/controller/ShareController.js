var AnalyticsController = require("/controller/AnalyticsController");
var RandomPlus = require("/lib/RandomPlus");

exports.CreateSharePhraseImage = function(text, authorText){
	var words = text.match(/\S+\s*/g);
	if (words == null) words = [];
	
	var view = Ti.UI.createView({
		backgroundColor: "#FEFEFA",//"#EEB200",//globals.colors.warning,
		borderColor: globals.colors.black,
		borderWidth: 1,
		width: 640,
		height: 640,
		bottom: -640
		//zIndex: 1000000
	});
	
	var container = Ti.UI.createView({
		width: 560,
		height: 560,
		layout: "vertical"
	});
	
	container.addEventListener("postlayout", function(){
		container.height = resultContainer.size.height+authorLabel.size.height+authorLabel.top;
		container.top = view.height/2-container.height/2;
	});
	
	var resultContainer = Ti.UI.createView({
		width: 560,
		height: Ti.UI.SIZE,
		layout: "horizontal"
	});
	
	
	for(var i = 0; i < words.length; i++){
		var n = i%globals.colors.wordBoxesColors.length; 
		var wordBox = exports.CreateWordBox(words[i], globals.colors.wordBoxesColors[n]);
		resultContainer.add(wordBox);
	}	
	
	var authorLabel = Ti.UI.createLabel({
		text: "-"+authorText,
		font: globals.fonts.bold35,
		width: 560,
		height: 40,
		color: globals.colors.black,
		textAlign: "right",
		top: 40
	});
	
	container.add(resultContainer);
	container.add(authorLabel);
	
	var logo = Ti.UI.createImageView({
		image:"/images/wordHopLogo.png",
		width: 299,
		height: 92,
		bottom: -10,
		right: -60,
		//transform: Ti.UI.create2DMatrix().scale(0.4,0.4)
	});
	
	view.add(container);
	
	
	//view.add(logo);
	
	return view;
};


exports.CreateSharePublicPhraseImage = function(text, introText, instructionsText, channel, callback){
	text = text+" ";
	
	var correctWordsArray = text.match(/\S+\s/g);
	var shuffledWordsArray = RandomPlus.ShuffleArrayCopy(correctWordsArray);
	
	var width = 598; //552
	var height = 296; //414
	var scale = 1;
	
	if(channel == "twitter"){
		width = 470;
		height = 253;
		scale = .67;
	}
	else if(channel == "message"){
		width = 480;
		height: 300;
	}
	
	var view = Ti.UI.createView({
		backgroundColor: globals.colors.white,//"#FEFEFA",//"#EEB200",//globals.colors.warning,
		borderColor: globals.colors.black,
		borderWidth: channel == "twitter" ? 0 : 1,
		width: width, //472 //600
		height: height, //393 //313
		left: -640,
		zIndex: 1000,
		//transform: Ti.UI.create2DMatrix().scale(scale, scale)
	});
	
	var container = Ti.UI.createView({
		//backgroundColor: "yellow",
		width: view.width,
		height: Ti.UI.SIZE,
		layout: "vertical"
	});
	
	var introLabel = Ti.UI.createLabel({
		//backgroundColor: "red",
		text: introText,
		font: globals.fonts.regular30,
		width: view.width - 30,
		height: 80,
		color: globals.colors.black,
		textAlign: "center",
		verticalAlign: "center",
		top: 20/2
	});
	
	var container2 = Ti.UI.createView({
		//backgroundColor: "blue",
		width: view.width-40,
		height: 140,
		top: 100
	});
	
	var resultContainer = Ti.UI.createView({
		//backgroundColor: "green",
		width: view.width - 40,
		height: Ti.UI.SIZE
		//top: 40/2
	});
	
	
	var wordsLabels = [];
	for(var i = 0; i < shuffledWordsArray.length; i++){
		var n = i%globals.colors.wordBoxesColors.length; 
		wordsLabels.push(exports.CreateWordBox(shuffledWordsArray[i], globals.colors.wordBoxesColors[n], 1));
		resultContainer.add(wordsLabels[i]);
	}
	
	container2.add(resultContainer);
	
	
	var logo = Ti.UI.createImageView({
		image:"/images/wordHopLogo.png",
		width: 299/4,
		height: 92/4,
		bottom: 15,
		right: 15
	});
	
	view.add(introLabel);	
	view.add(container2);
		
	view.add(logo);
	//view.add(container);
	
	
	
	setTimeout(ArrangeWords, 100);
	
	function ArrangeWords(){
		var width = resultContainer.width;
		var xPos = 0;
		var yPos = 0;
		var margin = 8/2;
		
		for (var j = 0; j < wordsLabels.length; j++){
			var rightEdge = xPos + wordsLabels[j].size.width+margin;
			if (rightEdge > width+12/2){
				xPos = 0;
				yPos += wordsLabels[j].size.height+margin;	
			}
			wordsLabels[j].left = xPos;
			wordsLabels[j].top = yPos;
			
			xPos += wordsLabels[j].size.width+margin;
		}
		
		//view.height = resultContainer.size.height + 380;
		
		setTimeout(function(){
			var image = view.toImage(null, true);
			callback(image);
			setTimeout(function(){globals.rootWin.remove(view);},2000);
		}, 100);
	}
	
	globals.rootWin.add(view);
	
	
	return view;
};


exports.CreateWordBox = function (text, color, sizeMultiplier){
	if(sizeMultiplier == null)  sizeMultiplier = 2;
	var view = Ti.UI.createView({
		backgroundColor: color,
		borderColor: globals.colors.borderLine2,
		borderWidth: 2*sizeMultiplier,
		borderRadius: 6*sizeMultiplier,
		width: Ti.UI.SIZE,
		height: 40*sizeMultiplier,
		left: 5*sizeMultiplier,
		top: 5*sizeMultiplier,
		opacity: 1,
		layout: "horizontal"
	});
	
	var label = Ti.UI.createLabel({
		text: text,
		font: sizeMultiplier == 1 ? globals.fonts.words2 : globals.fonts.words2Double,
		shadowOffset:{x:1*sizeMultiplier,y:1*sizeMultiplier},
		shadowColor: "black",
		left: 12*sizeMultiplier,
		height: 40*sizeMultiplier,
		color: globals.colors.wordsInBoxesUnselected
	});
	
	view.add(label);
	if(Ti.Platform.name == "android") view.add(Ti.UI.createView({width: 10*sizeMultiplier}));
	else view.add(Ti.UI.createView({width: 2*sizeMultiplier}));
	
	function Trim(textToTrim){
		var text2 = textToTrim;
		if(textToTrim[textToTrim.length-1] == " ") text2 = textToTrim.substring(0, textToTrim.length - 1);
		return text2;
	}
	
	return view;	
};


exports.SharePublicPuzzle = function(args){
	//args: url, messageBody, picture, channel,
	var Social = require('dk.napp.social');
	
	var text = {
		written:{
			facebook: {channel: "facebook", title: null, message: "#GoPhrazy Here's my puzzle.\nClick the link to solve!\n",},
			twitter: {channel: "twitter", title: null, message: "#GoPhrazy My puzzle. Click the link to solve! ",},
			email: {channel: "email", title: "Try to solve my GoPhrazy puzzle", message: "Click the link. \n\n"+args.url},
			message: {channel: "message", title: null, message: "Here's my GoPhrazy puzzle.\nClick the link to solve!\n\n"+args.url}, //messageBody: 
			copyPaste: {channel: "copyPaste", title: "", message: args.url}
		},
		general:{
			facebook: {channel: "facebook", title: null, message: "#GoPhrazy Try this puzzle.\nClick the link to solve!\n",},
			twitter: {channel: "twitter", title: null, message: "#GoPhrazy Try this puzzle. Click the link to solve! ",},
			email: {channel: "email", title: "Try to solve this GoPhrazy puzzle", message: "Click the link. \n\n"+args.url},
			message: {channel: "message", title: null, message: "Try this GoPhrazy puzzle.\nClick the link to solve!\n\n"+args.url}, //messageBody: 
			copyPaste: {channel: "copyPaste", title: "", message: args.url}
		},
		help:{
			facebook: {channel: "facebook", title: null, message: "#GoPhrazy Help me solve this puzzle!\nClick the link to solve!\n",},
			twitter: {channel: "twitter", title: null, message: "#GoPhrazy Help me solve this! Click the link to solve! ",},
			email: {channel: "email", title: "Help me solve this GoPhrazy puzzle! I'm stuck.", message: "Help me solve this, I'm stuck. Click the link. \n\n"+args.url},
			message: {channel: "message", title: null, message: "Help me solve this GoPhrazy puzzle! I'm stuck.\nClick the link to solve!\n\n"+args.url}, //messageBody: 
			copyPaste: {channel: "copyPaste", title: "", message: args.url}
		},
	};
	
	var actualType = text.written;
	if(args.shareType == "general") actualType = text.general;
	else if(args.shareType == "help") actualType = text.help;
	
	
	var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, "GoPhrazy.png");
	f.write(args.picture);
	
	if(args.channel == "facebook"){
		if(Social.isFacebookSupported()){ //min iOS6 required
			
			Social.facebook({
				text: actualType.facebook.message,
				image: f.nativePath, //local resource folder image
				url: args.url
			});
			setTimeout(function(){Ti.App.fireEvent("closeButtonBlocker");}, 1000);
			//AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:facebook", area: args.phraseID, value: globals.gameData.shareSolutionData.shareEmailCount});
		}
		else alert("Facebook not on!\nGo to your Settings/Facebook to fix it.");
	}	
	else if (args.channel == "email"){
    	var emailDialog = Ti.UI.createEmailDialog();
		emailDialog.subject = actualType.email.title;
		//emailDialog.toRecipients = ['foo@yahoo.com'];
		emailDialog.messageBody = actualType.email.message;
		emailDialog.addAttachment(args.picture);
		emailDialog.open();
		setTimeout(function(){Ti.App.fireEvent("closeButtonBlocker");}, 1000);
		//globals.gameData.shareSolutionData.shareEmailCount++;
		//globals.col.gameData.commit();	 
        
		//AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:email", area: args.phraseID, value: globals.gameData.shareSolutionData.shareEmailCount});
    }
	else if (args.channel == "twitter"){
		if(Social.isTwitterSupported()){
     		
     		var accounts = []; 
		    Social.addEventListener("accountList", function(e){
		    	Ti.API.info("Accounts:");
		    	accounts = e.accounts; //accounts
		    	Ti.API.info(accounts);
		    });
		    
		    Social.twitterAccountList();
	     	
	     	Social.twitter({
				text: actualType.twitter.message,
				image: f.nativePath,
				url: args.url
			});
			
			setTimeout(function(){Ti.App.fireEvent("closeButtonBlocker");},2000);
			//globals.gameData.shareSolutionData.shareTwitterCount++;
			//globals.col.gameData.commit();
			
			//AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:twitter", area: dataIn.phraseID, value: globals.gameData.shareSolutionData.shareTwitterCount});
     	}
     	else alert("Twitter not on!\nGo to your Settings/Twitter to fix this.");
	}
	else if (args.channel == "message"){
		//Titanium.Platform.openURL('sms:'+args.url);
		var SMS = require('com.omorandi');
		var smsDialog = SMS.createSMSDialog();
		smsDialog.messageBody = actualType.message.message;

        if (smsDialog.canSendAttachments()){
            smsDialog.addAttachment(f.nativePath);
        }
        else{
            Ti.API.info('We cannot send attachments');
        }

        smsDialog.addEventListener('complete', function(e){
            //Ti.API.info("Result: " + e.resultMessage);
           	//var a = Ti.UI.createAlertDialog({title: 'complete', message: 'Result: ' + e.resultMessage});
            //a.show();
            if (e.result == smsDialog.SENT)
            {
                //do something
            }
            else if (e.result == smsDialog.FAILED)
            {
               //do something else
            }
            else if (e.result == smsDialog.CANCELLED)
            {
               //don't bother
            }
            Ti.App.fireEvent("closeButtonBlocker");
        });

        smsDialog.open({animated: true});
	}
	else if (args.channel == "copyPaste"){
		Ti.UI.Clipboard.clearText();
		Ti.UI.Clipboard.setText(args.url);
		alert("Copied to Clipboard: "+Ti.UI.Clipboard.getText());
		Ti.App.fireEvent("closeButtonBlocker");
	}
};


exports.shareMenu = function(dataIn){
	var ServerData = require("model/ServerData");

	if (dataIn.correctWordsArray == null){
		var text = dataIn.text+" ";
		dataIn.correctWordsArray = text.match(/\S+\s/g);
		dataIn.shuffledWordsArray = RandomPlus.ShuffleArrayCopy(dataIn.correctWordsArray);
	}
	
	var messageBody = dataIn.message;
	if(dataIn.type == "help") messageBody+"\n\n"+CreateStringFromArray(dataIn.shuffledWordsArray);
	
	if(Ti.Platform.name == "iPhone OS"){
		var Social = require('dk.napp.social');
		var dialog = Ti.UI.createAlertDialog({
		    cancel: 3,
		    buttonNames: ['Facebook', 'Email', 'Twitter', "Cancel"],
		});
	 	
	  	dialog.addEventListener('click', function(e){
	   
		    if (e.index === e.source.cancel){
		      Ti.API.info('The cancel button was clicked');
		    }
		    if (e.index == 0){
				//=
				var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, "tempPic.png");
			    f.write(dataIn.picture);
	     		
	     		if(Social.isFacebookSupported()){ //min iOS6 required
					Social.facebook({
						text: messageBody,
						image: f.nativePath, //local resource folder image
						//url:"http://www.napp.dk"
					});
					var AnalyticsController = require("/controller/AnalyticsController");
					AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:facebook", area: dataIn.phraseID, value: globals.gameData.shareSolutionData.shareEmailCount});
				}
				else alert("Facebook not on!\nGo to your Settings/Facebook to fix it.");  	
				//=
		    }
		    else if (e.index == 1){
		    	var emailDialog = Ti.UI.createEmailDialog();
				emailDialog.subject = dataIn.message;
				//emailDialog.toRecipients = ['foo@yahoo.com'];
				emailDialog.messageBody = messageBody;
				emailDialog.addAttachment(dataIn.picture);
				emailDialog.open();
				
				globals.gameData.shareSolutionData.shareEmailCount++;
				globals.col.gameData.commit();	 
		        
		        var AnalyticsController = require("/controller/AnalyticsController");
				AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:email", area: dataIn.phraseID, value: globals.gameData.shareSolutionData.shareEmailCount});
		    }
		    else if (e.index == 2){
		     	Ti.API.info("Twitter available: " + Social.isTwitterSupported());
		     	
		     	if(Social.isTwitterSupported()){
		     		var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, "GoPhrazy.png");
				    f.write(dataIn.picture);
		     		
		     		var accounts = []; 
				    Social.addEventListener("accountList", function(e){
				    	Ti.API.info("Accounts:");
				    	accounts = e.accounts; //accounts
				    	Ti.API.info(accounts);
				    });
				    
				    Social.twitterAccountList();
			     	
			     	Social.twitter({
						text: messageBody,
						image: f.nativePath
						//url:"http://www.mozzarello.com"
					});
					
					globals.gameData.shareSolutionData.shareTwitterCount++;
					globals.col.gameData.commit();
					
					 var AnalyticsController = require("/controller/AnalyticsController");
					AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:twitter", area: dataIn.phraseID, value: globals.gameData.shareSolutionData.shareTwitterCount});
		     	}
		     	else alert("Twitter not on!\nGo to your Settings/Twitter to fix this.");
		     }
		});
	
		dialog.show();
	}
	else{
		var dialog = Ti.UI.createAlertDialog({
		    cancel: 3,
		    buttonNames: ['Facebook', 'Email', "Cancel"],
		});
	 	
	  	dialog.addEventListener('click', function(e){
	  		if (e.index === e.source.cancel){
		    	Ti.API.info('The cancel button was clicked');
		    }
		    if (e.index == 0){//Facebook
				var fb = require("facebook");
			
				var data = {
				    message: messageBody,
				    picture: dataIn.picture.media
				};
				
				fb.requestWithGraphPath('me/photos', data, 'POST', function(e){
				    if (e.success) {
				        var AnalyticsController = require("/controller/AnalyticsController");
						AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:facebook", area: dataIn.phraseID, value: globals.gameData.shareSolutionData.shareEmailCount});
						alert("Success!  From FB: " + e.result);
				    }
				    else{
				        if (e.error) {
				            alert(e.error);
				        } 
				        else{
				            alert("Unkown result");
				        }
				    }
				});
		    }
		    else if (e.index == 1){
		    	var emailDialog = Ti.UI.createEmailDialog();
				emailDialog.subject = dataIn.message;
				emailDialog.messageBody = messageBody;
				
				emailDialog.addAttachment(dataIn.picture.media);
				emailDialog.open();
				globals.gameData.shareSolutionData.shareEmailCount++;
				globals.col.gameData.commit();	 
		        
		        var AnalyticsController = require("/controller/AnalyticsController");
				AnalyticsController.RegisterEvent({category:"design", event_id: "sharedSolution:email", area: dataIn.phraseID, value: globals.gameData.shareSolutionData.shareEmailCount});
			}
	  	});
	  	
	  	dialog.show();
	}
	
	
	function CreateStringFromArray(array){
		var string = "";
		
		for(var i = 0; i <= array.length; i++){
			if(i < array.length) string += "/ "+array[i];
			else string += "/";
		}
		
		return string;
	}
	
};
