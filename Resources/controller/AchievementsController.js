exports.Init = function(){
	var Common = require('ui/Common');
	
	var achievements = globals.col.achievements.getAll();
	if (achievements.length == 0){
		var data = [{"name":"Skips Unlocked","achievementID":"skips_unlocked","description":"Skips Unlocked","goalValue":1,"points":1,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Hints Unlocked","achievementID":"hints_unlocked","description":"Hints Unlocked","goalValue":1,"points":2,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			//{"name":"Puzzle Post Unlocked","achievementID":"puzzlePost_unlocked","description":"Puzzle Post Unlocked","goalValue":1,"points":3,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Fortune Cookies Unlocked","achievementID":"fortuneCookies_unlocked","description":"Fortune Cookies Unlocked","goalValue":1,"points":4,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			//{"name":"Write your Own Puzzles Unlocked","achievementID":"composePuzzle_unlocked","description":"Write your Own Puzzles Unlocked","goalValue":1,"points":5,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Solve 50 Puzzles","achievementID":"solved_50","description":"Solve 50 Puzzles","goalValue":50,"points":5,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			{"name":"Solve 100 Puzzles","achievementID":"solved_100","description":"Solve 100 Puzzles","goalValue":100,"points":10,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			{"name":"Solve 250 Puzzles","achievementID":"solved_250","description":"Solve 250 Puzzles","goalValue":250,"points":25,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			{"name":"Solve 500 Puzzles","achievementID":"solved_500","description":"Solve 500 Puzzles","goalValue":500,"points":50,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			{"name":"Solve 1000 Puzzles","achievementID":"solved_1000","description":"Solve 1000 Puzzles","goalValue":1000,"points":100,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			{"name":"25 Puzzles in one day","achievementID":"solved_25_one_day","description":"25 Puzzles in one day","goalValue":25,"points":2,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"30 Puzzles in one day","achievementID":"solved_30_one_day","description":"30 Puzzles in one day","goalValue":30,"points":3,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"40 Puzzles in one day","achievementID":"solved_40_one_day","description":"40 Puzzles in one day","goalValue":40,"points":6,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"50 Puzzles in one day","achievementID":"solved_50_one_day","description":"50 Puzzles in one day","goalValue":50,"points":10,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"75 Puzzles in one day","achievementID":"solved_75_one_day","description":"75 Puzzles in one day","goalValue":75,"points":15,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"100 Puzzles in one day","achievementID":"solved_100_one_day","description":"100 Puzzles in one day","goalValue":100,"points":20,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"Solve a new puzzle on Monday","achievementID":"solved_monday","description":"Solve a new puzzle on Monday","goalValue":1,"points":1,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Solve a new puzzle on Tuesday","achievementID":"solved_tuesday","description":"Solve a new puzzle on Tuesday","goalValue":1,"points":1,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Solve a new puzzle on Wednesday","achievementID":"solved_wednesday","description":"Solve a new puzzle on Wednesday","goalValue":1,"points":1,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Solve a new puzzle on Thursday","achievementID":"solved_thursday","description":"Solve a new puzzle on Thursday","goalValue":1,"points":1,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Solve a new puzzle on Friday","achievementID":"solved_friday","description":"Solve a new puzzle on Friday","goalValue":1,"points":1,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Solve a new puzzle on Saturday","achievementID":"solved_saturday","description":"Solve a new puzzle on Saturday","goalValue":1,"points":1,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Solve a new puzzle on Sunday","achievementID":"solved_sunday","description":"Solve a new puzzle on Sunday","goalValue":1,"points":1,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"Solve a new puzzle every day of the week","achievementID":"solved_everyday_of_the_week","description":"Solve a new puzzle every day of the week","goalValue":1,"points":7,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			{"name":"3 Fortune Cookies in a row","achievementID":"solved_cookie_3_streak","description":"3 Fortune Cookies in a row","goalValue":3,"points":3,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"7 Fortune Cookies in a row","achievementID":"solved_cookie_7_streak","description":"7 Fortune Cookies in a row","goalValue":7,"points":7,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"14 Fortune Cookies in a row","achievementID":"solved_cookie_14_streak","description":"14 Fortune Cookies in a row","goalValue":14,"points":14,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"30 Fortune Cookies in a row","achievementID":"solved_cookie_30_streak","description":"30 Fortune Cookies in a row","goalValue":30,"points":30,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"60 Fortune Cookies in a row","achievementID":"solved_cookie_60_streak","description":"60 Fortune Cookies in a row","goalValue":60,"points":60,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"Solve a new puzzle every day for 5 days","achievementID":"solved_new_5_day_streak","description":"Solve a new puzzle every day for 5 days","goalValue":5,"points":5,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"Solve a new puzzle every day for 10 days","achievementID":"solved_new_10_day_streak","description":"Solve a new puzzle every day for 10 days","goalValue":10,"points":10,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"Solve a new puzzle every day for 20 days","achievementID":"solved_new_20_day_streak","description":"Solve a new puzzle every day for 20 days","goalValue":20,"points":20,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			{"name":"Solve a new puzzle every day for 50 days","achievementID":"solved_new_50_day_streak","description":"Solve a new puzzle every day for 50 days","goalValue":50,"points":50,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":1},
			//{"name":"Completed 1 Puzzle Post issue","achievementID":"complete_1_puzzle_post_issue","description":"Completed 1 Puzzle Post issue","goalValue":1,"points":5,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Completed 5 Puzzle Post issues","achievementID":"complete_5_puzzle_post_issue","description":"Completed 5 Puzzle Post issues","goalValue":5,"points":15,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Completed 10 Puzzle Post issues","achievementID":"complete_10_puzzle_post_issue","description":"Completed 10 Puzzle Post issues","goalValue":10,"points":30,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Completed 25 Puzzle Post issues","achievementID":"complete_25_puzzle_post_issue","description":"Completed 25 Puzzle Post issues","goalValue":25,"points":40,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Completed 50 Puzzle Post issues","achievementID":"complete_50_puzzle_post_issue","description":"Completed 50 Puzzle Post issues","goalValue":50,"points":100,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Have at least 1 friend","achievementID":"friends_1","description":"Have at least 1 friend","goalValue":1,"points":10,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Have at least 3 friends","achievementID":"friends_3","description":"Have at least 3 friends","goalValue":3,"points":30,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Have at least 5 friends","achievementID":"friends_5","description":"Have at least 5 friends","goalValue":5,"points":50,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Have at least 10 friends","achievementID":"friends_10","description":"Have at least 10 friends","goalValue":10,"points":100,"hidden":0,"repeatable":0,"released":1,"showCurrent":1,"showBest":0},
			//{"name":"Be #1 amongst your friends (3 Friends Minimum)","achievementID":"first_rank","description":"Be #1 amongst your friends (3 Friends Minimum)","goalValue":1,"points":10,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0},
			//{"name":"Compose and send a puzzle to a friend","achievementID":"compose_send_puzzle","description":"Compose and send a puzzle to a friend","goalValue":1,"points":10,"hidden":0,"repeatable":0,"released":1,"showCurrent":0,"showBest":0}
			];
				
			
				
		for (var i = 0; i < data.length; i++){
			globals.col.achievements.save({
				achievementID: data[i].achievementID,
				orderID: data[i].orderID,
				name: data[i].name,
				description: data[i].description,
				currentValue: 0,
				bestValue: 0,
				goalValue: data[i].goalValue,
				points: data[i].points,
				hidden: data[i].hidden == 0 ? false : true,
				repeatable: data[i].repeatable == 0 ? false : true,
				released: data[i].released == 0 ? false : true,
				showCurrent: data[i].showCurrent == 0 ? false : true,
				showBest: data[i].showBest == 0 ? false : true,
				complete: false,
			});
		}
		
		globals.col.achievements.commit();
	}
	
	
	exports.BootCheckAchievements();
	
	Ti.App.addEventListener("resume", exports.BootCheckAchievements);
};


exports.BootCheckAchievements = function(){
	Ti.API.info("ACHIEVEMENTS: Boot Check");
	var now = clock.getNow();
	var yesterday = new moment(now.valueOf()).subtract("d",1);
	
	//If new day reset solved in one day
	var lastClosedTime = new moment(globals.gameData.lastClosedTime);
	if((now.valueOf() > lastClosedTime.valueOf()) && (now.dayOfYear() != lastClosedTime.dayOfYear())){
		Ti.API.info("ACHIEVEMENTS: It's a brand new day! Resetting Solved in one day");
		exports.ResetAchievement("solved_25_one_day");
		exports.ResetAchievement("solved_30_one_day");
		exports.ResetAchievement("solved_40_one_day");
		exports.ResetAchievement("solved_50_one_day");
		exports.ResetAchievement("solved_75_one_day");
		exports.ResetAchievement("solved_100_one_day");
	}
	
	
	//If didn't solve fortune cookie yesterday
	var lastFortuneCookieSolvedTime = new moment(globals.gameData.lastFortuneCookieSolvedTime);
	if(lastFortuneCookieSolvedTime.valueOf() < yesterday.startOf("day").valueOf()){
		Ti.API.info("ACHIEVEMENTS: No fortune cookes solved yesterday. Resetting streaks.");
		exports.ResetAchievement("solved_cookie_3_streak");
		exports.ResetAchievement("solved_cookie_7_streak");
		exports.ResetAchievement("solved_cookie_14_streak");
		exports.ResetAchievement("solved_cookie_30_streak");
		exports.ResetAchievement("solved_cookie_60_streak");
	}
	
	
	//If didn't solve a new puzzle yesterday
	var lastNewPuzzleSolvedTime = new moment(globals.gameData.lastNewPuzzleSolvedTime);
	if(lastNewPuzzleSolvedTime.valueOf() < yesterday.startOf("day").valueOf()){
		Ti.API.info("ACHIEVEMENTS: No new puzzles solved yesterday. Resetting streaks.");
		exports.ResetAchievement("solved_new_5_day_streak");
		exports.ResetAchievement("solved_new_10_day_streak");
		exports.ResetAchievement("solved_new_20_day_streak");
		exports.ResetAchievement("solved_new_50_day_streak");
	}	
	
	globals.col.achievements.commit();
};


exports.IncreaseAchievement = function(achievementID, increaseValue, save){
	var achievement = globals.col.achievements.find({achievementID:  achievementID})[0];
	
	if(achievement && achievement.complete == false){
		Ti.API.info("ACHIEVEMENTS: Increased Achievement :"+achievement.achievementID);
		var newValue = achievement.currentValue + increaseValue;
		exports.SetAchievementValue(achievementID,newValue, save);
	}
	
	Ti.App.fireEvent("updateAchievementCompleteCount", {value: globals.col.achievements.count({complete: true})});
};

exports.SetAchievementValue = function(achievementID, newValue, save){
	var achievement = globals.col.achievements.find({achievementID:  achievementID})[0];
	achievement.currentValue = newValue;
	if(achievement.currentValue >= achievement.goalValue) {
		achievement.currentValue = achievement.goalValue;
		if(achievement.complete == false){
			achievement.complete = true;
			Ti.API.info("ACHIEVEMENTS: Completed Achievement :"+achievement.achievementID);
			//Ti.App.fireEvent("achievementComplete", {data:achievement});
			if(Ti.Platform.name == "iPhone OS"){
				if(globals.gameCenter.active == true){
					var gamekit = require('com.obigola.gamekit');
					gamekit.submitAchievement(achievementID,'100.0');
				}
				else TopNotification();
			}
			else TopNotification();
		}
	}	
	
	if(achievement.currentValue > achievement.bestValue) achievement.bestValue = achievement.currentValue;
		
	if(save == true) globals.col.achievements.commit();
		
	function TopNotification(){
		var copy = JSON.parse(JSON.stringify(achievement));
		copy.type = "achievement";
		if(globals.rootWin) globals.rootWin.OpenWindow("topNotificationsPopUp", copy);
		else {setTimeout(function(){if(globals.rootWin) globals.rootWin.OpenWindow("topNotificationsPopUp", copy);},2000);}
	}
};


exports.ResetAchievement = function(achievementID, save){
	var achievement = globals.col.achievements.find({achievementID:  achievementID})[0];
	
	if(achievement){
		achievement.currentValue = 0;
		Ti.API.info("ACHIEVEMENTS: Reset Achievement :"+achievement.achievementID);
		if(save == true) globals.col.achievements.commit();
	}
};


exports.GetAchievement = function(achievementID){
	return globals.col.achievements.find({achievementID:achievementID})[0];
};


exports.PhraseCompleteCheck = function(phrase){
	var now = clock.getNow();
	exports.IncreaseAchievement("solved_50", 1);
	exports.IncreaseAchievement("solved_100", 1);
	exports.IncreaseAchievement("solved_250", 1);
	exports.IncreaseAchievement("solved_500", 1);
	exports.IncreaseAchievement("solved_1000", 1);
	exports.IncreaseAchievement("solved_25_one_day", 1);
	exports.IncreaseAchievement("solved_30_one_day", 1);
	exports.IncreaseAchievement("solved_40_one_day", 1);
	exports.IncreaseAchievement("solved_50_one_day", 1);
	exports.IncreaseAchievement("solved_75_one_day", 1);
	exports.IncreaseAchievement("solved_100_one_day", 1);
	
	if(now.day() == 1) exports.IncreaseAchievement("solved_monday", 1);
	else if(now.day() == 2) exports.IncreaseAchievement("solved_tuesday", 1);
	else if(now.day() == 3) exports.IncreaseAchievement("solved_wednesday", 1);
	else if(now.day() == 4) exports.IncreaseAchievement("solved_thursday", 1);
	else if(now.day() == 5) exports.IncreaseAchievement("solved_friday", 1);
	else if(now.day() == 6) exports.IncreaseAchievement("solved_saturday", 1);
	else if(now.day() == 0) exports.IncreaseAchievement("solved_sunday", 1);
	
	//Check the whole week thing
	if(exports.GetAchievement("solved_monday").currentValue == 1 &&
		exports.GetAchievement("solved_tuesday").currentValue == 1 &&
		exports.GetAchievement("solved_wednesday").currentValue == 1 &&
		exports.GetAchievement("solved_thursday").currentValue == 1 &&
		exports.GetAchievement("solved_friday").currentValue == 1 &&
		exports.GetAchievement("solved_saturday").currentValue == 1 &&
		exports.GetAchievement("solved_sunday").currentValue == 1
	) 	 exports.IncreaseAchievement("solved_everyday_of_the_week", 1);
	
	
	if(phrase.type == "fortuneCookie"){
		exports.IncreaseAchievement("solved_cookie_3_streak", 1);
		exports.IncreaseAchievement("solved_cookie_7_streak", 1);
		exports.IncreaseAchievement("solved_cookie_14_streak", 1);
		exports.IncreaseAchievement("solved_cookie_30_streak", 1);
		exports.IncreaseAchievement("solved_cookie_60_streak", 1);
	}
	
	var lastNewPuzzleSolvedTime = new moment(globals.gameData.lastNewPuzzleSolvedTime);
	if(phrase.completeCount == 1 && (now.startOf("day").valueOf() > lastNewPuzzleSolvedTime.startOf("day").valueOf())){
		exports.IncreaseAchievement("solved_new_5_day_streak", 1);
		exports.IncreaseAchievement("solved_new_10_day_streak", 1);
		exports.IncreaseAchievement("solved_new_20_day_streak", 1);
		exports.IncreaseAchievement("solved_new_50_day_streak", 1);
	}
	
	globals.col.achievements.commit();
};


exports.GroupCheck = function (){
	exports.IncreaseAchievement("complete_1_puzzle_post_issue", 1);
	exports.IncreaseAchievement("complete_5_puzzle_post_issue", 1);
	exports.IncreaseAchievement("complete_10_puzzle_post_issue", 1);
	exports.IncreaseAchievement("complete_25_puzzle_post_issue", 1);
	exports.IncreaseAchievement("complete_50_puzzle_post_issue", 1);
	
	globals.col.achievements.commit();
};


exports.FriendsCheck = function (friendsCount){
	exports.SetAchievementValue("friends_1", friendsCount);
	exports.SetAchievementValue("friends_3", friendsCount);
	exports.SetAchievementValue("friends_5", friendsCount);
	exports.SetAchievementValue("friends_10", friendsCount);
	
	globals.col.achievements.commit();
};
