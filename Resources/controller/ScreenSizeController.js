exports.Init = function(){
	Ti.API.info('Screen Size Controller: Ti.Platform.displayCaps.density: ' + Ti.Platform.displayCaps.density);
	Ti.API.info('Screen Size Controller: Ti.Platform.displayCaps.dpi: ' + Ti.Platform.displayCaps.dpi);
	Ti.API.info("Screen Size Controller: Screen Width: "+Ti.Platform.displayCaps.platformWidth);
	Ti.API.info("Screen Size Controller: Screen Height: "+Ti.Platform.displayCaps.platformHeight);
	
	globals.platform.scale = 1;
	
	function GetWidthAndHeight(){
		var width;
		var height;
		if(Ti.UI.orientation ==  Ti.UI.PORTRAIT || Ti.UI.orientation ==  Ti.UI.UPSIDE_PORTRAIT){
			width = Ti.Platform.displayCaps.platformWidth;
			height = Ti.Platform.displayCaps.platformHeight;
		}
		else{
			width = Ti.Platform.displayCaps.platformHeight;
			height = Ti.Platform.displayCaps.platformWidth;
		}
		return {width: width, height: height};
	}
	
	if (globals.os == "ios"){
		globals.imagesPath = "/iphone/images";
		
		globals.platform.width = GetWidthAndHeight().width;
		globals.platform.height = GetWidthAndHeight().height;
		globals.platform.widthNumber = globals.platform.width ;
		globals.platform.heightNumber = globals.platform.heightNumber;
		var width = globals.platform.width;
		var height = globals.platform.height;
		globals.platform.actualHeight = globals.platform.height-50;
		globals.platform.actualHeightNumber = globals.platform.actualHeight;
		globals.platform.scale = 1;
		
		var aspectRatio = height/width;
		if(width > 414) {
			globals.platform.width = 414;
			globals.platform.widthNumber = 414;
			globals.platform.height = aspectRatio*globals.platform.width;
			globals.platform.heightNumber = aspectRatio*globals.platform.width;
			globals.platform.actualHeight = globals.platform.height-50;
			globals.platform.actualHeightNumber = globals.platform.height-50;
			globals.platform.scale = globals.platform.scale*GetWidthAndHeight().height/globals.platform.height;
			Ti.API.info("Screen Size Controller: iPad: Capped at 414 width");
			Ti.API.info("Screen Size Controller: iPad: Scale: "+globals.platform.scale);
		}	
	}
	else{
		globals.imagesPath = "/android/images";
		var width = Ti.Platform.displayCaps.platformWidth;
		var height = Ti.Platform.displayCaps.platformHeight;
		globals.platform.width = Ti.Platform.displayCaps.platformWidth/Ti.Platform.displayCaps.logicalDensityFactor;
		globals.platform.widthNumber = Ti.Platform.displayCaps.platformWidth/Ti.Platform.displayCaps.logicalDensityFactor;
		globals.platform.height = Ti.Platform.displayCaps.platformHeight/Ti.Platform.displayCaps.logicalDensityFactor;
		globals.platform.heightNumber = Ti.Platform.displayCaps.platformHeight/Ti.Platform.displayCaps.logicalDensityFactor;
		globals.platform.actualHeight = globals.platform.height-50;
		globals.platform.actualHeightNumber = globals.platform.actualHeight;
		
		globals.platform.scale = 1/Ti.Platform.displayCaps.logicalDensityFactor;
		Ti.API.info("Screen Size Controller: Factor: "+Ti.Platform.displayCaps.logicalDensityFactor);
		Ti.API.info("Screen Size Controller: Android: Scale / factor: "+globals.platform.scale);
		
		
		var aspectRatio = height/width;
		if(width > 414) {
			globals.platform.width = 414;
			globals.platform.widthNumber = 414;
			globals.platform.height = aspectRatio*globals.platform.width;
			globals.platform.heightNumber = aspectRatio*globals.platform.width;
			globals.platform.actualHeight = globals.platform.height-50;
			globals.platform.actualHeightNumber = globals.platform.height-50;
			globals.platform.scale = globals.platform.scale*Ti.Platform.displayCaps.platformHeight/globals.platform.height;
			Ti.API.info("Screen Size Controller: Android: Huge Screen: Capped at 414 width");
			Ti.API.info("Screen Size Controller: Android: Huge Screen: Scale: "+globals.platform.scale);
		}	
	}
	
	globals.adsBannerSpace = 50;//globals.gameData.ads.bannersEnabled == true ? 50 : 0;
	
	Ti.API.info("Screen Size Controller: Final Width: "+globals.platform.width);
	Ti.API.info("Screen Size Controller: Final Height: "+globals.platform.height);
	Ti.API.info("Screen Size Controller: Actual Height(no ads): "+globals.platform.actualHeight);
	Ti.API.info("Screen Size Controller: Final Scale: "+globals.platform.scale);
};
