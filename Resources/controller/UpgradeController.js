exports.CheckForUpgrade = function(){
	//Check for local upgrade
	var localCurrentVersion = Common.GetJsonText("model/"+globals.systemData.currenti18n+"/CurrentVersion.txt");
	var currentVersionObject = JSON.parse(localCurrentVersion);
	
	//Production
	//globals.upgradeBucket = "https://gophrazyproduction.s3.amazonaws.com/";
	
	//Sandbox
	globals.upgradeBucket = "https://gophrazysandbox.s3.amazonaws.com/";
	
	
	var appVersion = "";
	if(Ti.Platform.name == "iPhone OS") appVersion = currentVersionObject.iOSAppVersion;
	else if(Ti.Platform.name == "android") appVersion = currentVersionObject.androidAppVersion;
	
	Ti.API.info("Version Compare: "+VersionCompare(appVersion , globals.gameData.versions.appVersion));
	//Check and Upgrade From Local
	if (VersionCompare(appVersion , globals.gameData.versions.appVersion) == 1){
		Ti.API.info("UPGRADE CONTROLLER: Upgrading from Local data after app update");
		Ti.App.fireEvent("UpdgradeStatus",{value:"Uhh! Found new puzzles!"});
		UpgradeData(currentVersionObject, true);
		CheckServerData(false);
	}
	//Check Online for upgrade if local is not new.
	else{
		var upgradeTime = new moment(globals.gameData.upgradeTime);
		var now = clock.getNow();
		if (now.valueOf() > upgradeTime.valueOf()) CheckServerData(true);
		else Ti.App.fireEvent("startGame");	
	}	
	
	
	function CheckServerData(offLineEnabled){
		if(Titanium.Network.online == true){
			Ti.API.info("UPGRADE CONTROLLER: Upgrading from Server");
			Ti.App.fireEvent("UpdgradeStatus",{value:"Yes! Found new Puzzles!"});
			exports.GetFile(globals.upgradeBucket+globals.systemData.currenti18n+"/","CurrentVersion.txt", false, function(e){
				Ti.App.fireEvent("UpdgradeStatus",{value:"Shuffling words! He he!"});
				var versionObject = JSON.parse(e);
				
				if(versionObject.updating && versionObject.updating == true) {
					Ti.App.fireEvent("startGame");
					alert("Upps. We are uploading new puzzles.\nComeback in a couple of minutes for new content.\nIn the meantime you can play.");
				}	
				else UpgradeData(versionObject, false);
			});
		}
		else if(offLineEnabled == true) Ti.App.fireEvent("startGame");
	}
};


function UpgradeData(versionObject, isLocal){
	var PhrasesController = require("controller/PhrasesController");
	var GroupsController = require("controller/GroupsController");
	var LocalData = require("/model/LocalData");
	
	var appVersion = "";
	if(Ti.Platform.name == "iPhone OS") appVersion = versionObject.iOSAppVersion;
	else if(Ti.Platform.name == "android") appVersion = versionObject.androidAppVersion;
	
	if (isLocal == false){
		var phrasesUpdated = false;
		var groupsUpdated = false;
		var changesUpdated = false;
		Ti.API.info( "UPGRADE CONTROLLER: Version Compare: "+VersionCompare(versionObject.appVersion, globals.gameData.versions.appVersion));	
		
		if (VersionCompare(appVersion, globals.gameData.versions.appVersion) == 1){
			var UpgradeVersionPopUp = require("/ui/PopUps/UpgradeVersionPopUp");
			var upgradeVersionPopUp = new UpgradeVersionPopUp();
			globals.gameContainer.add(upgradeVersionPopUp);
			upgradeVersionPopUp.Open();
		}
		else{
			CheckPhrases();//Start the check loop
		}
	}
	else if (isLocal == true){
		var phrasesUpdated = false;
		var groupsUpdated = false;
		var changesUpdated = false;
		//This upgrade happens after you updated the app throught iTunes.
		if (VersionCompare(appVersion, globals.gameData.versions.appVersion) == 1){
			LocalData.UpgradeGameDataFromLocal();
			globals.gameData.versions.appVersion = appVersion;
			globals.col.gameData.commit();
			Ti.API.info("UPGRADE CONTROLLER: App Version Number from App Upgrade Data to Version: "+globals.gameData.versions.appVersion);
		}
		
		if (versionObject.phrasesVersion > globals.gameData.versions.phrasesVersion){
			PhrasesController.UpgradePhrases(JSON.parse(Common.GetJsonText("model/"+globals.systemData.currenti18n+"/PhrasesDB.txt")), versionObject.phrasesVersion);
			phrasesUpdated = true;
			globals.gameData.versions.phrasesVersion = versionObject.phrasesVersion;
			globals.col.gameData.commit();
			Ti.API.info("UPGRADE CONTROLLER: Main Phrases from App Upgrade Data to Version: "+globals.gameData.versions.phrasesVersion);
		}
		else phrasesUpdated = true;
		
		if (versionObject.groupsVersion > globals.gameData.versions.groupsVersion){
			GroupsController.UpgradeCollections(JSON.parse(Common.GetJsonText("model/"+globals.systemData.currenti18n+"/GroupsDB.txt")), versionObject.eventsVersion);
			groupsUpdated = true;
			globals.gameData.versions.groupsVersion = versionObject.groupsVersion;
			globals.col.gameData.commit();
			Ti.API.info("UPGRADE CONTROLLER: Groups from App Upgrade Data to Version: "+globals.gameData.versions.groupsVersion);
		}
		else groupsUpdated = true;
		
		changesUpdated = true; //There is no changes file locally ever updates only come from online
		
		//CheckUpdateStatus();
	}
	
	function CheckPhrases(){
		if (versionObject.phrasesVersion > globals.gameData.versions.phrasesVersion){
			var updatingPhrases = false;
			exports.GetFile(globals.upgradeBucket+globals.systemData.currenti18n+"/","PhrasesSinceVersion"+(globals.gameData.versions.phrasesVersion+1)+".txt",true, function(e){
			 	if(updatingPhrases == false){
			 		updatingPhrases = true;
			 		
					PhrasesController.UpgradePhrases(JSON.parse(e), versionObject.phrasesVersion);
					phrasesUpdated = true;
			
					globals.gameData.versions.phrasesVersion = versionObject.phrasesVersion;
					globals.col.gameData.commit();
					Ti.API.info("UPGRADE CONTROLLER: Main Phrases from Online Data to Version: "+globals.gameData.versions.phrasesVersion);
					CheckGroups();
					//if (groupsUpdated == true && phrasesUpdated == true && changesUpdated == true) CheckUpdateStatus();
			 	}
			});
		}
		else {
			phrasesUpdated = true;
			CheckGroups();
		}	
	}
	
	
	function CheckGroups(){
		if (versionObject.groupsVersion > globals.gameData.versions.groupsVersion){
			var updatingGroups = false;
			//Putting slight delay to avoid stuck
			//setTimeout(function(){
				exports.GetFile(globals.upgradeBucket+globals.systemData.currenti18n+"/","GroupsSinceVersion"+(globals.gameData.versions.groupsVersion+1)+".txt",true, function(e){
				if(updatingGroups == false){
					updatingGroups = true;
					
					GroupsController.UpgradeCollections(JSON.parse(e),versionObject.groupsVersion);
					groupsUpdated = true;
					
					globals.gameData.versions.groupsVersion = versionObject.groupsVersion;
					globals.col.gameData.commit();
					Ti.API.info("UPGRADE CONTROLLER: Groups from Online Data to Version: "+globals.gameData.versions.groupsVersion);
					//if (groupsUpdated == true && phrasesUpdated == true && changesUpdated == true) CheckUpdateStatus();
					CheckChanges();
				}
			});
			//},50);
		}
		else {
			groupsUpdated = true;
			CheckChanges();
		}	
	}
	
	
	function CheckChanges(){
		if (versionObject.changesVersion > globals.gameData.versions.changesVersion){
			var updatingChanges = false;
			//Putting slight delay to avoid stuck
			//setTimeout(function(){
			exports.GetFile(globals.upgradeBucket+globals.systemData.currenti18n+"/","ChangesSinceVersion"+(globals.gameData.versions.changesVersion+1)+".txt",true, function(e){
				if(updatingChanges == false){
					updatingChanges = true;
					
					var allData = JSON.parse(e);
					
					for (var i = 0; i < allData.data.length; i++){
						var change = allData.data[i];
						change.timeStamp = new moment(change.timeStamp).valueOf();
						//Save the changeData for report and What's new display
						var match = globals.col.changes.find({changeID: change.changeID})[0];
						if(match == null) {
							globals.col.changes.save(change);
						}	
						
						//Doing the changes
						var changeData = JSON.parse(change.changeData);
						if(changeData.gameData) LocalData.UpgradeGameDataFromChangesFile(changeData.gameData);
						if(changeData.phrases) PhrasesController.UpdatePhrases(changeData.phrases);
						if(changeData.groups) GroupsController.UpdateGroups(changeData.groups);
					}
					
					globals.col.changes.commit();
					
					
					globals.gameData.versions.changesVersion = allData.changesTillVersion;
					globals.col.gameData.commit();
					changesUpdated = true;
					Ti.API.info("UPGRADE CONTROLLER: Changes from Online Data to Version: "+globals.gameData.versions.changesVersion);
					//if (groupsUpdated == true && phrasesUpdated == true && changesUpdated == true) 
					CheckUpdateStatus();
				}
			});
			//},100);
		}
		else {
			changesUpdated = true;
			CheckUpdateStatus();
		}	
	}
	
	
	function CheckUpdateStatus(){
		if(isLocal == false){
			//This assures you check with Amazon S3 only once per day.
			//It's best to release the update shortly after 1 AM Italy Time or 5 PM Colorado Time.
			var now = clock.getNow();
			var upgradeTime = new moment(globals.gameData.upgradeTime);
			var newUpgradeTime = new moment.utc();//clock.getNow();
			newUpgradeTime.hours(13);
			newUpgradeTime.minutes(0);
			newUpgradeTime.seconds(0);
			if (newUpgradeTime.isBefore(now)) newUpgradeTime.add(1,'d'); //Every day
			
			
			globals.gameData.upgradeTime = newUpgradeTime.toISOString();
			globals.col.gameData.commit();
			Ti.API.info("UPGRADE CONTROLLER: New Upgrade Time: "+newUpgradeTime.toISOString());
			
			Ti.App.fireEvent("startGame");
		} 
		else if (Ti.Network.online == false)  alert("Sorry but you must be online to finish the upgrade. Kill the app and restart it to finish.");
	}
}


exports.GetFile = function(path, fileName, zipped, successCallback){
	var url = path+fileName;
	if(zipped == true) url = path+fileName+".zip";
	url = url.replace(/ /g,"%20");
	
	var counter = 0;
	var gotFile = false; //Trying to prevent double callback;
	
	var Connect = function (){
		var client = Ti.Network.createHTTPClient({
		     // function called when the response data is available
		     onload : function(e) {
		     	if(gotFile == false){
		     		gotFile = true;
		     		Ti.API.info("UPGRADE CONTROLLER: Success Getting File: "+fileName);
			        if(zipped == true){
			        	UnzipFile(this,fileName, function(data){
			        		successCallback(data);
			        	});
			        	
			        }
			        else successCallback(this.responseText);
		     	}
		     	
		     },
		     // function called when an error occurs, including a timeout
		     onerror : function(e) {
		        //Ti.API.debug(e);
		       // Ti.API.info(url);
		        if (counter < 1){
		        	Connect();
		        }
		        else{
		        	alert("Oh no. Error Connecting!\nWe are probably adding an update.\nWe'll update next time you start.");
		        	Ti.App.fireEvent("startGame");	
		        }
		       	counter++;
		     },
		     timeout : 10000  // in milliseconds
		});
	 	
		client.open("GET", url);
		client.send();
	};
	
	Connect();
};


function UnzipFile(file, fileName, successCallback){
	if (globals.os == "ios"){
		var zip = require('net.imthinker.ti.ssziparchive');
		var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'temp.zip');
	    f.write(file.responseData);
	    
	    var destination = Ti.Filesystem.applicationDataDirectory+'/'+globals.systemData.currenti18n;
	    
	  	zip.addEventListener('extract', function (e) {
	        if (e.success) {
	            console.log('UPGRADE CONTROLLER: ZIP extract is success: '+fileName);
	            var f2 = Ti.Filesystem.getFile(destination,fileName);
				successCallback(f2.read());
	        } else {
	            console.error('UPGRADE CONTROLLER: ZIP extract is failure: '+fileName);
	            console.error(e);
	        }
	    });
	  	
	  	zip.extract({
	        target: Ti.Filesystem.applicationDataDirectory + 'temp.zip',
	        destination: destination,
	        password: '',
	        overwrite: true
	    }); 
	}
	else{
		var zip = require('ti.compression');
		var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'temp.zip');
	    f.write(file.responseData);
	    
	    var destination = Ti.Filesystem.applicationDataDirectory+'/'+globals.systemData.currenti18n;
	  	var result = zip.unzip(destination, Ti.Filesystem.applicationDataDirectory + 'temp.zip', true); 
	  	
	  	if (result == "success") {
            console.log('UPGRADE CONTROLLER: ZIP extract is success: '+fileName);
            var f2 = Ti.Filesystem.getFile(destination, fileName);
			successCallback(f2.read());
        } 
        else {
            console.error('UPGRADE CONTROLLER: ZIP extract is failure: '+fileName);
            console.error(result);
        }
	}
}


function VersionCompare(a, b) {
    if (a === b) return 0;
    var a_components = a.split(".");
    var b_components = b.split(".");
    var len = Math.min(a_components.length, b_components.length);

    // loop while the components are equal
    for (var i = 0; i < len; i++) {
        if (parseInt(a_components[i]) > parseInt(b_components[i])) return 1;
        if (parseInt(a_components[i]) < parseInt(b_components[i])) return -1;
    }

    // If one's a prefix of the other, the longer one is greater.
    if (a_components.length > b_components.length) return 1;
    if (a_components.length < b_components.length) return -1;
    
    // Otherwise they are the same.
    return 0;
}