var AnalyticsController = require("/controller/AnalyticsController");

exports.SavePuzzleToServer = function(phraseID, callback){
	var RTS = require("lib/RTS");
	var now = clock.getNow();
	var phrase = globals.col.phrases.find({phraseID: phraseID})[0];
	Ti.API.info(phrase);
	var authorID = globals.gameData.userName;
	
	
	var SavePuzzleToServerCall = function(data){
		if (Titanium.Network.online == false){
			setTimeout(function(){Ti.App.fireEvent("NoConnection");},1000);
			Ti.App.fireEvent("closeButtonBlocker");
		}
		else{
			//Strings can't be empty on server
			var leadText = data.leadText;
			if(!leadText || leadText == ""){
				if(phrase.type == "written") leadText = "Try this puzzle out!";
				else leadText = " ";
			}
			
			var tryCount = 0;
			
			var SavePuzzleToServerCallCall = function (){
				RTS.putItem("playerPuzzleByAuthor", 
				{
					authorID: data.authorID,
					puzzleID: data.puzzleID, 
					leadText: leadText,
					puzzleText: data.puzzleText, 
					completeText: data.completeText == "" ? " ": data.completeText, 
					puzzleURL: data.puzzleURL, 
					solvedCountOthers: 0, 
					updatedDate: now.toISOString(), 
					createdDate: now.toISOString(), 
					type: data.type, 
					language: data.language,
					source: data.source,
					published: data.published
				}, 
					function(e){
						if(e.text != null){
							var response = JSON.parse(e.text);
							if (response.error == null){
								AnalyticsController.RegisterEvent({category:"design", event_id: "puzzleFactoryController:SavePuzzleToServer"});
								data.callback(response.data);
								tryCount = 0;
							}
							else{
								AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:SavePuzzleToServer:SavePuzzleToServerCall", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});				
			     				Ti.API.info("ERRROR:puzzleFactoryController:SavePuzzleToServer:SavePuzzleToServerCall");
								Ti.App.fireEvent("ErrorSentMessage");
								if(tryCount < 3) SavePuzzleToServerCallCall();
								else CreateRetryDialog(function(){SavePuzzleToServerCall(data);});
								tryCount++;
							}
							Ti.App.fireEvent("closeButtonBlocker");	
						}
						else{
							AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:SavePuzzleToServer:SavePuzzleToServerCall", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});				
			     			Ti.API.info("ERRROR:puzzleFactoryController:SavePuzzleToServer:SavePuzzleToServerCall");
							Ti.App.fireEvent("ErrorSentMessage");
							
							if(tryCount < 3) SavePuzzleToServerCallCall();
							else CreateRetryDialog(function(){SavePuzzleToServerCall(data);});
							tryCount++;
						}
					}
				);
			};
			
			SavePuzzleToServerCallCall();
		}
	};
	
	ShortenURL("http://gopp.s3-website-us-east-1.amazonaws.com/G.html?p="+authorID+"&s="+phraseID,
		function(e){
			phrase.puzzleURL = e.shortURL;
			globals.col.phrases.commit();
			
			SavePuzzleToServerCall({
				puzzleID: phrase.phraseID.toString(),
				authorID: globals.gameData.userName,
				leadText: phrase.leadText,
				puzzleText: phrase.text,
				completeText: phrase.author,
				puzzleURL: e.shortURL,
				type: phrase.type,
				reportedStatus: "clear",
				language: phrase.language,
				source: "online",
				published: phrase.published,
				callback: function(e1){
					callback(e1);
				}
			});
		}
	);
	
};


exports.EditPuzzleOnServer = function(phraseID){
	var RTS = require("lib/RTS");
	var now = clock.getNow();
	var phrase = globals.col.phrases.find({phraseID: phraseID})[0];
	
	if (Titanium.Network.online == false){
		setTimeout(function(){Ti.App.fireEvent("NoConnection");},1000);
	}
	else{
		
	}
};


exports.IncreasePuzzleSolvedCount = function(primaryKey, secondaryKey){
	var RTS = require("lib/RTS");
	globals.rootWin.OpenWindow("networkBar",{bgColor: "#FFD24D", text:"Submitting your score!"});
	
	var tryCount = 0;
	
	var IncreasePuzzleCall = function (){
		Ti.API.info("PUZZLEFACTORYCONTROLLER:IncreasePuzzleSolvedCount");
		RTS.increaseItem("playerPuzzleByAuthor", {primary: primaryKey, secondary: secondaryKey}, "solvedCountOthers", 1,  
			function(e){
				if(e.text != null){
					var result = JSON.parse(e.text);
					if(result.error == null && result != null){
						AnalyticsController.RegisterEvent({category:"design", event_id: "puzzleFactoryController:IncreasePuzzleSolvedCount"});
						Ti.App.fireEvent("CloseNetworkBar");
						tryCount = 0;
					}
					else {
						Ti.API.info("ERROR:puzzleFactoryController:IncreasePuzzleSolvedCount0 "+this.responseText+" "+this.statusText);
						AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:IncreasePuzzleSolvedCount", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});							
						Ti.App.fireEvent("ErrorSentMessage");
						if(tryCount < 3) IncreasePuzzleCall();
						else CreateRetryDialog(function(){exports.IncreasePuzzleSolvedCount(primaryKey, secondaryKey);});
						tryCount++;
					}
				}		
				else {
					Ti.App.fireEvent("ErrorSentMessage");
					Ti.API.info("ERROR:puzzleFactoryController:IncreasePuzzleSolvedCount1 "+this.responseText+" "+this.statusText);
					AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:IncreasePuzzleSolvedCount", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});								
					
					if(tryCount < 3) IncreasePuzzleCall();
					else CreateRetryDialog(function(){exports.IncreasePuzzleSolvedCount(primaryKey, secondaryKey);});
					tryCount++;
				};	
			}
		);	
	};
	
	IncreasePuzzleCall();
};


exports.DeletePuzzle = function(primaryKey, secondaryKey, callback){
	var RTS = require("lib/RTS");
	globals.rootWin.OpenWindow("networkBar",{bgColor: "#FFD24D", text:"Deleting Puzzle!"});
	
	var tryCount = 0;
	
	var DeletePuzzleCall = function(){
		RTS.deleteItem("playerPuzzleByAuthor", {primary: primaryKey, secondary: secondaryKey}, null,  
			function(e){
				if(e.text != null){
					var result = JSON.parse(e.text);
					if(result.error == null && result != null){
						AnalyticsController.RegisterEvent({category:"design", event_id: "puzzleFactoryController:DeletePuzzle"});
						Ti.App.fireEvent("CloseNetworkBar");
						tryCount = 0;
						globals.col.phrases.remove({phraseID: secondaryKey});
						globals.col.phrases.commit();
						
						if(callback) callback();
					}
					else {
						Ti.API.info("ERROR:puzzleFactoryController:IncreasePuzzleSolvedCount0 "+e.responseText+" "+e.statusText);
						AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:DeletePuzzle", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});							
						Ti.App.fireEvent("ErrorSentMessage");
						if(tryCount < 3) DeletePuzzleCall();
						else CreateRetryDialog(function(){exports.IncreasePuzzleSolvedCount(primaryKey, secondaryKey);});
						tryCount++;
					}
				}		
				else{
					Ti.App.fireEvent("ErrorSentMessage");
					Ti.API.info("ERROR:puzzleFactoryController:IncreasePuzzleSolvedCount1 "+e.responseText+" "+e.statusText);
					AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:DeletePuzzle", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});								
					
					if(tryCount < 3) DeletePuzzleCall();
					else CreateRetryDialog(function(){exports.IncreasePuzzleSolvedCount(primaryKey, secondaryKey);});
					tryCount++;
				};	
			}
		);	
	};
	
	DeletePuzzleCall();
};


exports.GetMyPuzzles = function(userID, callback, fromLoading){
	var RTS = require("lib/RTS");
	if(fromLoading != true) globals.rootWin.OpenWindow("networkBar",{bgColor: "#FFD24D", text:"Getting Your Puzzles!"});
	
	//RTS.queryItems("PlayersPuzzles", {secondary: userID},null,null,null,null,false, 
	RTS.queryItems(
		"playerPuzzleByAuthor", //table
		{primary: userID}, //key
		null, //properties
		null, //filter
		null, //startkey
		null, //limit
		null, //searchforward
		function(e){ //
			if(e.text != null){
				var result = JSON.parse(e.text);
				if(result.error == null && result != null){
					AnalyticsController.RegisterEvent({category:"design", event_id: "puzzleFactoryController:GetMyPuzzles"});
					UpdatePuzzlesData(result.data.items);
					if (callback) callback();
					Ti.App.fireEvent("CloseNetworkBar");
					Ti.App.fireEvent("updateSolvedByOthersCount");
				}
				else {
					Ti.API.info(result);
					Ti.App.fireEvent("ErrorSentMessage");
					AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:GetMyPuzzles", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});							
					CreateRetryDialog(function(){exports.GetMyPuzzles(userID, callback, fromLoading);});
				}
			}
			else {
				Ti.App.fireEvent("ErrorSentMessage");
				AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:GetMyPuzzles", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});								
				CreateRetryDialog(function(){exports.GetMyPuzzles(userID, callback, fromLoading);});
			}
		}
	);
};


exports.GetPuzzle = function(data, callback){
	if (Titanium.Network.online == false){
		setTimeout(function(){Ti.App.fireEvent("NoConnection");},1000);
		Ti.App.fireEvent("closeButtonBlocker");
	}
	else{
		var RTS = require("lib/RTS");		
		globals.rootWin.OpenWindow("networkBar",{bgColor: "#FFD24D", text:"Getting Linked Puzzle!"});
		
		Ti.API.info("PUZZLEFACTORYCONTROLLER:GETPUZZLE:Primary-"+data.primary+":Secondary:"+data.secondary);
		
		RTS.queryItems(
			"playerPuzzleByAuthor",
			{primary: data.primary, secondary: data.secondary},
			null, //properties
			{ "operator": "equals", "value": data.secondary}, //filter
			null, //startkey
			null, //limit
			null, //searchforward
			function(e){
				if(e.text != null){
					var result = JSON.parse(e.text);
					if(result.error == null && result != null){
						AnalyticsController.RegisterEvent({category:"design", event_id: "puzzleFactoryController:GetMyPuzzles"});
						
						if(result.data.items.length == 0 || result.data.items[0].puzzleID != data.secondary) {
							Ti.App.fireEvent("closeButtonBlocker");
							alert("Sorry it seems that that puzzle is not available!");
							Ti.App.fireEvent("CloseNetworkBar");
						}	
						else{
							UpdatePuzzlesData(result.data.items);
							
							var GroupsController = require("/controller/GroupsController");
							var group = GroupsController.CreateGroup("singlePhrase", data.secondary);
							globals.rootWin.OpenWindow("gameWin", group);
							Ti.App.fireEvent("CloseNetworkBar");
						}
					}
					else {
						Ti.API.info(result);
						Ti.App.fireEvent("ErrorSentMessage");
						AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:GetMyPuzzles", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});							
						CreateRetryDialog(function(){exports.GetPuzzle(data, callback);});
					}
				}
				else {
					Ti.App.fireEvent("ErrorSentMessage");
					AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:GetMyPuzzles", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});								
					CreateRetryDialog(function(){exports.GetPuzzle(data, callback);});
				}
			}
		);
	}	
};



var UpdatePuzzlesData = function(serverPuzzlesData){
	var PhrasesController = require("controller/PhrasesController");
	for(var i = 0; i < serverPuzzlesData.length; i++){
		if(serverPuzzlesData[i].type == "written"){
			var puzzle = globals.col.phrases.find({phraseID: serverPuzzlesData[i].puzzleID})[0];
			
			if(puzzle != null){
				puzzle.solvedCountOthers = serverPuzzlesData[i].solvedCountOthers;
			}
			else{
				//Save locally from server
				var authorText = serverPuzzlesData[i].authorText;
				var puzzleText = crypt.decrypt(serverPuzzlesData[i].puzzleText);
				var startingText = puzzleText+" ";
				var numberOfWords = startingText.match(/\S+\s/g).length;
				
				var phrase = PhrasesController.SavePhrase({
		   			phraseID: serverPuzzlesData[i].puzzleID,
		   			authorID:  serverPuzzlesData[i].authorID,
		   			leadText: serverPuzzlesData[i].leadText,
		   			text: serverPuzzlesData[i].puzzleText,
		   			author: serverPuzzlesData[i].completeText, //my name,
		   			type: serverPuzzlesData[i].type,
		   			length: puzzleText.length,
		   			numberOfWords: numberOfWords,
		   			difficulty: numberOfWords,
		   			isFunny: false,
		   			complete: false,
		   			isBeingUsed: false,
		   			favorite: false,
		   			receiverIDs: [],
		   			skipped: false,
		   			hide: false,
		   			written: true,
		   			createdDate: serverPuzzlesData[i].createdDate,
		   			updatedDate: serverPuzzlesData[i].updatedDate,
		   			solvedCountOthers: serverPuzzlesData[i].solvedCountOthers,
		   			puzzleURL: serverPuzzlesData[i].puzzleURL,
		   			language: serverPuzzlesData[i].language,
		   			source: "online",
		   			reportStatus: serverPuzzlesData[i].reportStatus
		   		});
			}
		}
	}
	globals.col.phrases.commit();
	
};


//Uses Google to shorten URL
function ShortenURL(longURL, callback){
	if (Titanium.Network.online == false){
		setTimeout(function(){Ti.App.fireEvent("NoConnection");},1000);
		Ti.App.fireEvent("closeButtonBlocker");
	}
	else{
		var tryCount = 0;
		
		var ShortenURLCall  = function(){
			var APIKey = 'AIzaSyBAMeo8ZRIoLVG_eMw4JaRsNpVFzFCX37g';
			var url = "https://www.googleapis.com/urlshortener/v1/url?key="+APIKey;
			var client = Ti.Network.createHTTPClient({
			     onload : function(e) {
			     	var responseObject = JSON.parse(this.responseText);
			     	
			     	Ti.API.info("puzzleFactoryController:SavePuzzleToServer:ShortenURL: Google Success Shortening "+longURL+" to "+responseObject.shortURL);
			     	if(callback) callback({shortURL: responseObject.id, longURL: longURL});
			     	tryCount = 0;
			     },
			     onerror : function(e) {
			     	AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:SavePuzzleToServer:ShortenURL", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});				
			     	Ti.API.info("puzzleFactoryController:SavePuzzleToServer:ShortenURL:"+{text:this.responseText, status: this.statusText});
			    	
			    	if(tryCount < 2) ShortenURLCall();
					else CreateRetryDialog(function(){ShortenURL(longURL, callback);});
					tryCount++;
			     },
			     timeout : 10000 
			});
			
			client.open("POST", url);
			client.setRequestHeader("Content-Type", "application/json");
		
			client.send(JSON.stringify({"longUrl": longURL}));
		};
		
		ShortenURLCall();
	}
}


//Creates short url
function EncodeString(i) {
	var DICTIONARY = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".split('');
    if (i == 0) 
	return DICTIONARY[0];
    
    var result = '';
    var base = DICTIONARY.length;
    
    while (i > 0) {
	result += DICTIONARY[(i % base)];
	i = Math.floor(i / base);
    }
    
    return result.split("").reverse().join("");
}


function CreateFacebookAppLinkURL(authorID, puzzleID, callback){	
	if (Titanium.Network.online == false){
		setTimeout(function(){Ti.App.fireEvent("NoConnection");},1000);
		Ti.App.fireEvent("closeButtonBlocker");	
	}
	else{
		
		//Facebook App Link good if posting on Facebook
		var uniqueID = EncodeString(clock.getNow().valueOf()*10+Math.floor(Math.random()*10));
		var ios = [
			{
		      url: "gophrazy://playerPuzzle/"+authorID+"/"+puzzleID,
		      app_store_id: 903559056,
		      app_name: "GoPhrazy"
		    }
	  	];
		
		var web = {"should_fallback" : false};
		var name = Ti.Network.encodeURIComponent("GoPhrazy_Player_Puzzle_"+puzzleID); 
		var access_token = "593595274047653|g1pgnmckznUDgS9QwxmM-lDqZ7g";

		var url = 'https://graph.facebook.com/593595274047653/app_link_hosts?access_token='+access_token+"&name="+name+"&ios="+JSON.stringify(ios)+"&web="+JSON.stringify(web);
		var client = Ti.Network.createHTTPClient({
		     onload : function(e) {
		     	var responseObject = JSON.parse(this.responseText);
		     	Ti.API.info("puzzleFactoryController:SavePuzzleToServer:CreateDeepLinkURL:"+" Success Creating Deep Link"+"http://fb.me/"+responseObject.id);
		        if(callback) callback("http://fb.me/"+responseObject.id);
		        //exports.GetPuzzleFromDeepLinkURL("http://fb.me/"+responseObject.id);
		     },
		     onerror : function(e) {	
		     	AnalyticsController.RegisterEvent({category:"error", area:"puzzleFactoryController:SavePuzzleToServer:CreateDeepLinkURL", message:JSON.stringify({text:this.responseText, status: this.statusText}), severity:"error"});				
		     	Ti.API.info("puzzleFactoryController:SavePuzzleToServer:CreateDeepLinkURL:"+{text:this.responseText, status: this.statusText});
		    	Ti.App.fireEvent("closeButtonBlocker");	
		     },
		     timeout : 10000 
		});
		
		client.open("POST", url);
		client.send();	
	};		
};


exports.UpdatePuzzleSolvedCountOthersOld = function(){
	var puzzles = globals.col.phrases.find({type: "written"});
	for(var i = 0; i < puzzles.length; i++){
		puzzles[i].solvedCountOthersOld = puzzles[i].solvedCountOthers;
	}
	
	globals.col.phrases.commit();
	
	Ti.App.fireEvent("updateSolvedByOthersCount");
};


exports.GetTotalSolvedCountDifference = function(){
	//Gets total count difference of new solved puzzle
	var newSolvedCount = 0;
	var oldSolvedCount = 0;
	
	var puzzles = globals.col.phrases.find({type: "written", authorID: globals.gameData.userName});
	for(var i = 0; i < puzzles.length; i++){
		newSolvedCount += puzzles[i].solvedCountOthers;
		oldSolvedCount += puzzles[i].solvedCountOthersOld;
	}
	
	return newSolvedCount - oldSolvedCount;
};


function CreateRetryDialog(retryCallback){
	var retryDialog = Ti.UI.createAlertDialog({
	    cancel: 1,
		title: "Retry?",
		message: "There was an error communicating with our server",
	    buttonNames: ['Yes', 'No'],
	    retryCallback: retryCallback
	});
				 	
	retryDialog.addEventListener('click', function(e){
	    if (e.index === e.source.cancel)  Ti.API.info('The cancel button was clicked');
	    if (e.index == 0) retryCallback();
	});	
	
	retryDialog.show();
}
