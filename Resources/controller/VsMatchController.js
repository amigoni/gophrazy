var AnalyticsController = require("/controller/AnalyticsController");

function Create(){
	var controller = {};
	var matchesData = [];
	var myUserName = "mozza1";//globals.gameData.userName;
	var currentMove = 0;
	
	function GetMyMatchesData(){
		var string = 'where={"$or":[{"p1UserName":"'+myUserName+'"},{"p2UserName":"'+myUserName+'"}]}';
		var request = Ti.Network.encodeURIComponent(string);
		var url = "https://api.parse.com/1/classes/Match?"+request;
	
		function InitCallback(e){
			matchesData = JSON.parse(e).results;
			Ti.API.info(matchesData.length);
			Ti.API.info("CurrentMove0: "+currentMove);
			if(currentMove < arrayofMoves.length){
				Ti.API.info(matchesData[0]);
				var move = arrayofMoves[currentMove];
				myUserName = move.myUserName;
				CompleteTurn(matchesData[0],move.score);
			}
		}
		
		CallServer(null, "GET", url, InitCallback);
		//1 For Whom
		//2 Store in MatchesData
	}
	
	function InitNewMatch(opponentUserName, round0P1Score){
		var url = "https://api.parse.com/1/classes/Match";
		//Create the object
		var now = clock.getNow();
		var objectToSend = {
			//objectId: "123456",
			type: "singlePvP", //singlePvP
			state: "open", //open,cancelled,finished
			winner: null, //p1UserName,p2UserName
			winType: null,//normal,resigned
			rounds:[
				{number: 0, p1Score: round0P1Score, p2Score: null, state: "open", wonBy: null, phraseID: 123},
				{number: 1, p1Score: null, p2Score: null, state: "open", wonBy: null, phraseID: 124},
				{number: 2, p1Score: null, p2Score: null, state: "open", wonBy: null, phraseID: 125}
			],
			totalScore:{p1: round0P1Score, p2: null},
			currentRound: 0,
			currentTurn: opponentUserName, //p1userName, p2userName
			p1UserName: myUserName,
			p2UserName: opponentUserName,
			p1Data: {
				userName: myUserName,
				userFacebookID: globals.gameData.userFacebookID,
				userTwitterID: globals.gameData.userTwitterID,
				roundsData: [
					{number: 0, score: round0P1Score},
					{number: 1, score: null},
					{number: 2, score: null}
				]
			},
			p2Data: {
				userName: opponentUserName,
				userFacebookID: null,
				userTwitterID: null,
				roundsData: [
					{number: 0, score: null},
					{number: 1, score: null},
					{number: 2, score: null}
				]
			},
			timeStarted: CreateParseDate(now.toISOString()), //time
			timeLastPlay: CreateParseDate(now.toISOString()), //time
			timeForfeit: CreateParseDate(now.add(2,"days").toISOString()), //time
			timeFinished: null, //time
			timeCancelled: null //time
		};
		
		function InitCallback(e){
			alert(e);
		}
		
		CallServer(JSON.stringify(objectToSend), "POST", url, InitCallback);
		//0 Match Type
		//1 Against Whom
		//2 What Phrases do we play
		
		//Send opponent a notification
	}
	
	function StartTurn(matchData){
		var url = "https://api.parse.com/1/classes/Match/"+matchData.objectId;
		
		var objectToSend = {};
		
		function StartTurnCallback(e){
			alert(e);
		}
		
		CallServer(objectToSend, "PUT", url, StartTurnCallback);
		//1 Which Match
		//2 Which Turn
		//3 When
	}
	
	function CompleteTurn(matchData, score){
		//Need to have the latest data before I submit this. Need to make sure of that.
		var url = "https://api.parse.com/1/classes/Match/"+matchData.objectId;
		
		var now = clock.getNow();
		var objectToSend = matchData;
		
		var round = matchData.rounds[matchData.currentRound];
		//You are P1
		if(matchData.p1UserName == myUserName){
			round.p1Score = score;
			if(round.p2Score != null) {
				round.state = "finished";
				if(round.p1Score < round.p2Score) round.wonBy = "p1";
				else round.wonBy = "p2";
			}	
			
			matchData.totalScore.p1 += score;
			matchData.p1Data.roundsData[matchData.currentRound].score = score;
			matchData.currentTurn = matchData.p2UserName;
		}	
		//You are P2
		else if(matchData.p2UserName == myUserName){
			round.p2Score = score;
			if(round.p1Score != null) {
				round.state = "finished";
				if(round.p2Score < round.p1Score) round.wonBy = "p2";
				else round.wonBy = "p1";
			}
			
			matchData.totalScore.p2 += score;
			matchData.p2Data.roundsData[matchData.currentRound].score = score;
			matchData.currentTurn = matchData.p1UserName;
		}
		else{
			Ti.API.info("VSMATCHCONTROLLER:NOT YOUR DATA");
		}
		
		
		if(round.state == "finished") matchData.currentRound++;
		if(matchData.currentRound >= matchData.rounds.length){
			matchData.state = "finished";
			matchData.winType = "normal";
			if(matchData.totalScore.p1 < matchData.totalScore.p2) matchData.winner = matchData.p1UserName;
			else matchData.winner = matchData.p2UserName;
			matchData.timeFinished = CreateParseDate(now.toISOString());
		}	
		
		matchData.timeLastPlay = CreateParseDate(now.toISOString());
		matchData.timeForfeit = CreateParseDate(now.add(2,"days").toISOString());
		
		Ti.API.info("AFTER");
		Ti.API.info(matchData);
		function CompleteTurnCallback(e){
			Ti.API.info(e);
			currentMove++;
			Ti.API.info("CurrentMove1: "+currentMove);
			GetMyMatchesData();
		}
		
		CallServer(JSON.stringify(objectToSend), "PUT", url, CompleteTurnCallback);
		//Send opponent a notification
	}
	
	function ResignMatch(matchData){
		var url = "https://api.parse.com/1/classes/Match/"+matchData.objectId;
		var objectToSend = {};
		
		function ResignMatchCallback(e){
			alert(e);
		}
		
		CallServer(objectToSend, "PUT", url, ResignMatchCallback);
		//1 Which Match
		//2 Callback
	}
	
	controller.GetMyMatchesData = GetMyMatchesData;
	controller.InitNewMatch = InitNewMatch;
	controller.StartTurn = StartTurn;
	controller.CompleteTurn = CompleteTurn;
	controller.ResignMatch = ResignMatch;
	controller.matchesData = matchesData;
	
	//InitNewMatch("mozza2", 5001);
	
	/*
	var arrayofMoves =[
		{myUserName: "mozza2", score: 5002},
		{myUserName: "mozza1", score: 5001},
		{myUserName: "mozza2", score: 5002},
		{myUserName: "mozza1", score: 5001},
		{myUserName: "mozza2", score: 5002}
	];
	
	//setTimeout(GetMyMatchesData, 2000);
	*/
	return controller;
}

module.exports = Create;


function CallServer(objectToSend, httpMethod, url, callback){
	//Ti.API.info(receiptObjectToSend);
	Ti.API.info("VSMATCHCONTROLLER:Calling Server");
	
	var applicationID = "BJ3oUgDugYnLQG7ldb3i8KJUNQvm5g1m1KKHrOx1";
	var restAPIKey = "7d9VO6QXP6kL7F4t7sUAJyIXlAQbGFNw8rgJLblh";
	
	var client = Ti.Network.createHTTPClient({	
	     onload : function(e) {
	     	Ti.API.info("VSMATCHCONTROLLER:CALLINGPARSE: Call Status Code: "+this.status+" "+this.statusText);
	     	if(callback) callback(this.responseText);
	     },
	     onerror : function(e) {
	     	Ti.API.info("STORE VSMATCHCONTROLLER:Error:Call Status Code: "+this.status+" "+this.statusText);
	     	Ti.API.info(this.responseText);
	     	//Need to look at error if 155 need to send a notification to myself. 
	    	if(this.status == 155 || this.status == 140){
	    		//Send me notification, reached the limit.
	    		//AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:verifyReceipt:limitReached-"+this.status+" "+this.errorMessage, message:"Error in Verification Server", severity:"critical"});				
	    	}
	    	else AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:verifyReceipt:"+this.status+" "+this.errorMessage, message:"Error in Verification Server", severity:"error"});				
	    	if(callback) callback(this.responseText);
	     },
	     onreadystatechange: function(e){
	     	//Ti.API.info("Ready State "+this.readyState+" Status Code: "+this.status);
	     },
	     timeout : 10000
	});
	
	client.open(httpMethod, url);
	client.setRequestHeader("X-Parse-Application-Id", applicationID ); 
	client.setRequestHeader("X-Parse-REST-API-Key", restAPIKey); 
	client.setRequestHeader("Content-Type", "application/json"); 
	client.send(objectToSend);
}	

function CreateParseDate(isoDate){
	return {
	  "__type": "Date",
	  "iso": isoDate
	};
}
