exports.InitGroups = function(){
	var LocalData = require("model/LocalData");
	var gameData = globals.gameData;
	var phrases = globals.col.phrases.getAll();
	
	globals.col.groups.save({	
		type: "skipbox",
		title: "skipbox",
		subtitle: "skipbox",
		name: "skipbox",
		phrasesCount: 0,
		phrasesLimit: gameData.skipboxLimit,
		phrasesIDs: [],
		locked: false
	});
	
	globals.col.groups.save({	
		type: "mailbox",
		title: "Mailbox",
		subtitle: "Mailbox",
		name: "Mailbox",
		phrasesCount: 0,
		phrasesIDs: [],
		locked: false
	});
	
	
	var groups = JSON.parse(Common.GetJsonText("model/"+globals.systemData.currenti18n+"/GroupsDB.txt"));
	exports.UpgradeCollections(groups);
	
	globals.col.groups.commit();
};


exports.CreateGroup = function(type, currentPhraseID, groupID){
	var PhrasesController = require("/controller/PhrasesController");
	var phrasesDifficultyCurve = globals.gameData.phrasesDifficultyCurve;	
	var gameData = globals.gameData;
	var group = {};
	
	if (type == "general"){
		group = {
			type: "general",
			name: "General",
			title: "General",
			subtitle: "",
			//numberOfPhrases: globals.col.phrases.count({type:{$nin: ["fortuneCookie", "riddle", "tutorial"]}}),
			numberOfPhrases:  globals.col.phrases.count({includeInGeneral: true}),
			locked: false,
			complete: false
		};
		
		var GetNextPhrase = function (phraseID){
			var ftuPhrases;
			if(globals.gameData.versions.ABTestingGroup == "A") ftuPhrases = globals.gameData.phrasesDifficultyCurve.ftuPhrasesA;
			else ftuPhrases = globals.gameData.phrasesDifficultyCurve.ftuPhrasesB; 
			
			//For the first set of phrases get from the provided list
			if (gameData.phrasesCompleteCountNoCookies < ftuPhrases.length && globals.tutorialOn != true) {
				phraseID = ftuPhrases[gameData.phrasesCompleteCountNoCookies];
				var phrase = globals.col.phrases.find({phraseID: phraseID})[0];
				if(phrase.skipped == true || phrase.complete == true){
					var found = false;
					var i = gameData.phrasesCompleteCountNoCookies;
					while (found == false){
						if (i >= ftuPhrases.length){
							group.currentPhrase = GetProperDifficultyPhrase();
							found = true;
						}
						else{
							var pID = ftuPhrases[i];
							var ph = globals.col.phrases.find({phraseID: pID})[0];
							if(ph.skipped == false && ph.complete == false){
								group.currentPhrase = ph;
								found = true;
							}
						}

						i++;
					}
				}
				else group.currentPhrase = phrase;
				
			}	
			//After get by the difficulty curve unless you have specified a specific ID like for tutorials.
			else{
				if (phraseID == null) group.currentPhrase = GetProperDifficultyPhrase();
				else group.currentPhrase = globals.col.phrases.find({phraseID: phraseID})[0];
			}
			
			globals.gameData.currentPhraseID = group.currentPhrase.phraseID;
			globals.col.gameData.commit();
			
			return group.currentPhrase;
		};
		
		var SkipPhrase = function (){
			PhrasesController.SkipPhrase(group.currentPhrase.phraseID, group);
			if(globals.tutorialOn != true) return GetNextPhrase();
		};
		
		var CompletePhrase = function (){
			PhrasesController.CompletePhrase(group.currentPhrase, group);
			GetGroupData();
		};
		
		var GetGroupData = function(){
			group.phrases = globals.col.phrases.find({includeInGeneral: true});
			
			group.incompletePhrases = globals.col.phrases.find({includeInGeneral: true, complete: false, skipped: false}); 
			if (group.incompletePhrases.length == 0) group.incompletePhrases = globals.col.phrases.find({includeInGeneral: true, complete: false, skipped: true});
			
			//This is for when you completed all the phrases.
			//Need to find the lowest skip count and dish out those phrases first
			//This way you keep going around the loop
			var lowestCompleteCount = 1;
			if (group.incompletePhrases.length == 0) 
			{
				lowestCompleteCount = globals.col.phrases.find({includeInGeneral: true}, {$sort: {completeCount: 1}})[0].completeCount;
				while (group.incompletePhrases.length == 0) 
				{
					Ti.API.info("GetGroupData General: lowestCompleteCount - "+lowestCompleteCount);
					group.incompletePhrases = globals.col.phrases.find({includeInGeneral: true, completeCount: lowestCompleteCount, skipped: false});
					if (group.incompletePhrases.length == 0) group.incompletePhrases = globals.col.phrases.find({includeInGeneral: true, completeCount: lowestCompleteCount});
					lowestCompleteCount++;
					Ti.API.info("Incomplete Phrases Length: "+group.incompletePhrases.length);
				}
			}
			
			group.currentPhrase = globals.col.phrases.find({phraseID: gameData.currentPhraseID})[0];
			
			if(gameData.currentPhraseID == "" || group.currentPhrase == null) GetNextPhrase();
			//When you come back from the tutorial the phrase is still set to the tutorial one. This makes sure you get a fresh one.
			if (group.currentPhrase.type == "tutorial" && globals.tutorialOn != true) GetNextPhrase();
			
			group.phrasesCount = group.phrases.length;
			group.phrasesCompleteCount = globals.gameData.phrasesCompleteCount;
		};
		
		group.SkipPhrase = SkipPhrase;
		group.GetNextPhrase = GetNextPhrase;
		group.CompletePhrase = CompletePhrase;
		group.GetGroupData = GetGroupData;
		
		GetGroupData();
	}
	else if (type == "collection"){
		var lastPhraseID;
		
		var GetNextPhrase = function (phraseID){
			var RandomPlus = require("lib/RandomPlus");
			
			if(group.incompletePhrases.length > 0){
				//For main get the proper difficulty.
				if (phraseID == null) {
					Ti.API.info("Next");
					var next = GetNext();
					group.currentPhrase = group.incompletePhrases[next];
					
					if(group.currentPhrase.phraseID == lastPhraseID && group.incompletePhrases.length > 1){
						for(var i = 0; i < group.incompletePhrases.length; i++){
							if(group.currentPhrase.phraseID == lastPhraseID){
								next = i+1;
								if(next >= group.incompletePhrases.length) next = 0;
								group.currentPhrase = group.incompletePhrases[next];
								break;
							}
						}
					}
					
					function GetNext(){
						var next;
						if (group.sort == "random") next = RandomPlus.RandomRange(0,group.incompletePhrases.length);
						else if (group.sort == null || group.sort == "asc") next = 0;
						else if (group.sort == "desc") next = group.incompletePhrases.length-1;
						return next;
					}
				}
				else group.currentPhrase = globals.col.phrases.find({phraseID:phraseID})[0];
			}
			
			if(globals.tutorialOn != true){
				if (group.currentPhrase != null) globals.col.groups.update({$id:group.$id},{$set:{currentPhraseID: group.currentPhrase.phraseID}},{},true);
				else globals.col.groups.update({$id:group.$id},{$set:{currentPhraseID: null}},{},true);
				globals.col.groups.commit();
			}	
			
			lastPhraseID = group.currentPhrase.phraseID;
			
			return group.currentPhrase;
		};
		
		var SkipPhrase = function (){
			PhrasesController.SkipPhrase(group.currentPhrase.phraseID, group);
			GetGroupData();
			if(!group.currentPhrase) GetNextPhrase();
			return group.currentPhrase;
		};
		
		var CompletePhrase = function (){
			PhrasesController.CompletePhrase(group.currentPhrase, group);
			GetGroupData();
		};
		
		var GetGroupData = function(){
			group = JSON.parse(JSON.stringify(globals.col.groups.find({$id:groupID})[0])); //Clone
			group.phrases = globals.col.phrases.find({groupID: group.groupID});
			group.incompletePhrases = globals.col.phrases.find({groupID: group.groupID, complete: false, skipped: false}, {$sort: {phraseID: 1}}); 
			if (group.incompletePhrases.length == 0) group.incompletePhrases = globals.col.phrases.find({groupID:group.groupID, complete:false}, {$sort: {phraseID: 1}});
			if (group.incompletePhrases.length == 0) group.incompletePhrases = globals.col.phrases.find({groupID:group.groupID, skipped: false}, {$sort: {phraseID: 1}});
			if (group.incompletePhrases.length == 0) group.incompletePhrases = globals.col.phrases.find({groupID:group.groupID}, {$sort: {phraseID: 1}});
			if(!group.currentPhrase) GetNextPhrase();//GetNextPhrase(897);
			//When you come back from the tutorial the phrase is still set to the tutorial one. This makes sure you get a fresh one.
			if (group.currentPhrase.type == "tutorial" && globals.tutorialOn != true) gameData.currentPhraseID = GetNextPhrase();
		
			group.SkipPhrase = SkipPhrase;
			group.GetNextPhrase = GetNextPhrase;
			group.CompletePhrase = CompletePhrase;
			group.GetGroupData = GetGroupData;
			group.FetchGroup = FetchGroup;
		};
		
		var FetchGroup = function(){
			return group;
		};
		
		GetGroupData();
	}
	else if (type == "skipbox"){
		var GetNextPhrase = function (){
			currentPosition++;
			if (currentPosition >= group.phrases.length) currentPosition = 0;
			group.currentPhrase = group.phrases[currentPosition];
			
			return group.currentPhrase;
		};
		
		var SkipPhrase = function (){
			GetNextPhrase();
		};
		
		var CompletePhrase = function (){
			PhrasesController.CompletePhrase(group.currentPhrase,group);
			GetGroupData();
		};
		
		var GetGroupData = function(){
			group = exports.GetskipboxGroup();
			group.SkipPhrase = SkipPhrase;
			group.GetNextPhrase = GetNextPhrase;
			group.CompletePhrase = CompletePhrase;
			
			var currentPosition = 0;
			for (var i = 0; i < group.phrases.length; i++){
				if (group.phrases[i].phraseID == currentPhraseID) {
					group.currentPhrase = group.phrases[i];
					currentPosition = i;
					break;
				}	
			}
		};
		
		GetGroupData();
	}
	else if (type == "mailbox"){
		var GetNextPhrase = function (){
			currentPosition++;
			if (currentPosition >= group.phrases.length) currentPosition = 0;
			group.currentPhrase = group.phrases[currentPosition];
			return group.currentPhrase;
		};
		
		var SkipPhrase = function (){
			GetNextPhrase();
		};
		
		var CompletePhrase = function (){
			PhrasesController.CompletePhrase(group.currentPhrase,group);
			GetGroupData();
		};
		
		var GetGroupData = function(){
			group = exports.GetMailboxGroup();
			group.SkipPhrase = SkipPhrase;
			group.GetNextPhrase = GetNextPhrase;
			group.CompletePhrase = CompletePhrase;
			
			for (var i = 0; i < group.phrases.length; i++){
				if (group.phrases[i].phraseID == currentPhraseID) {
					group.currentPhrase = group.phrases[i];
					break;
				}	
			}
		};
		
		GetGroupData();
	}
	else if (type == "dailyChallenge"){
		group = {
			type: "dailyChallenge",
			name: "Fortune Cookie",
			title: "Fortune Cookie",
			subtitle: "",
			numberOfPhrases: 1,
			locked: false,
			complete: false
		};	
		
		if (globals.tutorialOn != true){
			if (globals.gameData.dailyChallengeCurrentPhraseID == null){
				var DailyChallengeController = require('controller/DailyChallengeController');
				DailyChallengeController.SetNextDailyChallengePhrase();
			}
			group.currentPhrase =  globals.col.phrases.find({phraseID:globals.gameData.dailyChallengeCurrentPhraseID})[0];
		}
		else group.currentPhrase =  globals.col.phrases.find({phraseID:globals.currentTutorial.phraseID})[0];
		
		
		var CompletePhrase = function (){
			PhrasesController.CompletePhrase(group.currentPhrase,group);
		};
	
		var GetCurrentPhrase = function (){
			return group.currentPhrase;
		};
	
		group.CompletePhrase = CompletePhrase;
		group.GetNextPhrase = GetCurrentPhrase;
	}
	else if (type == "singlePhrase"){
		group = {
			type: "singlePhrase",
			name: "Single Phrase",
			title: "",
			subtitle: "",
			numberOfPhrases: 1,
			locked: false,
			complete: false
		};	
		
		group.currentPhrase =  globals.col.phrases.find({phraseID: currentPhraseID})[0];
			
		var CompletePhrase = function (){
			PhrasesController.CompletePhrase(group.currentPhrase, group);
		};
	
		var GetCurrentPhrase = function (){
			return group.currentPhrase;
		};
	
		group.CompletePhrase = CompletePhrase;
		group.GetNextPhrase = GetCurrentPhrase;
	}
	else if (type == "firstStart"){
		group = {
			type: "firstStart",
			name: "Intro",
			title: "Intro",
			subtitle: "",
			numberOfPhrases: 1,
			locked: false,
			complete: false
		};	
		
		group.currentPhrase =  globals.col.phrases.find({phraseID:globals.currentTutorial.phraseID})[0];
			
		var CompletePhrase = function (){
			Ti.App.fireEvent("tutorialStep");
			return group;
		};
	
		var GetCurrentPhrase = function (){
			return group.currentPhrase;
		};
	
		group.CompletePhrase = CompletePhrase;
		group.GetNextPhrase = GetCurrentPhrase;
	}
	
	
	function GetProperDifficultyPhrase(){
		var RandomPlus = require("lib/RandomPlus");
		
		var skipped = false;
		var difficulty = 0;
		var phrase;
		var difficulties = phrasesDifficultyCurve.phraseDifficultyRanges;
		var completeCount = globals.col.phrases.find({includeInGeneral: true}, {$sort: {completeCount: 1}})[0].completeCount;
		var getUsedPhrases = completeCount == 0 ? false : true;
		var phrases = [];
		var contingency = "up";
		var contingencySwitched = false;
		
		if(globals.gameData.versions.ABTestingGroup == "A") difficulty = phrasesDifficultyCurve.regularA[gameData.currentPhraseDifficultyPosition].phraseDifficulty;
		else difficulty = phrasesDifficultyCurve.regularB[gameData.currentPhraseDifficultyPosition].phraseDifficulty;
		
		
		var originalDifficulty = difficulty;
		if (difficulty == 4) contingency = "down";
		
		while (phrase == null){
			var low = difficulties[difficulty].low;
			var high = difficulties[difficulty].high;
			var phrases = globals.col.phrases.find({complete: getUsedPhrases, completeCount: completeCount, difficulty:{$gte:low,$lte:high}, includeInGeneral: true, skipped: skipped});	
			phrase = phrases.length > 0 ? phrases[RandomPlus.RandomRange(0, phrases.length)]: null; 
		
			//Contigency
			if(phrase == null){
				//1 Get Different Difficulty
				if (contingency == "up") {
					difficulty++; 
					Ti.API.info("GetProperDifficultyPhrase: Going Up in Difficulty");
				}
				else {
					difficulty--;  
					Ti.API.info("GetProperDifficultyPhrase: Going Down in Difficulty");
				} 
				
				if (difficulty < 0 || difficulty > 4){
					Ti.API.info("Resetting Difficulty");
					difficulty = originalDifficulty;
					//Look the way down first for contingencies in the middle
					if (contingencySwitched == false){
						Ti.API.info("Switching Contingency");
						if(difficulty != 4 && difficulty != 0){
							contingencySwitched = true;
							contingency = "down";
							difficulty--;
						}
					}
					//Go for Skipped Phrases
					else{
						if (skipped == false) {
							Ti.API.info("GetProperDifficultyPhrase: Getting Skipped Phrases");
							skipped = true;
							contingencySwitched = false;
							contingency = "up";
						}
						//Lastly get the used phrases, with the lowest complete count
						else{
							getUsedPhrases = true;
							contingencySwitched = false;
							contingency = "up";
							skipped = false;
							completeCount++;
							
							Ti.API.info("GetProperDifficultyPhrase: Getting Completed Phrases CompleteCount: "+completeCount);
						}
					}
				}
			}
		}
		
	
		return phrase;
	}
	
	
	return group;
};


exports.GetskipboxGroup = function(){
	var copy = JSON.parse(JSON.stringify(globals.col.groups.find({type:"skipbox"})[0]));
	copy.phrases = [];
	for (var i = 0; i < copy.phrasesIDs.length;i++){
		copy.phrases.push(globals.col.phrases.find({phraseID:copy.phrasesIDs[i]})[0]);
	}
	
	return copy;
};


exports.GetMailboxGroup = function(){
	var copy = JSON.parse(JSON.stringify(globals.col.groups.find({type:"mailbox"})[0]));
	copy.phrases = [];
	for (var i = 0; i < copy.phrasesIDs.length; i++){
		copy.phrases.push(globals.col.phrases.find({phraseID:copy.phrasesIDs[i]})[0]);
	}
	//Ti.API.info(copy);
	return copy; 
};


/////Collections
exports.InitCollection = function(data){
	globals.col.groups.save({
		type: "collection",
		groupID: data.groupID,
		kind: data.kind,
		name: data.name,
		title: data.name,
		description: data.description,
		phrasesCount: data.phrasesCount,
		phrasesCompleteCount: 0,
		phrasesIDs: data.phrasesIDs,
		avgDifficulty: data.avgDifficulty,
		sort: data.sort, // asc, desc,random
		locked: data.locked,
		unlockText: data.unlockText,
		cost: data.cost,
		bought: data.bought,
		featured: data.featured,
		isNew: data.isNew,
		complete: false,
		createdDate: new moment(data.createdDate).valueOf(),
		releaseDate: new moment(data.releaseDate).valueOf(),
		issueID: data.issueID
	});
};


exports.GetCollectionBookData = function(){
	var now = clock.getNow().valueOf();
	var openBoughtCollections = globals.col.groups.find({type: "collection", complete: false, bought: true, featured: false},{$sort:{releaseDate:-1}});
	var openNotBoughtCollections =  globals.col.groups.find({type: "collection", complete: false, bought: false, featured: false},{$sort:{releaseDate:-1}});
	var openCollections = openBoughtCollections.concat(openNotBoughtCollections);
	
	var returnObject = {
		lastCollection: globals.col.groups.find({groupID: globals.gameData.lastGroupID})[0],
		featuredCollections: globals.col.groups.find({type: "collection", featured: true, complete: false},{$sort:{groupID:-1}}),
		openCollections: openCollections,
		completeCollections: globals.col.groups.find({type: "collection", complete: true},{$sort:{groupID:-1}})
	};
	
	return returnObject;
};


exports.GetPhrasesForCollection = function(groupID){
	return globals.col.phrases.find({groupID:groupID, complete: true });
};


exports.GetPuzzlePostData = function(){
	var now = clock.getNow();
	return globals.col.groups.find({type:"collection", kind:"issue", releaseDate: {$lte: now.valueOf()}}, {$sort:{releaseDate: -1}});
};


exports.UpgradeCollections = function(newCollections){
	var newGroupsCount = 0;
	//Remove featured from old ones
	var collections = globals.col.groups.find({featured: true});
	for (var i = 0; i < collections.length; i++){
		collections[i].featured = false;
	}
	
	for (var i = 0; i < newCollections.length; i++){
		var collectionData  = newCollections[i];
		if(globals.col.groups.find({groupID: collectionData.groupID})[0] == null){
			//var count =  globals.col.phrases.count({groupID:collectionData.groupID});
			//collectionData.phrasesCount = count;
			collectionData.isNew = true;
			exports.InitCollection(collectionData);
			newGroupsCount++;
		}
	}
	globals.col.groups.commit();

	Ti.API.info("UPGRADE: Added Groups Count: "+newGroupsCount);
};


exports.UpgradeCollectionPhrases = function(phrases,groupID){
	var PhrasesController = require("controller/PhrasesController");
	for (var i = 0; i < phrases.length; i++){
		if (globals.col.phrases.find({phraseID: phrases[i].phraseID}).length == 0) PhrasesController.SavePhrase(phrases[i]);
	}
	globals.col.phrases.commit();
	
	globals.col.groups.update({groupID: groupID},{$set:{phrasesCount: phrases.length}},{},true);
	globals.col.groups.commit();
};


exports.MarkCollectionsAsViewed = function(){
	var collections = globals.col.groups.find({type: "collection",isNew: true});
	for (var i = 0; i < collections.length; i++){
		collections[i].isNew = false;
	}
	globals.col.groups.commit();
};


exports.MarkIssuesAsViewed = function(){
	var threeDayPast = clock.getNow().subtract("days",3).valueOf();
	var now = clock.getNow();
	var groups = globals.col.groups.find({kind: "issue",isNew: true, releaseDate: {$lte: threeDayPast}});
	for (var i = 0; i < groups.length; i++){
		groups[i].isNew = false;
	}
	
	globals.col.groups.commit();
	
	var newOnes = globals.col.groups.find({kind: "issue", isNew: true, releaseDate: {$lte: now.valueOf()}});
	
	if(newOnes.length > 0) Ti.App.fireEvent("UpdatePuzzlePostIcon",{newOn: true});
	else Ti.App.fireEvent("UpdatePuzzlePostIcon",{newOn: false});
};


exports.QueryForNewIssues = function(){
	var now = clock.getNow();
	var returnObject = {};
	var newOnes = globals.col.groups.find({kind: "issue", isNew: true, releaseDate: {$lte: now.valueOf()}});
	
	if(newOnes.length > 0) returnObject.newOn = true;
	else returnObject.newOn = false;
	
	return returnObject;
};


exports.UpdateGroups = function(updateData){
	for(var i = 0; i < updateData.changes.length; i++){
		var groupUpdateData = updateData.changes[i];
		var group = globals.col.groups.find({groupID: groupUpdateData.groupID})[0];
		if(group != null){
			FillObject(groupUpdateData, group);
			UpdateObject(groupUpdateData, group);
		}
		
		Ti.API.info("UPDATED GROUP: "+group.groupID);
	}
	
	globals.col.groups.commit();
	
	function FillObject(from, to) {
	    for (var key in from) {
	        if (from.hasOwnProperty(key)) {
	            if (Object.prototype.toString.call(from[key]) === '[object Object]') {
	                if (!to.hasOwnProperty(key)) {
	                    to[key] = {};
	                }
	                FillObject(from[key], to[key]);
	            }
	            else if (!to.hasOwnProperty(key)) {
	                to[key] = from[key];
	            }
	        }
	    }
	};
	
	function UpdateObject(newData, oldData){
		for (var key in newData) {
	        if (newData.hasOwnProperty(key)) {
	            if (Object.prototype.toString.call(newData[key]) === '[object Object]') {
	                UpdateObject(newData[key], oldData[key]);
	            }
	            else if (oldData.hasOwnProperty(key)) {
	                oldData[key] = newData[key];
	            }
	        }
	    }
	};
};
//////


/////Events
exports.InitEvent = function(data){
	globals.col.groups.save({
		type: "event",
		eventID: data.eventID,
		name: data.name,
		startDate: new moment(data.startDate).valueOf(),
		endDate: new moment(data.endDate).valueOf(),
		title: data.name,
		description: data.description,
		phrasesCount: data.phrasesCount,
		phrasesCompleteCount: 0,
		isNew: true,
		complete: false,
		expired: false
	});
};


exports.CompleteEvent = function(eventID){
	globals.col.groups.update({eventID:eventID},{$set:{complete:true}});
	globals.col.groups.commit();
};


exports.CheckEventsStatus = function(){
	// This is called every second.
	var now = clock.getNow().valueOf();
	var toExpireEvents = globals.col.groups.find({
			expired: false, 
			endDate:{$lte: now}
		});
	
	if (toExpireEvents.length > 0){
		for (var i = 0; i < toExpireEvents.length; i++){
			globals.col.groups.update({eventID: toExpireEvents[i].eventID},{$set:{expired:true}},{},true);
		}	
		
		globals.col.groups.commit();
	}	
};

exports.GetEventsData = function(){
	var returnObject = {};
	var now = clock.getNow().valueOf();
	
	returnObject.currentEvents = globals.col.groups.find({type:"event", endDate: {$gt:now}});
	returnObject.expiredEvents = globals.col.groups.find({type:"event", endDate: {$lte:now}});
	
	return returnObject;
};

exports.GetLastEventData = function(){
	return globals.col.groups.find({type:"event"}, {$sort:{endDate: -1}})[0];
};

exports.UpgradeEvents = function(groupsData,versionNumber){
	for (var i = 0; i < groupsData.length; i++){
		var eventData = groupsData[i];
		var event1 = globals.col.groups.find({eventID:eventData.eventID})[0];
		if (event1 == null){
			eventData.isNew = true;
			exports.InitEvent(eventData);
		}	
	}
	globals.col.groups.commit();
	globals.gameData.versions.eventsVersion = versionNumber;
	globals.col.gameData.commit();
};

exports.UpgradeEventPhrases = function(phrases,eventID){
	var PhrasesController = require("controller/PhrasesController");
	for (var i = 0; i < phrases.length; i++){
		if (globals.col.phrases.find({phraseID: phrases[i].phraseID}).length == 0) PhrasesController.SavePhrase(phrases[i]);
	}
	globals.col.phrases.commit();
	
	globals.col.groups.update({eventID: eventID},{$set:{phrasesCount: phrases.length}},{},true);
	globals.col.groups.commit();
};
/////
