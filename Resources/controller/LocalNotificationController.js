var moment = require("lib/moment.min");//Here for BG Service

exports.SaveLocalNotification = function (data){
	globals.col.notifications.save({
		type: data.type,
		alertBody: data.alertBody,
		date: data.date,
		repeat: data.repeat,
		permanent: data.permanent,
		sound: data.sound,
	});
	
	globals.col.notifications.commit();
};


exports.SetAllNotifications = function(){
	var notifications = globals.col.notifications.getAll();
	Ti.API.info("Local Notifications "+notifications.length);
	
	//1. Fortune Cookies
	if (globals.gameData.abilities.fortuneCookies.enabled == true)
	{
		// Fortune Cookie Ready
		if(globals.gameData.notifications.fortuneCookieAvailable.enabled == true){
			var hour = globals.gameData.notifications.fortuneCookieAvailable.time.hour;
			var minute =  globals.gameData.notifications.fortuneCookieAvailable.time.minute;
			
			var ready = {
				type: "fortuneCookieReady",
				requestCode: 1,
				permanent: true,
				date:  new moment().hour(hour).minute(minute).toISOString(),
				repeat: "daily",
				badge: 1,
				sound: "default",
				alertBody: "Your Fortune Cookie and free puzzles are ready!!!!"
			};
			notifications.push(ready);
		}
		
		if(globals.gameData.notifications.fortuneCookieExpires.enabled == true){
			if (globals.gameData.dailyChallengeComplete == false){
				var hour = globals.gameData.notifications.fortuneCookieExpires.time.hour;
				var minute =  globals.gameData.notifications.fortuneCookieExpires.time.minute;
				
				var expiration = {
					type: "fortuneCookieWarning",
					requestCode: 2,
					permanent: true,
					date: new moment().hour(hour).minute(minute).toISOString(),
					repeat: "daily",
					badge: 1,
					sound: "default",
					alertBody: "Your Fortune Cookie will expire at midnight!"
				};
				
				notifications.push(expiration);
			}
		}	
	}
	
	/*
	//2. Skip Warning
	var skip = {
		type: "skipWarning",
		permanent: true,
		date: new moment().add("days", 4).hour(18).minute(7).toISOString(),
		//repeat: "daily",
		badge: 1,
		sound: "default",
		alertBody: "Stuck on a puzzle? Just skip it!"
	};
	
	notifications.push(skip);
	
	//2. Ask Friend Warning
	var friend = {
		type: "skipWarning",
		permanent: true,
		date: new moment().add("days", 2).hour(18).minute(7).toISOString(),
		//date: new moment().add("seconds", 10).toISOString(),
		//repeat: "daily",
		badge: 1,
		sound: "default",
		alertBody: "Friends don't let friends get stuck on puzzles. Stuck? Ask someone for help."
	};
	
	notifications.push(friend);
	*/
	
	//3. Events
	/*
	if (globals.gameData.abilities.events.enabled == true){
		//Fina all open events. Schedule notification for 1 hour before the end date.
		var now = clock.getNow();
		var events = globals.col.groups.find({type:"event", complete: false, endDate:{$gt: now.valueOf()}});
		
		for (var i = 0; i < events.length; i++){
			var eventDate = new moment(events[i].endDate);
			if (now.valueOf() < eventDate.subtract("hour",1).valueOf()){
				var event1 = {
					type: "eventExpiration1HourWarning",
					permanent: false,
					date: new moment(events[i].endDate).subtract("hour",1).toISOString(),
					//repeat: "daily",
					badge: 1,
					sound: "default",
					alertBody: events[i].name+" event is about to expire. You have 1 hour."
				};
				
				notifications.push(event1);
			}
			
			if (now.valueOf() < eventDate.subtract("day",1).valueOf()){
				var event1 = {
					type: "eventExpiration1DayWarning",
					permanent: false,
					date: new moment(events[i].endDate).subtract("day",1).toISOString(),
					//repeat: "daily",
					badge: 1,
					sound: "default",
					alertBody: events[i].name+" event is about to expire. You have 1 hour."
				};
				
				notifications.push(event1);
			}
		}
	}
	*/
	
	if(globals.os == "ios"){
		var notify = require('bencoding.localnotify');
		notify.cancelAllLocalNotifications();
		
		
		for (var i = 0; i < notifications.length; i++){
			var notification = notifications[i];
			
			notify.scheduleLocalNotification({
			    alertBody: notification.alertBody,
			    alertAction:"",
			   	//userInfo:{"notificationTime": date},
			    sound: notification.sound,
			    repeat: notification.repeat,
			    badge: notification.badge,
			    date: new Date(new moment(notification.date).valueOf())
			});
			
			Ti.API.info("Notification set for:"+(new moment(notification.date).toISOString())+" "+notification.alertBody);
		}
	}
	else{
		//Import our module into our Titanium App
		var requestCode = 41;
		var alarmModule = require('bencoding.alarmmanager');
		var alarmManager = alarmModule.createAlarmManager();
		alarmManager.cancelAlarmNotification(1);
		alarmManager.cancelAlarmNotification(2);
		
		for (var i = 0; i < notifications.length; i++){
			var notification = notifications[i];
			var now = new Date(new moment(notification.date).valueOf());
			//var now = new Date();
			alarmManager.addAlarmNotification({	
				requestCode: notification.requestCode, //Request ID used to identify a specific alarm. Provide the same requestCode twice to update 	
				year: now.getFullYear(),
				month: now.getMonth(),
				day: now.getDate(),
				hour: now.getHours(),
				minute: now.getMinutes(), //Set the number of minutes until the alarm should go off
				second: now.getSeconds()+10,
				contentTitle: notification.alertBody, //Set the title of the Notification that will appear
				contentText: notification.alertBody, //Set the body of the notification that will apear
				icon: Ti.App.Android.R.drawable.appicon,
				playSound: true,
				vibrate: true,
				showLights: true,
				repeat: notification.repeat
			});	
		}	
	}
};


exports.InitNotifications = function (){
	var gameData = globals.gameData;
	exports.SetAllNotifications();
	Ti.API.info("Registered First Notifications");
};
