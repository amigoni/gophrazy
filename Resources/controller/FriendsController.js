exports.UpdateFriendsScore = function(data){
	var PushNotificationsController = require("controller/PushNotificationsController");
	var AchievementsController = require("/controller/AchievementsController");
	
	var startTime = new Date().getTime();
	var gameData = globals.gameData;
	var oldMe = JSON.parse(JSON.stringify(globals.col.friends.find({isMe:true})[0]));
	var oldFriendsData = JSON.parse(JSON.stringify(globals.col.friends.find({isMe:false, installed: true})));
	
	for (var i = 0; i < data.length; i++){
	    var personData = data[i];
		if (personData.application.id == 593595274047653){
			var friend = globals.col.friends.find({facebookID:personData.user.id})[0];
			if (friend != null) {
				if(friend.lastScore == null) friend.lastScore = 0;
				if(friend.lastScoreGain == null) friend.lastScoreGain = 0;
				friend.lastScore = friend.score;
				friend.lastScoreGain = personData.score - friend.lastScore;
				friend.score = personData.score;
			}	
		}
	}
	
	globals.col.friends.commit();
	
	var newMe = JSON.parse(JSON.stringify(globals.col.friends.find({isMe:true})[0]));
	var newFriendsData = JSON.parse(JSON.stringify(globals.col.friends.find({isMe:false, installed: true})));
	
	///Compare scores & Send Message to the ones you just passed.
	for (var i = 0; i < newFriendsData.length; i++){
		if(newMe.score > newFriendsData[i].score){
			if(oldMe.score <= newFriendsData[i].score) PushNotificationsController.SendBeatScorePushNotificationToUser(newFriendsData[i].facebookID, newMe.name+" just beat your score!");
		}
	}
	
	//Achievements
	var friendsWithApp = exports.GetNumberOfFriendsWithApp();
	AchievementsController.FriendsCheck(friendsWithApp);
	
	var ranking = exports.GetFriendsWithAppByScore();
	if(ranking.length >= 3 && ranking[0].facebookID == globals.gameData.userFacebookID) AchievementsController.IncreaseAchievement("first_rank", 1, true);
		
	
	
	
	Ti.API.info("Facebook: Update Friends Level: "+(new Date().getTime()-startTime));
	Ti.App.fireEvent("updateLeaderboard");
};


exports.UpdateFriendsInfo = function(data){
	var PassController = require("controller/PassController");
	var AnalyticsController = require("/controller/AnalyticsController");
	var startTime = new Date().getTime();
	var oldInstalledCount = JSON.parse(JSON.stringify(globals.col.friends.count({installed: true})));
	
	for (var i = 0; i < data.length; i++){
	    var personData = data[i];
	    var friend = globals.col.friends.find({facebookID: personData.id})[0];
	    
		if (friend != null){
			if(friend.installed != personData.installed || friend.thumbnailURL != personData.picture.data.url){
				globals.col.friends.update({facebookID:personData.id},
					{$set:{
						//firstName:personData.first_name,
						//lastName: personData.last_name,
						//name: personData.name,
						installed: personData.installed == null ? false: personData.installed,
						thumbnailURL: personData.picture.data.url
						}},
					{},true
				);	
			}
		}
		else{
			if (!personData.installed) personData.installed = false;
			globals.col.friends.save({
				facebookID: personData.id,
				firstName: personData.first_name,
				lastName: personData.last_name,
				name: personData.name,
				installed: personData.installed == null ? false: personData.installed,
				thumbnailURL: personData.picture.data.url,
				onTeam: false,
				isMe: false
				}
			);
		}
	}
	
	globals.col.friends.commit();
	
	Ti.API.info("Facebook: Update Friends Info: "+(new Date().getTime()-startTime));
	//PassController.CheckPassForFriends(exports.GetNumberOfFriendsWithApp());
	
	var newInstalledCount = globals.col.friends.count({installed: true});
	if(newInstalledCount > oldInstalledCount) AnalyticsController.RegisterEvent({category:"design", event_id: "friendsUpdate", value: newInstalledCount});
};


exports.GetFriendsWithAppByScore = function (){
	return globals.col.friends.find({installed: true},{$sort:{score:-1}});
};


exports.GetNumberOfFriendsWithApp = function (){
	return globals.col.friends.count({installed: true, facebookID:{$ne:globals.gameData.userFacebookID}});
};

exports.GetFriendsWithoutApp = function(){
	return globals.col.friends.find({installed: false},{$sort:{name:2}});
};


exports.GetFriendsForLevel = function (levelID){
	return globals.col.friends.find({level:levelID,facebookID:{$ne:globals.gameData.userFacebookID}});
};


exports.GetTeamMembers = function(){
	return globals.col.friends.find({installed: true, onTeam: true},{$sort:{score:-1}});
};

exports.GetAddMememberData = function (){
	var returnObject = {
		installed: globals.col.friends.find({installed: true, onTeam:false},{$sort:{score:-1}}),
		invite:exports.GetFriendsWithoutApp()
	};
	
	return returnObject;
};

exports.GetMessageFriendsData = function (){
	var start = clock.getNow();
	
	var returnObject = exports.GetAddMememberData();
	var friends =  returnObject.invite;
	var letters = [];
	
	for (var i = 0; i < friends.length; i++){
		var friend = friends[i];
		//Ti.API.info(friend);
		var fLetter = friend.name[0].toUpperCase();
		var letterExists = false;
		for (var j = 0; j < letters.length; j++){
			var letter = letters[j];
			if (letter.name == fLetter){
				letterExists = true;
				letter.friends.push(friend);
				break;
			}
		}
		
		if(letterExists == false) {
			var newLetter = CreateLetterObject(fLetter,friend);
			letters.push(newLetter);
		}	
	}
	
	returnObject.letters = letters;
	
	
	function CreateLetterObject(name,friend){
		var object = {
			name: name,
			friends: []
		};	
		object.friends.push(friend);
		
		return object;
	}
	
	//Ti.API.info((new moment().valueOf()-start.valueOf()));
	
	return returnObject;
};

exports.AddFriendToTeam = function(friendData){
	globals.col.friends.update({$id:friendData.$id},{$set:{onTeam: true}},{},true);
	globals.col.friends.commit();
	Ti.App.fireEvent("updateTeam", {teamMembers: exports.GetTeamMembers()});
};

exports.RemoveFriendFromTeam = function(friendData){
	globals.col.friends.update({$id:friendData.$id},{$set:{onTeam: false}},{},true);
	globals.col.friends.commit();
	Ti.App.fireEvent("updateTeam", {teamMembers: exports.GetTeamMembers()});
};
