function Create(){
	var controller = {};
	
	var now = clock.getNow();
	
	var tips = [{text:"Welcome to GoPhrazy"}];
	if(globals.stockData.tips) tips = globals.stockData.tips;
	
	var tipArray = [];
	var connectedToFacebook = false;
	if(globals.gameData.userFacebookID != "") connectedToFacebook = true;
	if (globals.gameData.tips){
		for (var i = 0; i < globals.gameData.tips.length; i++){
			var tip = globals.gameData.tips[i];
			if(tip.beforeConnection != connectedToFacebook){
				for(var j = 0; j < tip.frequency; j++){
					tipArray.push(tip.text);
				}	
			}
		}
	}
	else tipArray =["Hi! I'm updating!"];
	
	
	function GetText(){
		return tipArray[Math.floor(Math.random()*tipArray.length)];
	}
	
	controller.GetText = GetText;
	
	return controller;
};

module.exports = Create;

/*
function Version1(){
	var startText = "Hi!";
	var hour = now.hour();
	if(hour > 0 && hour <= 1) startText = "Mmmmm. Soo tired!";
	else if(hour >= 1 && hour < 6) startText = "I can't sleep! Let's play.";
	else if(hour >= 6 && hour < 8) startText = "Morning! It's early?!";
	else if(hour >= 6 && hour < 9) startText = "Good Morning! Ready to play?!";
	else if(hour >= 9 && hour < 10) startText = "Time for Breakfast?! Yes please!";
	else if(hour >= 10 && hour < 11) startText = "Hello!";
	else if(hour >= 11 && hour < 12) startText = "Almost Lunchtime!";
	else if(hour >= 12 && hour < 14) startText = "Time to eat! Buon Appetito!";
	else if(hour >= 14 && hour < 16) startText = "After lunch. Let's take a Siesta";
	else if(hour >= 16 && hour < 18) startText = "I feel like afternoon tea.";
	else if(hour >= 18 && hour < 20) startText = "I'm soo hungry again! Cheese!!";
	else if(hour >= 20 && hour < 22) startText = "My belly! I ate too much!";
	else if(hour >= 22 && hour < 23) startText = "Mmmmm. Soo tired!";
}

function Version2(){
	var RandomPlus = require("lib/RandomPlus");
	var texts = [
		"Prosciutto di Parma",
		"Burrata",
		"Gorgonzola",
		"Risotto",
		"Cantucci",
		"Cappuccino",
		"Macchiato",
		"Mozzarella di Bufala",
		"Polenta",
		"Piadina",
		"Torrone",
		"Pizza Margherita",
		"Carbonara",
		"Pizzoccheri",
		"Vespa",
		"Baci",
		"Cornetto",
		"Calzone",
		"Gelato",
		"Farinata",
		"Trofie al Pesto",
		"Taleggio",
		"Ricotta",
		"Crescenza",
		"Robiola",
		"Speck",
		"Prosciutto Cotto",
		"Cotoletta",
		"Ossobuco",
		"Pasticcini"
	];
	texts = RandomPlus.ShuffleArray(texts);
	
	return "Mozzarello loves "+texts[0];
}
*/