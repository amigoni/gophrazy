
function MakeCall (id, category, message, successCallback, errorCallback){
	//Production (USE THIS WHEN PUBLISHING)
	//var game_key = "0337b83de61bc5f7e2e61c6928b38b1e";
	//var secret_key = "1a973b8ef94f672ca852cff81fa608c7c99f60e8";
	
	//Testing (used this to no pollute the Sandbox old account) USE THIS WHILE DEVELOPING
	var game_key = 'b89b5d78071e0cbd2e503de3cc1c8836';                                  
	var secret_key = 'cba872f572b65759d1e9294b29bda1059bc9759b';
	
	//Sandbox (Old Account)
	//var game_key = '7fc1878f2b847931ac09033e849459a8';                                  
	//var secret_key = '11aae379b08bcf7ef8af4f151d0ee05e65ad34f7';
	
	
	var url = 'http://api.gameanalytics.com/1/'+game_key+'/'+category;
	var json_message = JSON.stringify(message);
	
	var header_auth_hex = Ti.Utils.md5HexDigest(json_message + secret_key);
	
	var client = Ti.Network.createHTTPClient({	
	     onload : function(e) {
	     	Ti.API.info("ANALYTICSCONTROLLER:"+this.responseText);
	     	if(successCallback) successCallback({id: id, text: this.responseText, status: this.statusText});
	     },
	     onerror : function(e) {
	     	if(errorCallback) errorCallback(e);
	     	Ti.API.info("ANALYTICSCONTROLLER:"+this.responseText);
	     	Ti.API.info("Status Code: "+this.status+" "+this.statusText);
	     },
	     onreadystatechange: function(e){
	     	//Ti.API.info("Ready State "+this.readyState+" Status Code: "+this.status);
	     },
	     timeout : 10000  // in milliseconds
	});
	
	client.open("POST", url);
	
	client.setRequestHeader("Authorization", header_auth_hex);
	client.setRequestHeader("Content-Type", "text/plain"); 
	
	client.send(json_message);
}

//To Prevent double calls
var lastEventData;
var lastTimeRegister = clock.getNow().valueOf();

exports.RegisterEvent = function(data){
	var newEvent;
	var now = clock.getNow().valueOf();
	var dataString = JSON.stringify(data);
	var lastDataString = JSON.stringify(lastEventData);
	if(dataString != lastDataString || (dataString == lastDataString && now - 1000 > lastTimeRegister)){
		//Game Analytics
		newEvent = JSON.parse(JSON.stringify(data)); // Make a copy
		newEvent.user_id = globals.gameData.analytics.userID;
		newEvent.session_id = globals.gameData.analytics.sessionID;
		newEvent.build ="v:"+globals.gameData.versions.appVersion+" CID:"+globals.gameData.versions.changesVersion+" GROUP:"+ globals.gameData.versions.ABTestingGroup; //this is for AB testing so should suffix the build name with the group
		
		var id = SaveEvent(newEvent);
		SendCachedEvent(id);
	}
	else{
		Ti.API.info("ANALYTICSCONTROLLER:RepeatCall");
	}
	
	lastEventData = data;
	lastTimeRegister = now;
	
	return newEvent;
};


exports.SendCachedEvents = function(){
	function SuccessCallBack(){
		if(Ti.Network.online == true){
			var events = globals.col.analyticsEvents.getAll();
		
			if(events.length > 0){
				Ti.API.info("ANALYTICSCONTROLLER:SendingCachedEventsMultiple:left-"+events.length);
				SendCachedEvent(events[0].$id, SuccessCallBack);
			} 
		}
	}
	
	SuccessCallBack();
};


function SaveEvent(eventData){
	globals.col.analyticsEvents.save({
		eventData: eventData
	});
	
	globals.col.analyticsEvents.commit();
	Ti.API.info("ANALYTICSCONTROLLER:SaveEvent");
	return globals.col.analyticsEvents.getLastInsertId();
}


function DeleteEvent(id){
	globals.col.analyticsEvents.remove({$id:id});
	globals.col.analyticsEvents.commit();
	Ti.API.info("ANALYTICSCONTROLLER:DeleteEvent");
}


function SendCachedEvent(id, successCallBack){
	Ti.API.info("ANALYTICSCONTROLLER:SendingCachedEvent");
	var copy = JSON.parse(JSON.stringify(globals.col.analyticsEvents.find({$id:id})[0]));
	var eventData = copy.eventData;
	var category = eventData.category;
	delete eventData.category;
	
	MakeCall(copy.$id, category, eventData, function(e){
		DeleteEvent(e.id);
		if(successCallBack) successCallBack();
	});
}

//Module to log crash events. This unfortunately is only after the module is initialized. Can't log before that as of now.
var Logger = require("yy.logcatcher");
Logger.addEventListener('error',function(e){
  var errorEvent = {
  	category: "error",
  	severity: "error",
  	message: JSON.stringify(e)
  };
  
  exports.RegisterEvent(errorEvent);
});
