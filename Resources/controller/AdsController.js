function Create(network){
	var controller;
	
	if(network == "chartboost"){
		var now = clock.getNow();
		controller = {};
		controller.ShowAd = function(){};
		controller.CheckToShowInterstitial = function(){};
		
		/*
		controller = require('com.inzori.chartboost');
		controller.hasCachedAd = false;
		
		controller.init({
			appId: "545e4c2043150f420ad4f9a8",
			appSignature: "7f0ddd4f3007f668dc1ef9348bed6de1b580d147",
			cache: true
		});
		
		controller.addEventListener('chartboostmodule:init',function(e){
			Ti.API.info(e);
		});
		
		controller.addEventListener('chartboostmodule:didFailToLoadInterstitial',function(e){Ti.API.info(e);});
		controller.addEventListener('chartboostmodule:didCacheInterstitial',function(e){
			controller.hasCachedAd = true;
			Ti.API.info(e);
		});
		controller.addEventListener('chartboostmodule:didDismissInterstitial',function(e){
			controller.hasCachedAd = false;
			Ti.API.info(e);
		});
		
		controller.addEventListener('chartboostmodule:didFailToLoadMoreApps',function(e){Ti.API.info(e);});
		
		
		controller.addEventListener('chartboostmodule:didDismissMoreApps',function(e){Ti.API.info(e);});
		
		function ShowAd(type){
			if(controller.hasCachedAd == true){
				if(type == "static_1") controller.showInterstitial("static_1");
				else if(type == "video_1") controller.showInterstitial("video_1");
				globals.gameData.ads.lastInterstitialDispayTime = now.toISOString();
				globals.gameData.ads.puzzlesCompleteCountSinceLastInterstitialAd = 0;
			}
			else{
				Ti.API.info("No Ad! Need Mediation Integration");
			}
		};
		
		function CheckToShowInterstitial(){
			now = clock.getNow();
			globals.gameData.ads.puzzlesCompleteCountSinceLastInterstitialAd++;
			if(controller.hasCachedAd == true && globals.gameData.passData.currentlyOnBoughtPass == false){
				var limitDisplayTime =  new moment(globals.gameData.ads.lastInterstitialDispayTime).add(globals.gameData.ads.interstitialTimeDeltaFrequency, "minutes");
				var minimumLimitDisplayTime = new moment(globals.gameData.ads.lastInterstitialDispayTime).add(globals.gameData.ads.interstitialMinimumTimeDeltaFrequency, "minutes");
				if(globals.gameData.phrasesCompleteCountForPass > globals.gameData.ads.firstInterstitialAppearsAtPuzzle && now.valueOf() > minimumLimitDisplayTime.valueOf() &&
					(now.valueOf() > limitDisplayTime.valueOf() || globals.gameData.ads.puzzlesCompleteCountSinceLastInterstitialAd >= globals.gameData.ads.interstitialCompleteCountFrequency))
				{
					globals.AdsController.ShowAd("static_1");
				}
			}	
			globals.col.gameData.commit();
		}
		
		controller.ShowAd = ShowAd;
		controller.CheckToShowInterstitial = CheckToShowInterstitial;
		*/
	}
	else if (network == "admob"){
		var Admob = require('ti.admob');
		controller = Admob.createView({
		    //top: 0, 
		    width: 250, 
		    height: 320,
		    adUnitId: 'ca-app-pub-7157276024136518/8463591589', // You can get your own at http: //www.admob.com/
		    adBackgroundColor: 'transparent',
		    testDevices: [Admob.SIMULATOR_ID],
		   // dateOfBirth: new Date(1985, 10, 1, 12, 1, 1),
		   // gender: 'male',
		   // keywords: ''
		});
		
	
		
		var now = clock.getNow();
		controller.hasCachedAd = false;
		
		controller.addEventListener('didReceiveAd', function(){
		    Ti.API.info('Did receive ad!');
		    controller.hasCachedAd = true;
		});
		
		controller.addEventListener('didFailToReceiveAd', function(){
		    Ti.API.info('Failed to receive ad!');
		    controller.hasCachedAd = false;
		   // Ti.API.info(e);
		});
		
		controller.addEventListener('willPresentScreen', function(){
		    Ti.API.info('Presenting screen!');
		});
		
		controller.addEventListener('willDismissScreen', function(){
		    Ti.API.info('Dismissing screen!');
		    //Ti.API.info(e);
		});
		
		controller.addEventListener('didDismissScreen', function(){
		    Ti.API.info('Dismissed screen!');
		    controller.hasCachedAd = false;
		   // Ti.API.info(e);
		   
		   
		});
		
		controller.addEventListener('willLeaveApplication', function(){
		    Ti.API.info('Leaving the app!');
		    //Ti.API.info(e);
		});
		
		function ShowAd(type){
			if(controller.hasCachedAd == true)
			{
				globals.gameContainer.add(controller);
				globals.gameData.ads.lastInterstitialDispayTime = now.toISOString();
				globals.gameData.ads.puzzlesCompleteCountSinceLastInterstitialAd = 0;
			}
			else{
				Ti.API.info("No Ad!");
			}
		};
		
		function DismissAd(){
			globals.gameContainer.remove(globals.AdsController);
			controller = null;
			globals.AdsController = Create("admob");
		}
		
		function CheckToShowInterstitial(){
			now = clock.getNow();
			globals.gameData.ads.puzzlesCompleteCountSinceLastInterstitialAd++;
			if(controller.hasCachedAd == true && globals.gameData.passData.currentlyOnBoughtPass == false){
				var limitDisplayTime =  new moment(globals.gameData.ads.lastInterstitialDispayTime).add(globals.gameData.ads.interstitialTimeDeltaFrequency, "minutes");
				var minimumLimitDisplayTime = new moment(globals.gameData.ads.lastInterstitialDispayTime).add(globals.gameData.ads.interstitialMinimumTimeDeltaFrequency, "minutes");
				if(globals.gameData.phrasesCompleteCountForPass > globals.gameData.ads.firstInterstitialAppearsAtPuzzle && now.valueOf() > minimumLimitDisplayTime.valueOf() &&
					(now.valueOf() > limitDisplayTime.valueOf() || globals.gameData.ads.puzzlesCompleteCountSinceLastInterstitialAd >= globals.gameData.ads.interstitialCompleteCountFrequency))
				{
					globals.AdsController.ShowAd("static_1");
				}
			}	
			globals.col.gameData.commit();
		}
		
		controller.ShowAd = ShowAd;
		controller.CheckToShowInterstitial = CheckToShowInterstitial;
	}
	
	
	return controller;
}

module.exports = Create;