// Used to display the time on the icon;
exports.GetDailyChallengeStatusSimple = function(){
	var now = clock.getNow();
	var expirationTime = new moment(globals.gameData.dailyChallengeExpirationTime);
	
	var returnObject = {};
	returnObject.todayDailyComplete = globals.gameData.dailyChallengeComplete;
	returnObject.timeDisplayString = GetTimeLeftString();
	
	
	if (now.valueOf() > expirationTime.valueOf()) {
		globals.gameData.dailyChallengeExpirationTime = clock.getNow().hours(0).minutes(0).seconds(1).add("d",1).toISOString();
		expirationTime = new moment(globals.gameData.dailyChallengeExpirationTime);
		globals.gameData.dailyChallengeComplete = false;
		exports.SetNextDailyChallengePhrase();
		//globals.col.gameData.commit();
	}
	
	function GetTimeLeftString(){
		var diff = new moment.duration(expirationTime.diff(now));
		var hours = diff.hours();
		if (hours < 10) hours = "0"+hours;
		var minutes = diff.minutes();
		if (minutes < 10) minutes = "0"+minutes;
		var seconds = diff.seconds();
		if (seconds < 10) seconds = "0"+seconds;
		var text = hours+":"+minutes+":"+seconds;
		
		return text;
	}
	
	return returnObject;
};


exports.CompleteDailyChallengeSimple = function(phraseID){
	var AnalyticsController = require("/controller/AnalyticsController");
	var MonstersController = require("/controller/MonstersController");
	var returnObject = {};
	returnObject.rewardMonster = false;
	returnObject.weekComplete = false;
	
	globals.gameData.dailyChallengeComplete = true;
	
	var fortuneCookieData = globals.gameData.fortuneCookieData;
	fortuneCookieData.completeCount++;
	//Calculate Complete Rate
	
	AnalyticsController.RegisterEvent({category: "design", event_id:"fortuneCookieComplete", value: fortuneCookieData.completeCount});
	
	fortuneCookieData.current.count++;
	if(fortuneCookieData.current.name == "hints"){
		if(fortuneCookieData.current.count >= fortuneCookieData.data.hints.max){
			globals.gameData.hints++;
			fortuneCookieData.current.count = 0;
			fortuneCookieData.current.completeCounter++;
			
			globals.gameData.hintsEarnedCount++;
			AnalyticsController.RegisterEvent({category: "design", event_id:"hintsEarned", value: globals.gameData.hintsEarnedCount});
		}
	}
	else if(fortuneCookieData.current.name == "skips"){
		if(fortuneCookieData.current.count >= fortuneCookieData.data.skips.max){
			globals.gameData.skips++;
			fortuneCookieData.current.count = 0;
			fortuneCookieData.current.completeCounter++;
			
			globals.gameData.skipsEarnedCount++;
			AnalyticsController.RegisterEvent({category: "design", event_id:"skipsEarned", value: globals.gameData.hintsEarnedCount});
		}
	}
	
	//Set the next reward to Hints or skips every 3.
	if(fortuneCookieData.current.completeCounter % fortuneCookieData.data.hints.freq == 0 && fortuneCookieData.current.completeCounter != 0){
		fortuneCookieData.current.name = "skips";	
	}
	else fortuneCookieData.current.name = "hints";	
	
	Ti.API.info("COMPLETE FORTUNE COOKIE Type :"+fortuneCookieData.current.name+" Count: "+fortuneCookieData.current.count+" Complete Counter: "+fortuneCookieData.current.completeCounter);

	globals.col.gameData.commit();
	
	setTimeout(function(){
		//globals.rootWin.OpenWindow("fortuneCookieRewardWin", returnObject);
		globals.rootWin.OpenWindow("smallSquarePopUp", "fortuneCookieReward");	
	},500);
};


//This is called after the Fortune cookie popup to set all the new data.
exports.ProcessFortuneCookie = function(){
	var fortuneCookieData = globals.gameData.fortuneCookieData;
	var weekComplete = true;
	for (var i = 0; i < fortuneCookieData.week.length; i++){
		var day = fortuneCookieData.week[i];
		if (day.complete == false) {
			weekComplete = false;
			break;
		}	
	}
	
	if (weekComplete == true){
		fortuneCookieData.inStock = 0;
		fortuneCookieData.weeksCompleted++;
	}
	
	if (fortuneCookieData.inStock >= 7){
		fortuneCookieData.inStock = 0;
	}
	
	globals.col.gameData.commit();
	//Ti.API.info(fortuneCookieData);
};


exports.SetNextDailyChallengePhrase = function(){
	var fortuneCookies = globals.col.phrases.find({type:"fortuneCookie", complete: false, difficulty:{$lte: 15}},{$sort:{$id:0}});
	if(fortuneCookies.length == 0) globals.col.phrases.find({type:"fortuneCookie", complete: true, difficulty:{$lte: 15}},{$sort:{$id:0}})[0];
	var newChallengeID = fortuneCookies[0].phraseID;
	globals.gameData.dailyChallengeCurrentPhraseID = newChallengeID;
	globals.col.gameData.commit();
};