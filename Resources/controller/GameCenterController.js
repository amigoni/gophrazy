
if(Ti.Platform.name == "iPhone OS"){
	globals.gameCenter = {};
	globals.gameCenter.active = false;
}		
	

function InitGameCenter(){
	if(Ti.Platform.name == "iPhone OS"){
		var gamekit = require('com.obigola.gamekit');
		gamekit.initGameCenter({
			success: function() {
				globals.gameCenter.active = true;
				var player = gamekit.getPlayerInfo();
				player = JSON.parse(player);
				
				if(globals.gameData.gameCenterData.id == ""){
					var AnalyticsController = require("/controller/AnalyticsController");
					AnalyticsController.RegisterEvent({category:"design", event_id: "gameCenter:loggedInFirstTime"});
				}
				
				globals.gameData.gameCenterData.id = player.id;
				globals.gameData.gameCenterData.alias = player.alias;
				
				Ti.API.info("GAMECENTER: INITIALIZED");
				Ti.API.info("GAMECENTER PLAYER INFO: ");
				Ti.API.info(player);
			},
			error: function(e) {
				Ti.API.info("GAMECENTER: ERROR");
				Ti.API.info(e);
			}
		});
	}	
}

Ti.App.addEventListener("initGameCenter", InitGameCenter);