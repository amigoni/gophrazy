exports.CheckPassForFriends = function(numberOfFriends){
	var FriendsController = require("controller/FriendsController");
	//Check everytime you login for the new friends
	if(globals.gameData.passData.passOpen == false){
		if(numberOfFriends >= globals.gameData.passData.numberOfFriendsAtUnlock + globals.gameData.passData.friendsIncrement){
			globals.gameData.passData.passes++;
			globals.gameData.passData.passesFriendsCount++;
			globals.gameData.passData.currentPassType = "friends";
			globals.gameData.passData.nextPassAt += globals.gameData.passData.curveFacebook;
			globals.gameData.passData.passOpen = true;
			globals.col.gameData.commit();
			alert("Yeah! Thanks for getting friends!\nYou can play more!");
		}
	}
};


exports.CompletePhrasePassCheck = function(){
	var FriendsController = require("controller/FriendsController");
	
	if (globals.gameData.passData.passOpen == true){
		var numberOfFriends = FriendsController.GetNumberOfFriendsWithApp();
		//Check if it's the first item otherwise check every second item with the remainder;
		var phrasesCompleteForPass = globals.gameData.phrasesCompleteCountForPass;

		if(phrasesCompleteForPass >= globals.gameData.passData.nextPassAt){
			ClosePass(numberOfFriends);
		}
	}
	
	function ClosePass(numberOfFriends){
		globals.gameData.passData.passOpen = false;
		globals.gameData.passData.currentlyOnBoughtPass = false;
		globals.gameData.passData.currentPassType = "none";
		globals.gameData.passData.numberOfFriendsAtUnlock = numberOfFriends;
		globals.col.gameData.commit();	
	}
	
	Ti.API.info("PASS CONTROLLER: Pass Data");
	Ti.API.info(globals.gameData.passData);
};


exports.UsePass = function(callback){
	globals.gameData.passData.passOpen = true;
	globals.gameData.passData.passes = 0;
	globals.col.gameData.commit();
	callback();
};


exports.CheckForAccess = function(){
	var accessOpen = false;
	if (globals.gameData.passData.passOpen == true) accessOpen = true;
	else{
		//1 Compare dates, if it's a new day give the pass
		var isPastDay = moment(globals.gameData.lastPlayedTime).isBefore(clock.getNow().valueOf(), "day");
		globals.gameData.lastPlayedTime = clock.getNow().toISOString();//Put here as it's already filtered. It should be in the Phrase Controller only for the right phrases
		if(isPastDay == true){
			Ti.API.info("Resetting Free Daily Passes");
			globals.gameData.passData.freeDailyPuzzlesUsed = 0;
			accessOpen = true;
			OpenNotification();
		}
		//It's the same day, check how many have been used and allow only if under the limit
		else{
			if(globals.gameData.passData.freeDailyPuzzlesUsed < globals.gameData.passData.freeDailyPuzzlesAllowed) {
				OpenNotification();
				accessOpen = true;	
			}	
			else accessOpen = false;
		}
		globals.col.gameData.commit();
	}
	
	function OpenNotification(){
		var TopNotificationsPopUp = require("/ui/PopUps/TopNotificationsPopUp");
		globals.rootWin.OpenWindow("topNotificationsPopUp", {type:"freePuzzle"});
	}
	
	return accessOpen;
};
