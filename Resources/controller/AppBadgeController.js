exports.Init = function(){
	//If there are any incomplete messages or new collections or new events the app badge is set to 1
	
	if(Ti.Platform.name == "iPhone OS") {
		Titanium.UI.iPhone.setAppBadge(0);	
	
		function UpdateAppBadge(){
			var incompleteMessages = globals.col.messages.count({complete: false});
			var newCollections = globals.col.groups.count({isNew: true, kind: "issue"});
			
			if(Ti.Platform.name == "iPhone OS"){
				if(incompleteMessages > 0 || (newCollections > 0 && globals.gameData.abilities.puzzlePost.enabled == true)) Titanium.UI.iPhone.setAppBadge(1);	
				else Titanium.UI.iPhone.setAppBadge(null);
			}
		}
		
		UpdateAppBadge();
	}
	
	
	Ti.App.addEventListener("updateMessagesCount", UpdateAppBadge);
};
