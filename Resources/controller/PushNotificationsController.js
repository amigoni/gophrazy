var ortc;

if(Ti.Platform.name == "iPhone OS") {
	ortc = require('/lib/co.realtime.ortc.apns');
} 
else if(Ti.Platform.name == "android") {
	ortc = require('co.realtime.ortc');
	//var androidPushNotification = require("/lib/AndroidPushNotification");
	//androidPushNotification.pushNotification();
}	


//Production
//var hinter1 = "1f8lNI";
//var skipper1 = "jlbN1hTNfYi0";

//Developement
var hinter1 = "OMDicR";
var skipper1 = "EwJLgICnNRdF";

globals.PushNotificationsChannelController = CreateChannelController();

exports.Init = function(){
	var ServerData = require("model/ServerData");
	
	if(globals.testing == false){
		globals.checkedForMessages = false;
		
		ortc.connectionMetadata = 'GoPhrazy';
		ortc.clusterUrl = 'http://ortc-developers.realtime.co/server/2.1';
		//ortc.url = 'http://ortc-developers-euwest1-S0001.realtime.co/server/2.1';
		ortc.addEventListener('onConnected', function(e) {
			Ti.API.info("ORTC: Connected: "+e);
			OnConnected();
		});
		
		ortc.addEventListener('onSubscribed', function(e) { 
			Ti.API.info("ORTC: Subscribed to: "+e.channel+" ");
			OnSubscribed();
		});
		
		ortc.addEventListener('onUnsubscribed', function(e) { 
			Ti.API.info("ORTC: Unsubscribed to: "+e.channel+" ");
			OnUnsubscribed();
		});
		
		ortc.addEventListener('onMessage', function(e) {
			Ti.API.info('ORTC: Received Message - Channel: '+e.channel+'- Message: '+e.message);
			OnMessage(e);
		});
		
	
		ortc.connect(hinter1);
		Ti.API.info("Push Notifications Controller: Init");
	}
	
	
	function OnConnected(){
		//TEMP this should happen after they reach the puzzle post 
		if(globals.gameData.notifications.pushNotificationsInitialized == true) globals.PushNotificationsChannelController.SubscribeToNextChannel();
		
		/*
		if(Ti.Platform.name == "android"){
			var pendingData = gcm.data;
			if (pendingData && pendingData !== null) {
				// if we're here is because user has clicked on the notification
				// and we set extras for the intent 
				// and the app WAS NOT running
				// (don't worry, we'll see more of this later)
				Ti.API.info('******* data (started) ' + JSON.stringify(pendingData));
			}
			
			gcm.registerForPushNotifications({
				success: function (ev) {
					// on successful registration
					Ti.API.info('******* success, ' + ev.deviceToken);
				},
				error: function (ev) {
					// when an error occurs
					Ti.API.info('******* error, ' + ev.error);
				},
				callback: function (ev) {
					// when a gcm notification is received WHEN the app IS IN FOREGROUND
					alert(ev);
				},
				unregister: function (ev) {
					// on unregister 
					Ti.API.info('******* unregister, ' + ev.deviceToken);
				},
				data: function (data) {
					// if we're here is because user has clicked on the notification
					// and we set extras in the intent 
					// and the app WAS RUNNING (=> RESUMED)
					// (again don't worry, we'll see more of this later)
					Ti.API.info('******* data (resumed) ' + JSON.stringify(data));
				}
			});
		
		// in order to unregister:
		// require('net.iamyellow.gcmjs').unregister();
		} 
		*/
	}
	
	function OnSubscribed(){
		//alert("ORTC: Subscribed to: "+e.channel+" "+e);
		if(globals.gameData.notifications.pushNotificationsInitialized == true) globals.PushNotificationsChannelController.SubscribeToNextChannel();
	}
	
	function OnUnsubscribed(){
		
	}
	
	function OnMessage(e){
		var myChannel = ChannelConverter(globals.gameData.userFacebookID);
		if (e.channel == myChannel || e.channel == myChannel+"_beatScore") ServerData.GetPuzzlesMessages(true);
		globals.checkedForMessages = true;
	}
};


function CreateChannelController(){
	var channelController = {};
	var channels = [];
	var currentPosition = 0;
	
	var announcementsChannel = "Announcements_"+globals.systemData.currenti18n;
	var myChannel = ChannelConverter(globals.gameData.userName);
	var beatScoreChannel = ChannelConverter(globals.gameData.userName)+"_beatScore";
	var solvedPuzzleChannel = ChannelConverter(globals.gameData.userName)+"_solved_puzzle";
	
	if(globals.gameData.notifications.announcements.enabled == true) channels.push(announcementsChannel);
	
	if(globals.gameData.userName){
		if(globals.gameData.notifications.messages.enabled == true && globals.gameData.userName != "") channels.push(myChannel);
		if(globals.gameData.notifications.beatScore.enabled == true && globals.gameData.userName != "") channels.push(beatScoreChannel);
		if(globals.gameData.notifications.solvedPuzzle && globals.gameData.notifications.solvedPuzzle.enabled == true && globals.gameData.userName != "") channels.push(solvedPuzzleChannel);
	}
	
	function SubscribeToNextChannel (){
		if(currentPosition < channels.length) {
			Ti.API.info("Subscribing to "+channels[currentPosition]);
			exports.SubscribeToChannel(channels[currentPosition], true);
			currentPosition++;
		}
	}
	
	function Refresh(){
		globals.PushNotificationsChannelController = CreateChannelController();
	}
	
	
	channelController.channels = channels;
	channelController.currentPosition = currentPosition;
	channelController.SubscribeToNextChannel = SubscribeToNextChannel;
	channelController.Refresh = Refresh;
	
	
	return channelController;
}


exports.SubscribeToChannel = function(channel, subscribeOnReconnect){
	if (Ti.Platform.name == "iPhone OS") {
		if(Ti.Platform.model != "Simulator") ortc.subscribeWithNotifications(channel,subscribeOnReconnect);
		else ortc.subscribe(channel, subscribeOnReconnect);
	}	
	else {
		Ti.API.info("Android Subscribe");
		ortc.subscribe(channel, subscribeOnReconnect);
	}	
};


exports.SubscribeToMyChannel = function(){
	if(globals.testing == false){
		var myChannel = ChannelConverter(globals.gameData.userName);
		if(myChannel != "") exports.SubscribeToChannel(myChannel, true);
	}
};


exports.UnsubscribeToChannel = function(channel){
	ortc.unsubscribe(channel);
};


exports.SendPushNotificationToUser = function(userID, message){
	exports.SendPushNotification(ChannelConverter(userID), message);
};


exports.SendBeatScorePushNotificationToUser = function(userID, message){
	exports.SendPushNotification(ChannelConverter(userID)+"_beatScore", message);
};


exports.SendSolvedPuzzleNotificationToUser = function(userID, puzzleData){
	//Check if you should send notification from puzzle data
	var message = "";
	var sendNotification = false;
	if(puzzleData.solvedCountOthers <= 5) sendNotification = true;
	else if(puzzleData.solvedCountOthers > 5 && puzzleData.solvedCountOthers <= 10 && puzzleData.solvedCountOthers%2 == 0) sendNotification = true;
	else if(puzzleData.solvedCountOthers > 10 && puzzleData.solvedCountOthers <= 50 && puzzleData.solvedCountOthers%5 == 0) sendNotification = true;
	else if(puzzleData.solvedCountOthers > 50 && puzzleData.solvedCountOthers <= 200 && puzzleData.solvedCountOthers%10 == 0) sendNotification = true;
	else if(puzzleData.solvedCountOthers > 200 && puzzleData.solvedCountOthers <= 1000 && puzzleData.solvedCountOthers%100 == 0) sendNotification = true;
	else if(puzzleData.solvedCountOthers > 1000 && puzzleData.solvedCountOthers <= 100000 && puzzleData.solvedCountOthers%1000 == 0) sendNotification = true;
	else if(puzzleData.solvedCountOthers > 100000 && puzzleData.solvedCountOthers%100000 == 0) sendNotification = true;
	
	if(puzzleData.solvedCountOthers == 1) message = "1 person has solved your puzzle!";
	else message = puzzleData.solvedCountOthers+" people have solved your puzzle!";
	
	 if(sendNotification == true) exports.SendPushNotification(ChannelConverter(userID), message);
};


exports.SendPushNotification = function(channel, message){
	if(globals.os == "ios" && globals.testing == false){
		var url = 'https://ortc-mobilepush.realtime.co/mp/publish';
		
		var notification =  {
		 	applicationKey: hinter1,
		 	privateKey: skipper1, 
		 	channel: channel, 
		 	message: message,
		 	//"payload" : { "sound" : "default", "badge" : "1"}
		};
		
		Ti.API.info(notification);
		
		var client = Ti.Network.createHTTPClient({
		     onload : function(e) {
		         Ti.API.info("ORTC Sent text: " + this.statusText);
		     },
		     onerror : function(e) {
		     	Ti.API.info("ORTC Error");
		        Ti.API.info(this.responseText+" "+this.statusText);
		     },
		     timeout : 10000
		});
		 
		client.setRequestHeader("Content-Type", "application/json");
		client.setRequestHeader('Charset', 'utf-8'); 
		client.open("POST", url);
		
		client.send(JSON.stringify(notification));
	}
};


//Seems like the channels can only be alpha not numeric so converting numbers to letters.
function ChannelConverter(dataIn){
	
	var returnString = dataIn;
	
	if(!isNaN(parseFloat(dataIn)) && isFinite(dataIn)) returnString = dataIn.toString();
	/*
	for( var i = 0; i < dataIn.length; i++){
		var c = dataIn[i];
		if (c == 0){ returnString += "a"; }
		else if(c == 1){ returnString += "b"; }
		else if(c == 2){ returnString += "c"; }
		else if(c == 3){ returnString += "d"; }
		else if(c == 4){ returnString += "e"; }
		else if(c == 5){ returnString += "f"; }
		else if(c == 6){ returnString += "g"; }
		else if(c == 7){ returnString += "h"; }
		else if(c == 8){ returnString += "i"; }
		else if(c == 9){ returnString += "j"; }
	}
	*/
	
	return returnString;
}


exports.GetChannelByType = function(type){
	var channel;
	if(type == "announcements") channel =  "Announcements_"+globals.systemData.currenti18n;
	else if(type == "messages") channel = ChannelConverter(globals.gameData.userName);
	else if(type == "beatScore") channel = ChannelConverter(globals.gameData.userName)+"_beatScore";
	else if(type == "solvedPuzzle") channel = ChannelConverter(globals.gameData.userName)+"solved_puzzle";
	
	return channel;
};


exports.CheckForPushNotificationsTutorialCompletion = function(){
	//Check for Notification tutorial
	var tutorialCompletePush = function(){
			globals.gameData.notifications.pushNotificationsInitialized = true;
			globals.gameData.notifications.messages.visible = true;
			globals.gameData.notifications.messages.enabled = true;
			globals.gameData.notifications.beatScore.visible = true;
			globals.gameData.notifications.beatScore.enabled = true;
			globals.gameData.notifications.solvedPuzzle.visible = true;
			globals.gameData.notifications.solvedPuzzle.enabled = true;
			globals.col.gameData.commit();
			globals.PushNotificationsChannelController.Refresh();
			globals.PushNotificationsChannelController.SubscribeToNextChannel();
			Ti.App.removeEventListener("tutorialComplete", tutorialCompletePush);
	};
		
	Ti.App.addEventListener("tutorialComplete", tutorialCompletePush);
	
	if(globals.gameData.notifications.pushNotificationsInitialized == false && Ti.Platform.name == "iPhone OS"){
		Ti.App.fireEvent("openTutorial", {name: "pushNotificationsFirst"});
	}
	else Ti.App.fireEvent("openTutorial", {name: "pushNotificationsNotFirst"});
};
