globals.windowController = {};
globals.windowController.stack = [];

globals.windowController.AddWindow = function (win){
	globals.windowController.stack.push(win);
};

globals.windowController.GoBack = function(){
	var win  = globals.windowController.stack[globals.windowController.stack.length-2];
	globals.windowController.BackToThisWindow(win);
	globals.windowController.stack[globals.windowController.stack.length-1] = null;
	globals.windowController.stack.pop();
};

globals.windowController.OpenAnotherWindow = function(win){
	win.animate(Ti.UI.createAnimation(
		{duration: globals.style.animationSpeeds.speed1, 
		transform: Ti.UI.create2DMatrix().translate(-globals.platform.width,0)}
	));
};
	
globals.windowController.BackToThisWindow = function(win){
	win.animate(Ti.UI.createAnimation(
		{duration: globals.style.animationSpeeds.speed1, 
		transform: Ti.UI.create2DMatrix().translate(0,0)}
	));
};