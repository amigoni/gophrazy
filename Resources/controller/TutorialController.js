var tutorials = {
	firstStart:{
		name: "firstStart",
		currentStep: 0,
		phraseID: 561,
		steps:[
			{text: "Hi!!! Welcome to GoPhrazy!\nReady to learn how to play?", button: "_next", word: null, position: "center" },
			{text: "Tap the words in the right order to create the phrase.\n-The First word is Capitalized.\n-The Last word ends with a period.", button: null, word: "_any", position: "center" },
			{text: "You got it!\nPress the X on top right.", button: "gameWinBackButton", word: null, position: "bottom" }
		]
	},
	hints:{
		name: "hints",
		currentStep: 0,
		phraseID: 558,
		steps:[
			{text: "You've unlocked Hints! They make your life much much easier.\n\nPlease read CAREFULLY, it will only take a minute.", button: "_next", word: null, position: "center" },
			{text: "Hints fix a word in its correct position in the phrase. Let me show you!\n\nPress the NEXT PUZZLE button.", button: "wonToolBarNextButton", word: null, position: "top" },
			{text: "Pretend you need help solving this puzzle.\n\nPress the HINT button at the BOTTOM of the screen.", button: "gameToolBarHintButton", word: null, position: "top" },
			{text: "Select the [very] word box as your Hint.", button: null, word: "very ", position: "center" },
			{text: "READ CAREFULLY.\nFrom now on every word you tap will automatically go to the correct side of the Hinted word. Let me show you.\nSelect [Hints].", button: null, word: "Hints ", position: "center" },
			{text: "See how it went before the hinted one?! Usually it would have gone after!\n\nPress [are].", button: null, word: "are ", position: "center" },
			{text: "press [useful]", button: null, word: "useful ", position: "center" },
			{text: "See how it went after?!\nFinally, press [things.]", button: null, word: "things. ", position: "center" },
			{text: "Now, here are 3 TIPS.\n\n1) You can use more than one hint but don't waste Hints on the first and last word since they are easy to spot.", button: "_next", word: null, position: "bottom" },
			{text: "2) For a Hint, choose a word that you think will help you reveal the structure of the phrase.\nThink before you choose!", button: "_next", word: null, position: "bottom" },
			{text: "3) After you choose a Hint, tap all the words to see on which side they fall. This will practically split the puzzle in two and make it easier to solve. Enjoy! :)", button: "gameWinBackButton", word: null, position: "bottom" },
		]
	},
	skipboxUnlocked:{
		name: "skipboxUnlocked",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "You've unlocked Skipping. You can now skip puzzles that are too hard or bothering.\n\nSkipped puzzles will go to the Skipbox in the main menu.", button: "_next", word: null, position: "center"},
		]
	},
	skipboxOpened:{
		name: "skipboxOpened",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "This is where your skipped puzzles end up. There is a "+globals.gameData.skipboxLimit+" puzzles limit to the Skipbox. When full, you can either upgrade it or you'll need to solve some of the puzzles to clear some space.", button: "_next", word: null, position: "center"}
		]
	},
	fortuneCookiesUnlocked:{
		name: "fortuneCookiesUnlocked",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "Mmmm. Smell that? \nDaily Fortune Cookies are here.\nYou can find them in the main menu!", button: "_next", word: null, position: "center"},
		]
	},
	fortuneCookiesOpened:{
		name: "fortuneCookiesOpened",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "Fortune Cookies are daily personal fortunes. Complete a Cookie to earn towards SKIPS and HINTS (more on those later).", button: "_next", word: null, position: "center"},
			{text: "Cookies are ready at Midnight! We'll notify you in the morning and at night if you didn't complete it.", button: "_next", word: null, position: "center"},
			{text: "If you want to turn off notifications go to Settings in the main menu.\n\nEnjoy all the cookies. There are hundreds of them and new ones coming weekly.", button: "_next", word: null, position: "center"},
		]
	},
	puzzlePostFirstPush:{
		name: "puzzlePostFirstPush",
		currentStep: 0,
		phraseID: null,
		steps: [
			{text: "Yes! You've unlocked The Puzzle Post.\nYou'll get the latest puzzles for free every week.\nYou can find the Puzzle Post in the main menu.", button: "_next", word: null, position: "center"},
			{text: "Complete a Puzzles Post to get rewards such as SKIPS and HINTS.", button: "_next", word: null, position: "center"},
			{text: "We'll send One notification per week when the Puzzle Post releases.", button: "_next", word: null, position: "center"},
			{text: "To turn off notifications go to Settings in the main menu.", button: "_next", word: null, position: "center"},
			{text: "Press OK on the next popup. :)\n\nEnjoy!", button: "_next", word: null, position: "center"}
		]
	},
	puzzlePostNotFirstPush:{
		name: "puzzlePostNotFirstPush",
		currentStep: 0,
		phraseID: null,
		steps: [
			{text: "Yes! You've unlocked The Puzzle Post.\nYou'll get the latest puzzles for free every week.\nYou can find the Puzzle Post in the main menu.", button: "_next", word: null, position: "center"},
			{text: "Complete a Puzzles Post to get rewards such as SKIPS and HINTS.", button: "_next", word: null, position: "center"},
			{text: "We'll send One notification per week when the Puzzle Post releases. To turn off notifications go to Settings in the main menu.", button: "_next", word: null, position: "center"},
			{text: "Enjoy! Bye!", button: "_next", word: null, position: "center"}
		]
	},
	puzzlePostOpened:{
		name: "puzzlePostOpened",
		currentStep: 0,
		phraseID: null,
		steps: [
			{text: "The Puzzle Post has at least one free issue every week.", button: "_next", word: null, position: "center"},
			{text: "Complete a Puzzles Post to get rewards such as SKIPS and HINTS.\nEnjoy!", button: "_next", word: null, position: "center"},
		]
	},
	pushNotificationsFirst:{
		name: "pushNotificationsFirst",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "Thanks for registering.\nWe'd like to notify you when someone solves a puzzle that you have composed. If you want to turn off notifications go to Settings in the main menu.", button: "_next", word: null, position: "center"},
			{text: "Press OK on the next popup.", button: "_next", word: null, position: "center"}
		]
	},
	pushNotificationsNotFirst:{
		name: "pushNotificationsNotFirst",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "We'll notify you when someone solves a puzzle that you have composed. If you want to turn off notifications go to Settings in the main menu.", button: "_next", word: null, position: "center"},
		]
	},
	puzzleBookOpened:{
		name: "puzzleBookOpened",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "The Puzzle Book is where you'll find all the puzzles you've solved.", button: "_next", word: null, position: "center"},
			{text: "There are two tabs: Favorites & All.\n\nTo Favorite a Puzzle click on the Heart when you solve it.", button: "_next", word: null, position: "center"},
			{text: "From here you can tap on any puzzle to play it again or to share it with your friends.", button: "_next", word: null, position: "center"},
		]
	},
	mailboxOpened:{
		name: "mailboxOpened",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "The Mailbox is where you communicate with your friends.\n\nThere are two tabs: Inbox and Write Puzzle. It's easy!", button: "_next", word: null, position: "center"}
		]
	},
	writePuzzleTabOpened:{
		name: "writePuzzleTabOpened",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "Come to this tab to write a new puzzle and find the ones you've written.", button: "_next", word: null, position: "center"}
		]
	},
	composeOpened:{
		name: "composeOpened",
		currentStep: 0,
		phraseID: null,
		steps:[
			{text: "Composing puzzles is easy. There are a few rules, but don't worry. If there are any problems we'll warn you.", button: "_next", word: null, position: "top"},
			{text: "Rules:\n- Characters not allowed will be deleted\n- Min 3 max 19 words and 90 characters\n- Phrase must end with . or ? or !", button: "_next", word: null, position: "top"},
			{text: "We can't wait to see what great puzzles you create. Enjoy and send and share lots of puzzles with your friends!", button: "_next", word: null, position: "top"}
		]
	},
};


exports.CheckIfButtonEnabled = function(buttonID){
	var returnValue = true;
	if (globals.tutorialOn == true && globals.currentTutorial != null){
		if (!buttonID || buttonID != globals.currentTutorial.currentButtonID) returnValue = false;
		if (globals.tutorialButtonReady == false) returnValue = false;
		if (globals.currentTutorial.name == "firstStart" && (buttonID == "gameToolBarClearButton" || buttonID == "gameToolBarUndoButton") &&  globals.currentTutorial.currentWordText == "_any") returnValue = true;
	}
	
	return returnValue;
};


exports.CheckIfCorrectWord = function(text){
	var returnValue = true;
	
	if (globals.tutorialOn == true && globals.currentTutorial != null){
		if (text == globals.currentTutorial.currentWordText || globals.currentTutorial.currentWordText == "_any") returnValue = true;
		else returnValue = false;
		if (globals.tutorialButtonReady == false) returnValue = false;
	}
	
	return returnValue;
};


exports.CreateTutorialController = function(name){
	var tutorialController = _.find(tutorials, function(t){return t.name == name;});
	tutorialController.currentStep = -1;
	
	
	function NextStep(){
		var returnValue;
		tutorialController.currentStep++;
		var i = tutorialController.currentStep;
		if (tutorialController.currentStep < tutorialController.steps.length){
			tutorialController.currentWordText = tutorialController.steps[i].word;
			tutorialController.currentButtonID = tutorialController.steps[i].button;
			tutorialController.currentText = tutorialController.steps[i].text;
			tutorialController.currentPosition = tutorialController.steps[i].position;
			returnValue = "update";
			
			for (var i = 0; i < globals.buttonsArray.length; i++){
				var button = globals.buttonsArray[i];
				if (button.buttonID == tutorialController.currentButtonID) button.StartGlow();
				else button.StopGlow();
			}
			
			Ti.App.fireEvent("tutorialUpdateRandomWordDisplay", {currentWordText: tutorialController.currentWordText});
		}
		else{
			//Tutorial is finished notify UI
			tutorialController.currentStep = 0;
			returnValue = "complete";
			if (tutorialController.name == "firstStart"){
				globals.gameData.firstStartComplete = true;
				globals.col.gameData.commit();
			}
			
			for (var i = 0; i < globals.buttonsArray.length; i++){
				var button = globals.buttonsArray[i];
				button.StopGlow();
			}
		}
		
		return returnValue;
	}
	
	tutorialController.NextStep = NextStep;
	
	NextStep();
	
	return tutorialController;
};
