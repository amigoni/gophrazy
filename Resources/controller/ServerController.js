var AnalyticsController = require("/controller/AnalyticsController");

function Create(){
	var RTS = require("lib/RTS");
	var object = {};
	
	function Register(userObject){
		var now = clock.getNow();
		if (Titanium.Network.online == false){
			setTimeout(function(){Ti.App.fireEvent("NoConnection");},1000);
		}
		else{
			RTS.queryItems("User", {primary: userObject.userName},null,null,null,null,false, 
				function(e){
					if(e.text != null){
						var result = JSON.parse(e.text);
						Ti.API.info("SERVERCONTROLLER:QUERYUSER");
						Ti.API.info(result);
						if(result.error == null && result != null){
							if(result.data.items.length == 0) ActuallyRegisterUser();
							else {
								Ti.App.fireEvent("closeButtonBlocker");
								Ti.App.fireEvent("UserAuthentication", {state: "UserNameTaken"});
							}	
						}
						else {
							AnalyticsController.RegisterEvent({category:"error", area:"ServerController:Register:queryUser", message: result.error, severity:"error"});
								
							Ti.App.fireEvent("closeButtonBlocker");
							Ti.API.info("SEVERCONTROLLER:QUERYUSER:ERROR");
						}	
					}
					else{
						AnalyticsController.RegisterEvent({category:"error", area:"ServerController:Register:queryUser", message:"no e.text", severity:"error"});
									
						Ti.App.fireEvent("closeButtonBlocker");
						Ti.App.fireEvent("ErrorSentMessage");
					}
				}
			);
			
			//Check the user name if it doesn't exist register it, otherwise tell them to change it.
			function ActuallyRegisterUser (){
				var userFacebookID = globals.gameData.userFacebookID;
				if(userFacebookID == "") userFacebookID = "none";
				userObject.userFacebookID = userFacebookID;
				userObject.passWord = crypt.encrypt(userObject.passWord);
				var objectToPut = userObject;
				
				RTS.putItem("User", objectToPut, 
					function(e){
						Ti.API.info("SERVERCONTROLLER:ACTUALLYREGISTERUSER:ATTEMPTING"+userObject.userName);
						if(e.text != null){
							var response = JSON.parse(e.text);
							setTimeout(function(){
								if (response.error == null){
									globals.gameData.userName = userObject.userName;
									globals.gameData.passWord = userObject.passWord;
									if(userObject.email!= null) globals.gameData.email = userObject.email;
									globals.col.gameData.commit();
									Ti.API.info("SERVERCONTROLLER:ACTUALLYREGISTERUSER:SUCCESS:"+userObject.userName);
									Ti.App.fireEvent("UserAuthentication", {state: "Registered"});
									var PushNotificationsController = require("/controller/PushNotificationsController");
									PushNotificationsController.CheckForPushNotificationsTutorialCompletion();
								}
								else {
									AnalyticsController.RegisterEvent({category:"error", area:"ServerController:ActuallyRegisterUser", message:response.error, severity:"critical"});
									Ti.API.info("SERVERCONTROLLER:ACTUALLYREGISTERUSER:ERROR:"+userObject.userName);
									Ti.App.fireEvent("ErrorSentMessage");
								}	
							},1000);
						}
						else{
							AnalyticsController.RegisterEvent({category:"error", area:"ServerController:ActuallyRegisterUser", message:"no e.text", severity:"critical"});	
							Ti.API.info("SERVERCONTROLLER:ACTUALLYREGISTERUSER:ERROR:"+userObject.userName);
						}
						Ti.App.fireEvent("closeButtonBlocker");
					}
				);
			}
			
			//Send notifications to friends
		}
	};
	
	
	function Login(userObject){
		var now = clock.getNow();
		if (Titanium.Network.online == false){
			setTimeout(function(){Ti.App.fireEvent("NoConnection");},1000);
		}
		else{
			RTS.queryItems("User", {primary: userObject.userName},null,null,null,null,false, 
				function(e){
					if(e.text != null){
						var result = JSON.parse(e.text);
						Ti.API.info("SERVERCONTROLLER:QUERYUSER");
	
						if(result.error == null && result != null){
							if(result.data.items.length == 0) {
								alert("Sorry but we can't find that username.");
							}
							else {
								//alert("Check Password Match?");
								if(userObject.passWord == crypt.decrypt(result.data.items[0].passWord)){
									globals.gameData.userName = userObject.userName;
									globals.gameData.passWord = userObject.passWord;
									if(userObject.email!= null) globals.gameData.email = userObject.email;
									globals.col.gameData.commit();
									Ti.API.info("SERVERCONTROLLER:ACTUALLYREGISTERUSER:SUCCESS:"+userObject.userName);
									Ti.App.fireEvent("UserAuthentication", {state: "LoggedIn"});
									var PushNotificationsController = require("/controller/PushNotificationsController");
									PushNotificationsController.CheckForPushNotificationsTutorialCompletion();
								}
								else alert("Sorry but the password doesn't match!");
							}
						}
						else {
							AnalyticsController.RegisterEvent({category:"error", area:"ServerController:Login:queryUser", message: result.error, severity:"critical"});						
							Ti.API.info("SEVERCONTROLLER:QUERYUSER:ERROR");
						}	
					}
					else {
						Ti.App.fireEvent("ErrorSentMessage");
						AnalyticsController.RegisterEvent({category:"error", area:"ServerController:Login:queryUser", message:"no e.text", severity:"critical"});	
					}	
					Ti.App.fireEvent("closeButtonBlocker");
				}
			);
		}
	}
	
	function ResetPassword(email){
		alert("To Implement maybe by email for now");
		/*
		RTS.queryItems("User", {primary: userObject.userName},null,null,null,null,false, 
			function(e){
				if(e.text != null){
					var result = JSON.parse(e.text);
					Ti.API.info("SERVERCONTROLLER:QUERYUSER");

					if(result.error == null && result != null){
						if(result.data.items.length == 0) {
							alert("We couldn't find any users with that email");
						}
						else {
							alert("TO IMPLEMENT Reset Password");
						}
					}
					else {
						Ti.API.info("SEVERCONTROLLER:QUERYUSER:ERROR");
					}	
				}
				else Ti.App.fireEvent("ErrorSentMessage");
			}
		);
		*/
		Ti.App.fireEvent("closeButtonBlocker");
		
	};
	
	object.Register = Register;
	object.Login = Login;
	
	return object;
}

module.exports = Create;