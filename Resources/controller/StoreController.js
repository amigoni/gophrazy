var Common = require('ui/Common');//Here to fix sometimes it doesn't find it, it seems

function Create(){
	var InAppProducts = require('com.logicallabs.inappproducts');
	var storeProducts = [];//Products from the module not the local db ones
	var lastPurchasePayload = GenerateGUID();//Put an ID just in case. This is used by android to verify the purchase
	
	var quella = KeyGen();
	//Prevent some hacking;
	function KeyGen(){
		var one = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoPNkJzHqbY/6mAjJwb4zUbOiOjvmg3b8fvydYdGXdv04r6vzgn/FD5NPJM7";
		var two = "bojAxi6sZ8vV+fYVIQey6HnrLSsdU/QXhT3p22a+kB4ym8SbKsOy2fWqL950nZCPYW/DC9txHy+ceFuKMAarFWAMJRe+MaVIbDIAAi8tMNjZ204GkmqveyAeA6JppzthAuiX69H8Zb3Hbs49CHNwLnSpKz5HBTfcg";
		var three = "WqHkar2HlEFccvWC++Kq47MIkEcKScS/oneDb/TiL5ClOas1gMxfwiVtkFI6zNxxJOJDSTlY66oHCVCfTruk2pQbtOtwJEGrOwq6B536QL/EkeEKMgiqlpZJbQIDAQAB";
		return one+two+three;
	}


	function Init(callback){
		var AnalyticsController = require("/controller/AnalyticsController");
		// This call (or any other) may fail on Android if the module hasn't finished
		// its initialization routine yet -- always wait for the stateChange event!!!
		Ti.API.info('STORE CONTROLLER:Module ready? ' + (InAppProducts.getSupportStatus() !== InAppProducts.SUPPORT_STATUS_ERROR));
		
		// Note: These product IDs must match the product IDs you configure on
		// iTunes Connect and Android Developer Console!
		var productIDs = [];
		var localStoreItems = globals.col.storeItems.getAll();
		if(localStoreItems.length == 0) {
			InitLocalStoreItems();
			localStoreItems =  globals.col.storeItems.getAll();
		}	
		for(var i = 0; i < localStoreItems.length; i++){
			productIDs.push(localStoreItems[i].itemID);
		}
		
		var lastPurchasingState = "";
		var lastStateTime = clock.getNow().valueOf();
		InAppProducts.addEventListener('purchaseUpdate', function(e) {
			var now = clock.getNow().valueOf();
			var currentState = purchaseStateToString(e.purchase.state);
			Ti.API.info("STORECONTROLLER:Current state: "+currentState);
			Ti.API.info("STORECONTROLLER:Last state: "+lastPurchasingState);
			Ti.API.info("STORECONTROLLER:Current time: "+now);
			Ti.API.info("STORECONTROLLER:Last time: "+lastStateTime);
			Ti.API.info("STORECONTROLLER:Time Delta: "+(now-lastStateTime));

			if (e.errorCode) {
				// This only happens on Android. On iOS, there is no error
				// condition associated with the purchaseUpdate event, although
				// the purchase itself may be in PURCHASE_STATE_FAILED state.
				Ti.API.info('STORE CONTROLLER:Purchase attempt failed (code: ' + e.errorCode + ')');
				alert("The Purchase Failed for some reason.\nPlease try again.");
				Ti.App.fireEvent("closeButtonBlocker");
			} else {
				//This if is here to avoid double calls
				if(currentState != lastPurchasingState || (currentState == lastPurchasingState && now - 1000 > lastStateTime )){
					Ti.API.info("STORE CONTROLLER:PROCESSING STATE");
					Ti.API.info('STORE CONTROLLER:Product: ' + e.purchase.SKU + ' state: ' +currentState);
				
					switch (e.purchase.state) {
						case InAppProducts.PURCHASE_STATE_PURCHASED:
							e.purchase.complete();
						    //Save Receipt in case verifcation crashes
							if(Ti.Platform.name == "iPhone OS"){
								SaveReceipt(Ti.Utils.base64encode(e.purchase.receipt).getText(), e.purchase.SKU);
								var lastReceipt = globals.col.receipts.find({receipt:e.purchase.receipt})[0];
								VerifyReceipt(lastReceipt, false);
							}
							else{
								if(e.purchase.SKU == "hint_1" || e.purchase.SKU == "pass_1" || e.purchase.SKU == "skip_1"){
									Ti.API.info("STORE CONTROLLER:CONSUMED");
									e.purchase.consume();
								} 
								
								var purchaseData = {
									orderID: e.purchase.orderNumber,
									packageName: "com.mozzarello.gophrazy",
									productID: e.purchase.SKU,
									purchaseTime: new moment(e.purchase.time).valueOf(),
									purchaseState: e.purchase.state,
									developerPayload: e.purchase.applicationPayload,
									purchaseToken: e.purchase.purchaseToken
								};
								
								if(purchaseData.developerPayload == lastPurchasePayload){
									Ti.API.info("STORE CONTROLLER: Payload Matches, Gonna Verify with Server");
									var fakeReceipt ={
											purchaseData : JSON.stringify(purchaseData),
											signature: e.purchase.signature,
											publicKey: quella,
											SKU: e.purchase.SKU
									};
									
									VerifyReceipt(fakeReceipt, false);
								}
								else{
									Ti.API.info("STORE CONTROLLER: Payload does not match!");
									AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:purchaseUpdate:purchasePurchased", message:"payloadNoMatch", severity:"error"});
									alert("The Purchase Failed for some reason,\nPlease try again.");
								}
								
							}
							// This is a possible state on both iOS and Android
							break;
						case InAppProducts.PURCHASE_STATE_CANCELED:
							// Android only
							alert('The Purchase was Canceled.');
							Ti.App.fireEvent("closeButtonBlocker");
							break;
						case InAppProducts.PURCHASE_STATE_REFUNDED:
							// Android only
							break;
						case InAppProducts.PURCHASE_STATE_PURCHASING:
							// iOS only
							break;
						case InAppProducts.PURCHASE_STATE_FAILED:
							// iOS only
							//AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:purchaseUpdate:purchaseFailed", message:"purchaseFailed", severity:"error"});
							alert('You Purchase failed or was cancelled.\nPlease try again.');
							Ti.App.fireEvent("closeButtonBlocker");
							break;
						case InAppProducts.PURCHASE_STATE_RESTORED:
							// iOS only
							e.purchase.complete();
							var fakeReceipt = {
								receipt: e.purchase.receipt,
								SKU: e.purchase.SKU
							};
							VerifyReceipt(fakeReceipt, false);
							break;
					}
					
					if (InAppProducts.autoCompletePurchases === false) {
						// This is for iOS only; autoCompletePurchases is constant
						// true on Android as there is no need/ability to separately
						// complete purchases; they are essentially always
						// auto-completed.
						switch (e.purchase.state) {
							case InAppProducts.PURCHASE_STATE_PURCHASED:
							
							case InAppProducts.PURCHASE_STATE_FAILED:
								e.purchase.complete();
							case InAppProducts.PURCHASE_STATE_RESTORED:
								e.purchase.complete();
						}
					}
				}
				
				lastPurchasingState = currentState;
				lastStateTime = now;
			}
		});
		
		
		
		InAppProducts.addEventListener('receivedProducts', function(e) {
			if (e.errorCode) {
				Ti.API.info("STORE CONTROLLER:InAppProducts:receivedProducts:"+e.errorMessage);
				AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:receiveProducts:"+e.errorCode, message: "Error Receiving Products", severity:"error"});
			} else {	
				if(e && e.products){
					storeProducts = e.products;
					
					globals.storeController.products = storeProducts;
					for(var i = 0; i < e.products.length; i++){
						var product = e.products[i];
						//Ti.API.info("STORE CONTROLLER: "+product.SKU+"/"+product.title+"/"+product.description+"/"+product.priceAsNumber+"/"+product.priceAsString+"/"+product.priceLocale);
					}
					UpdateLocalStoreItems(e.products);
					
					if(callback) callback();
					
					///Check Unverified Receipt
					var receipts = globals.col.receipts.getAll();
					if (receipts.length > 0) {
						//alert("There are some undelivered purchases, we are gonna check if they need to be delivered");
						for (var i = 0; i < receipts.length; i++){
							VerifyReceipt(receipts[i], true);
						}
					}	
				}
			}
		});
		
		
		
		//This usually used to restore purchases.
		InAppProducts.addEventListener('receivedPurchases', function(e) {
			// We may have received partial information even in case of an error...
			Ti.API.info('STORE CONTROLLER:RESTORING PURCHASES:Received ' + e.purchases.length + ' purchase records.');
			
			if (e.errorCode !== undefined) {
				Ti.API.info("STORE CONTROLLER:InAppProducts:receivedProducts:"+e.errorMessage);
				AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:receivePurchases:"+e.errorCode, message: "Error Receiving Purchases", severity:"error"});
				alert("There was an error getting your purchases.\nPlease try again later.");
				Ti.App.fireEvent("closeButtonBlocker");
			}
			else{
				for(var i = 0; i < e.purchases.length; i++){
					var SKU = e.purchases[i].SKU;
					var product = FindProductBySKU(SKU, true); //Use the StoreKit product to purchase
					BuyLocalItem(product, false);
					Ti.API.info("STORE CONTROLLER:Restored Purchase:"+SKU);
					Ti.API.info(e.purchases[i]);
				}
				
				alert("Your Purchases were restored.");
			}
		});
		
		
		
		InAppProducts.addEventListener('stateChange', function(e) {
			Ti.API.info('STORE CONTROLLER:InAppProducts:stateChange: Received stateChange event with state ' + e.state);
			switch(InAppProducts.state) {
				case InAppProducts.STATE_NOT_READY:
					Ti.API.info('STORE CONTROLLER:InAppProducts:stateChange: module is not ready!');
					if (e.errorCode) {
						Ti.API.info('STORE CONTROLLER:InAppProducts:stateChange: Initialization error code: ' + e.errorCode);
					}
					if (e.errorMessage) {
						Ti.API.info('STORE CONTROLLER:InAppProducts:stateChange: Initialization error msg: ' + e.errorMessage);
					}
					AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:stateChange:"+e.errorCode, message: "Cannot connect to Store", severity:"error"});
					break;
				case InAppProducts.STATE_READY:
					InAppProducts.getProducts({SKUs: productIDs});
					Ti.API.info('STORE CONTROLLER:InAppProducts module is ready!');
					break;
				case InAppProducts.STATE_NOT_SUPPORTED:
					Ti.API.info('STORE CONTROLLER:InAppProducts:stateChange: Simulator not supported! ' + 
						'Please run this test on an actual device!');
					break;
				default:
					Ti.API.info('STORE CONTROLLER:InAppProducts:stateChange: module is in invalid state!');
			}
		});
		
		
		
		var purchaseStateToString = function(state) {
			switch (state) {
				case InAppProducts.PURCHASE_STATE_PURCHASED:
					return 'purchased';
				case InAppProducts.PURCHASE_STATE_CANCELED:
					// Android only
					return 'canceled';
				case InAppProducts.PURCHASE_STATE_REFUNDED:
					// Android only
					return 'refunded';
				case InAppProducts.PURCHASE_STATE_PURCHASING:
					// iOS only
					return "purchasing";
				case InAppProducts.PURCHASE_STATE_FAILED:
					// iOS only
					return "failed";
				case InAppProducts.PURCHASE_STATE_RESTORED:
					// iOS only
					return "restored";
				default:
					return 'unknown';
			}
		};
	};



	function BuyStoreKitItem(SKU, quantity){
		if(quantity == null) quantity = 1;
		var product = FindProductBySKU(SKU, false); //Use there StoreKit product to purchase
		if (Ti.Platform.model != "Simulator"){
			if(globals.testing == false) {
				//UNIVERSAL: SHOULD HAVE AN EVENT FIRE HERE
				Common.CreateButtonBlocker(true,true);
				var text = "Contacting Apple";
				if(Ti.Platform.name == "android") text = "Contacting Google";
				Ti.App.fireEvent("changeButtonBlocker", {text: text});
				
				if(Ti.Platform.name == "iPhone OS") product.purchase({quantity : quantity});
				else {
					lastPurchasePayload = GenerateGUID();
					product.purchase({applicationPayload: lastPurchasePayload ,quantity : quantity});
				}	
			}	
			else BuyLocalItem(product, false);
		}
		else BuyLocalItem(product, false);
	};



	function BuyLocalItem(product, isReal){
		var SKU = product.SKU;
		var AnalyticsController = require("controller/AnalyticsController");
		var itemData = product;
		var gameData = globals.gameData;
		var totalCount = 1;
		
		if(itemData.itemID == "hint_1"){
			gameData.hints += itemData.quantity;
			globals.gameData.hintsBoughtCount += itemData.quantity;
			globals.col.gameData.commit();
			Ti.App.fireEvent("updateHints", {amount:gameData.hints});
			Ti.App.fireEvent("EnableHint");
			totalCount = globals.gameData.hintsBoughtCount;
		}
		else if(itemData.itemID == "pass_1"){
			gameData.passData.passes++;
			gameData.hints++;
			gameData.skips++;
			gameData.passData.passBoughtCount++;
			gameData.passData.currentlyOnBoughtPass = true;
			gameData.passData.currentPassType = "bought";
			globals.gameData.passData.nextPassAt += globals.gameData.passData.curveBought;
			globals.col.gameData.commit();
			Ti.App.fireEvent("boughtPass");
			totalCount = gameData.passData.passBoughtCount;
		}
		else if(itemData.itemID == "skip_1"){
			gameData.skips += itemData.quantity;
			globals.gameData.skipsBoughtCount += itemData.quantity;
			globals.col.gameData.commit();
			Ti.App.fireEvent("updateSkips", {amount: gameData.skips});
			Ti.App.fireEvent("EnableSkip");
			totalCount = globals.gameData.skipsBoughtCount;
		}
		else if(itemData.itemID == "compose_puzzle_early_access"){
			gameData.abilities.composePuzzle.enabled = true;
			globals.col.gameData.commit();
			Ti.App.fireEvent("EnableComposePuzzle");
			totalCount = 1;
		}
		else if (itemData.itemID == "skipbox_upgrade_1"){
			if(gameData.skipboxLimit != globals.gameData.skipboxUpgradeLimit){
				gameData.skipboxLimit = globals.gameData.skipboxUpgradeLimit;
				globals.col.gameData.commit();
				
				var skipboxGroup = globals.col.groups.find({type: "skipbox"})[0];
				skipboxGroup.phrasesLimit = globals.gameData.skipboxUpgradeLimit;
				globals.col.groups.commit();
				Ti.App.fireEvent("updateskipbox", {amount: globals.col.groups.find({type:"skipbox"})[0].phrasesIDs.length});
				if(globals.gameData.skips == 0) BuyLocalItem(FindProductBySKU("skip_1",true), false);
				totalCount = 1;
			}
		}
		
		if(isReal == true){
			Ti.API.info("STORE CONTROLLER: BUY EVENT");
			var amount = Math.ceil(itemData.priceAsNumber*100);
			AnalyticsController.RegisterEvent({category: "business", event_id:"PurchasedItem:"+itemData.itemID, amount: amount, currency: itemData.currency});
		}
	};



	function RestorePurchases(){
		InAppProducts.getPurchases();
	};



	function VerifyReceipt(receipt, SKU, missedVerification){
		//receiptObject is very different between iOS and Android
		
		var AnalyticsController = require("/controller/AnalyticsController");
		Ti.App.fireEvent("changeButtonBlocker", {text:"Verifying Receipt"});
		var receiptObject = {};
		if(Ti.Platform.name == "iPhone OS"){
			var encodedReceipt;
			if(missedVerification == false){
				encodedReceipt = Ti.Utils.base64encode(receipt).getText(); 
			}
			else {
				receipt.checkCount++;
				encodedReceipt = receipt.receipt; //Saved receipt is already text format
			}	
			receiptObject["receipt-data"] = encodedReceipt;
			receiptObject.store = "Apple";
			CallServer(JSON.stringify(receiptObject), SKU);	
		}
		else{
			receiptObject = receipt.receipt;
			receiptObject.store = "Google";
			Ti.API.info(receiptObject);
			CallServer(JSON.stringify(receiptObject), SKU);	
			//CheckVerificationResponse({});
		}
		
		
		function CallServer(receiptObjectToSend, SKU){
			//Ti.API.info(receiptObjectToSend);
			Ti.API.info("STORE CONTROLLER:RECEIPT VERIFICATION: Calling Server");
			Ti.App.fireEvent("verifyingReceipt");
			
			var applicationID = "YbTxpz58ZgYSEw13rudnxA3nmROvajQ3nYPnWByk";
			var restAPIKey = "4G94GO2VnHuktxnyL538ISPhnPaOvtAoWbIotTkN";
			
			//Development
			//var url = "https://api.parse.com/1/functions/checkReceiptSandbox";
			
			//Production
			var url = "https://api.parse.com/1/functions/checkReceipt";
			
			var client = Ti.Network.createHTTPClient({	
			     onload : function(e) {
			     	Ti.API.info("VerifyReceipt Success: Call Status Code: "+this.status+" "+this.statusText);
			     	CheckVerificationResponse(client.responseText);
			     },
			     onerror : function(e) {
			     	Ti.API.info("STORE CONTROLLER:VerifyReceipt Error:Call Status Code: "+this.status+" "+this.statusText);
			     	//Need to look at error if 155 need to send a notification to myself. 
			    	if(this.status == 155 || this.status == 140){
			    		//Send me notification, reached the limit.
			    		AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:verifyReceipt:limitReached-"+this.status+" "+this.errorMessage, message:"Error in Verification Server", severity:"critical"});				
			    	}
			    	else AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:verifyReceipt:"+this.status+" "+this.errorMessage, message:"Error in Verification Server", severity:"error"});				
			    	AskToRetryToVerify();
			     },
			     onreadystatechange: function(e){
			     	//Ti.API.info("Ready State "+this.readyState+" Status Code: "+this.status);
			     },
			     timeout : 10000
			});
			
			client.open("POST", url);
			client.setRequestHeader("X-Parse-Application-Id", applicationID ); 
			client.setRequestHeader("X-Parse-REST-API-Key", restAPIKey); 
			client.setRequestHeader("Content-Type", "text/plain"); 
			client.send(receiptObjectToSend);
		}
		
		
		function CheckVerificationResponse(responseText){	
			Ti.API.info("STORE CONTROLLER:Verification Response:"+responseText);
			var responseObject = JSON.parse(responseText);
			
			if(Ti.Platform.name == "iPhone OS"){
				if(responseObject.result) responseObject = JSON.parse(responseObject.result);
				if (responseObject.status == 0){
			 		ClearReceipt(receipt.receipt);
			 		var product = FindProductBySKU(receipt.SKU, true);
			 		BuyLocalItem(product, true);
			 		Ti.App.fireEvent("changeButtonBlocker", {text:"Yey! Receipt Verified\nAll done!"});
			 		setTimeout(function(){Ti.App.fireEvent("closeButtonBlocker");},2500);
			 		if(missedVerification == true) alert("Yey! Receipt Verified\nAll done! "+product.name);
			 		
					Ti.API.info('STORE CONTROLLER:Purchased '+receipt.SKU);
			 	}
			 	//1 Bad Receipt
			 	else if(responseObject.status == 21000 || responseObject.status == 21002 || responseObject.status == 21003 || responseObject.status == 21004 || responseObject.status == 21006 || responseObject.status == 21007 || responseObject.status == 21008){
			 		ClearReceipt(receipt.receipt);
			 		AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:receiptVerification:badReceipt:"+responseObject.status+" "+responseText, message:"Bad Receipt", severity:"critical"});									
			 		alert("Mmm. Your receipt seems bad.\nTry to Restore Purchases from Settings or contact Apple for a refund.");
			 		Ti.App.fireEvent("closeButtonBlocker");
			 	}
			 	//2 Apple Server down
			 	else if(responseObject.status == 21005){
			 		//alert("Sorry there was an error connecting to  verification.\nPress OK and we'll try again");
			 		AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:receiptVerification:appleServerError:"+responseObject.status+" "+responseText, message:"Apple Server Down", severity:"critical"});				
			 		AskToRetryToVerify();
			 	}
			 	//Either my server is down or can't connect. CHECK THIS CASE. NEED TO NOTIFY MYSELF IF THE SERVER IS DOWN.
				else{ 
					if(missedVerification == true && receipt.checkCount >= 3) {
						ClearReceipt(receipt.receipt);
						AnalyticsController.RegisterEvent({category:"error", area:"inAppProducts:receiptVerification:unknownError:"+responseObject.status+" "+responseText, message:"Other Verification error last case.", severity:"critical"});				
						alert("Mmm. We can't seem to verify the receipt.\nTry to Restore Purchases from Settings or contact Apple for a refund.");
					}
					else AskToRetryToVerify();
				}
			}
			else{
				if(responseObject.result == "success"){
					var product = FindProductBySKU(receipt.SKU, true);
					BuyLocalItem(product, true);
					Ti.App.fireEvent("changeButtonBlocker", {text:"Receipt Verified\nAll done!"});
			 		setTimeout(function(){Ti.App.fireEvent("closeButtonBlocker");},2500);
			 		if(missedVerification == true) alert("Verified Receipt, and delivered "+product.name);
			 		
					Ti.API.info('STORE CONTROLLER:Purchased '+receipt.SKU);	
				}
				else{
					alert("Mmm. Your receipt seems bad.\nIf you really bought this, please contact us.");
			 		Ti.App.fireEvent("closeButtonBlocker");
				}
			}	
		}
		
		
		function AskToRetryToVerify(){
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Yes', 'No'],
			    message: 'There was an error Verifying Receipt. Try again?',
			    title: 'Error'
			});
			
			dialog.addEventListener('click', function(e1){
				if(e1.index == 0){
					VerifyReceipt(receipt, missedVerification);
				} 
				else Ti.App.fireEvent("closeButtonBlocker");
			});
			
			dialog.show();
		}
	}



	function FindProductBySKU(SKU, isLocal){
		//Local products are there because products are not available if it's not the same time as the purchase.
		//so in case the verification failed I need a persistant reference to it.
		var returnProduct;
		var products = isLocal == true ? globals.col.storeItems.getAll() : storeProducts;	
		if (Ti.Platform.model == "Simulator") products = globals.col.storeItems.getAll();
		if (globals.testing == true) products = globals.col.storeItems.getAll();
	
		for (var i = 0; i < products.length; i++){
			var product = products[i];
			if(product.SKU == SKU) {
				returnProduct = product;
				break;
			}
		}	
		
		return returnProduct;
	};



	function GetStoreDataByType(type){
		//Need to synch price here with app store.
		return globals.col.storeItems.find({type:type});
	};



	function SaveReceipt(receipt, SKU){
		globals.col.receipts.save({receipt: receipt, SKU:SKU, checkCount: 0});
		globals.col.receipts.commit();
		Ti.API.info("STORE CONTROLLER:Saved Receipt");
	};
	
	
	
	function ClearReceipt(receipt){
		if(globals.col.receipts.find({receipt:receipt}).length == 0) alert("Can't find Receipt");
		else{
			globals.col.receipts.remove({receipt:receipt});
			globals.col.receipts.commit();
			Ti.API.info("STORE CONTROLLER:Cleared Receipt");
		}
	};


	//Need this in case receipt verification failed and we are trying at a later time. The purchase object at that time,
	//is not available anymore and the object is strange as the properties don't seem to be querable
	function InitLocalStoreItems(){
		//var storeItems = JSON.parse(Common.GetJsonText("/model/StoreData.txt"));
		var storeItems = globals.gameData.storeData;
		
		for (var i = 0; i < storeItems.length; i++){
			var data = storeItems[i];
			globals.col.storeItems.save({
				itemID: data.itemID,
				SKU: data.itemID,
				name: data.name,
				type: data.type,
				icon: data.icon,
				quantity: data.quantity,
				price: data.price,
				isBestDeal: data.isBestDeal,
				isMostPopular: data.isMostPopular
			});
		}
		
		globals.col.storeItems.commit();
	};



	function UpdateLocalStoreItems(){
		var localStoreItems = globals.col.storeItems.getAll();
		var products = storeProducts;
	
		for(var i = 0; i < localStoreItems.length; i++){
			var localStoreItem = localStoreItems[i];
			for(var j = 0; j < products.length; j++){
				var product = products[j];
				if(localStoreItem.SKU == product.SKU){
					localStoreItem.title = product.title; 
					localStoreItem.description = product.description; 
					localStoreItem.priceAsString = product.priceAsString;
					
					if(Ti.Platform.name == "iPhone OS"){
						var currency = product.priceLocale.substring(product.priceLocale.indexOf('=')+1, product.priceLocale.length);
						localStoreItem.priceAsNumber = product.priceAsNumber;
						localStoreItem.priceLocale = product.priceLocale;
						localStoreItem.currency = currency;
					}
					else{
						localStoreItem.priceAsNumber = product.priceAsString.slice(1, product.priceAsString.length-1);
						localStoreItem.priceLocale = product.priceAsString[0]; //This is not the locale but the symbol 
						localStoreItem.currency =  product.priceAsString[0]; //Just the symbol
					}
					
				}
			}
			//Ti.API.info(localStoreItem);
		}
		
		globals.col.storeItems.commit();
	};

	function GenerateGUID(){
	    //Got this on Stack overflow
	    var d = new Date().getTime();
	    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	        var r = (d + Math.random()*16)%16 | 0;
	        d = Math.floor(d/16);
	        return (c=='x' ? r : (r&0x7|0x8)).toString(16);
	    });
	    return uuid;
	};

	this.products = storeProducts;
	this.Init = Init;
	this.FindProductBySKU = FindProductBySKU;
	this.InitLocalStoreItems = InitLocalStoreItems;
	this.GetStoreDataByType = GetStoreDataByType;
	this.RestorePurchases = RestorePurchases;
	this.BuyStoreKitItem = BuyStoreKitItem;
	this.VerifyReceipt = VerifyReceipt;
};


function Fake(){
	var storeProducts = [];
	
	function Init(){}
	function FindProductBySKU(){}
	function InitLocalStoreItems(){}
	function GetStoreDataByType(){}
	function RestorePurchases(){}
	function BuyStoreKitItem(){}
	
	this.Init = Init;
	this.FindProductBySKU = FindProductBySKU;
	this.InitLocalStoreItems = InitLocalStoreItems;
	this.GetStoreDataByType = GetStoreDataByType;
	this.RestorePurchases = RestorePurchases;
	this.BuyStoreKitItem = BuyStoreKitItem;
	this.products = storeProducts;
}

module.exports = Create;
