// this sets the background color of the master UIView (when there are no windows/tab groups on it)
var self = Titanium.UI.createWindow({
	background: "#000"
});

var admob = require("metal.admobgoogleplay");

var baseView = Titanium.UI.createView({
	background: "#000"
});


var topId = Titanium.UI.createTextField({
    hintText : 'Type your ad unit id or use default',
    top : "15%",
    left: 10, 
    right: 10, 
    height : 35
});


var topButton = Titanium.UI.createButton({
	top:"20%",
	title: 'Top Ad',
   	width: "35%",
   	left: 10,
   	height: 40
});

var topLabel = Titanium.UI.createLabel({
	value: "",
	color:"#e3e3e3",
	top: "20%",
	left: "40%",
	height: 40
});

topButton.addEventListener('click', function(e) {
	var publishId = "ca-app-pub-5795209374815138/9811324006";
	if (topId.value !== undefined && topId.value.length !== 0) 
		 publishId = topId.value;
	
	
	Ti.API.info("publishId: "+publishId);
	
	var topAdv = admob.createView({
		top: 0,
		adSizeType: 'SMART',
		publisherId: publishId,
		testing: false
	});	
	self.add(topAdv);	
	topLabel.text = 'Requesting smart top banner ads...';
	
	topAdv.addEventListener('ad_received', function(e) {
		topLabel.text = "Ads received";
	});
	
	topAdv.addEventListener('ad_not_received', function(e) {
		topLabel.text = "Fail to load ads";
	});
});




var bottomId = Titanium.UI.createTextField({
    hintText : 'Type your ad unit id or use default',
    top : "75%",
    left: 10, 
    right: 10, 
    height : 35
});

var bottomButton = Titanium.UI.createButton({
	top:"80%",
	title: 'Bottom Ad',
   	width: "35%",
   	left: 10,
   	height: 40
});

var bottomLabel = Titanium.UI.createLabel({
	value: "",
	color:"#e3e3e3",
	top: "80%",
	left: "40%",
	height: 40
});


bottomButton.addEventListener('click', function(e) {
	var publishId = ((bottomId.value == null || bottomId.value.length == 0)? "ca-app-pub-5795209374815138/9811324006" : bottomId.value);
	var bottomAdv = admob.createView({
		bottom: 0,
		adSizeType: 'SMART',
		publisherId: publishId,
		testing: false
	});		
	self.add(bottomAdv);
	bottomLabel.text = 'Requesting smart buttom banner ads...';
	
	bottomAdv.addEventListener('ad_received', function(e) {
		bottomLabel.text = "Ads received";
	});
	
	bottomAdv.addEventListener('ad_not_received', function(e) {
		bottomLabel.text = "Fail to load ads";
	});
});




var interId = Titanium.UI.createTextField({
    hintText : 'Type your ad unit id or use default',
    top : "45%",
    left: 10, 
    right: 10, 
    height : 35
});


var interButton = Titanium.UI.createButton({
	top:"50%",
	title: 'Interstitial Ad',
   	width: "35%",
   	height: 40,
   	left: 10
});

var interLabel = Titanium.UI.createLabel({
	value: "",
	color:"#e3e3e3",
	top: "50%",
	left: "40%",
	height: 40
});

interButton.addEventListener('click', function(e) {
	var publishId = ((interId.value == null || interId.value.length == 0)? "ca-app-pub-5795209374815138/2288057201" : interId.value);
	
	var inter = admob.createView({
		bottom: 0,
		adSizeType: 'INTERSTITIALAD',
		publisherId: publishId,
		testing: false
	});		
	self.add(inter);
	interLabel.text = 'Requesting interstitial ads...';
	
	inter.addEventListener('ad_received', function(e) {
		interLabel.text = "Ads received";
	});
	
	inter.addEventListener('ad_not_received', function(e) {
		interLabel.text = "Fail to load ads";
	});
});


baseView.add(topId);
baseView.add(topButton);
baseView.add(topLabel);

baseView.add(interId);
baseView.add(interButton);
baseView.add(interLabel);

baseView.add(bottomId);
baseView.add(bottomButton);
baseView.add(bottomLabel);

self.add(baseView);


self.open();
