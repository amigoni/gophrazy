YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "InAppProductsModule",
        "Product",
        "Purchase"
    ],
    "modules": [
        "Readme"
    ],
    "allModules": [
        {
            "displayName": "Readme",
            "name": "Readme",
            "description": "## Introduction\n\nThis module allows the Titanium developer to provide in-app products.\n\n## Usage\n\n**Note**: This module provides functionality that **will not work** in the\nsimulator. Please run your tests on an actual device.\n\nThis module provides access to the products defined in Google Play Developer\nConsole. You will need to add your app to Google Play Developer\nConsole and define the in-app products there.\n\nThe following code segments are not complete; they only demonstrate the\nessentials of using the module. See <code>example/app.js</code> for a complete \nexample.\n\nYou load the module as usual:\n\n\tvar InAppProducts = require('com.logicallabs.inappproducts');\n\nBasic steps to get a list of available products:\n\n\tInAppProducts.addEventListener('receivedProducts', function(e) {\n\t\te.products.forEach(...\n\t});\n\t\n\tif (InAppProducts.getProducts({ SKUs: productIDs })) {\n\t\tTi.API.info('getProducts request started successfully.');\n\t} else {\n\t\talert('Error: could not start getProducts request!');\n\t}\n\nBasic steps to purchase a product:\n\n\tInAppProducts.addEventListener('purchaseUpdate', function(e) {\n       switch (e.purchase.state) {\n\t\t\t...\n\t});\n\tproductObj.purchase();\n\nBasic steps to get a list of purchased (and not yet consumed) products:\n\n\tInAppProducts.addEventListener('receivedPurchases', function(e) {\n       e.purchases.forEach(...\n\t});\n\tInAppProducts.getPurchases();\n\n\n## Consuming a Purchase\n\nThe purchase process of in-app products on Android includes an optional steps\nof consuming the purchase. This is useful for \"consumable\" purchases such as\ncoins in a game, as opposed to permanent purchases such as enabling a\nfor-fee feature. The purchase can be consumed by calling the\n{{#crossLink \"Purchase/consume:method\"}}\n{{/crossLink}} method. Once this method is called, the purchase in question\nwill not show up in the list of purchases provided by the\n{{#crossLink \"InAppProductsModule/getPurchases:method\"}}\n{{/crossLink}} method.\n\n## Issues and Limitations\n\nReceipt verification is not part of the module at\nthis time -- the best\npractice encouraged by Google is to perform this verification on an independent\nserver, not on the device.\n\n## Change Log\n\n### Version 1.0.0\n\n* First release\n\n### Version 1.0.1\n\n* Added safeguard against running module in emulator.\n\n## Author\n\ntitanium@logicallabs.com\n\n## License\n\nLogical Labs Commercial License\n\n## Copyright\n\nCopyright (c) 2013 by Logical Labs, LLC"
        }
    ]
} };
});