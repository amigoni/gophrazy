/**
 * InMobi Ads Plugin Sample * 
 * 
 * Author: Naresh Kapse
 * Company: InMobi
 */

// open a single window
var win = Ti.UI.createWindow({
       backgroundColor:'white'
});

var Inmobi = require('ti.inmobi');

var appId = "68755e31ebf943c5baa77128523a4de4";
// if (OS_ANDROID)
// {
// 	appId = "68755e31ebf943c5baa77128523a4de4";
// }

// You can get your APP ID from www.inmobi.com
// For Optional parameters
// Visit: https://docs.google.com/a/inmobi.com/document/d/168iVSDEW5NPqMlWAAf4m5wZwcsiPKNQ3WwNgBFcV4CA/edit
Inmobi.initWithParams(appId, {
/* Set User Information - Optional (for better Ad Targeting) */
"logLevel": Inmobi.CONSTANTS.LOGLEVEL_DEBUG,
"gender": Inmobi.CONSTANTS.GENDER_MALE,
"education": Inmobi.CONSTANTS.EDUCATION_HIGHSCHOOLORLESS,
"ethnicity": Inmobi.CONSTANTS.ETHNICITY_ASIAN, 
"dob": "29-11-1984",
"income": "10000",
"age": "30",
"maritalStatus": Inmobi.CONSTANTS.MARITAL_STATUS_SINGLE,
"hasChildren": Inmobi.CONSTANTS.TRUE,
"sexualOrientation": Inmobi.CONSTANTS.SEXUAL_ORIENTATION_STRAIGHT,
"language": "eng",
"postalCode": "11111",
"areaCode": "435",
"interests": "swimming, adventure sports",
"deviceIdMasks": [Inmobi.CONSTANTS.EXCLUDE_ADVERTISER_ID, Inmobi.CONSTANTS.EXCLUDE_ODIN1, Inmobi.CONSTANTS.EXCLUDE_UDID]
});

// InMobi Banner AD with callback functions.
var bannerAd = Inmobi.createBannerAd({
  "backgroundColor":"red",
  "top": 0,
  "left": 0,
  "width":320,
  "height":50,
  "adSize": Inmobi.CONSTANTS.ADSIZE_320X50,
  "refreshInterval": "-1",
  "keywords": "games, chess, magazines",
  "additionalParams": { "myname": "nkapser" },
  "refTagKey": "from",
  "refTagValue": "inmobi"
});

win.add(bannerAd);

bannerAd.addEventListener('onAdRequestCompleted', function(){
	Ti.API.info("banner Ad Request Completed!");
});
bannerAd.addEventListener('onAdRequestFailed', function(e){
	Ti.API.info("banner Ad Request Failed! "+e.message);
});
bannerAd.addEventListener('onClick', function(){
	Ti.API.info("Banner was clicked!");
});
bannerAd.addEventListener('beforePresentScreen', function(){
	Ti.API.info("Banner will go fullscreen!");
});
bannerAd.addEventListener('beforeDismissScreen', function(){
	Ti.API.info("Banner Ad will dismiss fullscreen!");
});
bannerAd.addEventListener('onDismissScreen', function(){
	Ti.API.info("Banner Ad dismissed fullscreen!");
});
bannerAd.addEventListener('onLeaveApplication', function(){
	Ti.API.info("Banner Ad will leave application!");
});

/*
 * Button to refresh banner ad.
 */
var refreshBannerButton = Ti.UI.createButton({
	title: "Refresh Banner",
	top: 240,
	width: 280,
	height: 40	
});
win.add(refreshBannerButton);

refreshBannerButton.addEventListener("click", function(event){
	bannerAd.refresh();
});

// InMobi Interstitial AD with callback functions.

var interstitialAd = Inmobi.createInterstitialAd({
  // Optional Params
  additionalParams: {},
  keywords: 'music, jazz, guitar', // provides App View context.
  "refTagKey": "",
  "refTagValue": ""
});

// if (OS_ANDROID){
// 	win.add(interstitialAd);	
// }

// Ad Request Lifecycle Messages
interstitialAd.addEventListener('onAdRequestCompleted', function(){
	Ti.API.info("interstitial Ad Request Completed!");
});
interstitialAd.addEventListener('onAdRequestFailed', function(e){
	Ti.API.info("interstitial Ad Request Failed! "+e.message);
});

// Display-Time Lifecycle Messages
interstitialAd.addEventListener('beforePresentScreen', function(){
	Ti.API.info("interstitial Ad will go fullscreen!");
});
interstitialAd.addEventListener('beforeDismissScreen', function(){
	Ti.API.info("interstitial Ad will dismiss fullscreen!");
});
interstitialAd.addEventListener('onDismissScreen', function(){
	Ti.API.info("interstitial Ad fullscreen dismissed!");
});
interstitialAd.addEventListener('onLeaveApplication', function(){
	Ti.API.info("interstitial Ad will leave application!");
});
interstitialAd.addEventListener('onClick', function(){
	Ti.API.info("interstitial Ad Clicked!");
});

/*
 * Button to load interstitial ad.
 */
var loadInterstitialAdButton = Ti.UI.createButton({
	title: "Load Interstitial",
	top: 300,
	width: 280,
	height: 40	
});
win.add(loadInterstitialAdButton);

loadInterstitialAdButton.addEventListener("click", function(event){
	interstitialAd.load();
});

/*
 * Button to show interstitial ad.
 */
var showInterstitialAdButton = Ti.UI.createButton({
	title: "Show Interstitial",
	top: 360,
	width: 280,
	height: 40	
});
win.add(showInterstitialAdButton);

showInterstitialAdButton.addEventListener("click", function(event){
	interstitialAd.show();
});


// InMobi Analytics Code

//Inmobi.analytics_startSessionManually();
/*
Inmobi.analytics_endSessionManually();

Inmobi.analytics_beginSection("home", {"user": "nkapser"});
Inmobi.analytics_endSection("sectionName", {});
Inmobi.analytics_tagEvent("eventName", {});

*/

win.open();