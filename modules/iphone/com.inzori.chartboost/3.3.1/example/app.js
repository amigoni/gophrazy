
// open a single window
var win = Ti.UI.createWindow({
	backgroundColor:'white'
});
var button1 = Ti.UI.createButton({
	borderWidth:1,
	borderColor: '#ddd',
	height: 60,
	width: 200,
	top: 100,
	title: 'interstitial'
});
win.add(button1);

var button2 = Ti.UI.createButton({
	borderWidth:1,
	borderColor: '#ddd',
	height: 60,
	width: 200,
	top: 180,
	title: 'show more apps'
});
win.add(button2);


var button3 = Ti.UI.createButton({
	borderWidth:1,
	borderColor: '#ddd',
	height: 60,
	width: 200,
	top: 260,
	title: 'interstitial somewhereelse'
});
win.add(button3);

var button4 = Ti.UI.createButton({
	borderWidth:1,
	borderColor: '#ddd',
	height: 60,
	width: 200,
	top: 340,
	title: 'interstitial afterpicture'
});
win.add(button4);


win.open();

// TODO: write your module tests here
var Chartboost = require('Chartboost');
Ti.API.info("module is => " + Chartboost);

Chartboost.init();

Chartboost.addListeners();	



button1.addEventListener('click',function(){
	
	Chartboost.showInterstitial();	//default location
});
button3.addEventListener('click',function(){
	
	Chartboost.showInterstitial("somewhereelse");	//location: somewhereelse
});
button4.addEventListener('click',function(){
	
	Chartboost.showInterstitial("afterpicture");	//location: afterpicture
});

button2.addEventListener('click',function(){
	
	Chartboost.showMoreApps();
});
