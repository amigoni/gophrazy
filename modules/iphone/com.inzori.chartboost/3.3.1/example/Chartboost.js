var Chartboost = require('com.inzori.chartboost');

var _chartboost = {
	appId: YOUR_APPID,
	appSignature: YOUR_APPSIGNATURE,
	cacheLocations: true,
	locations: ["startup","afterpicture","somewhereelse"]
};
	
exports.init = function(){
	Chartboost.init({
		appId: 			_chartboost.appId, 
		appSignature: 	_chartboost.appSignature, 
		cache: 			_chartboost.cacheLocations,	// optional, default=false
		locations: 		_chartboost.locations		// optional, use this parameter to cache each location you will use
	});	
};
exports.getChartboost = function(){
	return _chartboost;
};

exports.showInterstitial = function(location){
	if (location)
		Chartboost.showInterstitial(location);
	else
		Chartboost.showInterstitial();
	
};

exports.showMoreApps = function(){
	Chartboost.showMoreApps();	
};

exports.addListeners = function(){
	// Listeners
	// this event is fired when the init method finds an error
	Chartboost.addEventListener('chartboostmodule:init',function(e){
		
		Ti.API.info(e.message);

	});
	
	/*
	 * didFailToLoadInterstitial
	 *
	 * This is called when an interstitial has failed to load for any reason
	 *
	 * Is fired on:
	 * - No network connection
	 * - No publishing campaign matches for that user (go make a new one in the dashboard)
	 */
	Chartboost.addEventListener('chartboostmodule:didFailToLoadInterstitial',function(e){
		Ti.API.info('chartboostmodule:didFailToLoadInterstitial: ' + e.message);
	});
	
	/*
	 * didCacheInterstitial
	 *
	 * Passes in the location name that has successfully been cached.
	 *
	 * Is fired on:
	 * - All assets loaded
	 * - Triggered by cacheInterstitial
	 *
	 * Notes:
	 * - Similar to this is: cb.hasCachedInterstitial(String location)
	 * Which will return true if a cached interstitial exists for that location
	 */
	Chartboost.addEventListener('chartboostmodule:didCacheInterstitial',function(e){
		Ti.API.info('chartboostmodule:didCacheInterstitial: ' + e.message);
	});
	
	/*
	 * didFailToLoadMoreApps
	 *
	 * This is called when the more apps page has failed to load for any reason
	 *
	 * Is fired on:
	 * - No network connection
	 * - No more apps page has been created (add a more apps page in the dashboard)
	 * - No publishing campaign matches for that user (add more campaigns to your more apps page)
	 *  -Find this inside the App > Edit page in the Chartboost dashboard
	 */
	Chartboost.addEventListener('chartboostmodule:didFailToLoadMoreApps',function(e){
		Ti.API.info('chartboostmodule:didFailToLoadMoreApps: ' + e.message);
	});
	
	/*
	 * didDismissInterstitial
	 *
	 * This is called when an interstitial is dismissed
	 *
	 * Is fired on:
	 * - Interstitial click
	 * - Interstitial close
	 *
	 */
	Chartboost.addEventListener('chartboostmodule:didDismissInterstitial',function(e){
		Ti.API.info('chartboostmodule:didDismissInterstitial: '+ e.message);
	});
	
	/*
	 * didDismissMoreApps
	 *
	 * This is called when the more apps page is dismissed
	 *
	 * Is fired on:
	 * - More Apps click
	 * - More Apps close
	 *
	 */
	Chartboost.addEventListener('chartboostmodule:didDismissMoreApps',function(e){
		Ti.API.info('chartboostmodule:didDismissMoreApps: ' + e.message);
	});	
};
